#! /bin/bash
#*******************************************************************************
# E.S.O. - ALMA project
#
# "@(#) $Id$"
#
# who        when        what
# --------   ----------  ----------------------------------------------
# agrimstrup 2007-07-10  created
#

#************************************************************************
#   NAME
#
#   SYNOPSIS
#
#   DESCRIPTION
#
#   FILES
#
#   ENVIRONMENT
#
#   RETURN VALUES
#
#   CAUTIONS
#
#   EXAMPLES
#
#   SEE ALSO
#
#   BUGS
#
#------------------------------------------------------------------------
#

#
# Install Python Modules
#
#set -x
#export PYTHON_ROOT=/alma/$ALMASW_RELEASE/Python

# Load functions
. standardUtilities
#
# Fetch operating system and release version
#
os_discovery

LOG=buildIERSCache.log
CWD=`pwd`
#
exec > $LOG 2>&1

date

if [ ! -d $PYTHON_ROOT ]
then
  echo "$PYTHON_ROOT not found, cannot continue..."
  exit 1
fi

echo installing IERS Cache data for Astropy

cd ../PRODUCTS

# Run a function in astropy that clears the cache, then requires the IERS data download.
echo "Run script to download IERS cache data"

python download-iers.py

cd $CWD
result=$(grep Failed ${LOG}|wc -l)
if [ $result -gt 0 ]
then
    echo "IERS cache fail."
    date
    exit 1
else
    echo "IERS cache done."
fi
