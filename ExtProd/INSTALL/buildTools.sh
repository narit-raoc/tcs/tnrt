#! /bin/bash
#*******************************************************************************
# E.S.O. - VLT project
#
# build
#
# "@(#) $Id: buildTools,v 1.39 2012/01/09 14:39:47 tstaig Exp $"
#
# who       when        what
# --------  ----------  ----------------------------------------------
# eallaert  2007-09-03  updated for ACS 7.0, to install also Tcl/Tk

#************************************************************************
#   NAME
#   build - build ACS software TOOLS
#
#   SYNOPSIS
#   build 
#
#   DESCRIPTION
#   Wrapper script to control the overall building of ACS Software TOOLS.
#
#   According to the current environment, the procedure builds all
#   what can be built by calling the build<XXXX> specific procedure.
#   
#   Use the -vw command line option to force building VxWorks on Linux.
#   VxWorks is built automatically only on Sun. When VxWorks is built,
#   also msql and msqltcl will be generated.
#
#   FILES
#   ./buildOutput.<host>  WRITE  file containing the stout
#   ./buildError.<host>   WRITE  file containing the stout
#
#------------------------------------------------------------------------

#********************************************
# Read and execute instructions in:         *
#  README                                   *
#********************************************
#
# which platform?
#
. standardUtilities
os_discovery

setup_colors

if ! (! lsb_loc="$(type -p "lsb_release")" || [ -z "$lsb_loc" ])
then
	LSB_DIST=`lsb_release -d | awk '{print $2}'`

else	
	LSB_DIST="Other"
fi

#
# No INTROOT please!
#
if [ "$INTROOT" != "" ]
then 
    echo ""
    echo " ERROR: INTROOT shall not be defined (current \$INTROOT >$INTROOT<)"
    echo "     This installation procedure cannot handle this."
    echo ""
    exit 1
fi

TITLE="TNRT Common Software TOOLS"

#
# Some builds might failed if the tty is closed
#
echo "WARNING: Do not close this terminal: some build might fail!"

if [ "X$DISPLAY" = "X" ]
then
   echo "WARNING: DISPLAY not set. Some build/tests might fail!"
fi 

#
# build Gildas software see inside buildGidas
#
echo -n "buildGildas"
./buildGildas.sh && echo_success || echo_failure

#
# Install Python modules from pip using selected pyenv version
#
echo -n "buildPyModules"
./buildPyModules.sh && echo_success || echo_failure

active_py_ver=$(python --version)

# Python < 3.7 requires build PyQt5 from source.  Python > 3.7 installs from pip
if [ "$active_py_ver" = "Python 3.6.9" ]; then
    # buildPyQ5.sh requires sip-install from the previous script buildPyModules.sh
    # Therefore, we must re-evaluate the environment PATH using bash --login and reload ./bash_profile.
    echo -n "Found active version $active_py_ver"
    echo -n "buildPyQt5"
    bash --login ./buildPyQt5.sh > /dev/null && echo_success || echo_failure
fi

#
# Download new IERS cache data from Internet.  It is used heavily in Astropy.
# This data will need update approximately every month.  Automatic in Astropy, but requires
# internet.
#
echo -n "buildIERSCache"
./buildIERSCache.sh && echo_success || echo_failure

#
# build Atmosphere model am-10.0 and install it
# Also install atm binary file for simple model.
#
echo -n "buildAtmosphereModel"
./buildAtmosphereModel.sh && echo_success || echo_failure


echo "WARNING: Now log out and login again to make sure that"
echo "         the environment is re-evaluated!"
echo " "
echo "__oOo__"

