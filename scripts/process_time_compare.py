import argparse
from argparse import RawTextHelpFormatter
import os
import sys
import numpy as np
from astropy.io import fits
from astropy.time import Time

import pyqtgraph as pg
from pyqtgraph.Qt import QtGui

pg.setConfigOptions(antialias=True)

# NOTE: Default is dark background and CMY colors. Set white_background=True
# if you want white background and RGB colors.
white_background = False
# white_background = True

if white_background is True:
    pg.setConfigOption("background", "w")
    pg.setConfigOption("foreground", "k")

    # Use RGB color for white background
    color_acu = "b"
    color_tcs = "r"
    color_edd = "g"
    color_diff = "k"
    color_subsline = "k"
else:
    # Use CMY color for dark background
    color_acu = "y"
    color_tcs = "m"
    color_edd = "c"
    color_diff  = "b"
    color_subsline = "w"

# Setup rules for command line arguments
parser = argparse.ArgumentParser(
    description="Reads FITS file written by PipelineMbfits and diplays some graphs",
    formatter_class=RawTextHelpFormatter,
)
parser.add_argument(
    "filename", help="FITS file with format specified by DataFormatMbfits.py"
)

# If no command line arguments are avaiable, show help message and exit
if len(sys.argv[1:]) == 0:
    parser.print_help()
    exit()

# Read command line arguments into argparse structure
args = parser.parse_args()

hdul = None

if os.path.isfile(args.filename) is not True:
    print("ERROR: File not found")
    print("os.path.isfile(%s) = False" % args.filename)
    hdul = None
else:
    print("Processing file %s: " % args.filename)
    fid = open(args.filename, "rb")
    hdul = fits.open(fid)
    # leave the file open becuase fits.open will not store the entire file in RAM if it is
    # too big.  Close file at the end of this script.
    # print(hdul.info())
if hdul is None:
    exit()

# -----------------------------------------------------------------------------
# Load Data
# -----------------------------------------------------------------------------
# Strings for labels
str_title = "From File {}".format(os.path.basename(args.filename))

AZ = 0
EL = 1

# Make a time axis for all the plots.  Use seconds after start
tstart = hdul["MONITOR-ANT-PACKET"].data["time_tcs_mjd"][0]
tt = (hdul["MONITOR-ANT-PACKET"].data["time_tcs_mjd"] - tstart) * 86400.0

# Calcualte difference of time between TCS and ACU.
SECONDS_PER_DAY = 86400

deltat_tcs_acu = SECONDS_PER_DAY * (
    hdul["MONITOR-ANT-PACKET"].data["time_tcs_mjd"]
    - hdul["MONITOR-ANT-GENERAL"].data["actual_time"]
)

SECONDS_PER_DAY = 86400

# Get timestamps of spectrum packets for all subscans
datapar_hdulist = []

for hdu in hdul:
    print("found HDU name: {}".format(hdu.name))

    # Get all of the DATAPAR tables from the file
    if hdu.name == "DATAPAR-MBFITS":
        print("found subscan number: {}, start time: {}".format(hdu.header["SUBSNUM"], hdu.header['DATE-OBS']))
        print("Append 'DATAPAR-MBFITS' to datapar_list")
        if hdu.data["MJD"][0] == 0.0:
            print(
                "Found timestamp MJD == 0.  Looks like dummy row. discard and continue"
            )
            continue
        datapar_hdulist.append(hdu)
        


# Initialize the QtGui application before we create any PyQtGraph objects
app = QtGui.QApplication([])
grid_opacity = 0.7

# Compare ACU time stamps and TCS timestamps for each ACU status message received by TCS
layout_tcs_acu = pg.GraphicsLayoutWidget(show=True)
layout_tcs_acu.resize(1024, 768)
layout_tcs_acu.setWindowTitle("ACU and TCS Time Compare: {}".format(str_title))

# Create PlotItems and add to the layout
pi_timestamp = pg.PlotItem(title=str_title)
pi_step = pg.PlotItem()
pi_delta_tcs_acu = pg.PlotItem()

# Add to layout
layout_tcs_acu.addItem(pi_timestamp, 0,0,1,1)
layout_tcs_acu.addItem(pi_delta_tcs_acu, 1,0,1,1)

# Enable Legend for all PlotItems (must add Legend before we add PlotDataItems that have param name="xxxxx")
pi_timestamp.addLegend()
pi_delta_tcs_acu.addLegend()


# Add PlotDataItems to the PlotItems (PlotDataItem is return by function PlotItem.plot()) 
pi_timestamp.plot(
    hdul["MONITOR-ANT-PACKET"].data["status_message_counter"],
    hdul["MONITOR-ANT-GENERAL"].data["actual_time"],
    name="ACU",
    pen=None,
    symbol="o",
    symbolPen=color_acu,
    symbolBrush=None,
    symbolSize=16,
)

pi_timestamp.plot(
    hdul["MONITOR-ANT-PACKET"].data["status_message_counter"],
    hdul["MONITOR-ANT-PACKET"].data["time_tcs_mjd"],
    name="TCS",
    pen=None,
    symbol="d",
    symbolPen=color_tcs,
    symbolBrush=None,
)


pi_delta_tcs_acu.plot(
    deltat_tcs_acu,
    name="Difference (TCS - ACU)",
    pen=None,
    symbol="o",
    symbolPen=color_diff,
    symbolBrush=None,
)

ax_timestamp_B = pg.AxisItem(orientation="bottom", text="ACU status message counter")
ax_timestamp_L = pg.AxisItem(orientation="left", text="Time (MJD)")
ax_delta_tcs_acu_B = pg.AxisItem(orientation="bottom", text="ACU status message counter")
ax_delta_tcs_acu_L = pg.AxisItem(orientation="left", text="TCS-ACU", units="s")

# Enable axis label (why is this False by default in PyQtGraph?)
ax_list = [ax_timestamp_B, ax_timestamp_L, ax_delta_tcs_acu_B, ax_delta_tcs_acu_L]
for ax in ax_list:
    ax.showLabel(True)

# Add Axes
pi_timestamp.setAxisItems(
    axisItems={
        "bottom": ax_timestamp_B,
        "left": ax_timestamp_L,
    }
)

pi_delta_tcs_acu.setAxisItems(
    axisItems={
        "bottom": ax_delta_tcs_acu_B,
        "left": ax_delta_tcs_acu_L,
    }
)

# Enable grid lines
pi_timestamp.showGrid(True, True, alpha=grid_opacity)
pi_step.showGrid(True, True, alpha=grid_opacity)
pi_delta_tcs_acu.showGrid(True, True, alpha=grid_opacity)

# Display timestamps of ACU, TCS, and EDD Spectrometer on the same Timeline
layout_time3sys = pg.GraphicsLayoutWidget(show=True)
layout_time3sys.resize(1024, 768)
layout_time3sys.setWindowTitle("Timeline ACU, TCS, EDD Spectrometer: {}".format(str_title))

# Create PlotItems and add to the layout
pi_time3sys = pg.PlotItem(title=str_title)
pi_step3sys = pg.PlotItem()

# Add to layout
layout_time3sys.addItem(pi_time3sys, 0,0,1,1)
layout_time3sys.addItem(pi_step3sys, 1,0,1,1)

# Enable Legend for all PlotItems (must add Legend before we add PlotDataItems that have param name="xxxxx")
pi_time3sys.addLegend()
pi_step3sys.addLegend()

# Add PlotDataItems to the PlotItems (PlotDataItem is return by function PlotItem.plot()) 
# Timestamps arrive from EDD Spectrometer independent of ACU and TCS.  
# For each message received from ACU, TCS records it's own local timestamp to the file
#
# For each message received from EDD Spectrometer, TCS does not record a local timestamp.  It
# trusts that the timestamp embedded in the EDD Fits spectrum packet is correct.  Perhaps we should
# add ad feature in EddFitsClient to keep a local TCS timestamp for each packet recevied from
# EddFitsServer.
#
# We cannot plot EDD Spectrometer packet timestamps on a Y (dependent) axis 
# when ACU or TCS is on the X (independent) axis because there is no dependency in the data,
# and the number (and spacing between) samples is different.  Both *should* be synchronized
# to the same NTP server, but we cannot confirm if this is True from the data file alone.

time3sys_acu_unix = Time(hdul["MONITOR-ANT-GENERAL"].data["actual_time"], format="mjd").unix
pi_time3sys.plot(
    time3sys_acu_unix,
    np.zeros(len(time3sys_acu_unix)),
    name="ACU",
    pen=None,
    symbol="o",
    symbolPen=color_acu,
    symbolBrush=None,
    symbolSize=20,
)

time3sys_tcs_unix = Time(hdul["MONITOR-ANT-PACKET"].data["time_tcs_mjd"], format="mjd").unix
pi_time3sys.plot(
    time3sys_tcs_unix,
    np.zeros(len(time3sys_tcs_unix)),
    name="TCS",
    pen=None,
    symbol="d",
    symbolPen=color_tcs,
    symbolBrush=None,
    symbolSize=16
)

# Traverse all DATAPAR arrays (1 per subscan).  
#   - Plot the timestamps of all EDD Spectrometer timestamps
#   - Draw a line to indicate start of each subscan
first_subscan = True
for d in datapar_hdulist:
    timestamps_edd_unix = Time(d.data['MJD'], format="mjd").unix
    if first_subscan:
        line_name="EDD Spectrometer"
        first_subscan = False
    else:
        line_name=None

    pi_time3sys.plot(
        timestamps_edd_unix,
        np.zeros(len(timestamps_edd_unix)),
        name=line_name,
        pen=None,
        symbol="+",
        symbolPen=color_edd,
        symbolBrush=None,
        symbolSize=12
    )
    subscan_start = Time(d.header['DATE-OBS'], format="isot").unix
    subscan_start_line = pg.InfiniteLine(pos=subscan_start, 
        angle=90, 
        movable=False, 
        pen=color_subsline, 
        label="Subscan {}".format(d.header["SUBSNUM"]),
        labelOpts = {"rotateAxis":(1,0), "position":0.2})
    pi_time3sys.addItem(subscan_start_line)

# Get the ACU timestamps from 1 to end (discard the first one) to make array length
# same as output of np.diff.   This is the timestamp after the interval we measure with np.diff
time_interval_end_acu_unix= Time(hdul["MONITOR-ANT-GENERAL"].data["actual_time"][1:], format="mjd").unix
timestep_acu = SECONDS_PER_DAY*np.diff(hdul["MONITOR-ANT-GENERAL"].data["actual_time"])
mean_timestep_acu = np.mean(timestep_acu)
median_timestep_acu = np.median(timestep_acu)
stdev_timestep_acu = np.std(timestep_acu)
slope_timestep_acu = np.mean(np.diff(timestep_acu))
pi_step3sys.plot(
    time_interval_end_acu_unix,
    timestep_acu,
    name="ACU. mean={:.3f}s, median={:.3f}s, std={:3f}s, slope={}s/point".format(
        mean_timestep_acu, median_timestep_acu, stdev_timestep_acu, slope_timestep_acu),
    pen=None,
    symbol="o",
    symbolPen=color_acu,
    symbolBrush=None,
    symbolSize=16,
)

time_interval_end_tcs_unix= Time(hdul["MONITOR-ANT-PACKET"].data["time_tcs_mjd"][1:],format="mjd").unix
timestep_tcs = SECONDS_PER_DAY*np.diff(hdul["MONITOR-ANT-PACKET"].data["time_tcs_mjd"])
mean_timestep_tcs = np.mean(timestep_tcs)
median_timestep_tcs = np.median(timestep_tcs)
stdev_timestep_tcs = np.std(timestep_tcs)
slope_timestep_tcs = np.mean(np.diff(timestep_tcs))
pi_step3sys.plot(    
    time_interval_end_tcs_unix,
    timestep_tcs,
    name="TCS. mean={:.3f}s, median={:.3f}s, std={:3f}s, slope={}s/point".format(
        mean_timestep_tcs, median_timestep_tcs, stdev_timestep_tcs, slope_timestep_tcs),
    pen=None,
    symbol="d",
    symbolPen=color_tcs,
    symbolBrush=None,
    symbolSize=16
)



# Calculate statistics of all timestamps before making graphs because
# we need results of statistics to show in the Legend labels
timestep_edd_all_subscans = []
for d in datapar_hdulist:
    timestep_edd_all_subscans = timestep_edd_all_subscans + list(SECONDS_PER_DAY * np.diff(d.data['MJD']))
timestep_edd_all_subscans_ndarray = np.array(timestep_edd_all_subscans)
mean_timestep_edd = np.mean(timestep_edd_all_subscans_ndarray)
median_timestep_edd = np.median(timestep_edd_all_subscans_ndarray)
stdev_timestep_edd = np.std(timestep_edd_all_subscans_ndarray)
slope_timestep_edd = np.mean(np.diff(timestep_edd_all_subscans_ndarray))

first_subscan = True
for d in datapar_hdulist:
    time_interval_end_edd_unix = Time(d.data['MJD'][1:], format="mjd").unix
    timestep_edd = SECONDS_PER_DAY * np.diff(d.data['MJD'])
    if first_subscan:
        line_name="EDD Spectrometer. mean={:.3f}s, median={:.3f}s, std={:3f}s, slope={}s/point".format(
            mean_timestep_edd, median_timestep_edd, stdev_timestep_edd, slope_timestep_edd)
        first_subscan = False
    else:
        line_name=None

    pi_step3sys.plot(
        time_interval_end_edd_unix,
        timestep_edd,
        name=line_name,
        pen=None,
        symbol="+",
        symbolPen=color_edd,
        symbolBrush=None,
        symbolSize=12
    )
    subscan_start = Time(d.header['DATE-OBS'], format="isot").unix
    subscan_start_line = pg.InfiniteLine(pos=subscan_start, 
        angle=90, 
        movable=False, 
        pen=color_subsline, 
        label="Subscan {}".format(d.header["SUBSNUM"]),
        labelOpts = {"rotateAxis":(1,0)})

    pi_step3sys.addItem(subscan_start_line)

ax_time3sys_B = pg.DateAxisItem(orientation="bottom", utcOffset=0,text="Universal Time Coordinated (UTC)",)
ax_step3sys_B = pg.DateAxisItem(orientation="bottom", utcOffset=0,text="Universal Time Coordinated (UTC)",)
ax_step3sys_L = pg.AxisItem(orientation="left", text="<font>&Delta;Time step per packet</font>", units="s")

# Enable axis label (why is this False by default in PyQtGraph?)
ax_list = [ax_time3sys_B, ax_step3sys_B, ax_step3sys_L,]
for ax in ax_list:
    ax.showLabel(True)

pi_time3sys.setAxisItems(
    axisItems={
        "bottom": ax_time3sys_B,
    }
)
pi_time3sys.hideAxis("left")

pi_step3sys.setAxisItems(
    axisItems={
        "bottom": ax_step3sys_B,
        "left": ax_step3sys_L,
    }
)

# Enable grid lines
pi_step3sys.showGrid(False, True, alpha=grid_opacity)


# Start the Qt event loop and block this script until user closes the GUI window
# QtGui.QApplication.instance().exec_()
app.exec_()

# Close the file
fid.close()
