# standard python
import os
import socket
import ctypes
import threading
import logging
import coloredlogs
import socket
import datetime
from argparse import ArgumentParser

import DataFormatAcu

class AcuLoggingClient:
    def __init__(self, ip, port, loglevel):
        self.ip = ip
        self.port = port
        self.logger = get_colored_logger(self.__class__.__name__, loglevel)
        
        self.data_dir = os.getenv("DATA_DIR") or os.getenv("HOME") + "/data"
        
        if os.path.exists(self.data_dir):
            self.logger.debug("Environment vars $DATA_DIR={}, $HOME={}".format(os.getenv("DATA_DIR"),os.getenv("HOME")))
            self.logger.info("Using data directory: {}".format(self.data_dir))
        else:
            self.logger.warning("data directory {} does not exist.  Create now".format(self.data_dir))
            os.mkdir(self.data_dir)
        
        self.filename_base = "acu_" + datetime.datetime.utcnow().strftime("%Y%m%d_%H%M%S")
        self.filename = self.data_dir + "/" + self.filename_base + ".csv"
        self.logger.info("Generated complete filepath: " + self.filename)
        
        # Flow control Events
        self.acu_status_stop_event = threading.Event()
        self.logger.info("ACU status client instance created. ip={}, port={}, filename={}".format(self.ip, self.port, self.filename))
        if ip == "192.168.3.228":
            self.logger.warning("Using IP address of ACU Simulator")

    def stop_recv_loop(self):
        try:
            self.logger.debug("close file")
            self.outfile.close()
        except Exception as e:
            self.logger.warning("{}".format(e))
            pass
        self.logger.debug("set acu_status_stop_event to break out of recv_loop")
        self.acu_status_stop_event.set()
    
    def start_recv_loop(self):
        self.logger.info("Connecting to {}:{} ...".format(self.ip, self.port))

        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as acu_status_socket:
            acu_status_socket.connect((self.ip, self.port))
            acu_status_socket.settimeout(10)
            self.logger.info("ACU STATUS port connected")


            self.acu_status_stop_event.clear()     

            self.logger.debug("start recv_loop and block this thread until recv_loop is canceled")
            self.logger.info("Waiting for first data (timeout=10s)...")
            first_data_received = False

            # Loop forever until loop is canceled.  I think it is possible
            # for this loop to exit while a partial (non-blocking) socket read is in progress.
            # For now, I don't care because the client socket will be closed immediately,
            # then the server socket will be closed.  New server and client for the next
            # connection should not be affected by any partial data remaining from an old
            # connection.
        
            while not self.acu_status_stop_event.is_set():
                
                segments = []
                bytes_received = 0
                while bytes_received < ctypes.sizeof(DataFormatAcu.StatusMessageFrame):
                    remaining_bytes = (
                        ctypes.sizeof(DataFormatAcu.StatusMessageFrame) - bytes_received
                    )
                    # self.logger.debug(
                    #     "bytes received: {:d}, remaining_bytes: {:d}".format(
                    #         bytes_received, remaining_bytes
                    #     )
                    # )
                    segment = acu_status_socket.recv(remaining_bytes, socket.MSG_WAITALL)
                    # self.logger.debug(
                    #     "bytes received: {:d}, remaining_bytes: {:d}".format(
                    #         bytes_received, remaining_bytes
                    #     )
                    # )
                    if segment == b"":
                        raise RuntimeError("socket connection broken")
                    segments.append(segment)
                    bytes_received = bytes_received + len(segment)
                
                raw_bytes = b"".join(segments)

                status_message = DataFormatAcu.StatusMessageFrame.from_buffer_copy(
                    raw_bytes
                )

                if first_data_received is False:
                    self.logger.info("Received first data: message_length: {} bytes, status_message_counter: {}, version: {}".format(
                        status_message.message_length,
                        status_message.status_message_counter,
                        status_message.general_sts.version,
                    ))
                    first_data_received = True
                    self.logger.info("Opening file: {}".format(self.filename))
                    self.outfile = open(self.filename, 'at')

                    headerline = "{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{}\n".format(
                        "Time MJD",
                        "Program Track AZ", 
                        "Program Track EL",
                        "Program Offset AZ",
                        "Program Offset EL",
                        "Tracking Desired AZ",
                        "Tracking Desired EL",
                        "Encoder Position AZ",
                        "Encoder Position EL",
                        "Total Pointing Correction AZ",
                        "Total Pointing Correction EL",
                        "Actual Position AZ",
                        "Actual Position EL", 
                        "Safety Device Status 32 bits",
                        "Tracking System Status 32 bits",
                        "Tracking System RUN bit",
                        "Program Track Status 16 bits",
                        "Program Offset Status 16 bits",
                        "AZ trajectory_generator_acceleration",
                        "AZ trajectory_generator_velocity",
                        "AZ desired_velocity",
                        "AZ current_velocity",
                        "AZ position_deviation",
                        "AZ filtered_position_deviation",
                        "ACU Clock Time Offset",
                        "Time Source",
                        "No PPS",
                        "Clock Online",
                        "Clock OK",
                        "Tracking Time Offset",
                        )
                    self.logger.warning("Writing data ... CTRL+C to close file and disconnect".format(self.filename))
                    self.logger.debug(headerline[0:-1])
                    self.outfile.writelines([headerline])
                    
                # note: use tracking_sts.prog_track_az, prog_track_az for Ra/Dec, or Az/El tracking.
                # If we are tracking a satellite using TLE, record tle_track_az, tle_track_el instead.
                dataline = "{},{},{},{},{},{},{},{},{},{},{},{},{},{:d},{:d},{:d},{:d},{:d},{},{},{},{},{},{},{},{:d},{:d},{:d},{:d},{}\n".format(
                    status_message.general_sts.actual_time,
                    status_message.tracking_sts.prog_track_az,
                    status_message.tracking_sts.prog_track_el,
                    status_message.tracking_sts.prog_offset_az,
                    status_message.tracking_sts.prog_offset_el,
                    status_message.tracking_sts.desired_az,
                    status_message.tracking_sts.desired_el,
                    status_message.axis_status[0].encoder_position_incl_pcorr,
                    status_message.axis_status[1].encoder_position_incl_pcorr,
                    status_message.axis_status[0].current_pointing_correction,
                    status_message.axis_status[1].current_pointing_correction,
                    status_message.axis_status[0].actual_position,
                    status_message.axis_status[1].actual_position,
                    status_message.general_sts.safety_device_errors,
                    status_message.tracking_sts.bit_status,
                    1 if (status_message.tracking_sts.bit_status & DataFormatAcu.TR_OBJ_RUN > 0) else 0,
                    status_message.tracking_sts.prog_track_bs,
                    status_message.tracking_sts.prog_offset_bs,
                    status_message.axis_status[0].trajectory_generator_acceleration,
                    status_message.axis_status[0].trajectory_generator_velocity,
                    status_message.axis_status[0].desired_velocity,
                    status_message.axis_status[0].current_velocity,
                    status_message.axis_status[0].position_deviation,
                    status_message.axis_status[0].filtered_position_deviation,
                    status_message.general_sts.actual_time_offset,
                    status_message.general_sts.time_source,
                    status_message.general_sts.no_pps_signal,
                    status_message.general_sts.clock_online,
                    status_message.general_sts.clock_ok,
                    status_message.tracking_sts.time_offset
                )
                self.logger.debug(dataline[0:-1])
                self.outfile.writelines([dataline])
                
                #self.logger.debug("AxisStatus AZ(0): {}".format(status_message.axis_status[0]))
                #self.logger.debug("AxisStatus EL(1): {}".format(status_message.axis_status[1]))
            
            self.logger.debug("break out of receive loop, close file, close socket")
            acu_status_socket.close()        
            
def get_colored_logger(name, log_level):
    fmt_scrn = "%(asctime)s.%(msecs)03d [%(levelname)s]%(message)s"

    level_styles_scrn = {
        "critical": {"color": "red", "bold": True},
        "debug": {"color": "white", "faint": True},
        "error": {"color": "red"},
        "info": {"color": "green", "bright": True},
        "notice": {"color": "magenta"},
        "spam": {"color": "green", "faint": True},
        "success": {"color": "green", "bold": True},
        "verbose": {"color": "blue"},
        "warning": {"color": "yellow", "bright": True, "bold": True},
    }
    field_styles_scrn = {
        "asctime": {},
        "hostname": {"color": "magenta"},
        "levelname": {"color": "cyan", "bright": True},
        "name": {"color": "blue", "bright": True},
        "programname": {"color": "cyan"},
    }
    formatter_screen = coloredlogs.ColoredFormatter(
        fmt=fmt_scrn, level_styles=level_styles_scrn, field_styles=field_styles_scrn
    )

    # creating a handler to log on the console
    handler_screen = logging.StreamHandler()
    handler_screen.setFormatter(formatter_screen)

    logger = logging.getLogger(name)
    logger.addHandler(handler_screen)

    if log_level.upper() == "DEBUG":
        logger.setLevel(logging.DEBUG)
    elif log_level.upper() == "INFO":
        logger.setLevel(logging.INFO)
    elif log_level.upper() == "WARNING":
        logger.setLevel(logging.WARNING)
    elif log_level.upper() == "ERROR":
        logger.setLevel(logging.ERROR)
    else:
        logger.setLevel(logging.DEBUG)
        logger.error("invalid parameter log-level. Select level DEBUG")

    return logger

if __name__ == "__main__":
    parser = ArgumentParser(
        description="Receive ACU status message record selected data to .csv file."
    )
    parser.add_argument("-i", dest="ip", type=str, default=None, help="Host IP address [optional]. Default reads 'acu' from /etc/hosts")
    parser.add_argument("-p", dest="port", type=int, default=8101, help="Port number (7101 or 8101)")
    parser.add_argument("-l", "--log-level", dest="loglevel", type=str, help="Log level", default="INFO"
    )

    args = parser.parse_args()

    logger = get_colored_logger("__main__", args.loglevel)
    logger.info("test logger in module {}".format(__name__))
    
    # If IP address is not specified by parameter -i, try to use acu from /etc/hosts file
    # In case of dev config, it connects to ACU Simulator.
    # In case of production config, it connects to real ACU
    if args.ip == None:
        try:
            acu_hostname = "acu"
            ip = socket.gethostbyname(acu_hostname)
        except Exception as e:
            logger.error("{}: '{}'.  Check file /etc/hosts".format(e, acu_hostname))
            exit()
    # If specified by parameter -i, do not read /etc/hosts.  Use the IP address string
    # directly and catch connection errors
    else:
        ip = args.ip

    try:
        client = AcuLoggingClient(ip, args.port, args.loglevel)
        client.start_recv_loop()
    except ConnectionRefusedError:
        logger.error(
            "ConnectionRefusedError.  Cannot connect to {}:{}".format(args.ip, args.port))
        pass
    except KeyboardInterrupt:
        logger.warning("Catch KeyboardInterrupt.  stop client")
        client.stop_recv_loop()
