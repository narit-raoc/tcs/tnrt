# Clear pointing model that is stored in from ACU
clear_pmodel()

# Basic schedule parameters (run ASAP)
project_id = "az_limit_wrap"
obs_id = "SS"
schedule_params = ScheduleParams(project_id, obs_id)

# Specify duration and create on Source.  use for all scans
duration = 10
scantype_params = ScantypeParamsOnSource(duration)

# ------------------------------------------
# Test 1: Go to starting position in region AZ > 240
# ------------------------------------------
goto_azel(241, 45)

# Choose an AZ , EL in the region < 60
az = 59 * units.deg # should shortcut to +419
el = 45 * units.deg 
logger.debug("Choose a coordinate above the horizon now. (AZ, EL) = ({}, {}) [deg]".format(az.value, el.value))

# Transform the AZ, EL coordinate to ICRS that will be used to configure the tracking.
coord_altaz = SkyCoord(
    frame="altaz",
    az=az,
    alt=el,
    obstime=Time.now(),
    location=get_site_location(),
)
coord_icrs = coord_altaz.transform_to("icrs")
logger.debug("Calculated ICRS (RA, DEC) = ({}, {}) [deg]".format(coord_icrs.ra.deg, coord_icrs.dec.deg))

# Get the RA, DEC values from SkyCoord as floats.  Set all other properties to 0.
ra_catalog_icrs = coord_icrs.ra.deg
dec_catalog_icrs = coord_icrs.dec.deg
tracking_params = TrackingParamsEQ(ra_catalog_icrs, dec_catalog_icrs)

add_scan(schedule_params, scantype_params, tracking_params)

results = run_queue()
log("all results: {}".format(results))

# ------------------------------------------
# Test 2 Go to same starting position in region AZ > 240
# ------------------------------------------
goto_azel(241, 45)

# Choose an AZ , EL in the region > 60
az = 61 * units.deg # should NOT shortcut. Wrap back to +61
el = 45 * units.deg 
logger.debug("Choose a coordinate above the horizon now. (AZ, EL) = ({}, {}) [deg]".format(az.value, el.value))

# Transform the AZ, EL coordinate to ICRS that will be used to configure the tracking.
coord_altaz = SkyCoord(
    frame="altaz",
    az=az,
    alt=el,
    obstime=Time.now(),
    location=get_site_location(),
)
coord_icrs = coord_altaz.transform_to("icrs")
logger.debug("Calculated ICRS (RA, DEC) = ({}, {}) [deg]".format(coord_icrs.ra.deg, coord_icrs.dec.deg))

# Get the RA, DEC values from SkyCoord as floats.  Set all other properties to 0.
ra_catalog_icrs = coord_icrs.ra.deg
dec_catalog_icrs = coord_icrs.dec.deg
tracking_params = TrackingParamsEQ(ra_catalog_icrs, dec_catalog_icrs)

add_scan(schedule_params, scantype_params, tracking_params)

results = run_queue()
log("all results: {}".format(results))

# ------------------------------------------
# Test 3: Go to starting position in region AZ < 120
# ------------------------------------------
goto_azel(119, 45)

# Choose an AZ , EL in the region > 300
az = 301 * units.deg # should shortcut to -59
el = 45 * units.deg 
logger.debug("Choose a coordinate above the horizon now. (AZ, EL) = ({}, {}) [deg]".format(az.value, el.value))

# Transform the AZ, EL coordinate to ICRS that will be used to configure the tracking.
coord_altaz = SkyCoord(
    frame="altaz",
    az=az,
    alt=el,
    obstime=Time.now(),
    location=get_site_location(),
)
coord_icrs = coord_altaz.transform_to("icrs")
logger.debug("Calculated ICRS (RA, DEC) = ({}, {}) [deg]".format(coord_icrs.ra.deg, coord_icrs.dec.deg))

# Get the RA, DEC values from SkyCoord as floats.  Set all other properties to 0.
ra_catalog_icrs = coord_icrs.ra.deg
dec_catalog_icrs = coord_icrs.dec.deg
tracking_params = TrackingParamsEQ(ra_catalog_icrs, dec_catalog_icrs)

add_scan(schedule_params, scantype_params, tracking_params)

results = run_queue()
log("all results: {}".format(results))

# ------------------------------------------
# Test 4 Go to same starting position in region AZ < 120
# ------------------------------------------
goto_azel(119, 45)

# Choose an AZ , EL in the region < 300 
az = 299 * units.deg # should NOT shortcut. Wrap around to +299
el = 45 * units.deg 
logger.debug("Choose a coordinate above the horizon now. (AZ, EL) = ({}, {}) [deg]".format(az.value, el.value))

# Transform the AZ, EL coordinate to ICRS that will be used to configure the tracking.
coord_altaz = SkyCoord(
    frame="altaz",
    az=az,
    alt=el,
    obstime=Time.now(),
    location=get_site_location(),
)
coord_icrs = coord_altaz.transform_to("icrs")
logger.debug("Calculated ICRS (RA, DEC) = ({}, {}) [deg]".format(coord_icrs.ra.deg, coord_icrs.dec.deg))

# Get the RA, DEC values from SkyCoord as floats.  Set all other properties to 0.
ra_catalog_icrs = coord_icrs.ra.deg
dec_catalog_icrs = coord_icrs.dec.deg
tracking_params = TrackingParamsEQ(ra_catalog_icrs, dec_catalog_icrs)

add_scan(schedule_params, scantype_params, tracking_params)

results = run_queue()
log("all results: {}".format(results))

# ------------------------------------------
# Test 5 Go to start position in  region 120 < AZ < 240
# ------------------------------------------
goto_azel(121, 45)

# Choose an AZ , EL in the region > 300 
az = 359 * units.deg # should NOT shortcut
el = 45 * units.deg 
logger.debug("Choose a coordinate above the horizon now. (AZ, EL) = ({}, {}) [deg]".format(az.value, el.value))

# Transform the AZ, EL coordinate to ICRS that will be used to configure the tracking.
coord_altaz = SkyCoord(
    frame="altaz",
    az=az,
    alt=el,
    obstime=Time.now(),
    location=get_site_location(),
)
coord_icrs = coord_altaz.transform_to("icrs")
logger.debug("Calculated ICRS (RA, DEC) = ({}, {}) [deg]".format(coord_icrs.ra.deg, coord_icrs.dec.deg))

# Get the RA, DEC values from SkyCoord as floats.  Set all other properties to 0.
ra_catalog_icrs = coord_icrs.ra.deg
dec_catalog_icrs = coord_icrs.dec.deg
tracking_params = TrackingParamsEQ(ra_catalog_icrs, dec_catalog_icrs)

add_scan(schedule_params, scantype_params, tracking_params)

results = run_queue()
log("all results: {}".format(results))

# ------------------------------------------
# Test 6 Go to start position in  region 120 < AZ < 240
# ------------------------------------------
goto_azel(239, 45)

# Choose an AZ , EL in the region < 60 
az = 1 * units.deg # should NOT shortcut
el = 45 * units.deg 
logger.debug("Choose a coordinate above the horizon now. (AZ, EL) = ({}, {}) [deg]".format(az.value, el.value))

# Transform the AZ, EL coordinate to ICRS that will be used to configure the tracking.
coord_altaz = SkyCoord(
    frame="altaz",
    az=az,
    alt=el,
    obstime=Time.now(),
    location=get_site_location(),
)
coord_icrs = coord_altaz.transform_to("icrs")
logger.debug("Calculated ICRS (RA, DEC) = ({}, {}) [deg]".format(coord_icrs.ra.deg, coord_icrs.dec.deg))

# Get the RA, DEC values from SkyCoord as floats.  Set all other properties to 0.
ra_catalog_icrs = coord_icrs.ra.deg
dec_catalog_icrs = coord_icrs.dec.deg
tracking_params = TrackingParamsEQ(ra_catalog_icrs, dec_catalog_icrs)

add_scan(schedule_params, scantype_params, tracking_params)

results = run_queue()
log("all results: {}".format(results))
