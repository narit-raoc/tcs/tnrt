delay_until_scheduled_start = 5
project_id = "test_pulsar_script"
obs_id = "SS"
start_mjd = (Time.now() + delay_until_scheduled_start * units.second).mjd
source_name = "B0332+5434"
schedule_params = ScheduleParams(
    project_id,
    obs_id,
    start_mjd=start_mjd,
    source_name=source_name,
)


duration = 60  # second
scantype_params = ScantypeParamsOnSource(duration)

# Tracking Horizontal
az = 50
el = 45
tracking_params = TrackingParamsHO(az, el)

backend_params = BackendParamsEDD('TEST_jp_pulsar_L', project="test_jp_pulsar1", source_name="B0332+5434", ra="03h32m59.4096" , dec="+54d34m43.329s")
add_scan(schedule_params, scantype_params, tracking_params, backend_params=backend_params)

results = run_queue()

results = stop_queue()
