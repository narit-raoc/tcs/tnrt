delay_until_scheduled_start = 10
project_id = "test script"
obs_id = "SS"
start_mjd = (Time.now() + delay_until_scheduled_start * units.second).mjd
source_name = "AAA"
line_name = "BBB"
schedule_params = ScheduleParams(
    project_id,
    obs_id,
    start_mjd=start_mjd,
    source_name=source_name,
    line_name=line_name,
)


duration = 60  # second
scantype_params = ScantypeParamsOnSource(duration)

tracking_params = TrackingParamsSS("sun", 40, 40)

backend_params = BackendParamsHoloFFT()

add_scan(schedule_params, scantype_params, tracking_params, backend_params=backend_params)
results = run_queue()

results = stop_queue()
