# -*- coding: utf-8 -*-
import argparse
from argparse import RawTextHelpFormatter
import numpy as np
import pyqtgraph as pg
from pyqtgraph.Qt import QtWidgets

from astropy import units
from astropy.coordinates import EarthLocation
from astropy.time import Time
from astropy.time import TimeDelta
from astropy.utils import iers

from astropy.coordinates import Longitude
from astropy.coordinates import Latitude

from coordinate_utility import icrs_altaz_astropy
from coordinate_utility import icrs_altaz_iau
from coordinate_utility import icrs_to_geocentric_apparent_iau
from RaDecToAzEl import FB_RaDec2AzEl

# Constant
SECONDS_PER_DAY = 86400
MILLISECONDS_PER_DAY = 1000 * SECONDS_PER_DAY

# NOTE: Default is dark background and CMY colors. Set white_background=True
# if you want white background and RGB colors.
white_background = False
# white_background = True

if white_background is True:
    pg.setConfigOption("background", "w")
    pg.setConfigOption("foreground", "k")

    # Use RGB color for white background
    color1 = "b"
    color2 = "r"
    color3 = "g"
    color4 = "k"
else:
    # Use CMY color for dark background
    color1 = "y"
    color2 = "m"
    color3 = "c"
    color4 = "r"

symbol1 = "o"

size1 = 18
size2 = 12
size3  = 6

# Setup rules for command line arguments
parser = argparse.ArgumentParser(
    description="Simulate AZ, EL tracking solution for RA, DEC coordinates using various algorithms",
    formatter_class=RawTextHelpFormatter,
)
parser.add_argument(
    "-n",
    "--npoints",
    type=int,
    default=50,
    help="number of data items to create (lines in program track table)",
)
parser.add_argument(
    "-u",
    "--duration",
    type=float,
    default=300.0,
    help="time duration of the tracking table [seconds]",
)
parser.add_argument(
    "-r",
    "--ra_icrs",
    type=float,
    default=100.0,
    help="Right Ascension from catalog in ICRS coordinate system",
)
parser.add_argument(
    "-d",
    "--dec_icrs",
    type=float,
    default=30.0,
    help="Declination from catalog ICRS coordinate system",
)

parser.add_argument(
    "-s",
    "--pt_starttime",
    type=str,
    default=0,
    help="MJD of program track start time",
)
parser.add_argument(
    "-o",
    "--sim_time_offset",
    type=float,
    default=0,
    help="Time offset from pt_starttime to of the first simulated timestamp.  Can be (-) or (+)",
)

# Read command line arguments into argparse structure
args = parser.parse_args()
print("")
print("------------------------------------------------------------------------")
print("Inputs to Coordinate Generator")
print("------------------------------------------------------------------------")
print("Npoints [integer]: {}".format(args.npoints))
print("Duration [s]: {} ".format(args.duration))
print("RA catalog ICRS [deg]: {}".format(args.ra_icrs))
print("DEC catalog ICRS [deg]: {}".format(args.dec_icrs))
print("Time offset from now of start time [s]: {}".format(args.sim_time_offset))

# Define the EarthLocation at TNRT site
print("")
print("------------------------------------------------------------------------")
print("Site Location (constant)")
print("------------------------------------------------------------------------")
loc = EarthLocation(
    EarthLocation.from_geodetic(
        lon=99.216805 * units.deg,
        lat=18.864348 * units.deg,
        height=403.625 * units.m,
        ellipsoid="WGS84",
    )
)

print("Site Location Latitude [deg]: {}".format(loc.lat.value))
print("Site Location Longitude [deg]: {}".format(loc.lon.value))
print("Site Location Height [m]: {}".format(loc.height.value))

print("")
print("------------------------------------------------------------------------")
print("Generate time steps")
print("------------------------------------------------------------------------")
# If program track start time is not selected, use current time now
# Else parse the ISO time string anc create a time object from that.
if args.pt_starttime == 0:
    starttime_tracking = Time.now()
else:
    starttime_tracking = Time(args.pt_starttime, format="isot")

# Simulated time steps are relative to the program track start time 
# (which could be now or specified by parameter).
start_timesteps_mjd = (starttime_tracking + args.sim_time_offset * units.s).mjd
end_mjd = start_timesteps_mjd + args.duration / SECONDS_PER_DAY
(tt_mjd, tstep_days) = np.linspace(
    start_timesteps_mjd, end_mjd, args.npoints, endpoint=True, retstep=True
)
time_utc = Time(tt_mjd, format="mjd")

print("Start time now MJD: {}".format(start_timesteps_mjd))
print("End time MJD: {}".format(start_timesteps_mjd))
print("Time increment [s]: {} ".format(tstep_days * SECONDS_PER_DAY))
print("Time UTC MDJ: {}".format(time_utc))

print("")
print("------------------------------------------------------------------------")
print("Get Earth Orientation Parameters (EOP) from IERS at tracking start time")
print("Note: Use these values of EOP for the duration of this tracking operation")
print("------------------------------------------------------------------------")
iers_table = iers.IERS_Auto.open()
# Note: IERS ut1_utc function returns dut1 in seconds
# Create a "smart" Astropy Time object to simplify unit conversions
dut1 = TimeDelta(iers_table.ut1_utc(time_utc[0].jd), format="sec")
(polar_motion_x, polar_motion_y) = iers_table.pm_xy(time_utc[0].jd)  # arcsec
print("DUT1 (UT1-UTC) [s]: {}".format(dut1.sec))
print("Polar motion X [arcsec]: {}".format(polar_motion_x.value))
print("Polar motion Y [arcsec]: {}".format(polar_motion_y.value))

print("")
print("------------------------------------------------------------------------")
print("Calculate GST0hUT1 for legacy ACU tracking model")
print("GST0hUT1 = Greenwich Apparent Sidereal Time at 0:00 UT1")
print("Usually, sidereal time is expressed as a rotation angle.")
print("It is periodic: 2*pi radians, 360 deg, or 24 hourangle")
print("Translate to milliseconds of day. Periodic {} ms".format(MILLISECONDS_PER_DAY))
print("------------------------------------------------------------------------")

# Convert UTC time to UT1, then get integer part of the UT1 day.
# Note:
# MJD increments the day number +1 at midnight (0h)
# JD increments the day number at noon (12h)
# UT1 day increment follows the same convention as MJD. Therefore we must use integer
# of the MJD, *not* the JD (offset by 12 hours)
starttime_ut1 = starttime_tracking.ut1
starttime_ut1_integer_day = int(starttime_ut1.mjd)
print("Start time UT1 integer day: {}".format(starttime_ut1_integer_day))

startday_0hUT1 = Time(starttime_ut1_integer_day, format="mjd", scale="ut1")
GST0hUT1 = startday_0hUT1.sidereal_time("apparent", longitude="greenwich")

# Create a new astropy Time object of fractional day on UT1 time scale
GST0hUT1_ms = MILLISECONDS_PER_DAY * (GST0hUT1.hourangle / 24.0)

print("GST0hUT1 [hourangle]: {}".format(GST0hUT1.hourangle))
print("GST0hUT1 [ms]: {}".format(GST0hUT1_ms))

# Create Astropy "smart" angle objects to simplify my code
ra_icrs = Longitude(args.ra_icrs, unit=units.deg)
dec_icrs = Latitude(args.dec_icrs, unit=units.deg)

# # Transform the coorinates using 3 different methods
print("")
print("------------------------------------------------------------------------")
print("Astropy transform ICRS -> Observed AltAz ...")
print("------------------------------------------------------------------------")
(az_astropy, alt_astropy) = icrs_altaz_astropy(ra_icrs, dec_icrs, loc, time_utc)

print("AZ: {}".format(az_astropy))
print("")
print("EL: {}".format(alt_astropy))

print("")
print("------------------------------------------------------------------------")
print("IAU SOFA transform ICRS -> Observed AltAz (atco13) ...")
print("------------------------------------------------------------------------")
(az_iauAtco13, alt_iauAtco13) = icrs_altaz_iau(
    ra_icrs,
    dec_icrs,
    loc,
    time_utc,
    dut1,
    polar_motion_x,
    polar_motion_y,
)

print("AZ: {}".format(az_iauAtco13))
print("")
print("EL: {}".format(alt_iauAtco13))

print("")
print("------------------------------------------------------------------------")
print("IAU SOFA transform ICRS -> Geocentric Apparent -> ... (prepare for ACU input) ")
print("Sidereal Time -> Observed AltAz (predict ACU output)")
print("Note: This algorithm does not adjust for Polar Motion from IERS EOP")
print("------------------------------------------------------------------------")
# Transform ICRS RA DEC to Geocentric Apparent so we can use the sidereal time method.
# Using the legacy ACU tracking algorithm, this step is required by the TCS user
# before we load RA DEC into the ACU tracking table.
(ra_geocentric_apparent, dec_geocentric_apparent) = icrs_to_geocentric_apparent_iau(ra_icrs, dec_icrs, time_utc)
print("")
print("ra_geocentric_apparent [deg]: {}".format(ra_geocentric_apparent))
print("dec_geocentric_apparent [deg]: {}".format(dec_geocentric_apparent))

(az_manual, alt_manual, gst) = FB_RaDec2AzEl(
    time_utc.mjd,
    starttime_tracking.mjd,
    1000 * dut1.sec,
    GST0hUT1_ms,
    ra_geocentric_apparent.deg,
    dec_geocentric_apparent.deg,
    loc.lon.deg,
    loc.lat.deg,
    False,
    )

print("AZ: {}".format(az_manual))
print("EL: {}".format(alt_manual))

# -----------------------------------------------------------------------------
# Save the csv file
# -----------------------------------------------------------------------------
# Note: some data items are constant throughout the observation, so use numpy.full
# to fill the entire column with the same constant value
# N = args.npoints
# export_data_array = np.array(
#     [
#         time_utc.mjd,
#         np.full(N, ra_icrs.deg),
#         np.full(N, dec_icrs.deg),
#         ra_geocentric_apparent_iauAtci13,
#         dec_geocentric_apparent_iauAtci13,
#         np.full(N, loc.lat.value),
#         np.full(N, loc.lon.value),
#         np.full(N, loc.height.value),
#         np.full(N, 1000 * dut1.sec),  # convert seconds to milliseconds
#         np.full(N, polar_motion_x.value),
#         np.full(N, polar_motion_y.value),
#         np.full(N, GST0hUT1_ms),  # Note: not rounded to integer milliseconds
#         az_manual,
#         alt_manual,
#         az_iauAtco13,
#         alt_iauAtco13,
#         az_astropy,
#         alt_astropy,
#     ]
# )
# fname = "acu_tracking_calc.csv"
# print("writing file: {}".format(fname))
# np.savetxt(
#     fname,
#     export_data_array.T,
#     delimiter=",",
#     header="TIME_utc_mjd,\
#     RA_ICRS_deg,\
#     DEC_ICRS_deg,\
#     RA_apparent_deg,\
#     DEC_apparent_deg,\
#     LOCATION_latitude_deg,\
#     LOCATION_logntidue_deg,\
#     LOCATION_height_m,\
#     EOP_dut1_ms,\
#     EOP_polar_motion_x_arcsec,\
#     EOP_polar_motion_y_arcsec,\
#     GST0hUT1_ms,\
#     AZ_manual_deg,\
#     EL_manual_deg,\
#     AZ_iauAtco13_deg,\
#     EL_iauAtco13_deg,\
#     AZ_Astropy_deg,\
#     EL_Astropy_deg",
# )
# -----------------------------------------------------------------------------
# Calculate Differences between solutions in arcseconds
# -----------------------------------------------------------------------------
# Between iauAtco13 and Manual (reproduce legacy ACU)
d_az_iauAtco13_manual = 3600 * (az_manual - az_iauAtco13)
d_el_iauAtco13_manual = 3600 * (alt_manual - alt_iauAtco13)

# Between Astropy SkyCoord transform and Manual (reproduce legacy ACU)
d_az_astropy_manual = 3600 * (az_manual - az_astropy)
d_el_astropy_manual = 3600 * (alt_manual - alt_astropy)

# Between Astropy SkyCoord transform and iauAtco13
d_az_astropy_iauAtco13 = 3600 * (az_iauAtco13 - az_astropy)
d_el_astropy_iauAtco13 = 3600 * (alt_iauAtco13 - alt_astropy )

# -----------------------------------------------------------------------------
# Make Graphs
# -----------------------------------------------------------------------------
grid_opacity = 0.7
app = QtWidgets.QApplication([])
str_title = "<font>Tracking Simulation: ra_icrs: {} [deg], dec_icrs: {} [deg], dut1: {} [ms]<br>npoints: {}, duration: {} [s], sim_time_offset: {} [s], pt_start_time: {} [UTC]</font>".format(
    args.ra_icrs, args.dec_icrs, 1000*dut1.sec,
    args.npoints, args.duration, args.sim_time_offset, starttime_tracking.isot,
)

# Time axis data
time_unix = time_utc.unix

# -----------------------------------------------------------------------------
# Graph window 1
# -----------------------------------------------------------------------------
glw_abs = pg.GraphicsLayoutWidget(show=True)
glw_abs.resize(1024, 768)
glw_abs.setWindowTitle("Tracking Simulation")
pg.setConfigOptions(antialias=True)

pi_az_time = glw_abs.addPlot(0, 0, title=str_title)
pi_az_time.addLegend()
pi_az_time.plot(time_unix, az_manual, name="ACU sidereal", pen=None, symbol=symbol1, symbolBrush=None, symbolPen=color1, symbolSize=size1)
pi_az_time.plot(time_unix, az_iauAtco13, name="IAU SOFA atco13", pen=None, symbol=symbol1, symbolBrush=None, symbolPen=color2, symbolSize=size2)
pi_az_time.plot(time_unix, az_astropy, name="Astropy ICRS to AltAz", pen=None, symbol=symbol1, symbolBrush=None, symbolPen=color3, symbolSize=size3)
ax_L = pg.AxisItem(orientation="left", text="AZ", units="deg")
ax_B = pg.DateAxisItem(orientation="bottom", utcOffset=0,text="UTC")
ax_L.showLabel(True)
ax_B.showLabel(True)
pi_az_time.setAxisItems(axisItems={"bottom": ax_B, "left" : ax_L})
pi_az_time.showGrid(True, True, alpha=grid_opacity)

# Plot EL vs Time
pi_el_time = glw_abs.addPlot(1, 0)
pi_el_time.addLegend()
pi_el_time.plot(time_unix, alt_manual, name="ACU sidereal", pen=None, symbol=symbol1, symbolBrush=None, symbolPen=color1, symbolSize=size1)
pi_el_time.plot(time_unix, alt_iauAtco13, name="IAU SOFA atco13", pen=None, symbol=symbol1, symbolBrush=None, symbolPen=color2, symbolSize=size2)
pi_el_time.plot(time_unix, alt_astropy, name="Astropy ICRS to AltAz", pen=None, symbol=symbol1, symbolBrush=None, symbolPen=color3, symbolSize=size3)
ax_L = pg.AxisItem(orientation="left", text="EL", units="deg")
ax_B = pg.DateAxisItem(orientation="bottom", utcOffset=0,text="UTC")
ax_L.showLabel(True)
ax_B.showLabel(True)
pi_el_time.setAxisItems(axisItems={"bottom": ax_B, "left" : ax_L})
pi_el_time.showGrid(True, True, alpha=grid_opacity)

# Plot GST vs Time
pi_gst_time = glw_abs.addPlot(2, 0)
pi_gst_time.addLegend()
pi_gst_time.plot(time_unix, gst, pen=None, symbol=symbol1, symbolBrush=None, symbolPen=color4, symbolSize=size3)
ax_L = pg.AxisItem(orientation="left", text="GST Hour Angle", units="hour")
ax_B = pg.DateAxisItem(orientation="bottom", utcOffset=0,text="UTC")
ax_L.showLabel(True)
ax_B.showLabel(True)
pi_gst_time.setAxisItems(axisItems={"bottom": ax_B, "left" : ax_L})
pi_gst_time.showGrid(True, True, alpha=grid_opacity)


# -----------------------------------------------------------------------------
# Graph window 2 - Differences
# -----------------------------------------------------------------------------
glw_diff = pg.GraphicsLayoutWidget(show=True)
glw_diff.resize(1024, 768)
glw_diff.setWindowTitle("Difference Between Algorithms")
pg.setConfigOptions(antialias=True)

# Plot diff AZ vs Time
pi_d_az_time = glw_diff.addPlot(0, 0, title=str_title)
pi_d_az_time.addLegend()
pi_d_az_time.plot(time_unix, d_az_iauAtco13_manual, name="ACU Sidereal - iauAtco13", pen=None, symbol=symbol1, symbolBrush=None, symbolPen=color2, symbolSize=size2)
pi_d_az_time.plot(time_unix, d_az_astropy_manual, name="ACU Sidereal - Astropy AltAz", pen=None, symbol=symbol1, symbolBrush=None, symbolPen=color3, symbolSize=size3)
ax_L = pg.AxisItem(orientation="left", text="AZ", units="arcsec")
ax_B = pg.DateAxisItem(orientation="bottom", utcOffset=0,text="UTC")
ax_L.showLabel(True)
ax_B.showLabel(True)
pi_d_az_time.setAxisItems(axisItems={"bottom": ax_B, "left" : ax_L})
pi_d_az_time.showGrid(True, True, alpha=grid_opacity)

# Plot diff EL vs Time
pi_d_el_time = glw_diff.addPlot(1, 0)
pi_d_el_time.addLegend()
pi_d_el_time.plot(time_unix, d_el_iauAtco13_manual, name="ACU Sidereal - iauAtco13", pen=None, symbol=symbol1, symbolBrush=None, symbolPen=color2, symbolSize=size2)
pi_d_el_time.plot(time_unix, d_el_astropy_manual, name="ACU Sidereal - Astropy AltAz", pen=None, symbol=symbol1, symbolBrush=None, symbolPen=color3, symbolSize=size3)
ax_L = pg.AxisItem(orientation="left", text="EL", units="arcsec")
ax_B = pg.DateAxisItem(orientation="bottom", utcOffset=0,text="UTC")
ax_L.showLabel(True)
ax_B.showLabel(True)
pi_d_el_time.setAxisItems(axisItems={"bottom": ax_B, "left" : ax_L})
pi_d_el_time.showGrid(True, True, alpha=grid_opacity)


# Start the Qt event loop and block this script until user closes the GUI window
app.exec_()
