import numpy as np
import erfa # https://pypi.org/project/pyerfa/
from astropy.constants import c as speed_of_light

# For graph
import pyqtgraph as pg
from pyqtgraph.Qt import QtWidgets

# L-band frequency
f = 1.8e9

phpa = 1010
tc = 35
rh = 0.6
wl = speed_of_light.value / f

r0 = 75 # arcseconds
b1 = 5.7346 # no unit
b2 = 2.2874 # no unit


def get_el_correction_iau(el_deg):
    """
    From Page 91 of sofa_ast_c_2023.pdf (Sofa Astrometry Tools)
    http://www.iausofa.org/sofa_ast_c.pdf

    Determine the constants A and B in the atmospheric refraction model
    dZ = A tan Z + B tan^3 Z.

    Z is the "observed" zenith angle (i.e. affected by refraction)
    and dZ is what to add to Z to give the "topocentric" (i.e. in vacuo)
    zenith angle.

    This function is part of the International Astronomical Union’s
    SOFA (Standards of Fundamental Astronomy) software collection.

    Status: support function.

    Given:

    phpa  double pressure at the observer (hPa = millibar)
    tc    double ambient temperature at the observer (deg C)
    rh    double relative humidity at the observer (range 0−1)
    wl    double wavelength (micrometers)

    Returned:

    refa  double tan Z coefficient (radians)
    refb  double tan^3 Z coefficient (radians)
    """
    (refa, refb) = erfa.refco(phpa, tc, rh, wl)
    zenith_angle = np.radians(90-el_deg)
    dZ_radian = refa * np.tan(zenith_angle) + refb * (np.tan(zenith_angle)) ** 3
    dZ_arcsec = 3600*np.degrees(dZ_radian)
    return dZ_arcsec

def get_el_correction_ohb(el_deg):
    """
    From report "Correction of Atmospheric Refraction for Thai National
    Radio Telescope (TNRT).  Nikom Prasert, 2023
    """
    dZ_arcsec = r0 * np.abs(np.tan(np.radians((90 - el_deg - (b1 / (el_deg + b2))))))
    return dZ_arcsec


el_deg = np.arange(5, 90, 1)

dZ_iau = get_el_correction_iau(el_deg)
dZ_ohb = get_el_correction_ohb(el_deg)

print("dZ_iau [arcsec]: {}".format(dZ_iau))
print("dZ_ohb [arcsec]: {}".format(dZ_ohb))

# Show graphs
grid_opacity = 0.7
app = QtWidgets.QApplication([])
pw = pg.plot([], [])
pw.addLegend()
pw.plot(el_deg, dZ_iau, name="IAU. phpa: {}, tc: {}, rh: {}, wl: {:.3f}".format(phpa, tc, rh, wl), pen="c")
pw.plot(el_deg, dZ_ohb, name="TNRT. r0: {}, b1: {}, b2: {}".format(r0, b1, b2), pen="y")
axis_bottom = pg.AxisItem(orientation="bottom", text="EL Angle", units="deg")
axis_bottom.showLabel(True)
axis_left = pg.AxisItem(orientation="left", text="Refraction Correction", units="arcsec")
axis_left.showLabel(True)
pw.setAxisItems(axisItems={"bottom": axis_bottom,"left": axis_left,})
pw.showGrid(True, True, alpha=grid_opacity)

# Start the Qt event loop and block this script until user closes the GUI window
app.exec()
