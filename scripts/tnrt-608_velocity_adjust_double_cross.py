project_id = "no_projid"
obs_id = "no_obsid"
schedule_params = ScheduleParams(project_id, obs_id, source_name="sim_source", line_name="sim_line")
backend_params = BackendParamsMock("TNRT_dualpol_spectrometer", integration_time=1, freq_res=250000)

# --------
# Tracking
# --------
# Choose an AZ , EL above the horizon in the region that we want to test tracking.
az = 150 * units.deg
el = 45 * units.deg
log(
    "Choose a coordinate above the horizon now. (AZ, EL) = ({}, {}) [deg]".format(
        az.value, el.value
    )
)

# Transform the AZ, EL coordinate to ICRS that will be used to configure the tracking.
coord_altaz = SkyCoord(
    frame="altaz",
    az=az,
    alt=el,
    obstime=Time.now(),
    location=get_site_location(),
)
coord_icrs = coord_altaz.transform_to("icrs")
log(
    "Calculated ICRS (RA, DEC) = ({}, {}) [deg]".format(
        coord_icrs.ra.deg, coord_icrs.dec.deg
    )
)

# Get the RA, DEC values from SkyCoord as floats.  Set all other properties to 0.
ra_catalog_icrs = coord_icrs.ra.deg
dec_catalog_icrs = coord_icrs.dec.deg

tracking_params = TrackingParamsEQ(ra_catalog_icrs, dec_catalog_icrs)


# ---------------------------------------
# Scan type - single horizontal line scan
# ---------------------------------------
arm_length = 10560
time_per_arm = 30
scantype_params = ScantypeParamsCross(arm_length, time_per_arm, double_cross=True)

set_velocity("az", 2)
set_velocity("el", 1)
add_scan(schedule_params, scantype_params, tracking_params, backend_params=backend_params)
results = run_queue()

# Change velocity to half and run again
set_velocity("az", 1)
set_velocity("el", 0.5)
add_scan(schedule_params, scantype_params, tracking_params, backend_params=backend_params)
results = run_queue()

# Change velocity to 0.25 and run again
set_velocity("az", 0.5)
set_velocity("el", 0.25)
add_scan(schedule_params, scantype_params, tracking_params, backend_params=backend_params)
results = run_queue()

# Set velocity back to the default maximum value after finished.  This is not stored in ACU.
# only stored in TCS Antenna component.
set_velocity("az", 2)
set_velocity("el", 1)

# Show the results of all scans from this run to the terminal window.
# All types of scans have some common results.  For example: a list of filenames
# that were recorded.
log("all results: {}".format(results))
