import argparse
from argparse import RawTextHelpFormatter
import os
import sys
import numpy as np
import matplotlib
matplotlib.use("QT5agg")
import matplotlib.pyplot as plt
plt.style.use('dark_background')
from astropy.io import fits

helpstring = "Expected MBFITS file: See DataFormatMbfits.py"

# Setup rules for command line arguments
parser = argparse.ArgumentParser(description='Reads FITS file written by PipelineMbfits and diplays some graphs',
									epilog=helpstring, formatter_class=RawTextHelpFormatter)
parser.add_argument('filename', help='FITS file with format specified by AntennaDataFormat.py')

# If no command line arguments are avaiable, show help message and exit
if len(sys.argv[1:]) == 0:
	parser.print_help()
	exit()

# Read command line arguments into argparse structure
args = parser.parse_args()

hdul = None

if(os.path.isfile(args.filename) is not True):
	print('ERROR: File not found')
	print('os.path.isfile(%s) = False' % args.filename)
	hdul = None
else:
	print('Processing file %s: ' % args.filename)
	fid = open(args.filename, 'rb')
	hdul = fits.open(fid)
	# leave the file open becuase fits.open will not store the entire file in RAM if it is
	# too big.  Close file at the end of this script.
	#print(hdul.info())
if(hdul is None):
	exit()

# -----------------------------------------------------------------------------
# Make Graphs
# -----------------------------------------------------------------------------
# Strings for labels
str_title = 'From File %s' % os.path.basename(args.filename)
grid_opacity = 0.2

# Array index for axes that have stow pins
AZ = 0  # has 1 stow pin
EL = 1 # has 2 stow pins
THU = 6 # has 2 stow pins
GRS = 7 # has 1 stow pin

# Selected bits from BIT MODE CODED STOW STATUS (MTM ICD 6.5.2)
STOW_PIN1_EXTR = 1 << 4 # stow pin extracted, axis locked
STOW_PIN1_RETR = 1 << 5 # stow pin retrieved, axis unlocked
STOW_PIN2_EXTR = 1 << 8 # stow pin extracted, axis locked
STOW_PIN2_RETR = 1 << 9 # stow pin retrieved, axis unlocked

# Make a time axis for all the plots.  Use seconds after start
tstart = hdul['MONITOR-ANT-PACKET'].data['time_tcs_mjd'][0]
tt = (hdul['MONITOR-ANT-PACKET'].data['time_tcs_mjd'] - tstart) * 86400.0

# Linear axes
fig1 = plt.figure('Linear Axes', figsize=(8,10))

fig1_ax1 = fig1.add_subplot(6, 1, 1)
fig1_ax1.set_title(str_title)
fig1_ax1.set_ylabel('False | True')
fig1_ax1.grid(True, which="both", alpha=grid_opacity)
az_stowpin1_locked = (hdul['MONITOR-ANT-AXIS'].data['stow_status'][:,AZ] & STOW_PIN1_EXTR) / STOW_PIN1_EXTR
az_stowpin1_unlocked = (hdul['MONITOR-ANT-AXIS'].data['stow_status'][:,AZ] & STOW_PIN1_RETR) / STOW_PIN1_RETR
fig1_ax1.plot(tt, az_stowpin1_locked, label='AZ stowpin1 locked')
fig1_ax1.plot(tt, az_stowpin1_unlocked, label='AZ stowpin1 unlocked')
fig1_ax1.legend()

fig1_ax2 = fig1.add_subplot(6, 1, 2)
fig1_ax2.set_ylabel('False | True')
fig1_ax2.grid(True, which="both", alpha=grid_opacity)
el_stowpin1_locked = (hdul['MONITOR-ANT-AXIS'].data['stow_status'][:,EL] & STOW_PIN1_EXTR) / STOW_PIN1_EXTR
el_stowpin1_unlocked = (hdul['MONITOR-ANT-AXIS'].data['stow_status'][:,EL] & STOW_PIN1_RETR) / STOW_PIN1_RETR
fig1_ax2.plot(tt, el_stowpin1_locked, label='EL stowpin1 locked')
fig1_ax2.plot(tt, el_stowpin1_unlocked, label='EL stowpin1 unlocked')
fig1_ax2.legend()

fig1_ax3 = fig1.add_subplot(6, 1, 3)
fig1_ax3.set_ylabel('False | True')
fig1_ax3.grid(True, which="both", alpha=grid_opacity)
el_stowpin2_locked = (hdul['MONITOR-ANT-AXIS'].data['stow_status'][:,EL] & STOW_PIN2_EXTR) / STOW_PIN2_EXTR
el_stowpin2_unlocked = (hdul['MONITOR-ANT-AXIS'].data['stow_status'][:,EL] & STOW_PIN2_RETR) / STOW_PIN2_RETR
fig1_ax3.plot(tt, el_stowpin2_locked, label='EL stowpin2 locked')
fig1_ax3.plot(tt, el_stowpin2_unlocked, label='EL stowpin2 unlocked')
fig1_ax3.legend()

fig1_ax4 = fig1.add_subplot(6, 1, 4)
fig1_ax4.set_ylabel('False | True')
fig1_ax4.grid(True, which="both", alpha=grid_opacity)
thu_stowpin1_locked = (hdul['MONITOR-ANT-AXIS'].data['stow_status'][:,THU] & STOW_PIN1_EXTR) / STOW_PIN1_EXTR
thu_stowpin1_unlocked = (hdul['MONITOR-ANT-AXIS'].data['stow_status'][:,THU] & STOW_PIN1_RETR) / STOW_PIN1_RETR
fig1_ax4.plot(tt, thu_stowpin1_locked, label='THU stowpin1 locked')
fig1_ax4.plot(tt, thu_stowpin1_unlocked, label='THU stowpin1 unlocked')
fig1_ax4.legend()

fig1_ax5 = fig1.add_subplot(6, 1, 5)
fig1_ax5.set_ylabel('False | True')
fig1_ax5.grid(True, which="both", alpha=grid_opacity)
thu_stowpin2_locked = (hdul['MONITOR-ANT-AXIS'].data['stow_status'][:,THU] & STOW_PIN2_EXTR) / STOW_PIN2_EXTR
thu_stowpin2_unlocked = (hdul['MONITOR-ANT-AXIS'].data['stow_status'][:,THU] & STOW_PIN2_RETR) / STOW_PIN2_RETR
fig1_ax5.plot(tt, thu_stowpin2_locked, label='THU stowpin2 locked')
fig1_ax5.plot(tt, thu_stowpin2_unlocked, label='THU stowpin2 unlocked')
fig1_ax5.legend()

fig1_ax6 = fig1.add_subplot(6, 1, 6)
fig1_ax6.set_ylabel('False | True')
fig1_ax6.grid(True, which="both", alpha=grid_opacity)
grs_stowpin1_locked = (hdul['MONITOR-ANT-AXIS'].data['stow_status'][:,GRS] & STOW_PIN1_EXTR) / STOW_PIN1_EXTR
grs_stowpin1_unlocked = (hdul['MONITOR-ANT-AXIS'].data['stow_status'][:,GRS] & STOW_PIN1_RETR) / STOW_PIN1_RETR
fig1_ax6.plot(tt, grs_stowpin1_locked, label='GRS stowpin1 locked')
fig1_ax6.plot(tt, grs_stowpin1_unlocked, label='GRS stowpin1 unlocked')
fig1_ax6.legend()

# Close the file
fid.close()

plt.show()
