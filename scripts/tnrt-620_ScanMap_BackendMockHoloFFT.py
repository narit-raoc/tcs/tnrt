# --------
# Schedule
# --------
project_id = "test_mock_holofft"
obs_id = "SS"

schedule_params = ScheduleParams(project_id, obs_id, source_name="sim_source", line_name="sim_line")

# --------
# Tracking
# --------
az = 150
el = 60
tracking_params = TrackingParamsHO(az, el)


# --------
# Backend
# --------
freq_res = 100.0
fft_length = 100
backend_params = BackendParamsHoloFFT(freq_res, fft_length, mock=True)

# ---------------------------------------
# Scan type - Map, YES center cal, NO zigzag (same as Holography obs)
# ---------------------------------------
fwhm = 160
angular_resolution = fwhm / 2
line_length = fwhm * 4
measurements_per_line = int(line_length / angular_resolution)

# NOTE: this is the imporant link between ACU scan control and required time per measurement given freq_res
time_per_angular_step = 0.5

log("fwhm: {} arcsec, angular_resolution: {}, line_length: {}, measurements_per_line: {}, time_per_angular_step: {}".format(
    fwhm, angular_resolution, line_length, measurements_per_line, time_per_angular_step))

time_per_line = time_per_angular_step * measurements_per_line
nlines = measurements_per_line
spacing = angular_resolution
axis = "x"
zigzag = False
coord_system = "HO"
subscans_per_cal = 1
time_cal0 = 1
time_per_cal = 1

scantype_params = ScantypeParamsMap(line_length, time_per_line, nlines, spacing, axis,
    zigzag, coord_system, subscans_per_cal, time_cal0, time_per_cal)


# --------------
# Date pipelines
# --------------
data_params = DataParams(["atfits"])

add_scan(schedule_params, scantype_params, tracking_params, backend_params=backend_params, data_params=data_params)

# Run all scans in the queue.  show results JSON after queue is finished.
results = run_queue()

log("all results: {}".format(results))
