delay_until_scheduled_start = 5
project_id = "test script"
obs_id = "SS"
start_mjd = (Time.now() + delay_until_scheduled_start * units.second).mjd
source_name = "AAA"
line_name = "BBB"
schedule_params = ScheduleParams(
    project_id,
    obs_id,
    start_mjd=start_mjd,
    source_name=source_name,
    line_name=line_name,
)


duration = 60  # second
scantype_params = ScantypeParamsOnSource(duration)

# Tracking Horizontal
az = 50
el = 45
tracking_params = TrackingParamsHO(az, el)

#  For L-band provision
# backend_params = BackendParamsEDD("TNRT_dualpol_spectrometer_L")
# backend_params = BackendParamsEDD("TNRT_dualpol_spectrometer_L", integration_time=1, freq_res=4000)

# backend_params = BackendParamsEDD("TNRT_stokes_spectrometer_L")
# backend_params = BackendParamsEDD("TNRT_stokes_spectrometer_L", integration_time=1, freq_res=4000)

# Select backend EDD, choose pulsar timming mode, and override some settings
# backend_params = BackendParamsEDD('TNRT_pulsar_L', project="test_pulsar", source_name="B0332+5434", ra="03h32m59.4096" , dec="+54d34m43.329s")

#  For K-band provision
# backend_params = BackendParamsEDD("TNRT_dualpol_spectrometer_K")


add_scan(schedule_params, scantype_params, tracking_params, backend_params=backend_params)
results = run_queue()

results = stop_queue()
