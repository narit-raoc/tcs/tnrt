# Script to reproduce observation activity of ACU that results in dAz jump.
# It was observed when attempting to collect data for pointing model.

# NOTE: Before running this script on Simulator, preprae clocks as follows:
# - simulate the AZ, EL to find the time at which CygA is at Az = +10 deg.  
# - set the local OS computer clock to this start time
# - start ACS and nash.  The system will detect that ACU clock is not aligned with
# local OS clock.  Select "Y" to force ACU clock to follow local OS clock.
# - run this script at any time (during the real day), but TCS computer and ACU are using
# a false time to achieve the desired geometry.

# Command to find the time that we need to start the observation
# Find approximate time by simulation 24 hours with 100 data points.
# python issue543_find_starttime.py -n 100 -u 86400 -r 299.8681523682083 -d 40.73391589791667
# Simulate less duration and more data points to find the exact time to start the observation
# and calculate duration between start time and az arrives at +345
# python tnrt-543_find_starttime.py -n 1000 -u 3600 -r 299.8681523682083 -d 40.73391589791667 -s 2023-10-17T11:20:00.000

# For test on real ACU tracking object on sky, no need to adjust the clock.  
# Run the script at the actual time (12:00 UTC)

# - need 40 minutes to track across this range of AZ angles.

# https://gitlab.com/tnrt-commissioning/l-band-commissioning/-/issues/69
from astropy.coordinates import Longitude
from astropy.coordinates import Latitude

# Schedule params
project_id = 'tnrt-543_dAZ_jump_CCW'
obs_id = 'SS'
source_name = "CygA"

# Scantype Parameters for single cross scan - reuse for all scans
arm_length = 10560
time_per_arm = 60
scantype_params = ScantypeParamsCross(arm_length, time_per_arm)

# Tracking Parameters
# Input in HMS, DMS format because that's how the data is listed in Gitlab
# observation log.  Use Astropy to convert the ra, dec to decimal degrees.
ra_icrs_apy = Longitude("19h59m28.35656837s")
dec_icrs_apy =  Latitude("40d44m02.0972325s")
tracking_params = TrackingParamsEQ(ra_icrs_apy.deg, dec_icrs_apy.deg)

# Frontend receiver
# frontend_params = FrontendParamsK()

# Backend signal processing configuration
# NOTE: use BakendParams for simulator test, use BakendParamsEDD for real TNRT test
backend_params = BackendParamsEDD("TNRT_dualpol_spectrometer", integration_time=1, freq_res=200e3)
# backend_params = BackendParamsMock("TNRT_dualpol_spectrometer", integration_time=1, freq_res=200e3)

clear_axis_position_offsets()

# set pointing corrections
set_pmodel(3532, 259, -154, -118, 28, 0, 2603, -478, -554)
#clear_refraction() # clear pointing correction in EL from refraction model

# We can't interrupt the queue (cancel scan doesn't work!). Therefore, we cannot use
# the queue for this experiment.  because we must detect when the antenna tracks across North
# to approximately -5 deg.  Then command ACU to rotate around to +355 before we continue.

# Rotate around to Az=+370, El= same as current value.  This current position will guarantee that
# RADEC shorcut algorithm will start at +370, not +10 (it can happen if the previous position of AZ is < 240 for whatever reason)
goto_azel(370, session.antenna.objref.getElPosition())

log("current AZ, EL = ({},{})".format(session.antenna.objref.getAzPosition(), session.antenna.objref.getElPosition()))

while session.antenna.objref.getAzPosition() > 355:
    log("current AZ = {}".format(session.antenna.objref.getAzPosition()))
    schedule_params = ScheduleParams(project_id, obs_id)
    add_scan(schedule_params, scantype_params, tracking_params, backend_params=backend_params)
    run_queue()

# Rotate around to Az=+355, El= same as current value
goto_azel(-5, session.antenna.objref.getElPosition())

# Continue running cross-scan observations while tracking until ?? deg
while session.antenna.objref.getAzPosition() > -20:
    log("current AZ, EL = ({},{})".format(session.antenna.objref.getAzPosition(), session.antenna.objref.getElPosition()))
    schedule_params = ScheduleParams(project_id, obs_id)
    add_scan(schedule_params, scantype_params, tracking_params, backend_params=backend_params)
    run_queue()