# Minimum schedule parameters
project_id = 'no_projid'
obs_id = 'no_obsid'

# Scantype Parameters
subscan_duration = 10

# Tracking
az = 180
el = 60
tracking_offset_az = 0
tracking_offset_el = 0

# Parameter Blocks
schedule_params = ScheduleParams(project_id, obs_id)
scantype_params = ScantypeParamsOnSource(subscan_duration)
tracking_params = TrackingParamsHO(az, el, tracking_offset_az, tracking_offset_el)

clear_pmodel()
clear_axis_position_offsets()
clear_refraction()

# [1] No corrections
# -------------------
add_scan(schedule_params, scantype_params, tracking_params)
results = run_queue()
log('results for no pointing corrections: {}'.format(results))

# [2] Tracking offsets AZ, EL
# -------------------
tracking_offset_az = 2 * 3600
tracking_offset_el = 3 * 3600
tracking_params = TrackingParamsHO(az, el, tracking_offset_az, tracking_offset_el)
add_scan(schedule_params, scantype_params, tracking_params)
results = run_queue()
log('results for tracking offset AZ=2, EL=3) : {}'.format(results))
