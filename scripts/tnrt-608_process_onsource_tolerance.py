# -*- coding: utf-8 -*-
import argparse
from argparse import RawTextHelpFormatter
import os
import sys
import numpy as np
import pyqtgraph as pg
from pyqtgraph.Qt import QtGui, QtCore

from astropy.io import fits
from astropy import units
from astropy.coordinates import EarthLocation
from astropy.time import Time
from astropy.time import TimeDelta

from astropy.coordinates import Longitude
from astropy.coordinates import Latitude

from coordinate_utility import icrs_altaz_astropy
from RaDecToAzEl import FB_RaDec2AzEl

# NOTE: Default is dark background and CMY colors. Set white_background=True
# if you want white background and RGB colors.
white_background = False
#white_background = True

size1 = 18
size2 = 12
size3  = 6

if white_background is True:
    pg.setConfigOption("background", "w")
    pg.setConfigOption("foreground", "k")

    # Use RGB color for white background
    color1 = "b"
    color2 = "r"
    color3 = "g"
    color4 = "k"
else:
    # Use CMY color for dark background
    color1 = "y"
    color2 = "m"
    color3 = "c"
    color4 = "g"

# Setup rules for command line arguments
parser = argparse.ArgumentParser(
    description="Reads FITS file written by PipelineMbfits and diplays some graphs",
    formatter_class=RawTextHelpFormatter,
)
parser.add_argument(
    "filename", help="FITS file with format specified by DataFormatMbfits.py"
)

# If no command line arguments are avaiable, show help message and exit
if len(sys.argv[1:]) == 0:
    parser.print_help()
    exit()

# Read command line arguments into argparse structure
args = parser.parse_args()

hdul = None

if os.path.isfile(args.filename) is not True:
    print("ERROR: File not found")
    print("os.path.isfile(%s) = False" % args.filename)
    hdul = None
else:
    print("Processing file %s: " % args.filename)
    fid = open(args.filename, "rb")
    hdul = fits.open(fid)
    # leave the file open becuase fits.open will not store the entire file in RAM if it is
    # too big.  Close file at the end of this script.
    # print(hdul.info())
if hdul is None:
    exit()

# Constant
ARCSEC_PER_DEG = 3600

time_acu = Time(hdul["MONITOR-ANT-GENERAL"].data["actual_time"], format="mjd", scale="utc")
po_start_time = Time(hdul["MONITOR-ANT-TRACKING-OBJECT"].data["po_start_time"], format="mjd", scale="utc")

AZ = 0
EL = 1
actual_position_az = hdul['MONITOR-ANT-AXIS'].data['actual_position'][:, AZ]
actual_position_el = hdul['MONITOR-ANT-AXIS'].data['actual_position'][:, EL]

desired_position_az = hdul['MONITOR-ANT-AXIS'].data['desired_position'][:, AZ]
desired_position_el = hdul['MONITOR-ANT-AXIS'].data['desired_position'][:, EL]

position_error_az = 3600 * (desired_position_az - actual_position_az)
position_error_el = 3600 * (desired_position_el - actual_position_el)
position_error_az_projected_on_sky = position_error_az * np.cos(actual_position_el * np.pi / 180.0)
position_error_total = np.sqrt(position_error_az_projected_on_sky ** 2 + position_error_el ** 2)


# Second condition is when Program Track is "running" state
PTS_RUNNING = 1 << 4  # From ICD document.  bit 4 indicates program track running.
condition_program_offset_running = np.array(
    (hdul["MONITOR-ANT-TRACKING"].data["prog_offset_bs"] & PTS_RUNNING) > 0,
    dtype=bool,
)

index_to_keep = np.where(condition_program_offset_running)

# Time axis data
time_unix = time_acu.utc.unix
time_unix_po_running = time_unix[index_to_keep]
po_start_time_unix_running = po_start_time.utc.unix[index_to_keep]


# -----------------------------------------------------------------------------
# Make Graphs
# -----------------------------------------------------------------------------
# Strings for labels
str_title = "File {}".format(os.path.basename(args.filename))
grid_opacity = 0.7
app = QtGui.QApplication([])

# -----------------------------------------------------------------------------
# Graph window 1 - Position
# -----------------------------------------------------------------------------
glw_pos = pg.GraphicsLayoutWidget(show=True)
glw_pos.resize(1024, 768)
glw_pos.setWindowTitle("ACU On-Source Tolerance Analysis: {}".format(str_title))
pg.setConfigOptions(antialias=True)

# Plot AZ vs Time
pi_az_time = glw_pos.addPlot(0, 0, title=str_title)
pi_az_time.addLegend()
pi_az_time.plot(time_unix, actual_position_az, name="actual position", pen=None, symbol="o", symbolBrush=None, symbolPen=color1, symbolSize=size1)
pi_az_time.plot(time_unix, desired_position_az, name="desired position", pen=None, symbol="o", symbolBrush=None, symbolPen=color2, symbolSize=size2)

ax_L = pg.AxisItem(orientation="left", text="AZ", units="deg")
ax_B = pg.DateAxisItem(orientation="bottom", utcOffset=0,text="UTC")
ax_L.showLabel(True)
ax_B.showLabel(True)
pi_az_time.setAxisItems(axisItems={"bottom": ax_B, "left" : ax_L})
pi_az_time.showGrid(True, True, alpha=grid_opacity)

# # Plot EL vs Time
# pi_el_time = glw_pos.addPlot(1, 0)
# pi_el_time.addLegend()
# pi_el_time.plot(time_unix, actual_position_el, name="actual position", pen=None, symbol="o", symbolBrush=None, symbolPen=color1, symbolSize=size1)
# pi_el_time.plot(time_unix, desired_position_el, name="desired position", pen=None, symbol="o", symbolBrush=None, symbolPen=color2, symbolSize=size2)

# ax_L = pg.AxisItem(orientation="left", text="EL", units="deg")
# ax_B = pg.DateAxisItem(orientation="bottom", utcOffset=0,text="UTC")
# ax_L.showLabel(True)
# ax_B.showLabel(True)
# pi_el_time.setAxisItems(axisItems={"bottom": ax_B, "left" : ax_L})
# pi_el_time.showGrid(True, True, alpha=grid_opacity)

# Plot Position errors vs Time
onsource_tolerance = 8
onsource_tolerance_line = pg.InfiniteLine(pos=onsource_tolerance, 
        angle=0, 
        movable=False, 
        pen="m", 
        name="On Source Tolerance {}".format(onsource_tolerance)
        )
pi_err_time = glw_pos.addPlot(3, 0)
pi_err_time.addLegend()
#pi_err_time.plot(time_unix, position_error_az, name="position_error_az", pen=None, symbol="o", symbolBrush=None, symbolPen=color1, symbolSize=size3)
#pi_err_time.plot(time_unix, position_error_el, name="position_error_el", pen=None, symbol="o", symbolBrush=None, symbolPen=color2, symbolSize=size3)
#pi_err_time.plot(time_unix, position_error_az_projected_on_sky, name="position_error_az_projected_on_sky", pen=None, symbol="o", symbolBrush=None, symbolPen=color3, symbolSize=size3)
pi_err_time.plot(time_unix, position_error_total, name="position_error_total", pen=None, symbol="o", symbolBrush=None, symbolPen=color1, symbolSize=size3)
pi_err_time.plot(time_unix_po_running, np.zeros(time_unix_po_running.shape),
                 name="program offset (subscan) running", pen=None, symbol="+", symbolBrush=None, symbolPen=color4, symbolSize=size3)

pi_err_time.plot(po_start_time_unix_running, np.zeros(po_start_time_unix_running.shape),
                 name="po_start_time_unix_running", pen=None, symbol="o", symbolBrush=None, symbolPen=color3, symbolSize=size3)

pi_err_time.addItem(onsource_tolerance_line)

ax_L = pg.AxisItem(orientation="left", text="Error", units="arcsec")
ax_L.enableAutoSIPrefix(False)
ax_B = pg.DateAxisItem(orientation="bottom", utcOffset=0,text="UTC")
ax_L.showLabel(True)
ax_B.showLabel(True)
pi_err_time.setAxisItems(axisItems={"bottom": ax_B, "left" : ax_L})
pi_err_time.showGrid(True, True, alpha=grid_opacity)


# Start the Qt event loop and block this script until user closes the GUI window
# QtGui.QApplication.instance().exec_()
app.exec_()

# Close the file
fid.close()
