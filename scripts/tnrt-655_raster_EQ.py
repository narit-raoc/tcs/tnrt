# ----------------------------------------------------------------------
# Schedule
# ----------------------------------------------------------------------
project_id = "tnrt-655_raster_EQ"
obs_id = "SS"
source_name = "fake"
line_name = "continuum"

# ----------------------------------------------------------------------
# Tracking
# ----------------------------------------------------------------------

# Choose an AZ , EL above the horizon in the region that we want to test tracking.
az = 100 * units.deg
el = 60 * units.deg
log("Select a coordinate above the horizon now. (AZ, EL) = ({}, {}) [deg]".format(az.value, el.value))

# Transform the AZ, EL coordinate to ICRS that will be used to configure the tracking.
coord_altaz = SkyCoord(
    frame="altaz",
    az=az,
    alt=el,
    obstime=Time.now(),
    location=get_site_location(),
)
coord_icrs = coord_altaz.transform_to("icrs")
log("Calculated ICRS (RA, DEC) = ({}, {}) [deg]".format(coord_icrs.ra.deg, coord_icrs.dec.deg))

# Get the RA, DEC values from SkyCoord as floats.  Set all others to 0.
ra_catalog_icrs = coord_icrs.ra.deg
dec_catalog_icrs = coord_icrs.dec.deg
pm_ra = 0
pm_dec = 0
parallax = 0
radial_velocity = 0.0
user_pointing_correction_az = 0
user_pointing_correction_el = 0
send_icrs_to_acu = False

log('ra, dec: {},{}'.format(ra_catalog_icrs, dec_catalog_icrs))

tracking_params = TrackingParamsEQ(
    ra_catalog_icrs,
    dec_catalog_icrs,
    pm_ra,
    pm_dec,
    parallax,
    radial_velocity,
    send_icrs_to_acu,
    user_pointing_correction_az,
    user_pointing_correction_el,
)

# ----------------------------------------------------------------------
# Scantype pattern
# ----------------------------------------------------------------------
frontend_params = None
# Add frontend params in real system to record MONITOR data in .mbfits for frontend
# frontend_params = FrontendParamsL()

# ----------------------------------------------------------------------
# Backend
# ----------------------------------------------------------------------
backend_params = BackendParamsEDD("TNRT_dualpol_spectrometer_L", 
    integration_time = 2.0,
    freq_res = 200000.0,
    mock=True)

# ----------------------------------------------------------------------
# Data pipelines
# ----------------------------------------------------------------------
data_params = DataParams(["mbfits"])

# ----------------------------------------------------------------------
# Scantype pattern
# ----------------------------------------------------------------------

xlen  = 2400    #arcsec
xstep = 1200  #arcsec
ylen  = 2400    #arcsec 
ystep = 1200  #arcsec 
time_per_point = 10 #s
zigzag = True
primary_axis = "X"
coord_system = "HO"
offsource_x = -4386 #arcsec
offsource_y = 0.0 #arcsec
offsource_duration = 10 #s
offsource_coord_system ="HO"

scantype_params = ScantypeParamsRaster(
    xlen,
    xstep,
    ylen,
    ystep,
    time_per_point,
    zigzag,
    primary_axis,
    coord_system,
    offsource_x ,
    offsource_y ,
    offsource_duration,
    offsource_coord_system,
)

schedule_params = ScheduleParams(project_id, obs_id, scan_id=101, source_name=source_name, line_name=line_name)
add_scan(schedule_params, scantype_params, tracking_params, frontend_params, backend_params, data_params)

# >>>> Change coordinate system to "HO"
coord_system = "EQ"
offsource_coord_system ="EQ"

scantype_params = ScantypeParamsRaster(
    xlen,
    xstep,
    ylen,
    ystep,
    time_per_point,
    zigzag,
    primary_axis,
    coord_system,
    offsource_x ,
    offsource_y ,
    offsource_duration,
    offsource_coord_system,
)

schedule_params = ScheduleParams(project_id, obs_id, scan_id=102, source_name=source_name, line_name=line_name)
add_scan(schedule_params, scantype_params, tracking_params, frontend_params, backend_params, data_params)

# ---------------------------------------
# Scan type - Map, NO center cal, YES zigzag, offset pattern in RA, Dec
# ---------------------------------------

line_length = 7200
time_per_line = 30
nlines = 10
spacing = 720
axis = "x"
zigzag = True
coord_system = "HO"
subscans_per_cal = 0
time_cal0 = 30
time_per_cal = 5

scantype_params = ScantypeParamsMap(
    line_length,
    time_per_line,
    nlines,
    spacing,
    axis,
    zigzag,
    coord_system,
    subscans_per_cal,
    time_cal0,
    time_per_cal)

schedule_params = ScheduleParams(project_id, obs_id, scan_id=201, source_name=source_name, line_name=line_name)
add_scan(schedule_params, scantype_params, tracking_params, frontend_params, backend_params, data_params)

# >>>> Change coordinate system to "EQ"
coord_system = "EQ"

scantype_params = ScantypeParamsMap(
    line_length,
    time_per_line,
    nlines,
    spacing,
    axis,
    zigzag,
    coord_system,
    subscans_per_cal,
    time_cal0,
    time_per_cal)

schedule_params = ScheduleParams(project_id, obs_id, scan_id=202, source_name=source_name, line_name=line_name)
add_scan(schedule_params, scantype_params, tracking_params, frontend_params, backend_params, data_params)
# Run all scans in the queue. Receive results after queue is finished.
results = run_queue()
log("all results: {}".format(results))
