delay_until_scheduled_start = 0
project_id = "no_projid"
obs_id = "no_obsid"
start_mjd = (Time.now() + delay_until_scheduled_start * units.second).mjd
source_name = "no_source"
line_name = "no_line"
schedule_params = ScheduleParams(project_id, obs_id, start_mjd=start_mjd)
# schedule_params = ScheduleParams(project_id, obs_id, scan_id, priority, start_mjd, source_name, line_name)

# Choose an AZ , EL above the horizon in the region that we want to test tracking.
az = 40 * units.deg
el = 60 * units.deg
logger.debug(
    "Choose a coordinate above the horizon now. (AZ, EL) = ({}, {}) [deg]".format(
        az.value, el.value
    )
)

# Transform the AZ, EL coordinate to ICRS that will be used to configure the tracking.
coord_altaz = SkyCoord(
    frame="altaz",
    az=az,
    alt=el,
    obstime=Time.now(),
    location=get_site_location(),
)
coord_icrs = coord_altaz.transform_to("icrs")
logger.debug(
    "Calculated ICRS (RA, DEC) = ({}, {}) [deg]".format(
        coord_icrs.ra.deg, coord_icrs.dec.deg
    )
)

# Get the RA, DEC values from SkyCoord as floats.  Set all other properties to 0.
ra_catalog_icrs = coord_icrs.ra.deg
dec_catalog_icrs = coord_icrs.dec.deg
pm_ra = 0
pm_dec = 0
parallax = 0
radial_velocity = 0.0
send_icrs_to_acu = False
tracking_offset_az = 0.0
tracking_offset_el = 0.0
tracking_params_EQ = TrackingParamsEQ(
    ra_catalog_icrs,
    dec_catalog_icrs,
    pm_ra,
    pm_dec,
    parallax,
    radial_velocity,
    send_icrs_to_acu,
    tracking_offset_az,
    tracking_offset_el,
)

tracking_params_HO = TrackingParamsHO(40, 60)

arm_length = 7200
time_per_arm = 30.0
win_min = -1000
win_max = 1000
double_cross = False
scantype_params_cross = ScantypeParamsCross(
    arm_length, time_per_arm, win_min, win_max, double_cross
)

xlen = 1200
xstep = 600
ylen = 1200
ystep = 600
time_per_point = 7
zigzag = True
primary_axis_enum = "X"
coord_system_enum = "HO"

scantype_params_raster = ScantypeParamsRaster(
    xlen,
    xstep,
    ylen,
    ystep,
    time_per_point,
    zigzag,
    primary_axis_enum,
    coord_system_enum,
)

backend_params = BackendParamsMock("TNRT_dualpol_spectrometer")

log("tracking source at approximately 60 deg EL")
add_scan(schedule_params, scantype_params_cross, tracking_params_EQ, backend_params=backend_params)
add_scan(schedule_params, scantype_params_raster, tracking_params_EQ, backend_params=backend_params)

""" log("tracking source at approximately 30 deg EL")
# Choose an AZ , EL above the horizon in the region that we want to test tracking.
az = 40 * units.deg
el = 30 * units.deg
logger.debug(
    "Choose a coordinate above the horizon now. (AZ, EL) = ({}, {}) [deg]".format(
        az.value, el.value
    )
)

# Transform the AZ, EL coordinate to ICRS that will be used to configure the tracking.
coord_altaz = SkyCoord(
    frame="altaz",
    az=az,
    alt=el,
    obstime=Time.now(),
    location=get_site_location(),
)
coord_icrs = coord_altaz.transform_to("icrs")
logger.debug(
    "Calculated ICRS (RA, DEC) = ({}, {}) [deg]".format(
        coord_icrs.ra.deg, coord_icrs.dec.deg
    )
)

# Get the RA, DEC values from SkyCoord as floats.  Set all other properties to 0.
ra_catalog_icrs = coord_icrs.ra.deg
dec_catalog_icrs = coord_icrs.dec.deg
pm_ra = 0
pm_dec = 0
parallax = 0
radial_velocity = 0.0
send_icrs_to_acu = False
tracking_offset_az = 0.0
tracking_offset_el = 0.0
tracking_params_EQ = TrackingParamsEQ(
    ra_catalog_icrs,
    dec_catalog_icrs,
    pm_ra,
    pm_dec,
    parallax,
    radial_velocity,
    send_icrs_to_acu,
    tracking_offset_az,
    tracking_offset_el,
)


add_scan(schedule_params, scantype_params_cross, tracking_params_EQ, backend_params=backend_params)
add_scan(schedule_params, scantype_params_raster, tracking_params_EQ, backend_params=backend_params)
 """
# Run all scans in the queue.  Receive results after queue is finished.
results = run_queue()

# Show the results of all scans from this run to the terminal window.
# All types of scans have some common results.  For example: a list of filenames
# that were recorded.
log("all results: %s" % results)
