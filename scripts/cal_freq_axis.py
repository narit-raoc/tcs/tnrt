import numpy as np

sampling_rate = 4e9 # from hardware digitizer fixed
predecimation_factor = 1 # from provision L=2, K=1

decimated_sampling_rate = sampling_rate / predecimation_factor

nchannels = 8192 # from user parameter freq res and calculate
freqres = (decimated_sampling_rate / 2) / nchannels #1CDLT2F

print(freqres)

f0_nyquist_zone_2 = decimated_sampling_rate / 2
ref_channel =0  #1CRPX2F
channel_index = np.arange(ref_channel, ref_channel + nchannels)

intermediate_freq = f0_nyquist_zone_2 + (channel_index * freqres)

down_converter_freq = 16e9 # from frontend L=0, K=depend

rf_freq = intermediate_freq + down_converter_freq

print(rf_freq[ref_channel]) # 1CRVL2F
print(np.diff(rf_freq))

print(rf_freq[-1]+freqres)


rf_freq_at_ref_channel = f0_nyquist_zone_2 + (ref_channel * freqres) + down_converter_freq
print(rf_freq[ref_channel], rf_freq_at_ref_channel)
print(freqres)