# Schedule parameters
project_id = 'no_project_id'
obs_id = 'no_obsid'

# Scan type parameters.
# Typically, a manual scan we must stop manually by command stop_queue().
# We have option to set the time for the scan to automatically timeout and stop.
# If we don't set the duration, it will automatically stop after 60 minutes 
# (file size grows approximately 1 MB per minute)
duration = 300 # seconds

schedule_params = ScheduleParams(project_id, obs_id)
scantype_params = ScantypeParamsManual(duration)

# Create the scan and add it to the queue
add_scan(schedule_params, scantype_params)

# Run all scans in the queue.  Use blocking=False to add manual commands
# and manual stop scan after your commands are finished.

# Note, when using ScantypeParamsManual, the ACU tracking generator is not active
# therefore some offsets such as pointing model and refraction do not affect the result.
# These parameters are used by the tracking trajectory generator.
results = run_queue(blocking=False)

# -------------------------------------------------------------------------------
# Do something interesting (that doesn't depend on tracking trajectory generator)

# Wait and record data that includes time stamps from ACU and TCS computer
waitrel(duration, True, 5.0)

# -------------------------------------------------------------------------------
# Finished manual commands.  Now manual stop the scan.
# If you don't stop a non-blocking scan now, 
# it will timeout after `duration` seconds.
results = stop_queue()
