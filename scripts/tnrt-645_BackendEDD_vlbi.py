# Schedule
project_id = "BackendEDD_params"
obs_id = "SS"
source_name = "AAA"
line_name = "BBB"
schedule_params = ScheduleParams(
    project_id,
    obs_id,
    source_name=source_name,
    line_name=line_name,
)

# Scan Type
duration = 20  # second
scantype_params = ScantypeParamsOnSource(duration)

# Tracking
az = 50
el = 45
tracking_params = TrackingParamsHO(az, el)

# TCS Data pipelines.  
# Note: When BackendEDD is configured for pulsar, vlbi
# "spectrum_preview" doesn't have data.  no effect
# "mbfits" doesn't have spectrum data, but should still have ACU data in the MONITOR-ANT-xxxxx tables
# data_params = DataParams(["mbfits", "spectrum_preview"])
data_params = None


# Frontend configuration
# Select L-band receiver.  
# Currently (05/2024), we don't have any config parameters for the L-band frontend, but soon will have the option auto-enable LNA.
# frontend_params = FrontendParamsL()
frontend_params = None

#  Create various BackendEDD configurations.
# Set mock = True / False as needed for test
is_mock = True
backend_params_spectrometer = BackendParamsEDD("TNRT_dualpol_spectrometer_L", mock=is_mock, freq_res=122070.3125, integration_time=1.0)
backend_params_vlbi = BackendParamsEDD("TNRT_VLBI_L", mock=is_mock, output_samplerate=16000000, f_lo=238000000)
backend_params_pulsar = BackendParamsEDD('TNRT_pulsar_search_dryrun', mock=is_mock, project="nash_test", source_name="J0437-4715", ra="03h32m59.4096" , dec="+54d34m43.329s")

# Add some scans
add_scan(schedule_params, scantype_params, tracking_params, frontend_params, backend_params_spectrometer, data_params)
add_scan(schedule_params, scantype_params, tracking_params, frontend_params, backend_params_vlbi, data_params)
add_scan(schedule_params, scantype_params, tracking_params, frontend_params, backend_params_pulsar, data_params)

results = run_queue()
