project_id = "no_projid"
obs_id = "no_obsid"
schedule_params = ScheduleParams(project_id, obs_id, source_name="sim_source", line_name="sim_line")
tracking_params = TrackingParamsHO(150, 60)
backend_params = BackendParamsMock("TNRT_dualpol_spectrometer", integration_time=1, freq_res=250000)

# ---------------------------------------
# Scan type - single horizontal line scan
# ---------------------------------------
line_length = 7200
time_per_line = 30
nlines = 1
spacing = 720
axis = "x"
zigzag = False
coord_system = "HO"
subscans_per_cal = 0
time_cal0 = 30
time_per_cal = 5

scantype_params = ScantypeParamsMap(line_length, time_per_line, nlines, spacing, axis,
    zigzag, coord_system, subscans_per_cal, time_cal0, time_per_cal)

set_velocity("az", 2)
set_velocity("el", 1)
add_scan(schedule_params, scantype_params, tracking_params, backend_params=backend_params)
results = run_queue()

# Change velocity to half and run again
set_velocity("az", 1)
set_velocity("el", 0.5)
add_scan(schedule_params, scantype_params, tracking_params, backend_params=backend_params)
results = run_queue()

# Change velocity to 0.25 and run again
set_velocity("az", 0.5)
set_velocity("el", 0.25)
add_scan(schedule_params, scantype_params, tracking_params, backend_params=backend_params)
results = run_queue()

# Show the results of all scans from this run to the terminal window.
# All types of scans have some common results.  For example: a list of filenames
# that were recorded.
log("all results: {}".format(results))
