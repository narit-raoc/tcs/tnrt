import argparse
from argparse import RawTextHelpFormatter
import os
import sys
import numpy as np
import pyqtgraph as pg
from pyqtgraph.Qt import QtGui, QtCore
from astropy.io import fits

# NOTE: Default is dark background and CMY colors. Set white_background=True
# if you want white background and RGB colors.
white_background = False
# white_background = True

if white_background is True:
    pg.setConfigOption("background", "w")
    pg.setConfigOption("foreground", "k")

    # Use RGB color for white background
    color_acu = "b"
    color_tcs = "r"
    color_diff = "g"
else:
    # Use CMY color for dark background
    color_acu = "y"
    color_tcs = "c"
    color_diff = "m"

# Setup rules for command line arguments
parser = argparse.ArgumentParser(
    description="Reads FITS file written by PipelineMbfits and diplays some graphs",
    formatter_class=RawTextHelpFormatter,
)
parser.add_argument(
    "filename", help="FITS file with format specified by DataFormatMbfits.py"
)

# If no command line arguments are avaiable, show help message and exit
if len(sys.argv[1:]) == 0:
    parser.print_help()
    exit()

# Read command line arguments into argparse structure
args = parser.parse_args()

hdul = None

if os.path.isfile(args.filename) is not True:
    print("ERROR: File not found")
    print("os.path.isfile(%s) = False" % args.filename)
    hdul = None
else:
    print("Processing file %s: " % args.filename)
    fid = open(args.filename, "rb")
    hdul = fits.open(fid)
    # leave the file open becuase fits.open will not store the entire file in RAM if it is
    # too big.  Close file at the end of this script.
    # print(hdul.info())
if hdul is None:
    exit()

# -----------------------------------------------------------------------------
# Make Graphs
# -----------------------------------------------------------------------------
# Strings for labels
str_title = "From File {}".format(os.path.basename(args.filename))
grid_opacity = 0.7
app = QtGui.QApplication([])

AZ = 0
EL = 1

# Make a time axis for all the plots.  Use seconds after start
tstart = hdul["MONITOR-ANT-PACKET"].data["time_tcs_mjd"][0]
tt = (hdul["MONITOR-ANT-PACKET"].data["time_tcs_mjd"] - tstart) * 86400.0

# Calcualte difference of time between TCS and ACU.
SECONDS_PER_DAY = 86400

deltat_tcs_acu = SECONDS_PER_DAY * (
    hdul["MONITOR-ANT-PACKET"].data["time_tcs_mjd"]
    - hdul["MONITOR-ANT-GENERAL"].data["actual_time"]
)

# -----------------------------------------------------------------------------
# Graph window `1
# -----------------------------------------------------------------------------
win1 = pg.GraphicsLayoutWidget(show=True)
win1.resize(1024, 768)
win1.setWindowTitle("ACU Time Analysis: {}".format(str_title))
pg.setConfigOptions(antialias=True)

# Create the PlotItem
p1a = win1.addPlot(title=str_title)
win1.nextRow()
p1b = win1.addPlot()

# Some config before we add the lines
# p1a.addLegend()
# p1b.addLegend()

# Plot the message sequence number
line1a = p1a.plot(
    hdul["MONITOR-ANT-PACKET"].data["status_message_counter"],
    name="ACU Sequence",
    pen=color_acu,
    symbol="+",
    symbolPen=color_acu,
)
line1b = p1b.plot(
    np.diff(hdul["MONITOR-ANT-PACKET"].data["status_message_counter"]),
    name="Diff ACU Sequence",
    pen=color_acu,
    symbol="+",
    symbolPen=color_acu,
)

# Some config after we add the lines
axis_bottom1a = pg.AxisItem(orientation="bottom", text="Array index in data file")
axis_bottom1a.showLabel(True)

axis_left1a = pg.AxisItem(orientation="left", text="Sequence Number")
axis_left1a.showLabel(True)

axis_bottom1b = pg.AxisItem(orientation="bottom", text="Array index in data file")
axis_bottom1b.showLabel(True)

axis_left1b = pg.AxisItem(orientation="left", text="numpy.diff(sequence number)")
axis_left1b.showLabel(True)

p1a.setAxisItems(
    axisItems={
        "bottom": axis_bottom1a,
        "left": axis_left1a,
    }
)

p1b.setAxisItems(
    axisItems={
        "bottom": axis_bottom1b,
        "left": axis_left1b,
    }
)

p1a.showGrid(True, True, alpha=grid_opacity)
p1b.showGrid(True, True, alpha=grid_opacity)

# -----------------------------------------------------------------------------
# Graph window `2
# -----------------------------------------------------------------------------
win2 = pg.GraphicsLayoutWidget(show=True)
win2.resize(1024, 768)
win2.setWindowTitle("ACU Time Analysis: {}".format(str_title))
pg.setConfigOptions(antialias=True)

# Create the PlotItem
p2a = win2.addPlot(title=str_title)
win2.nextRow()
p2b = win2.addPlot()
win2.nextRow()
p2c = win2.addPlot()

# Some config before we add the lines
# p2a.addLegend()
# p2b.addLegend()
# p2c.addLegend()

# Plot the message sequence number
line2a = p2a.plot(
    hdul["MONITOR-ANT-GENERAL"].data["actual_time"],
    name="ACU Timestamp [MJD]",
    pen=color_acu,
    symbol="+",
    symbolPen=color_acu,
)
line2b = p2b.plot(
    hdul["MONITOR-ANT-PACKET"].data["time_tcs_mjd"],
    name="ACU Timestamp [MJD]",
    pen=color_acu,
    symbol="+",
    symbolPen=color_tcs,
)
line2c = p2c.plot(
    deltat_tcs_acu,
    name="Difference (TCS - ACU)",
    pen=color_diff,
    symbol="+",
    symbolPen=color_diff,
)

# Some config after we add the lines
axis_bottom2a = pg.AxisItem(orientation="bottom", text="Array index in data file")
axis_bottom2a.showLabel(True)

axis_left2a = pg.AxisItem(orientation="left", text="ACU Timestamp [MJD]")
axis_left2a.showLabel(True)

axis_bottom2b = pg.AxisItem(orientation="bottom", text="Array index in data file")
axis_bottom2b.showLabel(True)

axis_left2b = pg.AxisItem(orientation="left", text="TCS Timestamp [MJD]")
axis_left2b.showLabel(True)

axis_bottom2c = pg.AxisItem(orientation="bottom", text="Array index in data file")
axis_bottom2c.showLabel(True)

axis_left2c = pg.AxisItem(orientation="left", text="Difference (TCS - ACU)", units="s")
axis_left2c.showLabel(True)

p2a.setAxisItems(
    axisItems={
        "bottom": axis_bottom2a,
        "left": axis_left2a,
    }
)

p2b.setAxisItems(
    axisItems={
        "bottom": axis_bottom2b,
        "left": axis_left2b,
    }
)
p2c.setAxisItems(
    axisItems={
        "bottom": axis_bottom2c,
        "left": axis_left2c,
    }
)

p2a.showGrid(True, True, alpha=grid_opacity)
p2b.showGrid(True, True, alpha=grid_opacity)
p2c.showGrid(True, True, alpha=grid_opacity)

# Start the Qt event loop and block this script until user closes the GUI window
# QtGui.QApplication.instance().exec_()
app.exec_()

# Close the file
fid.close()
