# --------
# Schedule
# --------
project_id = "test_mock_holofft"
obs_id = "SS"

schedule_params = ScheduleParams(project_id, obs_id, source_name="sim_source", line_name="sim_line")

# --------
# Tracking
# --------
az = 150
el = 60
tracking_params = TrackingParamsHO(az, el)

# --------
# Backend
# --------
freq_res = 100.0
fft_length = 100
backend_params = BackendParamsHoloFFT(freq_res, fft_length, mock=True)

# ---------------------------------------
# Scan type - Map, YES center cal, NO zigzag (same as Holography obs)
# ---------------------------------------
scantype_params = ScantypeParamsOnSource(10)

# --------------
# Date pipelines
# --------------
data_params = DataParams(["atfits"])

add_scan(schedule_params, scantype_params, tracking_params, backend_params=backend_params, data_params=data_params)

# Run all scans in the queue.  show results JSON after queue is finished.
results = run_queue()

log("all results: {}".format(results))
