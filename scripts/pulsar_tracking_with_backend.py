project_id  = "Tracking-Obs_On_xxx"
obs_id      = "xxx_xx"
source_name = "xxx"
line_name   = "xx"
scan_id     = 3

schedule_params = ScheduleParams(
    project_id, 
    obs_id, 
    source_name=source_name, 
    scan_id=scan_id, 
    line_name=line_name
)

# Scantype Parameters
duration         = 60*2 #sec if need duration = 120s, setting to 130s to confrim more than 120s
scantype_params  = ScantypeParamsOnSource(duration)

# Tracking Parameters
# Use astropy to transform hms to decimal degrees.

# J0332+5434
ra_hms  = "03h32m59.4096s"
dec_dms = "+54d34m43.329s"
source_name = "B0332+5434"

# J0358+5413
# ra_hms  = "03h58m53.7238s"
# dec_dms = "+54d13m13.784s"
# source_name = "B0358+5413"

# J0437-4715
# ra_hms  = "04h37m15.8961737s"
# dec_dms = "-47d15m09.110714s"
# source_name = "B0437-4715"

# J0738-4042
# ra_hms  = "07h38m32.262s"
# dec_dms = "-40d42m39.42s"
# source_name = "B0738-4042"

# J0742-2822
# ra_hms  = "07h42m49.058s"
# dec_dms = "-28d22m43.76s"
# source_name = "B0742-2822"

# J0835-4510
# ra_hms  = "08h35m20.61149s"
# dec_dms = "-45d10m34.8751s"
# source_name = "B0835-4510"

# J0953+0755
# ra_hms  = "09h53m09.3097s"
# dec_dms = "+07d55m35.75s"
# source_name = "B0953+0755"

# J1239+2453
# ra_hms  = "12h39m40.4614s"
# dec_dms = "+24d53m49.29s"
# source_name = "B1239+2453"

# J1935+1616
# ra_hms  = "19h35m47.8259s"
# dec_dms = "+16d16m39.986s"
# source_name = "B1935+1616"

# J2022+2854
# ra_hms  = "20h22m37.0671s"
# dec_dms = "+28d54m23.104s"
# source_name = "B2022+2854"

# J0332+5434
# ra_hms  = "03h32m59.4096s"
# dec_dms = "+54d34m43.329s"
# source_name = "B0332+5434"


# # Choose RA, DEC from catalog (ICRS coordinate system)
skc_temp = SkyCoord(ra_hms, dec_dms)
ra_catalog_icrs = skc_temp.ra.deg
dec_catalog_icrs = skc_temp.dec.deg

# Use below in case of decimal degree is available
# ra_catalog_icrs = 187.70593075958
# dec_catalog_icrs = 12.39112329392

pm_ra = 0.0
pm_dec = 0.0
parallax = 0.0
radial_velocity = 0.0
tracking_offset_az = 0.0 #arcsec
tracking_offset_el = 0.0 #arcsec
send_icrs_to_acu = False

log('my coordinate is: {}'.format(ra_catalog_icrs))
log(dec_catalog_icrs)

tracking_params = TrackingParamsEQ(
    ra_catalog_icrs,
    dec_catalog_icrs,
    pm_ra,
    pm_dec,
    parallax,
    radial_velocity,
    send_icrs_to_acu,
    tracking_offset_az,
    tracking_offset_el,
)

# Choose Frontend Parameters
# not yet, use kgui instead

# Choose Backend Parameters
backend_params = BackendParamsEDD(
    'TNRT_pulsar',
    project = "test_pulsar",
    source_name = source_name,
    ra = ra_hms,
    dec = dec_dms,
)

# Choose RefCoord Parametes (for off-source calibration)
# not yet

# Create the scan and add it to the queue
add_scan(
    schedule_params,
    scantype_params,
    tracking_params,
    backend_params=backend_params
)

# Run all scans in the queue.  Receive results after queue is finished.
results = run_queue()

# Show the results of all scans from this run to the terminal window.
# All types of scans have some common results.  For example: a list of filenames
# that were recorded.
log("all results: {}".format(results))
