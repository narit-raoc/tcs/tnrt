import argparse
from argparse import RawTextHelpFormatter
import sys
import numpy as np
import os

from scipy.interpolate import interp1d

from astropy.io import fits

import pyqtgraph as pg
from pyqtgraph.Qt import QtWidgets


white_background = False
# white_background = True

grid_opacity = 0.7

if white_background is True:

    pg.setConfigOption("background", "w")
    pg.setConfigOption("foreground", "k")

    # Use RGB color for white background
    color1 = "b"
    color2 = "r"
    color3 = "g"
    color4 = "k"

else:
    # Use CMY color for dark background
    color1 = "y"
    color2 = "m"
    color3 = "c"
    color4 = "w"

# Create Pen and Brush objects to use in the graphs
pen1 = pg.mkPen(color1)
pen2 = pg.mkPen(color2)
pen3 = pg.mkPen(color3)
pen4 = pg.mkPen(color4)

brush1 = pg.mkBrush(color1)
brush2 = pg.mkBrush(color2)
brush3 = pg.mkBrush(color3)
brush4 = pg.mkBrush(color4)

size1 = 18
size2 = 12
size3 = 6


# Setup rules for command line arguments
parser = argparse.ArgumentParser(
    description="Reads FITS file written by PipelineMbfits and diplays some graphs",
    formatter_class=RawTextHelpFormatter,
)
parser.add_argument("filename", help="FITS file with format specified by DataFormatMbfits.py")

# If no command line arguments are avaiable, show help message and exit
if len(sys.argv[1:]) == 0:
    parser.print_help()
    exit()

# Read command line arguments into argparse structure
args = parser.parse_args()

hdul = None

if os.path.isfile(args.filename) is not True:
    print("ERROR: File not found")
    print("os.path.isfile(%s) = False" % args.filename)
    hdul = None
else:
    print("Processing file %s: " % args.filename)
    fid = open(args.filename, "rb")
    hdul = fits.open(fid)
    # leave the file open becuase fits.open will not store the entire file in RAM if it is
    # too big.  Close file at the end of this script.
    # print(hdul.info())
if hdul is None:
    exit()

# Copy some values from DataFormatAcu.py (keep this script stanalone -- not require import)
AZ = 0
EL = 1
TR_SUN_TRACK_ACTIVE = 1 << 12
TR_PROG_TRACK_ACTIVE = 1 << 13
TR_TLE_TRACK_ACTIVE = 1 << 14
TR_STAR_TRACK_ACTIVE = 1 << 15

timestamps_spe = hdul["ARRAYDATA-MBFITS"].data["MJD"]
timestamps_acu = hdul["MONITOR-ANT-GENERAL"].data["actual_time"]

actual_position_az = hdul["MONITOR-ANT-AXIS"].data["actual_position"][:, AZ]
actual_position_el = hdul["MONITOR-ANT-AXIS"].data["actual_position"][:, EL]

# Find tracking center coordinate from selected tracking algorithm in monitor_ant_tracking["bit_status"]
# The bit status exists for every ACU status message in the array.  
# Assume it does not change during this subscan. So, we can use the first value at index [0] 
hdu_acu_tracking = hdul["MONITOR-ANT-TRACKING"]

if (hdu_acu_tracking.data["bit_status"][0] & TR_SUN_TRACK_ACTIVE) == TR_SUN_TRACK_ACTIVE:
    print("Found selected tracking algorithm: TR_SUN_TRACK_ACTIVE")
    selected_tracking_algorithm_position_az = hdu_acu_tracking.data["sun_track_az"]
    selected_tracking_algorithm_position_el = hdu_acu_tracking.data["sun_track_el"]

elif (hdu_acu_tracking.data["bit_status"][0] & TR_PROG_TRACK_ACTIVE) == TR_PROG_TRACK_ACTIVE:
    print("Found selected tracking algorithm: TR_PROG_TRACK_ACTIVE")
    selected_tracking_algorithm_position_az = hdu_acu_tracking.data["prog_track_az"]
    selected_tracking_algorithm_position_el = hdu_acu_tracking.data["prog_track_el"]

elif (hdu_acu_tracking.data["bit_status"][0] & TR_TLE_TRACK_ACTIVE) == TR_TLE_TRACK_ACTIVE:
    print("Found selected tracking algorithm: TR_TLE_TRACK_ACTIVE")
    selected_tracking_algorithm_position_az = hdu_acu_tracking.data["tle_track_az"]
    selected_tracking_algorithm_position_el = hdu_acu_tracking.data["tle_track_el"]

elif (hdu_acu_tracking.data["bit_status"][0] & TR_STAR_TRACK_ACTIVE) == TR_STAR_TRACK_ACTIVE:
    print("Found selected tracking algorithm: TR_STAR_TRACK_ACTIVE")
    selected_tracking_algorithm_position_az = hdu_acu_tracking.data["star_track_az"]
    selected_tracking_algorithm_position_el = hdu_acu_tracking.data["star_track_el"]

else:
    print("Cannot identify selected tracking algorithm.  Set tracking algorithm position = (0,0)")
    selected_tracking_algorithm_position_az = 0
    selected_tracking_algorithm_position_el = 0

long_off_acu = actual_position_az - selected_tracking_algorithm_position_az
lat_off_acu = actual_position_el - selected_tracking_algorithm_position_el

interp_function_acu_az = interp1d(
    timestamps_acu, actual_position_az, kind="cubic", fill_value="extrapolate"
)
interp_function_acu_el = interp1d(
    timestamps_acu, actual_position_el, kind="cubic", fill_value="extrapolate"
)
interp_function_acu_long_off = interp1d(
    timestamps_acu, long_off_acu, kind="cubic", fill_value="extrapolate"
)
interp_function_acu_lat_off = interp1d(
    timestamps_acu, lat_off_acu, kind="cubic", fill_value="extrapolate"
)


# Use the interpolation function to find the values of AZ, EL at the timestamps of spectrometer

az_at_timestamps_spectrometer = interp_function_acu_az(timestamps_spe)
el_at_timestamps_spectrometer = interp_function_acu_el(timestamps_spe)

DATAPAR_SUBSCAN_HORIZONTAL = 4
DATAPAR_SUBSCAN_VERTICAL = 6

NUMBER_OF_SUBSCAN = int(hdul['SCAN-MBFITS'].header['NSUBS'])
# ---------------------------------------------------------------
# PyQtGraph
# ---------------------------------------------------------------
str_title = "Interpolation " + os.path.basename(args.filename)
app = QtWidgets.QApplication([])
win1 = pg.GraphicsLayoutWidget(show=True)
win1.resize(1600, 800)
win1.setWindowTitle(str_title)
pg.setConfigOptions(antialias=True)
("b")

# Create the PlotItem
p1 = pg.PlotItem(title="Azimuth")
win1.addItem(p1, 0, 0, 1, 1)

# Some config before we add the lines
# p1.setAspectLocked(True)
p1.addLegend()

# AZ Actual Position from ACU
p1.plot(
    hdul["MONITOR-ANT-GENERAL"].data["actual_time"],
    hdul["MONITOR-ANT-AXIS"].data["actual_position"][:, 0],
    name="ACU Actual AZ",
    pen=pen1,
    symbol="o",
    symbolPen=pen1,
    symbolBrush=None,
    symbolSize=size1,
)


# Some config after we add the lines
axis_bottom1 = pg.AxisItem(orientation="bottom", text="time_mjd")
axis_bottom1.showLabel(True)

axis_left1 = pg.AxisItem(orientation="left", text="AZ Actual Position")
axis_left1.showLabel(True)

p1.setAxisItems(
    axisItems={
        "bottom": axis_bottom1,
        "left": axis_left1,
    }
)

p1.showGrid(True, True, alpha=grid_opacity)

# Create the PlotItem
p2 = pg.PlotItem(title="Elevation")
win1.addItem(p2, 0, 1, 1, 1)

# Some config before we add the lines
# p1.setAspectLocked(True)
p2.addLegend()

# EL Actual Position from ACU
p2.plot(
    hdul["MONITOR-ANT-GENERAL"].data["actual_time"],
    hdul["MONITOR-ANT-AXIS"].data["actual_position"][:, 1],
    name="ACU Actual EL",
    pen=pen1,
    symbol="o",
    symbolPen=pen1,
    symbolBrush=None,
    symbolSize=size1,
)


# Some config after we add the lines
axis_bottom2 = pg.AxisItem(orientation="bottom", text="time_mjd")
axis_bottom2.showLabel(True)

axis_left2 = pg.AxisItem(orientation="left", text="EL Actual Position")
axis_left2.showLabel(True)

p2.setAxisItems(
    axisItems={
        "bottom": axis_bottom2,
        "left": axis_left2,
    }
)

p2.showGrid(True, True, alpha=grid_opacity)


# Create the PlotItem
p3 = pg.PlotItem(title="LONG OFF")
win1.addItem(p3, 1, 0, 1, 1)

# Some config before we add the lines
# p1.setAspectLocked(True)
p3.addLegend()

if hdul[DATAPAR_SUBSCAN_HORIZONTAL].header["NLNGTYPE"] == "ALON-GLS":
    long_off_projected = long_off_acu * np.cos(np.radians(actual_position_el))
    long_off_label = "ACU offset from tracking AZ * cos(EL)"
else:
    long_off_projected = long_off_acu
    long_off_label = "ACU offset from tracking AZ"

# LONG OFF calculated from ACU
p3.plot(
    hdul["MONITOR-ANT-GENERAL"].data["actual_time"],
    long_off_projected,
    name=long_off_label,
    pen=pen1,
    symbol="o",
    symbolPen=pen1,
    symbolBrush=None,
    symbolSize=size1
)


# Some config after we add the lines
axis_bottom3 = pg.AxisItem(orientation="bottom", text="time_mjd")
axis_bottom3.showLabel(True)

axis_left3 = pg.AxisItem(orientation="left", text="AZ Offset from Tracking")
axis_left3.showLabel(True)

p3.setAxisItems(
    axisItems={
        "bottom": axis_bottom3,
        "left": axis_left3,
    }
)

p3.showGrid(True, True, alpha=grid_opacity)

# Create the PlotItem
p4 = pg.PlotItem(title="LAT OFF")
win1.addItem(p4, 1, 1, 1, 1)

# Some config before we add the lines
# p1.setAspectLocked(True)
p4.addLegend()

line31 = p4.plot(
    hdul["MONITOR-ANT-GENERAL"].data["actual_time"],
    lat_off_acu,
    name = "ACU offset from tracking EL",
    pen=pen1,
    symbol="o",
    symbolPen=pen1,
    symbolBrush=None,
    symbolSize=size1,
)

for i in range(NUMBER_OF_SUBSCAN):

    timestamps_spe = hdul[i *2 +4].data["MJD"]
    # AZ Encoder after interpolated to spectrum timestamp
    p1.plot(
        hdul[i * 2 + 4].data["MJD"],
        hdul[i * 2 + 4].data["AZIMUTH"],
        name="Subscan {} DATAPAR[AZIMUTH]".format(i+1),
        pen=None,
        symbol="o",
        symbolPen=pen2,
        symbolBrush=brush2,
        symbolSize=size2,
    )

    p1.plot(
        hdul[i * 2 + 4].data["MJD"],
        interp_function_acu_az(timestamps_spe),
        name="Subscan {} interp1d again".format(i+1),
        pen=None,
        symbol="x",
        symbolPen=pen3,
        symbolBrush=brush3,
        symbolSize=size3,
    )

    # EL Encoder after interpolated to spectrum timestamp
    p2.plot(
        hdul[i * 2 + 4].data["MJD"],
        hdul[i * 2 + 4].data["ELEVATIO"],
        name="Subscan {} DATAPAR[ELEVATIO]".format(i+1),
        pen=None,
        symbol="o",
        symbolPen=pen2,
        symbolBrush=brush2,
        symbolSize=size2,
    )

    p2.plot(
        hdul[i * 2 + 4].data["MJD"],
        interp_function_acu_el(timestamps_spe),
        name="Subscan {} interp1d again".format(i+1),
        pen=None,
        symbol="x",
        symbolPen=pen3,
        symbolBrush=brush3,
    )

    # LONG OFF after interpolated to spectrum timestamp
    p3.plot(
        hdul[i * 2 + 4].data["MJD"],
        hdul[i * 2 + 4].data["LONGOFF"],
        name="Subscan {} DATAPAR[LONGOFF]".format(i+1),
        pen=None,
        symbol="o",
        symbolPen=pen2,
        symbolBrush=brush2,
        symbolSize=size2,
    )

    p3.plot(
        hdul[i * 2 + 4].data["MJD"],
        interp_function_acu_long_off(timestamps_spe)*  np.cos(np.radians(interp_function_acu_el(timestamps_spe))),
        name="Subscan {} interp1d again".format(i+1),
        pen=None,
        symbol="x",
        symbolPen=pen3,
        symbolBrush=brush3,
        symbolSize=size3,
    )

    # LAT OFF after interpolated to spectrum timestamp
    p4.plot(
        # timestamps_spe,
        # el_at_timestamps_spectrometer,
        hdul[i * 2 + 4].data["MJD"],
        hdul[i * 2 + 4].data["LATOFF"],
        name="Subscan {} DATAPAR[LATOFF]".format(i+1),
        pen=None,
        symbol="o",
        symbolPen=pen2,
        symbolBrush=brush2,
        symbolSize=size2,
    )

    
    p4.plot(
        hdul[i * 2 + 4].data["MJD"],
        interp_function_acu_lat_off(timestamps_spe),
        name="Subscan {} interp1d again".format(i+1),
        pen=None,
        symbol="x",
        symbolPen=pen3,
        symbolBrush=brush3,
        symbolSize=size3,
    )

    

# Some config after we add the lines
axis_bottom4 = pg.AxisItem(orientation="bottom", text="time_mjd")
axis_bottom4.showLabel(True)

axis_left4 = pg.AxisItem(orientation="left", text="EL Offset from Tracking")
axis_left4.showLabel(True)

p4.setAxisItems(
    axisItems={
        "bottom": axis_bottom4,
        "left": axis_left4,
    }
)

p4.showGrid(True, True, alpha=grid_opacity)


app.exec_()

fid.close()
