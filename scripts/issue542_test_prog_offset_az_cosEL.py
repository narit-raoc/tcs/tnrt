# Minimum schedule parameters
project_id = 'no_projid'
obs_id = 'no_obsid'

# Scantype Parameters
arm_length = 7200
time_per_arm = 30

# Tracking
az = 180
el = 60
tracking_offset_az = 0
tracking_offset_el = 0

# Parameter Blocks
schedule_params = ScheduleParams(project_id, obs_id)
scantype_params = ScantypeParamsCross(arm_length, time_per_arm)
tracking_params = TrackingParamsHO(az, el, tracking_offset_az, tracking_offset_el)

clear_pmodel()
clear_axis_position_offsets()
clear_refraction()

# [2] Huge pointing model
# -------------------
set_pmodel(0, 0, 0, 0, 0, 0, 36000, 0, 0)

add_scan(schedule_params, scantype_params, tracking_params)
results = run_queue()
log('results for set_pmodel(0, 0, 0, 0, 0, 0, 36000, 0, 0): {}'.format(results))
