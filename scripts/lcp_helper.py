# Spiro Sarris.
# 03/2022
# 10/2022

# Standard python modules
import logging
import threading

# Modules installed from pip
import numpy as np
from astropy.time import Time
from astropy.utils import iers
from astropy import units
from astropy.coordinates import Longitude
from astropy.coordinates import Latitude
from astropy.coordinates import Angle
from astropy.units import Quantity

try:
    # For Python < 3.7, import erfa from astropy
    from astropy import _erfa as erfa
except:
    # Python >= 3.7, import erfa from pyerfa package
    # https://pypi.org/project/pyerfa/
    import erfa

from PyQt5 import QtCore
from PyQt5 import QtGui
from PyQt5 import QtWidgets
from PyQt5.QtCore import QState
from PyQt5.QtCore import QStateMachine
from PyQt5.QtCore import QMutex

dark_theme = QtGui.QPalette()

# Color group alias (to keep lines of code short)
dis = QtGui.QPalette.Disabled
act = QtGui.QPalette.Active
inac = QtGui.QPalette.Inactive

txt_flags = QtCore.Qt.TextSelectableByMouse

bg_lightgray = 35
bg_gray = 60
# Central roles.  If ColorGroup is not not specified, default is QtGui.QPalette.Active
dark_theme.setColor(act, QtGui.QPalette.Window, QtGui.QColor(bg_lightgray, bg_lightgray, bg_lightgray))
dark_theme.setColor(act, QtGui.QPalette.WindowText, QtGui.QColor(255, 255, 255))
dark_theme.setColor(act, QtGui.QPalette.Base, QtGui.QColor(bg_gray, bg_gray, bg_gray))
dark_theme.setColor(act, QtGui.QPalette.AlternateBase, QtGui.QColor(66, 66, 66))
dark_theme.setColor(act, QtGui.QPalette.ToolTipBase, QtGui.QColor(53, 53, 53))
dark_theme.setColor(act, QtGui.QPalette.ToolTipText, QtGui.QColor(180, 180, 180))
# dark_theme.setColor(QtGui.QPalette.PlaceHolderText, QtGui.QColor(180, 180, 180))
dark_theme.setColor(act, QtGui.QPalette.Text, QtGui.QColor(255, 255, 255))
dark_theme.setColor(act, QtGui.QPalette.Button, QtGui.QColor(bg_gray, bg_gray, bg_gray))
dark_theme.setColor(act, QtGui.QPalette.ButtonText, QtGui.QColor(255, 255, 255))
dark_theme.setColor(act, QtGui.QPalette.BrightText, QtGui.QColor(255, 255, 255))

# Colors for 3D rendered edges of buttons, scrollbars, ...
dark_theme.setColor(act, QtGui.QPalette.Light, QtGui.QColor(180, 180, 180))
dark_theme.setColor(act, QtGui.QPalette.Midlight, QtGui.QColor(90, 90, 90))
dark_theme.setColor(act, QtGui.QPalette.Dark, QtGui.QColor(35, 35, 35))
dark_theme.setColor(act, QtGui.QPalette.Mid, QtGui.QColor(35, 35, 35))
dark_theme.setColor(act, QtGui.QPalette.Shadow, QtGui.QColor(50, 50, 50))

# Selected items
dark_theme.setColor(act, QtGui.QPalette.Highlight, QtGui.QColor(42, 130, 218))
dark_theme.setColor(act, QtGui.QPalette.HighlightedText, QtGui.QColor(180, 180, 180))

# Hyperlink  / URL
dark_theme.setColor(act, QtGui.QPalette.Link, QtGui.QColor(56, 252, 196))
dark_theme.setColor(act, QtGui.QPalette.LinkVisited, QtGui.QColor(56, 252, 196))

# For widgets that don't have a color role assigned
dark_theme.setColor(act, QtGui.QPalette.NoRole, QtGui.QColor(66, 66, 66))

# Colors for disabled items in the current focus window (QtGui.QPalette.Disabled)
dark_theme.setColor(dis, QtGui.QPalette.WindowText, QtGui.QColor(127, 127, 127))
dark_theme.setColor(dis, QtGui.QPalette.Text, QtGui.QColor(127, 127, 127))
dark_theme.setColor(dis, QtGui.QPalette.ButtonText, QtGui.QColor(127, 127, 127))
dark_theme.setColor(dis, QtGui.QPalette.Highlight, QtGui.QColor(80, 80, 80))
dark_theme.setColor(dis, QtGui.QPalette.HighlightedText, QtGui.QColor(127, 127, 127))


def configure_logger(logger):
    logger.setLevel(logging.INFO)
    fmt_scrn = (
        "%(asctime)s [%(levelname)s] %(module)s.%(name)s.%(funcName)s(): %(message)s"
    )
    formatter_screen = logging.Formatter(fmt=fmt_scrn)
    handler_screen = logging.StreamHandler()
    handler_screen.setFormatter(formatter_screen)
    handler_screen.setLevel(logging.DEBUG)
    logger.addHandler(handler_screen)


class MainWindow(QtWidgets.QMainWindow):
    """ """

    def __init__(self, window_label):
        """
        Create MainWindow QtGui
        """
        self.logger = logging.getLogger(self.__class__.__name__)
        configure_logger(self.logger)
        self.logger.info("Started log for {}".format(self.logger.name))

        super(MainWindow, self).__init__()

        self.setup_widgets(window_label)
        self.setup_state_machine()

        self.iers_table = iers.IERS_Auto.open()

        update_interval_ms = 1000

        # Create a timer to run always - for UTC time and EOP updates
        self.timer_utc_eop = QtCore.QTimer()
        self.timer_utc_eop.setInterval(update_interval_ms)
        self.timer_utc_eop.timeout.connect(self.update_time_data)
        self.timer_utc_eop.start()

        # Get time and EOP now (not wait 1 second until the timer_utc_eop updates.)
        self.update_time_data()

    def update_results(self):
        # If user has already clicked the Calculate button, this is true
        try:
            self.get_user_input()
            self.update_dut_and_gst_for_acu()
            self.icrs_to_geocentric_apparent_iau()
            self.icrs_to_altaz_iau()
        except Exception as e:
            msg = "{}: {}".format(e.__class__.__name__, e)
            self.logger.debug(msg)
            self.label_result_status_value.setStyleSheet(
                "color: red; font-weight: bold"
            )
            self.label_result_status_value.setText("ERROR")
            self.label_result_message_value.setText(msg)
            self.pb_stop_calculator.clicked.emit()
            return

        self.label_result_status_value.setStyleSheet("color: green; font-weight: bold")
        self.label_result_status_value.setText("OK")
        self.label_result_message_value.setText("")

    def update_time_data(self):
        self.time_now = Time.now()
        self.time_now_ut1 = self.time_now.ut1
        self.dut1 = float(self.time_now.delta_ut1_utc)
        (self.polar_motion_x, self.polar_motion_y) = self.iers_table.pm_xy(
            self.time_now.jd
        )  # arcsec

        self.date1 = self.time_now.jd1  # UTC as a 2-part. . .
        self.date2 = self.time_now.jd2  # . . . quasi Julian Date (Notes 3,4)

        self.label_time_utc_value.setText(self.time_now.utc.iso)
        lst_hms = self.time_now.sidereal_time(
            "apparent", longitude=Longitude(float(self.input_lon.text()) * units.deg)
        ).hms
        self.label_lst_value.setText(
            "{:02d}:{:02d}:{:012.9f}".format(
                int(lst_hms[0]), int(lst_hms[1]), lst_hms[2]
            )
        )

        self.label_time_ut1_value.setText(self.time_now_ut1.iso)

        self.label_dut1_value.setText(str(self.dut1))
        self.label_polar_motion_x_value.setText(str(self.polar_motion_x.value))
        self.label_polar_motion_y_value.setText(str(self.polar_motion_y.value))

        # If our RUNNING state is in the set of active states of the QStateMachine, update results.
        if self.state_running in self.state_machine.configuration():
            self.update_results()

    def update_dut_and_gst_for_acu(self):

        # Get GAST (Greenwich Apparent Sidereal Time) at 0h UT on the date of the observation.  Used for ACU tracking
        self.logger.debug("Time UT1: {}".format(self.time_now))

        self.label_result_time_utc_value.setText(self.time_now.utc.iso)
        # Get integer and fractional part of the UT1 day.
        # Note.
        # MJD increments the day number +1 at midnight (0h)
        # JD increments the day number at noon (12h)
        time_ut1_integer_day = int(self.time_now_ut1.mjd)
        time_ut1_fractional_day = self.time_now_ut1.mjd % 1

        dut1_ms = int(np.round(1000 * self.dut1))
        self.label_dut1_ms_value.setText("{:d}".format(dut1_ms))

        self.logger.debug(
            "mjd (integer day), mjd (fractional day): ({},{})".format(
                time_ut1_integer_day, time_ut1_fractional_day
            )
        )

        if time_ut1_fractional_day < 0:
            self.logger.logWarning("fractional day < 0.  Subtract 1 integer day")
            time_ut1_integer_day = time_ut1_integer_day - 1
            self.logger.logWarning("integer day: {}".format(time_ut1_integer_day))

        # Create an astropy Time object of fractional day on UT1 time scale
        startday_0hUT1 = Time(time_ut1_integer_day, format="mjd", scale="ut1")
        GST0hUT1 = startday_0hUT1.sidereal_time("apparent", longitude="greenwich")

        # round to the nearest integer millisecond (ACU accepts integer, not float)
        GST0hUT1_ms = int(np.round(1000 * 86400 * (GST0hUT1.hourangle / 24.0)))

        self.label_gst_ms_value.setText("{:d}".format(GST0hUT1_ms))

    def get_user_input(self):
        # Check the state of radio button, fill all the boxes before we read data from deg inputs
        if self.radio_button_deg.isChecked():
            self.logger.debug("DEG selected. Calculate HMS, DMS for info")
            rc_apy = Longitude(float(self.input_ra_icrs_deg.text()) * units.deg)
            dc_apy = Latitude(float(self.input_de_icrs_deg.text()) * units.deg)

            self.input_ra_icrs_hms.setText(rc_apy.to_string(unit=units.hour))
            self.input_de_icrs_dms.setText(dc_apy.to_string(unit=units.deg))

        elif self.radio_button_hdms.isChecked():
            self.logger.debug("HMS, DMS selected. Calculate Deg and fill")
            # Astropy Longitude can accept string in hms string format('23h59m59s')
            rc_apy = Longitude(self.input_ra_icrs_hms.text())
            # Astropy Latitude can accept string in dms string format ('89d59m59s')
            dc_apy = Latitude(self.input_de_icrs_dms.text())
            self.logger.debug(
                "read ra: {} hms, dec: {} dms".format(rc_apy.hms, dc_apy.dms)
            )
            self.logger.debug(
                "set ra, dec degrees to: {}, {}".format(rc_apy.deg, dc_apy.deg)
            )
            self.input_ra_icrs_deg.setText(str(rc_apy.deg))
            self.input_de_icrs_deg.setText(str(dc_apy.deg))

        # Get data from strings in GUI text input.  Create smart astropy objects to simplify unit conversion
        rc_apy = Longitude(float(self.input_ra_icrs_deg.text()) * units.deg)
        dc_apy = Latitude(float(self.input_de_icrs_deg.text()) * units.deg)

        # Proper motion implied arcsec per year, but use only unit arcsec to make conversion to radian easier
        pr_apy = Angle(float(self.input_pm_ra.text()) * units.arcsec)
        pd_apy = Angle(float(self.input_pm_de.text()) * units.arcsec)

        px_apy = Angle(float(self.input_px.text()) * units.arcsec)
        rv_apy = Quantity(float(self.input_rv.text()) * units.km / units.s)

        lat_apy = Latitude(float(self.input_lat.text()) * units.deg)
        lon_apy = Longitude(float(self.input_lon.text()) * units.deg)
        height_apy = Quantity(float(self.input_height.text()) * units.m)

        self.logger.debug(
            "rc_apy: {}, dc_apy: {}, pr_apy: {}, pd_apy: {}, px_apy: {}, rv_apy: {}".format(
                rc_apy, dc_apy, pr_apy, pd_apy, px_apy, rv_apy
            )
        )

        # Convert units to use wth IAU models and get the raw value from the "smart" Astropy objects.
        self.rc = rc_apy.radian  # ICRS [ α, δ ] at J2000.0 (radians, Note 1)
        self.dc = dc_apy.radian  # RA proper motion (radians/year; Note 2)
        self.pr = pr_apy.radian  # RA proper motion (radians/year; Note 2)
        self.pd = pd_apy.radian  # Dec proper motion (radians/year)
        self.px = px_apy.arcsec  # parallax (arcsec)
        self.rv = rv_apy.value  # radial velocity (km/s, positive if receding)

        self.logger.debug(
            "rc: {}, dc: {}, pr: {}, pd: {}, px: {}, rv: {}".format(
                self.rc,
                self.dc,
                self.pr,
                self.pd,
                self.px,
                self.rv,
            )
        )

        self.logger.debug(
            "lat_apy: {}, lon_apy: {}, height_apy: {}".format(
                lat_apy, lon_apy, height_apy
            )
        )
        self.elong = lon_apy.radian  # longitude (radians, east-positive, Note 6)
        self.phi = lat_apy.radian  # geodetic latitude (radians, Note 6)
        self.hm = height_apy.value  # height above ellipsoid (m, geodetic Notes 6,8)

        self.logger.debug(
            "phi: {}, elong: {}, hm: {}".format(self.phi, self.elong, self.hm)
        )

    def icrs_to_geocentric_apparent_iau(self):
        """
        Follow this example to get geocentric apparent RA, DEC coordinates
        Follow example in document http://www.iausofa.org/2019_0722_C/sofa/sofa_ast_c.pdf
        """

        (ri, di, eo) = erfa.atci13(
            self.rc, self.dc, self.pr, self.pd, self.px, self.rv, self.date1, self.date2
        )

        # Convert scalar result to Quantity object with units
        # Rotate the right ascension from ERA (Earth Rotation Angle) origin to
        # GST (Greenwich Sidereal Time) origin using EO (Equation of the Origins)
        # erfa.anp wraps angle to value within range [0 .. 2 * pi]
        ra_geocentric_apparent = Longitude((erfa.anp(ri - eo)) * units.radian)
        de_geocentric_apparent = Latitude(di * units.radian)

        # Convert to degrees and return data to use as input to ACU tracking generator
        self.logger.debug(
            "ri: {}, di: {}".format(
                ra_geocentric_apparent.deg, de_geocentric_apparent.deg
            )
        )

        self.label_ra_apparent_deg_value.setText(str(ra_geocentric_apparent.deg))
        self.label_de_apparent_deg_value.setText(str(de_geocentric_apparent.deg))

        # Get result in HMS, DMS format
        ra_hms = ra_geocentric_apparent.hms
        de_dms = de_geocentric_apparent.dms

        self.label_ra_apparent_hms_value.setText(
            "{:02d}:{:02d}:{:012.9f}".format(int(ra_hms[0]), int(ra_hms[1]), ra_hms[2])
        )
        self.label_de_apparent_dms_value.setText(
            "{:02d}:{:02d}:{:012.9f}".format(int(de_dms[0]), int(de_dms[1]), de_dms[2])
        )

    def icrs_to_altaz_iau(self):
        """
        Convert to AltAz observed coordinates using SOFA model
        """

        # # polar motion coordinates (radians, Note 7)
        xp = (np.pi / 180) * (self.polar_motion_x.value / 3600)
        yp = (np.pi / 180) * (self.polar_motion_y.value / 3600)

        phpa = 0  # pressure at the observer (hPa ≡ mB, Note 8
        tc = 0  # ambient temperature at the observer (deg C)
        rh = 0  # relative humidity at the observer (range 0-1)
        wl = 1000  # wavelength (micrometers, Note 9). no effect if other refraction params are 0

        # Use IAU SOFA model to transform to AltAz observed
        # function name tco13 -> [transform][catalog] to [observed] model [2013]
        # Returns
        # -------
        # aob   observed azimuth (radians: N=0 ◦ , E=90 ◦ )
        # zob   observed zenith distance (radians)
        # hob   observed hour angle (radians)
        # dob   observed declination (radians)
        # rob   observed right ascension (CIO-based, radians)
        # eo    equation of the origins (ERA−GST)

        (aob, zob, hob, dob, rob, eo) = erfa.atco13(
            self.rc,
            self.dc,
            self.pr,
            self.pd,
            self.px,
            self.rv,
            self.date1,
            self.date2,
            self.dut1,
            self.elong,
            self.phi,
            self.hm,
            xp,
            yp,
            phpa,
            tc,
            rh,
            wl,
        )

        az = (aob * units.radian).to(units.deg).value

        # Result from IAU SOFA model is Zenith angle.  We want Alt/EL
        el = ((90 * units.deg) - (zob * units.radian).to(units.deg)).value

        self.label_az_value.setText(str(az))
        self.label_el_value.setText(str(el))

    def shutdown(self):
        self.logger.debug("nothing to do here")

    def enable_radec_deg(self):
        self.input_ra_icrs_deg.setEnabled(True)
        self.input_de_icrs_deg.setEnabled(True)
        self.input_ra_icrs_deg.setText("0.0")
        self.input_de_icrs_deg.setText("0.0")
        self.input_ra_icrs_hms.setEnabled(False)
        self.input_de_icrs_dms.setEnabled(False)
        self.input_ra_icrs_hms.setText("")
        self.input_de_icrs_dms.setText("")

    def enable_radec_hdms(self):
        self.input_ra_icrs_deg.setEnabled(False)
        self.input_de_icrs_deg.setEnabled(False)
        self.input_ra_icrs_deg.setText("")
        self.input_de_icrs_deg.setText("")
        self.input_ra_icrs_hms.setEnabled(True)
        self.input_de_icrs_dms.setEnabled(True)
        self.input_ra_icrs_hms.setText("00h00m0.000s")
        self.input_de_icrs_dms.setText("00d00m0.000s")

    def setup_widgets(self, window_label):
        self.resize(1200, 600)
        self.setWindowTitle(window_label)

        self.central_widget = QtWidgets.QWidget()
        self.setCentralWidget(self.central_widget)

        self.layout_left = QtWidgets.QVBoxLayout()
        self.layout_left.setAlignment(QtCore.Qt.AlignTop)

        self.layout_right = QtWidgets.QVBoxLayout()
        self.layout_right.setAlignment(QtCore.Qt.AlignTop)

        self.layout_main = QtWidgets.QHBoxLayout()
        self.central_widget.setLayout(self.layout_main)

        # ------------------------------------------
        # Time
        # ------------------------------------------
        self.group_time = QtWidgets.QGroupBox("Time (from local OS)")
        self.layout_grid_time = QtWidgets.QGridLayout()
        self.group_time.setLayout(self.layout_grid_time)

        self.label_time_utc_name = QtWidgets.QLabel("Time UTC [ISO 8601]")
        self.label_time_utc_value = QtWidgets.QLabel("")
        self.label_time_utc_value.setTextInteractionFlags(txt_flags)

        self.label_lst_name = QtWidgets.QLabel("Local Sidereal Time")
        self.label_lst_value = QtWidgets.QLabel("")
        self.label_lst_value.setTextInteractionFlags(txt_flags)

        self.layout_grid_time.addWidget(self.label_time_utc_name, 0, 0)
        self.layout_grid_time.addWidget(self.label_time_utc_value, 0, 1)
        self.layout_grid_time.addWidget(self.label_lst_name, 1, 0)
        self.layout_grid_time.addWidget(self.label_lst_value, 1, 1)

        # ------------------------------------------
        # EOP
        # ------------------------------------------
        self.group_eop = QtWidgets.QGroupBox(
            "Earth Orientation Parameters (EOP from IERS online)"
        )
        self.layout_grid_eop = QtWidgets.QGridLayout()
        self.group_eop.setLayout(self.layout_grid_eop)

        self.label_time_ut1_name = QtWidgets.QLabel("Time UT1 [ISO 8601]")
        self.label_time_ut1_value = QtWidgets.QLabel("")

        self.label_dut1_name = QtWidgets.QLabel("DUT1 (UT1-UTC) [s]")
        self.label_dut1_value = QtWidgets.QLabel("")

        self.label_polar_motion_x_name = QtWidgets.QLabel("Polar Motion X [arcsec]")
        self.label_polar_motion_x_value = QtWidgets.QLabel("")

        self.label_polar_motion_y_name = QtWidgets.QLabel("Polar Motion Y [arcsec]")
        self.label_polar_motion_y_value = QtWidgets.QLabel("")

        self.layout_grid_eop.addWidget(self.label_time_ut1_name, 0, 0)
        self.layout_grid_eop.addWidget(self.label_time_ut1_value, 0, 1)
        self.layout_grid_eop.addWidget(self.label_dut1_name, 1, 0)
        self.layout_grid_eop.addWidget(self.label_dut1_value, 1, 1)
        self.layout_grid_eop.addWidget(self.label_polar_motion_x_name, 2, 0)
        self.layout_grid_eop.addWidget(self.label_polar_motion_x_value, 2, 1)
        self.layout_grid_eop.addWidget(self.label_polar_motion_y_name, 3, 0)
        self.layout_grid_eop.addWidget(self.label_polar_motion_y_value, 3, 1)

        # ------------------------------------------
        # Earth Location
        # ------------------------------------------
        self.group_location_in = QtWidgets.QGroupBox("Earth Location")
        self.layout_grid_location_in = QtWidgets.QGridLayout()
        self.group_location_in.setLayout(self.layout_grid_location_in)

        self.label_lat = QtWidgets.QLabel("Latitude [deg]")
        self.label_lon = QtWidgets.QLabel("Longitude [deg]")
        self.label_height = QtWidgets.QLabel("Height [m, WGS84]")

        self.input_lat = QtWidgets.QLineEdit("18.864348")
        self.input_lon = QtWidgets.QLineEdit("99.216805")
        self.input_height = QtWidgets.QLineEdit("403.625")

        self.layout_grid_location_in.addWidget(self.label_lat, 0, 0)
        self.layout_grid_location_in.addWidget(self.input_lat, 0, 1)
        self.layout_grid_location_in.addWidget(self.label_lon, 1, 0)
        self.layout_grid_location_in.addWidget(self.input_lon, 1, 1)
        self.layout_grid_location_in.addWidget(self.label_height, 2, 0)
        self.layout_grid_location_in.addWidget(self.input_height, 2, 1)

        # ------------------------------------------
        # Catalog Input
        # ------------------------------------------
        self.group_radec_in = QtWidgets.QGroupBox("Object Catalog Input")
        self.layout_grid_radec_in = QtWidgets.QGridLayout()
        self.group_radec_in.setLayout(self.layout_grid_radec_in)

        self.radio_button_group_radec = QtWidgets.QButtonGroup()
        self.radio_button_deg = QtWidgets.QRadioButton("deg, deg")
        self.radio_button_hdms = QtWidgets.QRadioButton("hms, dms")
        self.radio_button_group_radec.addButton(self.radio_button_deg)
        self.radio_button_group_radec.addButton(self.radio_button_hdms)

        self.label_radec_input_format = QtWidgets.QLabel("{Ra, Dec} Input format")
        self.label_ra_icrs_deg = QtWidgets.QLabel("RA ICRS at J2000 [0.0 to 360.0]")
        self.label_de_icrs_deg = QtWidgets.QLabel("DEC ICRS at J2000 [-90.0 to 90.0]")
        self.label_ra_icrs_hms = QtWidgets.QLabel(
            "RA ICRS at J2000 [0h0m0s to 24h0m0.0s]"
        )
        self.label_de_icrs_dms = QtWidgets.QLabel(
            "DEC ICRS at J2000 [-90d0m0s to 90d0m0.0s]"
        )
        self.label_pm_ra = QtWidgets.QLabel("Proper Motion RA [arcsec/year]")
        self.label_pm_de = QtWidgets.QLabel("Proper Motion DEC [arcsec/year]")
        self.label_px = QtWidgets.QLabel("Parallax (arcsec)")
        self.label_rv = QtWidgets.QLabel(
            "Radial Velocity [km/s] (positive if receding)"
        )

        self.input_ra_icrs_deg = QtWidgets.QLineEdit("0")
        self.input_de_icrs_deg = QtWidgets.QLineEdit("0")
        self.input_ra_icrs_hms = QtWidgets.QLineEdit("0")
        self.input_de_icrs_dms = QtWidgets.QLineEdit("0")
        self.input_pm_ra = QtWidgets.QLineEdit("0")
        self.input_pm_de = QtWidgets.QLineEdit("0")
        self.input_px = QtWidgets.QLineEdit("0")
        self.input_rv = QtWidgets.QLineEdit("0")

        self.layout_grid_radec_in.addWidget(self.label_radec_input_format, 0, 0)
        self.layout_grid_radec_in.addWidget(self.radio_button_deg, 0, 1)
        self.layout_grid_radec_in.addWidget(self.radio_button_hdms, 1, 1)
        self.layout_grid_radec_in.addWidget(self.label_ra_icrs_deg, 2, 0)
        self.layout_grid_radec_in.addWidget(self.input_ra_icrs_deg, 2, 1)
        self.layout_grid_radec_in.addWidget(self.label_de_icrs_deg, 3, 0)
        self.layout_grid_radec_in.addWidget(self.input_de_icrs_deg, 3, 1)
        self.layout_grid_radec_in.addWidget(self.label_ra_icrs_hms, 4, 0)
        self.layout_grid_radec_in.addWidget(self.input_ra_icrs_hms, 4, 1)
        self.layout_grid_radec_in.addWidget(self.label_de_icrs_dms, 5, 0)
        self.layout_grid_radec_in.addWidget(self.input_de_icrs_dms, 5, 1)
        self.layout_grid_radec_in.addWidget(self.label_pm_ra, 6, 0)
        self.layout_grid_radec_in.addWidget(self.input_pm_ra, 6, 1)
        self.layout_grid_radec_in.addWidget(self.label_pm_de, 7, 0)
        self.layout_grid_radec_in.addWidget(self.input_pm_de, 7, 1)
        self.layout_grid_radec_in.addWidget(self.label_px, 8, 0)
        self.layout_grid_radec_in.addWidget(self.input_px, 8, 1)
        self.layout_grid_radec_in.addWidget(self.label_rv, 9, 0)
        self.layout_grid_radec_in.addWidget(self.input_rv, 9, 1)

        self.radio_button_deg.setChecked(True)
        self.enable_radec_deg()
        self.radio_button_deg.toggled.connect(self.enable_radec_deg)
        self.radio_button_hdms.toggled.connect(self.enable_radec_hdms)

        self.pb_run_calculator = QtWidgets.QPushButton("Run")
        self.pb_stop_calculator = QtWidgets.QPushButton("Stop")

        # ------------------------------------------
        # Results Time
        # ------------------------------------------
        self.group_result_time = QtWidgets.QGroupBox("Results: Time of Calculation")
        self.layout_grid_result_time = QtWidgets.QGridLayout()
        self.group_result_time.setLayout(self.layout_grid_result_time)

        self.label_result_time_utc_name = QtWidgets.QLabel("Time UTC [ISO 8601]")
        self.label_result_time_utc_value = QtWidgets.QLabel("")
        self.label_result_time_utc_value.setTextInteractionFlags(txt_flags)

        self.layout_grid_result_time.addWidget(self.label_result_time_utc_name, 0, 0)
        self.layout_grid_result_time.addWidget(self.label_result_time_utc_value, 0, 1)

        # ------------------------------------------
        # Results RA, DEC
        # ------------------------------------------
        self.group_result_radec = QtWidgets.QGroupBox("Results: Apparent RA, DEC")
        self.layout_grid_result_radec = QtWidgets.QGridLayout()
        self.group_result_radec.setLayout(self.layout_grid_result_radec)

        self.label_ra_apparent_deg_name = QtWidgets.QLabel("RA Apparent [deg]")
        self.label_ra_apparent_hms_name = QtWidgets.QLabel("RA Apparent [hms]")

        self.label_de_apparent_deg_name = QtWidgets.QLabel("Dec Apparent [deg]")
        self.label_de_apparent_dms_name = QtWidgets.QLabel("Dec Apparent [dms]")

        self.label_ra_apparent_deg_value = QtWidgets.QLabel("")
        self.label_ra_apparent_hms_value = QtWidgets.QLabel("")

        self.label_de_apparent_deg_value = QtWidgets.QLabel("")
        self.label_de_apparent_dms_value = QtWidgets.QLabel("")

        self.label_ra_apparent_deg_value.setTextInteractionFlags(txt_flags)
        self.label_de_apparent_deg_value.setTextInteractionFlags(txt_flags)
        self.label_ra_apparent_hms_value.setTextInteractionFlags(txt_flags)
        self.label_de_apparent_dms_value.setTextInteractionFlags(txt_flags)

        self.layout_grid_result_radec.addWidget(self.label_ra_apparent_deg_name, 0, 0)
        self.layout_grid_result_radec.addWidget(self.label_ra_apparent_deg_value, 0, 1)

        self.layout_grid_result_radec.addWidget(self.label_de_apparent_deg_name, 1, 0)
        self.layout_grid_result_radec.addWidget(self.label_de_apparent_deg_value, 1, 1)

        self.layout_grid_result_radec.addWidget(self.label_ra_apparent_hms_name, 2, 0)
        self.layout_grid_result_radec.addWidget(self.label_ra_apparent_hms_value, 2, 1)

        self.layout_grid_result_radec.addWidget(self.label_de_apparent_dms_name, 3, 0)
        self.layout_grid_result_radec.addWidget(self.label_de_apparent_dms_value, 3, 1)

        # ------------------------------------------
        # Results DUT1 and Sidereal Time for ACU
        # ------------------------------------------
        self.group_result_eop = QtWidgets.QGroupBox("Results:  DUT1 and Sidereal Time")
        self.layout_grid_result_eop = QtWidgets.QGridLayout()
        self.group_result_eop.setLayout(self.layout_grid_result_eop)

        self.label_dut1_ms_name = QtWidgets.QLabel("DUT1 (UT1-UTC) [integer ms]")
        self.label_dut1_ms_value = QtWidgets.QLabel("")
        self.label_dut1_ms_value.setTextInteractionFlags(txt_flags)

        self.label_gst_ms_name = QtWidgets.QLabel("GST0hUT1 [integer ms]")
        self.label_gst_ms_value = QtWidgets.QLabel("")
        self.label_gst_ms_value.setTextInteractionFlags(txt_flags)

        self.layout_grid_result_eop.addWidget(self.label_dut1_ms_name, 0, 0)
        self.layout_grid_result_eop.addWidget(self.label_dut1_ms_value, 0, 1)
        self.layout_grid_result_eop.addWidget(self.label_gst_ms_name, 1, 0)
        self.layout_grid_result_eop.addWidget(self.label_gst_ms_value, 1, 1)

        # ------------------------------------------
        # Results AZ, EL
        # ------------------------------------------
        self.group_result_azel = QtWidgets.QGroupBox("Results: AZ, EL Tracking")
        self.layout_grid_result_azel = QtWidgets.QGridLayout()
        self.group_result_azel.setLayout(self.layout_grid_result_azel)

        self.label_az_name = QtWidgets.QLabel("Azimuth [deg]")
        self.label_el_name = QtWidgets.QLabel("Elevation [deg]")

        self.label_az_value = QtWidgets.QLabel("")
        self.label_el_value = QtWidgets.QLabel("")

        self.label_az_value.setTextInteractionFlags(txt_flags)
        self.label_el_value.setTextInteractionFlags(txt_flags)

        self.layout_grid_result_azel.addWidget(self.label_az_name, 0, 0)
        self.layout_grid_result_azel.addWidget(self.label_az_value, 0, 1)
        self.layout_grid_result_azel.addWidget(self.label_el_name, 1, 0)
        self.layout_grid_result_azel.addWidget(self.label_el_value, 1, 1)

        # ------------------------------------------
        # Log / Information
        # ------------------------------------------
        self.group_info = QtWidgets.QGroupBox("Information")
        self.layout_grid_info = QtWidgets.QGridLayout()
        self.group_info.setLayout(self.layout_grid_info)

        self.label_calc_state_name = QtWidgets.QLabel(
            "Calculator State [STOPPED / RUNNING]"
        )
        self.label_calc_state_value = QtWidgets.QLabel("")

        self.label_result_status_name = QtWidgets.QLabel("Result Status [OK / ERROR]")
        self.label_result_status_value = QtWidgets.QLabel("")

        self.label_result_message_value = QtWidgets.QLabel("")
        self.label_result_message_value.setWordWrap(True)

        self.layout_grid_info.addWidget(self.label_calc_state_name, 0, 0)
        self.layout_grid_info.addWidget(self.label_calc_state_value, 0, 1)
        self.layout_grid_info.addWidget(self.label_result_status_name, 1, 0)
        self.layout_grid_info.addWidget(self.label_result_status_value, 1, 1)
        self.layout_grid_info.addWidget(self.label_result_message_value, 2, 0, 1, 2)

        # ------------------------------------------
        # Arrange widgets
        # ------------------------------------------
        self.layout_left.addWidget(self.group_time)
        self.layout_left.addWidget(self.group_eop)
        self.layout_left.addWidget(self.group_location_in)
        self.layout_left.addWidget(self.group_radec_in)

        self.layout_right.addWidget(self.pb_run_calculator)
        self.layout_right.addWidget(self.pb_stop_calculator)

        self.layout_right.addWidget(self.group_result_time)
        self.layout_right.addWidget(self.group_result_radec)
        self.layout_right.addWidget(self.group_result_eop)
        self.layout_right.addWidget(self.group_result_azel)
        self.layout_right.addWidget(self.group_info)

        self.layout_main.addLayout(self.layout_left, 5)
        self.layout_main.addLayout(self.layout_right, 5)

    def setup_state_machine(self):
        # Define the states finite state machine (FSM)
        self.state_machine = QStateMachine()
        self.state_stopped = QState()
        self.state_running = QState()

        # Define transitions
        self.state_stopped.addTransition(
            self.pb_run_calculator.clicked, self.state_running
        )
        self.state_running.addTransition(
            self.pb_stop_calculator.clicked, self.state_stopped
        )

        # Add states to the state machine
        self.state_machine.addState(self.state_stopped)
        self.state_machine.addState(self.state_running)

        # Listen for the signals from state changes and call functions
        self.state_running.entered.connect(self.run_calculator)
        self.state_stopped.entered.connect(self.stop_calculator)

        # Initialize and start
        self.state_machine.setInitialState(self.state_stopped)
        self.state_machine.start()

    def run_calculator(self):
        # Set button states and text for this state
        self.pb_run_calculator.setEnabled(False)
        self.pb_stop_calculator.setEnabled(True)

        self.pb_run_calculator.setText("RUNNING")
        self.pb_stop_calculator.setText("STOP")

        # Update information section
        self.label_calc_state_value.setStyleSheet("color: green; font-weight: bold")
        self.label_calc_state_value.setText("RUNNING")

        # Disable editing of user input while calculator is running
        self.input_lat.setEnabled(False)
        self.input_lon.setEnabled(False)
        self.input_height.setEnabled(False)
        self.input_pm_ra.setEnabled(False)
        self.input_pm_de.setEnabled(False)
        self.input_px.setEnabled(False)
        self.input_rv.setEnabled(False)
        self.radio_button_deg.setEnabled(False)
        self.radio_button_hdms.setEnabled(False)

        self.input_ra_icrs_deg.setEnabled(False)
        self.input_de_icrs_deg.setEnabled(False)
        self.input_ra_icrs_hms.setEnabled(False)
        self.input_de_icrs_dms.setEnabled(False)

        # Update results immediately so we don't have to wait for the next auto calculation
        self.update_results()

    def stop_calculator(self):
        # Set button states and text for this state
        self.pb_run_calculator.setEnabled(True)
        self.pb_stop_calculator.setEnabled(False)

        self.pb_run_calculator.setText("RUN")
        self.pb_stop_calculator.setText("STOPPED")

        # Update information section
        self.label_calc_state_value.setStyleSheet("color: yellow; font-weight: bold")
        self.label_calc_state_value.setText("STOPPED")

        # Enable edit of user input while calculator is stopped
        self.input_lat.setEnabled(True)
        self.input_lon.setEnabled(True)
        self.input_height.setEnabled(True)
        self.input_pm_ra.setEnabled(True)
        self.input_pm_de.setEnabled(True)
        self.input_px.setEnabled(True)
        self.input_rv.setEnabled(True)
        self.radio_button_deg.setEnabled(True)
        self.radio_button_hdms.setEnabled(True)

        if self.radio_button_deg.isChecked():
            self.input_ra_icrs_deg.setEnabled(True)
            self.input_de_icrs_deg.setEnabled(True)
        elif self.radio_button_hdms.isChecked():
            self.input_ra_icrs_hms.setEnabled(True)
            self.input_de_icrs_dms.setEnabled(True)


if __name__ == "__main__":
    # Start logger
    logger = logging.getLogger(__name__)
    configure_logger(logger)
    logger.info("Started log for {}".format(logger.name))

    # Create instance of Qt GUI Application
    app = QtWidgets.QApplication([])

    # Set theme style
    app.setStyle('Fusion')

    # Set colors
    app.setPalette(dark_theme)

    # Create instance of MainWindow.  The class MainWindow inherits QtGui objects
    # from Qt.QMainWindow and from DataMonitorGUI.  It extends the parent classes by
    # adding non-GUI member variables and functions to update GUI.
    window = MainWindow("ACU LCP Helper")

    # Enable the window to show after Qt event loop starts
    window.show()

    # Start Qt Event loop (including open GUI window)
    app.exec_()

    # After GUI window is closed, clean up.
    window.shutdown()
