import argparse
from argparse import RawTextHelpFormatter
import os
import sys
from astropy.io import fits
from astropy.time import Time

import pyqtgraph as pg
from pyqtgraph.Qt import QtGui

pg.setConfigOptions(antialias=True)
# NOTE: Default is dark background and CMY colors. Set white_background=True
# if you want white background and RGB colors.
white_background = False
#white_background = True

if white_background is True:
    pg.setConfigOption("background", "w")
    pg.setConfigOption("foreground", "k")

    # Use RGB color for white background
    color1 = "g"
    color2 = "b"
    color3 = "r"
    color4 = "m"    
else:
    # Use CMY color for dark background
    color1 = "y"
    color2 = "c"
    color3 = "m"
    color4 = "b"
    
size1 = 6
size2 = 12
size3 = 18
size4 = 24

# Setup rules for command line arguments
parser = argparse.ArgumentParser(
    description="Reads FITS file written by PipelineMbfits and diplays some graphs",
    formatter_class=RawTextHelpFormatter,
)
parser.add_argument(
    "filename", help="FITS file with format specified by DataFormatMbfits.py"
)

# If no command line arguments are avaiable, show help message and exit
if len(sys.argv[1:]) == 0:
    parser.print_help()
    exit()

# Read command line arguments into argparse structure
args = parser.parse_args()

hdul = None

if os.path.isfile(args.filename) is not True:
    print("ERROR: File not found")
    print("os.path.isfile(%s) = False" % args.filename)
    hdul = None
else:
    print("Processing file %s: " % args.filename)
    fid = open(args.filename, "rb")
    hdul = fits.open(fid)
    # leave the file open becuase fits.open will not store the entire file in RAM if it is
    # too big.  Close file at the end of this script.
    # print(hdul.info())
if hdul is None:
    exit()

# Strings for labels
str_title_az = "AZIMUTH data from file {}".format(os.path.basename(args.filename))
str_title_el = "ELEVATION data from file {}".format(os.path.basename(args.filename))

# Load Data
AZ = 0
EL = 1

# Initialize the QtGui application before we create any PyQtGraph objects
app = QtGui.QApplication([])
grid_opacity = 0.7

ratio = 4/3
h = 1080
w = h * ratio

# =============================================================================
# Create GraphicsLayoutWidgets (new windows)
# =============================================================================

# Window 1 = Tracking Subsystem contribution to position
#   Contribution is calculated from by user parameters and time of day
layout_tracking = pg.GraphicsLayoutWidget(show=True)
layout_tracking.resize(w, h)
layout_tracking.setWindowTitle("Tracking Subsystem {}".format(os.path.basename(args.filename)))

# Window 2 = Axis subsystem contribution to position
#   Contribution is calculated from user parameters only 
layout_axis = pg.GraphicsLayoutWidget(show=True)
layout_axis.resize(w, h)
layout_axis.setWindowTitle("Axis Subsystem {}".format(os.path.basename(args.filename)))

# Window 3 = Pointing subsystem contribution to position.  
#   Contribution is defined dynamically (in real-time) from user parameters and 
#   real-time position of AZ, EL, GRS axes.  
layout_pointing = pg.GraphicsLayoutWidget(show=True)
layout_pointing.resize(w, h)
layout_pointing.setWindowTitle("Pointing Subsystem {}".format(os.path.basename(args.filename)))

# Window 4 = Position results of AZ and EL calculated from contributions of 
#   Tracking, Axis, and Pointing subsystems.
layout_result = pg.GraphicsLayoutWidget(show=True)
layout_result.resize(w, h)
layout_result.setWindowTitle("Result Position {}".format(os.path.basename(args.filename)))

# =============================================================================
# Create PlotItems ("subplot" widgets to add to GraphicsLayoutWidgets)
# Use PlotItem.plot() to add a PlotDataItem to the PlotItem for each set of data on the same graph axis
# .plot() returns a PlotDataItem that can be kept as a reference for future modification (animation),
# I don't save the reference because the plot data will not be changed in this script.
# =============================================================================

# Define a function to add the axes, grids, labels.  We can  use the same for all graphs
def add_axes(plotitem):
    ax_L = pg.AxisItem(orientation="left", units="deg")
    ax_B = pg.DateAxisItem(orientation="bottom", utcOffset=0,text="UTC")
    ax_L.showLabel(True)
    #ax_B.showLabel(True)
    plotitem.setAxisItems(axisItems={"bottom": ax_B, "left" : ax_L})
    plotitem.showGrid(True, True, alpha=grid_opacity)


# Make a time axis to use for all graphs.  Convert MJD to unix time so we can
# use the DateAxisItem and display time in a convenient calendar format
time_acu_unix = Time(hdul["MONITOR-ANT-GENERAL"].data["actual_time"], format="mjd").unix

# -----------------------------------------------------------------------------
# Tracking
# -----------------------------------------------------------------------------
# Central tracking coordinate
pi_TR_program_track_az = pg.PlotItem(title="hdul['MONITOR-ANT-TRACKING'].data['prog_track_az']")
pi_TR_program_track_az.plot(time_acu_unix, hdul['MONITOR-ANT-TRACKING'].data['prog_track_az'], 
                            pen=None, symbol="o", symbolBrush=None, symbolPen=color2)
layout_tracking.addItem(pi_TR_program_track_az, 0, 0)
add_axes(pi_TR_program_track_az)

pi_TR_program_track_el = pg.PlotItem(title="hdul['MONITOR-ANT-TRACKING'].data['prog_track_el']")
pi_TR_program_track_el.plot(time_acu_unix, hdul['MONITOR-ANT-TRACKING'].data['prog_track_el'],
                            pen=None, symbol="o", symbolBrush=None, symbolPen=color2)
layout_tracking.addItem(pi_TR_program_track_el, 0, 1)
add_axes(pi_TR_program_track_el)

# Scan pattern around central tracking coordinate
pi_TR_program_offset_az = pg.PlotItem(title="hdul['MONITOR-ANT-TRACKING'].data['prog_offset_az']")
pi_TR_program_offset_az.plot(time_acu_unix, hdul['MONITOR-ANT-TRACKING'].data['prog_offset_az'],
                             pen=None, symbol="o", symbolBrush=None, symbolPen=color2)
layout_tracking.addItem(pi_TR_program_offset_az, 1, 0)
add_axes(pi_TR_program_offset_az)


pi_TR_program_offset_el = pg.PlotItem(title="hdul['MONITOR-ANT-TRACKING'].data['prog_offset_el']")
pi_TR_program_offset_el.plot(time_acu_unix, hdul['MONITOR-ANT-TRACKING'].data['prog_offset_el'], 
                             pen=None, symbol="o", symbolBrush=None, symbolPen=color2)
layout_tracking.addItem(pi_TR_program_offset_el, 1, 1)
add_axes(pi_TR_program_offset_el)

# Constant tracking position offset 
# (not used after 17 MAR 2023. replaced by dynamic user pointing correction)
pi_TR_position_offset_az = pg.PlotItem(title="hdul['MONITOR-ANT-TRACKING'].data['position_offset_az']")
pi_TR_position_offset_az.plot(time_acu_unix, hdul['MONITOR-ANT-TRACKING'].data['position_offset_az'],
                              pen=None, symbol="o", symbolBrush=None, symbolPen=color2)
layout_tracking.addItem(pi_TR_position_offset_az, 2, 0)
add_axes(pi_TR_position_offset_az)


pi_TR_position_offset_el = pg.PlotItem(title="hdul['MONITOR-ANT-TRACKING'].data['position_offset_el']")
pi_TR_position_offset_el.plot(time_acu_unix, hdul['MONITOR-ANT-TRACKING'].data['position_offset_el'],
                              pen=None, symbol="o", symbolBrush=None, symbolPen=color2)
layout_tracking.addItem(pi_TR_position_offset_el, 2, 1)
add_axes(pi_TR_position_offset_el)


# -----------------------------------------------------------------------------
# Axis
# -----------------------------------------------------------------------------
# Constant Axis position offset
pi_AX_current_position_offset_az = pg.PlotItem(title="hdul['MONITOR-ANT-AXIS'].data['current_position_offset'][:, AZ]")
pi_AX_current_position_offset_az.plot(time_acu_unix, hdul['MONITOR-ANT-AXIS'].data['current_position_offset'][:, AZ],
                                      pen=None, symbol="o", symbolBrush=None, symbolPen=color4)
layout_axis.addItem(pi_AX_current_position_offset_az, 0, 0)
add_axes(pi_AX_current_position_offset_az)


pi_AX_current_position_offset_el = pg.PlotItem(title="hdul['MONITOR-ANT-AXIS'].data['current_position_offset'][:, EL]")
pi_AX_current_position_offset_el.plot(time_acu_unix, hdul['MONITOR-ANT-AXIS'].data['current_position_offset'][:, EL],
                                      pen=None, symbol="o", symbolBrush=None, symbolPen=color4)
layout_axis.addItem(pi_AX_current_position_offset_el, 0, 1)
add_axes(pi_AX_current_position_offset_el)


# Constant Axis external position offset.  I have never seen this in use.  Where does it come from?
pi_AX_external_position_offset_az = pg.PlotItem(title="hdul['MONITOR-ANT-AXIS'].data['external_position_offset'][:, 0]")
pi_AX_external_position_offset_az.plot(time_acu_unix, hdul['MONITOR-ANT-AXIS'].data['external_position_offset'][:, AZ],
                                       pen=None, symbol="o", symbolBrush=None, symbolPen=color4)
layout_axis.addItem(pi_AX_external_position_offset_az, 1, 0)
add_axes(pi_AX_external_position_offset_az)

pi_AX_external_position_offset_el = pg.PlotItem(title="hdul['MONITOR-ANT-AXIS'].data['external_position_offset'][:, 1]")
pi_AX_external_position_offset_el.plot(time_acu_unix, hdul['MONITOR-ANT-AXIS'].data['external_position_offset'][:, EL],
                                       pen=None, symbol="o", symbolBrush=None, symbolPen=color4)
layout_axis.addItem(pi_AX_external_position_offset_el, 1, 1)
add_axes(pi_AX_external_position_offset_el)

# -----------------------------------------------------------------------------
# Pointing (dynamic corrections)
# -----------------------------------------------------------------------------
SELECTED_SAMPLE = 0
nineparam_pointing_model_str = "p1={}, p2={}, p3={}, p4={}, p5={}, p6={}, p7={}, p8={}, p9={}".format(
    hdul['MONITOR-ANT-TRACKING'].data['p1'][SELECTED_SAMPLE],
    hdul['MONITOR-ANT-TRACKING'].data['p2'][SELECTED_SAMPLE],
    hdul['MONITOR-ANT-TRACKING'].data['p3'][SELECTED_SAMPLE],
    hdul['MONITOR-ANT-TRACKING'].data['p4'][SELECTED_SAMPLE],
    hdul['MONITOR-ANT-TRACKING'].data['p5'][SELECTED_SAMPLE],
    hdul['MONITOR-ANT-TRACKING'].data['p6'][SELECTED_SAMPLE],
    hdul['MONITOR-ANT-TRACKING'].data['p7'][SELECTED_SAMPLE],
    hdul['MONITOR-ANT-TRACKING'].data['p8'][SELECTED_SAMPLE],
    hdul['MONITOR-ANT-TRACKING'].data['p9'][SELECTED_SAMPLE],
)

refraction_model_str = "r0={}, b1={}, b2={}".format(
    hdul['MONITOR-ANT-TRACKING'].data['r0'][SELECTED_SAMPLE],
    hdul['MONITOR-ANT-TRACKING'].data['b1'][SELECTED_SAMPLE],
    hdul['MONITOR-ANT-TRACKING'].data['b2'][SELECTED_SAMPLE],
)

inclinometer_model_str = "i1_x={}, i1_y={}, i1_temp={}, i2_x={}, i2_y={}, i2_temp={}".format(
    hdul['MONITOR-ANT-TRACKING'].data['inclinometer1_x'][SELECTED_SAMPLE],
    hdul['MONITOR-ANT-TRACKING'].data['inclinometer1_y'][SELECTED_SAMPLE],
    hdul['MONITOR-ANT-TRACKING'].data['inclinometer1_temp'][SELECTED_SAMPLE],
    hdul['MONITOR-ANT-TRACKING'].data['inclinometer2_x'][SELECTED_SAMPLE],
    hdul['MONITOR-ANT-TRACKING'].data['inclinometer2_y'][SELECTED_SAMPLE],
    hdul['MONITOR-ANT-TRACKING'].data['inclinometer2_temp'][SELECTED_SAMPLE],
)
pi_PO_model_corrections_az = pg.PlotItem(title="hdul['MONITOR-ANT-TRACKING'].data['pointing_model_correction_az']<font><br>{}</font>".format(nineparam_pointing_model_str))
pi_PO_model_corrections_az.plot(time_acu_unix, hdul['MONITOR-ANT-TRACKING'].data['pointing_model_correction_az'],
    pen=None, symbol="o", symbolBrush=None, symbolPen=color3)
layout_pointing.addItem(pi_PO_model_corrections_az, 0, 0)
add_axes(pi_PO_model_corrections_az)


pi_PO_model_corrections_el = pg.PlotItem(title="hdul['MONITOR-ANT-TRACKING'].data['pointing_model_correction_el']<font><br>{}</font>".format(nineparam_pointing_model_str))
pi_PO_model_corrections_el.plot(time_acu_unix, hdul['MONITOR-ANT-TRACKING'].data['pointing_model_correction_el'],
    pen=None, symbol="o", symbolBrush=None, symbolPen=color3)
layout_pointing.addItem(pi_PO_model_corrections_el, 0, 1)
add_axes(pi_PO_model_corrections_el)

pi_PO_refraction_model_corrections_el = pg.PlotItem(title="hdul['MONITOR-ANT-TRACKING'].data['refraction_model_correction_el']<font><br>{}</font>".format(refraction_model_str))
pi_PO_refraction_model_corrections_el.plot(time_acu_unix, hdul['MONITOR-ANT-TRACKING'].data['refraction_model_correction_el'],
    pen=None, symbol="o", symbolBrush=None, symbolPen=color3)
layout_pointing.addItem(pi_PO_refraction_model_corrections_el, 1, 1)
add_axes(pi_PO_refraction_model_corrections_el)

pi_PO_inclinometer_model_corrections_el = pg.PlotItem(title="hdul['MONITOR-ANT-TRACKING'].data['inclinometer_model_correction_el']<font><br></font>{}".format(inclinometer_model_str))
pi_PO_inclinometer_model_corrections_el.plot(time_acu_unix, hdul['MONITOR-ANT-TRACKING'].data['inclinometer_model_correction_el'],
    pen=None, symbol="o", symbolBrush=None, symbolPen=color3)
layout_pointing.addItem(pi_PO_inclinometer_model_corrections_el, 2, 1)
add_axes(pi_PO_inclinometer_model_corrections_el)

# Note user fine pointing correction EL unit is mdeg in ACU status message.  Convert to deg to use auto-SI axis labels
pi_PO_user_fine_pointing_correction_az = pg.PlotItem(title="hdul['MONITOR-ANT-TRACKING'].data['user_fine_pointing_correction_az']")
pi_PO_user_fine_pointing_correction_az.plot(time_acu_unix, 0.001 * hdul['MONITOR-ANT-TRACKING'].data['user_fine_pointing_correction_az'],
    pen=None, symbol="o", symbolBrush=None, symbolPen=color3)
layout_pointing.addItem(pi_PO_user_fine_pointing_correction_az, 3, 0)
add_axes(pi_PO_user_fine_pointing_correction_az)

# Note user fine pointing correction EL unit is mdeg in ACU status message.  Convert to deg to use auto-SI axis labels
pi_PO_user_fine_pointing_correction_el = pg.PlotItem(title="hdul['MONITOR-ANT-TRACKING'].data['user_fine_pointing_correction_el']")
pi_PO_user_fine_pointing_correction_el.plot(time_acu_unix, 0.001 * hdul['MONITOR-ANT-TRACKING'].data['user_fine_pointing_correction_el'],
    pen=None, symbol="o", symbolBrush=None, symbolPen=color3)
layout_pointing.addItem(pi_PO_user_fine_pointing_correction_el, 3, 1)
add_axes(pi_PO_user_fine_pointing_correction_el)

# -----------------------------------------------------------------------------
# Results
# -----------------------------------------------------------------------------
# Enable Legend for some PlotItems that have more than 1 PlotDataItem and require legend labels per line
# (must add Legend before we use PlotItem.plot() to generate the PlotDataItems that have param name="xxxxx")

# PlotItem to show pointing corrections calculated by the TRACKING subsystem, then indicated in each AXIS status
pi_result_pointing_correction_az = pg.PlotItem(title="Total Pointing Correction AZ")
pi_result_pointing_correction_az.addLegend()
pi_result_pointing_correction_az.plot(time_acu_unix, hdul['MONITOR-ANT-TRACKING'].data['total_pointing_correction_az'],
    pen=None, symbol="o", symbolBrush=None, symbolPen=color3, symbolSize=30, name="Tracking total_pointing_correction_az")
pi_result_pointing_correction_az.plot(time_acu_unix, hdul['MONITOR-ANT-AXIS'].data['current_pointing_correction'][:, AZ],
    pen=None, symbol="o", symbolBrush=None, symbolPen=color1, symbolSize=22, name="Axis current_pointing_correction")
layout_result.addItem(pi_result_pointing_correction_az, 0, 0)
add_axes(pi_result_pointing_correction_az)

pi_result_pointing_correction_el = pg.PlotItem(title="Total Pointing Correction EL")
pi_result_pointing_correction_el.addLegend()
pi_result_pointing_correction_el.plot(time_acu_unix, hdul['MONITOR-ANT-TRACKING'].data['total_pointing_correction_el'],
    pen=None, symbol="o", symbolBrush=None, symbolPen=color3, symbolSize=30, name="Tracking total_pointing_correction_el")
pi_result_pointing_correction_el.plot(time_acu_unix, hdul['MONITOR-ANT-AXIS'].data['current_pointing_correction'][:, EL],
    pen=None, symbol="o", symbolBrush=None, symbolPen=color1, symbolSize=22, name="Axis current_pointing_correction")
layout_result.addItem(pi_result_pointing_correction_el, 0, 1)
add_axes(pi_result_pointing_correction_el)

# PlotItem to show axis position from perspective of TRACKING sybsystem, AXIS encoder, 
# and finally "ideal" reference frame.
pi_result_position_az = pg.PlotItem(title="Position AZ")
pi_result_position_az.addLegend()
pi_result_position_az.plot(time_acu_unix, hdul['MONITOR-ANT-TRACKING'].data['desired_az'],
    pen=None, symbol="o", symbolBrush=None, symbolPen=color4, symbolSize=size4, name="Tracking desired_az")
pi_result_position_az.plot(time_acu_unix, hdul['MONITOR-ANT-AXIS'].data['desired_position'][:, AZ],
    pen=None, symbol="o", symbolBrush=None, symbolPen=color3, symbolSize=size3, name="Axis desired_position")
pi_result_position_az.plot(time_acu_unix, hdul['MONITOR-ANT-AXIS'].data['encoder_position_incl_pcorr'][:, AZ],
    pen=None, symbol="o", symbolBrush=None, symbolPen=color2, symbolSize=size2, name="Axis encoder_position_incl_pcorr")
pi_result_position_az.plot(time_acu_unix, hdul['MONITOR-ANT-AXIS'].data['actual_position'][:, AZ],
    pen=None, symbol="o", symbolBrush=None, symbolPen=color1, symbolSize=size1, name="Axis actual_position")
layout_result.addItem(pi_result_position_az, 1, 0)
add_axes(pi_result_position_az)

pi_result_position_el = pg.PlotItem(title="Position EL")
pi_result_position_el.addLegend()
pi_result_position_el.plot(time_acu_unix, hdul['MONITOR-ANT-TRACKING'].data['desired_el'],
    pen=None, symbol="o", symbolBrush=None, symbolPen=color4, symbolSize=size4, name="Tracking desired_el")
pi_result_position_el.plot(time_acu_unix, hdul['MONITOR-ANT-AXIS'].data['desired_position'][:, EL],
    pen=None, symbol="o", symbolBrush=None, symbolPen=color3, symbolSize=size3, name="Axis desired_position")
pi_result_position_el.plot(time_acu_unix, hdul['MONITOR-ANT-AXIS'].data['encoder_position_incl_pcorr'][:, EL],
    pen=None, symbol="o", symbolBrush=None, symbolPen=color2, symbolSize=size2, name="Axis encoder_position_incl_pcorr")
pi_result_position_el.plot(time_acu_unix, hdul['MONITOR-ANT-AXIS'].data['actual_position'][:, EL],
    pen=None, symbol="o", symbolBrush=None, symbolPen=color1, symbolSize=size1, name="Axis actual_position")
layout_result.addItem(pi_result_position_el, 1, 1)
add_axes(pi_result_position_el)

# Start the Qt event loop and block this script until user closes the GUI window
# QtGui.QApplication.instance().exec_()
app.exec_()

# Close the file
fid.close()