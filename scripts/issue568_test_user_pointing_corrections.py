clear_pmodel()
clear_axis_position_offsets()
clear_refraction()

# Minimum schedule parameters
project_id = 'test_tracking_offsets'
obs_id = 'ss'

# Scantype Parameters
subscan_duration = 10
# Parameter Blocks
scantype_params = ScantypeParamsOnSource(subscan_duration)

az = 30
el = 60
user_pointing_correction_az_list = [0, 3 * 3600, 0, 3 * 3600]
user_pointing_correction_el_list = [0, 0, 5 * 3600, 5 * 3600]

# Generate scan_id based on elevation, user_pointing_correction_az, user_pointing_correction_el
for (corr_az, corr_el) in zip(user_pointing_correction_az_list, user_pointing_correction_el_list):
    id = int(10000 * el + 100 * corr_az/3600 + corr_el/3600)
    log("az: {}, el: {}, offset_az: {}, offset_el: {}, scan_id: {}".format(az, el, corr_az, corr_el, id))
    schedule_params = ScheduleParams(project_id, obs_id, scan_id=id)
    tracking_params = TrackingParamsHO(az, el, corr_az, corr_el)
    add_scan(schedule_params, scantype_params, tracking_params)

results = run_queue()
log(results)

# Set pointing model and run again
set_pmodel(p1=2*3600, p7=4*3600)

# Generate scan_id based on elevation, user_pointing_correction_az, user_pointing_correction_el
for (corr_az, corr_el) in zip(user_pointing_correction_az_list, user_pointing_correction_el_list):
    id = int(10000 * el + 100 * corr_az/3600 + corr_el/3600)
    log("az: {}, el: {}, offset_az: {}, offset_el: {}, scan_id: {}".format(az, el, corr_az, corr_el, id))
    schedule_params = ScheduleParams(project_id, obs_id, scan_id=id)
    tracking_params = TrackingParamsHO(az, el, corr_az, corr_el)
    add_scan(schedule_params, scantype_params, tracking_params)

results = run_queue()
log(results)

clear_pmodel()