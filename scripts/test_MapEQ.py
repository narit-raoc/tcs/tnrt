# --------
# Schedule
# --------
project_id = "no_projid"
obs_id = "no_obsid"

schedule_params = ScheduleParams(project_id, obs_id, source_name="sim_source", line_name="sim_line")

# --------
# Tracking
# --------
# Choose an AZ , EL above the horizon in the region that we want to test tracking.
az = 150 * units.deg
el = 45 * units.deg
log(
    "Choose a coordinate above the horizon now. (AZ, EL) = ({}, {}) [deg]".format(
        az.value, el.value
    )
)

# Transform the AZ, EL coordinate to ICRS that will be used to configure the tracking.
coord_altaz = SkyCoord(
    frame="altaz",
    az=az,
    alt=el,
    obstime=Time.now(),
    location=get_site_location(),
)
coord_icrs = coord_altaz.transform_to("icrs")
log(
    "Calculated ICRS (RA, DEC) = ({}, {}) [deg]".format(
        coord_icrs.ra.deg, coord_icrs.dec.deg
    )
)

# Get the RA, DEC values from SkyCoord as floats.  Set all other properties to 0.
ra_catalog_icrs = coord_icrs.ra.deg
dec_catalog_icrs = coord_icrs.dec.deg

tracking_params = TrackingParamsEQ(ra_catalog_icrs, dec_catalog_icrs)


# --------
# Backend
# --------
backend_params = BackendParamsMock("TNRT_dualpol_spectrometer", integration_time=1, freq_res=250000)

# ---------------------------------------
# Scan type - single horizontal line scan, offset pattern AZ, EL
# ---------------------------------------
line_length = 7200
time_per_line = 30
nlines = 1
spacing = 720
axis = "x"
zigzag = False
coord_system = "HO"
subscans_per_cal = 0
time_cal0 = 30
time_per_cal = 5

scantype_params = ScantypeParamsMap(line_length, time_per_line, nlines, spacing, axis,
    zigzag, coord_system, subscans_per_cal, time_cal0, time_per_cal)

add_scan(schedule_params, scantype_params, tracking_params, backend_params=backend_params)

# ---------------------------------------
# Scan type - Map, NO center cal, NO zigzag, offset pattern AZ, EL
# ---------------------------------------
line_length = 7200
time_per_line = 30
nlines = 10
spacing = 720
axis = "x"
zigzag = False
coord_system = "HO"
subscans_per_cal = 0
time_cal0 = 30
time_per_cal = 5

scantype_params = ScantypeParamsMap(line_length, time_per_line, nlines, spacing, axis,
    zigzag, coord_system, subscans_per_cal, time_cal0, time_per_cal)

add_scan(schedule_params, scantype_params, tracking_params, backend_params=backend_params)

# ---------------------------------------
# Scan type - Map, NO center cal, YES zigzag, offset pattern in RA, Dec
# ---------------------------------------

line_length = 7200
time_per_line = 30
nlines = 10
spacing = 720
axis = "x"
zigzag = True
coord_system = "EQ"
subscans_per_cal = 0
time_cal0 = 30
time_per_cal = 5

scantype_params = ScantypeParamsMap(line_length, time_per_line, nlines, spacing, axis,
    zigzag, coord_system, subscans_per_cal, time_cal0, time_per_cal)

add_scan(schedule_params, scantype_params, tracking_params, backend_params=backend_params)

# Run all scans in the queue.  Receive results after queue is finished.
results = run_queue()

# Show the results of all scans from this run to the terminal window.
# All types of scans have some common results.  For example: a list of filenames
# that were recorded.
log("all results: %s" % results)
