delay_until_scheduled_start = 5
project_id = "test script"
obs_id = "SS"
start_mjd = (Time.now() + delay_until_scheduled_start * units.second).mjd
source_name = "AAA"
line_name = "BBB"
schedule_params = ScheduleParams(
    project_id,
    obs_id,
    start_mjd=start_mjd,
    source_name=source_name,
    line_name=line_name,
)


duration = 20  # second
scantype_params = ScantypeParamsOnSource(duration)

# Tracking Horizontal
az = 50
el = 45
tracking_params = TrackingParamsHO(az, el)

#  For L-band provision

# test with "defualt" configure on TNRT_dualpol_sepctrometer mode
backend_params = BackendParamsEDD("TNRT_dualpol_spectrometer")
add_scan(schedule_params, scantype_params, tracking_params, backend_params=backend_params)

# test with "specific" configure on TNRT_dualpol_sepctrometer mode
backend_params = BackendParamsEDD("TNRT_dualpol_spectrometer", integration_time=1, freq_res=4000)
add_scan(schedule_params, scantype_params, tracking_params, backend_params=backend_params)

# run all the quese
results = run_queue()

