# About version tags
The TNRT TCS project is in progress of using semantic version naming system (~release::Major . ~release::Minor . ~release::Patch - RELEASE_LABELS) following the specification published at https://semver.org/ in the software release workflow.  Following the concept of semantic versioning,  each release tag has release notes in this file that are organized by major / minor / patch and optional pre-release labels that determine the next steps in deployment.  Since this is a new workflow for the NARIT team, the version number and pre-release labels are a "best effort" to help the users understand the scope, intent, and risk of changes -- not a strict guarantee about the level of backward compatibility by automated systems.

**API** : The TNRT Command Line Interface API https://tnrt-nash.netlify.app/ and data file format specifications

**Software Compatibility Levels**

~release::Major Changes to API are not backward compatible

~release::Minor New features, internal improvements, other changes that are backward compatible from interface of user API

~release::Patch Backward-compatible bug fixes

**Release labels**

~ScienceReview Changes can be ~release::Major or ~release::Minor in the context of software compatibility version.  However, these changes can affect the algorithms or quality of numerical data results.  Releases that include the label ~ScienceReview must be reviewed by a member of the Science team before deployment to TNRT site.  Althouth the specification at https://semver.org calls these labels "pre-release", we should keep this label
in the release tag name for informative project deployment history.
# Next Release
# v0.7.0 (2024 DEC 17)
<details>
<summary>
HOLOFFT full spectrum ~release::Minor
</summary>

* Merge Requests: !292+

* Issues: None

* Comments:
</details>


# v0.6.3 (2024 AUG 02)
<details>
<summary>
Show correct tag in release notes header. ~release::Patch
</summary>

* Merge Requests: None

* Issues: None

* Comments:
</details>

# v0.6.2 (2024 JUL 30)
<details>
<summary>
Restore lost frequency axis data in .mbfits ARRAYDATA header. ~release::Patch
</summary>

* Merge Requests: !278+

* Issues: #650

* Comments:
Fixes a regression bug that was introduced in v0.6.0 when BackendParamsEDD starting accepting parameters for VLBI and PULSAR.
The bug causes .mbfits files to have no frequency axis data (freq resolution, start freq, ...).  Now, the frequency axis
metadata is written at the time of running the scan.  It was writing at the time of constructing scan objects to add to the queue.  Then CDB data was overwritten at runtime.
</details>
<details>
<summary>
Use ACU trackmode radec, not radecshortcut for .mbfits SCANDIR subscan geometry ~release::Patch
</summary>

* Merge Requests: !280+

* Issues: #655

* Comments:

</details>
<details>
<summary>
Use mutex lock in  mbfits pipeline write monitor to prevent array dimension mismatch ~release::Patch
</summary>

* Merge Requests: !279+

* Issues: #601

* Comments:

</details>
<details>
<summary>
Improve exception handling for `run_queue` to show details more than `CORBA.UNKNOWN` and not deadlock `nash` / `Scan.run_queue()` ~release::Patch
</summary>

* Merge Requests: !279+

* Issues: #601

* Comments:

</details>

# v0.6.1 (2024 MAY 13)
<details>
<summary>
Add logic to support redis port production EDD(2022Sep) and development EDD(2024May)(VLBI release). ~release::Patch
</summary>

* Merge Requests: !268+

* Issues: #

* Comments:
small release with add logic to support redis port 6333 and 6379
</details>

# v0.6.0 (2024 MAY 09)
<details>
<summary>
Set integration time units = microseconds (was milliseconds) in EddFitsClient. ~release::Major
</summary>

* Merge Requests: !267+

* Issues: #600+ 

* Comments:
Upgrade of EDD bakend system to latest version uses units of microseconds in packet header of data from edd_fits_server.  We must set TCS EddFitsClient to use the correct units.  If not, timestamps appear incorrect and will be discarded (no data in .mbfits file)
</details>

<details>
<summary>
Add user script parameters for VLBI. ~release::Minor
</summary>

* Merge Requests: !267+

* Issues: #645+ 

* Comments:
Also restructure and cleanup the generation of EDD configure json strings for spectrometer and pulsar.
</details>

<details>
<summary>
BackendHoloFFT implement frequency resolution following same pattern as BackendEDD. ~release::Minor
</summary>

* Merge Requests: !265+

* Issues: #636+ 

* Comments:
</details>

<details>
<summary>
Many small feature improvements to Holography TCS observation in response to first shakeout system test. ~release::Patch
</summary>

* Merge Requests: !262+

* Issues: #628+, #629+, #630+, #631+, 

* Comments:
BackendHoloFFT(ACS component), FFT35670A (FFT analyzer SCPI interface), PipelineALMATIfits, and DataMonitor to fix all of the problems from the first shakeout data recording of the Holography backend system test using Waveform Generator and TCS.
</details>

<details>
<summary>
Create new data_params for user script to select TCS data (file type) pipelines per scan ~release::Minor
</summary>

* Merge Requests: !261+

* Issues: #619+

* Comments: 
</details>

# v0.5.0 (2024 JAN 11)
<details>
<summary>
Move utils/cdb to tcs-central-db repo / Move utils/NotificationChannel to tcs-common-lib ~release::Minor
</summary>

* Merge Requests: !257+

* Issues: 

* Comments: New repository tcs-build it require to re-build/install the new structure that used for 4 repository(tnrt, tcs-central-db, tcs-monitor-grafana, tcs-commonm-lib)
</details>

<details>
<summary>
Add metadata / mbfits headers for ScanMap (OTF) including holography subscan types "HOLO" and "CORR" ~release::Minor
</summary>

* Merge Requests: !256+

* Issues: #612+, #618+

* Comments: 
</details>

<details>
<summary>
Cleanup PipelineALMATIfits to follow same logic as PipelineMBfits ~release::Minor
</summary>

* Merge Requests: !255+

* Issues: #617+

* Comments: No new features.  Only to "renovate" the old DataformatALMATIfits and pipeline
that has not been maintained for 2 years since all TNRT commissionining has evolved the use
of PipelineMBfits.
</details>
<details>
<summary>
Add Holography data to DataMonitor GUI ~release::Minor
</summary>

* Merge Requests: !254+

* Issues: #615+ , #616+

* Comments: 
</details>

<details>
<summary>
New feature tcs-redis-monitor grafana dashboard to view scan current values during obs ~release::Minor
</summary>

* Merge Requests: !253+

* Issues: https://gitlab.com/narit-raoc/tcs/tcs-monitor-grafana/-/issues/1 , https://gitlab.com/narit-raoc/tcs/tcs-monitor-grafana/-/issues/2

* Comments: project `tcs-monitor-grafana`, has the actual implementation and Docker container.  Changes to `tnrt` are only paths and a few references.
</details>

# v0.4.0 (2023 NOV 07)
<details>
<summary>
Add `nash` user commands `get_velocity`, `set_velocity`. to set the maximum velocity used by ACU druing all tracking activities.  Set TCS AZ, EL position and velocity limits to be the same as ACU ~release::Minor
</summary>

* Milestones: 

* Merge Requests: !248+

* Issues: #608+

* Comments: The ACU often has Az Group Safety Error related to AZ velocity deviation.  The current hypothesis is that we can set the maximum tracknig velocity to be less than system absolute maximum.  Hopefully, this can reduce the occurence of AZ velocity deviation errors.
</details>
<details>
<summary>
Bugfix: allow ScanMap to set line axis = "y" ~release::Minor
</summary>

* Milestones: 

* Merge Requests: !245+

* Issues: #607+

* Comments: 
</details>
<details>
<summary>
Update deprecated python syntax to be compabible with python 3.7 ~release::Patch
</summary>

* Milestones: 

* Merge Requests: !243+

* Issues: #605+, #606+

* Comments: The current TCS system is using Python 3.6.9 (set by Alma Common Software 2020 APR).  During a feasibility study to upgrade ACS to 2022 AUG using Python 3.8.6, we found some deprecated syntax in various parts of NARIT TCS.  The old syntax creates warnings in python 3.6.9, and errors in python 3.8.6. Therefore, I update the syntax to be compatible with py 3.8.6 and still works correctly without warnings in py 3.6.9.
</details>

# v0.3.1 (2023 AUG 28)
<details>
<summary>
Update release notes to edit content in v0.3.0 ~release::Patch
</summary>

* Milestones:

* Merge Requests:

* Issues: 

* Comments: 
</details>

# v0.3.0 (2023 AUG 24)
<details>
<summary>
New scan pattern ScanMap.  Also known as OTF map.  Simplest case is single OTF line.  
The Most complicated case is Holography scan with center calibration. ~release::Minor
</summary>

* Milestones: %7 , %8 , %15

* Merge Requests: !239+

* Issues: #602+

* Comments: 
</details>
<details>
<summary>
view_mbfits GUI new feature to display continuous ACU position in Geometry tab ~release::Minor
</summary>

* Milestones:

* Merge Requests: !239+

* Issues:

* Comments: This new feature is only a change the the GUI.  It can show old .mbfits files too 
</details>
<details>
<summary>
`acu_logger.py` reads  IP address of ACU from `/etc/hosts/` by default. ~release::Patch
</summary>

* Milestones:

* Merge Requests: !237+

* Issues:

* Comments: 
</details>
<details>
<summary>
Save `.mbfits` files in sub-directories by day (`tnrt_data/YY/MM/DD/YYYYMMDD_HHMMSS.mbfits`). not one flat directory with too many files ~release::Minor
</summary>

* Milestones:

* Merge Requests: !236+

* Issues: #603+

* Comments: 
</details>

# v0.2.0 (2023 JUN 27)
<details>
<summary>
EddFitsClient subtract 1 * integration time from EDD Spectrometer packets ~release::Minor ~ScienceReview
</summary>

* Milestones: %7

* Merge Requests: !232+

* Issues: #599+, #593+

* Comments: This is a hack because we don't know the root cause of timestamp alignment with wrong packet.  Remove this hack if we can find the root cause and fix it.
</details>
<details>
<summary>
Fix bug frontendL monitor fail ~release::Minor
</summary>

* Milestones: 

* Merge Requests: !229

* Issues: #590+

* Comments: FrontendL doesn't not provide downconverter data so it fail the monitor frontend process
</details>
<details>
<summary>
Create new scripts/acu_logger.py standalone ACU data logger to csv
</summary>

* Milestones: 

* Merge Requests: !226

* Issues: #594+

* Comments:
</details>

<details>
<summary>
Organize unit tests of Atmoshpere and TemplatePy to use consistent structure ~release::Patch
</summary>

* Milestones: 

* Merge Requests: !224

* Issues: None

* Comments:
</details>

<details>
<summary>
Remove data archive ~release::Minor
</summary>

* Milestones: 

* Merge Requests: !222

* Issues: #572

* Comments: Create new repository to deal with data archive here >> https://gitlab.com/narit-raoc/tnrt-data-archive
</details>

# v0.1.0 (2023 APR 18)
<details>
<summary>
View Mbfits read frequency axis from header ~release::Minor
</summary>

* Milestones: 

* Merge Requests: !219

* Issues: #482 #574

* Comments: view_mbfits read frequency axis from header so now support K band
</details>

<details>
<summary>
~ScienceReview Calculate LONGOFF and LATOFF in .mbfits DATAPAR from actual positions derived from ACU encoders.  Was commanded position in program offset table ~release::Minor
</summary>

* Milestones: %7

* Merge Requests: !218+

* Issues: #573+

* Comments:
LONGOFF and LATOFF are the values used as "dAz" and "dEL" to calculate the pointng model from cross scan data.  In the past, the ACU status message had so many incorrect and inconsistent data that we could not use it to calculate LONGOFF and LATOFF from actual positions.  We had to use commanded program offset table values as a placeholder.  Now, the ACU status message is correct, so we can use the data. 
</details>

<details>
<summary>
Rename `tracking offsets` to `user pointing corrections` to indicate what it actually does in the ACU ~release::Major
</summary>

* Milestones: %7

* Merge Requests: !218+

* Issues: #571+

* Comments:
The ICD documentation of ACU has been confusing in the past about this function where the command and side effects were
mixed between the Tracking subsystem and Pointing Correction subsystem.  Now it is clearly in the Pointing Correction subsystem. 
</details>

<details>
<summary>
Use redis to store metadata ~release::Minor
</summary>

* Milestones: 

* Merge Requests: !217+

* Issues: #560+

* Comments: Use redis as Central database to store metadata. Frequency-axis-related headers are added. Default header values are removed and filled with None instead. 
</details>
<details>
<summary>
Tracking before, during, and after UT1 day 00:00 works correctly ~release::Minor
New graphs to visualize axis offsets and pointing corrections for debugging pointing model ~release::Minor
</summary>

* Milestones: %7

* Merge Requests: !216+

* Issues: #554+ , #555+

* Comments: 
</details>
<details>
<summary>
Tracking before, during, and after UT1 day 00:00 works uses the correct day ~release::Minor
</summary>

* Milestones: %7

* Merge Requests: !213+

* Issues: #552

* Comments: 
</details>

<details>
<summary>
Edit ACU remote interface used by TCS remote client (more status data and changes subsystem of tracking offset command). ~release::Minor
</summary>

* Milestones: %7

* Merge Requests: !215+

* Issues: #566 , #563

* Comments: After 17 MAR 2023 the ACU status message includes encoder position directly (including pointing corrections) and "actual_position" which does not indicate the pointing corrections.  (this was **incorrectly** called "encoder_position" in previous versions of TCS software)
</details>

# 0.0.0-ScienceReview (2023 MAR 02)

NOTE: OHB ACU software has not changed since 2023 JAN 12.  The remaining issues assigned to OHB are here: ~OHB

<details>
<summary>
Rename nash function `set_constant_pointing_offsets` to `set_axis_position_offsets` ~release::Major 
</summary>

* Milestones: %7 

* Merge Requests: !203+

* Issues: #502+

* Comments: Old scripts that use this function must be updated to use new function name
</details>

<details>
<summary>
~ScienceReview Use most current tracking parameters at time of start tracking - not at time of queue creation ~release::Minor </summary>

* Milestones: %7

* Merge Requests: !211+

* Issues:

    - #551+

    - #553+

    - #558+

* Comments:  We have not yet identified all of the root causes of unusual cross-scan results after pointing model is loaded.  During the investigation, the above issues were identified and now fixed.  These updates will not likely result in a perfect pointing model, but the results will be better than before.  For example, metadata headers in MBFITS were stale (old metadata from previous scan in the queue was "stuck"), and now should be correct.  Also, when a scan crossed the UT1 day boundary, the old data was "stuck" in the tracking parameters until restart ACS ScanC Container.  Now, I am confident that each scan uses the current data. These improvements should reduce confusion and allow us to continue debug #543+ when antenna wraps around 0/360 boundary.  Improved tracking might also fix the perception that the beam size appears too big.  Since ACU has not yet been upgraded, any scan that tracks across the UT1 day boundary will have a discontinuity.  But, the next scan in the queue should be correct.
</details>

<details>
<summary>
~ScienceReview Account for pointing model corrections in cos(EL) projection of program offset azimuth ~release::Minor 
</summary>

* Milestones: %7

* Merge Requests: !211+

* Issues: #542+

* Comments: In the past, the AZ program offset table (horizontal arm of cross scan) was projected by 1/cos(EL) too wide (if Pointing Model P7 > 0).  However, when data is recorded to .mbfits, it was scaled by cos(EL) correctly for the actual EL angle.  So the horizontal width of cross scan was wider than necessary, but recorded data was still valid for Gaussian fitting and pointing model calculation.  Now, the actual width of the horizontal part of cross scan should be the correct width.
 
</details>

<details>
<summary>
Add new parameter "TNRT_pulsar" for BackendParamsEDD in observation script ~release::Minor 
</summary>

* Milestones: %32

* Merge Requests: !209+

* Issues: #549+

* Comments:  Operators can now do a pulsar observation using the standard nash script parameters.  No need for software engineer to login and start all of the pieces manually.
</details>

<details>
<summary>
Refactor zmq to allow creation of multiple objects, not force singleton ~release::Minor 
</summary>

* Milestones: %41

* Merge Requests: !205+

* Issues: #519+

* Comments:  ACS notification has been replaced by ZeroMQ notification.  The only noticeable change for hte user is that subsystems will start and initialize faster.
</details>

<details>
<summary>
Use redis as central database for metadata (cdb) ~release::Minor 
</summary>

* Milestones: 

    - %41
    
    - %38

* Merge Requests: !208+

* Issues: #557+

* Comments:  This change requires installation and configuration of `docker` before rebuilding the TCS software project.  This new step of system installation is documented in the TCS "new install" wiki https://gitlab.com/narit-raoc/tnrt/-/wikis/Install-TCS-Ubuntu-20-(ACS-2020APR) This is the first Docker Container used by TNRT TCS.  In the future, we plan to replace ACS Container processes with Docker Containers to enable compatibility with modern industry standards.
</details>

<details>
<summary>
Keep power monitor data from L/K frontend in .mbfits for Radio Frequency Interference monitor ~release::Minor 
</summary>

* Milestones: None

* Merge Requests: !207+

* Issues: #441+

* Comments:  `.mbfits` files created after this change include power sensor data throughout observation.  The data is stored in the new hdu name `['MONITOR-FRONTEND']`.  Includes data
```
ColDefs(
    name = 'timestamp'; format = 'D'; unit = 'day[MJD]'
    name = 'level1pps'; format = 'D'; unit = 'V'
    name = 'level100mhz'; format = 'D'; unit = 'V'
    name = 'valve_sts'; format = 'J'; bzero = 2147483648
    name = 'temp_cold'; format = 'D'; unit = 'C'
    name = 'temp_warm'; format = 'D'; unit = 'C'
    name = 'temp_object'; format = 'D'; unit = 'C'
    name = 'temp_sink'; format = 'D'; unit = 'C'
    name = 'lna_status'; format = '2J'; bzero = 2147483648; dim = '(2)'
    name = 'attenuation'; format = '2D'; unit = 'dB'; dim = '(2)'
    name = 'total_power'; format = '2D'; unit = 'dBm'; dim = '(2)'
    name = 'heater20k'; format = 'D'; unit = '%'
    name = 'heater70k'; format = 'D'; unit = '%'
    name = 't15'; format = 'D'; unit = 'K'
    name = 't70'; format = 'D'; unit = 'K'
    name = 'down_converter_freq'; format = 'D'; unit = 'MHz'
)
```
</details>

<details>
<summary>
Add "Export CSV" button to view_mbfits GUI ~release::Minor 
</summary>

* Milestones: %38

* Merge Requests: !204+

* Issues: #472+

* Comments:
</details>

<details>
<summary>
Disconnect PySimpleClient client every time and avoid error "too many file descriptors" in ScanC ~release::Patch 
</summary>

* Milestones: None

* Merge Requests: !210+

* Issues: None

* Comments:  When TCS runs a scan, it creates many connections within ACS framework using the PySimpleClient (socked client).  Before this bugfix, some of the socket clients remained "alive" and accumulate until the system has too many connections.  ScanC container fails with an error "too many file descriptors" or something like that.  After this change, all socket clients are disconnected after finished use, so we don't see this error anymore.
</details>

<details>
<summary>
Enable HXP axis offsets ~release::Patch
</summary>

* Milestones: %38

* Merge Requests: !203

* Issues: #531

* Comments:  ACU Hexapod control has an unusual state machine where the HXP offsets were not applied until the next command.  This is fixed on the TCS side by sending the next command to HXP to go to the same current position.  Then, the offets are "activated"
</details>

# 2023_JAN_23
Details not available

# 2023_JAN_12
Details not available

# 2023_DEC_14
Details not available

# 2023_DEC_09
Details not available
