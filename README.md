# TNRT Software Project
(This document is https://gitlab.com/narit-raoc/tnrt/blob/master/README.md . Edit and commit this README.md file to update this document)


Welcome to NARIT-RAOC Thai National Radio Telescope software.  
This repository includes the NARIT Telescope Control Software (TCS), which is a set of CORBA Components and Python clients that interact with existing CORBA Containers and Components from Alma Common Software (ACS).  TCS controls and monitors the status of all other subsystems of the 40m radiotelescope.

## License
Files in the ExtProd directory and subdirectories are pre-requisites to build this project.  They can be downloaded from the original author distribution, but they are stored here in git LFS for convenience of building the TNRT project using the specific versions.  This project does not define license of software packages in `ExtProd/`.  Refer to the original distribution of each package in `ExtProd/` to confirm license information. 

Files in the LGPL directory and subdirectories are available using LGPL License derived from
Alma Common Software https://bitbucket.alma.cl/projects/ASW/repos/acs/browse

## User Interface Documentation (nash CLI)
https://tnrt-nash.netlify.app/ (Documentation website for users and operators including example observation scripts)

## Antenna Control Unit (ACU) Remote Control Interface Specification
https://nc.narit.or.th/nextcloud/index.php/s/Exdm95AdCfZtsYM

## Software System Architecture
https://gitlab.com/narit-raoc/tnrt/-/wikis/TCS-software-architecture-and-block-diagrams (Wiki page about architecture and block diagrams)

## Installation of TNRT TCS
https://gitlab.com/narit-raoc/tnrt/-/wikis/Install-TCS-Ubuntu-20-(ACS-2020APR) (Wiki page about TCS software installation)

## Development Workflow
https://gitlab.com/narit-raoc/tnrt/-/wikis/Git-Workflow (Wiki page about Git software development workflow)