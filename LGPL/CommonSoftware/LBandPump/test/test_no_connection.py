import unittest
import logging
import coloredlogs
import time

# import ACS modules
from Acspy.Clients.SimpleClient import PySimpleClient

# import TCS modules
import LBandPumpError
import LBandPumpErrorImpl

level_styles_scrn = {
    "critical": {"color": "red", "bold": True},
    "debug": {"color": "white", "faint": True},
    "error": {"color": "red"},
    "info": {"color": "green", "bright": True},
    "notice": {"color": "magenta"},
    "spam": {"color": "green", "faint": True},
    "success": {"color": "green", "bold": True},
    "verbose": {"color": "blue"},
    "warning": {"color": "yellow", "bright": True, "bold": True},
}
field_styles_scrn = {
    "asctime": {},
    "hostname": {"color": "magenta"},
    "levelname": {"color": "cyan", "bright": True},
    "name": {"color": "blue", "bright": True},
    "programname": {"color": "cyan"},
}


class TestLBandPump(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        # Configure logger
        self.logger = logging.getLogger("LBandPump")
        self.logger.setLevel(logging.DEBUG)

        fmt_scrn = "%(asctime)s [%(levelname)s]: %(message)s"
        formatter_screen = coloredlogs.ColoredFormatter(
            fmt=fmt_scrn, level_styles=level_styles_scrn, field_styles=field_styles_scrn
        )

        # creating a handler to log on the console
        handler_screen = logging.StreamHandler()
        handler_screen.setFormatter(formatter_screen)
        handler_screen.setLevel(logging.DEBUG)

        # remove all handlers if they exist
        # remove because PySimpleClient logger already has handlers which
        # show duplicate of my log messages, but without the pretty color format.
        if len(self.logger.handlers) > 0:
            self.logger.handlers = []

        # adding handlers
        self.logger.addHandler(handler_screen)
        self.logger.debug("Started logger name %s" % self.logger.name)

        self.logger.debug("Starting PySimpleClient")
        self.sc = None
        self.sc = PySimpleClient()

        self.LBandPump = None
        self.component_name = "LBandPump"

        # Get reference to ComponentA.  If it is already activated by
        # ACS Command Center, this will be a reference to the same object.
        # If it is not active, this command will activate and create a new instance.
        try:
            self.logger.debug("Connecting to ACS component %s" % self.component_name)
            self.LBandPump = self.sc.getComponent(self.component_name)
        except Exception as e:
            self.logger.error("Cannot get ACS component object reference for %s" % self.component_name)
            self.cleanup()

    @classmethod
    def tearDownClass(self):
        try:
            time.sleep(0.5)
            self.logger.debug("Releasing ACS component %s" % self.component_name)
            self.sc.releaseComponent(self.component_name)
            self.LBandPump = None
        except Exception as e:
            self.logger.error("Cannot release component reference for %s" % self.component_name)

        if self.sc is not None:
            self.sc.disconnect()
            del self.sc

    def test_start_no_connection(self):
        with self.assertRaises(LBandPumpError.ConnectionFailedEx):
            self.LBandPump.start()

    def test_check_status_no_connection(self):
        with self.assertRaises(LBandPumpError.ConnectionFailedEx):
            self.LBandPump.check_status()


if __name__ == "__main__":
    unittest.main()
