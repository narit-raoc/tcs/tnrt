import unittest
from Pump import Pump
from unittest.mock import patch, Mock, call
import threading
from constant import COMMAND


class TestPump(unittest.TestCase):
    def setUp(self):
        with patch("Pump.socket.gethostbyname") as mock_get_host:
            mock_get_host.return_value = "mock ip"
            self.myPump = Pump()

    def test_connect(self):
        """[connect] should connect success"""
        with patch("Pump.socket") as mock_socket:
            mock_connection = Mock()
            mock_connection.connect = Mock()
            mock_socket.socket = Mock(return_value=mock_connection)
            mock_socket.AF_INET = "AF_INET"
            mock_socket.SOCK_STREAM = "SOCK_STREAM"
            self.myPump.connect()
            mock_socket.socket.assert_called_with("AF_INET", "SOCK_STREAM")
            mock_connection.connect.assert_called_with(("mock ip", 5005))

    def test_disconnect(self):
        """[disconnect] should disconnect success"""
        with patch("Pump.socket") as mock_socket:
            mock_connection = Mock()
            mock_connection.connect = Mock()
            mock_connection.close = Mock()
            mock_socket.socket = Mock(return_value=mock_connection)
            mock_socket.AF_INET = "AF_INET"
            mock_socket.SOCK_STREAM = "SOCK_STREAM"
            self.myPump.connect()
            self.myPump.disconnect()
            mock_connection.close.assert_called_once()

    def test_send_command_normal_success(self):
        """[send_command] should return \"SUCCESS\" when response is equal to command"""
        with patch("Pump.socket") as mock_socket:
            mock_recv_return = Mock()
            mock_recv_return.decode = Mock(return_value="1")
            mock_connection = Mock()
            mock_connection.connect = Mock()
            mock_connection.close = Mock()
            mock_connection.send = Mock()
            mock_connection.recv = Mock(return_value=mock_recv_return)
            mock_socket.socket = Mock(return_value=mock_connection)
            mock_socket.AF_INET = "AF_INET"
            mock_socket.SOCK_STREAM = "SOCK_STREAM"
            result = self.myPump.send_command("1")
            mock_connection.send.assert_called_with(str.encode("1"))
            self.assertEqual(result, "SUCCESS")

    def test_send_command_normal_fail(self):
        """[send_command] should raise runtimeErorr if response is not equal to command"""
        with patch("Pump.socket") as mock_socket:
            mock_recv_return = Mock()
            mock_recv_return.decode = Mock(return_value="2")
            mock_connection = Mock()
            mock_connection.connect = Mock()
            mock_connection.close = Mock()
            mock_connection.send = Mock()
            mock_connection.recv = Mock(return_value=mock_recv_return)
            mock_socket.socket = Mock(return_value=mock_connection)
            mock_socket.AF_INET = "AF_INET"
            mock_socket.SOCK_STREAM = "SOCK_STREAM"
            with self.assertRaises(RuntimeError):
                self.myPump.send_command("1")
                mock_connection.send.assert_called_with(str.encode("1"))

    def test_send_command_check_status_on(self):
        """[send_command] should return \"ON\" if response is C when check status"""
        with patch("Pump.socket") as mock_socket:
            mock_recv_return = Mock()
            mock_recv_return.decode = Mock(return_value="C")
            mock_connection = Mock()
            mock_connection.connect = Mock()
            mock_connection.close = Mock()
            mock_connection.send = Mock()
            mock_connection.recv = Mock(return_value=mock_recv_return)
            mock_socket.socket = Mock(return_value=mock_connection)
            mock_socket.AF_INET = "AF_INET"
            mock_socket.SOCK_STREAM = "SOCK_STREAM"
            result = self.myPump.send_command("5")
            mock_connection.send.assert_called_with(str.encode("5"))
            self.assertEqual(result, "ON")

    def test_send_command_check_status_off(self):
        """[send_command] should return \"OFF\" if response is A when check status"""
        with patch("Pump.socket") as mock_socket:
            mock_recv_return = Mock()
            mock_recv_return.decode = Mock(return_value="A")
            mock_connection = Mock()
            mock_connection.connect = Mock()
            mock_connection.close = Mock()
            mock_connection.send = Mock()
            mock_connection.recv = Mock(return_value=mock_recv_return)
            mock_socket.socket = Mock(return_value=mock_connection)
            mock_socket.AF_INET = "AF_INET"
            mock_socket.SOCK_STREAM = "SOCK_STREAM"
            result = self.myPump.send_command("5")
            mock_connection.send.assert_called_with(str.encode("5"))
            self.assertEqual(result, "OFF")

    def test_send_command_check_status_short_circuit(self):
        """[send_command] should return \"SHORT_CIRCUIT\" if response is D when check status"""

        with patch("Pump.socket") as mock_socket:
            mock_recv_return = Mock()
            mock_recv_return.decode = Mock(return_value="D")
            mock_connection = Mock()
            mock_connection.connect = Mock()
            mock_connection.close = Mock()
            mock_connection.send = Mock()
            mock_connection.recv = Mock(return_value=mock_recv_return)
            mock_socket.socket = Mock(return_value=mock_connection)
            mock_socket.AF_INET = "AF_INET"
            mock_socket.SOCK_STREAM = "SOCK_STREAM"
            result = self.myPump.send_command("5")
            mock_connection.send.assert_called_with(str.encode("5"))
            self.assertEqual(result, "SHORT_CIRCUIT")

    def test_send_command_check_status_fail(self):
        """[send_command] should return \"FAIL\" if response is B or something else when check status"""

        with patch("Pump.socket") as mock_socket:
            mock_recv_return = Mock()
            mock_recv_return.decode = Mock(return_value="B")
            mock_connection = Mock()
            mock_connection.connect = Mock()
            mock_connection.close = Mock()
            mock_connection.send = Mock()
            mock_connection.recv = Mock(return_value=mock_recv_return)
            mock_socket.socket = Mock(return_value=mock_connection)
            mock_socket.AF_INET = "AF_INET"
            mock_socket.SOCK_STREAM = "SOCK_STREAM"
            result = self.myPump.send_command("5")
            mock_connection.send.assert_called_with(str.encode("5"))
            self.assertEqual(result, "FAIL")

    def test_stream_command(self):
        """[stream_command] should should call send command 3 times in 3 seconds"""
        mock_send_command = Mock()
        self.myPump.send_command = mock_send_command
        mock_event = threading.Event()
        mock_event.set()
        timer = threading.Timer(3, mock_event.clear)
        timer.start()
        self.myPump.stream_command(mock_event, "command")
        mock_send_command.assert_has_calls(
            [
                call("command"),
                call("command"),
                call("command"),
            ]
        )

    def test_start_already_on(self):
        """[start] should raise exception if pump already on"""
        with self.assertRaises(Exception) as err:
            self.myPump.isOn.set()
            self.myPump.start()

    def test_start_success(self):
        """[start] should start new thread"""
        with patch("Pump.threading.Thread") as mock_thread:
            mock_thread_instance = Mock()
            mock_thread_instance.start = Mock()
            mock_thread.return_value = mock_thread_instance

            mock_connect = Mock()
            self.myPump.connect = mock_connect

            self.myPump.start()
            mock_thread.assert_called_once_with(
                name="power-on-thread",
                target=self.myPump.stream_command,
                args=[self.myPump.isOn, COMMAND["START"]],
            )
            mock_thread_instance.start.assert_called_once()
            mock_connect.assert_called_once()
            self.assertEqual(mock_thread_instance.daemon, True)

    def test_stop(self):
        """[stop] should clear isOn and IsTurbo event"""
        self.myPump.isOn.set()
        self.myPump.isTurbo.set()
        self.myPump.stop()
        self.assertEqual(self.myPump.isOn.is_set(), False)
        self.assertEqual(self.myPump.isTurbo.is_set(), False)

    def test_turbo_on_already_on(self):
        """[turbo_on] should raise exception if is Turbo already set"""
        self.myPump.isTurbo.set()
        with self.assertRaises(Exception):
            self.myPump.turbo_on()

    def test_turbo_on_pump_off(self):
        """[turbo_on] should raise exception if pump is off"""
        with self.assertRaises(Exception):
            self.myPump.turbo_on()

    def test_turbo_on_success(self):
        """[turbo_on] should start new thread"""
        self.myPump.isOn.set()
        with patch("Pump.threading.Thread") as mock_thread:
            mock_thread_instance = Mock()
            mock_thread_instance.start = Mock()
            mock_thread.return_value = mock_thread_instance

            self.myPump.turbo_on()
            mock_thread.assert_called_once_with(
                name="turbo-thread",
                target=self.myPump.stream_command,
                args=[self.myPump.isTurbo, COMMAND["TURBO_ON"]],
            )
            mock_thread_instance.start.assert_called_once()
            self.assertEqual(mock_thread_instance.daemon, True)

    def test_turbo_off(self):
        """[turbo_off] should clear IsTurbo event"""
        self.myPump.isTurbo.set()
        self.myPump.turbo_off()
        self.assertEqual(self.myPump.isTurbo.is_set(), False)


if __name__ == "__main__":
    unittest.main()
