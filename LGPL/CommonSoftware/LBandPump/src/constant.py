BUFFER_SIZE = 1
PORT = 5005
COMMAND_INTERVAL = 1

COMMAND = {
    "START": "1",
    "STOP": "6",  # stop pump and disable turbo
    "TURBO_ON": "3",
    "TURBO_OFF": "4",
    "CHECK_STATUS": "5",
}

PUMP_STATUS = {
    "OFF": "A",
    "FAIL": "B",
    "ON": "C",
    "SHORT_CIRCUIT": "D",
}
