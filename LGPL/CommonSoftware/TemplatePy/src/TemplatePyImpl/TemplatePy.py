# -*- coding: utf-8 -*-
import threading
import time

from Acspy.Servants.ACSComponent import ACSComponent
from Acspy.Servants.ContainerServices import ContainerServices
from Acspy.Servants.ComponentLifecycle import ComponentLifecycle
from AcsutilPy.ACSPorts  import getIP

# TCS packages
import TemplatePyMod
import TemplatePyMod__POA
import TemplatePyErrorImpl
import TemplatePyError

# Replace ACS
from Supplier import Supplier
from Consumer import Consumer


class TemplatePy(TemplatePyMod__POA.TemplatePy, ACSComponent, ContainerServices, ComponentLifecycle):
	'''
	Template for ACS component using Python
	'''
	# Constructor
	def __init__(self):
		ACSComponent.__init__(self)
		ContainerServices.__init__(self)
		self.logger = self.getLogger()
		self.val_public = 3.0
		self.__val_private = 9.0
		self.current_nc_struct = TemplatePyMod.ExampleStruct(0, 0, 0, 0)

		# Create an object of class TemplatePyMo/home/ssarris/tnrt/LGPL/CommonSoftware/TemplatePy/lib/python/site-packages/d.ExampleStruct which was generated from
		# TemplatePy.idl struct definition (Python doesn't have structs, so IDL compiler
		# generates classes to use in Python.)
		self.data_struct = TemplatePyMod.ExampleStruct(0, 1.0, 2.0, 3.0)
		
		# Create an object of class TemplatePyMod.ExampleValue which was generated from
		# TemplatePy.idl valuetype definition.  valuetype is the IDL syntax that is used
		# to generate a class in Python / C++ / Java automatically.
		self.data_valuetype = TemplatePyMod.ExampleChildValueType1(1.1, 2.2, 1234, 5678, True)

		self.data_enum = TemplatePyMod.option1
		self.ip = getIP()
	
	# Destructor
	def __del__(self):
		pass
	#------------------------------------------------------------------------------------
	# LifeCycle functions called by ACS system when we activate and deactivate components.
	# Inherited from headers in Acspy.Servants.ComponentLifecycle.  


	def initialize(self):
		self.logger.logInfo('INITIALIZE')
		# Create notification channel suppliers to publish combined data
		
	def execute(self):
		self.logger.logInfo('EXECUTE')

	def aboutToAbort(self):
		self.logger.logInfo('ABOUT_TO_ABORT')

	def cleanUp(self):
		self.logger.logInfo('CLEANUP')

	#------------------------------------------------------------------------------------
	# Functions defined in IDL to interface from outside this class	
	def get_ip(self):
		return self.ip
	
	def get_double(self):
		self.logger.logInfo('get_double. should return {}'.format(self.__val_private))
		return(self.__val_private)

	def get_enum(self):
		self.logger.logInfo('get_enum. should return {}'.format(self.data_enum))
		return(self.data_enum)

	def get_struct(self):
		self.logger.logInfo('get_struct. should return {}'.format(self.data_struct))
		return(self.data_struct)

	def get_valuetype(self):
		self.logger.logInfo('get_valuetype. should return {}'.format(self.data_valuetype))
		return(self.data_valuetype)

	def get_database_value(self):
		cdbElement = self.getCDBElement('alma/TemplatePy', 'TemplatePy')
		self.logger.logDebug(cdbElement)
		x = float(cdbElement[0]['example_val'])
		self.logger.logInfo('get_database_value. should return {}'.format(x))
		return(x)
	
	def set_double(self, val):
		self.logger.logInfo('set_double to object of type {}'.format(type(val)))
		self.__val_private = float(val)
		self.val_public = 2*float(val)
		self.logger.logInfo('value set to {}'.format(self.__val_private))

	def set_enum(self, val):
		self.logger.logInfo('set_enum to object of type {}'.format(type(val)))
		self.data_enum = val
		self.logger.logInfo('value set to {}'.format(self.data_enum))

	def set_struct(self, val):
		self.logger.logInfo('set_struct to object of type {}'.format(type(val)))
		self.data_struct = val
		self.logger.logInfo('value set to {}'.format(self.data_struct))

	def set_valuetype_simple(self, val):
		self.logger.logInfo('set_valuetype_simple')
		self.data_valuetype = val
		self.logger.logInfo('value set to {}'.format(self.data_valuetype))

	def set_valuetype_inherit(self, val):
		self.logger.logInfo('set_valuetype_inherit')
		self.data_valuetype = val
		self.logger.logInfo('value set to {}'.format(self.data_valuetype))

	def test_exception(self):
		# Raise an exception using the ACS Exception stack
		# NOTE the name must have "Impl" at the end of the module name and Exception class when we
		# create an object of this type..
		# Because OmniORB auto-generates the Python implementation of these classes from
		# the original TemplatePyError.idl and appends "Impl" to the actual Python classes that we use..
		try:
			raise TemplatePyErrorImpl.ExampleErrorExImpl
		except TemplatePyError.ExampleErrorEx as e:
			self.logger.logInfo("Caught the correct type of exception locally in TemplatePy: {}".format(e))
			new_exception = TemplatePyErrorImpl.ExampleErrorExImpl(exception=e)
			# Create a new exception and raise it again.  OmniORB should catch this and report to the
			# remote system.
			raise(new_exception)
		except Exception as e:
			self.logger.logError("Caught the wrong type of exception {}".format(e))

	def test_notification(self):
		## supplier ZMQ
		self.logger.logInfo('Creating ZMQ Supplier')
		self.zmq_supplier = Supplier(TemplatePyMod.NC_CHANNEL_NAME)

		## consumer ZMQ
		self.logger.logInfo('Creating ZMQ Consumer for channel {}: '.format(TemplatePyMod.NC_CHANNEL_NAME))
		self.zmq_consumer = Consumer(TemplatePyMod.NC_CHANNEL_NAME)
		self.zmq_consumer.add_subscription(self.handler_example)
	
		new_struct = TemplatePyMod.ExampleStruct(10, 22.2, 33.3, 44.4)
		self.logger.logInfo('send data_struct on NC channel: {}'.format(new_struct))
		self.zmq_supplier.publish_event(new_struct)
		time.sleep(0.2)
		self.logger.logDebug('current_nc_struct: {}'.format(self.current_nc_struct))
		if((self.current_nc_struct.sequence_counter == new_struct.sequence_counter) & (self.current_nc_struct.val1 == new_struct.val1) & (self.current_nc_struct.val2 == new_struct.val2) & (self.current_nc_struct.val3 == new_struct.val3)):
			result = True
		else:
			result = False

		self.logger.logDebug('disconnect ZMQ Consumer for channel: {}'.format(TemplatePyMod.NC_CHANNEL_NAME))
		try:
			self.zmq_consumer.disconnect()
		except:
			self.zmq_consumer = None
		
		self.logger.logDebug('disconnect ZMQ Supplier for channel: {}'.format(TemplatePyMod.NC_CHANNEL_NAME))
		try:
			self.zmq_supplier.disconnect()
		except:
			self.zmq_supplier = None

		return(result)

	#------------------------------------------------------------------------------------
	# Internal functions not exposed in IDL
	def handler_example(self, data_struct):
		lock = threading.Lock()
		lock.acquire()
		self.logger.logDebug('received event: {}'.format(data_struct))
		self.current_nc_struct = data_struct
		lock.release()
		
if __name__ == "__main__":
	t = TemplatePy()