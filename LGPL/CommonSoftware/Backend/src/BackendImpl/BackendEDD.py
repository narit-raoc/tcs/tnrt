# -*- coding: utf-8 -*-
# import standard modules
import numpy as np
import json
from json.decoder import JSONDecodeError
import threading
import logging

# import ACS modules
from Acspy.Servants.ACSComponent import ACSComponent
from Acspy.Servants.ContainerServices import ContainerServices
from Acspy.Servants.ComponentLifecycle import ComponentLifecycle
from Acspy.Nc.Supplier import Supplier
from Acspy.Clients.SimpleClient import PySimpleClient

# import TNRT modules
import BackendMod__POA
import BackendMod
import BackendErrorImpl
import DataAggregatorError

# Default values and config (ip addresses, ...)
from CentralDB import CentralDB
import BackendDefaults as config
import cdbConstants
import default_param_edd

# import mpikat modules
import tcs_usb_interface


class BackendEDD(BackendMod__POA.BackendEDD, ACSComponent, ContainerServices, ComponentLifecycle):
    """
    docstring
    """

    def __init__(self):
        ACSComponent.__init__(self)
        ContainerServices.__init__(self)
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.logInfo(
            "test logger in class {}, module {}".format(self.__class__.__name__, __name__)
        )

        self.simulate_scan_supplier = None  # Need this to set number of channels for DataAggregator array when we simulate data.

        # Flow control events
        self.event_stop_status_notification_loop = threading.Event()

        try:
            self.cdb = CentralDB()
        except Exception as e:
            acs_corba_exception =  BackendErrorImpl.ConnectionErrorExImpl()
            acs_corba_exception.addData(str(e))
            raise acs_corba_exception
        

        self.logger.logInfo("__init__ (constructor) complete")

    # ------------------------------------------------------------------------------------
    # LifeCycle functions called by ACS system when we activate and deactivate components.
    # Inherited from headers in Acspy.Servants.ComponentLifecycle.
    # ------------------------------------------------------------------------------------

    def initialize(self):
        self.logger.logInfo("initialize() start")

        self.logger.logInfo("Read host IP and port from config file BackendDefaults.py")

        self.edd_master_controller_host = config.edd_master_controller_host
        self.edd_master_controller_port = config.edd_master_controller_port
        self.logger.logInfo(
            "Found edd_master_controller_host = {}, edd_master_controller_port = {}".format(
                self.edd_master_controller_host, self.edd_master_controller_port
            )
        )

        self.edd_redis_host = config.edd_redis_host
        if self.edd_redis_host == "192.168.90.11":
            
            self.edd_redis_port = int(6333)
            self.logger.logInfo('Using redis port 6333')
        
        elif self.edd_redis_host == "192.168.90.12":
            
            self.edd_redis_port = int(6379)
            self.logger.logInfo('Using redis port 6379')
        else:
            self.logger.logInfo('error comfrom redis port not avialble')

        self.logger.logInfo(
            "Found edd_redis_host = {}, edd_redis_port = {}".format(
                self.edd_redis_host, self.edd_redis_port
            )
        )

        self.logger.logInfo(
            "Creating Notification Channel Suppler {}".format(BackendMod.CHANNEL_NAME_STATUS)
        )
        self.status_supplier = Supplier(BackendMod.CHANNEL_NAME_STATUS)

        self.sc = PySimpleClient()
        self.da = None
        self.da_name = "DataAggregator"
        self.da = self.connect_component(self.da_name)

        self.logger.logInfo("initialize() finished")

    def execute(self):
        # Do nothing.  Use initialize() function
        pass

    def aboutToAbort(self):
        # Do nothing here.   Use cleanup() function
        pass

    def cleanUp(self):
        self.logger.logInfo("cleanup() start")

        self.logger.logInfo("Disconnect ACS Notification suppliers")

        if self.status_supplier != None:
            self.status_supplier.disconnect()

        try:
            self.sc.releaseComponent(self.da_name)
            self.sc.disconnect()
            self.da = None
        except Exception as e:
            self.logger.logError("Cannot release component reference for %s" % self.da_name)

        self.logger.logInfo("cleanup() finished")

    # ------------------------------------------------------------------------------------
    # IDL interface functions for use from outside this object
    # ------------------------------------------------------------------------------------

    def client_start(self):
        """
        @brief	This fuction is try to connecting the USB with tcs_usb_interface.py
                        Initail ip, port, redis in def __init__(self):
        """
        # Connect to Master Controller
        # TODO - handle failed connections and timeout.
        try:
            self.logger.logInfo(
                "Connect katcp client to edd master controller {}:{}, redis {}:{}".format(
                    self.edd_master_controller_host,
                    self.edd_master_controller_port,
                    self.edd_redis_host,
                    self.edd_redis_port,
                )
            )
            self.edd_master_controller_client = tcs_usb_interface.TcsUsbInterface(
                self.edd_master_controller_host,
                self.edd_master_controller_port,
                self.edd_redis_host,
                self.edd_redis_port,
            )
        except ConnectionError as e:
            self.logger.logError(e)
            raise BackendErrorImpl.ConnectionErrorExImpl()

    def client_stop(self):
        """
        @brief	This fuction is try to disconnect the USB interface (stop the socket that open)
        """
        try:
            self.logger.logInfo("Stop client connection to master controller")
            self.edd_master_controller_client.stop()
        except Exception as e:
            self.logger.logError(str(e))
            self.logger.logWarning("Do nothing. Continue test")

    def configure(self, config_json):
        """
        @brief      Configure EDD to receive and process data

        @return     katcp reply object [[[ !configure ok | (fail [error description]) ]]]
        """
        # Write data to redis CDB to prepare for data file headers
        config_dict = json.loads(config_json)
        self.logger.logInfo("config dict: {}".format(config_dict))

        if "spectrometer" in self.preset_config_name.lower():
            sampling_rate = default_param_edd.SPECTROMETER_SAMPLING_RATE_DEFAULT
            self.logger.debug("default sampling_rate: {}".format(sampling_rate))
        
            ref_channel = default_param_edd.SPECTROMETER_REF_CHANNEL_DEFAULT
            self.logger.debug("default ref_channel: {}".format(ref_channel))

            predecimation_factor = default_param_edd.preset_config_name_setting[self.preset_config_name]['predecimation_factor']
            self.logger.debug("default predecimation_factor: {}".format(predecimation_factor))
            
            decimated_sampling_rate = sampling_rate / predecimation_factor
            self.logger.debug("default decimated_sampling_rate: {}".format(decimated_sampling_rate))

            f0_nyquist_zone_2 = decimated_sampling_rate / 2
            self.logger.debug("default f0_nyquist_zone_2: {}".format(f0_nyquist_zone_2))

            # Get downconverter frequency that was set by Frontend
            down_converter_freq = self.cdb.get(cdbConstants.DOWN_CONVERTER_FREQ, float) or 0
            self.logger.debug("cdb down_converter_freq: {}".format(down_converter_freq))

            # Get fft_length and naccumulate from the first product name
            # If there are more than 1 product in the list, don't use.
            product_name = list(config_dict["products"].keys())[0]
            self.logger.logInfo("product_name: {}".format(product_name))
            fft_length = config_dict["products"][product_name]["fft_length"]
            naccumulate = config_dict["products"][product_name]["naccumulate"]

            actual_integration_time = naccumulate * (predecimation_factor / sampling_rate) * fft_length
            self.logger.debug("actual_integration_time: {}".format(actual_integration_time))
            
            actual_freq_res = decimated_sampling_rate / fft_length
            self.logger.debug("actual_freq_res: {}".format(actual_freq_res))
            
            rf_freq_at_ref_channel = f0_nyquist_zone_2 + (ref_channel * actual_freq_res) + down_converter_freq
            self.logger.debug("rf_freq_at_ref_channel: {}".format(rf_freq_at_ref_channel))

            # NOTE:  About calculation of CHANNELS for .mbfits ARRAYDATA header
            # - In the case of TCS DataSimulator, nchannels = fft_length 
            # because the simulator generates complex valued data and uses both 1st and 2nd nyquist zones.
            # - In the case of the real EDD Spectrometer, nchannels = fft_length / 2.
            # because it performs the FFT on real-valued data and sends only the second Nyquist zone data 
            # out of edd_fits_server
            nchannels = int(fft_length / 2)
            self.logger.debug("nchannels: {}".format(nchannels))

            try:
                ## Set metadata to cdb
                self.cdb.set(cdbConstants.NUSEFEED, default_param_edd.preset_config_name_setting[self.preset_config_name]['nusefeed'])
                self.cdb.set(cdbConstants.REF_CHANNEL, ref_channel)
                self.cdb.set(cdbConstants.FREQRES, actual_freq_res)
                self.cdb.set(cdbConstants.INTEGRATION_TIME, actual_integration_time)
                self.cdb.set(cdbConstants.CHANNELS, nchannels)
                self.cdb.set(cdbConstants.FREQ_AT_REF_CH, rf_freq_at_ref_channel)

                # Get back CDB values, log some debug messages.
                self.logger.debug("cdb NUSEFEED: {}".format(self.cdb.get(cdbConstants.NUSEFEED, int)))
                self.logger.debug("cdb REF_CHANNEL: {}".format(self.cdb.get(cdbConstants.REF_CHANNEL, int)))
                self.logger.debug("cdb FREQRES: {}".format(self.cdb.get(cdbConstants.FREQRES, float)))
                self.logger.debug("cdb INTEGRATION_TIME: {}".format(self.cdb.get(cdbConstants.INTEGRATION_TIME, float)))
                self.logger.debug("cdb CHANNELS: {}".format(self.cdb.get(cdbConstants.CHANNELS, int)))
                self.logger.debug("cdb FREQ_AT_REF_CH: {}".format(self.cdb.get(cdbConstants.FREQ_AT_REF_CH, float)))
            except Exception as e:
                acs_corba_exception =  BackendErrorImpl.InternalErrorExImpl()
                acs_corba_exception.addData(str(e))
                raise acs_corba_exception


        # check if same config is applied
        pipeline_status = self.edd_master_controller_client.get_pipeline_status()
        if pipeline_status != 'idle':
            current_config = self.edd_master_controller_client.get_current_config()
            config_not_change = self.compare_config(current_config, config_json)
            if config_not_change and pipeline_status != 'error':
                self.logger.logInfo('Same config, do not reconfigure')
                return ""
        
            self.logger.logInfo('Deconfiguring old config..')
            self.deconfigure()
        
        self.logger.logInfo("Sending new configure message: {}".format(config_json))
        reply = str(self.edd_master_controller_client.configure(config_json))
        return self.edd_master_controller_client.decode_katcp_message(reply.__str__())

    def deconfigure(self):
        """
        @brief      De-configure EDD from last config

        @return     katcp reply object [[[ !configure ok | (fail [error description]) ]]]
        """
        # TODO: handel when fail reply
        reply = self.edd_master_controller_client.deconfigure()
        self.logger.logInfo("type(reply): {}, reply: {}".format(type(reply), reply))
        return self.edd_master_controller_client.decode_katcp_message(reply.__str__())

    def capture_start(self):
        """
        @brief      Start the EDD backend processing

        @note       This method may be updated in future to pass a 'scan configuration' containing
                                source and position information necessary for the population of output file
                                headers.

        @note       This is the KATCP wrapper for the capture_start command

        @return     katcp reply object [[[ !capture_start ok | (fail [error description]) ]]]
        """
        # TODO: handel when fail reply
        current_status = self.edd_master_controller_client.get_pipeline_status()
        if current_status == 'ready':
            self.logger.logInfo('pipeline status is in ready state, skip capture_start')
            return ""
        reply = self.edd_master_controller_client.capture_start()
        self.logger.logInfo("type(reply): {}, reply: {}".format(type(reply), reply))
        return self.edd_master_controller_client.decode_katcp_message(reply.__str__())

    def capture_stop(self):
        """
        @brief      Stop the EDD backend processing

        @note       This is the KATCP wrapper for the capture_stop command

        @return     katcp reply object [[[ !capture_stop ok | (fail [error description]) ]]]
        """
        # TODO: handel when fail reply

        reply = self.edd_master_controller_client.capture_stop()
        self.logger.logInfo("type(reply): {}, reply: {}".format(type(reply), reply))
        return self.edd_master_controller_client.decode_katcp_message(reply.__str__())

    def measurement_prepare(self, config_json):
        try:
            config_dict = json.loads(config_json)
        except JSONDecodeError:
            self.logger.logWarning("JSONDecodeError. empty string?  Send empty config dict")
            config_dict = {}

        self.logger.logInfo("measurement prepare dict: {}".format(config_dict))
        reply = self.edd_master_controller_client.measurement_prepare(config_dict)
        self.logger.logInfo("type(reply): {}, reply: {}".format(type(reply), reply))
        return self.edd_master_controller_client.decode_katcp_message(reply.__str__())

    def measurement_start(self):
        # start edd fits client here to connect to edd fits server 
        # prepare to get data immediatly after measurement_start
        try:
            current_provision = self.edd_master_controller_client.get_current_provision()
            self.logger.logInfo("current provision name is {}".format(current_provision))
            if("spectrometer" in current_provision):
                self.logger.logInfo('"spectrometer" in current provision name.  start edd fits client ...')
                self.da.fits_client_start()
                self.logger.logInfo('Done')
            else:
                self.logger.logInfo('"spectrometer" not in current provision name.  do not start edd fits client')

            reply = self.edd_master_controller_client.measurement_start()
            self.logger.logInfo("type(reply): {}, reply: {}".format(type(reply), reply))
            return self.edd_master_controller_client.decode_katcp_message(reply.__str__())
        except DataAggregatorError.ConnectionRefusedErrorEx as e:
            self.logger.logError(e)
            raise BackendErrorImpl.ConnectionErrorExImpl
        except Exception as e:
            self.logger.logError(e)
            raise BackendErrorImpl.InternalErrorExImpl
        
    def measurement_stop(self):
        self.da.fits_client_stop()
        reply = self.edd_master_controller_client.measurement_stop()
        self.logger.logInfo("type(reply): {}, reply: {}".format(type(reply), reply))
        return self.edd_master_controller_client.decode_katcp_message(reply.__str__())

    def provision(self, preset_config_name):
        self.preset_config_name = preset_config_name
        current_state = self.edd_master_controller_client.get_pipeline_status()
        
        if(current_state != 'unprovisioned'):
            current_provision = self.edd_master_controller_client.get_current_provision()

            if(current_provision == preset_config_name):
                self.logger.logInfo('Same provision, skip provisioning')
                return ""

            self.deprovision()
        try:
            reply = self.edd_master_controller_client.provision(preset_config_name)

            reply_decode = self.edd_master_controller_client.decode_katcp_message(reply.__str__())
            reply_status = reply_decode.split(" ")
            # self.logger.logInfo("type(reply_decode): {}, rereply_decodeply: {}".format(type(reply_decode), reply_decode))
            self.logger.logInfo("type(reply_status): {}, reply: {}".format(type(reply_status[1]), reply_status[1]))
            
            if reply_status[1] != "ok":
                self.deconfigure()
                self.deprovision()
                reply = self.edd_master_controller_client.provision(preset_config_name)
                reply_decode = self.edd_master_controller_client.decode_katcp_message(reply.__str__())
                reply_status = reply_decode.split(" ")
                if reply_status[1] != "ok":
                    self.beep()
                    
            self.logger.logInfo("type(reply): {}, reply: {}".format(type(reply), reply))
            return self.edd_master_controller_client.decode_katcp_message(reply.__str__())
        except Exception as e:
            self.logger.logError(e)

            raise BackendErrorImpl.InternalErrorExImpl()

    def deprovision(self):
        reply = self.edd_master_controller_client.deprovision()
        self.logger.logInfo("type(reply): {}, reply: {}".format(type(reply), reply))
        return self.edd_master_controller_client.decode_katcp_message(reply.__str__())

    def metadata_update(self, metadata_json):
        metadata_dict = json.loads(metadata_json)
        self.logger.logInfo("metadata_update dict: {}".format(metadata_dict))
        self.edd_master_controller_client.metadata_update(metadata_dict)

    def help(self):
        """
        @brief	Send help command to the katcp server

        @reture help argument
        """
        reply = self.edd_master_controller_client.help()
        return reply

    def status_loop_start(self, status_period):
        self.status_period = status_period
        self.status_message_count = 0
        self.thread_status_notification = threading.Thread(target=self._status_notification_loop)
        self.logger.logInfo("Start _status_notification_loop")
        self.event_stop_status_notification_loop.clear()
        self.thread_status_notification.start()

    def status_loop_stop(self):
        self.logger.logInfo("Stop _status_notification_loop")
        self.event_stop_status_notification_loop.set()

    def my_dspsr_test(self):
        """
        @brief      Start simulate dsosr process

        @note       This will be delete next. need docker to run on this process

        @return     result of dspsr process
        """
        # TODO: handel when fail reply

        reply = self.edd_master_controller_client.my_dspsr_test()
        self.logger.logInfo(reply)

    # ------------------------------------------------------------------------------------
    # Internal functions (not available from IDL interface)
    # ------------------------------------------------------------------------------------
    def compare_config(self, current, new):
        '''
        Compare if the the current config and new config are the same

        return Boolean, True if they are the same, False if they aren't
        '''
        try:
            new_config_obj = json.loads(new)
            current_config_obj = json.loads(current)

            new_gated_spectrometer_0_fft_length = new_config_obj['products']['gated_spectrometer_0']['fft_length']
            new_gated_spectrometer_0_naccumulate = new_config_obj['products']['gated_spectrometer_0']['naccumulate']

            new_gated_spectrometer_1_fft_length = new_config_obj['products']['gated_spectrometer_1']['fft_length']
            new_gated_spectrometer_1_naccumulate = new_config_obj['products']['gated_spectrometer_1']['naccumulate']
                
            current_gated_spectrometer_0_fft_length = current_config_obj['products']['gated_spectrometer_0']['fft_length']
            current_gated_spectrometer_0_naccumulate = current_config_obj['products']['gated_spectrometer_0']['naccumulate']

            current_gated_spectrometer_1_fft_length = current_config_obj['products']['gated_spectrometer_1']['fft_length']
            current_gated_spectrometer_1_fft_naccumulate = current_config_obj['products']['gated_spectrometer_1']['naccumulate']

            if new_gated_spectrometer_0_fft_length != current_gated_spectrometer_0_fft_length:
                return False
            if new_gated_spectrometer_0_naccumulate != current_gated_spectrometer_0_naccumulate:
                return False
            if new_gated_spectrometer_1_fft_length != current_gated_spectrometer_1_fft_length:
                return False
            if new_gated_spectrometer_1_naccumulate != current_gated_spectrometer_1_fft_naccumulate:
                return False

            return True
        except: 
            return False

    def connect_component(self, component_name):
        try:
            self.logger.debug("Connecting to ACS component %s" % component_name)
            return self.sc.getComponent(component_name)
        except Exception as e:
            self.logger.error("Cannot get ACS component object reference for %s" % component_name)

    def _status_notification_loop(self):
        # Loop through data array, create EDD fits notification messages
        while not self.event_stop_status_notification_loop.is_set():
            self.status_message_count = self.status_message_count + 1
            self.logger.logInfo(
                "publish backend status.counter = {} (TODO add useful data to status notification)".format(
                    self.status_message_count
                )
            )

            # Assemble a complete packet in the required format publish
            status_message = BackendMod.StatusNotifyBlock(int(self.status_message_count))

            self.status_supplier.publishEvent(status_message)

            # use wait instead of sleep because it can cancel immediately if anothe thread calls function stop_simulate
            self.event_stop_status_notification_loop.wait(timeout=self.status_period)

    def beep(self):
        '''
        Compare if the the current config and new config are the same

        return Boolean, True if they are the same, False if they aren't
        '''

        duration = 1.5  # seconds
        freq = 700  # Hz

        return True

# Main defined only for testing with data in native format
# Not passing data throuth IDL type translations
if __name__ == "__main__":
    s = BackendEDD()
    s.client_start()
    current_config = s.edd_master_controller_client.get_current_config()
    print(current_config)
