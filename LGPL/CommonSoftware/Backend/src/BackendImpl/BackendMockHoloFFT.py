# -*- coding: utf-8 -*-
# import standard modules
import json
from json.decoder import JSONDecodeError
import threading
import logging
import numpy as np
from astropy.time import Time


# import ACS modules
from Acspy.Servants.ACSComponent import ACSComponent
from Acspy.Servants.ContainerServices import ContainerServices
from Acspy.Servants.ComponentLifecycle import ComponentLifecycle
from AcsutilPy.ACSPorts  import getIP

from Supplier import Supplier

# import TNRT modules
import BackendMod__POA
import BackendMod
import BackendErrorImpl
from BackendDefaults import generate_dtype_msg_holodata
from CentralDB import CentralDB
import cdbConstants
from DataSimulator import HolographySimulator

class BackendMockHoloFFT(
    BackendMod__POA.BackendMockHoloFFT, ACSComponent, ContainerServices, ComponentLifecycle
):
    """
    docstring
    """

    def __init__(self):
        ACSComponent.__init__(self)
        ContainerServices.__init__(self)
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.logInfo("test logger in class {}, module {}".format(self.__class__.__name__, __name__))

        self.ip = getIP()

        self.state = "disconnected"
        self.measurement_state = "idle"

        # Flow control events
        self.event_stop_status_notification_loop = threading.Event()
        self.event_stop_measurement = threading.Event()

        self.cdb = CentralDB()
        
        self.logger.logInfo("__init__ (constructor) complete")

    # ------------------------------------------------------------------------------------
    # LifeCycle functions called by ACS system when we activate and deactivate components.
    # Inherited from headers in Acspy.Servants.ComponentLifecycle.
    # ------------------------------------------------------------------------------------

    def initialize(self):
        self.logger.logInfo("initialize() start")

        self.logger.logInfo(
            "Creating Notification Channel Suppler {}".format(BackendMod.CHANNEL_NAME_STATUS)
        )
        self.status_supplier = Supplier(BackendMod.CHANNEL_NAME_STATUS)
        self.data_supplier = Supplier(BackendMod.CHANNEL_NAME_HOLO_DATA)
        self.logger.logInfo("initialize() finished")

    def execute(self):
        # Do nothing.  Use initialize() function
        pass

    def aboutToAbort(self):
        # Do nothing here.   Use cleanup() function
        pass

    def cleanUp(self):
        self.logger.logInfo("cleanup() start")
        self.logger.logInfo("Disconnect ACS Notification suppliers")

        if self.status_supplier != None:
            self.status_supplier.disconnect()

        if self.data_supplier != None:
            self.data_supplier.disconnect()

        self.logger.logInfo("Disconnect ACS Notification consumers")
        self.logger.logInfo("cleanup() finished")

    # ------------------------------------------------------------------------------------
    # IDL interface functions for use from outside this object
    # ------------------------------------------------------------------------------------

    def get_ip(self):
        return self.ip
    
    def get_state(self):
        return self.state

    def get_measurement_state(self):
        return self.measurement_state

    def client_start(self):
        self.state = "connected"
        self.logger.logInfo("mock state = connected")

    def client_stop(self):
        if self.state == "disconnected":
            raise BackendErrorImpl.ConnectionErrorExImpl()
        
        self.state = "disconnected"
        self.logger.logInfo("mock state = disconnected")

    def provision(self, prest_config_name):
        if self.state == "disconnected":
            raise BackendErrorImpl.ConnectionErrorExImpl()

        self.state = "idle"
        self.logger.logInfo("mock state = idle")
        return "provision success"

    def deprovision(self):
        if self.state == "disconnected":
            raise BackendErrorImpl.ConnectionErrorExImpl()

        self.state = "connected"
        self.logger.logInfo("mock state = connected")
        return "deprovision success"

    def configure(self, config_json):
        allowed_state = ["idle"]

        if self.state == "disconnected":
            raise BackendErrorImpl.ConnectionErrorExImpl()

        if self.state not in allowed_state:
            self.logger.logError('Wrong Mode: state "{}" is not allowed'.format(self.state))
            raise BackendErrorImpl.WrongModeErrorExImpl()

        try:
            config_dict = json.loads(config_json)
            self.logger.logInfo("config_json: {}".format(config_json))
            self.logger.logInfo("config_dict: {}".format(config_dict))

            # Calculate parameters necessary for data simulation
            # and set current metadata values
            self.fft_length = config_dict["fft_length"]
            self.freq_res = config_dict["freq_res"]
            self.measurement_duration = 1 / self.freq_res

            # Set current values of metadata in redis database
            self.cdb.set(cdbConstants.FREQRES, self.freq_res)
            self.cdb.set(cdbConstants.CHANNELS, self.fft_length)
            self.cdb.set(cdbConstants.INTEGRATION_TIME, self.measurement_duration)
            
            # Simulate data arrays.  In the real system, FFT35760A.py will generate
            # the packets from actual signal data and stream the packets.
            # Hardcode some reasonable values for software testing.  
            # This is not a full-function simulator
            
            fwhm = 160
            angular_resolution = fwhm / 2
            line_length = fwhm * 8
            integrations_per_line = int(line_length / angular_resolution)
            time_per_angular_step = 0.5  # should be same as receiver integration time
            subscans_per_cal = 1
            integrations_per_calN = 10
            integrations_per_cal0 = 30
            peak_magntidue_ref = 8.5e-6  # mean value of first calibration in Yebes file TEST5772.FITS
            peak_magntidue_tst = 5.8e-5  # mean value of first calibration in Yebes file TEST5772.FITS
            
            self.hsim = HolographySimulator(
                integrations_per_line,
                angular_resolution,
                time_per_angular_step,
                subscans_per_cal,
                integrations_per_calN,
                integrations_per_cal0,
                peak_magntidue_ref,
                peak_magntidue_tst,
                fwhm,
            )

            self.state = "configured"
            return "configuration success"
        except Exception as e:
            self.state = "error"
            self.logger.logError(str(e))
            raise BackendErrorImpl.InternalErrorExImpl()

    def deconfigure(self):
        allowed_state = ["configured", "streaming", "error"]

        if self.state == "disconnected":
            raise BackendErrorImpl.ConnectionErrorExImpl()

        if self.state not in allowed_state:
            self.logger.logError('Wrong Mode: state "{}" is not allowed'.format(self.state))
            raise BackendErrorImpl.WrongModeErrorExImpl()

        try:
            self.state = "idle"
            self.measurement_state = "idle"
            return "reset complete"
        except Exception as e:
            self.state = "error"
            self.logger.logError(str(e))
            raise BackendErrorImpl.InternalErrorExImpl()

    def capture_start(self):
        allowed_state = ["configured"]

        if self.state == "disconnected":
            raise BackendErrorImpl.ConnectionErrorExImpl()

        if self.state not in allowed_state:
            self.logger.logError('Wrong Mode: state "{}" is not allowed'.format(self.state))
            raise BackendErrorImpl.WrongModeErrorExImpl()

        self.state = "streaming"
        self.measurement_state = "ready"
        return "capture start success"

    def capture_stop(self):
        allowed_state = ["streaming"]

        if self.state == "disconnected":
            raise BackendErrorImpl.ConnectionErrorExImpl()

        if self.state not in allowed_state:
            self.logger.logError('Wrong Mode: state "{}" is not allowed'.format(self.state))
            raise BackendErrorImpl.WrongModeErrorExImpl()

        self.state = "idle"
        self.measurement_state = "idle"
        return "capture stop success"

    def measurement_prepare(self, config_json):
        allowed_state = ["ready"]

        if self.state == "disconnected":
            raise BackendErrorImpl.ConnectionErrorExImpl()

        if self.measurement_state not in allowed_state:
            self.logger.logError(
                'Wrong Mode: state "{}" is not allowed'.format(self.measurement_state)
            )
            raise BackendErrorImpl.WrongModeErrorExImpl()

        self.measurement_state = "set"
        return "measurement prepare success"

    def measurement_start(self):
        allowed_state = ["set"]

        if self.state == "disconnected":
            raise BackendErrorImpl.ConnectionErrorExImpl()

        if self.measurement_state not in allowed_state:
            self.logger.logError(
                'Wrong Mode: state "{}" is not allowed'.format(self.measurement_state)
            )
            raise BackendErrorImpl.WrongModeErrorExImpl()

        self.event_stop_measurement.clear()
        self.thread_data_notification = threading.Thread(
            target=self._data_notification_loop,
            args=[self.measurement_duration],  # measurement duration defines the loop period
        )
        self.thread_data_notification.daemon = True
        self.logger.logInfo("Starting data notification loop...")
        self.thread_data_notification.start()
        self.measurement_state = "running"
        return "measurement start success"

    def measurement_stop(self):
        allowed_state = ["running"]

        if self.state == "disconnected":
            raise BackendErrorImpl.ConnectionErrorExImpl()

        if self.measurement_state not in allowed_state:
            self.logger.logError(
                'Wrong Mode: state "{}" is not allowed'.format(self.measurement_state)
            )
            raise BackendErrorImpl.WrongModeErrorExImpl()

        self.logger.logInfo("Stopping data notification loop...")
        self.event_stop_measurement.set()
        self.measurement_state = "ready"
        return "measurement stop success"

    def metadata_update(self, metadata_json):
        pass

    def status_loop_start(self, status_period):
        self.status_period = status_period
        self.status_message_count = 0
        self.thread_status_notification = threading.Thread(target=self._status_notification_loop)
        self.logger.logInfo("Start _status_notification_loop")
        self.event_stop_status_notification_loop.clear()
        self.thread_status_notification.start()

    def status_loop_stop(self):
        self.logger.logInfo("Stop _status_notification_loop")
        self.event_stop_status_notification_loop.set()

    # ------------------------------------------------------------------------------------
    # Internal functions (not available from IDL interface)
    # ------------------------------------------------------------------------------------

    def _status_notification_loop(self):
        # Loop through data array, create EDD fits notification messages
        while not self.event_stop_status_notification_loop.is_set():
            self.status_message_count = self.status_message_count + 1
            self.logger.logDebug(
                "publish backend status.counter = {}".format(
                    self.status_message_count
                )
            )

            # Assemble a complete packet in the required format publish
            status_message = BackendMod.StatusNotifyBlock(int(self.status_message_count))

            self.status_supplier.publish_event(status_message)

            # use wait instead of sleep because it can cancel immediately if anothe thread calls function stop_simulate
            self.event_stop_status_notification_loop.wait(timeout=self.status_period)

    def _data_notification_loop(self, sample_capture_duration):
        self.logger.logInfo("data notification loop is started")
        
        # Generate numpy array datatype for 1024 points of FFT
        fft_length = 128
        dtype_msg_holodata = generate_dtype_msg_holodata(fft_length)

        # create msg_holodate numpy array of zeros.  Use np.squeeze to reduce arrays lengh=1 to be scalar values
        msg_holodata = np.squeeze(np.zeros(1, dtype=dtype_msg_holodata))

        sample_rate = 51.2e3 # default sample rate of Keysight FFT analyzer before decimation (DDC)        
        freq_ref = 0
        freq_tst = 0
        freq_ddc = 20e3
        noise_magnitude_ref = 1e-7 # I don't know the real system values.  make something convenient for sim
        noise_magnitude_tst = 1e-9

        # A series of time points to generate all signals for spectrum simulation
        tt = np.arange(0, fft_length) / sample_rate
        
        msg_holodata["sample_capture_duration"] = self.hsim.time_per_angular_step    
        msg_holodata["spectrum_freq_axis"] = np.linspace(-1 * sample_rate / 2, sample_rate/ 2 , fft_length, endpoint=False) + freq_ddc

        self.logger.logDebug(
            "Start loop to publish hsim data every {:.2f} seconds and repeat until stopped".format(
                msg_holodata["sample_capture_duration"]
            )
        )

        while not self.event_stop_measurement.is_set():
            # NOTE: This Mock backend doesn't know about center cal "CORR" subscans.
            # It only loops through "HOLO" line scan data across the target so
            # we can see some simulation data with shapes and colors for software
            # testing.  The data is not syncrhonized to subscans.  Just a dumb infinite
            # loop to exercise the functions and data structures for software testing.

            y = 0
            win = np.hamming(fft_length)

            for otf_map_row in self.hsim.otf_map:
                for x in range(self.hsim.npoints_per_otf_line):
                    self.event_stop_measurement.wait(timeout=sample_capture_duration)
                    msg_holodata["time_mjd"] = Time.now().mjd

                    ref_magnitude = np.abs(otf_map_row["ref_antenna_signal_watts"][x])
                    tst_magnitude = np.abs(otf_map_row["tst_antenna_signal_watts"][x])
                    ref_phase = np.angle(otf_map_row["ref_antenna_signal_watts"][x])
                    tst_phase = np.angle(otf_map_row["tst_antenna_signal_watts"][x])
                    
                    sig_ref = ref_magnitude * np.exp(1j * (2 * np.pi * freq_ref * tt + ref_phase))
                    sig_tst = tst_magnitude * np.exp(1j * (2 * np.pi * freq_tst * tt + tst_phase))

                    # Simulate noise floor and add to the pure signal
                    noise_ref = noise_magnitude_ref * (np.random.normal(size=tt.shape[0]) + 1j * np.random.normal(size=tt.shape[0]))
                    noise_tst = noise_magnitude_tst * (np.random.normal(size=tt.shape[0]) + 1j * np.random.normal(size=tt.shape[0]))
                    sn_ref = sig_ref + noise_ref
                    sn_tst = sig_tst + noise_tst

                    # Save time-series phase data for visualization of frequency tracking
                    msg_holodata["timeseries_phase_ch1_rad"] = np.angle(sn_ref)
                    msg_holodata["timeseries_phase_ch2_rad"] = np.angle(sn_tst)
                    
                    # Calculate FFT spectrum
                    spectrum_ref_complex = np.fft.fftshift(np.fft.fft(sn_ref * win)) / fft_length
                    spectrum_tst_complex = np.fft.fftshift(np.fft.fft(sn_tst * win)) / fft_length
                    spectrum_rel_complex = spectrum_tst_complex / spectrum_ref_complex
                    
                    msg_holodata["spectrum_absolute_magnitude_ch1_dbm"] = 30 + 10 * np.log10(np.abs(spectrum_ref_complex))
                    msg_holodata["spectrum_absolute_magnitude_ch2_dbm"] = 30 + 10 * np.log10(np.abs(spectrum_tst_complex))
                    msg_holodata["spectrum_relative_magnitude_db"] = 10*np.log10(np.abs(spectrum_rel_complex))
                    msg_holodata["spectrum_relative_phase_deg"] = np.degrees(np.angle(spectrum_rel_complex))

                    marker_index = np.argmax(msg_holodata["spectrum_absolute_magnitude_ch1_dbm"])
                    msg_holodata["marker_index"] = marker_index
                    msg_holodata["marker_val_freq_hz"] = msg_holodata["spectrum_freq_axis"][marker_index]
                    msg_holodata["marker_val_absolute_magnitude_ch1_dbm"] = msg_holodata["spectrum_absolute_magnitude_ch1_dbm"][marker_index]
                    msg_holodata["marker_val_absolute_magnitude_ch2_dbm"] = msg_holodata["spectrum_absolute_magnitude_ch2_dbm"][marker_index]
                    msg_holodata["marker_val_relative_magnitude_db"] = msg_holodata["spectrum_relative_magnitude_db"][marker_index]
                    msg_holodata["marker_val_phase_ch1_deg"] = np.degrees(np.angle(spectrum_ref_complex))[marker_index]
                    msg_holodata["marker_val_phase_ch2_deg"] = np.degrees(np.angle(spectrum_tst_complex))[marker_index]
                    msg_holodata["marker_val_relative_phase_deg"] = msg_holodata["spectrum_relative_phase_deg"][marker_index]

                    self.logger.debug(
                            "subscan ({:03d}/{:03d}) HOLO send message ({:03d}/{:03d})".format(
                                y + 1,
                                self.hsim.otf_map.shape[0],
                                x + 1,
                                otf_map_row.shape[0],
                            )
                        )
                    
                    self.data_supplier.publish_event(msg_holodata)

                y = y+1
                        
        self.logger.logInfo("data notification loop is terminated")


# Main defined only for testing with data in native format
# Not passing data throuth IDL type translations
if __name__ == "__main__":
    s = BackendMockHoloFFT()
