# -*- coding: utf-8 -*-
# import standard modules
from astropy.time import Time
import numpy as np
import json
from json.decoder import JSONDecodeError
import threading
import logging

# import ACS modules
from Acspy.Servants.ACSComponent import ACSComponent
from Acspy.Servants.ContainerServices import ContainerServices
from Acspy.Servants.ComponentLifecycle import ComponentLifecycle
from Acspy.Nc.Supplier import Supplier
from Acspy.Nc.Consumer import Consumer

# import TNRT modules
import BackendMod__POA
import BackendMod
import BackendErrorImpl
import BackendError

from EPM1913A import EPM1913A


class BackendPowerMeter(
    BackendMod__POA.BackendPowerMeter, ACSComponent, ContainerServices, ComponentLifecycle
):
    """
    docstring
    """

    def __init__(self):
        ACSComponent.__init__(self)
        ContainerServices.__init__(self)
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.logInfo(
            "test logger in class {}, module {}".format(self.__class__.__name__, __name__)
        )

        self.EPM = EPM1913A()
        self.state = "disconnected"
        self.measurement_state = "idle"
        
        # Flow control events
        self.event_stop_status_notification_loop = threading.Event()
        self.event_stop_measurement = threading.Event()

        self.logger.logInfo("__init__ (constructor) complete")

    # ------------------------------------------------------------------------------------
    # LifeCycle functions called by ACS system when we activate and deactivate components.
    # Inherited from headers in Acspy.Servants.ComponentLifecycle.
    # ------------------------------------------------------------------------------------

    def initialize(self):
        self.logger.logInfo("initialize() start")

        self.logger.logInfo(
            "Creating Notification Channel Suppler {}".format(BackendMod.CHANNEL_NAME_STATUS)
        )
        self.status_supplier = Supplier(BackendMod.CHANNEL_NAME_STATUS)
        self.data_supplier = Supplier(BackendMod.CHANNEL_NAME_POWERMETER_DATA)
        self.logger.logInfo("initialize() finished")

    def execute(self):
        # Do nothing.  Use initialize() function
        pass

    def aboutToAbort(self):
        # Do nothing here.   Use cleanup() function
        self.EPM = None
        self.RecordThread = None
        if self.mainNotificationSuppInstance != None:
            self.mainNotificationSuppInstance.disconnect()	
        self.stopRecorder()
    

    def cleanUp(self):
        self.logger.logInfo("cleanup() start")

        self.logger.logInfo("Disconnect ACS Notification suppliers")

        if self.RecordThreadFlag:
            self.RecordThreadFlag = False

        if self.status_supplier != None:
            self.status_supplier.disconnect()

        if self.data_supplier != None:
            self.data_supplier.disconnect()

        self.logger.logInfo("Disconnect ACS Notification consumers")

        self.logger.logInfo("cleanup() finished")

    # ------------------------------------------------------------------------------------
    # IDL interface functions for use from outside this object
    # ------------------------------------------------------------------------------------

    def get_state(self):
        return self.state

    def get_measurement_state(self):
        return self.measurement_state

    def client_start(self):
        if self.state == "disconnected":
            try:
                self.EPM.connect()
                self.state = "connected"
                self.logger.logInfo("Connected to EPM1913A Powermeter")
            except Exception as e:
                self.logger.logError(str(e))
                raise BackendErrorImpl.ConnectionErrorExImpl()
        else:
            self.logger.logInfo("Client is already started")

    def client_stop(self):
        if self.state == "disconnected":
            raise BackendErrorImpl.ConnectionErrorExImpl()

        try:
            self.EPM.disconnect()
            self.state = "disconnected"
            self.measurement_state = "idle"
            self.logger.logInfo("Disconnected to EPM1913A Powermeter")
        except Exception as e:
            self.logger.logError(str(e))
            raise BackendErrorImpl.InternalErrorExImpl()

    def provision(self, prest_config_name):
        if self.state == "disconnected":
            raise BackendErrorImpl.ConnectionErrorExImpl()

        self.state = "idle"
        return "provision success"

    def deprovision(self):
        if self.state == "disconnected":
            raise BackendErrorImpl.ConnectionErrorExImpl()

        self.state = "connected"
        return "deprovision success"

    def configure(self, config_json):
        allowed_state = ["idle"]

        if self.state == "disconnected":
            raise BackendErrorImpl.ConnectionErrorExImpl()

        if self.state not in allowed_state:
            self.logger.logError('Wrong Mode: state "{}" is not allowed'.format(self.state))
            raise BackendErrorImpl.WrongModeErrorExImpl()

        # {"integrationTime": 1}
        try:
            config = json.loads(config_json)
            self.integrationTime = config["integrationTime"]
            self.EPM.deviceSetup()

            # There are three different ways to query the power measurement using FETC?, MEAS?, and READ?.
            # In Free Run or Continuous mode, you can use either FETC? or MEAS? to query the power measurement. 
            # 1. configuring the remote interface
            # 2. zeroing and calibrating
                 # https://www.transcat.com/media/pdf/Keysight-N1913-1914A-programming-guide.pdf
                 # page 31-33  
            
            self.state = "configured"
            return "configuration success"
        except Exception as e:
            self.state = "error"
            self.logger.logError(str(e))
            raise BackendErrorImpl.InternalErrorExImpl()

    def deconfigure(self):
        allowed_state = ["configured", "streaming", "error"]

        if self.state == "disconnected":
            raise BackendErrorImpl.ConnectionErrorExImpl()

        if self.state not in allowed_state:
            self.logger.logError('Wrong Mode: state "{}" is not allowed'.format(self.state))
            raise BackendErrorImpl.WrongModeErrorExImpl()

        try:
            self.EPM.clear()
            self.state = "idle"
            self.measurement_state = "idle"
            return "reset complete"
        except Exception as e:
            self.state = "error"
            self.logger.logError(str(e))
            raise BackendErrorImpl.InternalErrorExImpl()

    def capture_start(self):
        allowed_state = ["configured"]

        if self.state == "disconnected":
            raise BackendErrorImpl.ConnectionErrorExImpl()

        if self.state not in allowed_state:
            self.logger.logError('Wrong Mode: state "{}" is not allowed'.format(self.state))
            raise BackendErrorImpl.WrongModeErrorExImpl()


        self.state = "streaming"
        self.measurement_state = "ready"
        return "capture start success"

    def capture_stop(self):
        allowed_state = ["streaming"]

        if self.state == "disconnected":
            raise BackendErrorImpl.ConnectionErrorExImpl()

        if self.state not in allowed_state:
            self.logger.logError('Wrong Mode: state "{}" is not allowed'.format(self.state))
            raise BackendErrorImpl.WrongModeErrorExImpl()

        self.state = "idle"
        self.measurement_state = "idle"
        return "capture stop success"

    def measurement_prepare(self, config_json):
        allowed_state = ["ready"]

        if self.state == "disconnected":
            raise BackendErrorImpl.ConnectionErrorExImpl()

        if self.measurement_state not in allowed_state:
            self.logger.logError(
                'Wrong Mode: state "{}" is not allowed'.format(self.measurement_state)
            )
            raise BackendErrorImpl.WrongModeErrorExImpl()

        self.measurement_state = "set"
        return "measurement prepare success"

    def measurement_start(self):
        allowed_state = ["set"]

        if self.state == "disconnected":
            raise BackendErrorImpl.ConnectionErrorExImpl()

        if self.measurement_state not in allowed_state:
            self.logger.logError(
                'Wrong Mode: state "{}" is not allowed'.format(self.measurement_state)
            )
            raise BackendErrorImpl.WrongModeErrorExImpl()

        
        status_period = self.integrationTime
        self.event_stop_measurement.clear()
        self.thread_data_notification = threading.Thread(
            target=self._data_notification_loop,
            args=[status_period],
        )
        self.thread_data_notification.daemon = True
        self.logger.logInfo("Starting data notification loop...")
        self.thread_data_notification.start()
        self.measurement_state = "running"
        return "measurement start success"

    def measurement_stop(self):
        allowed_state = ["running"]

        if self.state == "disconnected":
            raise BackendErrorImpl.ConnectionErrorExImpl()

        if self.measurement_state not in allowed_state:
            self.logger.logError(
                'Wrong Mode: state "{}" is not allowed'.format(self.measurement_state)
            )
            raise BackendErrorImpl.WrongModeErrorExImpl()

        self.logger.logInfo("Stopping data notification loop...")
        self.event_stop_measurement.set()
        self.measurement_state = "ready"
        return "measurement stop success"

    def metadata_update(self, metadata_json):
        pass

    def status_loop_start(self, status_period):
        self.status_period = status_period
        self.status_message_count = 0
        self.thread_status_notification = threading.Thread(target=self._status_notification_loop)
        self.logger.logInfo("Start _status_notification_loop")
        self.event_stop_status_notification_loop.clear()
        self.thread_status_notification.start()

    def status_loop_stop(self):
        self.logger.logInfo("Stop _status_notification_loop")
        self.event_stop_status_notification_loop.set()

    # ------------------------------------------------------------------------------------
    # Internal functions (not available from IDL interface)
    # ------------------------------------------------------------------------------------

    def _status_notification_loop(self):
        # Loop through data array, create EDD fits notification messages
        while not self.event_stop_status_notification_loop.is_set():
            self.status_message_count = self.status_message_count + 1
            self.logger.logInfo(
                "publish backend status.counter = {} (TODO add useful data to status notification)".format(
                    self.status_message_count
                )
            )

            # Assemble a complete packet in the required format publish
            status_message = BackendMod.StatusNotifyBlock(int(self.status_message_count))

            self.status_supplier.publishEvent(status_message)

            # use wait instead of sleep because it can cancel immediately if anothe thread calls function stop_simulate
            self.event_stop_status_notification_loop.wait(timeout=self.status_period)

    def _data_notification_loop(self, status_period):
        self.logger.logInfo("data notification loop is started")
        
       
        while not self.event_stop_measurement.is_set():
            Powermeterdata = self.EPM.getData()
            self.data_supplier.publishEvent(Powermeterdata)
            self.logger.logInfo("publish Powermeterdata {}".format(Powermeterdata))
            self.event_stop_measurement.wait(timeout=status_period)
        self.logger.logInfo("data notification loop is terminated")


# Main defined only for testing with data in native format
# Not passing data throuth IDL type translations
if __name__ == "__main__":
    s = BackendPowerMeter()


