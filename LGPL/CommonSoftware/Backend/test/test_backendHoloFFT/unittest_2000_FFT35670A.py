# Project: TNRT - Thai National Radio Telescope
# Organization: NARIT - National Astronomical Research Institute of Thailand

from unittest_common import ColoredTestCase
import BackendDefaults as config
from FFT35670A import FFT35670A
import numpy as np

class ConnectedDeviceTestCase(ColoredTestCase):
    def setUp(self):
        """
        This function runs 1 time per test_xxx function in this class. before test start
        """
        super().setUp()
        self.client = FFT35670A(config.holofft_host, config.holofft_port, True, "DEBUG")
        self.client.connect()


    def tearDown(self):
        """
        This function runs 1 time per test_xxx function in this class. after test complete
        """
        self.client.disconnect()
        super().tearDown()

    def test_2000_get_id(self):
        self.result = self.client.get_id()
        self.assertIsInstance(self.result, str)

    def test_2010_reset_device(self):
        self.result = self.client.reset_device()

    def test_2020_preset_device(self):
        self.result = self.client.preset_device()

    def test_2030_get_error(self):
        self.result = self.client.get_error()
        self.assertIsInstance(self.result, str)

    def test2040_get_operation_complete(self):
        self.result = self.client.get_operation_complete()
        self.assertIsInstance(self.result, int)
    
    def test_2050_wait(self):
        self.result = self.client.wait()

    def test_2052_wait_operation_complete(self):
        self.result = self.client.wait_operation_complete()

    def test_2052_blocking_capture_data(self):
        self.result = self.client.blocking_capture_data()
    
    def test_2053_wait_calibration_complete(self):
        self.result = self.client.wait_calibration_complete()
    
    def test_2054_wait_display_ready(self):
        self.result = self.client.wait_display_ready()
        
    def test_2060_set_command_echo_enabled_false(self):
        self.result = self.client.set_command_echo_enabled(False)

    def test_2061_set_command_echo_enabled_true(self):
        self.result = self.client.set_command_echo_enabled(True)
    
    def test_2070_set_keybaord_enabled_false(self):
        self.result = self.client.set_keybaord_enabled(False)

    def test_2071_set_keybaord_enabled_true(self):
        self.result = self.client.set_keybaord_enabled(True)

    def test_2080_set_instrument_mode_FFT(self):
         self.result = self.client.set_instrument_mode_FFT()
    
    def test_2100_calibrate(self):
        self.result = self.client.calibrate()
    
    def test_2110_set_nchannels_measurement1(self):
        self.result = self.client.set_nchannels_measurement(1)
    
    def test_2112_set_nchannels_measurement2(self):
        self.result = self.client.set_nchannels_measurement(2)

    def test_2120_set_ac_input_coupling(self):
        self.result = self.client.set_ac_input_coupling(1)
        self.result = self.client.set_ac_input_coupling(2)

    def test_2210_get_measurement_duration(self):
        self.result = self.client.get_measurement_duration()
        self.assertIsInstance(self.result, float)

    def test_2200_set_fft_length(self):
        self.result = self.client.set_fft_length(200)

    def test_2210_get_fft_length(self):
        val = 800
        self.client.set_fft_length(val)
        self.result = self.client.get_fft_length()
        self.assertIsInstance(self.result, int)
        self.assertEqual(self.result, val)
        
    def test_2215_set_window_function(self):
        self.client.set_window_function("FLAT")
    
    def test_2220_voltage_range_auto_1t(self):
        self.result = self.client.set_voltage_range_auto(1, True)

    def test_2222_voltage_range_auto_2t(self):
        self.result = self.client.set_voltage_range_auto(2, True)

    def test_2224_voltage_range_auto_1f(self):
        self.result = self.client.set_voltage_range_auto(1, False)

    def test_2226_voltage_range_auto_2f(self):
        self.result = self.client.set_voltage_range_auto(2, False)

    def test_2230_get_span(self):
        self.result = self.client.get_span()
        self.assertIsInstance(self.result, float)
    
    def test_2231_set_span(self):
        nfft = 800
        requested_span = 500
        max_span_2ch = 51.2e3
        
        ratio_max_to_requested = max_span_2ch / requested_span
        decimation = 2 ** np.floor(np.log2(ratio_max_to_requested))
        actual_span = max_span_2ch / decimation

        self.logger.debug("requested span: {}".format(requested_span))
        self.logger.debug("ratio max/req (power of 2): {}".format(ratio_max_to_requested))
        self.logger.debug("decimation: {}".format(decimation))
        self.logger.debug("actual_span: {}".format(actual_span))
   
        self.result = self.client.set_fft_length(nfft)
        self.client.set_span(requested_span)
        
        self.result = self.client.get_span()
        self.assertIsInstance(self.result, float)
        self.assertEqual(self.result, actual_span)
        
    def test_2240_get_center(self):
        self.result = self.client.get_center()
        self.assertIsInstance(self.result, float)

    def tes_2242_set_center(self):
        self.result = self.client.set_center(25600)

    def test_2300_set_active_traces(self):
        self.client.set_active_traces("B")
        self.client.set_active_traces("A")
        self.client.set_active_traces("ABCD")

    def test_23002_set_active_traces(self):
        self.client.set_active_traces("B")
        self.client.set_active_traces("A")
        self.client.set_active_traces("ABCD")

    def test_2304_set_axis_format_mlog(self):
        self.client.set_axis_format(1,"MLOG")

    def test_2306_set_axis_format_mlog(self):
        self.client.set_axis_format(2,"MLOG")

    def test_2308_set_axis_format_mlog(self):
        self.client.set_axis_format(3,"MLOG")

    def test_2310_set_axis_format_mlog(self):
        self.client.set_axis_format(4,"PHAS")
    
    def test_2312_set_decibel_reference(self):
        self.client.set_decibel_reference(1,"DBM")
        self.client.set_decibel_reference(2,"DBM")
        self.client.set_decibel_reference(3,"DBM")
    
    def test_2314_set_yaxis_angle_unit(self):
        self.client.set_axis_format(4,"PHAS")
        self.client.set_yaxis_angle_unit(4,"RAD")
        self.client.set_yaxis_angle_unit(4,"DEGR")
    
    def test_2400_calculate_absolute_power_spectrum(self):
        self.client.calculate_absolute_power_spectrum(1, 1)
        self.client.calculate_absolute_power_spectrum(2, 2)
    
    def test_2402_calculate_relative_power_spectrum(self):
        self.client.calculate_relative_power_spectrum(3)
        self.client.calculate_relative_power_spectrum(4)

    def test_2404_enable_marker_peak_tracking1(self):
        self.client.enable_marker_peak_tracking(1)
    
    def test_2406_enable_marker_peak_tracking2(self):
        self.client.enable_marker_peak_tracking(2)

    def test_2408_set_center_to_marker1(self):
        self.client.set_center_to_marker(1)
    
    def test_2410_set_center_to_marker2(self):
        self.client.set_center_to_marker(2)
        
    def test_2412_get_frequency_for_doppler_correction(self):
        self.client.enable_marker_peak_tracking(1)
        self.result = self.client.get_frequency_for_doppler_correction()
        self.assertIsInstance(self.result, float)
        
    def test_2500_set_display_format_quad(self):
        self.client.set_display_format_quad()
        
    def test_2600_autoscale_axis_once(self):
        self.client.preset_device()
        self.client.set_active_traces("ABCD")
        self.client.autoscale_axis_once(1)
        self.client.autoscale_axis_once(2)
        self.client.autoscale_axis_once(3)
        self.client.autoscale_axis_once(4)

    def test_2800_system_test(self):
        freq_res = 1
        fft_length = 800
    
        self.client.provision()
        self.client.configure(freq_res, fft_length)
        self.client.measurement_start()
        
    