import unittest
from unittest_common import ColoredTestCase
import BackendError

# import ACS modules
from Acspy.Clients.SimpleClient import PySimpleClient

class Tests(ColoredTestCase):
    def setUp(self):
        super().setUp()
        self.result = None
        
        self.sc = None
        self.sc = PySimpleClient()

        self.BackendHoloFFT = None
        self.component_name = "BackendMockHoloFFT"
        self.config_string = '{"freq_res": 100, "fft_length": 100}'

        # Get reference to Component.  If it is already activated by
        # ACS Command Center, this will be a reference to the same object.
        # If it is not active, this command will activate and create a new instance.
        try:
            self.BackendHoloFFT = self.sc.getComponent(self.component_name)
            self.logger.debug("component name {}, ref: {}".format(self.component_name, self.BackendHoloFFT))
        except Exception as e:
            self.logger.error(
                "Cannot get ACS component object reference for %s" % self.component_name
            )
            self.cleanup()

    def tearDown(self):
        try:
            if self.BackendHoloFFT.get_state() != "disconnected":
                self.BackendHoloFFT.client_stop()
            self.sc.releaseComponent(self.component_name)
            self.BackendHoloFFT = None
            if self.sc is not None:
                self.sc.disconnect()
                del self.sc
        except Exception as e:
            self.logger.error("Cannot release component reference for %s" % self.component_name)
            if self.sc is not None:
                self.sc.disconnect()
                del self.sc
                raise(e)
        

    def test_1000_client_start_twice(self):
        self.BackendHoloFFT.client_start()
        self.BackendHoloFFT.client_start()
        self.assertEqual(self.BackendHoloFFT.get_state(), "connected")

    def test_2000_all_state_sequence(self):
        self.BackendHoloFFT.client_start()
        self.assertEqual(self.BackendHoloFFT.get_state(), "connected")

        result = self.BackendHoloFFT.provision("")
        self.assertEqual(result, "provision success")
        self.assertEqual(self.BackendHoloFFT.get_state(), "idle")

        result = self.BackendHoloFFT.configure(self.config_string)
        self.assertEqual(result, "configuration success")
        self.assertEqual(self.BackendHoloFFT.get_state(), "configured")

        result = self.BackendHoloFFT.capture_start()
        self.assertEqual(result, "capture start success")
        self.assertEqual(self.BackendHoloFFT.get_state(), "streaming")
        self.assertEqual(self.BackendHoloFFT.get_measurement_state(), "ready")

        result = self.BackendHoloFFT.measurement_prepare("")
        self.assertEqual(result, "measurement prepare success")
        self.assertEqual(self.BackendHoloFFT.get_measurement_state(), "set")

        result = self.BackendHoloFFT.measurement_start()
        self.assertEqual(result, "measurement start success")
        self.assertEqual(self.BackendHoloFFT.get_measurement_state(), "running")

        result = self.BackendHoloFFT.measurement_stop()
        self.assertEqual(result, "measurement stop success")
        self.assertEqual(self.BackendHoloFFT.get_measurement_state(), "ready")

        result = self.BackendHoloFFT.deconfigure()
        self.assertEqual(result, "reset complete")
        self.assertEqual(self.BackendHoloFFT.get_state(), "idle")

        result = self.BackendHoloFFT.deprovision()
        self.assertEqual(result, "deprovision success")
        self.assertEqual(self.BackendHoloFFT.get_state(), "connected")

        self.BackendHoloFFT.client_stop()
        self.assertEqual(self.BackendHoloFFT.get_state(), "disconnected")

    def test_3000_capture_stop(self):
        self.BackendHoloFFT.client_start()
        self.BackendHoloFFT.provision("")
        self.BackendHoloFFT.configure(self.config_string)
        self.BackendHoloFFT.capture_start()
        result = self.BackendHoloFFT.capture_stop()
        self.assertEqual(result, "capture stop success")
        self.assertEqual(self.BackendHoloFFT.get_state(), "idle")
        self.assertEqual(self.BackendHoloFFT.get_measurement_state(), "idle")

    def test_4000_wrong_mode(self):
        self.BackendHoloFFT.client_start()

        with self.assertRaises(BackendError.WrongModeErrorEx):
            self.BackendHoloFFT.configure(self.config_string)
            self.assertEqual(self.BackendHoloFFT.get_state(), "connected")

        with self.assertRaises(BackendError.WrongModeErrorEx):
            self.BackendHoloFFT.deconfigure()
            self.assertEqual(self.BackendHoloFFT.get_state(), "connected")

        with self.assertRaises(BackendError.WrongModeErrorEx):
            self.BackendHoloFFT.capture_start()
            self.assertEqual(self.BackendHoloFFT.get_state(), "connected")

        with self.assertRaises(BackendError.WrongModeErrorEx):
            self.BackendHoloFFT.capture_stop()
            self.assertEqual(self.BackendHoloFFT.get_state(), "connected")

        with self.assertRaises(BackendError.WrongModeErrorEx):
            self.BackendHoloFFT.measurement_prepare("")
            self.assertEqual(self.BackendHoloFFT.get_state(), "connected")

        with self.assertRaises(BackendError.WrongModeErrorEx):
            self.BackendHoloFFT.measurement_start()
            self.assertEqual(self.BackendHoloFFT.get_state(), "connected")

        with self.assertRaises(BackendError.WrongModeErrorEx):
            self.BackendHoloFFT.measurement_stop()
            self.assertEqual(self.BackendHoloFFT.get_state(), "connected")

    def test_5000_not_connected_to_FFT_analyzer(self):
        with self.assertRaises(BackendError.ConnectionErrorEx):
            self.BackendHoloFFT.client_stop()

        with self.assertRaises(BackendError.ConnectionErrorEx):
            self.BackendHoloFFT.provision("")
            self.assertEqual(self.BackendHoloFFT.get_state(), "disconnected")

        with self.assertRaises(BackendError.ConnectionErrorEx):
            self.BackendHoloFFT.deprovision()
            self.assertEqual(self.BackendHoloFFT.get_state(), "disconnected")

        with self.assertRaises(BackendError.ConnectionErrorEx):
            self.BackendHoloFFT.configure(self.config_string)
            self.assertEqual(self.BackendHoloFFT.get_state(), "disconnected")

        with self.assertRaises(BackendError.ConnectionErrorEx):
            self.BackendHoloFFT.deconfigure()
            self.assertEqual(self.BackendHoloFFT.get_state(), "disconnected")

        with self.assertRaises(BackendError.ConnectionErrorEx):
            self.BackendHoloFFT.capture_start()
            self.assertEqual(self.BackendHoloFFT.get_state(), "disconnected")

        with self.assertRaises(BackendError.ConnectionErrorEx):
            self.BackendHoloFFT.capture_stop()
            self.assertEqual(self.BackendHoloFFT.get_state(), "disconnected")

        with self.assertRaises(BackendError.ConnectionErrorEx):
            self.BackendHoloFFT.measurement_prepare("")
            self.assertEqual(self.BackendHoloFFT.get_state(), "disconnected")

        with self.assertRaises(BackendError.ConnectionErrorEx):
            self.BackendHoloFFT.measurement_start()
            self.assertEqual(self.BackendHoloFFT.get_state(), "disconnected")

        with self.assertRaises(BackendError.ConnectionErrorEx):
            self.BackendHoloFFT.measurement_stop()
            self.assertEqual(self.BackendHoloFFT.get_state(), "disconnected")


if __name__ == "__main__":
    unittest.main()
