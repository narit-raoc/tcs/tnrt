# Project: TNRT - Thai Namtional Radio Telescope
# Company: NARIT - National Astronomical Research Institute of Thailand
# Author: Spiro Sarris
# Date: 2019.09.08

from Acspy.Servants.ACSComponent import ACSComponent
from Acspy.Servants.ContainerServices import ContainerServices
from Acspy.Servants.ComponentLifecycle import ComponentLifecycle
from Acspy.Servants.ComponentLifecycle import ComponentLifecycleException
from Acspy.Clients.SimpleClient import PySimpleClient
from Acspy.Nc.Supplier import Supplier
from Acspy.Nc.Consumer import Consumer
import ACSErrTypeCommonImpl
import DataAggregatorMod
import DataAggregatorErrorImpl
import DataAggregatorError
import PowerMeterMod__POA
import PowerMeterMod
import PowerMeterErrorImpl
import PowerMeterError

import numpy as np
import logging
import time
import astropy.units as units
from astropy.time import Time

class TestPowerMeter:
	#------------------------------------------------------------------------------------
	# Constructor and destructor
	#------------------------------------------------------------------------------------
	def __init__(self):
		'''
		Create references and initialize to None
		'''
		self.logger = logging.getLogger(self.__class__.__name__)
		self.sequence_counter_powermeter = 0
		self.time_tcs_mjd = (1.0/86400.0)*3.0
		self.time_acu_mjd = 0

		# Initialize dictionary of important data for notification channel inputs to 
		# DataAggregator.
		# We can use this data to iterate through inputs
		self.outputs = {
			'powermeter' : {
				'channel': PowerMeterMod.MAINNOTIFICATIONCHANNEL, 
				'dtype': PowerMeterMod.mainPowerMeterNotifyBlock,
				'supplier': None
				}
		}

		# Initialize dictionary data for notification channel output of DataAggregator
		self.inputs = {
			'metadata': {
				'channel': PowerMeterMod.MAINNOTIFICATIONCHANNEL,
				'dtype': PowerMeterMod.mainPowerMeterNotifyBlock,
				'handler': self.handler_powermeter,
				'consumer': None
				}
		}

		# Create Notifiation Channel Suppliers to generate data and push into
		# DataAggregator instance for testing
		for key in self.outputs:
			self.logger.logInfo('Creating Notification Channel Supplier %s: ' % key)
			try:
				self.outputs[key]['supplier'] = Supplier(self.outputs[key]['channel'])
			except:
				self.outputs[key]['supplier'] = None
				self.logger.logError('Failed to initialize NC Supplier: %s' % key)

		# Create Notifiation Channel Consumer to receive the data that is output from
		# DataAggregator instance
		for key in self.inputs:
			self.logger.logInfo('Creating Notification Channel Consumer %s: ' % key)
			try:
				self.inputs[key]['consumer'] = Consumer(self.inputs[key]['channel'])
				self.inputs[key]['consumer'].addSubscription(self.inputs[key]['dtype'], self.inputs[key]['handler'])
				self.inputs[key]['consumer'].consumerReady()
			except:
				self.inputs[key]['consumer'] = None
				self.logger.logError('Failed to initialize NC Consumer %s: ' % key)

		self.logger.info('Starting PySimpleClient')
		self.sc = PySimpleClient()
		
		# Get reference to the DataAggregator component.  If it is already activated by
		# ACS Command Center, this will be a reference to the same DataAggregator.
		# If it is not active, this command will activate and create a new instance.
		try:
			self.PowerMeter = self.sc.getComponent("PowerMeter")  # need to start container fisrt
		except Exception as e:
			self.logger.error('Cannot get component reference for %s' % "PowerMeter")


	def __del__(self):
		'''
		Destrcutore of unit test class.  Disconnect all
		'''
		for key in self.outputs:
			self.logger.logInfo('Destroying Notification Channel Supplier %s: ' % key)
			if self.outputs[key]['supplier'] is not None:
				try:
					self.outputs[key]['supplier'].disconnect()
				except:
					self.outputs[key] = None

		for key in self.inputs:	
			self.logger.logInfo('Destroying Notification Channel Consumer %s: ' % key)
			if self.inputs[key]['consumer'] is not None:
				try:
					self.inputs[key]['consumer'].disconnect()
				except:
					self.inputs[key]['consumer'] = None		

		try:
			self.sc.releaseComponent("PowerMeter")
		except Exception as e:
			self.logger.error('Cannot release component reference for %s' % "PowerMeter")
		
		self.sc.disconnect()
		self.sc = None

	def handler_powermeter(self, dataStruct):
		'''
		Handle the data that was output by the DataAggregator NC Supplier.
		'''
		#self.logger.logDebug('handle event from data aggregator - Metadata')
		self.logger.logDebug(dataStruct)

	def simulate_powermeter(self):
		data = np.random.uniform(-5.0,-2.0)
		self.sequence_counter_powermeter += 1
		powerValue = float(data)
		powerTimestamp = Time.now().mjd
		time.sleep(1.5)   
		self.logger.logInfo("PowerValue : " + str(data) + " TimeStamp : " + str(powerTimestamp))      
		
		eventToPublish = PowerMeterMod.mainPowerMeterNotifyBlock(self.sequence_counter_powermeter,powerValue,powerTimestamp)
		return eventToPublish
		
	def coutON(self):
		count = 0
		for count in range(65535):
			count += 1
		
# end class TestPowerMeter

if __name__ == '__main__':
	# Create an instance of the test environment that will send and receive data from DataAggregator
	mytest = TestPowerMeter()
	
	try:
		
		print(mytest.PowerMeter.connect())
		time.sleep(1)
		
		mytest.PowerMeter.stopRecorder()
		time.sleep(1)

		print(mytest.PowerMeter.identify())
		time.sleep(1)
		
		print(mytest.PowerMeter.preset())
		time.sleep(1)
		
		print(mytest.PowerMeter.checkEPMError())
		time.sleep(1)
		

		mytest.PowerMeter.autoAveragingMode('OFF')
		time.sleep(1)
		print(mytest.PowerMeter.checkAutoAveragingMode())
		time.sleep(1)
		
		mytest.PowerMeter.autoAveragingMode('ON')
		time.sleep(1)
		print(mytest.PowerMeter.checkAutoAveragingMode())
		time.sleep(1)
		
		
		mytest.PowerMeter.startRecorder()
		
		raw_input("wiat for result and p	ress Enter to stop theread...")
		
		mytest.PowerMeter.stopRecorder()
		time.sleep(1)
		
		print("Test pass")
	
	except Exception as e:
		mytest.__del__()	
		print(e)
	
	mytest.__del__()	
	
