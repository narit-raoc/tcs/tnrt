import astropy.units as units
from astropy.time import Time

#How to make MJD timestamp in 1 line.  Result is a 64-bit float (double) 
timestamp1_mjd = Time.now().mjd

print(type(timestamp1_mjd))
print(timestamp1_mjd)


# --- more examples for fun
# Another option that shows more detail about time scale
astropy_time_object = Time(Time.now(), scale='utc')
timestamp2_mjd = astropy_time_object.mjd

print(timestamp2_mjd)

print('Show example of future or past')
timestamp3_mjd = (Time.now() + 24.0*units.hour).mjd

print(timestamp3_mjd)

print('Calculate difference between 2 times')
t4 = astropy_time_object = Time(Time.now(), scale='utc')
t5 = astropy_time_object = Time(Time.now() + 12.0*units.hour, scale='utc')
dt = t5-t4
print(dt)