import unittest
import logging
import coloredlogs
from unittest.mock import patch, Mock

import BackendError

# import ACS modules
from Acspy.Clients.SimpleClient import PySimpleClient

level_styles_scrn = {
    "critical": {"color": "red", "bold": True},
    "debug": {"color": "white", "faint": True},
    "error": {"color": "red"},
    "info": {"color": "green", "bright": True},
    "notice": {"color": "magenta"},
    "spam": {"color": "green", "faint": True},
    "success": {"color": "green", "bold": True},
    "verbose": {"color": "blue"},
    "warning": {"color": "yellow", "bright": True, "bold": True},
}
field_styles_scrn = {
    "asctime": {},
    "hostname": {"color": "magenta"},
    "levelname": {"color": "cyan", "bright": True},
    "name": {"color": "blue", "bright": True},
    "programname": {"color": "cyan"},
}


class TestPowermeter(unittest.TestCase):
    def setUp(self):
        # Configure logger
        self.logger = logging.getLogger("PowerMeter")
        self.logger.setLevel(logging.DEBUG)

        fmt_scrn = "%(asctime)s [%(levelname)s]: %(message)s"
        formatter_screen = coloredlogs.ColoredFormatter(
            fmt=fmt_scrn, level_styles=level_styles_scrn, field_styles=field_styles_scrn
        )

        # creating a handler to log on the console
        handler_screen = logging.StreamHandler()
        handler_screen.setFormatter(formatter_screen)
        handler_screen.setLevel(logging.DEBUG)

        # remove all handlers if they exist
        # remove because PySimpleClient logger already has handlers which
        # show duplicate of my log messages, but without the pretty color format.
        if len(self.logger.handlers) > 0:
            self.logger.handlers = []

        # adding handlers
        self.logger.addHandler(handler_screen)
        self.sc = None
        self.sc = PySimpleClient()

        self.BackendPowerMeter = None
        self.component_name = "BackendPowerMeter"
        self.config_string = '{"integrationTime": 1}'

        # Get reference to ComponentA.  If it is already activated by
        # ACS Command Center, this will be a reference to the same object.
        # If it is not active, this command will activate and create a new instance.
        try:
            self.BackendPowerMeter = self.sc.getComponent(self.component_name)
        except Exception as e:
            self.logger.error(
                "Cannot get ACS component object reference for %s" % self.component_name
            )
            self.cleanup()

    def tearDown(self):
        try:
            if self.BackendPowerMeter.get_state() != "disconnected":
                self.BackendPowerMeter.client_stop()
            self.sc.releaseComponent(self.component_name)
            self.BackendPowerMeter = None
        except Exception as e:
            self.logger.error("Cannot release component reference for %s" % self.component_name)

        if self.sc is not None:
            self.sc.disconnect()
            del self.sc

    def test_client_start_twice(self):
        self.BackendPowerMeter.client_start()
        self.BackendPowerMeter.client_start()
        self.assertEqual(self.BackendPowerMeter.get_state(), "connected")

    def test_full_pipeline(self):
        self.BackendPowerMeter.client_start()
        self.assertEqual(self.BackendPowerMeter.get_state(), "connected")

        result = self.BackendPowerMeter.provision("")
        self.assertEqual(result, "provision success")
        self.assertEqual(self.BackendPowerMeter.get_state(), "idle")

        result = self.BackendPowerMeter.configure(self.config_string)
        self.assertEqual(result, "configuration success")
        self.assertEqual(self.BackendPowerMeter.get_state(), "configured")

        result = self.BackendPowerMeter.capture_start()
        self.assertEqual(result, "capture start success")
        self.assertEqual(self.BackendPowerMeter.get_state(), "streaming")
        self.assertEqual(self.BackendPowerMeter.get_measurement_state(), "ready")

        result = self.BackendPowerMeter.measurement_prepare("")
        self.assertEqual(result, "measurement prepare success")
        self.assertEqual(self.BackendPowerMeter.get_measurement_state(), "set")

        result = self.BackendPowerMeter.measurement_start()
        self.assertEqual(result, "measurement start success")
        self.assertEqual(self.BackendPowerMeter.get_measurement_state(), "running")

        result = self.BackendPowerMeter.measurement_stop()
        self.assertEqual(result, "measurement stop success")
        self.assertEqual(self.BackendPowerMeter.get_measurement_state(), "ready")

        result = self.BackendPowerMeter.deconfigure()
        self.assertEqual(result, "reset complete")
        self.assertEqual(self.BackendPowerMeter.get_state(), "idle")

        result = self.BackendPowerMeter.deprovision()
        self.assertEqual(result, "deprovision success")
        self.assertEqual(self.BackendPowerMeter.get_state(), "connected")

        self.BackendPowerMeter.client_stop()
        self.assertEqual(self.BackendPowerMeter.get_state(), "disconnected")

    def test_capture_stop(self):
        self.BackendPowerMeter.client_start()
        self.BackendPowerMeter.provision("")
        self.BackendPowerMeter.configure(self.config_string)
        self.BackendPowerMeter.capture_start()
        result = self.BackendPowerMeter.capture_stop()
        self.assertEqual(result, "capture stop success")
        self.assertEqual(self.BackendPowerMeter.get_state(), "idle")
        self.assertEqual(self.BackendPowerMeter.get_measurement_state(), "idle")

    def test_wrong_mode(self):
        self.BackendPowerMeter.client_start()

        with self.assertRaises(BackendError.WrongModeErrorEx):
            self.BackendPowerMeter.configure(self.config_string)
            self.assertEqual(self.BackendPowerMeter.get_state(), "connected")

        with self.assertRaises(BackendError.WrongModeErrorEx):
            self.BackendPowerMeter.deconfigure()
            self.assertEqual(self.BackendPowerMeter.get_state(), "connected")

        with self.assertRaises(BackendError.WrongModeErrorEx):
            self.BackendPowerMeter.capture_start()
            self.assertEqual(self.BackendPowerMeter.get_state(), "connected")

        with self.assertRaises(BackendError.WrongModeErrorEx):
            self.BackendPowerMeter.capture_stop()
            self.assertEqual(self.BackendPowerMeter.get_state(), "connected")

        with self.assertRaises(BackendError.WrongModeErrorEx):
            self.BackendPowerMeter.measurement_prepare("")
            self.assertEqual(self.BackendPowerMeter.get_state(), "connected")

        with self.assertRaises(BackendError.WrongModeErrorEx):
            self.BackendPowerMeter.measurement_start()
            self.assertEqual(self.BackendPowerMeter.get_state(), "connected")

        with self.assertRaises(BackendError.WrongModeErrorEx):
            self.BackendPowerMeter.measurement_stop()
            self.assertEqual(self.BackendPowerMeter.get_state(), "connected")

    def test_not_connected_to_Powermeter(self):
        with self.assertRaises(BackendError.ConnectionErrorEx):
            self.BackendPowerMeter.client_stop()

        with self.assertRaises(BackendError.ConnectionErrorEx):
            self.BackendPowerMeter.provision("")
            self.assertEqual(self.BackendPowerMeter.get_state(), "disconnected")

        with self.assertRaises(BackendError.ConnectionErrorEx):
            self.BackendPowerMeter.deprovision()
            self.assertEqual(self.BackendPowerMeter.get_state(), "disconnected")

        with self.assertRaises(BackendError.ConnectionErrorEx):
            self.BackendPowerMeter.configure(self.config_string)
            self.assertEqual(self.BackendPowerMeter.get_state(), "disconnected")

        with self.assertRaises(BackendError.ConnectionErrorEx):
            self.BackendPowerMeter.deconfigure()
            self.assertEqual(self.BackendPowerMeter.get_state(), "disconnected")

        with self.assertRaises(BackendError.ConnectionErrorEx):
            self.BackendPowerMeter.capture_start()
            self.assertEqual(self.BackendPowerMeter.get_state(), "disconnected")

        with self.assertRaises(BackendError.ConnectionErrorEx):
            self.BackendPowerMeter.capture_stop()
            self.assertEqual(self.BackendPowerMeter.get_state(), "disconnected")

        with self.assertRaises(BackendError.ConnectionErrorEx):
            self.BackendPowerMeter.measurement_prepare("")
            self.assertEqual(self.BackendPowerMeter.get_state(), "disconnected")

        with self.assertRaises(BackendError.ConnectionErrorEx):
            self.BackendPowerMeter.measurement_start()
            self.assertEqual(self.BackendPowerMeter.get_state(), "disconnected")

        with self.assertRaises(BackendError.ConnectionErrorEx):
            self.BackendPowerMeter.measurement_stop()
            self.assertEqual(self.BackendPowerMeter.get_state(), "disconnected")


if __name__ == "__main__":
    unittest.main()
