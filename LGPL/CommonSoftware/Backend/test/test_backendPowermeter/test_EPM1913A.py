import unittest
from EPM1913A import EPM1913A
from unittest.mock import patch, Mock, call

class TestEPM1913A(unittest.TestCase):

    def setUp(self):
        with patch("EPM1913A.pyvisa.ResourceManager") as mock_resource_manager:
            instance = mock_resource_manager.return_value
            self.mock_TCPIPSocket = Mock()
            self.mock_TCPIPSocket.clear = Mock()
            self.mock_TCPIPSocket.close = Mock()
            self.mock_TCPIPSocket.write = Mock()
            self.mock_TCPIPSocket.read = Mock()
            self.mock_TCPIPSocket.query = Mock()
            instance.open_resource.return_value = self.mock_TCPIPSocket
            self.instrument = EPM1913A()
            result = self.instrument.connect()
            self.assertEqual(result, self.mock_TCPIPSocket)
            self.assertEqual(self.instrument.inst.timeout, 15000)

    def tearDown(self):
        self.mock_TCPIPSocket.clear.reset_mock()
        self.mock_TCPIPSocket.close.reset_mock()
        self.mock_TCPIPSocket.write.reset_mock()
        self.mock_TCPIPSocket.read.reset_mock()

        self.instrument.disconnect()
        self.mock_TCPIPSocket.close.assert_called()
        self.mock_TCPIPSocket.clear.assert_called()

    def test_IDN(self):
        """[IDN] should return instrument identity"""
        self.mock_TCPIPSocket.query.return_value = "instrument identity"
        result = self.instrument.IDN()
        self.mock_TCPIPSocket.query.assert_called_with("*IDN?")
        self.assertEqual(result, "instrument identity")

    def test_checkError_pass(self):
        """[checkError pass] should send correct command to instrument and return +"""
        mock_r = Mock()
        self.mock_TCPIPSocket.query.return_value = "+0, No error"
        mock_r =  self.mock_TCPIPSocket.query.return_value[0]

        result = self.instrument.checkError()
        
        self.mock_TCPIPSocket.query.assert_called_with("SYST:ERR?")
        self.assertEqual(result[0], True)
    
    def test_checkError_fail(self):
        """[checkError fail] should send correct command to instrument and return -"""
        mock_r = Mock()
        self.mock_TCPIPSocket.query.return_value = "-231,Data questionable;CAL ERROR ChA"
        mock_r =  self.mock_TCPIPSocket.query.return_value[0]
    
        result = self.instrument.checkError()
        
        self.mock_TCPIPSocket.query.assert_called_with("SYST:ERR?")
        self.assertEqual(result[0], False)

    def test_OPC(self):
        """[OPC] should send correct command to instrument"""
        self.mock_TCPIPSocket.query.return_value = "+1"
        result = self.instrument.OPC()
        self.mock_TCPIPSocket.query.assert_called_with("*OPC?")
        self.assertEqual(result, "+1")

    def test_calibration_pass(self):
        """[calibration_pass] should send correct command to instrument"""
        mock_checkError = Mock()
        self.instrument.checkError = mock_checkError
        mock_checkError.return_value = True, "+0, No error"
        
        
        result = self.instrument.calibration()
        
        mock_checkError.assert_called_with()
        self.mock_TCPIPSocket.write.assert_called_with("CAL1:AUTO ONCE")
        self.assertEqual(result, True)
        
    def test_calibration_fail(self):
        """[calibration_fail] should send correct command to instrument"""
        mock_checkError = Mock()
        self.instrument.checkError = mock_checkError
        mock_checkError.return_value = False, "-231,Data questionable;CAL ERROR ChA"
        
        
        result = self.instrument.calibration()
        
        mock_checkError.assert_called_with()
        self.mock_TCPIPSocket.write.assert_called_with("CAL1:AUTO ONCE")
        self.assertEqual(result, False)
        
    def test_reset(self):
        """[reset] should send correct command to instrument wait 20 second"""
        mock_checkError = Mock()
        self.instrument.checkError = mock_checkError
        mock_checkError.return_value = True, "0, No error"
        
        result = self.instrument.reset()
        mock_checkError.assert_called_with()
        self.mock_TCPIPSocket.write.assert_has_calls(
            [
                call("*RST"),
                call("SYST:PRES"),
            ]
        )
        self.assertEqual(result, True)
        
    def test_preset(self):
        """[preset] should send correct command to instrument"""
        mock_checkError = Mock()
        self.instrument.checkError = mock_checkError
        mock_checkError.return_value = True, "0, No error"
        
        result = self.instrument.preset()
        mock_checkError.assert_called_with()
        self.mock_TCPIPSocket.write.assert_called_with("SYST:PRES")
        self.assertEqual(result, True)
    
    def test_clear(self):
        """[preset] should send correct command to instrument"""
        mock_checkError = Mock()
        self.instrument.checkError = mock_checkError
        mock_checkError.return_value = True, "0, No error"
        
        result = self.instrument.clear()
        mock_checkError.assert_called_with()
        self.mock_TCPIPSocket.write.assert_called_with("*CLS")
        self.assertEqual(result, True)
    
    def test_checkAutoAveragingMode(self):
        """[checkAutoAveragingMode] should send correct command to instrument"""
        self.mock_TCPIPSocket.query.return_value = 1
        result = self.instrument.checkAutoAveragingMode()
        self.mock_TCPIPSocket.query.assert_called_with("AVER:COUN:AUTO?")
        self.assertEqual(result, 1)
        
    def test_ONautoAveragingMode(self):
        """[ONautoAveragingMode] should send correct command to instrument"""
        mock_checkError = Mock()
        self.instrument.checkError = mock_checkError
        mock_checkError.return_value = True, "0, No error"
        
        result = self.instrument.ONautoAveragingMode()
        mock_checkError.assert_called_with()
        self.mock_TCPIPSocket.write.assert_called_with("AVERage:COUNt:AUTO ON")
        self.assertEqual(result, True)

    def test_OFFautoAveragingMode(self):
        """[OFFautoAveragingMode] should send correct command to instrument"""
        mock_checkError = Mock()
        self.instrument.checkError = mock_checkError
        mock_checkError.return_value = True, "0, No error"
        
        result = self.instrument.OFFautoAveragingMode()
        mock_checkError.assert_called_with()
        self.mock_TCPIPSocket.write.assert_called_with("AVERage:COUNt:AUTO OFF")
        self.assertEqual(result, True)
    
    def test_getTimeReference(self):
        """[getTimeReference] should send correct command to instrument"""
        with patch("astropy.time.Time.now") as mock_time:
            instance = mock_time.return_value
            instance.mjd = "mocked_time"
            result = self.instrument.getTimeReference()
            self.assertEqual(result, "mocked_time")

    def test_deviceSetup(self):
        """[deviceSetup] should send correct command to instrument"""
        mock_checkError = Mock()
        mock_clear = Mock()
        mock_preset = Mock()
        self.instrument.checkError = mock_checkError
        self.instrument.clear = mock_clear
        self.instrument.preset = mock_preset

        mock_checkError.return_value = True, "0, No error"
        mock_clear.return_value = True
        mock_preset.return_value = True

        result = self.instrument.deviceSetup()
        mock_checkError.assert_called_with()
        mock_clear.assert_called_with()
        mock_preset.assert_called_with()
        self.mock_TCPIPSocket.write.assert_called_with("INIT:CONT ON")
        self.assertEqual(result, True)

    def test_getData(self):
        """[getData] should send correct command to instrument and return PowerMeter data"""
        with patch("BackendMod.Powermeterdata") as mock_powermeterdata:
            self.instrument.getTimeReference = Mock(return_value="mocked_timeRef")
            mock_measure = Mock()
            self.instrument.measureFETCMode = mock_measure
            mock_measure.return_value = float(-48.5179188)

            pd = self.instrument.getData()
            self.assertEqual(pd.timeRef, "mocked_timeRef")
            self.assertEqual(pd.testPowdBm, -48.5179188)

    def test_measureFETCMode(self):
        """[measureFETCMode] should send correct command to instrument and return PowerMeter data"""
        self.mock_TCPIPSocket.query.return_value = -5.06591875E+001
        result = self.instrument.measureFETCMode()
        self.mock_TCPIPSocket.query.assert_called_with("FETC?")
        self.assertEqual(result, -5.06591875E+001)


if __name__ == "__main__":
    unittest.main()
