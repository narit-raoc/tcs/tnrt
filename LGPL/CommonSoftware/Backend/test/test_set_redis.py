import redis
conn = redis.Redis(host='192.168.90.11', port=6379, db=4)

project = {"value":"test_pulsar"}
conn.hmset("project", project)
print(conn.hgetall("project"))

source_name = {"value":"B0332+5434"}
conn.hmset("source-name", source_name)
print(conn.hgetall("source-name"))

ra = {"value":"03:32:59.3469570182353"}
conn.hmset("ra", ra)
print(conn.hgetall("ra"))

dec = {"value":"55:34:43.2453881363263"}
conn.hmset("dec", dec)
print(conn.hgetall("dec"))

del conn