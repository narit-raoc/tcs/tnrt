import pprint
import json

class sCoord:
	def __init__(self, p1, p2):
		self.x = p1
		self.y = p2

class Metadata_Scan:
	def __init__(self, p1, p2, p3):
		self.time_start = p1
		self.duration = p2
		self.scan_name = p3
		self.coords = sCoord(-99, -88)


class Metadata_Antenna:
	def __init__(self, p1, p2, p3):
		self.param1 = p1
		self.param2list = p2
		self.param3list = p3


class Metadata:
	def __init__(self):
		self.key_scan = Metadata_Scan(21, 22, 'test name')
		self.key_antenna = Metadata_Antenna(11, [5, 6, 7, 8], [77, 88, 99])


def object_to_json(object):
	return json.dumps(object, default=lambda o: getattr(o, '__dict__', str(o)))

def json_to_dict(json_string):
	return json.loads(json_string)

if __name__ == "__main__":
	obj = Metadata()
	print('Original OBJECT:')
	pprint.pprint(obj)
	print("")

	json_str = object_to_json(obj)
	print('JSON string')
	pprint.pprint(json_str)
	print("")

	dic = json_to_dict(json_str)
	print('DICTIONARY')
	pprint.pprint(dic)