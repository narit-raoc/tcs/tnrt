# Project: TCS - Thai National Radio Telescope
# Author: Spiro Sarris
# Date: 2023.05

#import standard modules
import unittest
import logging
import coloredlogs
import socket
import ctypes
import time
from astropy.time import Time

# import ACS modules
from Acspy.Clients.SimpleClient import PySimpleClient

# import from NARIT TCS
import DataFormatAcu

class Tests(unittest.TestCase):
    def setUp(self):
        '''
        This function runs 1 time per test_xxx function in this class. before test start
        '''
        # Configure logger
        self.logger = logging.getLogger('TemplateTester')
        self.logger.setLevel(logging.DEBUG)

        fmt_scrn = '%(asctime)s [%(levelname)s]: %(message)s'

        level_styles_scrn = {'critical': {'color': 'red', 'bold': True}, 'debug': {'color': 'white', 'faint': True}, 'error': {'color': 'red'}, 'info': {'color': 'green', 'bright': True}, 'notice': {'color': 'magenta'}, 'spam': {'color': 'green', 'faint': True}, 'success': {'color': 'green', 'bold': True}, 'verbose': {'color': 'blue'}, 'warning': {'color': 'yellow', 'bright': True, 'bold': True}}
        field_styles_scrn = {'asctime': {}, 'hostname': {'color': 'magenta'}, 'levelname': {'color': 'cyan', 'bright': True}, 'name': {'color': 'blue', 'bright': True}, 'programname': {'color': 'cyan'}}
        formatter_screen = coloredlogs.ColoredFormatter(fmt=fmt_scrn, level_styles=level_styles_scrn, field_styles=field_styles_scrn)

        # creating a handler to log on the console
        handler_screen = logging.StreamHandler()
        handler_screen.setFormatter(formatter_screen)
        handler_screen.setLevel(logging.DEBUG)

        # remove all handlers if they exist
        # remove because PySimpleClient logger already has handlers which
        # show duplicate of my log messages, but without the pretty color format.
        if (len(self.logger.handlers) > 0):
            self.logger.handlers = []

        # adding handlers
        self.logger.addHandler(handler_screen)
        self.logger.debug('Started logger name %s' % self.logger.name)

        self.logger.debug('Starting PySimpleClient')
        self.sc = None
        self.sc = PySimpleClient()
        
        self.objref = None
        self.component_name = 'MockAcu'
        # Get reference to Component.  If it is already activated by
        # ACS Command Center, this will be a reference to the same object.
        # If it is not active, this command will activate and create a new instance.
        try:
            self.logger.debug('Connecting to ACS component %s' % self.component_name)
            self.objref = self.sc.getComponent(self.component_name)
        except Exception as e:
            self.logger.error('Cannot get ACS component object reference for %s' % self.component_name)
            self.cleanup()

        # member variables to keep data for tests
        self.current_struct = None
        self.current_valuetype = None

    def tearDown(self):
        '''
        This function runs 1 time per test_xxx function in this class. after test complete
        '''
        try:
            self.logger.debug('Releasing ACS component %s' % self.component_name)
            self.sc.releaseComponent(self.component_name)
            self.objref = None
        except Exception as e:
            self.logger.error('Cannot release component reference for %s' % self.component_name)

        if(self.sc is not None):
            self.sc.disconnect()
            del self.sc

    def test100_status(self):
        HOST = '127.0.0.1'  # The server's hostname or IP address
        PORT = 9001        # The port used by the server

        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            self.logger.debug('start connect ...')
            s.connect((HOST, PORT))
            s.settimeout(10)
            
            countdown = 10
            while countdown > 0:
                raw_bytes = s.recv(ctypes.sizeof(DataFormatAcu.StatusMessageFrame), socket.MSG_WAITALL)
                status_message = DataFormatAcu.StatusMessageFrame.from_buffer_copy(raw_bytes)
                self.logger.debug('count: {} time_mjd {}'.format(status_message.status_message_counter, status_message.general_sts.actual_time))
                countdown = countdown -1
            s.close()

    def test200_command(self):
        HOST = '127.0.0.1'  # The server's hostname or IP address
        PORT = 9000        # The port used by the server
        packet = DataFormatAcu.CommandPacketStandard()

        packet.start_flag = DataFormatAcu.STARTFLAG

        packet.phdr = DataFormatAcu.PacketHeader()
        packet.phdr.message_length = ctypes.sizeof(packet)
        packet.phdr.source = DataFormatAcu.SRC_REMOTE
        packet.phdr.command_serial_number =990
        packet.phdr.number_of_commands = 1

        packet.chdr = DataFormatAcu.CommandHeader()
        packet.chdr.command_type = DataFormatAcu.CMD_PARAM 
        packet.chdr.subsystem_id = DataFormatAcu.SUBSYSTEM_SYSTEM
        packet.chdr.command_id = DataFormatAcu.CMD_SET_MASTER

        packet.end_flag = DataFormatAcu.ENDFLAG

        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            self.logger.debug('initiate connect ...')
            s.connect((HOST, PORT))
            s.settimeout(10.0)
            tsleep = 5
            self.logger.debug('sleep {} seconds before start command ...'.format(tsleep))
            time.sleep(tsleep)
            self.logger.debug('connected')

            packet.phdr.command_serial_number += 1
            packet.params = DataFormatAcu.ParameterStandard(DataFormatAcu.CMD_SET_MASTER_REMOTE, 0.0)
            self.logger.debug("packet")
            self.logger.debug(repr(packet))
            self.logger.debug('send packet')
            s.sendall(packet)
            self.logger.debug('wait for receive packet ...')
            raw_ack = s.recv(ctypes.sizeof(DataFormatAcu.Acknowledge), socket.MSG_WAITALL)
            self.logger.debug('received')
            ack = DataFormatAcu.Acknowledge.from_buffer_copy(raw_ack)
            self.logger.debug('ACK:')
            self.logger.debug(repr(ack))
            self.logger.debug('')
            self.logger.debug('sleep 2 seconds before next command')
            time.sleep(2.0)
            packet.phdr.command_serial_number += 1
            packet.params = DataFormatAcu.ParameterStandard(DataFormatAcu.CMD_SET_MASTER_LCP, 0.0)
            self.logger.debug("packet")
            self.logger.debug(repr(packet))
            self.logger.debug('send packet')
            s.sendall(packet)
            self.logger.debug('wait for receive packet ...')
            raw_ack = s.recv(ctypes.sizeof(DataFormatAcu.Acknowledge), socket.MSG_WAITALL)
            self.logger.debug('received')
            ack = DataFormatAcu.Acknowledge.from_buffer_copy(raw_ack)
            self.logger.debug('ACK:')
            self.logger.debug(repr(ack))
            self.logger.debug('')
            self.logger.debug('sleep 2 seconds before next command')
            time.sleep(2.0)

            s.close()

    
    
if __name__ == '__main__':
    unittest.main()
