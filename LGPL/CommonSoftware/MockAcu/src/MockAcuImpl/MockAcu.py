# Project: TNRT - Thai National Radio Telescope
# Company: NARIT - National Astronomical Research Institute of Thailand
# Author: Spiro Sarris
# Date: 2020.10.25

# Standard python packages
import logging
import socket
import threading
import socketserver
import ctypes
import time
from astropy.time import Time

# ACS packages
from Acspy.Servants.ACSComponent import ACSComponent
from Acspy.Servants.ContainerServices import ContainerServices
from Acspy.Servants.ComponentLifecycle import ComponentLifecycle
from Acspy.Servants.ComponentLifecycle import ComponentLifecycleException
import ACSErr

# TNRT packages
import DataFormatAcu as acu
import MockAcuMod
import MockAcuMod__POA
import MockAcuErrorImpl
import MockAcuError

# config
import AntennaDefaults as config


class CommandHandler(socketserver.BaseRequestHandler):
    """
    It is instantiated once per connection to the server, and must
    override the handle() method to implement communication to the
    client.
    """

    def handle(self):
        self.logger = logging.getLogger("AntennaC")

        # Set some items in the mock ack message to be fixed for all messages.
        ack_message = acu.Acknowledge()
        ack_message.start_flag = acu.STARTFLAG
        ack_message.message_length = ctypes.sizeof(acu.Acknowledge)
        ack_message.source = 2
        ack_message.command_identifier = 150
        ack_message.status = acu.CMD_ACK_STS_DONE
        ack_message.end_flag = acu.ENDFLAG

        while True:
            try:
                # Read the command from remote control interface.
                # biggest packets is around 1.2k, so try t read 2048

                nbytes = (
                    ctypes.sizeof(acu.STARTFLAG)
                    + ctypes.sizeof(acu.PacketHeader)
                    + ctypes.sizeof(acu.CommandHeader)
                )

                self.logger.logDebug("Waiting for command ...")
                raw_bytes = self.request.recv(nbytes, socket.MSG_WAITALL)

                packet_header = acu.PacketHeader.from_buffer_copy(
                    raw_bytes, ctypes.sizeof(acu.STARTFLAG)
                )
                self.logger.logDebug(repr(packet_header))

                command_header = acu.CommandHeader.from_buffer_copy(
                    raw_bytes,
                    ctypes.sizeof(acu.STARTFLAG) + ctypes.sizeof(acu.PacketHeader),
                )
                self.logger.logDebug(repr(command_header))

                # # Update these 3 items for each meassage to that ACU component parses the ACK
                # # and doesn't raise any exceptions.
                ack_message.command_serial_number = packet_header.command_serial_number
                ack_message.subsystem_id = command_header.subsystem_id
                ack_message.command = command_header.command_id

                param_struct = None
                self.logger.logDebug(
                    "read remaining bytes for command type %d"
                    % command_header.command_id
                )
                if (command_header.subsystem_id == acu.SUBSYSTEM_HXP.value) and (
                    command_header.command_type == acu.CMD_MODE.value
                ):
                    nbytes = ctypes.sizeof(acu.ParameterHexapod) + ctypes.sizeof(
                        acu.ENDFLAG
                    )
                    self.logger.logDebug(
                        "found ParameterHexapod, read nbytes=%d" % (nbytes)
                    )
                    raw_bytes = self.request.recv(nbytes, socket.MSG_WAITALL)
                    param_struct = acu.ParameterHexapod.from_buffer_copy(raw_bytes)
                elif command_header.command_id in acu.list_ParameterStandard:
                    nbytes = ctypes.sizeof(acu.ParameterStandard) + ctypes.sizeof(
                        acu.ENDFLAG
                    )
                    self.logger.logDebug(
                        "found ParameterStandard, read nbytes=%d" % (nbytes)
                    )
                    raw_bytes = self.request.recv(nbytes, socket.MSG_WAITALL)
                    param_struct = acu.ParameterStandard.from_buffer_copy(raw_bytes)
                elif command_header.command_id in acu.list_ParameterTrackTable:
                    nbytes = ctypes.sizeof(acu.ParameterTrackTable) + ctypes.sizeof(
                        acu.ENDFLAG
                    )
                    self.logger.logDebug(
                        "found ParameterTrackTable, read nbytes=%d" % (nbytes)
                    )
                    raw_bytes = self.request.recv(nbytes, socket.MSG_WAITALL)
                    param_struct = acu.ParameterTrackTable.from_buffer_copy(raw_bytes)
                elif command_header.command_id in acu.list_ParameterTrackTableHexapod:
                    nbytes = ctypes.sizeof(
                        acu.ParameterTrackTableHexapod
                    ) + ctypes.sizeof(acu.ENDFLAG)
                    self.logger.logDebug(
                        "found ParameterTrackTableHexapod, read nbytes=%d" % (nbytes)
                    )
                    raw_bytes = self.request.recv(nbytes, socket.MSG_WAITALL)
                    param_struct = acu.ParameterTrackTableHexapod.from_buffer_copy(
                        raw_bytes
                    )
                elif (
                    command_header.command_id in acu.list_ParameterElevationOffsetTable
                ):
                    nbytes = ctypes.sizeof(
                        acu.ParameterElevationOffsetTable
                    ) + ctypes.sizeof(acu.ENDFLAG)
                    self.logger.logDebug(
                        "found ParameterElevationOffsetTable, read nbytes=%d" % (nbytes)
                    )
                    raw_bytes = self.request.recv(nbytes, socket.MSG_WAITALL)
                    param_struct = acu.ParameterElevationOffsetTable.from_buffer_copy(
                        raw_bytes
                    )
                elif command_header.command_id in acu.list_ParameterPmodel:
                    nbytes = ctypes.sizeof(acu.ParameterPmodel) + ctypes.sizeof(
                        acu.ENDFLAG
                    )
                    self.logger.logDebug(
                        "found ParameterPmodel, read nbytes=%d" % (nbytes)
                    )
                    raw_bytes = self.request.recv(nbytes, socket.MSG_WAITALL)
                    param_struct = acu.ParameterPmodel.from_buffer_copy(raw_bytes)
                elif command_header.command_id in acu.list_ParameterRefraction:
                    nbytes = ctypes.sizeof(acu.ParameterRefraction) + ctypes.sizeof(
                        acu.ENDFLAG
                    )
                    self.logger.logDebug(
                        "found ParameterRefraction, read nbytes=%d" % (nbytes)
                    )
                    raw_bytes = self.request.recv(nbytes, socket.MSG_WAITALL)
                    param_struct = acu.ParameterRefraction.from_buffer_copy(raw_bytes)
                elif command_header.command_id in acu.list_ParameterNORAD:
                    nbytes = ctypes.sizeof(acu.ParameterNORAD) + ctypes.sizeof(
                        acu.ENDFLAG
                    )
                    self.logger.logDebug(
                        "found ParameterNORAD, read nbytes=%d" % (nbytes)
                    )
                    raw_bytes = self.request.recv(nbytes, socket.MSG_WAITALL)
                    param_struct = acu.ParameterNORAD.from_buffer_copy(raw_bytes)
                elif command_header.command_id in acu.list_ParameterSetTrackMode:
                    nbytes = ctypes.sizeof(acu.ParameterSetTrackMode) + ctypes.sizeof(
                        acu.ENDFLAG
                    )
                    self.logger.logDebug(
                        "found ParameterSetTrackMode, read nbytes=%d" % (nbytes)
                    )
                    raw_bytes = self.request.recv(nbytes, socket.MSG_WAITALL)
                    param_struct = acu.ParameterSetTrackMode.from_buffer_copy(raw_bytes)
                else:
                    self.logger.logDebug(
                        "param_struct type not found in lists.  ignore"
                    )
                    param_struct = None

                if param_struct is not None:
                    self.logger.logDebug(repr(param_struct))

                    try:
                        self.request.sendall(ack_message)
                    except ConnectionResetError:
                        break
                    except BrokenPipeError:
                        break

            except ValueError:
                break


class StatusHandler(socketserver.BaseRequestHandler):
    """
    It is instantiated once per connection to the server, and must
    override the handle() method to implement communication to the
    client.
    """

    def handle(self):
        self.logger = logging.getLogger("AntennaC")

        # Generate a fake status message and send it in a loop forever.
        status_message = acu.StatusMessageFrame()
        status_message.start_flag = acu.STARTFLAG
        status_message.end_flag = acu.ENDFLAG
        status_message.message_length = ctypes.sizeof(acu.StatusMessageFrame)
        status_message.status_message_type = 0

        # Set some bits to True for all axes.  In the real system, all of these bits
        # cannot be True at the same time.
        # But this is convenient for fast Mock testing.
        for i in range(len(status_message.axis_status)):  # ACU has 8 axes
            status_message.axis_status[i].bit_sts = (
                status_message.axis_status[i].bit_sts | acu.ABS_ACTIVE
            )
            status_message.axis_status[i].bit_sts = (
                status_message.axis_status[i].bit_sts | acu.ABS_INACTIVE
            )
            status_message.axis_status[i].stow_status = (
                status_message.axis_status[i].stow_status | acu.STOW_POSOK
            )

        status_message.hexapod_sts.bit_sts = (
            status_message.hexapod_sts.bit_sts | acu.ABS_ACTIVE
        )
        status_message.hexapod_sts.bit_sts = (
            status_message.hexapod_sts.bit_sts | acu.ABS_INACTIVE
        )

        status_message.tracking_sts.prog_offset_bs = (
            status_message.tracking_sts.prog_offset_bs | acu.PTS_COMPLETED
        )

        # Set GRS actual mode to STOPPED to flow control believes that it arrived to
        # the command position already
        status_message.axis_status[7].actual_mode = acu.MODE_STOP.value

        self.logger.logDebug("Client connected. Start serving status messages ...")
        vertexShutterStatus = 0
        while True:
            status_message.status_message_counter += 1
            status_message.general_sts.actual_time = Time.now().mjd
            status_message.vertexshutter_sts.status = (
                vertexShutterStatus % 4
            ) + 1  # vertexShutter has 4 status (1-4)
            vertexShutterStatus += 1
            # self.logger.logDebug('status_message.general_sts.actual_time {}'.format(status_message.general_sts.actual_time))
            try:
                self.request.sendall(status_message)
                time.sleep(0.2)
            except ConnectionResetError:
                break
            except BrokenPipeError:
                break


class MockAcu(
    MockAcuMod__POA.MockAcu, ACSComponent, ContainerServices, ComponentLifecycle
):
    """
    docstring
    """

    # Constructor
    def __init__(self):
        ACSComponent.__init__(self)
        ContainerServices.__init__(self)
        self.logger = self.getLogger()

    # Destructor
    def __del__(self):
        pass

    # ------------------------------------------------------------------------------------
    # ASC Component LifeCycle functions called by ACS system when we activate and
    # deactivate components.  Inherited from headers in Acspy.Servants.ComponentLifecycle.

    def initialize(self):
        self.logger.logInfo("INITIALIZE")
        # Get IP address and Port to create the socket servers

        self.host = config.MockAcuIP
        self.port_cmd = int(config.cmdPort)
        self.port_sts = int(config.stsPort)

        self.logger.logDebug(
            "Read from CDB config: IP address = %s, command port = %d, status port = %d"
            % (self.host, self.port_cmd, self.port_sts)
        )

        self.logger.logInfo(
            "Starting socketserver IP address = %s, command port = %d"
            % (self.host, self.port_cmd)
        )
        self.server_cmd = socketserver.TCPServer(
            (self.host, self.port_cmd), CommandHandler
        )
        self.thread_cmd = threading.Thread(target=self.server_cmd.serve_forever)
        self.thread_cmd.start()

        self.logger.logInfo(
            "Starting socketserver IP address = %s, status port = %d"
            % (self.host, self.port_sts)
        )
        self.server_sts = socketserver.TCPServer(
            (self.host, self.port_sts), StatusHandler
        )
        self.thread_sts = threading.Thread(target=self.server_sts.serve_forever)
        self.thread_sts.start()

    def execute(self):
        pass

    def aboutToAbort(self):
        self.logger.logInfo("ABOUT_TO_ABORT")

    def cleanUp(self):
        self.logger.logInfo("CLEANUP")

        self.logger.logInfo(
            "socket servers shutdown(). wait serve_forever() to complete"
        )
        self.server_cmd.shutdown()
        self.server_sts.shutdown()

        self.logger.logInfo("socket servers server_close()")
        self.server_cmd.server_close()
        self.server_sts.server_close()

    # ------------------------------------------------------------------------------------
    # Functions defined in IDL to interface from outside this class
    # NONE


if __name__ == "__main__":
    m = MockAcu()
