import AtmosphereWrapper as amw

# p = pressure
# t = temperature
# h = humidity

a = amw.Atmosphere()
# Set default values of b1, b2
a.setB1(5.9)
a.setB2(2.5)

pressure = 915.7
temperature = -2.2
humidity = 70

# Set some values in the Atmosphere object.  will be used to calculate new r0,
# and refraction angle.
a.setPressure(pressure)
a.setTemperature(temperature)
a.setHumidity(humidity)

print('\nget updated values of p,t,h')
p = a.getPressure()
t = a.getTemperature()
h = a.getHumidity()

print('Pressure: %6.2f, Temperature: %6.2f, Humidity %4.2f' % (p, t, h))
print('r0: %5.2f\t\tb1: %5.2f\t\tb2: %5.2f' % (a.r0(), a.getB1(), a.getB2()))
print('__________________________________________________')
print('')
el = 0
print('EL angle (deg): %4.1f\tElevationCorrection: %f' %
						(el, a.elevationCorrection(el)))
el = 30
print('EL angle (deg): %4.1f\tElevationCorrection: %f' %
						(el, a.elevationCorrection(el)))
el = 60
print('EL angle (deg): %4.1f\tElevationCorrection: %f' %
						(el, a.elevationCorrection(el)))
el = 90
print('EL angle (deg): %4.1f\tElevationCorrection: %f' %
						(el, a.elevationCorrection(el)))

pressure = 913.3
temperature = 21.7
humidity = 30

a.setPressure(pressure)
a.setTemperature(temperature)
a.setHumidity(humidity)

print('\nget updated values of p,t,h')
p = a.getPressure()
t = a.getTemperature()
h = a.getHumidity()

print('Pressure: %6.2f, Temperature: %6.2f, Humidity %4.2f' % (p, t, h))
print('r0: %5.2f\t\tb1: %5.2f\t\tb2: %5.2f' % (a.r0(), a.getB1(), a.getB2()))
print('__________________________________________________')
print('')
el = 0
print('EL angle (deg): %4.1f\tElevationCorrection: %f' %
						(el, a.elevationCorrection(el)))
el = 30
print('EL angle (deg): %4.1f\tElevationCorrection: %f' %
						(el, a.elevationCorrection(el)))
el = 60
print('EL angle (deg): %4.1f\tElevationCorrection: %f' %
						(el, a.elevationCorrection(el)))
el = 90
print('EL angle (deg): %4.1f\tElevationCorrection: %f' %
						(el, a.elevationCorrection(el)))

pressure = 913.3
temperature = 21.7
humidity = 70

a.setPressure(pressure)
a.setTemperature(temperature)
a.setHumidity(humidity)

print('\nget updated values of p,t,h')
p = a.getPressure()
t = a.getTemperature()
h = a.getHumidity()

print('Pressure: %6.2f, Temperature: %6.2f, Humidity %4.2f' % (p, t, h))
print('r0: %5.2f\t\tb1: %5.2f\t\tb2: %5.2f' % (a.r0(), a.getB1(), a.getB2()))
print('__________________________________________________')
print('')
el = 0
print('EL angle (deg): %4.1f\tElevationCorrection: %f' %
						(el, a.elevationCorrection(el)))
el = 45
print('EL angle (deg): %4.1f\tElevationCorrection: %f' %
						(el, a.elevationCorrection(el)))
el = 60
print('EL angle (deg): %4.1f\tElevationCorrection: %f' %
						(el, a.elevationCorrection(el)))
el = 90
print('EL angle (deg): %4.1f\tElevationCorrection: %f' %
						(el, a.elevationCorrection(el)))
