# TODO: install QT4Agg or QT5Agg graphic backend, then set matplotlib to use
# Default backend graph is not beatiful.
# matplotlib.use("Qt4Agg")
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import AtmosphereWrapper as amw

# range of values
# pressure: 	500	<= 	p <=	2000
# tempearute: 	-50	<= 	t <=	80
# humidity: 	0 	<= 	h <=	100
# frequency: 	500 <= 	f <=	200000
# elevation: 	0 	<=	f <=	90

a = amw.Atmosphere()
# Set values of b1, b2
a.setB1(5.9)
a.setB2(2.5)
# ======================================================
# Set pressure, temperature, humidity.
# Calculate elevation correctoin vs. Elevation angle
# ======================================================
p = 600
t = 30
h = 50
a.setPressure(p)
a.setTemperature(t)
a.setHumidity(h)

elevation_min_deg = 1
elevation_max_deg = 90
elevation_step_deg = 1

# numpy array of elevation in degrees
npts = int((elevation_max_deg - elevation_min_deg)/elevation_step_deg + 1)
ee = np.linspace(elevation_min_deg, elevation_max_deg, npts)

# Initialize to impossible number to create array
elCorrection = -999 * np.ones(ee.size)

# Open a file to write CSV and save results
with open('./csv/elcorrection_el.csv', 'w') as file_result:
	file_result.write('EL_degrees, ELcorrection_arcsec\n')
	for k in range(ee.size):
		elCorrection[k] = a.elevationCorrection(ee[k])
		print('p=%5.2f [mbar], t=%4.2f [C], h=%4.1f [%%], r0=%5.2f, EL=%4.2f [deg], ELcorr=%f [arcsec]' % (p,t,h,a.r0(),ee[k],elCorrection[k]))
		file_result.write('%f, %f\n' % (ee[k], elCorrection[k]))

# Prepare plot figure window.  figsize is pixels/100.
fig = plt.figure(1, figsize=(10,6))

# Select subplot 2.  Plot Opacity vs. Elevation for fixed freq
ax2 = plt.subplot(1, 1, 1)
ax2.plot(ee, elCorrection)
ax2.set_title('EL correction [arcsec] vs EL angle [deg] @ temp %4.2f [C], pressure %4.2f [mbar], humidity %3.1f [%%]' %(t, p, h))
ax2.set_xlabel('Elevation [deg]')
ax2.set_ylabel('Elevation Correction [arcsec]')
ax2.grid(True, which="both")


# ======================================================
# Set pressure, temperature, Elevation angle.
# Calculate elevation correctoin vs. humidity
# ======================================================
p = 600
t = 30
# h = 50
e = 45
a.setPressure(p)
a.setTemperature(t)

h_min = 0
h_max = 100
h_step = 1

# numpy array of 
npts = int((h_max - h_min)/h_step + 1)
hh = np.linspace(h_min, h_max, npts)

# Initialize to impossible number to create array
elCorrection = -999 * np.ones(hh.size)

for k in range(hh.size):
	a.setHumidity(hh[k])
	elCorrection[k] = a.elevationCorrection(e)
	print('p=%5.2f [mbar], t=%4.2f [C], h=%4.1f [%%], r0=%5.2f, EL=%4.2f [deg], ELcorr=%f [arcsec]' % (p,t,hh[k],a.r0(),e,elCorrection[k]))	

# Prepare plot figure window.  figsize is pixels/100.
fig = plt.figure(2, figsize=(10, 6))

# Select subplot 2.  Plot Opacity vs. Elevation for fixed freq
ax2 = plt.subplot(1, 1, 1)
ax2.plot(hh, elCorrection)
ax2.set_title('EL correction [arcsec] vs Humidity [%%] @ temp %4.2f [C], pressure %4.2f [mbar], elevation %3.1f [deg]' %(t, p, e))
ax2.set_xlabel('Humidity [%]')
ax2.set_ylabel('Elevation Correction [arcsec]')
ax2.grid(True, which="both")

# ======================================================
# Set pressure, Elevation angle, humidity.
# Calculate elevation correctoin vs. temperature
# ======================================================
p = 600
#t = 30
h = 50
e = 45
a.setPressure(p)
a.setHumidity(h)

t_min = -50
t_max = 80
t_step = 1

# numpy array of 
npts = int((t_max - t_min)/t_step + 1)
tt = np.linspace(t_min, t_max, npts)

# Initialize to impossible number to create array
elCorrection = -999 * np.ones(tt.size)

for k in range(tt.size):
	a.setTemperature(tt[k])
	elCorrection[k] = a.elevationCorrection(e)
	print('p=%5.2f [mbar], t=%4.2f [C], h=%4.1f [%%], r0=%5.2f, EL=%4.2f [deg], ELcorr=%f [arcsec]' % (p, tt[k], h, a.r0(), e, elCorrection[k]))

# Prepare plot figure window.  figsize is pixels/100.
fig = plt.figure(3, figsize=(10,6))

# Select subplot 2.  Plot Opacity vs. Elevation for fixed freq
ax2 = plt.subplot(1,1,1)
ax2.plot(tt,elCorrection)
ax2.set_title('EL correction [arcsec] vs Temperature [C] @ pressure %4.2f [mbar], humidity %3.1f [%%], elevation %3.1f [deg]' %(p,h,e))
ax2.set_xlabel('Temperature [C]')
ax2.set_ylabel('Elevation Correction [arcsec]')
ax2.grid(True, which="both")

# ======================================================
# Set Elevation angle, temperature, humidity.
# Calculate elevation correctoin vs. pressure
# ======================================================
#p = 600
t = 30
h = 50
e = 45
#a.setPressure(p)
a.setTemperature(t)
a.setHumidity(h)

p_min = 500
p_max = 2000
p_step = 10

# numpy array of elevation in degrees
npts = int((p_max - p_min)/p_step + 1)
pp = np.linspace(p_min, p_max, npts)

# Initialize to impossible number to create array
elCorrection = -999 * np.ones(pp.size)

for k in range(pp.size):
	a.setPressure(pp[k])
	elCorrection[k] = a.elevationCorrection(e)
	print('p=%5.2f [mbar], t=%4.2f [C], h=%4.1f [%%], r0=%5.2f, EL=%4.2f [deg], ELcorr=%f [arcsec]' % (pp[k], t, h, a.r0(), e, elCorrection[k]))

# Prepare plot figure window.  figsize is pixels/100.
fig = plt.figure(4, figsize=(10,6))

# Select subplot 2.  Plot Opacity vs. Elevation for fixed freq
ax2 = plt.subplot(1, 1, 1)
ax2.plot(pp, elCorrection)
ax2.set_title('EL correction [arcsec] vs Pressure [mbar] @ temp %4.2f [C], humidity %3.1f [%%], elevation %3.1f [deg]' % (t, h, e))
ax2.set_xlabel('Pressure [mbar]')
ax2.set_ylabel('Elevation Correction [arcsec]')
ax2.grid(True, which="both")

# Open the GUI window and show all
plt.show()
