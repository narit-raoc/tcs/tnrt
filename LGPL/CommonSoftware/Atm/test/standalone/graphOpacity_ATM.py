# matplotlib.use("Qt4Agg")
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import AtmosphereWrapper as amw

# range of values
# pressure: 	500	<= 	p <=	2000
# tempearute: 	-50	<= 	t <=	80
# humidity: 	0 	<= 	h <=	100
# frequency: 	500 <= 	f <=	200000
# elevation: 	0 	<=	f <=	90
# water vapor:	0	<	w

a = amw.Atmosphere()
# Set default values of b1, b2
a.setB1(5.9)
a.setB2(2.5)

pressure = 915.7
temperature = 25
humidity = 70

# Set valus to instance variables in Atmosphere object
a.setPressure(pressure)
a.setTemperature(temperature)
a.setHumidity(humidity)
# w, precipitable water accounts for temperature, pressure, humidity set in Atm object.
w = a.wh2o()

elevation_min_deg = 1
elevation_max_deg = 90
elevation_step_deg = 1

freq_min_Hz = 1e9
freq_max_Hz = 30e9
freq_step_Hz = 1e9

# select elevation to use for opacity vs. frequency
select_elevation = 45

# select freqeucny to use for opacity vs. elevation
select_freq_MHz = 2000


# numpy array of elevation in degrees
npts = int((elevation_max_deg - elevation_min_deg)/elevation_step_deg + 1)
ee_deg = np.linspace(elevation_min_deg, elevation_max_deg, npts)

# numpy array of frequency in MHz
npts = int((freq_max_Hz - freq_min_Hz)/freq_step_Hz + 1)
ff_Hz = np.linspace(freq_min_Hz, freq_max_Hz, npts)
ff_MHz = ff_Hz / 1e6

# Initialize opacity to values of -1 so that we know it is not real
opacity_ff = -1 * np.ones(ff_MHz.size)
opacity_ee = -1 * np.ones(ee_deg.size)

# Open a file to write CSV and save results
with open('./csv/opac_ff_ATM.csv', 'w') as file_op_ff, open('./csv/opac_el_ATM.csv', 'w') as file_op_ee:
	file_op_ff.write('freq_MHz, opacity_neper\n')
	file_op_ee.write('EL_deg, opacity_neper\n')
	
	for k in range(ff_MHz.size):
		to = a.opacity_ATM(ff_MHz[k], w, select_elevation)
		opacity_ff[k] = to['signalSideBandOpacity']
		print('processing freqency %d of %d: freq=%f MHz, opacity=%f' %
								(k+1, ff_MHz.size, ff_MHz[k], opacity_ff[k]))
		file_op_ff.write('%f, %f\n' % (ff_MHz[k], opacity_ff[k]))	

	for k in range(ee_deg.size):
		to = a.opacity_ATM(select_freq_MHz, w, ee_deg[k])
		opacity_ee[k] = to['signalSideBandOpacity']
		print('processing elevation %d of %d: EL=%f deg, opacity=%f' %
								(k+1, ee_deg.size, ee_deg[k], opacity_ee[k]))
		file_op_ee.write('%f, %f\n' % (ee_deg[k], opacity_ee[k]))

# Prepare plot figure window.  figsize is pixels/100.
fig = plt.figure(figsize=(10, 6))

# Create subplot grid. 2 rows, 1 column, Select subplot 1. Opacity vs.
# Frequency for fixed elevation
ax1 = plt.subplot(2, 1, 1)
ax1.plot(ff_MHz, opacity_ff)
ax1.set_title('Opacity (ATM model) @ EL %4.1f [deg], temp %4.2f [degC], pressure %4.2f [mbar], humidity %3.1f [%%]' % (select_elevation, temperature, pressure, humidity))
ax1.set_xlabel('Frequency [MHz]')
ax1.set_ylabel('Opacity [neper]')
ax1.grid(True, which="both")

# Select subplot 2.  Plot Opacity vs. Elevation for fixed freq
ax2 = plt.subplot(2, 1, 2)
ax2.plot(ee_deg, opacity_ee)
ax2.set_title('Opacity (ATM model) @ freq %9.2f [MHz], temp %4.2f [degC], pressure %4.2f [mbar], humidity %3.1f [%%]' % (select_freq_MHz, temperature, pressure, humidity))
ax2.set_xlabel('Elevation [deg]')
ax2.set_ylabel('Opacity [neper]')
ax2.grid(True, which="both")

# Open the GUI window and show all
plt.show()
