import AtmosphereWrapper as amw

a = amw.Atmosphere()
# Set default values of b1, b2
a.setB1(5.9)
a.setB2(2.5)

pressure = 915.7
temperature = -2.2
humidity = 70

# Set some values in the Atmosphere object.  will be used to calculate new r0,
# and refraction angle.
a.setPressure(pressure)
a.setTemperature(temperature)
a.setHumidity(humidity)

print('\nget updated values of p, t, h, w')
p = a.getPressure()
t = a.getTemperature()
h = a.getHumidity()

# Choose elevation angle
elevation = 90

# Calculate water vapor.  uses instance member var of Atmosphere for p,t,h
w = a.wh2o()

print('r0: %5.2f\t\tb1: %5.2f\t\tb2: %5.2f' % (a.r0(), a.getB1(), a.getB2()))
print('Pressure: %6.2f, \tTemperature: %6.2f, \tHumidity %4.2f, \tWater %4.2f'
						% (p, t, h, w))
print('__________________________________________________')

freq = 22235

# Run the model and read the results of opacity from output
toe = a.opacity_ATM(freq, w, elevation)
opacity_ATM = toe['signalSideBandOpacity']

print('Freq: %d\tElevation: %5.2d\t Opacity: %f' %
						(freq, elevation, opacity_ATM))
