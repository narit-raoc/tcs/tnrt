# Project: NARIT TCS - Thai National Radio Telescope
# Author: Spiro Sarris
# Date: 2023 APR

#import standard modules
import unittest
import logging

# pip modules
import coloredlogs
import numpy as np
import matplotlib.pyplot as plt

# import ACS modules
from Acspy.Clients.SimpleClient import PySimpleClient

# import TCS modules
import atmosphere

class Tests(unittest.TestCase):
    def setUp(self):
        '''
        This function runs 1 time per test_xxx function in this class. before test start
        '''
        # Configure logger
        self.logger = logging.getLogger('TemplateTester')
        self.logger.setLevel(logging.DEBUG)

        fmt_scrn = '%(asctime)s [%(levelname)s]: %(message)s'

        level_styles_scrn = {'critical': {'color': 'red', 'bold': True}, 'debug': {'color': 'white', 'faint': True}, 'error': {'color': 'red'}, 'info': {'color': 'green', 'bright': True}, 'notice': {'color': 'magenta'}, 'spam': {'color': 'green', 'faint': True}, 'success': {'color': 'green', 'bold': True}, 'verbose': {'color': 'blue'}, 'warning': {'color': 'yellow', 'bright': True, 'bold': True}}
        field_styles_scrn = {'asctime': {}, 'hostname': {'color': 'magenta'}, 'levelname': {'color': 'cyan', 'bright': True}, 'name': {'color': 'blue', 'bright': True}, 'programname': {'color': 'cyan'}}
        formatter_screen = coloredlogs.ColoredFormatter(fmt=fmt_scrn, level_styles=level_styles_scrn, field_styles=field_styles_scrn)

        # creating a handler to log on the console
        handler_screen = logging.StreamHandler()
        handler_screen.setFormatter(formatter_screen)
        handler_screen.setLevel(logging.DEBUG)

        # remove all handlers if they exist
        # remove because PySimpleClient logger already has handlers which
        # show duplicate of my log messages, but without the pretty color format.
        if (len(self.logger.handlers) > 0):
            self.logger.handlers = []

        # adding handlers
        self.logger.addHandler(handler_screen)
        self.logger.debug('Started logger name %s' % self.logger.name)

        self.logger.debug('Starting PySimpleClient')
        self.sc = None
        self.sc = PySimpleClient()
        
        self.objref = None
        self.component_name = 'ATM'
        # Get reference to Component.  If it is already activated by
        # ACS Command Center, this will be a reference to the same object.
        # If it is not active, this command will activate and create a new instance.
        try:
            self.logger.debug('Connecting to ACS component %s' % self.component_name)
            self.objref = self.sc.getComponent(self.component_name)
        except Exception as e:
            self.logger.error('Cannot get ACS component object reference for %s' % self.component_name)
            self.cleanup()
        
        self.result = None

    def tearDown(self):
        '''
        This function runs 1 time per test_xxx function in this class. after test complete
        '''
        self.logger.info("result: {}".format(self.result))
        try:
            self.logger.debug('Releasing ACS component %s' % self.component_name)
            self.sc.releaseComponent(self.component_name)
            self.objref = None
        except Exception as e:
            self.logger.error('Cannot release component reference for %s' % self.component_name)

        if(self.sc is not None):
            # Remove logging handlers from logger in PySimpleClient to reduce clutter
            # We are going to disconnect and delete the client, so don't care about last log messages
            self.sc.logger.handlers = []
            self.sc.disconnect()
            del self.sc

    def test100_datatypes(self):
        # Confirm that we can get all of the data types from the the remote object
        self.logger.debug('test ACS Component type(objref): {}'.format(type(self.objref)))
        self.assertEqual(type(self.objref), atmosphere._objref_Atm)

    def test101_getTemperature(self):
        self.result = self.objref.getTemperature()
        self.assertIsInstance(self.result, float)
    
    def test102_getPressure(self):
        self.result = self.objref.getPressure()
        self.assertIsInstance(self.result, float)

    def test103_getHumidity(self):
        self.result = self.objref.getHumidity()
        self.assertIsInstance(self.result, float)

    def test104_r0(self):
        self.result = self.objref.r0()
        self.assertIsInstance(self.result, float)

    def test105_b1(self):
        self.result = self.objref.b1()
        self.assertIsInstance(self.result, float)

    def test106_b2(self):
        self.result = self.objref.b2()
        self.assertIsInstance(self.result, float)
    
    def test200_elevationCorrection(self):
        self.logger.debug("elevationCorrection(45)")
        self.result = self.objref.elevationCorrection(45)
        self.assertIsInstance(self.result, float)

    def test201_Ph2o(self):
        self.logger.debug("Ph2o(35, 50)")
        self.result = self.objref.Ph2o(35, 50)
        self.assertIsInstance(self.result, float)

    def test202_dewTemperature(self):
        self.result = self.objref.dewTemperature()
        self.assertIsInstance(self.result, float)

    def test203_wh2o(self):
        self.result = self.objref.wh2o()
        self.assertIsInstance(self.result, float)
        
    def test300_atmOpacity(self):
        p = 900
        t = 30
        h = 50
        f = 20000
        e = 45

        self.logger.debug("atmOpacity({},{},{},{},{})".format(p,t,h,f,e))
        self.result = self.objref.atmOpacity(p,t,h,f,e)
        self.assertIsInstance(self.result, float)
    
    def test300_atmOpacity_ATM(self):
        f = 20000
        w = 5
        e = 45

        self.logger.debug("atmOpacity_ATM({},{},{})".format(f,w,e))
        self.result = self.objref.atmOpacity_ATM(f,w,e)
        self.assertIsInstance(self.result, atmosphere.totalAtmOpacityEmissivity)
    
    def test400_printELcorrection(self):
        self.logger.info('Starting with default values of p,t,h')
        p = self.objref.getPressure();
        t = self.objref.getTemperature();
        h = self.objref.getHumidity();

        self.logger.info('Pressure: %6.2f, Temperature: %6.2f, Humidity %4.2f' % (p,t,h))
        self.logger.info('r0: %5.2f, b1: %5.2f, b2: %5.2f' % (self.objref.r0(), self.objref.b1(), self.objref.b2()))

        pressure = 915.7
        temperature = -2.2
        humidity = 70

        # Set some values in the Atm object.  will be used to calculate new r0, and refraction angle.
        self.objref.setPressure(pressure)
        self.objref.setTemperature(temperature)
        self.objref.setHumidity(humidity)

        self.logger.info('get updated values of p,t,h')
        p = self.objref.getPressure();
        t = self.objref.getTemperature();
        h = self.objref.getHumidity();

        self.logger.info('Pressure: %6.2f, Temperature: %6.2f, Humidity %4.2f' % (p,t,h))
        self.logger.info('r0: %5.2f\t\tb1: %5.2f\t\tb2: %5.2f' % (self.objref.r0(), self.objref.b1(), self.objref.b2()))
        self.logger.info('__________________________________________________')
        self.logger.info('')
        el = 0
        self.logger.info('EL angle (deg): %4.1f\tElevationCorrection: %f'%(el, self.objref.elevationCorrection(el)))
        el = 30
        self.logger.info('EL angle (deg): %4.1f\tElevationCorrection: %f'%(el, self.objref.elevationCorrection(el)))
        el = 60
        self.logger.info('EL angle (deg): %4.1f\tElevationCorrection: %f'%(el, self.objref.elevationCorrection(el)))
        el = 90
        self.logger.info('EL angle (deg): %4.1f\tElevationCorrection: %f'%(el, self.objref.elevationCorrection(el)))

        pressure = 913.3
        temperature = 21.7
        humidity = 30

        self.objref.setPressure(pressure)
        self.objref.setTemperature(temperature)
        self.objref.setHumidity(humidity)

        self.logger.info('get updated values of p,t,h')
        p = self.objref.getPressure();
        t = self.objref.getTemperature();
        h = self.objref.getHumidity();

        self.logger.info('Pressure: %6.2f, Temperature: %6.2f, Humidity %4.2f' % (p,t,h))
        self.logger.info('r0: %5.2f\t\tb1: %5.2f\t\tb2: %5.2f' % (self.objref.r0(), self.objref.b1(), self.objref.b2()))
        self.logger.info('__________________________________________________')
        self.logger.info('')
        el = 0
        self.logger.info('EL angle (deg): %4.1f\tElevationCorrection: %f'%(el, self.objref.elevationCorrection(el)))
        el = 30
        self.logger.info('EL angle (deg): %4.1f\tElevationCorrection: %f'%(el, self.objref.elevationCorrection(el)))
        el = 60
        self.logger.info('EL angle (deg): %4.1f\tElevationCorrection: %f'%(el, self.objref.elevationCorrection(el)))
        el = 90
        self.logger.info('EL angle (deg): %4.1f\tElevationCorrection: %f'%(el, self.objref.elevationCorrection(el)))

        pressure = 913.3
        temperature = 21.7
        humidity = 70

        self.objref.setPressure(pressure)
        self.objref.setTemperature(temperature)
        self.objref.setHumidity(humidity)

        self.logger.info('get updated values of p,t,h')
        p = self.objref.getPressure();
        t = self.objref.getTemperature();
        h = self.objref.getHumidity();

        self.logger.info('Pressure: %6.2f\tTemperature: %6.2f\tHumidity %4.2f' % (p,t,h))
        self.logger.info('r0: %5.2f\t\tb1: %5.2f\t\tb2: %5.2f' % (self.objref.r0(), self.objref.b1(), self.objref.b2()))
        self.logger.info('__________________________________________________')
        self.logger.info('')
        el = 0
        self.logger.info('EL angle (deg): %4.1f\tElevationCorrection: %f'%(el, self.objref.elevationCorrection(el)))
        el = 30
        self.logger.info('EL angle (deg): %4.1f\tElevationCorrection: %f'%(el, self.objref.elevationCorrection(el)))
        el = 60
        self.logger.info('EL angle (deg): %4.1f\tElevationCorrection: %f'%(el, self.objref.elevationCorrection(el)))
        el = 90
        self.logger.info('EL angle (deg): %4.1f\tElevationCorrection: %f'%(el, self.objref.elevationCorrection(el)))

    def test402_printOpacity_AM(self):
        self.logger.info('Starting with default values of p,t,h')
        p = self.objref.getPressure();
        t = self.objref.getTemperature();
        h = self.objref.getHumidity();

        self.logger.info('Pressure: %6.2f, Temperature: %6.2f, Humidity %4.2f' % (p,t,h))
        self.logger.info('r0: %5.2f, b1: %5.2f, b2: %5.2f' % (self.objref.r0(), self.objref.b1(), self.objref.b2()))

        elevation = 90

        pressure = 915.7
        temperature = -2.2
        humidity = 70

        # Set some values in the Atm object.  will be used to calculate new r0, and refraction angle.
        self.objref.setPressure(pressure)
        self.objref.setTemperature(temperature)
        self.objref.setHumidity(humidity)

        self.logger.info('get updated values of p,t,h')
        p = self.objref.getPressure();
        t = self.objref.getTemperature();
        h = self.objref.getHumidity();

        self.logger.info('Pressure: %6.2f, Temperature: %6.2f, Humidity %4.2f' % (p,t,h))
        self.logger.info('r0: %5.2f\t\tb1: %5.2f\t\tb2: %5.2f' % (self.objref.r0(), self.objref.b1(), self.objref.b2()))
        self.logger.info('__________________________________________________')

        freq = 22235
        self.logger.info('Freq: %d\tOpacity: %f\tWater vapor pressure: %f ' % (freq, self.objref.atmOpacity(p, t, h, freq, elevation), self.objref.Ph2o(t, h)))
        freq = 23400
        self.logger.info('Freq: %d\tOpacity: %f\tWater vapor pressure: %f ' % (freq, self.objref.atmOpacity(p, t, h, freq, elevation), self.objref.Ph2o(t, h)))
        freq = 8400
        self.logger.info('Freq: %d\tOpacity: %f\tWater vapor pressure: %f ' % (freq, self.objref.atmOpacity(p, t, h, freq, elevation), self.objref.Ph2o(t, h)))
        freq = 6600
        self.logger.info('Freq: %d\tOpacity: %f\tWater vapor pressure: %f ' % (freq, self.objref.atmOpacity(p, t, h, freq, elevation), self.objref.Ph2o(t, h)))
        freq = 4900
        self.logger.info('Freq: %d\tOpacity: %f\tWater vapor pressure: %f ' % (freq, self.objref.atmOpacity(p, t, h, freq, elevation), self.objref.Ph2o(t, h)))

        pressure = 913.3
        temperature = 21.7
        humidity = 30

        self.objref.setPressure(pressure)
        self.objref.setTemperature(temperature)
        self.objref.setHumidity(humidity)

        self.logger.info('get updated values of p,t,h')
        p = self.objref.getPressure();
        t = self.objref.getTemperature();
        h = self.objref.getHumidity();

        self.logger.info('Pressure: %6.2f, Temperature: %6.2f, Humidity %4.2f' % (p,t,h))
        self.logger.info('r0: %5.2f\t\tb1: %5.2f\t\tb2: %5.2f' % (self.objref.r0(), self.objref.b1(), self.objref.b2()))
        self.logger.info('__________________________________________________')
        freq = 22235
        self.logger.info('Freq: %d\tOpacity: %f\tWater vapor pressure: %f ' % (freq, self.objref.atmOpacity(p, t, h, freq, elevation), self.objref.Ph2o(t, h)))
        freq = 23400
        self.logger.info('Freq: %d\tOpacity: %f\tWater vapor pressure: %f ' % (freq, self.objref.atmOpacity(p, t, h, freq, elevation), self.objref.Ph2o(t, h)))
        freq = 8400
        self.logger.info('Freq: %d\tOpacity: %f\tWater vapor pressure: %f ' % (freq, self.objref.atmOpacity(p, t, h, freq, elevation), self.objref.Ph2o(t, h)))
        freq = 6600
        self.logger.info('Freq: %d\tOpacity: %f\tWater vapor pressure: %f ' % (freq, self.objref.atmOpacity(p, t, h, freq, elevation), self.objref.Ph2o(t, h)))
        freq = 4900
        self.logger.info('Freq: %d\tOpacity: %f\tWater vapor pressure: %f ' % (freq, self.objref.atmOpacity(p, t, h, freq, elevation), self.objref.Ph2o(t, h)))

        pressure = 913.3
        temperature = 21.7
        humidity = 70

        self.objref.setPressure(pressure)
        self.objref.setTemperature(temperature)
        self.objref.setHumidity(humidity)

        self.logger.info('get updated values of p,t,h')
        p = self.objref.getPressure();
        t = self.objref.getTemperature();
        h = self.objref.getHumidity();

        self.logger.info('Pressure: %6.2f\tTemperature: %6.2f\tHumidity %4.2f' % (p,t,h))
        self.logger.info('r0: %5.2f\t\tb1: %5.2f\t\tb2: %5.2f' % (self.objref.r0(), self.objref.b1(), self.objref.b2()))
        self.logger.info('__________________________________________________')
        freq = 22235
        self.logger.info('Freq: %d\tOpacity: %f\tWater vapor pressure: %f ' % (freq, self.objref.atmOpacity(p, t, h, freq, elevation), self.objref.Ph2o(t, h)))
        freq = 23400
        self.logger.info('Freq: %d\tOpacity: %f\tWater vapor pressure: %f ' % (freq, self.objref.atmOpacity(p, t, h, freq, elevation), self.objref.Ph2o(t, h)))
        freq = 8400
        self.logger.info('Freq: %d\tOpacity: %f\tWater vapor pressure: %f ' % (freq, self.objref.atmOpacity(p, t, h, freq, elevation), self.objref.Ph2o(t, h)))
        freq = 6600
        self.logger.info('Freq: %d\tOpacity: %f\tWater vapor pressure: %f ' % (freq, self.objref.atmOpacity(p, t, h, freq, elevation), self.objref.Ph2o(t, h)))
        freq = 4900
        self.logger.info('Freq: %d\tOpacity: %f\tWater vapor pressure: %f ' % (freq, self.objref.atmOpacity(p, t, h, freq, elevation), self.objref.Ph2o(t, h)))

    def test403_printOpacity_ATM(self):
        self.logger.info('Starting with default values of p,t,h')
        p = self.objref.getPressure();
        t = self.objref.getTemperature();
        h = self.objref.getHumidity();

        self.logger.info('Pressure: %6.2f, Temperature: %6.2f, Humidity %4.2f' % (p,t,h))
        self.logger.info('r0: %5.2f, b1: %5.2f, b2: %5.2f' % (self.objref.r0(), self.objref.b1(), self.objref.b2()))

        elevation = 90

        pressure = 915.7
        temperature = -2.2
        humidity = 70

        # Set some values in the Atm object.  will be used to calculate new r0, and refraction angle.
        self.objref.setPressure(pressure)
        self.objref.setTemperature(temperature)
        self.objref.setHumidity(humidity)

        self.logger.info('get updated values of p,t,h, water')
        p = self.objref.getPressure()
        t = self.objref.getTemperature()
        h = self.objref.getHumidity()
        w = self.objref.wh2o()

        self.logger.info('Pressure: %6.2f, Temperature: %6.2f, Humidity %4.2f, Water %4.2f' % (p,t,h,w))
        self.logger.info('__________________________________________________')

        freq = 22235

        to = self.objref.atmOpacity_ATM(freq,w,elevation)
        self.logger.info('Freq: %d\tOpacity: %f' % (freq, to.signalSideBandOpacity))
        self.logger.info(to)
    
    def test500_graphELcorrection(self):
        self.logger.info('r0: %5.2f, b1: %5.2f, b2: %5.2f' % (self.objref.r0(), self.objref.b1(), self.objref.b2()))

        # range of values
        # pressure: 	500	<= 	p <=	2000
        # tempearute: 	-50	<= 	t <=	80
        # humidity: 	0 	<= 	h <=	100
        # frequency: 	500 <= 	f <=	200000
        # elevation: 	0 	<=	f <=	90

        # ======================================================
        # Set pressure, temperature, humidity.  
        # Calculate elevation correctoin vs. Elevation angle
        # ======================================================
        p = 600
        t = 30
        h = 50
        self.objref.setPressure(p)
        self.objref.setTemperature(t)
        self.objref.setHumidity(h)

        elevation_min_deg = 1
        elevation_max_deg = 90
        elevation_step_deg = 1

        # numpy array of elevation in degrees
        npts = int((elevation_max_deg - elevation_min_deg)/elevation_step_deg + 1)
        ee = np.linspace(elevation_min_deg, elevation_max_deg, npts)

        # Initialize to impossible number to create array
        elCorrection = -999*np.ones(ee.size)

        for k in range(ee.size):
            elCorrection[k] = self.objref.elevationCorrection(ee[k])
            self.logger.info('p=%5.2f [mbar], t=%4.2f [C], h=%4.1f [%%], r0=%5.2f, EL=%4.2f [deg], ELcorr=%f [arcsec]' % (p,t,h,self.objref.r0(),ee[k],elCorrection[k]))	

        # Prepare plot figure window.  figsize is pixels/100.
        fig = plt.figure(1, figsize=(10,6))

        # Select subplot 2.  Plot Opacity vs. Elevation for fixed freq
        ax2 = plt.subplot(1,1,1)
        ax2.plot(ee,elCorrection)
        ax2.set_title('EL correction [arcsec] vs EL angle [deg] @ temp %4.2f [C], pressure %4.2f [mbar], humidity %3.1f [%%]' %(t, p, h))
        ax2.set_xlabel('Elevation [deg]')
        ax2.set_ylabel('Elevation Correction [arcsec]')
        ax2.grid(True, which="both")


        # ======================================================
        # Set pressure, temperature, Elevation angle.  
        # Calculate elevation correctoin vs. humidity
        # ======================================================
        p = 600
        t = 30
        #h = 50
        e = 45
        self.objref.setPressure(p)
        self.objref.setTemperature(t)

        h_min = 0
        h_max = 100
        h_step = 1

        # numpy array of elevation in degrees
        npts = int((h_max - h_min)/h_step + 1)
        hh = np.linspace(h_min, h_max, npts)

        # Initialize to impossible number to create array
        elCorrection = -999*np.ones(hh.size)

        for k in range(hh.size):
            self.objref.setHumidity(hh[k]);
            elCorrection[k] = self.objref.elevationCorrection(e)
            self.logger.info('p=%5.2f [mbar], t=%4.2f [C], h=%4.1f [%%], r0=%5.2f, EL=%4.2f [deg], ELcorr=%f [arcsec]' % (p,t,hh[k],self.objref.r0(),e,elCorrection[k]))	

        # Prepare plot figure window.  figsize is pixels/100.
        fig = plt.figure(2, figsize=(10,6))

        # Select subplot 2.  Plot Opacity vs. Elevation for fixed freq
        ax2 = plt.subplot(1,1,1)
        ax2.plot(hh,elCorrection)
        ax2.set_title('EL correction [arcsec] vs Humidity [%%] @ temp %4.2f [C], pressure %4.2f [mbar], elevation %3.1f [deg]' %(t, p, e))
        ax2.set_xlabel('Humidity [%]')
        ax2.set_ylabel('Elevation Correction [arcsec]')
        ax2.grid(True, which="both")

        # ======================================================
        # Set pressure, Elevation angle, humidity.  
        # Calculate elevation correctoin vs. temperature
        # ======================================================
        p = 600
        #t = 30
        h = 50
        e = 45
        self.objref.setPressure(p)
        self.objref.setHumidity(h)

        t_min = -50
        t_max = 80
        t_step = 1

        # numpy array of elevation in degrees
        npts = int((t_max - t_min)/t_step + 1)
        tt = np.linspace(t_min, t_max, npts)

        # Initialize to impossible number to create array
        elCorrection = -999*np.ones(tt.size)

        for k in range(tt.size):
            self.objref.setTemperature(tt[k]);
            elCorrection[k] = self.objref.elevationCorrection(e)
            self.logger.info('p=%5.2f [mbar], t=%4.2f [C], h=%4.1f [%%], r0=%5.2f, EL=%4.2f [deg], ELcorr=%f [arcsec]' % (p,tt[k],h,self.objref.r0(),e,elCorrection[k]))	

        # Prepare plot figure window.  figsize is pixels/100.
        fig = plt.figure(3, figsize=(10,6))

        # Select subplot 2.  Plot Opacity vs. Elevation for fixed freq
        ax2 = plt.subplot(1,1,1)
        ax2.plot(tt,elCorrection)
        ax2.set_title('EL correction [arcsec] vs Temperature [C] @ pressure %4.2f [mbar], humidity %3.1f [%%], elevation %3.1f [deg]' %(p,h,e))
        ax2.set_xlabel('Temperature [C]')
        ax2.set_ylabel('Elevation Correction [arcsec]')
        ax2.grid(True, which="both")

        # ======================================================
        # Set Elevation angle, temperature, humidity.  
        # Calculate elevation correctoin vs. pressure
        # ======================================================
        #p = 600
        t = 30
        h = 50
        e = 45
        #self.objref.setPressure(p)
        self.objref.setTemperature(t)
        self.objref.setHumidity(h)

        p_min = 500
        p_max = 2000
        p_step = 10

        # numpy array of elevation in degrees
        npts = int((p_max - p_min)/p_step + 1)
        pp = np.linspace(p_min, p_max, npts)

        # Initialize to impossible number to create array
        elCorrection = -999*np.ones(pp.size)

        for k in range(pp.size):
            self.objref.setPressure(pp[k]);
            elCorrection[k] = self.objref.elevationCorrection(e)
            self.logger.info('p=%5.2f [mbar], t=%4.2f [C], h=%4.1f [%%], r0=%5.2f, EL=%4.2f [deg], ELcorr=%f [arcsec]' % (pp[k],t,h,self.objref.r0(),e,elCorrection[k]))	

        # Prepare plot figure window.  figsize is pixels/100.
        fig = plt.figure(4, figsize=(10,6))

        # Select subplot 2.  Plot Opacity vs. Elevation for fixed freq
        ax2 = plt.subplot(1,1,1)
        ax2.plot(pp,elCorrection)
        ax2.set_title('EL correction [arcsec] vs Pressure [mbar] @ temp %4.2f [C], humidity %3.1f [%%], elevation %3.1f [deg]' %(t,h,e))
        ax2.set_xlabel('Pressure [mbar]')
        ax2.set_ylabel('Elevation Correction [arcsec]')
        ax2.grid(True, which="both")

        # Open the GUI window and show all
        plt.show()


    def test501_graphOpacity_AM(self):
        self.logger.info('r0: %5.2f, b1: %5.2f, b2: %5.2f' % (self.objref.r0(), self.objref.b1(), self.objref.b2()))

        # range of values
        # pressure: 	500	<= 	p <=	2000
        # tempearute: 	-50	<= 	t <=	80
        # humidity: 	0 	<= 	h <=	100
        # frequency: 	500 <= 	f <=	200000
        # elevation: 	0 	<=	f <=	90

        pressure = 915.7
        temperature = 25
        humidity = 70

        elevation_min_deg = 1
        elevation_max_deg = 90
        elevation_step_deg = 1

        freq_min_Hz = 1e9
        freq_max_Hz = 30e9
        freq_step_Hz = 1e9

        # select elevation to use for opacity vs. frequency
        select_elevation = 45

        # select freqeucny to use for opacity vs. elevation
        select_freq_MHz = 20000


        # numpy array of elevation in degrees
        npts = int((elevation_max_deg - elevation_min_deg)/elevation_step_deg + 1)
        ee_deg = np.linspace(elevation_min_deg, elevation_max_deg, npts)

        # numpy array of frequency in MHz
        npts = int((freq_max_Hz - freq_min_Hz)/freq_step_Hz + 1)
        ff_Hz = np.linspace(freq_min_Hz, freq_max_Hz, npts)
        ff_MHz = ff_Hz / 1e6

        # Initialize opacity to values of -1 so that we know it is not real
        opacity_ff = -1*np.ones(ff_MHz.size)
        opacity_ee = -1*np.ones(ee_deg.size)

        for k in range(ff_MHz.size):
            opacity_ff[k] = self.objref.atmOpacity(pressure, temperature, humidity, ff_MHz[k], select_elevation)
            self.logger.info('processing freqency %d of %d: freq=%f MHz, opacity=%f' % (k + 1,ff_MHz.size,ff_MHz[k],opacity_ff[k]))

        for k in range(ee_deg.size):
            opacity_ee[k] = self.objref.atmOpacity(pressure, temperature, humidity, select_freq_MHz, ee_deg[k])
            self.logger.info('processing elevation %d of %d: EL=%f deg, opacity=%f' % (k + 1,ee_deg.size,ee_deg[k],opacity_ee[k]))	

        # Prepare plot figure window.  figsize is pixels/100.
        fig = plt.figure(figsize=(10,6))

        # Create subplot grid. 2 rows, 1 column, Select subplot 1. Opacity vs. Frequency for fixed elevation
        ax1 = plt.subplot(2,1,1)
        ax1.plot(ff_MHz,opacity_ff)
        ax1.set_title('Opacity (AM-10.0 model) @ EL %4.1f [deg], temp %4.2f [degC], pressure %4.2f [mbar], humidity %3.1f [%%]' %(select_elevation, temperature, pressure, humidity))
        ax1.set_xlabel('Frequency [MHz]')
        ax1.set_ylabel('Opacity [neper]')
        ax1.grid(True, which="both")

        # Select subplot 2.  Plot Opacity vs. Elevation for fixed freq
        ax2 = plt.subplot(2,1,2)
        ax2.plot(ee_deg,opacity_ee)
        ax2.set_title('Opacity (AM-10.0 model) @ freq %9.2f [MHz], temp %4.2f [degC], pressure %4.2f [mbar], humidity %3.1f [%%]' %(select_freq_MHz, temperature, pressure, humidity))
        ax2.set_xlabel('Elevation [deg]')
        ax2.set_ylabel('Opacity [neper]')
        ax2.grid(True, which="both")

        # Open the GUI window and show all
        plt.show()


    def test502_graphOpacity_ATM(self):
        self.logger.info('Initial values')
        self.logger.info('r0: %5.2f, b1: %5.2f, b2: %5.2f' % (self.objref.r0(), self.objref.b1(), self.objref.b2()))

        # range of values
        # pressure: 	500	<= 	p <=	2000
        # tempearute: 	-50	<= 	t <=	80
        # humidity: 	0 	<= 	h <=	100
        # frequency: 	500 <= 	f <=	200000
        # elevation: 	0 	<=	f <=	90
        # water vapor:	0	<	w

        pressure = 915.7
        temperature = 25
        humidity = 70

        # Set some values in the Atm object.  will be used to calculate new r0, and refraction angle.
        self.objref.setPressure(pressure)
        self.objref.setTemperature(temperature)
        self.objref.setHumidity(humidity)
        # w, precipitable water accounts for temperature, pressure, humidity set in Atm object.
        w = self.objref.wh2o()

        elevation_min_deg = 1
        elevation_max_deg = 90
        elevation_step_deg = 1

        freq_min_Hz = 1e9
        freq_max_Hz = 30e9
        freq_step_Hz = 1e9

        # select elevation to use for opacity vs. frequency
        select_elevation = 45

        # select freqeucny to use for opacity vs. elevation
        select_freq_MHz = 2000


        # numpy array of elevation in degrees
        npts = int((elevation_max_deg - elevation_min_deg)/elevation_step_deg + 1)
        ee_deg = np.linspace(elevation_min_deg, elevation_max_deg, npts)

        # numpy array of frequency in MHz
        npts = int((freq_max_Hz - freq_min_Hz)/freq_step_Hz + 1)
        ff_Hz = np.linspace(freq_min_Hz, freq_max_Hz, npts)
        ff_MHz = ff_Hz / 1e6

        # Initialize opacity to values of -1 so that we know it is not real
        opacity_ff = -1*np.ones(ff_MHz.size)
        opacity_ee = -1*np.ones(ee_deg.size)

        for k in range(ff_MHz.size):
            to = self.objref.atmOpacity_ATM(ff_MHz[k],w,select_elevation)
            opacity_ff[k] = to.signalSideBandOpacity
            self.logger.info('processing freqency %d of %d: freq=%f MHz, opacity=%f' % (k + 1,ff_MHz.size,ff_MHz[k],opacity_ff[k]))

        for k in range(ee_deg.size):
            to = self.objref.atmOpacity_ATM(select_freq_MHz,w,ee_deg[k])
            opacity_ee[k] = to.signalSideBandOpacity
            self.logger.info('processing elevation %d of %d: EL=%f deg, opacity=%f' % (k + 1,ee_deg.size,ee_deg[k],opacity_ee[k]))	

        # Prepare plot figure window.  figsize is pixels/100.
        fig = plt.figure(figsize=(10,6))

        # Create subplot grid. 2 rows, 1 column, Select subplot 1. Opacity vs. Frequency for fixed elevation
        ax1 = plt.subplot(2,1,1)
        ax1.plot(ff_MHz,opacity_ff)
        ax1.set_title('Opacity (ATM model) @ EL %4.1f [deg], temp %4.2f [degC], pressure %4.2f [mbar], humidity %3.1f [%%]' %(select_elevation, temperature, pressure, humidity))
        ax1.set_xlabel('Frequency [MHz]')
        ax1.set_ylabel('Opacity [neper]')
        ax1.grid(True, which="both")

        # Select subplot 2.  Plot Opacity vs. Elevation for fixed freq
        ax2 = plt.subplot(2,1,2)
        ax2.plot(ee_deg,opacity_ee)
        ax2.set_title('Opacity (ATM model) @ freq %9.2f [MHz], temp %4.2f [degC], pressure %4.2f [mbar], humidity %3.1f [%%]' %(select_freq_MHz, temperature, pressure, humidity))
        ax2.set_xlabel('Elevation [deg]')
        ax2.set_ylabel('Opacity [neper]')
        ax2.grid(True, which="both")

        # Open the GUI window and show all
        plt.show()

    def test503_graphOpacity_compare(self):
        self.logger.info('r0: %5.2f, b1: %5.2f, b2: %5.2f' % (self.objref.r0(), self.objref.b1(), self.objref.b2()))

        # range of values
        # pressure: 	500	<= 	p <=	2000
        # tempearute: 	-50	<= 	t <=	80
        # humidity: 	0 	<= 	h <=	100
        # frequency: 	500 <= 	f <=	200000
        # elevation: 	0 	<=	f <=	90

        pressure = 915.7
        temperature = 25
        humidity = 70

        # Set some values in the Atm object.  will be used to calculate new r0, and refraction angle.
        self.objref.setPressure(pressure)
        self.objref.setTemperature(temperature)
        self.objref.setHumidity(humidity)
        # w, precipitable water accounts for temperature, pressure, humidity set in Atm object.
        w = self.objref.wh2o()


        elevation_min_deg = 1
        elevation_max_deg = 90
        elevation_step_deg = 1

        freq_min_Hz = 1e9
        freq_max_Hz = 30e9
        freq_step_Hz = 0.2e9

        # select elevation to use for opacity vs. frequency
        select_elevation = 30

        # select freqeucny to use for opacity vs. elevation
        select_freq_MHz = 25000


        # numpy array of elevation in degrees
        npts = int((elevation_max_deg - elevation_min_deg)/elevation_step_deg + 1)
        ee_deg = np.linspace(elevation_min_deg, elevation_max_deg, npts)

        # numpy array of frequency in MHz
        npts = int((freq_max_Hz - freq_min_Hz)/freq_step_Hz + 1)
        ff_Hz = np.linspace(freq_min_Hz, freq_max_Hz, npts)
        ff_MHz = ff_Hz / 1e6

        # Initialize opacity to values of -1 so that we know it is not real
        opacity_ff_ATM = -1*np.ones(ff_MHz.size)
        opacity_ee_ATM = -1*np.ones(ee_deg.size)
        opacity_ff_AM = -1*np.ones(ff_MHz.size)
        opacity_ee_AM = -1*np.ones(ee_deg.size)


        for k in range(ff_MHz.size):
            to = self.objref.atmOpacity_ATM(ff_MHz[k],w,select_elevation)
            opacity_ff_ATM[k] = to.signalSideBandOpacity
            opacity_ff_AM[k] = self.objref.atmOpacity(pressure, temperature, humidity, ff_MHz[k], select_elevation)
            self.logger.info('processing freqency %d of %d: freq=%f MHz, opacity_ATM=%f, opacity_AM=%f' % (k + 1,ff_MHz.size,ff_MHz[k],opacity_ff_ATM[k], opacity_ff_AM[k]))

        for k in range(ee_deg.size):
            to = self.objref.atmOpacity_ATM(select_freq_MHz,w,ee_deg[k])
            opacity_ee_ATM[k] = to.signalSideBandOpacity
            opacity_ee_AM[k] = self.objref.atmOpacity(pressure, temperature, humidity, select_freq_MHz, ee_deg[k])
            self.logger.info('processing elevation %d of %d: EL=%f deg, opacity_ATM=%f, opacity_AM=%f' % (k + 1,ee_deg.size,ee_deg[k],opacity_ee_ATM[k], opacity_ee_AM[k]))	

        # Prepare plot figure window.  figsize is pixels/100.
        fig = plt.figure(figsize=(10,6))

        # Create subplot grid. 2 rows, 1 column, Select subplot 1. Opacity vs. Frequency for fixed elevation
        ax1 = plt.subplot(2,1,1)
        ax1.plot(ff_MHz,opacity_ff_ATM, 'r',label='ATM (2001)')
        ax1.plot(ff_MHz,opacity_ff_AM, 'b', label='AM-10.0 (2018)')
        ax1.set_title('Opacity @ EL %4.1f [deg], temp %4.2f [degC], pressure %4.2f [mbar], humidity %3.1f [%%]' %(select_elevation, temperature, pressure, humidity))
        ax1.set_xlabel('Frequency [MHz]')
        ax1.set_ylabel('Opacity [neper]')
        ax1.grid(True, which="both")
        ax1.legend(loc=0) # let matplotlib choose location of legend

        # Select subplot 2.  Plot Opacity vs. Elevation for fixed freq
        ax2 = plt.subplot(2,1,2)
        ax2.plot(ee_deg,opacity_ee_ATM, 'r',label='ATM (2001)')
        ax2.plot(ee_deg,opacity_ee_AM, 'b', label='AM-10.0 (2018)')
        ax2.set_title('Opacity @ freq %9.2f [MHz], temp %4.2f [degC], pressure %4.2f [mbar], humidity %3.1f [%%]' %(select_freq_MHz, temperature, pressure, humidity))
        ax2.set_xlabel('Elevation [deg]')
        ax2.set_ylabel('Opacity [neper]')
        ax2.grid(True, which="both")
        ax2.legend(loc=0) # let matplotlib choose location of legend

        # Open the GUI window and show all
        plt.show()



if __name__ == '__main__':
    unittest.main()
