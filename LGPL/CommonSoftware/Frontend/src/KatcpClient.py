from katcp import BlockingClient, MessageParser
import time
import FrontendConfig as config
import FrontendMod
from astropy.time import Time


class KatcpClient(BlockingClient):
    def __init__(self, host, port):
        self.msg_parser = MessageParser()
        super(KatcpClient, self).__init__(host, port)
        self.start()

    def __del__(self):
        self.stop()
        self.join()
        self.disconnect()

    def request(self, command):
        msg = self.msg_parser.parse(command.encode())
        return self.blocking_request(msg)

    def get_sensor_value(self, sensor):
        (reply, inform) = self.request("?sensor-value {}".format(sensor))
        self.check_reply_message(reply, inform)

        if len(inform) > 1 and len(inform[0].arguments) == 1:  # getting error
            return inform[0].arguments[0].decode()

        return inform[0].arguments[4].decode("utf-8")

    def check_reply_message(self, reply, inform):
        if reply.arguments[0].decode() == "invalid":
            raise RuntimeError("Error: {}".format(reply.arguments[1].decode()))

        if reply.arguments[0].decode() == "fail":
            raise RuntimeError("Error: {}".format(inform[0].arguments[0].decode()))

    def debug_message(self, reply, inform):
        print("===============reply===============")
        print("reply - {}".format(reply))
        print("mtype = {}".format(reply.mtype))
        print("name = {}".format(reply.name))
        print("arguments = {}".format(reply.arguments))
        print("mid = {}".format(reply.mid))
        print("---------inform-----------")
        for m in inform:
            print("mtype = {}".format(m.mtype))
            print("name = {}".format(m.name))
            print("arguments = {}".format(m.arguments))
            print("mid = {}".format(m.mid))
            print("------")

    def set_digitizer(self, val):
        if val not in [True, False]:
            raise ValueError("Value error: value should be boolean")
        if not self.is_connected():
            raise ConnectionError("Connection error: Device is not connected")

        convertedVal = None
        if val:
            convertedVal = "true"
        else:
            convertedVal = "false"

        (reply, inform) = self.request("?rxs-power-digitizer {}".format(convertedVal))
        self.check_reply_message(reply, inform)

    def set_down_converter(self, val):
        if val < 14000 or val > 212000:
            raise ValueError("Value error: value should be between 14000-212000")
        if not self.is_connected():
            raise ConnectionError("Connection error: Device is not connected")

        (reply, inform) = self.request("?rxs-dwnc-frequency {}".format(val))
        self.check_reply_message(reply, inform)

    def set_attenuation(self, chain, val):
        if val < 0 or val > 31.5 or chain not in ["1", "2"]:
            raise ValueError("Value error: chain should be 1 or 2 and value should be between 0-31.5")
        if not self.is_connected():
            raise ConnectionError("Connection error: Device is not connected")

        (reply, inform) = self.request("?rxs-signalprocessors-sp0{}-attenuation {}".format(chain, val))
        self.check_reply_message(reply, inform)

    def set_operation_mode(self, mode):
        if mode not in [
            "go-soft-off",
            "go-cold-operational",
            "go-debug",
            "do-regeneration-to-warm",
            "do-regeneration",
            "error-noncritical-stay-cold",
            "error-noncritical-soft-off",
            "error-critical",
        ]:
            raise ValueError("Value error: mode incorrect")
        if not self.is_connected():
            raise ConnectionError("Connection error: Device is not connected")

        (reply, inform) = self.request("?rxs-rsm-opmode {}".format(mode))
        self.check_reply_message(reply, inform)

    def set_mmic(self, chain, val):
        if val not in [True, False] or chain not in ["1", "2"]:
            raise ValueError("Value error: chain should be 1 or 2 and value should be boolean")
        if not self.is_connected():
            raise ConnectionError("Connection error: Device is not connected")

        convertedVal = None
        if val:
            convertedVal = "true"
        else:
            convertedVal = "false"
        (reply, inform) = self.request("?rxs-mmic-enable-mmic{} {}".format(chain, convertedVal))
        self.check_reply_message(reply, inform)

    def set_cryo_motor(self, val):
        if val < 45 or val > 90:
            raise ValueError("Value error: value should be between 45-90")
        if not self.is_connected():
            raise ConnectionError("Connection error: Device is not connected")
        (reply, inform) = self.request("?rxs-cryocooler-motorstartspeed {}".format(val))
        self.check_reply_message(reply, inform)

    def shutdown(self):
        self.set_digitizer(False)
        self.set_operation_mode("go-soft-off")

    def get_data(self, receiver):

        if not self.is_connected():
            raise ConnectionError("Connection error: Device is not connected")
        
        down_converter_freq_mHz = 0

        if receiver == 'K':
            down_converter_freq_mHz = float(self.get_sensor_value("rxs.dwnc.frequency"))
        
        data_dict = {
            'level1pps':self.get_sensor_value("rxs.tfr.level1pps"),
            'level100mhz':self.get_sensor_value("rxs.tfr.level100mhz"),
            'valve_sts': self.get_sensor_value("rxs.vac-valve-open"),
            'temp_cold':self.get_sensor_value("rxs.boxweather.tempcold"),
            'temp_warm':self.get_sensor_value("rxs.boxweather.tempwarm"),
            'temp_object': self.get_sensor_value("rxs.atc.objecttemp"),
            'temp_sink': self.get_sensor_value("rxs.atc.sinktemp"),
            'lna_status': [
                1 if self.get_sensor_value("rxs.mmic.enable.mmic1") == 'true' else 0,
                1 if self.get_sensor_value("rxs.mmic.enable.mmic2") == 'true' else 0
            ],
            'attenuation': [
                self.get_sensor_value("rxs.signalprocessors.sp01.attenuation"),
                self.get_sensor_value("rxs.signalprocessors.sp02.attenuation"),
            ],
            'total_power': [
                self.get_sensor_value("rxs.signalprocessors.sp01.totalpower"),
                self.get_sensor_value("rxs.signalprocessors.sp02.totalpower"),
            ],
            'timestamp': Time.now().mjd,
            'heater20k': self.get_sensor_value("rxs.dewar.heater20k"),
            'heater70k': self.get_sensor_value("rxs.dewar.heater70k"),
            't15': self.get_sensor_value("rxs.tempvac.temp15k"),
            't70': self.get_sensor_value("rxs.tempvac.temp70k"),
            'down_converter_freq': down_converter_freq_mHz * 1e6,
            'digitizer_status': self.get_sensor_value("rxs.power.digitizer")
        }
        return data_dict


if __name__ == "__main__":
    try:
        app = KatcpClient(config.kband_ip, config.kband_port)
        while not app.is_connected():
            print("connecting...")
            time.sleep(0.5)

        print("connected")
        # app.shutdown()

    except Exception as error:
        print(error)
    except KeyboardInterrupt:
        app.stop()
        app.join()
    finally:
        app.stop()
        app.join()
