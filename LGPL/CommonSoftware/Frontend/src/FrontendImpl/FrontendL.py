# -*- coding: utf-8 -*-
import json
from Acspy.Servants.ACSComponent import ACSComponent
from Acspy.Servants.ContainerServices import ContainerServices
from Acspy.Servants.ComponentLifecycle import ComponentLifecycle
from Acspy.Servants.ComponentLifecycle import ComponentLifecycleException
from Acspy.Nc.Supplier import Supplier
from Acspy.Nc.Consumer import Consumer
import ACSErr

from .BaseFrontend import BaseFrontend

# TCS packages
import FrontendMod
import FrontendMod__POA
import FrontendErrorImpl
import FrontendError

import FrontendConfig as config


class FrontendL(BaseFrontend, FrontendMod__POA.FrontendL, ACSComponent, ContainerServices, ComponentLifecycle):

    # Constructor
    def __init__(self):
        ACSComponent.__init__(self)
        ContainerServices.__init__(self)
        super(FrontendL, self).__init__(config.lband_ip, config.lband_port)
        self.logger = self.getLogger()

    # Destructor
    def __del__(self):
        pass

    # ------------------------------------------------------------------------------------
    # LifeCycle functions called by ACS system when we activate and deactivate components.
    # Inherited from headers in Acspy.Servants.ComponentLifecycle.

    def initialize(self):
        self.logger.logInfo("INITIALIZE")

    def execute(self):
        self.logger.logInfo("EXECUTE")

    def aboutToAbort(self):
        self.logger.logInfo("ABOUT_TO_ABORT")

    def cleanUp(self):
        self.logger.logInfo("CLEANUP")

        self.event_stop_stream_monitor_data.set()

        for key in self.outputs:
            self.logger.logInfo('Destroying Notification Channel Supplier %s: ' % key)
            if self.outputs[key]['supplier'] is not None:
                try:
                    self.outputs[key]['supplier'].disconnect()
                except:
                    self.outputs[key] = None

        # stop katcp client
        self.client.stop()
        self.client.disconnect()

    # ------------------------------------------------------------------------------------
    # Functions defined in IDL to interface from outside this class

    def get_data(self):
        try:
            data = self.client.get_data('L')
            return json.dumps(data)
        except ValueError:
            raise FrontendErrorImpl.inputErrorExImpl()
        except RuntimeError:
            raise FrontendErrorImpl.requestErrorExImpl()
        except ConnectionError:
            raise FrontendErrorImpl.noConnectionErrorExImpl()

    # ------------------------------------------------------------------------------------
    # Internal functions not exposed in IDL


if __name__ == "__main__":
    t = FrontendL()
