import socket

# connection
kband_ip = socket.gethostbyname("KBand")
kband_port = 53121

lband_ip = socket.gethostbyname("LBand")
lband_port = 53121

# NC channel
nc_channel_monitor = 'channel_frontend_monitor'
