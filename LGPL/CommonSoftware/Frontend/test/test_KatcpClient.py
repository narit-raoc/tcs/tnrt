from multiprocessing.sharedctypes import Value
import unittest
from unittest.mock import patch, MagicMock, call
from KatcpClient import KatcpClient
from katcp import Message
import FrontendMod


class MockMessageParser:
    def parse(self, msg):
        return "modified {}".format(msg.decode())


class TestKatcpClient(unittest.TestCase):
    @patch("KatcpClient.MessageParser")
    def setUp(self, mock_MessageParser):
        self.mock_start = MagicMock()
        self.mock_stop = MagicMock()
        self.mock_join = MagicMock()
        self.mock_disconnect = MagicMock()
        self.mock_blocking_request = MagicMock()
        self.mock_is_connected = MagicMock()
        mock_MessageParser.return_value = MockMessageParser()

        def mock_init(mock_init_self, host, port):
            mock_init_self.start = self.mock_start
            mock_init_self.stop = self.mock_stop
            mock_init_self.join = self.mock_join
            mock_init_self.disconnect = self.mock_disconnect
            mock_init_self.is_connected = self.mock_is_connected
            mock_init_self.blocking_request = self.mock_blocking_request

        with patch("KatcpClient.BlockingClient.__init__", new=mock_init):
            self.client = KatcpClient("host", "port")
            self.mock_start.assert_called_once()
            mock_MessageParser.assert_called_once()

    def test_request(self):
        """[request] should call blocking_request function with modified message"""
        self.mock_blocking_request.return_value = "result"
        result = self.client.request("msg")
        self.mock_blocking_request.assert_called_once_with("modified msg")
        self.assertEqual(result, "result")

    def test_get_sensor_value_success(self):
        """[get_sensor_value] should return sensor value when getting normal message from server"""
        mock_request = MagicMock()
        mock_check_reply_message = MagicMock()
        self.client.request = mock_request
        self.client.check_reply_message = mock_check_reply_message
        mock_request.return_value = (
            "reply",
            [
                Message(
                    mtype=1,
                    name="mock-msg",
                    arguments=[
                        b"date",
                        b"1",
                        b"something",
                        b"something",
                        b"sensor value",
                    ],
                )
            ],
        )
        result = self.client.get_sensor_value("sensor name")
        mock_check_reply_message.assert_called_once()
        self.assertEqual(result, "sensor value")

    def test_get_sensor_value_error(self):
        """[get_sensor_value] should return sensor value when getting normal message from server"""
        mock_request = MagicMock()
        mock_check_reply_message = MagicMock()
        self.client.request = mock_request
        self.client.check_reply_message = mock_check_reply_message
        mock_request.return_value = (
            "reply",
            [
                Message(
                    mtype=1,
                    name="mock-msg",
                    arguments=[b"error message from server"],
                ),
                Message(
                    mtype=1,
                    name="mock-msg",
                    arguments=[
                        b"date",
                        b"1",
                        b"something",
                        b"something",
                        b"sensor value",
                    ],
                ),
            ],
        )
        result = self.client.get_sensor_value("sensor name")
        mock_check_reply_message.assert_called_once()
        self.assertEqual(result, "error message from server")

    def test_check_reply_message_invalid(self):
        """[check_reply_message] should raise runtime error if reply is invalid"""
        mock_reply = Message(
            mtype=1,
            name="mock-msg",
            arguments=[b"invalid", b"error message"],
        )
        with self.assertRaises(RuntimeError):
            self.client.check_reply_message(mock_reply, "mock inform")

    def test_check_reply_message_fail(self):
        """[check_reply_message] should raise runtime error if reply is fail"""
        mock_reply = Message(
            mtype=1,
            name="mock-msg",
            arguments=[b"fail", b"error message"],
        )
        mock_inform = [
            Message(
                mtype=1,
                name="mock-msg",
                arguments=[b"error message"],
            )
        ]
        with self.assertRaises(RuntimeError):
            self.client.check_reply_message(mock_reply, mock_inform)

    def test_set_digitizer_wrong_input(self):
        """[set_digitizer] should raise ValueError if input is not correct"""
        with self.assertRaises(ValueError):
            self.client.set_digitizer("wrong input")

    def test_set_digitizer_no_connection(self):
        """[set_digitizer] should raise ConnectionError if instrument is not connected"""
        self.mock_is_connected.return_value = False
        with self.assertRaises(ConnectionError):
            self.client.set_digitizer(True)

    def test_set_digitizer_success(self):
        """[set_digitizer] should call check_reply_message if no error occured"""
        mock_request = MagicMock()
        mock_check_reply_message = MagicMock()
        mock_request.return_value = ("mock reply", "mock inform")
        self.client.check_reply_message = mock_check_reply_message
        self.client.request = mock_request
        self.client.set_digitizer(True)
        mock_check_reply_message.assert_called_once()
        mock_request.assert_called_once_with("?rxs-power-digitizer true")

    def test_set_down_converter_wrong_input(self):
        """[set_down_converter] should raise ValueError if input is not correct"""
        with self.assertRaises(ValueError):
            self.client.set_down_converter(1)

    def test_set_down_converter_no_connection(self):
        """[set_down_converter] should raise ConnectionError if instrument is not connected"""
        self.mock_is_connected.return_value = False
        with self.assertRaises(ConnectionError):
            self.client.set_down_converter(15000)

    def test_set_down_converter_success(self):
        """[set_down_converter] should call check_reply_message if no error occured"""
        mock_request = MagicMock()
        mock_check_reply_message = MagicMock()
        mock_request.return_value = ("mock reply", "mock inform")
        self.client.check_reply_message = mock_check_reply_message
        self.client.request = mock_request
        self.client.set_down_converter(15000)
        mock_check_reply_message.assert_called_once()
        mock_request.assert_called_once_with("?rxs-dwnc-frequency 15000")

    def test_set_attenuation_wrong_input(self):
        """[set_attenuation] should raise ValueError if input is not correct"""
        with self.assertRaises(ValueError):
            self.client.set_attenuation("1", 35)

    def test_set_attenuation_no_connection(self):
        """[set_attenuation] should raise ConnectionError if instrument is not connected"""
        self.mock_is_connected.return_value = False
        with self.assertRaises(ConnectionError):
            self.client.set_attenuation("1", 15)

    def test_set_attenuation_success(self):
        """[set_attenuation] should call check_reply_message if no error occured"""
        mock_request = MagicMock()
        mock_check_reply_message = MagicMock()
        mock_request.return_value = ("mock reply", "mock inform")
        self.client.check_reply_message = mock_check_reply_message
        self.client.request = mock_request
        self.client.set_attenuation("1", 15)
        mock_check_reply_message.assert_called_once()
        mock_request.assert_called_once_with("?rxs-signalprocessors-sp01-attenuation 15")

    def test_set_operation_mode_wrong_input(self):
        """[set_operation_mode] should raise ValueError if input is not correct"""
        with self.assertRaises(ValueError):
            self.client.set_operation_mode("wrong input")

    def test_set_operation_mode_no_connection(self):
        """[set_operation_mode] should raise ConnectionError if instrument is not connected"""
        self.mock_is_connected.return_value = False
        with self.assertRaises(ConnectionError):
            self.client.set_operation_mode("go-soft-off")

    def test_set_operation_mode_success(self):
        """[set_operation_mode] should call check_reply_message if no error occured"""
        mock_request = MagicMock()
        mock_check_reply_message = MagicMock()
        mock_request.return_value = ("mock reply", "mock inform")
        self.client.check_reply_message = mock_check_reply_message
        self.client.request = mock_request
        self.client.set_operation_mode("go-soft-off")
        mock_check_reply_message.assert_called_once()
        mock_request.assert_called_once_with("?rxs-rsm-opmode go-soft-off")

    def test_set_mmic_wrong_input(self):
        """[set_mmic] should raise ValueError if input is not correct"""
        with self.assertRaises(ValueError):
            self.client.set_mmic("3", True)

    def test_set_mmic_no_connection(self):
        """[set_mmic] should raise ConnectionError if instrument is not connected"""
        self.mock_is_connected.return_value = False
        with self.assertRaises(ConnectionError):
            self.client.set_mmic("1", True)

    def test_set_mmic_success(self):
        """[set_mmic] should call check_reply_message if no error occured"""
        mock_request = MagicMock()
        mock_check_reply_message = MagicMock()
        mock_request.return_value = ("mock reply", "mock inform")
        self.client.check_reply_message = mock_check_reply_message
        self.client.request = mock_request
        self.client.set_mmic("1", True)
        mock_check_reply_message.assert_called_once()
        mock_request.assert_called_once_with("?rxs-mmic-enable-mmic1 true")

    def test_set_cryo_motor_wrong_input(self):
        """[set_cryo_motor] should raise ValueError if input is not correct"""
        with self.assertRaises(ValueError):
            self.client.set_cryo_motor(1)

    def test_set_cryo_motor_no_connection(self):
        """[set_cryo_motor] should raise ConnectionError if instrument is not connected"""
        self.mock_is_connected.return_value = False
        with self.assertRaises(ConnectionError):
            self.client.set_cryo_motor(60)

    def test_set_cryo_motor_success(self):
        """[set_cryo_motor] should call check_reply_message if no error occured"""
        mock_request = MagicMock()
        mock_check_reply_message = MagicMock()
        mock_request.return_value = ("mock reply", "mock inform")
        self.client.check_reply_message = mock_check_reply_message
        self.client.request = mock_request
        self.client.set_cryo_motor(60)
        mock_check_reply_message.assert_called_once()
        mock_request.assert_called_once_with("?rxs-cryocooler-motorstartspeed 60")

    def test_get_data_no_connection(self):
        """[get_data] should raise ConnectionError if instrument is not connected"""
        self.mock_is_connected.return_value = False
        with self.assertRaises(ConnectionError):
            self.client.get_data('K')

    def test_get_data_K_success(self):
        """[get_data] should call check_reply_message if no error occured"""
        mock_get_sensor_value = MagicMock()
        mock_check_reply_message = MagicMock()
        mock_get_sensor_value.return_value = ("mock reply", "mock inform")
        self.client.check_reply_message = mock_check_reply_message
        self.client.get_sensor_value = mock_get_sensor_value
        result = self.client.get_data('K')
        mock_get_sensor_value.assert_has_calls(
            [
                call("rxs.tfr.level1pps"),
                call("rxs.tfr.level100mhz"),
                call("rxs.vac-valve-open"),
                call("rxs.boxweather.tempcold"),
                call("rxs.boxweather.tempwarm"),
                call("rxs.atc.objecttemp"),
                call("rxs.atc.sinktemp"),
                call("rxs.mmic.enable.mmic1"),
                call("rxs.mmic.enable.mmic2"),
                call("rxs.signalprocessors.sp01.attenuation"),
                call("rxs.signalprocessors.sp02.attenuation"),
                call("rxs.signalprocessors.sp01.totalpower"),
                call("rxs.signalprocessors.sp02.totalpower"),
                call("rxs.dewar.heater20k"),
                call("rxs.dewar.heater70k"),
                call("rxs.tempvac.temp15k"),
                call("rxs.tempvac.temp70k"),
                call("rxs.dwnc.frequency"),
            ]
        )
        self.assertEqual(type(result), dict)

    def test_get_data_success(self):
        """[get_data] should call check_reply_message if no error occured"""
        mock_get_sensor_value = MagicMock()
        mock_check_reply_message = MagicMock()
        mock_get_sensor_value.return_value = ("mock reply", "mock inform")
        self.client.check_reply_message = mock_check_reply_message
        self.client.get_sensor_value = mock_get_sensor_value
        result = self.client.get_data('L')
        mock_get_sensor_value.assert_has_calls(
            [
                call("rxs.tfr.level1pps"),
                call("rxs.tfr.level100mhz"),
                call("rxs.vac-valve-open"),
                call("rxs.boxweather.tempcold"),
                call("rxs.boxweather.tempwarm"),
                call("rxs.atc.objecttemp"),
                call("rxs.atc.sinktemp"),
                call("rxs.mmic.enable.mmic1"),
                call("rxs.mmic.enable.mmic2"),
                call("rxs.signalprocessors.sp01.attenuation"),
                call("rxs.signalprocessors.sp02.attenuation"),
                call("rxs.signalprocessors.sp01.totalpower"),
                call("rxs.signalprocessors.sp02.totalpower"),
                call("rxs.dewar.heater20k"),
                call("rxs.dewar.heater70k"),
                call("rxs.tempvac.temp15k"),
                call("rxs.tempvac.temp70k"),
            ]
        )
        self.assertEqual(type(result), dict)
