import unittest
import logging
import coloredlogs
import time
import socket
from unittest.mock import patch, Mock, MagicMock

# import ACS modules
from Acspy.Clients.SimpleClient import PySimpleClient

# import TCS modules
import FrontendMod
import FrontendErrorImpl
import FrontendError

level_styles_scrn = {
    "critical": {"color": "red", "bold": True},
    "debug": {"color": "white", "faint": True},
    "error": {"color": "red"},
    "info": {"color": "green", "bright": True},
    "notice": {"color": "magenta"},
    "spam": {"color": "green", "faint": True},
    "success": {"color": "green", "bold": True},
    "verbose": {"color": "blue"},
    "warning": {"color": "yellow", "bright": True, "bold": True},
}
field_styles_scrn = {
    "asctime": {},
    "hostname": {"color": "magenta"},
    "levelname": {"color": "cyan", "bright": True},
    "name": {"color": "blue", "bright": True},
    "programname": {"color": "cyan"},
}


class TestFrontendK(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        # Configure logger
        self.logger = logging.getLogger("Frontend")
        self.logger.setLevel(logging.DEBUG)

        fmt_scrn = "%(asctime)s [%(levelname)s]: %(message)s"
        formatter_screen = coloredlogs.ColoredFormatter(
            fmt=fmt_scrn, level_styles=level_styles_scrn, field_styles=field_styles_scrn
        )

        # creating a handler to log on the console
        handler_screen = logging.StreamHandler()
        handler_screen.setFormatter(formatter_screen)
        handler_screen.setLevel(logging.DEBUG)

        # remove all handlers if they exist
        # remove because PySimpleClient logger already has handlers which
        # show duplicate of my log messages, but without the pretty color format.
        if len(self.logger.handlers) > 0:
            self.logger.handlers = []

        # adding handlers
        self.logger.addHandler(handler_screen)
        self.logger.debug("Started logger name %s" % self.logger.name)

        self.logger.debug("Starting PySimpleClient")
        self.sc = None
        self.sc = PySimpleClient()

        self.Kband = None
        self.component_name = "FrontendK"

        # Get reference to ComponentA.  If it is already activated by
        # ACS Command Center, this will be a reference to the same object.
        # If it is not active, this command will activate and create a new instance.
        try:
            self.logger.debug("Connecting to ACS component %s" % self.component_name)

            self.Kband = self.sc.getComponent(self.component_name)
        except Exception as e:
            self.logger.error("Cannot get ACS component object reference for %s" % self.component_name)
            self.cleanup()

    @classmethod
    def tearDownClass(self):
        try:
            self.logger.debug("Releasing ACS component %s" % self.component_name)
            self.sc.releaseComponent(self.component_name)
            self.Kband.stop()
            self.Kband.join()
            self.Kband = None
        except Exception as e:
            self.logger.error("Cannot release component reference for %s" % self.component_name)

        if self.sc is not None:
            self.sc.disconnect()
            del self.sc

    def test_01_connect(self):
        """[connect] should connect return ResponseMessage success"""
        result = self.Kband.connect()
        self.assertEqual(type(result), FrontendMod.ResponseMessage)
        self.assertEqual(result.status, "success")

    def test_02_set_operation_mode_wrong_input(self):
        """[set_operation_mode] should raise inputError"""
        with self.assertRaises(FrontendError.inputErrorEx):
            self.Kband.set_operation_mode("wrong input")

    def test_03_set_operation_mode_success(self):
        """[set_operation_mode] should return ReponseMessage success and current status"""
        self.Kband.set_operation_mode("go-soft-off")
        result = self.Kband.set_operation_mode("go-soft-off")
        self.assertEqual(type(result), FrontendMod.ResponseMessage)
        self.assertEqual(result.status, "success")
        self.assertEqual(result.message, "operation mode: go-soft-off")

    def test_04_set_mmic_success(self):
        """[set_mmic] should return ReponseMessage success and current status"""
        result = self.Kband.set_mmic("1", True)
        self.assertEqual(type(result), FrontendMod.ResponseMessage)
        self.assertEqual(result.status, "success")
        self.assertEqual(result.message, "chain a status: on")

    def test_04_set_mmic_wrong_input(self):
        """[set_mmic] should raise inputError"""
        with self.assertRaises(FrontendError.inputErrorEx):
            self.Kband.set_mmic("5", True)

    def test_05_set_digitizer_success(self):
        """[set_digitizer] should return ReponseMessage success and current status"""
        result = self.Kband.set_digitizer(True)
        self.assertEqual(type(result), FrontendMod.ResponseMessage)
        self.assertEqual(result.status, "success")
        self.assertEqual(result.message, "digitizer status: on")

    def test_05_set_down_converter_wrong_input(self):
        """[set_down_converter] should raise inputError"""
        with self.assertRaises(FrontendError.inputErrorEx):
            self.Kband.set_down_converter(1)

    def test_05_set_down_converter_success(self):
        """[set_down_converter] should return ReponseMessage success and current status"""
        result = self.Kband.set_down_converter(15000)
        self.assertEqual(type(result), FrontendMod.ResponseMessage)
        self.assertEqual(result.status, "success")
        self.assertEqual(result.message, "down converter value: 15000.0")

    def test_05_set_attenuation_success(self):
        """[set_attenuation] should return ReponseMessage success and current status"""
        result = self.Kband.set_attenuation("1", 15)
        self.assertEqual(type(result), FrontendMod.ResponseMessage)
        self.assertEqual(result.status, "success")
        self.assertEqual(result.message, "attenuation value: 15.0")

    def test_05_set_attenuation_wrong_input(self):
        """[set_attenuation] should raise inputError"""
        with self.assertRaises(FrontendError.inputErrorEx):
            self.Kband.set_attenuation("5", 100)

    def test_05_set_cryo_motor_success(self):
        """[set_cryo_motor] should return ReponseMessage success and current status"""
        result = self.Kband.set_cryo_motor(45)
        self.assertEqual(type(result), FrontendMod.ResponseMessage)
        self.assertEqual(result.status, "success")
        self.assertEqual(result.message, "cryo motor speed: 45.0")

    def test_05_set_cryo_motor_wrong_input(self):
        """[set_cryo_motor] should raise inputError"""
        with self.assertRaises(FrontendError.inputErrorEx):
            self.Kband.set_cryo_motor(1)

    def test_06_configure_frontend_success(self):
        """[configure_frontend] should return ResponseMessage success and current status"""
        result = self.Kband.configure_frontend(True, True)
        self.assertEqual(type(result), FrontendMod.ResponseMessage)
        self.assertEqual(result.status, "success")
        self.assertEqual(result.message, "frontend configured successfully")

    def test_06_configure_frontend_wrong_input(self):
        """[configure_frontend] should riase inputError"""
        with self.assertRaises(FrontendError.inputErrorEx):
            self.Kband.configure_frontend("wrong_input", True)

    def test_07_deconfigure_frontend_success(self):
        """[deconfigure_frontend] should return ResponseMessage success and current status"""
        result = self.Kband.deconfigure_frontend(True, True)
        self.assertEqual(type(result), FrontendMod.ResponseMessage)
        self.assertEqual(result.status, "success")
        self.assertEqual(result.message, "frontend deconfigure successfully")

    def test_07_deconfigure_frontend_wrong_input(self):
        """[deconfigure_frontend] should raise inputError"""
        with self.assertRaises(FrontendError.inputErrorEx):
            self.Kband.deconfigure_frontend("wrong_input", True)

    def test_10_get_data_success(self):
        """[get_data] should return ReponseMessage success and current status"""
        time.sleep(5)
        result = self.Kband.get_data()
        self.assertEqual(type(result), FrontendMod.KbandData)
        self.assertEqual(result.device_sts, "ok")
        self.assertEqual(result.down_converter.freq, "15000")
        self.assertEqual(result.chain_a.attenuation, "15")
        self.assertEqual(result.chain_a.status, "true")
