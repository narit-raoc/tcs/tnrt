# Project: TNRT - Thai National Radio Telescope
# Organization: NARIT - National Astronomical Research Institute of Thailand
# Author: Spiro Sarris
import json
import ScanMod

class BackendModelFactory:
    def __init__(self, backend_params):
        if isinstance(backend_params, ScanMod.BackendParamsEDD):
            if backend_params.mock == True:
                self.name = "MOCK_EDD"
                self.component_name = "BackendMockEDD"
            else:
                self.name = "EDD"
                self.component_name = "BackendEDD"
            self.preset_config_name = backend_params.preset_config_name
            self.kwargs_json = backend_params.kwargs_json
        elif isinstance(backend_params, ScanMod.BackendParamsHoloFFT):
            try:
                is_mock = (json.loads(backend_params.kwargs_json)["mock"] == True)
            except KeyError:
                # "mock" does not exist in the list of parameters. Therefore, is_mock = False
                is_mock = False
            
            if is_mock:
                self.name = "MOCK_HOLOFFT"
                self.component_name = "BackendMockHoloFFT"
            else:
                self.name = "HOLOFFT"
                self.component_name = "BackendHoloFFT"
            self.preset_config_name = backend_params.preset_config_name
            self.kwargs_json = backend_params.kwargs_json
        elif isinstance(backend_params, ScanMod.BackendParamsHoloSDR):
            self.name = "HOLOSDR"
            self.component_name = "BackendHoloSDR"
            self.preset_config_name = backend_params.preset_config_name
            self.kwargs_json = backend_params.kwargs_json
        elif isinstance(backend_params, ScanMod.BackendParamsSKARAB):
            self.name = "SKARAB"
            self.component_name = "BackendSKARAB"
            self.preset_config_name = backend_params.preset_config_name
            self.kwargs_json = backend_params.kwargs_json
        elif isinstance(backend_params, ScanMod.BackendParamsOptical):
            self.name = "OPTICAL"
            self.component_name = "BackendOptical"
            self.preset_config_name = backend_params.preset_config_name
            self.kwargs_json = backend_params.kwargs_json
        elif isinstance(backend_params, ScanMod.BackendParamsPowerMeter):
            self.name = "POWERMETER"
            self.component_name = "BackendPowerMeter"
            self.preset_config_name = backend_params.preset_config_name
            self.kwargs_json = backend_params.kwargs_json
        else:
            self.name = "NULL"
            self.component_name = ""
            self.preset_config_name = "NONE"


# Main defined only for testing with data in native format
# Not passing data throuth IDL type translations
if __name__ == "__main__":
    pass
