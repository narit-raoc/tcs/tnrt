# Project: TNRT - Thai National Radio Telescope
# Organization: NARIT - National Astronomical Research Institute of Thailand
# Author: Spiro Sarris
# Date: 2021.11.10

import logging
from abc import ABC
from abc import abstractmethod
import threading
import numpy as np
from astropy.time import Time
import time

# Import Alma Common Software (ACS)
from Acspy.Clients.SimpleClient import PySimpleClient

# TNRT
import tnrtAntennaMod


class AbstractBaseSubscan(ABC):
    """
    AbstractBaseSubscan an abstract class that cannot be instantiated.
    Some common functions and variables are implemented in this abstract class and can be
    used from child classes with the super(). functions.

    Python will not allow the user to create an instance of this class because it contains
    virtual functions.
    """

    # Define properties and methods that are abstract, and must be implemented by a subclass
    # before a Subscan object can be created.

    @abstractmethod
    def set_interpolation_mode(self):
        """
        NEWTON, SPLINE, etc.
        """
        pass

    @abstractmethod
    def set_nlines(self):
        """
        number of data points in the program offset table
        """
        pass

    @abstractmethod
    def generate_subscan_table(
        self, nlines, start_mjd, end_mjd, wait_at_first_coord=False
    ):
        """
        Every subscan has either a program offset table or a hexapod program
        track table.  The details to generate it such as line, single point,
        selection of axis, ... must be implemented in subclass.  For example,

        Classes that inherit from AbstractSubscanAZEL, will implement this method
        with a table of values including  {time, az/ra,  el/dec} while tracking a
        central tracking coordinate  (ACU program track table).

        Classes that inherit from AbstractSubscanHXP, will implement this method
        with a table of values including
        {time, hxp_x, hxp_y, hxp_z, hxp_tx, hxp_ty, hxp_tz} while tracking a
        central tracking coordinate (ACU program track table).

        Perhaps in the future, a new type of subscan will generate both
        program offset table and hexapod program track table.  As of now (FEB 2021),
        a subscan only uses either the AZEL or HXP table format, but not both.

        Parameters
        ----------
        nlines : int
                The number of lines in the tracking table.  Minimum is 5 for ACU interpolation
                algorithm.  Maximum is 20 or 50 depending on which type of table is implemented
                (AZEL or HXP).  For tracking tables that have a linear trajectory, we use the
                minimum number of entries and let the ACU interpolate the line.
        table_limits : numpy ndarray
                2D array where the first column is time in units MJD.
                AZEL tables have 2 more columns: AzRA, ElDec
                HXP tables have 6 more columns: x, y, z, tx, ty, tz
        """
        pass

    @abstractmethod
    def start_subscan_tracking(self, table):
        """
        Every subscan must send a table of data to the ACU and start tracking.

        AbstractSubscanAZEL implements this function using  ACU
        program offset table (TCS Antenna function loadProgramOffsetTable and trackSetup)

        AbstractSubscanHXP implements this fucntion using ACU hexapod
        program track table (TCS Antenna function loadProgramTrackTableHexapod and TODO??)
        """
        pass

    @abstractmethod
    def wait_for_subscan_completed(self):
        """
        Every subscan must know when it is completed.

        AbstractSubscanAZEL implements this function using  bit status of program offset table.
        If program offset table in ACU is completed, then subscan is completed.

        AbstractSubscanHXP implements this fucntion using bit status of hexapod program track table
        TODO: implement this

        SubscanSleep implements this function using a countdown timer.  Nothing about ACU.
        """
        pass


    def __init__(self, name, subs_num, duration, subs_type="NONE", timeout_offset=15):
        """
        Parameters
        ----------
        name : string
                unique name to find results
        subs_num : integer
                subscan number
        duration : float
                duration of subscan
                unit = seconds
        subs_type : string 4 characters to describe the role of this subscan in the 
                complete scan. For example "NOIS", "CORR", "HOLO", ...
        """
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.logTrace(
            "test logger in class %s, module %s" % (self.__class__.__name__, (__name__))
        )

        self.name = name
        self.subs_num = subs_num
        self.duration = duration
        self.subs_type = subs_type
        self.timeout_offset = timeout_offset

        # Define some common settings.
        # same for all subclass implementations of AbstractBaseSubscan

        # Load this program offset table now.  Overwrite existing table
        self.loadMode = tnrtAntennaMod.newL

        # Name of Antenna component to send commands for pointing and tracking.
        self.antenna_name = "AntennaTNRT"
        self.antenna = None

        # Name of DataAggregator component to access data pipelines.
        self.da_name = "DataAggregator"
        self.da = None

        # Flow control objects
        self.run_finished = threading.Event()

    def tostring(self):
        return "%s" % self.__class__.__name__

    def connect_components(self):
        # Client to access other ACS components
        self.logger.logInfo("Starting PySimpleClient")
        self.sc = PySimpleClient()

        # Connect to DataAggregator for access to data pipelines
        try:
            self.logger.logInfo("Connecting to component %s" % self.da_name)
            self.da = self.sc.getComponent(self.da_name)
        except Exception as e:
            self.logger.error("Cannot get component reference for %s" % self.da_name)

        # Connect to Antenna for access to pointing and tracking
        try:
            self.logger.logInfo("Connecting to component %s" % self.antenna_name)
            self.antenna = self.sc.getComponent(self.antenna_name)
        except Exception as e:
            self.logger.error(
                "Cannot get component reference for %s" % self.antenna_name
            )
    
    def set_trackmode(self, trackmode):
        """
        azel, radec, radecshortcut, or None
        """
        self.trackMode = trackmode

    def run(self, stop_event, pipeline_names):
        """
        Run the subscans.  The sequence of events in this parent class is the standard.
        If a subclass must change the sequence of function calls, implement (override) this
        run() function in the sublcass.

        Subscans are not on a schedule.  They run immediately in sequence after the
        Scan shecule start time has arrived.

        Parameters
        ----------
        stop_event : threading.Event
                Used by the calling function (in AbstractBaseScan) to asynchronously
                stop the current subscan and return the status.
        """
        self.logger.logDebug(
            "Enter %s.run(). Subscan Number = %d, name = %s"
            % (self.__class__.__name__, self.subs_num, self.name)
        )

        # Save the reference to the stop_event in this object.  This allows outside calling
        # function to interrupt the run function in this object.
        self.stop_event = stop_event
        self.pipeline_names = pipeline_names

        # Get access to Antenna, Notification Channel, DataAggregator ...
        self.connect_components()

        try:
            self.da.write_before_subscan()
        except AttributeError as e:
            self.logger.logError(
                "Failed to write_before_subscan. DataAggregator not connected"
            )

        # Generate a temporary program offset table to slew the antenna to the AZ/EL of
        # the first coordinate of the subscan program offset table.  This temporary
        # table has a very long duration of 1 day to guarantee that Antenna waits at the
        # first coordinate until subscan's "real" program offset table is loaded.
        # In reality, the subscan will overwrite this temporary table after a few seconds.
        # when the antenna is prepared at the coordinate and Backends have started recording
        # data.
        start_mjd = Time.now().mjd
        end_mjd = start_mjd + 1
        table = self.generate_subscan_table(
            start_mjd, end_mjd, wait_at_first_coord=True
        )

        # Load the table and immediately start tracking the {time, AZ/RA, EL/DEC} offset coordinates.
        # This function will block until antenna has arrived at the commanded offset tracking coordinate.
        self.start_subscan_tracking(table)
        self.logger.logInfo('Antenna arrived at coordinate')

        # Assume frontends are already configured and ready.

        # Now that antenna is tracking the first coordinate of the program offset table,
        # use the current time and generate the program offset table for this subscan *now*

        # Note: If we use the time "now" exactly in the tracking table data ...
        # By the time we generate the table and send to ACU and command to start tracking
        # (500 ms to 1000 ms depending on ACU time to respond with ACK message and calculations),
        # the timestamp of the first data point in the tracking table is already passed.
        # ACU will continue to set the desired_position for each axis following the absolute time
        # in the tracking table.  But each axis has a maximum acceleration and velocity.  So
        # The physical antenna (and axis encoder_position) cannot catch up to the desired position
        # of the ACU trajectory generator.
        # This situation is a problem for flow control that depends on Antenna arrived at command
        # position.  Therefore, add a small delay to the first timestamp in the tracking table
        # to guarantee that the timestamp is in the future (milliseconds) and not lost in the past.

        delay_until_start = 1.5
        start_mjd = Time.now().mjd + delay_until_start / 86400.0
        end_mjd = start_mjd + self.duration / 86400.0
        table = self.generate_subscan_table(start_mjd, end_mjd)

        t_start = Time(start_mjd, format='mjd')
        self.logger.logInfo('Tracking start time {}'.format(t_start.iso))

        # Load the table and immediately start tracking the {time, AZ/RA, EL/DEC} offset coordinates.
        # This function will block until antenna has arrived at the commanded offset tracking coordinate.
        self.start_subscan_tracking(table)

        for pname in self.pipeline_names:
            try:
                self.logger.logDebug("enable_write {} in subscan".format(pname))
                self.da.enable_write(pname, str(self.subs_num))
            except AttributeError as e:
                self.logger.logError("DataAggregator not connected")

        # TODO: remove this function after modify other pipelines to the same structure as mbfits
        try:
            self.da.clear_subscan_buffers()
            self.logger.logDebug('Clear subscan buffers')
        except AttributeError as e:
            self.logger.logError("Failed to clear subscan buffers. DataAggregator not connected")

        # Set the flow control event state to be not finished and not stopped
        self.run_finished.clear()
        self.stop_event.clear()

        self.logger.logInfo("Block until subscan complete.  Estimate {} sec".format(self.duration))

        subscan_complete_trigger = threading.Thread(target=self.wait_for_subscan_completed)
        subscan_complete_trigger.daemon = True
        subscan_complete_trigger.start()

        self.stop_event.wait()

        # If the subscan run loop exited because an outside function set stop_event,
        # but self.run_finished is still false :
        if not self.run_finished.is_set():
            # If we call cleanup() from this thread, cleanup() will block until complete,
            # then this function returns and gives control back to AbstractBaseScan.run_subscans()
            self.cleanup()

        # Result will be True if subscan finished on schedule
        # Result will be False if subscan was stopped manually
        return self.run_finished.is_set()

    def blocking_countdown(self, duration, period):
        completed = 0
        # Log once before the loop
        self.logger.logDebug(
            "Wait ({} / {}) [s]. {:.0f} % complete".format(
                completed, duration, 100.0 * completed / duration
            )
        )

        while not self.stop_event.wait(timeout=period):
            self.logger.logDebug(
                "Wait ({} / {}) [s]. {:.0f} % complete".format(
                    completed, duration, 100.0 * completed / duration
                )
            )
            completed = completed + period
            if completed >= duration:
                break

    def cleanup(self):
        """
        Disconnect components, etc.
        This function will typically be called from AbstractBaseSubscan.run()
        However, if the operator cancels a subscan, this function will be called
        from Scan component to interrupt the "waiting" loop and cleanup immediately.
        """
        self.logger.logDebug("Enter %s.cleanup()" % self.__class__.__name__)
        try:
            self.da.write_after_subscan(str(self.subs_num))
        except AttributeError as e:
            self.logger.logError("DataAggregator not connected")

        try:
            self.sc.releaseComponent(self.da_name)
            self.da = None
        except Exception as e:
            self.logger.error(
                "Cannot release component reference for %s" % self.da_name
            )

        try:
            self.sc.releaseComponent(self.antenna_name)
            self.antenna = None
        except Exception as e:
            self.logger.error(
                "Cannot release component reference for %s" % self.antenna_name
            )

        if self.sc is not None:
            self.sc.disconnect()
            del self.sc
            self.sc = None

        self.logger.logDebug(
            "Subscan run and data writing is finished.return control to scan"
        )


class AbstractSubscanAZEL(AbstractBaseSubscan):
    """
    AbstractSubscanAZEL is an abstract class that has common functions for subscans
    that use the AZ and EL axes to more through a pattern using ACU program offset tables
    (although the program offset table can be in RaDec coordinate system, the
    actual movement of the antenna is achieve through AZ and EL axes).

    Common functions and variables are implemented in this abstract class and can be
    used from child classes with the super(). functions.

    Python will not allow the user to create an instance of this class because it contains
    virtual functions.
    """

    def start_subscan_tracking(self, table):
        """
        Concrete implementation of the required abstract method start_subscan_tracking.
        Estimate slew time for convenience of user.  Then load the program offset table
        which contains timestamps starting now.  The Antenna immediately starts tracking
        the program offset table.  This function will block until Antenna notifies that
        it is tracking the program coordinate with error < tolerance.

        Parameters
        ----------
        table :
                Program offset table to load immediately.
        """
        try:
            # Calculate how many seconds until antenna can arrive at tracking coordinate

            # Create a timestamp for now. Use in slewtime. Only used for EQ coordinates,
            # but the slewTime function requires this parameter.
            start_mjd = Time.now().mjd

            # Get current AZ and EL from antenna
            az_main = self.antenna.getAzPosition()
            el_main = self.antenna.getElPosition()

            offx0 = table[0].azRa
            offy0 = table[0].elDec

            slewtime = self.antenna.slewTime(
                az_main, el_main, self.trackMode, start_mjd, 2000, offx0, offy0
            )
            self.logger.logDebug(
                "Estimate slewtime to arrive at subscan = %f [sec]" % slewtime
            )
        except Exception as e:
            self.logger.logError(e)

        # Load the program offset table to begin tracking this subscan.
        # Block until arrive at the start of subscan.
        blocking = True
        update_period = 2.0
        self.logger.logDebug("Load program offset table and start tracking")
        self.antenna.trackOffsetPattern(
            self.loadMode,
            self.interpolationMode,
            self.trackMode,
            table,
            blocking,
            update_period,)

    def wait_for_subscan_completed(self):
        try:
            self.logger.logDebug("Try: block until Antenna program offset completed ...")
            self.antenna.blockUntilOffsetTrackingCompleted(self.duration + self.timeout_offset)
            for pname in self.pipeline_names:
                try:
                    self.logger.logDebug("disable_write {} in subscan".format(pname))
                    self.da.disable_write(pname, str(self.subs_num))
                except AttributeError as e:
                    self.logger.logError("DataAggregator not connected")
                    self.logger.logError(e)
        except AttributeError:
            period = 10
            self.logger.logWarning("Antenna not connected. block for {} s with message every {} s".format(
                    self.duration, 
                    period
                )
            )
            self.blocking_countdown(self.duration, period)

        self.logger.logDebug("Blocking function finished. stop_event.is_set(): {}".format(self.stop_event.is_set()))

        if not self.stop_event.is_set():
            self.cleanup_thread = threading.Thread(target=self.cleanup)
            self.cleanup_thread.start()
            self.run_finished.set()
            self.stop_event.set()


class AbstractSubscanHXP(AbstractBaseSubscan):
    """
    AbstractSubscanHXP is an abstract class that has common functions for subscans
    that use the Hexapod axes (x, y, z, tx, ty, tz) to more through a pattern
    using ACU hexapod program track table

    Common functions and variables are implemented in this abstract class and can be
    used from child classes with the super(). functions.

    Python will not allow the user to create an instance of this class because it contains
    virtual functions.
    """

    # Define properties and methods that are abstract, and must be implemented by a subclass
    # before a Subscan object can be created.

    def start_subscan_tracking(self, table):
        """
        Estimate slew time for convenience of user.  Then load the program offset table
        which contains timestamps starting now.  The Antenna immediately starts tracking
        the program offset table.  This function will block until Antenna notifies that
        it is tracking the program coordinate with error < tolerance.

        Parameters
        ----------
        table :
                table to load immediately.
        """
        blocking = True
        update_period = 1.0

        try:
            blocking = True
            update_period = 2.0
            self.logger.logDebug("Load and start tracking HXP proram track table")
            self.antenna.trackPatternHxp(
                self.loadMode, self.interpolationMode, table, blocking, update_period
            )
        except Exception as e:
            self.logger.logError(e)

    def wait_for_subscan_completed(self):
        """
        TODO: implement this function using bit status of hexapod program track table using same logic
        as we use in AbstractSubscanAZEL and program offset table
        """
        pass


class SubscanSleep(AbstractBaseSubscan):
    """
    SubscanSleep doesn't command any system.  Only block this thread
    while other subsystems.  This Subscan
    is useful if we want to record monitor data from Antenna / ACU motor encoders,
    weather station, early commissioning of receviers, etc.
    and still have the structured metadata to create standard data product such as MBFITS.
    """

    def __init__(self, name, subs_num, duration):
        # Use the constructor of parent class to inherit all common variables and update common metadata
        AbstractBaseSubscan.__init__(
            self, name, subs_num, duration, subs_type="NONE", timeout_offset=0
        )

        self.set_trackmode(None)

    # Implement required abstract methods of AbstractBaseSubscan.
    # Since SubscanSleep function start_subscan_tracking does not actually load a program
    # table to the ACU, the parameters for interpolation mode, trackmode, nlines are not used.
    def set_interpolation_mode(self):
        pass

    def set_nlines(self):
        pass

    def generate_subscan_table(self, start_mjd, end_mjd, wait_at_first_coord=False):
        return None

    def start_subscan_tracking(self, table):
        pass

    def wait_for_subscan_completed(self):
        period = 5
        self.logger.logWarning(
            "block for {} s with message every {} s".format(self.duration, period)
        )
        self.blocking_countdown(self.duration, period)

        self.logger.logDebug(
            "Blocking function finished. stop_event.is_set(): {}".format(
                self.stop_event.is_set()
            )
        )

        if not self.stop_event.is_set():
            self.cleanup()
            self.run_finished.set()
            self.stop_event.set()


class SubscanLine(AbstractSubscanAZEL):
    """
    SubscanLine commands the antenna to follow a line in AZ-EL or RADEC coordinates relative to the
    central tracking coordinate (uses Program Offset Table of ACU).
    """

    def __init__(
        self, name, subs_num, duration, xlim, ylim, trackmode, subs_type="NONE"
    ):
        """
        Parameters
        ----------
        name : string
                unique name to find results
        subs_num : integer
                subscan number
        duration : float
                duration of subscan
                unit = seconds
        xlim : tuple (xmin, xmax)
                start and finish AZ coordinates of this line
                unit = arcsec
        ylim : tuple (xmin, xmax)
                start and finish EL coordinates of this line
                unit = arcsec
        trackmode : {tnrtAntennaMod.azel, tnrtAntennaMod.radec, tnrtAntennaMod.radecshortcut}
                tnrtAntennaMod.azel : ACU program offset table pattern in Azimuth / Elevation

                tnrtAntennaMod.radec : ACU program offset table pattern in RA, DEC not crossing NORTH 0 deg AZ

                tnrtAntennaMod.radecshortcut : ACU program offset table pattern RA, Dec.  Allowed to cross NORTH 0 deg AZ.
        subs_type : string 4 characters to describe the role of this subscan in the 
                complete scan. For example "NOIS", "CORR", "HOLO", ...
        """
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.logTrace(
            "test logger in class %s, module %s" % (self.__class__.__name__, (__name__))
        )

        # Use parent class to handle common properties
        AbstractBaseSubscan.__init__(self, name, subs_num, duration, subs_type)

        # Call the required functions that implement abstract methods of AbstractBaseSubscan
        self.set_nlines()
        self.set_interpolation_mode()
        self.set_trackmode(trackmode)

        # Extend parent class with more data for this specific type of subscan
        self.xlim = xlim
        self.ylim = ylim

    # Implement abstract methods that are required by parent class
    def set_nlines(self):
        # Minimum number of lines to generate a table.  ACU interpolation enforces this.
        # Since this offset trajectory is a straight line or single point (line with lenght=0),
        # ACU interpolation can handle the details if we send the minimum number of points.
        self.nlines = 5

    def set_interpolation_mode(self):
        # Use SPLINE interpolation to make a smooth trajectory from start to finish
        self.interpolationMode = tnrtAntennaMod.SPLINE

    def generate_subscan_table(self, start_mjd, end_mjd, wait_at_first_coord=False):
        """
        Generates the program offset table from scheduling / flow control times and
        xlim, ylim contained in this object.
        Parameters
        ----------
                wait_at_first_coord : {`True`, `False`}. default = `False`
                        `True` : The tracking table will be generated with full list of timestamps but repeat
                        the first coordinate for the entire table.  This is useful to prepare the Antenna at
                        the correct position before the actual start time of data recording and traverse pattern

                        `False` : The table is generate for the full ist of timestamps and position coordinates
        """
        # Put the start and end times in a tuple to make remaining numpy code structure consitent
        tlim = (start_mjd, end_mjd)

        # Create lists of start and end values to prepare to generate 2D array from numpy linspace
        # Convert arcsed to degrees
        start_values = [tlim[0], self.xlim[0] / 3600.0, self.ylim[0] / 3600.0]

        if wait_at_first_coord is False:
            # Create the end coordinate for the actual end of the subscan
            end_values = [tlim[1], self.xlim[1] / 3600.0, self.ylim[1] / 3600.0]
        else:
            # Create the end coordinates same as the start coordinate to hold tracking in place
            # while waiting for start time
            end_values = [tlim[1], self.xlim[0] / 3600.0, self.ylim[0] / 3600.0]

        # Generate a 2D numpy array for the program offset table.
        # Conveniently, Numpy generates this array in "row-major" order
        # so we don't require a transpose before sending to the ACU.
        table_np = np.linspace(start_values, end_values, self.nlines)
        self.logger.logTrace("table_np {}".format(table_np))

        table_idl = []

        for row in table_np:
            table_idl.append(tnrtAntennaMod.timeAzRaElDecStruct(row[0], row[1], row[2]))

        self.logger.logDebug("length of table %d" % len(table_idl))
        self.logger.logDebug("table_idl {}".format(table_idl))

        return table_idl


class SubscanSingle(SubscanLine):
    """
    SubscanLineHO commands the antenna to stay on a single point RA-DEC coordinates
    relative to the central tracking coordinate (uses Program Offset Table of ACU).
    A Single point subscan is the same as a line with xmin==xmax, ymin==ymax.
    So we can reuse the parent class SubscanLineEQ and set the xlim and ylim to be a point
    located at appropriate offset.
    """

    def __init__(
        self, name, subs_num, duration, offset_x, offset_y, trackmode, subs_type="NONE"
    ):
        """
        Parameters
        ----------
        name : string
                unique name to find results
        subs_num : integer
                subscan number
        duration : float
                duration of subscan
                unit = seconds
        offset_x : float
                unit = arcsec
                offset from central program tracking coordinate RA
        offset_y : float
                offset from central program tracking coordinate DEC
                unit = arcsec
        trackmode : {tnrtAntennaMod.azel, tnrtAntennaMod.radec, tnrtAntennaMod.radecshortcut}
                tnrtAntennaMod.azel : ACU program offset table pattern in Azimuth / Elevation

                tnrtAntennaMod.radec : ACU program offset table pattern in RA, DEC not crossing NORTH 0 deg AZ

                tnrtAntennaMod.radecshortcut : ACU program offset table pattern RA, Dec.  Allowed to cross NORTH 0 deg AZ.
        subs_type : string 4 characters to describe the role of this subscan in the 
                complete scan. For example "NOIS", "CORR", "HOLO", ...
        """
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.logTrace(
            "test logger in class %s, module %s" % (self.__class__.__name__, (__name__))
        )

        # Set minimum and maximum coordinate of the line to be the same exact point.
        # So, SubscanLineHO will generate a line of length=0 at location offset_az, offset_el.
        xlim = (offset_x, offset_x)
        ylim = (offset_y, offset_y)

        # Use the constructor of parent class to inherit all common variables and update common metadata
        SubscanLine.__init__(
            self, name, subs_num, duration, xlim, ylim, trackmode, subs_type
        )

        # Since every point in the tracking table is the same value, we dont' really care
        # which interpolation method is used becuase they should all give the same
        # result -- fixed on one point.  So, use the interpolation defined in
        # parent class SubscanLine.


class SubscanFocus(AbstractSubscanHXP):
    """
    SubscanLine commands the antenna to follow a line in AZ-EL or RADEC coordinates relative to the
    central tracking coordinate (uses Program Offset Table of ACU).
    """

    def __init__(
        self,
        name,
        subs_num,
        duration,
        hxp_xlim,
        hxp_ylim,
        hxp_zlim,
        hxp_txlim,
        hxp_tylim,
        hxp_tzlim,
        subs_type="NONE",
    ):
        """
        Parameters
        ----------
        name : string
                unique name to find results
        subs_num : integer
                subscan number
        duration : float
                duration of subscan
                unit = seconds
        hxp_xlim : tuple (xmin, xmax)
                start and finish x coordinates of HXP focus
                unit = mm
        hxp_ylim : tuple (ymin, ymax)
                start and finish y coordinates of HXP focus
                unit = mm
        hxp_zlim : tuple (zmin, zmax)
                start and finish x coordinates of HXP focus
                unit = mm
        hxp_txlim : tuple (txmin, txmax)
                start and finish tx coordinates of HXP focus
                unit = deg
        hxp_tylim : tuple (tymin, tymax)
                start and finish ty coordinates of HXP focus
                unit = deg
        hxp_tzlim : tuple (tzmin, tzmax)
                start and finish t coordinates of HXP focus
                unit = deg
        """
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.logTrace(
            "test logger in class %s, module %s" % (self.__class__.__name__, (__name__))
        )

        # Use parent class to handle common properties
        AbstractBaseSubscan.__init__(self, name, subs_num, duration, subs_type)

        # Call the required functions that implement abstract methods of AbstractBaseSubscan
        self.set_nlines()
        self.set_interpolation_mode()
        self.set_trackmode(None)

        # Extend parent class with more data for this specific type of subscan
        self.hxp_xlim = hxp_xlim
        self.hxp_ylim = hxp_ylim
        self.hxp_zlim = hxp_zlim
        self.hxp_txlim = hxp_txlim
        self.hxp_tylim = hxp_tylim
        self.hxp_tzlim = hxp_tzlim

    # Implement abstract methods that are required by parent class
    def set_nlines(self):
        # Minimum number of lines to generate a table.  ACU interpolation enforces this.
        # Since this offset trajectory is a straight line or single point (line with lenght=0),
        # ACU interpolation can handle the details if we send the minimum number of points.
        self.nlines = 5

    def set_interpolation_mode(self):
        # Use SPLINE interpolation to make a smooth trajectory from start to finish
        self.interpolationMode = tnrtAntennaMod.SPLINE

    def generate_subscan_table(self, start_mjd, end_mjd, wait_at_first_coord=False):
        """
        Generates the hexagon program track  table from scheduling / flow control times and
        hxp_xlim, hxp_ylim, hxp_zlim, hxp_txlim, hxp_tylim, hxp_tzlim, contained in this object.
        Parameters
        ----------
                wait_at_first_coord : {`True`, `False`}. default = `False`
                        `True` : The tracking table will be generated with full list of timestamps but repeat
                        the first coordinate for the entire table.  This is useful to prepare the Antenna at
                        the correct position before the actual start time of data recording and traverse pattern

                        `False` : The table is generate for the full ist of timestamps and position coordinates
        """
        # Put the start and end times in a tuple to make remaining numpy code structure consitent
        tlim = (start_mjd, end_mjd)

        # Create lists of start and end values to prepare to generate 2D array from numpy linspace
        start_values = [
            tlim[0],
            self.hxp_xlim[0],
            self.hxp_ylim[0],
            self.hxp_zlim[0],
            self.hxp_txlim[0],
            self.hxp_tylim[0],
            self.hxp_tzlim[0],
        ]

        if wait_at_first_coord is False:
            # Create the end coordinate for the actual end of the subscan
            end_values = [
                tlim[1],
                self.hxp_xlim[1],
                self.hxp_ylim[1],
                self.hxp_zlim[1],
                self.hxp_txlim[1],
                self.hxp_tylim[1],
                self.hxp_tzlim[1],
            ]
        else:
            # Create the end coordinates same as the start coordinate to hold tracking in place
            # while waiting for start time
            end_values = [
                tlim[1],
                self.hxp_xlim[0],
                self.hxp_ylim[0],
                self.hxp_zlim[0],
                self.hxp_txlim[0],
                self.hxp_tylim[0],
                self.hxp_tzlim[0],
            ]

        # Generate a 2D numpy array for the program offset table.
        # Conveniently, Numpy generates this array in "row-major" order
        # so we don't require a transpose before sending to the ACU.
        table_np = np.linspace(start_values, end_values, self.nlines)
        self.logger.logTrace("table_np {}".format(table_np))

        table_idl = []

        for row in table_np:
            table_idl.append(
                tnrtAntennaMod.timeXYZTXTYTZStruct(
                    row[0], row[1], row[2], row[3], row[4], row[5], row[6]
                )
            )

        self.logger.logDebug("length of table %d" % len(table_idl))
        self.logger.logTrace("table_idl {}".format(table_idl))

        return table_idl

    def __repr__(self):
        return "{}: xlim{}, ylim{}. zlim{}, txlim{}, tylim{}, tzlim{}".format(
            self.__class__,
            self.hxp_xlim,
            self.hxp_ylim,
            self.hxp_zlim,
            self.hxp_txlim,
            self.hxp_tylim,
            self.hxp_tzlim,
        )


# Main defined only for testing with data in native format
# Not passing data throuth IDL type translations
if __name__ == "__main__":
    pass
