# Project: TNRT - Thai National Radio Telescope
# Company: NARIT - National Astronomical Research Institute of Thailand
# Author: Spiro Sarris
# Date: 2020.11.20

# Python packages (built-in and PyPi)
import sys

if sys.version_info[0] == 2:
    from Queue import PriorityQueue
else:
    from queue import PriorityQueue

import json
from astropy.time import Time
import logging
import numpy as np
import threading
import requests
from requests.exceptions import HTTPError, ConnectionError
import ScanMod
import ScanErrorImpl
import ScanError
import os

# ACS packages
from Acspy.Servants.ACSComponent import ACSComponent
from Acspy.Servants.ContainerServices import ContainerServices
from Acspy.Servants.ComponentLifecycle import ComponentLifecycle
from Acspy.Servants.ComponentLifecycle import ComponentLifecycleException
from Acspy.Nc.Supplier import Supplier

from AcsutilPy.ACSPorts  import getIP

# TCS packages
import ScanMod
import ScanMod__POA
import ScanErrorImpl
import ScanError
import ScanDefaults
from scantype_models import ScanModelFactory

from CentralDB import CentralDB
import cdbConstants


class Scan(ScanMod__POA.Scan, ACSComponent, ContainerServices, ComponentLifecycle):
    """
    Docstring for Scan class
    """

    # Constructor
    def __init__(self):
        ACSComponent.__init__(self)
        ContainerServices.__init__(self)
        self.logger = self.getLogger()

        self.nc_supplier = None
        self.scan_queue = None
        self.current_scan = None

        self.ip = getIP()

        # Flow control events
        self.queue_stopped = threading.Event()
        self.current_scan_stopped = threading.Event()

        # Dictionary of results
        self.results_queue = {}

        # Allow North crossing Azimuth to cross 0 degrees when generating tracking tables.
        # For EQ and TLE tracking, ACU should handle this directly.  We have to use this parameter
        # when we generate AZ / EL tracking table for solar system body from JPLEphem.
        self.northCrossing = True

        # count of retry before fail if tracknig cannot arrive at command AZ/EL (windy?)
        self.maxTrackingErrors = 5

        # If False, ACU calculates the AZ/EL from the RA/DEC tracking table or TLE
        self.useHorizontalTables = False

        self.cdb = CentralDB()

    # Destructor
    def __del__(self):
        pass

    # ------------------------------------------------------------------------------------
    # LifeCycle functions called by ACS system when we activate and deactivate components.
    # Inherited from headers in Acspy.Servants.ComponentLifecycle.

    def initialize(self):
        self.logger.logDebug("%s.initialize()" % self.__class__.__name__)

        self.logger.logInfo(
            "Creating Notification Channel Supplier %s " % ScanMod.CHANNEL_NAME_METADATA
        )
        try:
            self.nc_supplier = Supplier(ScanMod.CHANNEL_NAME_METADATA)
        except:
            self.nc_supplier = None
            self.logger.logError(
                "Failed to initialize NC Supplier: %s " % ScanMod.CHANNEL_NAME_METADATA
            )

        # Create a new queue to add Scan objects
        self.scan_queue = PriorityQueue()

        # Initialize scan number to 0.  Increment we add a new scan.
        # Survivives for the lifecycle of the Scan component.
        # When we connect nash, it will do a time sync scan to consume scan number 0.
        # Then scans after will start with 1.
        self.scan_id_counter = 1
        self.scan_id_lastrun = 0

    def execute(self):
        self.logger.debug("execute ()")

    def aboutToAbort(self):
        self.logger.debug("aboutToAbort ()")

    def cleanUp(self):
        self.logger.logDebug("%s.cleanUp()" % self.__class__.__name__)
        self.nc_supplier.disconnect()
        self.cdb = None

    # ------------------------------------------------------------------------------------
    # Functions defined in IDL to interface from outside this class

    def get_ip(self):
        return self.ip

    def qsize(self):
        """
        Return size of queue (quantity of Scan items)
        """
        return self.scan_queue.qsize()

    def run_queue(self, blocking=True):
        """
        Process all items in the queue
        Return results after queue is finished.  If run_queue() was executed as non-blocking,
        This function will immediately return an empty dictionary.  However, the user wants the results
        after the non-blocking queue is stopped by user function.
        Therefore, stop_queue() also returns the results.
        """
        try:
            # Set the flow control event that queue is not stopped. should run
            self.queue_stopped.clear()

            # clear the results before starting queue
            self.results_queue = {}

            # Process all scans in the queue
            while (not self.scan_queue.empty()) and (not self.queue_stopped.is_set()):
                self.logger.logInfo("Scan queue size = %d" % self.qsize())
                # Get the next item from the queue.  Since this is a priority queue,
                # the item in the queue is a tuple of the form (priority, Scan object).
                # After we pull from the queue, the priority number is not imporant.
                # Therefore, we can get only the Scan object from tuple index [1]
                self.current_scan = self.scan_queue.get()[1]

                # Set flow control event that current scan is not stopped.
                self.current_scan_stopped.clear()

                # Call run() in this thread to block until run() is complete.
                # The Scan implemenation class (ex. ScanFivePoint) will create
                # Worker Threads for Antenna, Backend, Data Pipelines.
                # The flow control event self.current_scan_stopped allows the
                # Scan controller ACS component to interrupt and stop the current scan
                self.logger.logDebug(self.current_scan)
                results_scan = self.current_scan.run(self.current_scan_stopped)
                
                # Add the results of this current scan to the dictionary for the the entire queue
                scan_number = self.cdb.get(cdbConstants.SCANNUM)
                self.results_queue[scan_number] = results_scan
                self.scan_id_lastrun = int(scan_number)

            # After last scan in the queue run() is complete, clear the current scan
            self.current_scan = None

            # Translate the Python dictionary of results to a JSON string.
            results_json = json.dumps(self.results_queue)
            self.logger.logInfo(results_json)
            return results_json

        except Exception as e:
            self.logger.logError("Catch: {}".format(e))
            # Clear the queue if any scans remain.  We should start a new queue after such an error.
            nitems_deleted = self.clearQueue()
            self.logger.logError("Deleted {} items from the Queue".format(nitems_deleted))

            self.logger.logError("Raise new ACS CORBA Exception with stack trace details")
            acs_corba_exception = ScanErrorImpl.GeneralErrorExImpl(exception=e)
            raise acs_corba_exception 


    def stop_queue(self):
        """
        Stop running the queue
        Cancel the current scan and delete it from the queue.  Don't delete the
        remaining scans in the queue.

        If run_queue() was executed as non-blocking,
        it would immediately return an empty dictionary of results.
        However, the user calls this function stop_queue,
        the user wants the results at the time of stop_queue()
        Therefore, stop_queue() also returns the results.
        """
        self.logger.logDebug("Set event to exit the queue run loop")
        self.queue_stopped.set()

        self.logger.logDebug(
            "Set event to stop the current scan now if it is already running"
        )
        self.current_scan_stopped.set()

        self.logger.logDebug(
            "Cancel the scheduler for current scan in the future and set event subscans_finished"
        )
        if self.current_scan is not None:
            self.current_scan.stop()

        # Translate the Python dictionary of results to a JSON string.
        results_json = json.dumps(self.results_queue)
        self.logger.logInfo(results_json)
        return results_json

    def get_metadata(self):
        """
        # If a scan is already read from the queue into the member self.current_scan,
        return the metadata of the current scan.  Else, return a structure of default values for testing
        the data structure.  This can happen if the queue is empty or if the queue has Scans, but
        run_queue() has not yet been called to start processing Scans.
        """
        try:
            self.logger.logDebug("get metadata from current_scan")
            return self.current_scan.metadata
        except AttributeError:
            self.logger.logDebug(
                "current_scan is None, return data structure with default values"
            )
            return ScanDefaults.metadata

    def get_scan_mbfits_header(self):
        try:
            return self.current_scan.metadata.SCAN_MBFITS_HEADER
            self.logger.logDebug("get metadata from current_scan")
        except Exception:
            self.logger.logDebug(
                "Exception - return data structure with default values"
            )
            return ScanDefaults.SCAN_MBFITS_HEADER

    def get_scan_mbfits_columns(self):
        try:
            self.logger.logDebug("get metadata from current_scan")
            return self.current_scan.metadata.SCAN_MBFITS_COLUMNS
        except Exception:
            self.logger.logDebug(
                "Exception - return data structure with default values"
            )
            return ScanDefaults.SCAN_MBFITS_COLUMNS

    def get_febepar_mbfits_header(self):
        try:
            self.logger.logDebug("get metadata from current_scan")
            return self.current_scan.metadata.FEBEPAR_MBFITS_HEADER
        except Exception:
            self.logger.logDebug(
                "Exception - return data structure with default values"
            )
            return ScanDefaults.FEBEPAR_MBFITS_HEADER

    def get_arraydata_mbfits_header(self):
        try:
            self.logger.logDebug("get metadata from current_scan")
            return self.current_scan.metadata.ARRAYDATA_MBFITS_HEADER
        except Exception:
            self.logger.logDebug(
                "Exception - return data structure with default values"
            )
            return ScanDefaults.ARRAYDATA_MBFITS_HEADER

    def get_datapar_mbfits_header(self):
        try:
            self.logger.logDebug("get metadata from current_scan")
            return self.current_scan.metadata.DATAPAR_MBFITS_HEADER
        except Exception:
            self.logger.logDebug(
                "Exception - return data structure with default values"
            )
            return ScanDefaults.DATAPAR_MBFITS_HEADER

    def addScan(
        self,
        schedule_params,
        scantype_params,
        tracking_params,
        frontend_params,
        backend_params,
        data_params,
    ):
        # Update the scan number if the user did not include a scan number override
        if schedule_params.scan_id == 0:
            schedule_params.scan_id = self.scan_id_counter
            self.scan_id_counter += 1

        # Create an object of the scan and add to the queue
        s = ScanModelFactory.from_params(
            schedule_params,
            scantype_params,
            tracking_params,
            frontend_params,
            backend_params,
            data_params,
        )
        self.put(s)

    def clearQueue(self):
        """
        Delete everything in the queue.
        """
        nitems = self.scan_queue.qsize()
        del self.scan_queue
        # Create a new queue
        self.scan_queue = PriorityQueue()
        return nitems

    def get_scan_id_counter(self):
        return self.scan_id_counter

    def get_scan_id_lastrun(self):
        return self.scan_id_lastrun

    def is_current_scan_canceled(self):
        if self.current_scan_stopped.is_set():
            return True
        else:
            return False
        
    def is_trackcoord_arrived(self):
        try:
            if self.current_scan.trackcoord.antenna_arrived.is_set():
                return True
            else:
                return False
        except AttributeError:
            self.logger.logWarning("current scan = None. cannot check trackcoord arrived")
            return False

    def setTrajectoryParams(self, trajectoryStr):
        """Sets some variable that we use to generate tracking tables for solar system bodies. and tracking
        retries. horizontal tables using a ScanMod.trajectoryStructure composed of two booleans:
        trajectoryStr.northCrossing True = Antenna can cross the North. False = Az is restricted to (0,360)
        hojectoryStr.useHorizontalTables True = Use horizontal tables. False = Use equatorial tables.
        """
        self.northCrossing = trajectoryStr.northCrossing
        self.useHorizontalTables = trajectoryStr.useHorizontalTables
        self.maxTrackingErrors = trajectoryStr.maxTrackingErrors

    def getTrajectoryParams(self):
        """return a trajectory structure with information on  northCrossing and
        useHorizontalTables variables.
        """
        return ScanMod.trajectoryStructure(
            self.northCrossing, self.useHorizontalTables, self.maxTrackingErrors
        )

    def get_TLE(self, id):
        cacheFilePath = os.getenv("HOME") + "/cache/TLE/{}.tle".format(id)
        try:
            headers = {
                "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36"
            }
            TLEResponse = requests.get(
                "https://tle.ivanstanojevic.me/api/tle/{}".format(id), headers=headers
            )
            TLEResponse.raise_for_status()
            TLEData = TLEResponse.json()
            formattedTLEData = ScanMod.TLEData(
                name=TLEData["name"],
                line1=TLEData["line1"],
                line2=TLEData["line2"],
                date=TLEData["date"],
                message="",
            )
            os.makedirs(os.path.dirname(cacheFilePath), exist_ok=True)
            file = open(cacheFilePath, "w")
            file.writelines("{}\n".format(TLEData["name"]))
            file.writelines("{}\n".format(TLEData["line1"]))
            file.writelines("{}\n".format(TLEData["line2"]))
            file.writelines("{}\n".format(TLEData["date"]))
            file.close()
            return formattedTLEData
        except HTTPError as http_err:
            self.logger.logError("HTTP error occurred: {}".format(http_err))
            raise ScanErrorImpl.httpResponseExImpl()
        except ConnectionError as connection_err:
            try:
                self.logger.logError(
                    "No connection error occurred: {}".format(connection_err)
                )
                file = open(cacheFilePath, "r")
                lines = file.readlines()
                formattedTLEData = ScanMod.TLEData(
                    name=lines[0],
                    line1=lines[1],
                    line2=lines[2],
                    date=lines[3],
                    message="This data is from cache since the TLE source could not be reached due to internet connection problem",
                )
                return formattedTLEData
            except (FileNotFoundError, FileExistsError) as file_err:
                self.logger.logError(
                    "File not found error occurred {}:".format(file_err)
                )
                raise (
                    ScanErrorImpl.noInternetConnectionExImpl(exception=connection_err)
                )
        except Exception as err:
            self.logger.logError("Other error: {}".format(err))
            raise

    # ------------------------------------------------------------------------------------
    # Internal functions not exposed in IDL (but still available from Python module)

    def refresh_all_priority(self):
        """
        Use a priority queue and organize by countdown time until start.
        Priority lower number is greater priority.  So, use countdown time until
        start scan to assign priority (scan start time - current time).
        Each time a new scan is added, traverse the queue and recalculate
        Countdown time.  Insert the new scan using the countdown time as priority.
        Remember to test if the duration of a scan overlaps another scan already
        in the queue.
        """
        # Create a temporary PrirotyQueue to rebuild the schedule
        temp_queue = PriorityQueue()
        while not self.scan_queue.empty():
            # Get the next item from the queue. Keep only the scan object, and discard
            # the priority becuase we will caluclate it again.
            scan = self.scan_queue.get()[1]
            priority = self.calculate_priority(scan)
            temp_queue.put((priority, scan))

        # Now that the temp_queue has all updated priority from current time,
        # keep this queue.
        self.scan_queue = temp_queue

    def calculate_priority(self, new_scan):
        """
        calculate_priority(new_scan) calculates the "countdown" time until
        new_scan start.  We use countdown time  priority parameter for
        PriorityQueue.  Lower countdown number is high priority and will run
        sooner than high countdown number (far in the future).  priority does not
        need to be an integer.  So, we can use MJD countdown directly as a priority.
        If the operator does not choose an MJD start time, the default value is zero.
        Therefore the countdown will be a large negative number.  If we add to the
        queue directly with a large negative number priority, scans that don't have a
        scheduled start time will be added to the head of the queue instead of tail
        (queue in reverse order).  Use numpy.abs to convert to positive numbers.  Then,
        scans that have MJD=0 for start time will be added to the end of the queue after
        all scans that have a valid scheduled start time.  For scans that have a valid
        schedule start time in the future, np.abs() function has no effect because the countdown
        is already a small positive number.
        """
        priority = np.abs(new_scan.schedule_params.start_mjd - Time.now().mjd)
        return priority

    def put(self, new_scan):
        """
        Add new scan to the queue in the correct sequence
        (priority calculated from start time).
        This function is not included in the IDL becuase it expects an input
        parameter of type AbstractBaseScan (or child class), not an IDL struct.
        """
        self.refresh_all_priority()
        priority = self.calculate_priority(new_scan)
        self.scan_queue.put((priority, new_scan))


# Main defined only for testing with data in native format
# Not passing data throuth IDL type translations
if __name__ == "__main__":
    s = Scan()
