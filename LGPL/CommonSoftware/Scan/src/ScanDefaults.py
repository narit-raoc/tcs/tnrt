import ScanMod

d_string = "U"
d_double = float(0.0)
d_float = float(0.0)
d_long = int(1)  # Sometimes used to create array dimensions, therefore must be > 0
d_boolean = False
d_date = "2020-01-01T00:00:00.000"

SUBS_NUM = {
    "CAL_BEFORE": 100001,
    "CAL_AFTER": 100002,
    "CROSS": {
        "HP": 0, # horizontal positive
        "HN": 1, # horizontal negative
        "VP": 2, # vertical positive
        "VN": 3, # vertical negative
    }
}

PRIMARY_MBFITS_HEADER = ScanMod.PRIMARY_MBFITS_HEADER_TYPE(
    0,  # short NAXIS;		// type="I" value=0
    True,  # boolean SIMPLE;	// type="L" value=True
    32,  # short BITPIX;		// type="I" value=32
    True,  # boolean EXTEND;	// type="L" value=True
    "TNRO",  # string TELESCOP;	// Telescope Name type="20A"
    "NARIT",  # string ORIGIN;	// Organisation or Institution type="20A"
    "TCS2020.05",  # string CREATOR;	// Software (including version)" type="20A"
    "1.67",  # string MBFTSVER;	// MBFITS version" type="10A"
    "astropy.io.fits",
)  # string COMMENT;	// type="20A"

# Set default values for all of the data types required by MBFITS interface.
# Do this once now, then we can update actual vaules at runtime in scans.
SCAN_MBFITS_HEADER = ScanMod.SCAN_MBFITS_HEADER_TYPE(
    "SCAN-MBFITS",  # string EXTNAME;	// name of this table "SCAN-MBFITS"
    "TNRO",  # string TELESCOP; 	// Telescope Name, type="20A"
    d_double,  # double SITELONG; 	// Observatory longitude, unit="deg"
    d_double,  # double SITELAT; 	// Observatory latitude, unit="deg"
    d_float,  # float SITEELEV;	// Observatory elevation, unit="m"
    40,  # float DIAMETER;	// Dish diameter
    d_string,  # string PROJID;	// Project ID" type="20A"
    d_string,  # string OBSID;		// Observer and operator initials type="24A"
    d_long,  # long SCANNUM;		// Scan number
    d_string,  # string TIMESYS;	// Time system (TAI, UTC, UT1, etc ...) for MJD and DATE-OBS type="04A"
    d_date,  # string DATE_OBS;	// Scan start in TIMESYS system " type="23A"  format ISO-8601: CCYY-MM-DDThh:mm:ss.ssss
    59800,  # double MJD;		// Scan date/time (Modified Julian Date) in TIMESYS system unit="day"
    2000.0,  # double EQUOBS;	// (optional) Equinox of observation unit="year"
    d_double,  # double LST;		// Local apparent sidereal time (scan start) unit="s"
    d_long,  # long NOBS;		// (DEPRECATED - keep for back compatibility) replaced by NSUBS
    d_long,  # long NSUBS;		// Number of subscans in this scan (replaces NOBS after v1.5)
    d_double,  # double UTC2UT1; 	// UT1-UTC time translation unit="s"
    d_double,  # double TAI2UTC;	// UTC-TAI time translation unit="s"
    d_double,  # double ETUTC;		// Ephemeris Time - UTC time translation unit="s"
    d_double,  # double GPSTAI;	// GPS time - TAI translation unit="s"
    d_string,  # string WCSNAME;	// Human-readable description of frame type="20A". example: "Horizontal Coordinates"
    d_string,  # string RADESYS;	// Additional system definition for ecliptic/equatorial coords" type="08A"
    2000.0,  # float EQUINOX; 	// Equinox unit="Julian yrs"
    d_string,  # string BLNGTYPE;	// Basis system type (longitude) - XLON-SFL (where x takes appropriate values) type="8A"
    d_string,  # string CTYPE1;	// (DEPRECATED - keep for back compatibility) replaced by BLNGTYPE
    d_string,  # string BLATTYPE;	// Basis system type (latitude) - XLAT-SFL (where x takes appropriate values) type="8A"
    d_string,  # string CTYPE2;	// (DEPRECATED - keep for back compatibility) replaced by BLATTYPE
    d_double,  # double NLNGZERO;	// Native frame zero in basis system (longitude)
    d_double,  # double CRVAL1;	// (DEPRECATED - keep for back compatibility) replaced by NLNGZERO
    d_double,  # double NLATZERO; 	// Native frame zero in basis system (latitude)
    d_double,  # double CRVAL2;	// (DEPRECATED - keep for back compatibility) replaced by NLATZERO
    d_string,  # string NLNGTYPE; 	// Native system type (longitude) - XLON-SFL (where x takes appropriate values) type="8A"
    d_string,  # string CTYPE1N;	// (DEPRECATED - keep for back compatibility) replaced by NLNGTYPE
    d_string,  # string NLATTYPE; 	// Native system type (latitude) - XLAT-SFL (where x takes appropriate values) type="8A"
    d_string,  # string CTYPE2N;	// (DEPRECATED - keep for back compatibility) replaced by NLATTYPE
    d_double,  # double LONPOLE;	// Native longitude of celestial pole: range -180 to +180 deg unit="deg"
    d_double,  # double LATPOLE;	// Basis latitude of native pole unit="deg"
    d_string,  # string X_OBJECT;	// Source name type="20A"
    d_double,  # double BLONGOBJ;	// Source longitude in basis frame unit="deg"
    d_double,  # double BLATOBJ;	// Source latitude in basis frame unit="deg"
    d_double,  # double LONGOBJ;	// Source longitude in user native frame unit="deg"
    d_double,  # double LATOBJ;	// Source latitude in user native frame unit="deg"
    d_double,  # double PATLONG;	// Pattern longitude offset in user native frame
    d_double,  # double PATLAT;	// Pattern latitude offset in user native frame
    d_string,  # string CALCODE;	// Calibrator Code type="04A"
    d_boolean,  # boolean MOVEFRAM;	// True if tracking a moving frame. In the case of moving objects, the orbital elements are also stored. The abbeviations in description match JPL Horizons.
    d_double,  # double PERIDATE;	// TP, Full Julian date of perihelion unit="Julian days"
    d_double,  # double PERIDIST; 	// QR, perihelion distance unit="AU"
    d_double,  # double LONGASC;	// OM, Longitude of ascending node unit="deg"
    d_double,  # double OMEGA;		// W, Angle from asc. node to perihelion" unit="deg"
    d_double,  # double INCLINAT;	// IN, Inclination unit="deg"
    d_double,  # double ECCENTR;	// EC, Eccentricity
    d_double,  # double ORBEPOCH;	// EPOCH, Epoch of orbital elements unit="Julian days"
    d_double,  # double ORBEQNOX;	// Elements equinox: J2000.0 or B1950.0 unit="years"
    d_double,  # double DISTANCE;	// Geocentric Distance unit="AU"
    d_string,  # string SCANTYPE;	// Scan astronomical type type="20A"
    d_string,  # string SCANMODE;	// Mapping mode type="20A"
    d_string,  # string SCANGEOM;	// Scan geometry type="20A"
    d_string,  # string SCANDIR;	// "scan direction" type="04A"
    d_long,  # long SCANLINE;	// "number of lines in a scan"
    d_long,  # long SCANRPTS;	// "number of repeats of each scan line"
    d_double,  # double SCANLEN;	// "(OTF/RASTER) line length"
    d_double,  # double SCANXVEL;	// "(OTF) tracking rate along line."
    d_double,  # double SCANTIME;	// "(OTF) time for one line"
    d_double,  # double SCANXSPC;	// "(RASTER) step along line between samples"
    d_double,  # double SCANYSPC;	// "(OTF/RASTER) step between scan/raster lines"
    d_double,  # double SCANSKEW;	// "(OTF/RASTER) offset in scan direction between lines"
    d_double,  # double SCANROT;	// "(OTF/RASTER) rotation of user frame meridian wrt basis meridian. measure positive E of N."
    d_double,  # double SCANPAR1;	// "spare scan parameter"
    d_double,  # double SCANPAR2;	// "another spare scan parameter"
    d_string,  # string CROCYCLE;	// "CAL/REF/ON loop string" type="20A"
    d_boolean,  # boolean ZIGZAG;	// "(OTF/RASTER) Scan in zigzag?"
    d_double,  # double TRANDIST;	// "(HOLO) Holography transmitter distance" unit="m"
    d_double,  # double TRANFREQ;	// "(HOLO) Holography transmitter frequency" unit="Hz"
    d_double,  # double TRANFOCU; 	// "(HOLO) Holography transmitter offset from prime focus" unit="deg"
    d_boolean,  # boolean WOBUSED;	// "Wobbler used?"
    d_double,  # double WOBTHROW;	// "Wobbler throw" unit="deg"
    d_string,  # string WOBDIR;	// "Wobbler throw direction" type="04A"
    d_float,  # float WOBCYCLE; 	// "Wobbler period" unit="s"
    d_string,  # string WOBMODE;	// "Wobbler mode (SQUARE/TRIANGULAR)" type="20A"
    d_string,  # string WOBPAT;	// "Wobbler Pattern (e.g. NEG, POS, SYM)"
    d_string,  # string PHASE1;	// "Phase 1 description" type="20A"
    d_string,  # string PHASE2;	// "Phase 2 description" type="20A"
    d_string,  # string PHASE3;	// "Phase 3 description" type="20A"
    d_string,  # string PHASE4;	// "Phase 4 description" type="20A"
    d_string,  # string PHASE5;	// "Phase 5 description" type="20A"
    d_string,  # string PHASE6;	// "Phase 6 description" type="20A"
    d_string,  # string PHASE7;	// "Phase 7 description" type="20A"
    d_string,  # string PHASE8;	// "Phase 8 description" type="20A"
    d_long,  # long NFEBE;		// "Number of FEBEs"
    d_float,  # float PDELTAIA;	// "Accumulated user ptg correction IA" unit="deg"
    d_float,  # float PDELTACA;	// "Accumulated user ptg correction CA" unit="deg"
    d_float,  # float PDELTAIE;	// "Accumulated user ptg correction IE" unit="deg"
    d_float,  # float FDELTAX;	// Accumulated user focus correction X unit="m"
    d_float,  # float FDELTAY;	// Accumulated user focus correction Y unit="m"
    d_float,  # float FDELTAZ;	// Accumulated user focus correction Z unit="m"
    d_float,  # float FDELTAXT;	// Accumulated user focus correction XTilt unit="deg"
    d_float,  # float FDELTAYT;	// Accumulated user focus correction YTilt unit="deg"
    d_float,  # float FDELTAZT;	// Accumulated user focus correction ZTilt unit="deg"
    d_float,  # float FDELTAIA;	// "Accumulated ptg corr to IA due to focus" unit="deg"
    d_float,  # float FDELTACA;	// "Accumulated ptg corr to CA due to focus" unit="deg"
    d_float,  # float FDELTAIE;	// "Accumulated ptg corr to IE due to focus" unit="deg"
    d_float,  # float IA;			// "Pointing Coefficient (-P1)"  unit="deg"
    d_float,  # float IE;			// "Pointing Coefficient (P7)" unit="deg"
    d_float,  # float HASA;		// "Pointing Coefficient" unit="deg"
    d_float,  # float HACA;		// "Pointing Coefficient" unit="deg"
    d_float,  # float HESE;		// "Pointing Coefficient (P9 was ZFLX)" unit="deg"
    d_float,  # float HECE;		// "Pointing Coefficient (P8; was ECEC)" unit="deg"
    d_float,  # float HESA;		// "Pointing Coefficient" unit="deg"
    d_float,  # float HASA2;		// "Pointing Coefficient" unit="deg"
    d_float,  # float HACA2;		// "Pointing Coefficient" unit="deg"
    d_float,  # float HESA2;		// "Pointing Coefficient" unit="deg"
    d_float,  # float HECA2;		// "Pointing Coefficient" unit="deg"
    d_float,  # float HACA3;		// "Pointing Coefficient" unit="deg"
    d_float,  # float HECA3;		// "Pointing Coefficient" unit="deg"
    d_float,  # float HESA3;		// "Pointing Coefficient" unit="deg"
    d_float,  # float NPAE;		// "Pointing Coefficient (-P3)" unit="deg"
    d_float,  # float CA;			// "Pointing Coefficient (-P2)" unit="deg"
    d_float,  # float AN;			// "Pointing Coefficient (-P5)" unit="deg"
    d_float,  # float AW;			// "Pointing Coefficient (-P4)" unit="deg"
)

SCAN_MBFITS_COLUMNS = ScanMod.SCAN_MBFITS_COLUMNS_TYPE(
    []
)  # sequence<string> FEBE;		// "Frontend-backend combination identification" type="68A"

FEBEPAR_MBFITS_HEADER = ScanMod.FEBEPAR_MBFITS_HEADER_TYPE(
    "FEBEPAR-MBFITS",  # string EXTNAME;	// name of this table "FEBEPAR-MBFITS"
    "NULL-NULL-NULL",  # string FEBE;		// "Frontend-backend combination identification" type="68A"
    d_long,  # long SCANNUM;		// "Scan number"
    d_date,  # string DATE_OBS;	// "Scan start in TIMESYS system" type="23A"
    d_string,  # string DEWCABIN;	// "Dewar cabin: CASS_C or NASMYTH_A or NASMYTH_B" type="68A"
    d_string,  # string DEWRTMOD;	// "Dewar tracking system" type="05A"
    d_float,  # float DEWUSER;	// "Dewar user angle" unit="Deg"
    d_float,  # float DEWZERO;	// "Dewar zero angle" unit="Deg"
    d_long,  # long FEBEBAND;	// "Maximum number configurable basebands for this FEBE"
    d_long,  # long FEBEFEED;	// "Total number of feeds"
    d_long,  # long NUSEBAND;	// "Number of BASEBANDs in use"
    d_long,  # long FDTYPCOD;	// "FEEDTYPE code definition" type="512A"
    d_float,  # float FEGAIN;		// "Frontend amplifier gain"
    d_string,  # string SWTCHMOD;	// "Switch mode" type="20A"
    d_long,  # long NPHASES;		// "No. of switch phases in a switch"
    d_float,  # float FRTHRWLO;	// "Phase 1 frequency switching throw" unit="Hz"
    d_float,  # float FRTHRWHI;	// "Phase 2 frequency switching throw" unit="Hz"
    d_float,  # float TBLANK;		// "Blank time of backend" unit="s"
    d_float,  # float TSYNC;		// "Sync. time of backend" unit="s"
    d_float,  # float IARX;		// "Receiver ptg coef. adds to IA" unit="deg"
    d_float,  # float IERX; 		// "Receiver ptg coef. adds to IE" unit="deg"
    d_float,  # float HASARX;		// "Receiver ptg coef. adds to HASA" unit="deg"
    d_float,  # float HACARX;		// "Receiver ptg coef. adds to HACA" unit="deg"
    d_float,  # float HESERX;		// "Receiver ptg coef. adds to HESE"  unit="deg"
    d_float,  # float HECERX;		// "Receiver ptg coef. adds to HECE" unit="deg"
    d_float,  # float HESARX;		// "Receiver ptg coef. adds to HESA" unit="deg"
    d_float,  # float HASA2RX;	// "Receiver ptg coef. adds to HASA2" unit="deg"
    d_float,  # float HACA2RX;	// "Receiver ptg coef. adds to HACA2" unit="deg"
    d_float,  # float HESA2RX;	// "Receiver ptg coef. adds to HESA2" unit="deg"
    d_float,  # float HECA2RX;	// "Receiver ptg coef. adds to HECA2" unit="deg"
    d_float,  # float HACA3RX;	// "Receiver ptg coef. adds to HACA3" unit="deg"
    d_float,  # float HECA3RX;	// "Receiver ptg coef. adds to HECA3" unit="deg"
    d_float,  # float HESA3RX;	// "Receiver ptg coef. adds to HESA3" unit="deg"
    d_float,  # float NPAERX;		// "Receiver ptg coef. adds to NPAE" unit="deg"
    d_float,  # float CARX;		// "Receiver ptg coef. adds to CA" unit="deg"
    d_float,  # float ANRX;		// "Receiver ptg coef. adds to AN" unit="deg"
    d_float,  # float AWRX;		// "Receiver ptg coef. adds to AW" unit="deg"
)

ARRAYDATA_MBFITS_HEADER = ScanMod.ARRAYDATA_MBFITS_HEADER_TYPE(
    "ARRAYDATA-MBFITS",  # // name of this table "ARRAYDATA-MBFITS"
    d_string,  # string FEBE;		// "Frontend-backend combination ID" type="17A"
    d_long,  # long BASEBAND;		// "Baseband number" type="J" unit="--"
    d_long,  # long SCANNUM;		// "Scan number" type="J"
    d_long,  # long OBSNUM;		// "Subscan number" type="J"
    d_long,  # long SUBSNUM;		// "Subscan number" type="J"
    d_date,  # string DATE_OBS;	// "Subscan start in TIMESYS system" type="23A"
    d_long,  # long CHANNELS;		// "Number of spec channels for this BASEBAND" type="J"
    d_long,  # long NUSEFEED;		// "Number of feeds in use for this BASEBAND" type="J"
    d_double,  # double FREQRES;		// "Frequency resolution" type="D" unit="Hz"
    d_double,  # double BANDWID;		// "Bandwidth of this BASEBAND in the sky/telescope frequency frame" type="D" unit="Hz"
    d_string,  # string MOLECULE;	// "Main line molecule" type="20A"
    d_string,  # string TRANSITI;	// "Transition" type="20A"
    d_double,  # double RESTFREQ;	// "Rest frequency of line" type="D" unit="Hz"
    d_double,  # double SKYFREQ;		// "Sky frequency of the line including all velocity corrections" type="D" unit="Hz"
    d_string,  # string SIDEBAND;	// "Main sideband (USB/LSB)" type="03A"
    d_double,  # double SBSEP;		// "Sideband separation including band offsets in the sky/telescope frequency frame" type="D" unit="Hz"
    d_string,  # string X_2CTYP2;	// "Feed axis (in USEFEED array)" type="08A"
    d_long,  # long X_2CRPX2;		// "Axis 2 Ref position = 1" type="J"
    d_long,  # long X_2CRVL2;		// "Axis 2 feed index value at this position" type="J"
    d_long,  # long X_2CDLT2A;		// "Axis 2 Feed index separation" = 1
    d_long,  # long X_21CD2A;		// (DEPRECATED - keep for back compatibility) replaced by 2CDLT2A
    d_string,  # string WCSNM2F;		// Axis name type="08A" Frequency description in source rest frame for main sideband
    d_string,  # string X_1CTYP2F;	// Frequency axis for col.2 type="8A" value = "FREQ    ". Axis (1) description for DATA
    d_float,  # float X_1CRPX2F;	// Ref. channel type="E"
    d_double,  # double X_1CRVL2F;	// Frequency at ref. channel in rest frame type="D" unit="Hz"
    d_double,  # double X_1CDLT2F;	// Channel separation" unit="Hz"
    d_double,  # double X_11CD2F;	// (DEPRECATED - keep for back compatibility) replaced by 1CDLT2F
    d_string,  # string X_1CUNI2F;	// Unit type="08A" value="Hz"
    d_string,  # string X_1SPEC2F;	// Standard of rest for frequencies type="08A" example="LSRK"
    d_string,  # string X_1SOBS2F;	// Observing frame type="08A". Ref. frame where spectral coordinate has no spatial variation. value="TOPOCENT"
    d_string,  # string WCSNM2I;		// Axis name type="08A" Frequency description in source rest frame for image sideband
    d_string,  # string X_1CTYP2I;	// Frequency axis for col.2 type="8A" value = "FREQ    ". Axis (1) description for DATA
    d_float,  # float X_1CRPX2I;	// Ref. channel type="E"
    d_double,  # double X_1CRVL2I;	// Frequency at ref. channel in rest frame type="D" unit="Hz"
    d_double,  # double X_1CDLT2I;	// Channel separation" unit="Hz"
    d_double,  # double X_11CD2I;	// (DEPRECATED - keep for back compatibility) replaced by 1CDLT2F
    d_string,  # string X_1CUNI2I;	// Unit type="08A" value="Hz"
    d_string,  # string X_1SPEC2I;	// Standard of rest for frequencies type="08A" example="LSRK"
    d_string,  # string X_1SOBS2I;	// Observing frame type="08A". Ref. frame where spectral coordinate has no spatial variation. value="TOPOCENT"
    d_string,  # string WCSNM2S;		// Axis name type="08A" Frequency description in sky (telescope) frame for main sideband
    d_string,  # string X_1CTYP2S;	// Frequency axis for col.2 type="8A" value = "FREQ    ". Axis (1) description for DATA
    d_float,  # float X_1CRPX2S;	// Ref. channel type="E"
    d_double,  # double X_1CRVL2S;	// Frequency at ref. channel in rest frame type="D" unit="Hz"
    d_double,  # double X_1CDLT2S;	// Channel separation" unit="Hz"
    d_double,  # double X_11CD2S;	// (DEPRECATED - keep for back compatibility) replaced by 1CDLT2F
    d_string,  # string X_1CUNI2S;	// Unit type="08A" value="Hz"
    d_string,  # string X_1SPEC2S;	// Standard of rest for frequencies type="08A" example="LSRK"
    d_string,  # string X_1SOBS2S;	// Observing frame type="08A". Ref. frame where spectral coordinate has no spatial variation. value="TOPOCENT"
    d_string,  # string WCSNM2J;		// Axis name type="08A" Frequency description in sky (telescope) frame for image sideband
    d_string,  # string X_1CTYP2J;	// Frequency axis for col.2 type="8A" value = "FREQ    ". Axis (1) description for DATA
    d_float,  # float X_1CRPX2J;	// Ref. channel type="E"
    d_double,  # double X_1CRVL2J;	// Frequency at ref. channel in rest frame type="D" unit="Hz"
    d_double,  # double X_1CDLT2J;	// Channel separation" unit="Hz"
    d_double,  # double X_11CD2J;	// (DEPRECATED - keep for back compatibility) replaced by 1CDLT2F
    d_string,  # string X_1CUNI2J;	// Unit type="08A" value="Hz"
    d_string,  # string X_1SPEC2J;	// Standard of rest for frequencies type="08A" example="LSRK"
    d_string,  # string X_1SOBS2J;	// Observing frame type="08A". Ref. frame where spectral coordinate has no spatial variation. value="TOPOCENT"
    d_string,  # string WCSNM2R;		// Axis name type="08A" Velocity description in rest frame (e.g. LSR) for heterodyne receivers.
    d_string,  # string X_1CTYP2R;	// Velocity axis for col.2 type="8A" value = "FREQ    ". Axis (1) description for DATA
    d_float,  # float X_1CRPX2R;	// Ref. channel type="E"
    d_double,  # double X_1CRVL2R;	// Frequency at ref. channel in rest frame type="D" unit="km/s"
    d_double,  # double X_1CDLT2R;	// Channel separation" unit="km/s"
    d_double,  # double X_11CD2R;	// (DEPRECATED - keep for back compatibility) replaced by 1CDLT2F
    d_string,  # string X_1CUNI2R;	// Unit type="08A" value="km/s"
    d_string,  # string X_1SPEC2R;	// Standard of rest for frequencies type="08A" example="LSRK"
    d_string,  # string X_1SOBS2R;	// Observing frame type="08A". Ref. frame where spectral coordinate has no spatial variation. value="TOPOCENT"
    d_float,  # float X_1VSOU2R;	// Source velocity in rest frame" type="E" unit="km/s"
    d_float,  # float X_1VSYS2R;	// Observer vel. in rest frame" type="E" unit="km/s"  in direction of observation at subscan start.
)

DATAPAR_MBFITS_HEADER = ScanMod.DATAPAR_MBFITS_HEADER_TYPE(
    "DATAPAR-MBFITS",  # string EXTNAME;		// name of this table "DATAPAR-MBFITS"
    d_long,  # long SCANNUM;		// "Scan number" type="J"
    d_long,  # long OBSNUM;		// "Subscan number" type="J"
    d_long,  # long SUBSNUM;		// "Subscan number" type="J"
    d_date,  # string DATE_OBS;	// "Subscan start in TIMESYS system" type="23A"
    d_string,  # string FEBE;		// "Frontend-backend combination ID" type="17A"
    d_double,  # double LST;			// "Local apparent sidereal time (obs. start)" type="D" unit="s"
    d_string,  # string SUBSTYPE;	// "Subscan type" type="04A"
    d_string,  # string OBSTYPE;		// (DEPRECATED - keep for back compatibility) replaced by SUBSTYPE
    d_string,  # string SCANTYPE;	// "Scan type" type="20A"
    d_string,  # string SCANMODE;	// "Mapping mode" type="20A"
    d_string,  # string SCANDIR;		//"(optional) scan direction" type="04A"
    d_double,  # double SCANLEN;		// "(optional, OTF/RASTER) line length" type="D" unit=""
    d_double,  # double SCANXVEL;	// "(optional, OTF) tracking rate along line." type="D" unit=""
    d_double,  # double SCANTIME;	// "(optional, OTF) time for one line" type="D" unit=""
    d_double,  # double SCANXSPC;	// "(optional, RASTER) step along line between samples" type="D" unit=""
    d_double,  # double SCANYSPC;	// "(optional, OTF/RASTER) step between scan/raster lines" type="D" unit=""
    d_double,  # double SCANSKEW;	// "(optional, OTF/RASTER) offset in scan direction between lines" type="D" unit=""
    d_double,  # double SCANPAR1;	// "(optional) spare scan parameter" type="D" unit=""
    d_double,  # double SCANPAR2;	// "(optional) another spare scan parameter" type="D" unit=""
    d_string,  # string NLNGTYPE; 	// Native system type (longitude)XLON-SFL (where x takes appropriate values) type="8A".  Use for AZ pattern while tracking RA main coord. Pointing scans and antenna pattern on EQ sources
    d_string,  # string CTYPE1N;		// (DEPRECATED - keep for back compatibility) replaced by NLNGTYPE
    d_string,  # string NLATTYPE; 	// Native system type (latitude)XLAT-SFL (where x takes appropriate values) type="8A". Use for EL pattern while tracking DEC main coord.  Pointing scans and antenna pattern on EQ sources
    d_string,  # string CTYPE2N;		// (DEPRECATED - keep for back compatibility) replaced by NLATTYPE
    d_boolean,  # boolean DPBLOCK;	// "Data blocking?" type="L"
    d_long,  # long NINTS;			// "Number of integrations in block" type="J"  preset="1"
    d_string,  # string OBSTATUS;	// "Subscan ok? (OK/ABORT)" type="10A"
    d_boolean,  # boolean WOBCOORD;	// "Coordinates include wobbler offsets?" type="L"
)

# Create an object of all the data
metadata = ScanMod.MetadataNotifyBlock(
    PRIMARY_MBFITS_HEADER,
    SCAN_MBFITS_HEADER,
    SCAN_MBFITS_COLUMNS,
    FEBEPAR_MBFITS_HEADER,
    ARRAYDATA_MBFITS_HEADER,
    DATAPAR_MBFITS_HEADER,
)
