# -----------------------------------------------------------------------------
# Copyright (C) 2022
# National Astronomical Research Institute of Thailand (NARIT)
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License along with
# this program; if not, see <https://www.gnu.org/licenses>.
# ------------------------------------------------------------------------------
from unittest_common import ScanTestCase
import ScanMod

class Tests(ScanTestCase):
    def setUp(self):
        super().setUp()
        self.result = None

    def tearDown(self):
        self.logger.info(self.result)
        super().tearDown()

    def test5500_DataParamsNone(self):
        pipeline_names = []
        self.logger.debug("pipeline_names: {}".format(pipeline_names))
        self.result = ScanMod.DataParams(pipeline_names)
        self.assertIsInstance(self.result, ScanMod.DataParams)

    def test5510_DataParamsMbfits(self):
        pipeline_names = ["mbfits"]
        self.logger.debug("pipeline_names: {}".format(pipeline_names))
        self.result = ScanMod.DataParams(pipeline_names)
        self.assertIsInstance(self.result, ScanMod.DataParams)

    def test5511_DataParamSpectrum(self):
        pipeline_names = ["spectrum_preview"]
        self.logger.debug("pipeline_names: {}".format(pipeline_names))
        self.result = ScanMod.DataParams(pipeline_names)
        self.assertIsInstance(self.result, ScanMod.DataParams)

    def test5512_DataParamsAtfits(self):
        pipeline_names = ["atfits"]
        self.logger.debug("pipeline_names: {}".format(pipeline_names))
        self.result = ScanMod.DataParams(pipeline_names)
        self.assertIsInstance(self.result, ScanMod.DataParams)

    def test5513_DataParamsGildas(self):
        pipeline_names = ["gildas"]
        self.logger.debug("pipeline_names: {}".format(pipeline_names))
        self.result = ScanMod.DataParams(pipeline_names)
        self.assertIsInstance(self.result, ScanMod.DataParams)

    def test5514_DataParamsMultiple(self):
        pipeline_names = ["mbfits", "spectrum_preview"]
        self.logger.debug("pipeline_names: {}".format(pipeline_names))
        self.result = ScanMod.DataParams(pipeline_names)
        self.assertIsInstance(self.result, ScanMod.DataParams)

    def test5515_DataParamsMultiple2(self):
        pipeline_names = ["spectrum_preview", "atfits"]
        self.logger.debug("pipeline_names: {}".format(pipeline_names))
        self.result = ScanMod.DataParams(pipeline_names)
        self.assertIsInstance(self.result, ScanMod.DataParams)

