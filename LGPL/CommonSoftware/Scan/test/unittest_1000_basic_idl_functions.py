# -----------------------------------------------------------------------------
# Copyright (C) 2022
# National Astronomical Research Institute of Thailand (NARIT)
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License along with
# this program; if not, see <https://www.gnu.org/licenses>.
# ------------------------------------------------------------------------------
import time

from Acspy.Nc.Supplier import Supplier
from Acspy.Nc.Consumer import Consumer

import ScanMod
import ScanError
import ScanDefaults

from unittest_common import ScanTestCase


class Tests(ScanTestCase):
    def setUp(self):
        super().setUp()
        self.result = None

    def tearDown(self):
        self.logger.info(self.result)
        super().tearDown()

    def test1000_get_metadata(self):
        self.logger.debug("get_scan_mbfits_header()")
        self.result = self.objref.get_scan_mbfits_header()
        self.assertIsInstance(self.result, ScanMod.SCAN_MBFITS_HEADER_TYPE)

    def test1001_get_metadata(self):
        self.logger.debug("get_scan_mbfits_columns()")
        self.result = self.objref.get_scan_mbfits_columns()
        self.assertIsInstance(self.result, ScanMod.SCAN_MBFITS_COLUMNS_TYPE)

    def test1002_get_metadata(self):
        self.logger.debug("get_febepar_mbfits_header()")
        self.result = self.objref.get_febepar_mbfits_header()
        self.assertIsInstance(self.result, ScanMod.FEBEPAR_MBFITS_HEADER_TYPE)

    def test1003_get_metadata(self):
        self.logger.debug("get_arraydata_mbfits_header()")
        self.result = self.objref.get_arraydata_mbfits_header()
        self.assertIsInstance(self.result, ScanMod.ARRAYDATA_MBFITS_HEADER_TYPE)

    def test1004_get_metadata(self):
        self.logger.debug("get_datapar_mbfits_header()")
        self.result = self.objref.get_datapar_mbfits_header()
        self.assertIsInstance(self.result, ScanMod.DATAPAR_MBFITS_HEADER_TYPE)

    def test1005_get_metadata(self):
        self.logger.debug("get_metadata()")
        self.result = self.objref.get_metadata()
        self.assertIsInstance(self.result, ScanMod.MetadataNotifyBlock)

    def test1100_get_TLE(self):
        self.logger.debug("get_TLE()")
        self.result = self.objref.get_TLE(38098)
        self.assertIsInstance(self.result, ScanMod.TLEData)

    def test1101_get_TLE_not_found(self):
        with self.assertRaises(ScanError.httpResponseEx):
            self.logger.debug("get_TLE_not_found()")
            self.objref.get_TLE(2)

    def test1200_get_scan_id_counter(self):
        self.logger.debug("get_scan_id_counter()")
        self.result = self.objref.get_scan_id_counter()
        self.assertIsInstance(self.result, int)

    def test1201_get_scan_id_lastrun(self):
        self.logger.debug("get_scan_id_lastrun()")
        self.result = self.objref.get_scan_id_lastrun()
        self.assertIsInstance(self.result, int)

    def test1202_qsize(self):
        self.logger.debug("qsize()")
        self.result = self.objref.qsize()
        self.assertIsInstance(self.result, int)

    def test1300_notification(self):
        nc_channel_name = ScanMod.CHANNEL_NAME_METADATA
        nc_channel_dtype = ScanMod.MetadataNotifyBlock

        self.logger.debug(
            "Creating Notification Channel Consumer {}: ".format(nc_channel_name)
        )
        self.consumer = Consumer(nc_channel_name)
        self.consumer.addSubscription(nc_channel_dtype, self._handler_function)
        self.consumer.consumerReady()
        self.consumer = None

        self.logger.debug(
            "Creating Notification Channel Supplier {}: ".format(nc_channel_name)
        )
        self.supplier = Supplier(ScanMod.CHANNEL_NAME_METADATA)

        scan_metadata = ScanDefaults.metadata

        self.logger.debug("publish message Scan metadata")
        self.supplier.publishEvent(scan_metadata)

    def _handler_function(self, notification_data):
        pass
