# -----------------------------------------------------------------------------
# Copyright (C) 2022
# National Astronomical Research Institute of Thailand (NARIT)
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License along with
# this program; if not, see <https://www.gnu.org/licenses>.
# ------------------------------------------------------------------------------
from unittest_common import ScanTestCase
import ScanMod
import ScanDefaults
import tracking_models


class Tests(ScanTestCase):
    def setUp(self):
        super().setUp()
        self.result = None
        self.metadata = ScanDefaults.metadata

        user_pointing_correction_az = 0
        user_pointing_correction_el = 0
        elevation_min = 15
        elevation_max = 85
        north_crossing = True
        use_horizontal_tables = False
        max_tracking_errors = 6

        self.tracking_common = ScanMod.TrackingParamsCommon(
            user_pointing_correction_az,
            user_pointing_correction_el,
            elevation_min,
            elevation_max,
            north_crossing,
            use_horizontal_tables,
            max_tracking_errors,
        )

    def tearDown(self):
        self.logger.info(self.result)
        super().tearDown()

    def test2000_TrackCoordModelNone(self):
        self.logger.debug("create TrackCoordNone")
        self.result = tracking_models.TrackCoordModelFactory.from_params(
            self.metadata, None
        )
        self.assertIsInstance(self.result, tracking_models.TrackCoordNone)

    def test2001_TrackCoordHO(self):
        self.logger.debug("create TrackCoordHO")
        az = 50
        el = 10
        tracking_params = ScanMod.TrackingParamsHO(az, el, self.tracking_common)

        self.result = tracking_models.TrackCoordModelFactory.from_params(
            self.metadata, tracking_params
        )
        self.assertIsInstance(self.result, tracking_models.TrackCoordHO)

    def test2002_TrackCoordModelTLE(self):
        self.logger.debug("create TrackCoordTLE")
        line0 = "INTELSAT 22 (IS-22)     "
        line1 = "1 38098U 12011A   20070.00674396 -.00000076  00000-0  00000+0 0  9990"
        line2 = "2 38098   0.0020 184.2253 0002748 169.5430 248.8790  1.00272611 29083"
        tracking_params = ScanMod.TrackingParamsTLE(
            line0, line1, line2, self.tracking_common
        )
        self.result = tracking_models.TrackCoordModelFactory.from_params(
            self.metadata, tracking_params
        )
        self.assertIsInstance(self.result, tracking_models.TrackCoordTLE)

    def test2003_TrackCoordModelEQ(self):
        self.logger.debug("create TrackCoordEQ")
        ra_catalog = 300
        dec_catalog = 60
        pm_ra = 0
        pm_dec = 0
        parallax = 0
        radial_velocity = 10.0
        send_icrs_to_acu = False
        tracking_params = ScanMod.TrackingParamsEQ(
            ra_catalog,
            dec_catalog,
            pm_ra,
            pm_dec,
            parallax,
            radial_velocity,
            send_icrs_to_acu,
            self.tracking_common,
        )
        self.result = tracking_models.TrackCoordModelFactory.from_params(
            self.metadata, tracking_params
        )
        self.assertIsInstance(self.result, tracking_models.TrackCoordEQ)
