from sgp4.api import Satrec
import numpy as np

from astropy.time import Time
from astropy import units as units
from astropy.coordinates import EarthLocation
from ScanImpl import Scan

EARTH_EQUATORIAL_RADIUS = 6378.135  # equatorial radius
EARTH_FLATTENING_CONSTANT = 1 / 298.26
GEO_SYNC_RADIUS = 42164.57


def eci_coords_observer(obs_lat_rad, obs_alt_km, lmst_rad):
    '''
    Infer ECI coordinates of observer for a certain observation time.
    Parameters
    ----------
    lmst_rad : `~numpy.ndarray` or float
            Local mean sidereal time, LMST [rad]
    Returns
    -------
    x, y, z : `~numpy.ndarray` or float
            ECI (=Geocentric cartesian inertial) coordinates of observer [km]
    Notes
    -----
    The function accounts for Earth flattening.
    '''

    # C = 1
    # S = 1
    C = 1. / np.sqrt(1 + EARTH_FLATTENING_CONSTANT * (EARTH_FLATTENING_CONSTANT - 2) * np.sin(obs_lat_rad) ** 2)
    S = (1. - EARTH_FLATTENING_CONSTANT) ** 2 * C

    # make ECI coordinates corrected for earth flattening
    earth_rad = EARTH_EQUATORIAL_RADIUS + obs_alt_km
    x = earth_rad * C * np.cos(obs_lat_rad) * np.cos(lmst_rad)
    y = earth_rad * C * np.cos(obs_lat_rad) * np.sin(lmst_rad)
    z = earth_rad * S * np.sin(obs_lat_rad)

    return x, y, z

def lookangle(xs, ys, zs, xo, yo, zo, obs_lat_rad, lmst_rad):
    '''
    Horizontal position of and distance to satellite w.r.t. observer.
    Parameters
    ----------
    xs, ys, zs : `~numpy.ndarray` or float
            ECI (=Geocentric cartesian inertial) coordinates of satellite [km]
    xo, yo, zo : `~numpy.ndarray` or float
            ECI (=Geocentric cartesian inertial) coordinates of observer [km]
    lmst_rad : `~numpy.ndarray` or float
            Local mean sidereal time, LMST [rad]
    Returns
    -------
    az, el : `~numpy.ndarray` or float
            Azimuth/elevation of satellite w.r.t. observer [deg]
    dist : `~numpy.ndarray` or float
            Distance to satellite [km]
    '''

    rx = xs - xo
    ry = ys - yo
    rz = zs - zo


    s_obs_lat_rad = np.sin(obs_lat_rad)
    c_obs_lat_rad = np.cos(obs_lat_rad)
    s_lmst_rad = np.sin(lmst_rad)
    c_lmst_rad = np.cos(lmst_rad)

    r_s = (
        s_obs_lat_rad * c_lmst_rad * rx +
        s_obs_lat_rad * s_lmst_rad * ry -
        c_obs_lat_rad * rz
    )
    r_e = -s_lmst_rad * rx + c_lmst_rad * ry
    r_z = (
        c_obs_lat_rad * c_lmst_rad * rx +
        c_obs_lat_rad * s_lmst_rad * ry +
        s_obs_lat_rad * rz
    )

    dist = np.sqrt(r_s ** 2 + r_e ** 2 + r_z ** 2)
    az = np.degrees(np.arctan2(-r_e, r_s) + np.pi)
    az = (az + 180) % 360 - 180
    el = np.degrees(np.arcsin(r_z / dist))

    return az, el, dist

longitude = 99.216805
latitude = 18.864348
height = 403.625

site_location = EarthLocation(EarthLocation.from_geodetic(lon=longitude * units.deg,
                                                          lat=latitude * units.deg,
                                                          height=height * units.m,
                                                          ellipsoid='WGS84'))
print('site location')
print(site_location)


# MJD from the user
obstime_mjd = Time.now().mjd
# Create an Astropy Time object
obstime = Time(obstime_mjd, format='mjd', scale='utc')
# Calculate local mean sidereal time at this location
lmst_rad = obstime.sidereal_time('mean', 'greenwich').rad + site_location.lon.rad

xo, yo, zo = eci_coords_observer(site_location.lat.rad, site_location.height.to(units.km).value, lmst_rad)

# Definitions for the satellite
scan = Scan.Scan()
TLEData = scan.get_TLE(38098) # INTELSAT 22
line0 = TLEData.name
line1 = TLEData.line1
line2 = TLEData.line2

satellite = Satrec.twoline2rv(line1, line2)


e, (xs, ys, zs), v = satellite.sgp4(obstime.jd1, obstime.jd2)


print('error code')
print(e)
print('')

print('position [km] XYZ Earth Centered Intertial coordinates (ECI)')
print([xs, ys, zs])
print('')

print('velocity [km/s] XYZ')
print(v)
print('')


# Use reference from pycraf to calculate the AltAz angle without installing pycraf package
# We are stuck in Python 2.7.x, but pycraf package only supports Python 3.5+, so just
# copy the bits and pieces that I need.
# https://github.com/bwinkel/pycraf/blob/master/pycraf/satellite/satellite.py

# Other reference is


az, el, dist = lookangle(xs, ys, zs, xo, yo, zo,site_location.lat.rad, lmst_rad)

if(az < 0):
    az = az + 360

print('option 1 - transform satellite ECI->AltAz @ observer(time, location) manually using technique from pycraf package')
print('az = %f' % az)
print('el = %f' % el)
