import argparse
from argparse import RawTextHelpFormatter
import sys
import numpy as np
import os
from astropy.io import fits


# Setup rules for command line arguments
parser = argparse.ArgumentParser(
    description="Reads FITS file written by PipelineMbfits and diplays some graphs",
    formatter_class=RawTextHelpFormatter,
)
parser.add_argument("filename", help="FITS file with format specified by DataFormatMbfits.py")

# If no command line arguments are avaiable, show help message and exit
if len(sys.argv[1:]) == 0:
    parser.print_help()
    exit()

# Read command line arguments into argparse structure
args = parser.parse_args()

if os.path.isfile(args.filename) is not True:
    print("ERROR: File not found")
    print("os.path.isfile(%s) = False" % args.filename)
    hdul = None
else:
    print("Processing file %s: " % args.filename)
    fid = open(args.filename, "rb")
    hdul = fits.open(fid)
if hdul is None:
    exit()



expected_cross_scan_nsubs = 6
map_num_to_subscan = [
    {
        'name': 'CAL_BEFORE',
        'value': '100001'
    },
    {
        'name': 'HP',
        'value': '0'
    }, 
    {
        'name': 'HN',
        'value': '1'
    }, 
    {
        'name': 'VP',
        'value': '2'
    }, 
    {
        'name': 'VN',
        'value': '3'
    }, 
    {
        'name': 'CAL_AFTER',
        'value': '100002'
    },
]

if hdul['SCAN-MBFITS'].header['NSUBS'] == expected_cross_scan_nsubs:
    print('[OK] NSUBS' )

for x in range(expected_cross_scan_nsubs):
    # ARRAYDATA-MBFITS
    arraydata_mbfits_start_index = 3
    if(hdul[2 * x + arraydata_mbfits_start_index].header['SUBSNUM'] == int(map_num_to_subscan[x]['value'])):
        print('[OK] ARRAYDATA-MBFITS {} subscan SUBSNUM'.format(map_num_to_subscan[x]['name']))
    else:
        print('[FAIL] ARRAYDATA-MBFITS {} subscan SUBSNUM'.format(map_num_to_subscan[x]['name']))

    # DATAPAR-MBFITS
    datapar_mbfits_start_index = 4
    if(hdul[2 * x + datapar_mbfits_start_index].header['SUBSNUM'] == int(map_num_to_subscan[x]['value'])):
        print('[OK] DATAPAR-MBFITS {} subscan SUBSNUM'.format(map_num_to_subscan[x]['name']))
    else:
        print('[FAIL] DATAPAR-MBFITS {} subscan SUBSNUM'.format(map_num_to_subscan[x]['name']))


fid.close()


