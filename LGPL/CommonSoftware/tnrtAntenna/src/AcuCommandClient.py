# Project: TNRT - Thai National Radio Telescope
# Organization: NARIT - National Astronomical Research Institute of Thailand
# Author: Spiro Sarris
# Date: 2020.11.01

import threading
import ctypes
import socket
import logging
import DataFormatAcu as acu
import time


class AckError(Exception):
    pass


class AcuCommandClient(threading.Thread):
    """
    Client to format messages for ACU.  No state information is
    saved here.  Ony create commands and check ACK.
    """

    def __init__(self, host, port, recv_timeout):
        self.host = host
        self.port = port
        self.recv_timeout = recv_timeout
        self.sock = None
        self.command_serial_number = 1
        self.logger = logging.getLogger("AcuCommandClient")
        super().__init__()

    def run(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            self.sock.connect((self.host, self.port))
            self.sock.settimeout(self.recv_timeout)
        except:
            self.sock.close()
            raise

    def client_close(self):
        self.sock.close()

    def send_command(self, packet):
        packet.start_flag = acu.STARTFLAG
        packet.end_flag = acu.ENDFLAG

        ack = None
        retry = 3

        while retry > 0:
            retry = retry - 1
            # Increment long-living serial number (for this client connection)
            self.command_serial_number += 1
            # Set the serial number of the current packet
            packet.phdr.command_serial_number = self.command_serial_number
            self.logger.info(
                "Sending CMD id: {} serial number: {} ...".format(
                    packet.chdr.command_id, packet.phdr.command_serial_number
                )
            )
            self.logger.debug("CMD: {}".format(repr(packet)))

            # Send the packet
            self.sock.sendall(packet)
            try:
                # Read the ACK message and compare the header to the packet we sent
                ack = self.check_ack(
                    packet.chdr.subsystem_id,
                    packet.chdr.command_id,
                    packet.phdr.command_serial_number,
                )
            except AckError:
                self.logger.warning(
                    "check_ack failed.  send command again. retry remain {:d}".format(
                        retry
                    )
                )
                # Something is wrong with ACK.  return to the while loop and try again
                continue

            if (ack.status == acu.CMD_ACK_STS_DONE.value) or (
                ack.status == acu.CMD_ACK_STS_ACCEPTED.value
            ):
                self.logger.info("ACK Status success (DONE or ACCEPTED)")
                break  # ACK is good.  exit this loop.

            if (ack.status == acu.CMD_ACK_STS_WRONG_MODE.value) and (
                packet.phdr.number_of_commands > 1
            ):
                self.logger.info(
                    "ACK Status cmdWRONGMODE for a multi-command packet.  Do not repeat this command."
                )
                break
                # SS. 06/2020.  When we send an AllAXis command, the ACU always returns subsystem 20 (M3) and WRONGMODE
                # even though the command appears to succeed.  Ask Katja about what ACK to expect for a multiple
                # command.

            self.logger.warning(
                "ACK Status not success (not DONE or ACCEPTED).  Send retry remain {:d}".format(
                    retry
                )
            )

        # (SS. 05/2022):
        # After checking everything and ACK still doesn't make sense, create a dummy ACK with
        # status CMD_ACK_STS_TIMEOUT to avoid unhandled exceptions when this function is not used in a try : except block.
        # Makes unittest easier.
        if ack is None:
            ack = acu.Acknowledge(status=acu.CMD_ACK_STS_TIMEOUT)

        return ack

    def check_ack(self, subsystem_id, command_id, command_serial_number):
        # SS. Sometimes old ACK messages remain the buffer of ACU socket server if we connect again after
        # another error and the old message was not flushed.  Use while loop to read all old ACK messages
        # until we arrive at the ACK for current command.  If this socket recv fails on timeout because
        # partial or broken message, raise an exception and escape the loop that way..
        # countdown retry to avoid infinite loop if some unknown error causes socket server to crash.
        retry = 5

        # Create an empty ACK message with command_serial_number=0.  The first command serial
        # number is 1, therefore this loop must run at least once for the first command after
        # connection
        ack = acu.Acknowledge(command_serial_number=0)
        ack.status = acu.CMD_ACK_STS_UNDEF
        while (ack.command_serial_number < command_serial_number) and (retry > 0):
            retry = retry - 1
            # Read the raw ACK message from the socket (ACU should send this ACK within 2 seconds)
            try:
                # blocking receive with timeout.  Read all the bytes. They don't always arrive
                # in one read, therefore need a loop to read all.
                segments = []
                bytes_received = 0

                while bytes_received < ctypes.sizeof(acu.Acknowledge):
                    remaining_bytes = ctypes.sizeof(acu.Acknowledge) - bytes_received
                    segment = self.sock.recv(remaining_bytes, socket.MSG_WAITALL)
                    # self.logger.debug('bytes received: {:d}, remaining_bytes: {:d}'.format(bytes_received, remaining_bytes))
                    if segment == b"":
                        self.logger.error("socket connection broken")
                    segments.append(segment)
                    bytes_received = bytes_received + len(segment)

                raw_ack = b"".join(segments)

            except:
                self.logger.warning(
                    "Socket recv error. read ACK again. retry remain {:d}".format(retry)
                )
                continue
            # Deserialize the buffer into an Acknowledge object
            ack = acu.Acknowledge.from_buffer_copy(raw_ack)
            self.logger.debug("ACK: {}".format(repr(ack)))

            if len(raw_ack) != ctypes.sizeof(ack):
                self.logger.error(
                    "ack.socket recv TIMEOUT. bytes read = {:d} of requested {:d}".format(
                        cyptes.sizeof(raw_ack), ctypes.sizeof(ack)
                    )
                )
            if ack.start_flag != acu.STARTFLAG.value:
                self.logger.warning(
                    "ack.start_flag = {:X}.  expect {:X}".format(
                        ack.start_flag, acu.STARTFLAG.value
                    )
                )
            if ack.message_length != ctypes.sizeof(ack):
                self.logger.warning(
                    "ack.message_length = {:d}, expect {:d}".format(
                        ack.message_length, sizeof(ack)
                    )
                )
            if ack.source != acu.SRC_REMOTE.value:
                self.logger.warning(
                    "ack.source = {:d}, expect {:d}".format(
                        ack.source, acu.SRC_REMOTE.value
                    )
                )
            if ack.subsystem_id != subsystem_id:
                self.logger.warning(
                    "ack.subsystem_id = {:d}, expect {:d}".format(
                        ack.subsystem_id, subsystem_id
                    )
                )
            if ack.command != command_id:
                self.logger.warning(
                    "ack.command_id = {:d}, expect {:d}".format(ack.command, command_id)
                )
            if ack.command_serial_number != command_serial_number:
                self.logger.warning(
                    "ack.command_serial_number = {:d}, expect {:d}".format(
                        ack.command_serial_number, command_serial_number
                    )
                )
            if ack.command_identifier != acu.CMD_ACK.value:
                self.logger.warning(
                    "ack.command_identifier = {:d}, expect {:d}".format(
                        ack.command_identifier, acu.CMD_ACK.value
                    )
                )
            if ack.end_flag != acu.ENDFLAG.value:
                self.logger.warning(
                    "ack.end_flag = {:X}, expect {:X}".format(
                        ack.end_flag, acu.ENDFLAG.value
                    )
                )

        # Exited the while loop because:
        #   (1) serial number matches or
        #   (2) maximum retry already used, but ACK is still not correct.
        if retry == 0:
            raise AckError

        return ack

    # Functions that can be reused for many specific commands
    def send_standard_command(
        self, command_type, subsystem_id, command_id, param1, param2
    ):
        packet = acu.CommandPacketStandard()
        packet.phdr = acu.PacketHeader(
            ctypes.sizeof(packet), acu.SRC_REMOTE, 0, acu.CMD_QTY_SINGLE
        )
        packet.chdr = acu.CommandHeader(command_type, subsystem_id, command_id)
        packet.params = acu.ParameterStandard(param1, param2)
        return self.send_command(packet)

    def send_hexapod_command(
        self,
        command_type,
        subsystem_id,
        command_id,
        param1,
        param2,
        param3,
        param4,
        param5,
        param6,
        param7,
        param8,
        param9,
        param10,
        param11,
        param12,
    ):
        packet = acu.CommandPacketHexapod()
        packet.phdr = acu.PacketHeader(
            ctypes.sizeof(packet), acu.SRC_REMOTE, 0, acu.CMD_QTY_SINGLE
        )
        packet.chdr = acu.CommandHeader(command_type, subsystem_id, command_id)
        packet.params = acu.ParameterHexapod(
            param1,
            param2,
            param3,
            param4,
            param5,
            param6,
            param7,
            param8,
            param9,
            param10,
            param11,
            param12,
        )
        return self.send_command(packet)

    def send_all_axis_mode_command(self, command_id):
        packet = acu.CommandPacketAllAxis()
        packet.phdr = acu.PacketHeader(
            ctypes.sizeof(packet), acu.SRC_REMOTE, 0, acu.CMD_QTY_ALLAXIS
        )
        packet.chdr_ax1 = acu.CommandHeader(acu.CMD_MODE, acu.SUBSYSTEM_AZ, command_id)
        packet.params_ax1 = acu.ParameterStandard(0.0, 0.0)
        packet.chdr_ax2 = acu.CommandHeader(acu.CMD_MODE, acu.SUBSYSTEM_EL, command_id)
        packet.params_ax2 = acu.ParameterStandard(0.0, 0.0)
        packet.chdr_ax3 = acu.CommandHeader(acu.CMD_MODE, acu.SUBSYSTEM_M3, command_id)
        packet.params_ax3 = acu.ParameterStandard(0.0, 0.0)
        packet.chdr_ax4 = acu.CommandHeader(acu.CMD_MODE, acu.SUBSYSTEM_M3R, command_id)
        packet.params_ax4 = acu.ParameterStandard(0.0, 0.0)
        packet.chdr_ax5 = acu.CommandHeader(acu.CMD_MODE, acu.SUBSYSTEM_M4A, command_id)
        packet.params_ax5 = acu.ParameterStandard(0.0, 0.0)
        packet.chdr_ax6 = acu.CommandHeader(acu.CMD_MODE, acu.SUBSYSTEM_M4B, command_id)
        packet.params_ax6 = acu.ParameterStandard(0.0, 0.0)
        packet.chdr_ax7 = acu.CommandHeader(acu.CMD_MODE, acu.SUBSYSTEM_THU, command_id)
        packet.params_ax7 = acu.ParameterStandard(0.0, 0.0)
        packet.chdr_ax8 = acu.CommandHeader(acu.CMD_MODE, acu.SUBSYSTEM_GRS, command_id)
        packet.params_ax8 = acu.ParameterStandard(0.0, 0.0)
        packet.chdr_hxp = acu.CommandHeader(acu.CMD_MODE, acu.SUBSYSTEM_HXP, command_id)
        packet.params_hxp = acu.ParameterHexapod(
            0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0
        )
        return self.send_command(packet)

    # Special commands
    def send_track_table_command(
        self,
        command_id,
        object_id,
        load_mode,
        interpolate_mode,
        track_mode,
        sequence_length,
        timeAzRaElDec_Array_50,
    ):
        packet = acu.CommandPacketTrackTable()
        packet.phdr = acu.PacketHeader(
            ctypes.sizeof(packet), acu.SRC_REMOTE, 0, acu.CMD_QTY_SINGLE
        )
        packet.chdr = acu.CommandHeader(acu.CMD_SPECIAL, acu.SUBSYSTEM_TR, command_id)
        packet.params = acu.ParameterTrackTable(
            object_id,
            load_mode,
            interpolate_mode,
            track_mode,
            sequence_length,
            timeAzRaElDec_Array_50,
        )
        return self.send_command(packet)

    def send_track_table_hexapod_command(
        self, load_mode, interpolate_mode, sequence_length, timePos_Array_20
    ):
        packet = acu.CommandPacketTrackTableHexapod()
        packet.phdr = acu.PacketHeader(
            ctypes.sizeof(packet), acu.SRC_REMOTE, 0, acu.CMD_QTY_SINGLE
        )
        packet.chdr = acu.CommandHeader(
            acu.CMD_SPECIAL, acu.SUBSYSTEM_HXP, acu.CMD_LOAD_PROGRAM_TRACK_TABLE_HEXAPOD
        )
        packet.params = acu.ParameterTrackTableHexapod(
            load_mode, interpolate_mode, sequence_length, timePos_Array_20
        )
        return self.send_command(packet)

    def send_elevation_table_hexapod_command(self, sequence_length, elPos_Array_50):
        packet = acu.CommandPacketElevationOffsetTable()
        packet.phdr = acu.PacketHeader(
            ctypes.sizeof(packet), acu.SRC_REMOTE, 0, acu.CMD_QTY_SINGLE
        )
        packet.chdr = acu.CommandHeader(
            acu.CMD_SPECIAL,
            acu.SUBSYSTEM_HXP,
            acu.CMD_LOAD_ELEVATION_OFFSET_TABLE_HEXAPOD,
        )
        packet.params = acu.ParameterElevationOffsetTable(
            sequence_length, elPos_Array_50
        )
        return self.send_command(packet)

    def send_pointing_model_command(self, double_Array_9):
        packet = acu.CommandPacketPointingModel()
        packet.phdr = acu.PacketHeader(
            ctypes.sizeof(packet), acu.SRC_REMOTE, 0, acu.CMD_QTY_SINGLE
        )
        packet.chdr = acu.CommandHeader(
            acu.CMD_SPECIAL, acu.SUBSYSTEM_PO, acu.CMD_SET_POINTING_MODEL
        )
        packet.params = acu.ParameterPmodel(double_Array_9)
        return self.send_command(packet)

    def send_refraction_command(self, r0, b1, b2):
        packet = acu.CommandPacketRefraction()
        packet.phdr = acu.PacketHeader(
            ctypes.sizeof(packet), acu.SRC_REMOTE, 0, acu.CMD_QTY_SINGLE
        )
        packet.chdr = acu.CommandHeader(
            acu.CMD_SPECIAL, acu.SUBSYSTEM_PO, acu.CMD_SET_REFRACTION_CORRECTION
        )
        packet.params = acu.ParameterRefraction(r0, b1, b2)
        return self.send_command(packet)

    def send_norad_command(self, noradName, noradLine1, noradLine2):
        object_id = 1  # always 1
        packet = acu.CommandPacketNORAD()
        packet.phdr = acu.PacketHeader(
            ctypes.sizeof(packet), acu.SRC_REMOTE, 0, acu.CMD_QTY_SINGLE
        )
        packet.chdr = acu.CommandHeader(
            acu.CMD_SPECIAL, acu.SUBSYSTEM_TR, acu.CMD_LOAD_NORAD
        )
        packet.params = acu.ParameterNORAD(object_id, noradName, noradLine1, noradLine2)
        return self.send_command(packet)

    def send_set_track_mode_command(self, trackDataSource):
        packet = acu.CommandPacketSetTrackMode()
        packet.phdr = acu.PacketHeader(
            ctypes.sizeof(packet), acu.SRC_REMOTE, 0, acu.CMD_QTY_SINGLE
        )
        packet.chdr = acu.CommandHeader(
            acu.CMD_SPECIAL, acu.SUBSYSTEM_TR, acu.CMD_SET_TRACK_MODE
        )
        packet.params = acu.ParameterSetTrackMode(trackDataSource, 0.0, 0.0, 0.0)
        return self.send_command(packet)

    def send_track_setup_command(self, startTimeMJD, programTrackMode):
        object_id = 1  # always 1
        packet = acu.CommandPacketTrackSetup()
        packet.phdr = acu.PacketHeader(
            ctypes.sizeof(packet), acu.SRC_REMOTE, 0, acu.CMD_QTY_SINGLE
        )
        packet.chdr = acu.CommandHeader(
            acu.CMD_SPECIAL, acu.SUBSYSTEM_TR, acu.CMD_TRACK_SETUP
        )
        packet.params = acu.ParameterTrackSetup(
            object_id, startTimeMJD, programTrackMode
        )
        return self.send_command(packet)


if __name__ == "__main__":
    command_client = AcuCommandClient("127.0.0.1", 9000, 5)
    command_client.start()

    packet = acu.CommandPacketStandard()

    # Generate a test message packet
    packet.start_flag = acu.STARTFLAG
    packet.phdr = acu.PacketHeader()
    packet.phdr.message_length = ctypes.sizeof(packet)
    packet.phdr.source = acu.SRC_REMOTE
    packet.phdr.number_of_commands = 1
    packet.chdr = acu.CommandHeader()
    packet.chdr.command_type = acu.CMD_MODE
    packet.chdr.subsystem_id = acu.SUBSYSTEM_M3
    packet.chdr.command_id = acu.CMD_POSITION_ABSOLUTE
    packet.params = acu.ParameterStandard(0.0, 0.0)
    packet.end_flag = acu.ENDFLAG

    print("manual assemble commmand")
    print("COMMAND: {}".format(repr(packet)))
    print()
    # Send use the client to send the packet
    ack_status = command_client.send_command(packet)
    print("ACK: {}".format(repr(ack_status)))
    print()
    print("send standard command")
    ack_status = command_client.send_standard_command(
        acu.CMD_MODE, acu.SUBSYSTEM_EL, acu.CMD_INACTIVE, 0, 0
    )
    print("ACK: {}".format(repr(ack_status)))
    print()

    print("send hexapod command")
    ack_status = command_client.send_hexapod_command(
        acu.CMD_MODE,
        acu.SUBSYSTEM_HXP,
        acu.CMD_INACTIVE,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
    )
    print("ACK: {}".format(repr(ack_status)))

    command_client.client_close()
