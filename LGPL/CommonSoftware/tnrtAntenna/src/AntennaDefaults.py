# Project: TNRT - Thai National Radio Telescope
# Organization: NARIT - National Astronomical Research Institute of Thailand
# Author: Spiro Sarris
# Date: 2020.11.11

import TypeConverterAntenna
import DataFormatAcu as acu
import socket

time_mjd = 59000.0
ctypes_obj = acu.StatusMessageFrame()
ctypes_obj.tracking_obj_sts.tle_name = bytes(
    "123456789_123456789_12345",
    "ascii",
)
ctypes_obj.tracking_obj_sts.tle_line1 = bytes(
    "123456789_123456789_123456789_123456789_123456789_123456789_123456789_",
    "ascii",
)
ctypes_obj.tracking_obj_sts.tle_line2 = bytes(
    "123456789_123456789_123456789_123456789_123456789_123456789_123456789_",
    "ascii",
)
metadata = TypeConverterAntenna.from_ctype_StatusMessageFrame(time_mjd, ctypes_obj)

zCorrEl0 = 0
zCorrEl1 = 0
zCorrEl2 = 0
zCorrEl3 = 0
xCorrEl0 = 0
xCorrEl1 = 0
xCorrEl2 = 0
xCorrEl3 = 0
yCorrEl0 = 0
yCorrEl1 = 83
yCorrEl2 = 0
yCorrEl3 = 0
txCorrEl0 = 0
txCorrEl1 = 0
txCorrEl2 = 0
txCorrEl3 = 0
tyCorrEl0 = 0
tyCorrEl1 = 0
tyCorrEl2 = 0
tyCorrEl3 = 0
latitude40mN = 18.864348
longitude40mE = 99.216805
height40m = 403.625
onsource_tolerance = 8
tolerance_hxp_linear = 1
tolerance_hxp_rotate = 8

# connection
AcuIP = socket.gethostbyname("acu")
MockAcuIP = socket.gethostbyname("mockAcu")
cmdPort = 9000
stsPort = 9001

flow_control_logging_period = 5  # second
