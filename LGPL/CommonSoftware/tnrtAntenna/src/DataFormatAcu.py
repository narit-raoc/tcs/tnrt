# Project: TNRT - Thai National Radio Telescope
# Organization: NARIT - National Astronomical Research Institute of Thailand
# Author: Spiro Sarris
# Date: 2020.11.05

from ctypes import LittleEndianStructure

# ACU Definitions adapted from MTM file AntComm.h
# Use these definitions to interact with binary command, acknowledge, and status messages of ACU

##
# MT Mechatronics document uses data type specifiers such as 'UINT16, DWORD, ...'
# Create typedef here to translate the type specifier to python ctypes data types
##

from ctypes import c_char as INT8  # // 8 Bit signed
from ctypes import c_short as INT16  # // 16 Bit signed
from ctypes import c_int as INT32  # // 32 Bit signed
from ctypes import c_ubyte as UINT8  # // 8 Bit unsigned
from ctypes import c_ushort as UINT16  # // 16 Bit unsigned
from ctypes import c_uint as UINT32  # // 32 Bit unsigned
from ctypes import c_ushort as WORD  # // 16 Bit unsigned, used for in bit coded data
from ctypes import c_uint as DWORD  # // 32 Bit unsigned used for in bit mode coded data
from ctypes import c_float as REAL32  # // 32 Bit floating point
from ctypes import c_double as REAL64  # // 64 Bit floating point
from ctypes import sizeof

## Values to affect Antenna progrma logic depending on if we connect to MockAcu or real external ACU
ACU_TYPE_MOCK = 0
ACU_TYPE_REMOTE = 1

## Sizes of tracking tables
M1_TRACK_TABLE_LEN_MIN = 5
M1_TRACK_TABLE_LEN_MAX = 50

HXP_TRACK_TABLE_LEN_MIN = 5  # // TODO - function to use this is not implemented yet
HXP_TRACK_TABLE_LEN_MAX = 20  # // TODO - function to use this is not implemented yet

HXP_EL_TABLE_LEN_MIN = 5
HXP_EL_TABLE_LEN_MAX = 50

NORAD_TLE_NAME_LEN = 25
NORAD_TLE_LINE_LEN = 70

# flags
STARTFLAG = UINT32(0x1DFCCF1A)
ENDFLAG = UINT32(0xA1FCCFD1)

CMD_QTY_SINGLE = UINT32(1)
CMD_QTY_ALLAXIS = UINT32(9)

# command source
SRC_LCP = UINT32(1)
SRC_REMOTE = UINT32(2)
SRC_HAND_HELD_PANEL = UINT32(3)
SRC_SECOND_CONTROL_PANEL = UINT32(6)
SRC_TERTIARY_CONTROL_PANEL = UINT32(8)
SRC_ACU_INTERNAL = UINT32(10)

# command type
CMD_MODE = UINT16(1)
CMD_PARAM = UINT16(2)
CMD_SPECIAL = UINT16(3)

# subsystem identifiers
SUBSYSTEM_VOID = UINT16(0)
SUBSYSTEM_AZ = UINT16(1)
SUBSYSTEM_EL = UINT16(2)
SUBSYSTEM_FC = UINT16(4)  # // facilitycontrol
SUBSYSTEM_PO = UINT16(5)  # // Pointing
SUBSYSTEM_TR = UINT16(7)  # // Tracking
SUBSYSTEM_SYSTEM = UINT16(9)
SUBSYSTEM_HXP = UINT16(10)
SUBSYSTEM_M3 = UINT16(20)
SUBSYSTEM_M3R = UINT16(21)
SUBSYSTEM_M4A = UINT16(22)
SUBSYSTEM_M4B = UINT16(23)
SUBSYSTEM_VX = UINT16(24)
SUBSYSTEM_THU = UINT16(25)
SUBSYSTEM_GRS = UINT16(26)

# # /* ------------------------------ MODE COMMANDS -----------------------*/
CMD_INACTIVE = UINT16(2001)
CMD_ACTIVE = UINT16(2002)
CMD_POSITION_ABSOLUTE = UINT16(2003)
CMD_POSITION_RELATIVE = UINT16(2004)
CMD_SLEW = UINT16(2005)
CMD_STOP = UINT16(2007)
CMD_PROGRAM_TRACK = UINT16(2008)
CMD_REFERENCE = UINT16(2009)
CMD_RESET = UINT16(2015)
CMD_MOVE_PREPOSITION = UINT16(2016)
CMD_OPEN = UINT16(2040)
CMD_CLOSE = UINT16(2041)
CMD_STOW = UINT16(2050)
CMD_UNSTOW = UINT16(2051)
CMD_DRIVE_TO_STOW = UINT16(2052)
CMD_STOW_STOP = UINT16(2053)
CMD_DRIVE_TO_PARK = UINT16(2060)

# # /* ------------------------------ PARAMETER COMMANDS -------------------*/

CMD_SET_MASTER = UINT16(3005)
CMD_SET_FAN = UINT16(3006)
CMD_SET_M3SYNC_EL = UINT16(3008)
CMD_POSITION_OFFSET = UINT16(3040)
CMD_POINTING_CORRECTION = UINT16(3041)
CMD_SET_TRACKING_OFFSET_TIME = UINT16(3051)
CMD_SET_USER_POINTING_CORRECTION_AZ = UINT16(3052)
CMD_SET_USER_POINTING_CORRECTION_EL = UINT16(3053)
CMD_TIME_SOURCE = UINT16(3080)
CMD_TIME_OFFSET = UINT16(3081)
CMD_HEXAPOD_ELEVATION_OFFSET_TABLE_ON = UINT16(3060)
CMD_CORRECTION_VALUES_ON = UINT16(3063)
CMD_SET_DUT1_GST0 = UINT16(3068)
CMD_HEXAPOD_POSITION_OFFSET = UINT16(3040)  # // same as CMD_POSITION_OFFSET
CMD_ENABLE_TRACKING = UINT16(3086)
CMD_STOP_TRACKING = UINT16(3087)

# # /* ----  SPECIAL COMMANDS -----------------------*/
CMD_LOAD_PROGRAM_TRACK_TABLE = UINT16(3050)
CMD_LOAD_PROGRAM_OFFSET_TABLE = UINT16(3056)
CMD_LOAD_PROGRAM_TRACK_TABLE_HEXAPOD = UINT16(3057)
CMD_LOAD_ELEVATION_OFFSET_TABLE_HEXAPOD = UINT16(3069)
CMD_SET_POINTING_MODEL = UINT16(3066)
CMD_SET_REFRACTION_CORRECTION = UINT16(3067)
CMD_LOAD_NORAD = UINT16(3073)
CMD_SET_TRACK_MODE = UINT16(3085)


# # /* ----  COMMAND ACKNOWLEDGE (response from ACU) --------------- */
CMD_ACK = UINT32(150)

# # /* ----  static constANT PRE-DEFINED PARAMETERS -------------- */

# # // CMD_MOVE_PREPOSITION param1
PRESET_POS_1 = REAL64(1.0)
PRESET_POS_2 = REAL64(2.0)
PRESET_POS_3 = REAL64(3.0)
PRESET_POS_4 = REAL64(4.0)
PRESET_POS_5 = REAL64(5.0)
PRESET_POS_6 = REAL64(6.0)
PRESET_POS_7 = REAL64(7.0)
PRESET_POS_8 = REAL64(8.0)
PRESET_POS_9 = REAL64(9.0)
PRESET_POS_10 = REAL64(10.0)

# # // CMD_DRIVE_TO_STOW param1
CMD_DRIVE_TO_STOW_NEXT = REAL64(0.0)
CMD_DRIVE_TO_STOW_1 = REAL64(1.0)
CMD_DRIVE_TO_STOW_2 = REAL64(2.0)

# # //CMD_STOW, CMD_UNSTOW, CMD_STOW_STOP} param1STOW
STOWPIN_ONLY1 = REAL64(1.0)
STOWPIN_ONLY2 = REAL64(2.0)
STOWPIN_BOTH = REAL64(3.0)

# // CMD_SET_MASTER param1
CMD_SET_MASTER_LCP = REAL64(1.0)
CMD_SET_MASTER_REMOTE = REAL64(2.0)
CMD_SET_MASTER_HHP = REAL64(3.0)  # used by HHP directly.  Not by remote command
CMD_SET_MASTER_SCP = REAL64(4.0)
CMD_SET_MASTER_TCP = REAL64(5.0)

# // CMD_SET_M3SYNC_EL param1
CMD_SET_MThreesyncEl_INACTIVE = REAL64(0.0)
CMD_SET_MThreesyncEl_ACTIVE = REAL64(1.0)

# // CMD_POINTING_CORRECTION param1
CMD_POINTING_CORRECTION_OFF = REAL64(0.0)
CMD_POINTING_CORRECTION_ON = REAL64(1.0)

# // CMD_TIME_SOURCE param1
CMD_TIME_SOURCE_INTERNAL = REAL64(1.0)
CMD_TIME_SOURCE_NTP = REAL64(2.0)
CMD_TIME_SOURCE_ABSOLUTE = REAL64(3.0)

# // CMD_TIME_OFFSET param1
CMD_TIME_OFFSET_PLUS1SEC = REAL64(1.0)
CMD_TIME_OFFSET_MINUS1SEC = REAL64(2.0)
CMD_TIME_OFFSET_ABSOLUTE = REAL64(3.0)
CMD_TIME_OFFSET_RELATIVE = REAL64(4.0)

# // CMD_SET_FAN param1
CMD_SET_FAN_OFF = REAL64(0.0)
CMD_SET_FAN_ON = REAL64(1.0)

# // CMD_HEXAPOD_ELEVATION_OFFSET_TABLE_ON param1
CMD_HEXAPOD_ELEVATION_OFFSET_TABLE_ON_DISABLE = REAL64(0.0)
CMD_HEXAPOD_ELEVATION_OFFSET_TABLE_ON_ENABLE = REAL64(1.0)

# // CMD_CORRECTION_VALUES_ON param1
CMD_CORRECTION_VALUES_ON_INCLINOMETER = REAL64(1.0)
CMD_CORRECTION_VALUES_ON_REFRACTION = REAL64(2.0)
CMD_CORRECTION_VALUES_ON_PMODEL = REAL64(3.0)

# // CMD_CORRECTION_VALUES_ON param2
CMD_CORRECTION_VALUES_ON_DISABLE = REAL64(0.0)
CMD_CORRECTION_VALUES_ON_ENABLE = REAL64(1.0)

# // CMD_HEXAPOD_POSITION_OFFSET param1
CMD_HEXAPOD_POSITION_OFFSET_X = REAL64(1.0)
CMD_HEXAPOD_POSITION_OFFSET_Y = REAL64(2.0)
CMD_HEXAPOD_POSITION_OFFSET_Z = REAL64(3.0)
CMD_HEXAPOD_POSITION_OFFSET_TX = REAL64(4.0)
CMD_HEXAPOD_POSITION_OFFSET_TY = REAL64(5.0)
CMD_HEXAPOD_POSITION_OFFSET_TZ = REAL64(6.0)

# // CMD_ENABLE_TRACKING param1
CMD_ENABLE_TRACKING_DISABLE_AND_DEACTIVATE = REAL64(0.0)
CMD_ENABLE_TRACKING_ENABLE_AND_ACTIVATE = REAL64(1.0)

# // CMD_STOP_TRACKING
# parameters not used

# // CMD_LOAD_PROGRAM_TRACK_TABLE param Load Mode
CMD_LOAD_PROGRAM_TRACK_LOAD_MODE_NEW = UINT16(1)
CMD_LOAD_PROGRAM_TRACK_LOAD_MODE_ADD = UINT16(2)
CMD_LOAD_PROGRAM_TRACK_LOAD_MODE_RESET = UINT16(3)

# // CMD_LOAD_PROGRAM_TRACK_TABLE param Interpolation Type
CMD_LOAD_PROGRAM_TRACK_INTERPOLATION_MODE_NEWTON = UINT16(0)
CMD_LOAD_PROGRAM_TRACK_INTERPOLATION_MODE_SPLINE = UINT16(1)

# // CMD_LOAD_PROGRAM_TRACK_TABLE param Track Mode
CMD_LOAD_PROGRAM_TRACK_TRACK_MODE_AZEL = UINT16(1)
CMD_LOAD_PROGRAM_TRACK_TRACK_MODE_RADEC = UINT16(2)
CMD_LOAD_PROGRAM_TRACK_TRACK_MODE_RADECSHORTCUT = UINT16(3)

# CMD_SET_TRACK_MODE parameter
CMD_SET_TRACK_MODE_TLE = UINT16(1)
CMD_SET_TRACK_MODE_PROGRAM_TRACK = UINT16(2)
CMD_SET_TRACK_MODE_SUN_TRACK = UINT16(4)
CMD_SET_TRACK_MODE_STAR_TRACK = UINT16(5)

# // CMD_ACK param Status
CMD_ACK_STS_NO_COMMAND = UINT32(0)
CMD_ACK_STS_DONE = UINT32(1)
CMD_ACK_STS_WRONG_MODE = UINT32(4)
CMD_ACK_STS_PARAM_ERR = UINT32(5)
CMD_ACK_STS_WRONG_LEN = UINT32(6)
CMD_ACK_STS_UNDEF = UINT32(7)
CMD_ACK_STS_ACCEPTED = UINT32(9)
CMD_ACK_STS_TIMEOUT = UINT32(10)

# // AXIS STATUS param Axis State
AXIS_INACTIVE = UINT16(0)
AXIS_DEACTIVATING = UINT16(1)
AXIS_ACTIVATING = UINT16(2)
AXIS_ACTIVE = UINT16(3)

# // AXIS STATUS param Axis Trajectory State (not used in Acu.cpp?)
TRAJECTORY_OFF = UINT16(0)
TRAJECTORY_HOLDING = UINT16(1)
TRAJECTORY_EMERGENCY_STOP = UINT16(2)
TRAJECTORY_STOP = UINT16(3)
TRAJECTORY_SLEWING_VELOCITY = UINT16(4)
TRAJECTORY_POSITION = UINT16(6)
TRAJECTORY_TRACKING = UINT16(7)

# // AXIS STATUS param Axis Actual Mode (not used in Acu.cpp?)
MODE_IGNORE = UINT16(2000)
MODE_INACTIVE = UINT16(2001)
MODE_ACTIVE = UINT16(2002)
MODE_ABS_POS = UINT16(2003)
MODE_REL_POS = UINT16(2004)
MODE_SLEW = UINT16(2005)
MODE_STOP = UINT16(2007)
MODE_TRACKING_CONTROLLER = UINT16(2008)
MODE_SUN_TRACK = UINT16(2009)
MODE_INTERLOCK = UINT16(2014)
MODE_RESET = UINT16(2015)
MODE_SYNC_WITH_EL = UINT16(2030)
MODE_STOW = UINT16(2050)
MODE_UNSTOW = UINT16(2051)
MODE_DRIVE_TO_STOW = UINT16(2052)
MODE_STOP_STOW_PIN = UINT16(2053)
MODE_DRIVE_TO_PARK = UINT16(2060)

# // TRACKING STATUS param actTrObj
TRACKING_OBJECT_INVALID = UINT16(0)
TRACKING_OBJECT_VALID = UINT16(1)
TRACKING_OBJECT_SUN = UINT16(1000)


# // Push the value of the current byte alignment on the internal stack and then


class AbstractAcuStruct(LittleEndianStructure):
    """sets the alignment to 1 byte for structs that inherit this class.
    If not set to 1, some data types will be aligned to 4-byte or 8-byte boundaries
    """

    _pack_ = 1

    def __repr__(self):
        return "<{} {}>".format(
            self.__class__.__name__,
            ", ".join(
                ["{} = {}".format(key, getattr(self, key)) for key, _ in self._fields_]
            ),
        )


# /* ---------- Headers  ------------------------------------*/
# // Packet header.  Used for all commands
class PacketHeader(AbstractAcuStruct):
    """16 bytes"""

    _fields_ = [
        ("message_length", UINT32),
        ("source", UINT32),
        ("command_serial_number", UINT32),
        ("number_of_commands", UINT32),
    ]


# // Command message header. common for all types of commands
class CommandHeader(AbstractAcuStruct):
    """6 bytes"""

    _fields_ = [
        ("command_type", UINT16),
        ("subsystem_id", UINT16),
        ("command_id", UINT16),
    ]


# /* ---------- Parameter Types -----------------------------*/
class TimeAzRaElDec(AbstractAcuStruct):
    """24 bytes"""

    _fields_ = [("time", REAL64), ("az_ra", REAL64), ("el_dec", REAL64)]


class ElPos(AbstractAcuStruct):
    """28 bytes"""

    _fields_ = [
        ("el", REAL32),
        ("x", REAL32),
        ("y", REAL32),
        ("z", REAL32),
        ("tx", REAL32),
        ("ty", REAL32),
        ("tz", REAL32),
    ]


class TimePos(AbstractAcuStruct):
    """56 bytes"""

    _fields_ = [
        ("time", REAL64),
        ("x", REAL64),
        ("y", REAL64),
        ("z", REAL64),
        ("tx", REAL64),
        ("ty", REAL64),
        ("tz", REAL64),
    ]


class ParameterStandard(AbstractAcuStruct):
    """16 bytes"""

    _fields_ = [
        ("param1", REAL64),
        ("param2", REAL64),
    ]


class ParameterHexapod(AbstractAcuStruct):
    """96 bytes"""

    _fields_ = [
        ("param1", REAL64),
        ("param2", REAL64),
        ("param3", REAL64),
        ("param4", REAL64),
        ("param5", REAL64),
        ("param6", REAL64),
        ("param7", REAL64),
        ("param8", REAL64),
        ("param9", REAL64),
        ("param10", REAL64),
        ("param11", REAL64),
        ("param12", REAL64),
    ]


class ParameterTrackTable(AbstractAcuStruct):
    """1210 bytes"""

    _fields_ = [
        ("object_id", UINT16),
        ("load_mode", UINT16),
        ("interpolation_mode", UINT16),
        ("tracking_mode", UINT16),
        ("sequence_length", UINT16),
        ("time_az_el_sequence", TimeAzRaElDec * M1_TRACK_TABLE_LEN_MAX),
    ]


class ParameterTrackTableHexapod(AbstractAcuStruct):
    """1126 bytes"""

    _fields_ = [
        ("load_mode", UINT16),
        ("interpolation_mode", UINT16),
        ("sequence_length", UINT16),
        ("pos_sequence", TimePos * HXP_TRACK_TABLE_LEN_MAX),
    ]


class ParameterElevationOffsetTable(AbstractAcuStruct):
    """1402 bytes"""

    _fields_ = [
        ("sequence_length", UINT16),
        ("pos_sequence", ElPos * HXP_EL_TABLE_LEN_MAX),
    ]


class ParameterPmodel(AbstractAcuStruct):
    """72 bytes"""

    _fields_ = [("p", REAL64 * 9)]


class ParameterRefraction(AbstractAcuStruct):
    """24 bytes"""

    _fields_ = [
        ("r0", REAL64),
        ("b1", REAL64),
        ("b2", REAL64),
    ]


class ParameterNORAD(AbstractAcuStruct):
    """167 bytes"""

    _fields_ = [
        ("object_id", UINT16),
        ("tle_name", INT8 * NORAD_TLE_NAME_LEN),
        ("tle_line1", INT8 * NORAD_TLE_LINE_LEN),
        ("tle_line2", INT8 * NORAD_TLE_LINE_LEN),
    ]


class ParameterSetTrackMode(AbstractAcuStruct):
    """26 bytes"""

    _fields_ = [
        ("tracking_mode", UINT16),
        ("not_used1", REAL64),
        ("not_used2", REAL64),
        ("not_used3", REAL64),
    ]


# /* ---------- Complete packet data types ---------------------*/
class CommandPacketStandard(AbstractAcuStruct):
    """46 bytes"""

    _fields_ = [
        ("start_flag", UINT32),
        ("phdr", PacketHeader),
        ("chdr", CommandHeader),
        ("params", ParameterStandard),
        ("end_flag", UINT32),
    ]


class CommandPacketHexapod(AbstractAcuStruct):
    """126 bytes"""

    _fields_ = [
        ("start_flag", UINT32),
        ("phdr", PacketHeader),
        ("chdr", CommandHeader),
        ("params", ParameterHexapod),
        ("end_flag", UINT32),
    ]


class CommandPacketTrackTable(AbstractAcuStruct):
    """1240 bytes"""

    _fields_ = [
        ("start_flag", UINT32),
        ("phdr", PacketHeader),
        ("chdr", CommandHeader),
        ("params", ParameterTrackTable),
        ("end_flag", UINT32),
    ]


class CommandPacketTrackTableHexapod(AbstractAcuStruct):
    """1156 bytes"""

    _fields_ = [
        ("start_flag", UINT32),
        ("phdr", PacketHeader),
        ("chdr", CommandHeader),
        ("params", ParameterTrackTableHexapod),
        ("end_flag", UINT32),
    ]


class CommandPacketElevationOffsetTable(AbstractAcuStruct):
    """1432 bytes"""

    _fields_ = [
        ("start_flag", UINT32),
        ("phdr", PacketHeader),
        ("chdr", CommandHeader),
        ("params", ParameterElevationOffsetTable),
        ("end_flag", UINT32),
    ]


class CommandPacketPointingModel(AbstractAcuStruct):
    """102 bytes"""

    _fields_ = [
        ("start_flag", UINT32),
        ("phdr", PacketHeader),
        ("chdr", CommandHeader),
        ("params", ParameterPmodel),
        ("end_flag", UINT32),
    ]


class CommandPacketRefraction(AbstractAcuStruct):
    """54 bytes"""

    _fields_ = [
        ("start_flag", UINT32),
        ("phdr", PacketHeader),
        ("chdr", CommandHeader),
        ("params", ParameterRefraction),
        ("end_flag", UINT32),
    ]


class CommandPacketNORAD(AbstractAcuStruct):
    """197 bytes"""

    _fields_ = [
        ("start_flag", UINT32),
        ("phdr", PacketHeader),
        ("chdr", CommandHeader),
        ("params", ParameterNORAD),
        ("end_flag", UINT32),
    ]


class CommandPacketSetTrackMode(AbstractAcuStruct):
    """42 bytes"""

    _fields_ = [
        ("start_flag", UINT32),
        ("phdr", PacketHeader),
        ("chdr", CommandHeader),
        ("params", ParameterSetTrackMode),
        ("end_flag", UINT32),
    ]


# // Special packet to send mode command (STOP, RESET, ACTIVATE, DEACTIVATE)
# // to all 9 axes in one TCP/IP socket message.  Should work for
# // AZ, EL, M3, M3R, M4A, M4B, THU, GRS, HXP.  Not for Vertex shutter axis.
# // Benefit of this message is that we will only have to wait for ACK response
# // from ACU 1 * 350ms.  Not 8 * 350 ms
class CommandPacketAllAxis(AbstractAcuStruct):
    _fields_ = [
        ("start_flag", UINT32),
        ("phdr", PacketHeader),
        ("chdr", CommandHeader),
        ("params", ParameterStandard),
        ("chdr_ax2", CommandHeader),
        ("params_ax2", ParameterStandard),
        ("chdr_ax3", CommandHeader),
        ("params_ax3", ParameterStandard),
        ("chdr_ax4", CommandHeader),
        ("params_ax4", ParameterStandard),
        ("chdr_ax5", CommandHeader),
        ("params_ax5", ParameterStandard),
        ("chdr_ax6", CommandHeader),
        ("params_ax6", ParameterStandard),
        ("chdr_ax7", CommandHeader),
        ("params_ax7", ParameterStandard),
        ("chdr_ax8", CommandHeader),
        ("params_ax8", ParameterStandard),
        ("chdr_hxp", CommandHeader),
        ("params_hxp", ParameterHexapod),
        ("end_flag", UINT32),
    ]


# /* ---------------- [ ACK format] ------------------------------*/


class Acknowledge(AbstractAcuStruct):
    _fields_ = [
        ("start_flag", UINT32),
        ("message_length", UINT32),
        ("source", UINT32),
        ("subsystem_id", UINT32),
        ("command", UINT32),
        ("command_serial_number", UINT32),
        ("command_identifier", UINT32),
        ("status", UINT32),
        ("end_flag", UINT32),
    ]


# /* ---------------- [ Status Message formats] ------------------*/


class GeneralStatus(AbstractAcuStruct):
    """139 bytes"""

    _fields_ = [
        ("version", UINT16),
        ("diagnosis_signal", REAL64),
        ("facility_control_status", DWORD),
        ("power_status", DWORD),
        ("operation_status", DWORD),
        ("system_warnings", DWORD),
        ("software_interlock", DWORD),
        ("emergency_stops", DWORD),
        ("safety_device_errors", DWORD),
        ("safety_device_errors_aux", DWORD),
        ("communication_status", DWORD),
        ("control_mode_status", DWORD),
        ("actual_time", REAL64),
        ("actual_time_offset", REAL64),
        ("time_source", UINT16),
        ("no_pps_signal", UINT8),
        ("clock_online", UINT8),
        ("clock_ok", UINT8),
        ("wind_speed", REAL32),
        ("temperature_M1", REAL32 * 16),
    ]


# Bit encoding for GeneralStatus.operation_status
SO_SYSSIM = 1 << 0

# Bit encoding for GeneralStatus.control_mode_status
CONTROL_MODE_STATUS_LCP_STS_CONNECT = 1 << 0
CONTROL_MODE_STATUS_LCP_CMD_CONNECT = 1 << 1
CONTROL_MODE_STATUS_SCP_STS_CONNECT = 1 << 2
CONTROL_MODE_STATUS_SCP_CMD_CONNECT = 1 << 3
CONTROL_MODE_STATUS_TCP_STS_CONNECT = 1 << 4
CONTROL_MODE_STATUS_TCP_CMD_CONNECT = 1 << 5
CONTROL_MODE_STATUS_REMOTE_STS_CONNECT = 1 << 6
CONTROL_MODE_STATUS_REMOTE_CMD_CONNECT = 1 << 7
CONTROL_MODE_STATUS_HHP_C1_CONNECT = 1 << 20


# /* System Status*/
class SystemStatus(AbstractAcuStruct):
    """4 bytes"""

    _fields_ = [("bit_status", DWORD)]


# /* Vertex Shutter Status*/
class VertexShutterStatus(AbstractAcuStruct):
    """10 bytes"""

    _fields_ = [
        ("status", UINT16),
        ("elements_open", WORD),
        ("elements_close", WORD),
        ("command_elements_open", WORD),
        ("command_elements_close", WORD),
    ]


# /* Axis Status ref 6.5*/
class AxisStatus(AbstractAcuStruct):
    """138 bytes"""
    # Important NOTE: (SS. 03/2023)
    # Follwing ACU status message change on 16/03/2023:
    # [1] "encoder_position_incl_pcorr": Directly from Encoder.  If pointing corrections are non-zero, 
    # that means the user wants to pre-distort the movement to compensate for mechanical alignment, structure bending, 
    # and atmosphere refraction that we expect to happen at the commanded angle position.  The pointing corrections
    # are calculated using models and mesurement data from "on sky" observations.  The Encoder is a device
    # located at each axis of the antenna structure.  It does not know about the complete structure of antenna 
    # or the atmosphere refraction.  Therefore, it can only report it's own position -- which has already been
    # pre-distorted by pointing corrections.
    
    # [2] "actual_position":  IF the pointing models and resulting pointing corrections were calculated correctly
    # and used correctly during data collection, the commanded pointing corrections will be canceled out byrefraction
    # and bending of structure.  The result is the "actual position" of the optical axis of the telescope.
    # This is the number that we should use to understand where the telescope is looking "on sky" because
    # non-ideal effects have been removed.  NOTE: If pointing correction data is not accurate,
    # the "actual position" will not indicate the angle of the optical axis of telescope.
    _fields_ = [
        ("bit_sts", DWORD),
        ("stow_status", DWORD),
        ("error_status", DWORD),
        ("warning_status", DWORD),
        ("encoder_disc_status", DWORD),
        ("state", UINT16),
        ("trajectory_state", UINT16),
        ("actual_mode", UINT16),
        ("desired_position", REAL64),
        ("trajectory_generator_position", REAL64),
        ("encoder_position_incl_pcorr", REAL64),
        ("actual_position", REAL64),  
        ("current_position_offset", REAL64),
        ("position_deviation", REAL64),
        ("filtered_position_deviation", REAL64),
        ("current_pointing_correction", REAL64),
        ("external_position_offset", REAL64),
        ("trajectory_generator_velocity", REAL64),
        ("desired_velocity", REAL64),
        ("current_velocity", REAL64),
        ("trajectory_generator_acceleration", REAL64),
        ("motor_selection", WORD),
        ("brakes_selection", WORD),
        ("brakes_open", WORD),
        ("brakes_command", WORD),
    ]


# Bit encoding for AxisStatus.bit_sts
ABS_INIT_OK = 1 << 0
ABS_SIM = 1 << 1
ABS_RESET = 1 << 2
ABS_NOT_STOP = 1 << 3
ABS_BEREIT = 1 << 4
ABS_MOT_EIN = 1 << 5
ABS_PRE_AZ = 1 << 6
ABS_LOCKAT = 1 << 7
ABS_OVERRIDE = 1 << 8
ABS_POS_OFFSET = 1 << 9
ABS_ACTIVE = 1 << 10
ABS_PARK_POS = 1 << 11
ABS_POINT_CORR = 1 << 12
ABS_PREAZ_OK = 1 << 13
ABS_INACTIVE = 1 << 14
ABS_DEACTIVATE = 1 << 15
ABS_ACTIVATE = 1 << 16
ABS_SYNC_ACTIVE = 1 << 17
ABS_SYNC_OK = 1 << 18
# ...
ABS_OFFSET_EXTERN = 1 << 22
ABS_TIMER_ACTIVATED = 1 << 23
ABS_CONF_OK = 1 << 24
# ...
ABS_SECTOR_B = 1 << 30
ABS_SECTOR_A = 1 << 31

# Bit encoding for AxisStatus.stow_status
STOW_POSOK = 1 << 0


# /* Motor Status ref 6.6*/
class MotorStatus(AbstractAcuStruct):
    """58 bytes"""

    _fields_ = [
        ("bit_sts", DWORD),
        ("actual_position", REAL64),
        ("actual_velocity", REAL64),
        ("actual_torque", REAL64),
        ("utilization_rate", REAL64),
        ("motor_temperature", REAL32),
        ("motor_overload_inverter", REAL32),
        ("motor_overload_motor", REAL32),
        ("motor_error_instance", WORD),
        ("motor_error_reaction", WORD),
        ("motor_error_module", WORD),
        ("motor_error_code", WORD),
        ("limit_down", UINT8),
        ("limit_up", UINT8),
    ]


# /* Tracking Status */
class TrackingStatus(AbstractAcuStruct):
    """358 bytes"""

    _fields_ = [
        ("bit_status", DWORD),
        ("prog_track_bs", DWORD),
        ("prog_track_act_index", UINT32),
        ("prog_track_end_index", UINT32),
        ("prog_offset_bs", DWORD),
        ("prog_offset_act_index", UINT32),
        ("prog_offset_end_index", UINT32),
        ("sun_track_az", REAL32),
        ("sun_track_el", REAL32),
        ("prog_track_az", REAL32),
        ("prog_track_el", REAL32),
        ("star_track_az", REAL32),
        ("star_track_el", REAL32),
        ("tle_track_az", REAL32),
        ("tle_track_el", REAL32),
        ("prog_offset_az", REAL64),
        ("prog_offset_el", REAL64),
        ("position_offset_az", REAL64),
        ("position_offset_el", REAL64),
        ("time_offset", REAL32),
        ("desired_az", REAL64),
        ("desired_el", REAL64),
        ("antenna_latitude", REAL64),
        ("antenna_longitude", REAL64),
        ("antenna_altitude", REAL64),
        ("inclinometer1_x", REAL64),
        ("inclinometer1_y", REAL64),
        ("inclinometer1_temp", REAL64),
        ("inclinometer2_x", REAL64),
        ("inclinometer2_y", REAL64),
        ("inclinometer2_temp", REAL64),
        ("inclinometer_model_correction_el", REAL64),
        ("p1", REAL64),
        ("p2", REAL64),
        ("p3", REAL64),
        ("p4", REAL64),
        ("p5", REAL64),
        ("p6", REAL64),
        ("p7", REAL64),
        ("p8", REAL64),
        ("p9", REAL64),
        ("pointing_model_correction_az", REAL64),
        ("pointing_model_correction_el", REAL64),
        ("r0", REAL64),
        ("b1", REAL64),
        ("b2", REAL64),
        ("refraction_model_correction_el", REAL64),
        ("user_fine_pointing_correction_az", REAL64),
        ("user_fine_pointing_correction_el", REAL64),
        ("total_pointing_correction_az", REAL64),
        ("total_pointing_correction_el", REAL64),
        ("total_pointing_correction_grs", REAL64),
        ("dut1", INT16),
        ("gst0hut", UINT32),
    ]


# Bit encoding for TrackingStatus.bit_status
TR_INIT_OK = 1 << 0
TR_ERROR_ACTIVE = 1 << 1
TR_OBJ_VALID = 1 << 2
TR_OBJ_ACTIVE = 1 << 3
TR_OBJ_ENABLED = 1 << 4
TR_OBJ_STOP = 1 << 5
TR_OBJ_VISIBLE = 1 << 6
TR_OBJ_RUN = 1 << 7
TR_NOT_USED_8 = 1 << 8
TR_NOT_USED_9 = 1 << 9
TR_POS_OFFSET_SET = 1 << 10
TR_PROG_OFFSET_ACTIVE = 1 << 11
TR_SUN_TRACK_ACTIVE = 1 << 12
TR_PROG_TRACK_ACTIVE = 1 << 13
TR_TLE_TRACK_ACTIVE = 1 << 14
TR_STAR_TRACK_ACTIVE = 1 << 15
TR_NOT_USED_16 = 1 << 16
TR_NOT_USED_17 = 1 << 17
TR_NOT_USED_18 = 1 << 18
TR_NOT_USED_19 = 1 << 19
TR_TLE_NO_PATH = 1 << 20
TR_TLE_PATH_SEARCH = 1 << 21
TR_STAR_TRACK_ERROR = 1 << 22
TR_NOT_USED_23 = 1 << 23
TR_NOT_USED_24 = 1 << 24
TR_NOT_USED_25 = 1 << 25
TR_NOT_USED_26 = 1 << 26
TR_NOT_USED_27 = 1 << 27
TR_INCLINOMETER_CORRECTION_ENABLED = 1 << 28
TR_REFRACTION_CORRECTION_ENABLED = 1 << 29
TR_POINTING_MODEL_CORRECTION_ENABLED = 1 << 30
TR_NOT_USED_31 = 1 << 31

# Bit encoding for TrackingStatus.prog_track_bs and TrackingStatus.prog_offset_bs
PTS_INIT_OK = 1 << 0
PTS_DATA_VALID = 1 << 1
PTS_ERROR = 1 << 2
PTS_STARTPOS = 1 << 3
PTS_RUNNING = 1 << 4
PTS_COMPLETED = 1 << 5
PTS_TIMEDIST_FAULT = 1 << 6
PTS_INTERPOLATION_ERROR = 1 << 7
PTS_NEWTON = 1 << 8
PTS_SPLINE = 1 << 9
PTS_STEP = 1 << 10
PTS_LAGRANGE = 1 << 11
PTS_AZ_EL = 1 << 12
PTS_RA_DEC = 1 << 13
PTS_RA_DEC_SHORT = 1 << 14
PTS_RADEC_CONVERT_FAULT = 1 << 15

# /*Hexapod Status*/
class GeneralHexapodStatus(AbstractAcuStruct):
    """4 bytes"""

    _fields_ = [("general_hexapod_status", DWORD)]


class HexapodStatus(AbstractAcuStruct):
    """186 bytes"""

    _fields_ = [
        ("bit_sts", DWORD),
        ("bit_error", DWORD),
        ("bit_warning", DWORD),
        ("state", UINT16),
        ("spindle_select", WORD),
        ("mode", UINT16),
        ("pos_desired", REAL32 * 6),
        ("velocity_desired", REAL32 * 6),
        ("pos_trajectory", REAL32 * 6),
        ("velocity_trajectory", REAL32 * 6),
        ("offset", REAL32 * 6),
        ("pos_actual", REAL32 * 6),
        ("velocity_actual", REAL32 * 6),
    ]


# /* Hexapod Spindle Status */
class SpindleStatus(AbstractAcuStruct):
    """52 bytes"""

    _fields_ = [
        ("motor_sts", DWORD),
        ("motor_temperature", REAL32),
        ("motor_temperature_inverter", REAL32),
        ("motor_overload_motor", REAL32),
        ("motor_error_instance", WORD),
        ("motor_error_reaction", WORD),
        ("motor_error_module", WORD),
        ("motor_error_code", WORD),
        ("upper_limit_switch", UINT8),
        ("lower_limit_switch", UINT8),
        ("actual_torque", REAL32),
        ("rate_loop", WORD),
        ("actual_velocity", REAL32),
        ("spindle_sts", DWORD),
        ("spindle_errors", DWORD),
        ("spindle_warnings", DWORD),
        ("actual_position", REAL32),
    ]


# /* Hexapod Tracking Status */
class HexapodTrackStatus(AbstractAcuStruct):
    """68 bytes"""

    _fields_ = [
        ("bit_sts", DWORD),
        ("start_time", REAL64),
        ("actual_index", UINT32),
        ("end_index", UINT32),
        ("track_mode_actual_position", REAL64 * 6),
    ]


# /* Tracking Object Status */
class TrackingObjectStatus(AbstractAcuStruct):
    """289 bytes"""

    _fields_ = [
        ("prog_track_mode", UINT16),
        ("time_offset", REAL32),
        ("tracking_object_position_offset_az", REAL32),
        ("tracking_object_position_offset_el", REAL32),
        ("tle_name", INT8 * NORAD_TLE_NAME_LEN),
        ("tle_line1", INT8 * NORAD_TLE_LINE_LEN),
        ("tle_line2", INT8 * NORAD_TLE_LINE_LEN),
        ("pt_interpolation_mode", UINT16),
        ("pt_track_mode", UINT16),
        ("pt_end_index", UINT16),
        ("pt_start_time", REAL64),
        ("pt_table11", REAL64),
        ("pt_table12", REAL64),
        ("pt_table21", REAL64),
        ("pt_table22", REAL64),
        ("po_interpolation_mode", UINT16),
        ("po_track_mode", UINT16),
        ("po_end_index", UINT16),
        ("po_start_time", REAL64),
        ("po_table11", REAL64),
        ("po_table12", REAL64),
        ("po_table21", REAL64),
        ("po_table22", REAL64),
        ("pt_star_track_epoch", UINT16),
        ("pt_star_track_ra", REAL64),
        ("pt_star_track_dec", REAL64),
    ]


# /* Status Message */


class StatusMessageFrame(AbstractAcuStruct):
    """3340 bytes"""

    _fields_ = [
        ("start_flag", DWORD),
        ("status_message_type", UINT16),
        ("message_length", UINT32),
        ("status_message_counter", UINT32),
        ("general_sts", GeneralStatus),
        ("system_sts", SystemStatus),
        ("vertexshutter_sts", VertexShutterStatus),
        ("axis_status", AxisStatus * 8),
        ("az_msts", MotorStatus * 4),
        ("el_msts", MotorStatus * 4),
        ("m3_msts", MotorStatus * 2),
        ("m3r_msts", MotorStatus),
        ("m4a_msts", MotorStatus),
        ("m4b_msts", MotorStatus),
        ("thu_msts", MotorStatus * 2),
        ("grs_msts", MotorStatus),
        ("tracking_sts", TrackingStatus),
        ("general_hexapod_status", GeneralHexapodStatus),
        ("hexapod_sts", HexapodStatus),
        ("spindle_sts", SpindleStatus * 6),
        ("hexapod_track_sts", HexapodTrackStatus),
        ("tracking_obj_sts", TrackingObjectStatus),
        ("end_flag", DWORD),
    ]


# Lists used by MockAcu to determine how many bytes to read from socket for each parameter type.
# Must use primitive types in the list so they can be found in command such as "if (x in list)"
list_ParameterStandard = [
    CMD_INACTIVE.value,
    CMD_ACTIVE.value,
    CMD_POSITION_ABSOLUTE.value,
    CMD_POSITION_RELATIVE.value,
    CMD_SLEW.value,
    CMD_STOP.value,
    CMD_PROGRAM_TRACK.value,
    CMD_REFERENCE.value,
    CMD_RESET.value,
    CMD_MOVE_PREPOSITION.value,
    CMD_OPEN.value,
    CMD_CLOSE.value,
    CMD_STOW.value,
    CMD_UNSTOW.value,
    CMD_DRIVE_TO_STOW.value,
    CMD_STOW_STOP.value,
    CMD_DRIVE_TO_PARK.value,
    CMD_SET_MASTER.value,
    CMD_SET_FAN.value,
    CMD_SET_M3SYNC_EL.value,
    CMD_POSITION_OFFSET.value,
    CMD_POINTING_CORRECTION.value,
    CMD_SET_TRACKING_OFFSET_TIME.value,
    CMD_SET_USER_POINTING_CORRECTION_AZ.value,
    CMD_SET_USER_POINTING_CORRECTION_EL.value,
    CMD_TIME_SOURCE.value,
    CMD_TIME_OFFSET.value,
    CMD_HEXAPOD_ELEVATION_OFFSET_TABLE_ON.value,
    CMD_CORRECTION_VALUES_ON.value,
    CMD_SET_DUT1_GST0.value,
    CMD_HEXAPOD_POSITION_OFFSET.value,
    CMD_ENABLE_TRACKING.value,
    CMD_STOP_TRACKING.value,
]

list_ParameterTrackTable = [
    CMD_LOAD_PROGRAM_TRACK_TABLE.value,
    CMD_LOAD_PROGRAM_OFFSET_TABLE.value,
]

list_ParameterTrackTableHexapod = [CMD_LOAD_PROGRAM_TRACK_TABLE_HEXAPOD.value]

list_ParameterElevationOffsetTable = [CMD_LOAD_ELEVATION_OFFSET_TABLE_HEXAPOD.value]

list_ParameterPmodel = [CMD_SET_POINTING_MODEL.value]

list_ParameterRefraction = [CMD_SET_REFRACTION_CORRECTION.value]

list_ParameterNORAD = [CMD_LOAD_NORAD.value]

list_ParameterSetTrackMode = [CMD_SET_TRACK_MODE.value]

if __name__ == "__main__":
    print("sizeof(AxisStatus): {}".format(sizeof(AxisStatus)))
    print("sizeof(TrackingStatus): {}".format(sizeof(TrackingStatus)))
    
    print("sizeof(StatusMessageFrame): {}".format(sizeof(StatusMessageFrame)))
