# Project: TNRT - Thai National Radio Telescope
# Organization: NARIT - National Astronomical Research Institute of Thailand
# Author: Spiro Sarris

# standard packages
import logging
import threading
import socket
import ctypes
import time
import numpy as np

from Supplier import Supplier

from astropy import units as units
from astropy.coordinates import SkyCoord
from astropy.coordinates import EarthLocation
from astropy.coordinates import AltAz
from astropy.time import Time
from astropy.coordinates import get_body
try:
    # For Python < 3.7, import erfa from astropy
    from astropy import _erfa as erfa
except:
    # Python >= 3.7, import erfa from pyerfa package
    # https://pypi.org/project/pyerfa/
    import erfa


# ACS
from Acspy.Servants.ACSComponent import ACSComponent
from Acspy.Servants.ContainerServices import ContainerServices
from Acspy.Servants.ComponentLifecycle import ComponentLifecycle
from Acspy.Servants.ComponentLifecycle import ComponentLifecycleException
from AcsutilPy.ACSPorts  import getIP
import ACSErr

# ACS Component
import tnrtAntennaMod__POA
import tnrtAntennaMod
import tnrtAntennaErrorImpl
import tnrtAntennaError

# shortcuts from IDL module
from tnrtAntennaMod import AZ as AZ
from tnrtAntennaMod import EL as EL
from tnrtAntennaMod import PO as PO
from tnrtAntennaMod import TR as TR
from tnrtAntennaMod import HXP as HXP
from tnrtAntennaMod import M3 as M3
from tnrtAntennaMod import M3R as M3R
from tnrtAntennaMod import M4A as M4A
from tnrtAntennaMod import M4B as M4B
from tnrtAntennaMod import VX as VX
from tnrtAntennaMod import THU as THU
from tnrtAntennaMod import GRS as GRS
from tnrtAntennaMod import hxp_x as hxp_x
from tnrtAntennaMod import hxp_y as hxp_y
from tnrtAntennaMod import hxp_z as hxp_z
from tnrtAntennaMod import hxp_tx as hxp_tx
from tnrtAntennaMod import hxp_ty as hxp_ty
from tnrtAntennaMod import hxp_tz as hxp_tz


# ACU Interface
from AcuCommandClient import AcuCommandClient
import DataFormatAcu as acu
import TypeConverterAntenna as tc

# config
import AntennaDefaults as config


class tnrtAntenna(
    tnrtAntennaMod__POA.tnrtAntenna, ACSComponent, ContainerServices, ComponentLifecycle
):
    def __init__(self):
        ACSComponent.__init__(self)
        ContainerServices.__init__(self)
        self.logger = logging.getLogger(self.__class__.__name__)

        # Supplier to publish data on the notification channel
        self.__nc_supplier = None
        
        self.ip = getIP()
        self.__nc_arrived_status = None

        self.__aStatus = tnrtAntennaMod.stowed
        self.__aStatusIndex = 10  # 10 == stowed

        # Use Python threading event to wait for antenna to arrive at command tracking
        # position (could be program track table or TLE)
        # This is helpful if we want to block program execution while Antenna is
        # moving from old AZ, EL angle to new AZ, EL command.  Same concept for Hexapod.
        self.on_track = threading.Event()
        self.on_track_hxp = threading.Event()

        # Position error at which we can signal that the antenna is "on track" and start
        # other systems.
        self.tolerance_azel = None

        # Maximum slewtime for AZ/EL.  Use this to timeout the blocking trackCoordinate<HO | TLE | EQ>
        self.maximum_slewtime = 250.0

        # Maximum slewtime for HXP.  Us this to timout the blocking trackPatternHxp function
        self.maximum_slewtime_hxp = 90

        # Set the ACU maximum velocity for tracking and position commands as a default.
        # User function can set a slower velocity
        # This can be helpful for reducing ACU errors when antenna moves to fast
        self.velocity = {
            AZ : tc.subsystem_properties[AZ]["P108_v_MaxSys"],
            EL : tc.subsystem_properties[EL]["P108_v_MaxSys"],
        }
    
        self.acu_status = None
        self.acu_status_host = None
        self.acu_status_host_mock = None
        self.acu_status_port_ = None
        self.acu_status_socket_recv_timeout = 10.0
        self.acu_status_socket = None
        self.acu_status_socket_connected = threading.Event()
        self.acu_status_socket_connected_timeout = 1.0
        self.acu_status_message_received = threading.Event()
        self.acu_status_message_received_timeout = 10.0
        self.acu_status_stop_event = threading.Event()

        self.acu_command_host = None
        self.acu_command_port = None
        self.acu_command_host_mock = None
        self.acu_command_socket_recv_timeout = 10.0
        self.acu_command_client = None
        self.acu_command_remote_connected = threading.Event()

        self.connected_acu_type = None

        # Flow control events to allow blocking function until axis is active / intactive
        # For Tracking subsystem, we use this event to signal that antenna has arrived at the tracking coordinate
        # For Vertex subsystem, use these events to signal that all vertex panels are opened / closed.
        self.flow_control = {
            AZ: {
                "index": 0,
                "status_message_frame": "axis_status",
                "event": {
                    "activate": {
                        "thread": threading.Event(),
                        "compare_bit": acu.ABS_ACTIVE,
                        "data_element": "bit_sts",
                    },
                    "deactivate": {
                        "thread": threading.Event(),
                        "compare_bit": acu.ABS_INACTIVE,
                        "data_element": "bit_sts",
                    },
                    "drive_to_stow": {
                        "thread": threading.Event(),
                        "compare_bit": acu.STOW_POSOK,
                        "data_element": "stow_status",
                    },
                },
            },
            EL: {
                "index": 1,
                "status_message_frame": "axis_status",
                "event": {
                    "activate": {
                        "thread": threading.Event(),
                        "compare_bit": acu.ABS_ACTIVE,
                        "data_element": "bit_sts",
                    },
                    "deactivate": {
                        "thread": threading.Event(),
                        "compare_bit": acu.ABS_INACTIVE,
                        "data_element": "bit_sts",
                    },
                    "drive_to_stow": {
                        "thread": threading.Event(),
                        "compare_bit": acu.STOW_POSOK,
                        "data_element": "stow_status",
                    },
                },
            },
            M3: {
                "index": 2,
                "status_message_frame": "axis_status",
                "event": {
                    "activate": {
                        "thread": threading.Event(),
                        "compare_bit": acu.ABS_ACTIVE,
                        "data_element": "bit_sts",
                    },
                    "deactivate": {
                        "thread": threading.Event(),
                        "compare_bit": acu.ABS_INACTIVE,
                        "data_element": "bit_sts",
                    },
                },
            },
            M3R: {
                "index": 3,
                "status_message_frame": "axis_status",
                "event": {
                    "activate": {
                        "thread": threading.Event(),
                        "compare_bit": acu.ABS_ACTIVE,
                        "data_element": "bit_sts",
                    },
                    "deactivate": {
                        "thread": threading.Event(),
                        "compare_bit": acu.ABS_INACTIVE,
                        "data_element": "bit_sts",
                    },
                },
            },
            M4A: {
                "index": 4,
                "status_message_frame": "axis_status",
                "event": {
                    "activate": {
                        "thread": threading.Event(),
                        "compare_bit": acu.ABS_ACTIVE,
                        "data_element": "bit_sts",
                    },
                    "deactivate": {
                        "thread": threading.Event(),
                        "compare_bit": acu.ABS_INACTIVE,
                        "data_element": "bit_sts",
                    },
                },
            },
            M4B: {
                "index": 5,
                "status_message_frame": "axis_status",
                "event": {
                    "activate": {
                        "thread": threading.Event(),
                        "compare_bit": acu.ABS_ACTIVE,
                        "data_element": "bit_sts",
                    },
                    "deactivate": {
                        "thread": threading.Event(),
                        "compare_bit": acu.ABS_INACTIVE,
                        "data_element": "bit_sts",
                    },
                },
            },
            THU: {
                "index": 6,
                "status_message_frame": "axis_status",
                "event": {
                    "activate": {
                        "thread": threading.Event(),
                        "compare_bit": acu.ABS_ACTIVE,
                        "data_element": "bit_sts",
                    },
                    "deactivate": {
                        "thread": threading.Event(),
                        "compare_bit": acu.ABS_INACTIVE,
                        "data_element": "bit_sts",
                    },
                    "drive_to_stow": {
                        "thread": threading.Event(),
                        "compare_bit": acu.STOW_POSOK,
                        "data_element": "stow_status",
                    },
                },
            },
            GRS: {
                "index": 7,
                "status_message_frame": "axis_status",
                "event": {
                    "activate": {
                        "thread": threading.Event(),
                        "compare_bit": acu.ABS_ACTIVE,
                        "data_element": "bit_sts",
                    },
                    "deactivate": {
                        "thread": threading.Event(),
                        "compare_bit": acu.ABS_INACTIVE,
                        "data_element": "bit_sts",
                    },
                    "axis_actual_mode_stop": {
                        "thread": threading.Event(),
                        "compare_value": acu.MODE_STOP.value,
                        "data_element": "actual_mode",
                    },

                },
            },
            HXP: {
                "status_message_frame": "hexapod_sts",
                "event": {
                    "activate": {
                        "thread": threading.Event(),
                        "compare_bit": acu.ABS_ACTIVE,
                        "data_element": "bit_sts",
                    },
                    "deactivate": {
                        "thread": threading.Event(),
                        "compare_bit": acu.ABS_INACTIVE,
                        "data_element": "bit_sts",
                    },
                },
            },
            VX: {
                "status_message_frame": "vertexshutter_sts",
                "event": {
                    "open": {
                        "thread": threading.Event(),
                        "compare_value": 2,
                        "data_element": "status",
                    },
                    "close": {
                        "thread": threading.Event(),
                        "compare_value": 3,
                        "data_element": "status",
                    },
                },
            },
            TR: {
                "status_message_frame": "tracking_sts",
                "event": {
                    "program_offset_complete": {
                        "thread": threading.Event(),
                        "compare_bit": acu.PTS_COMPLETED,
                        "data_element": "prog_offset_bs",
                    }
                },
            },
        }

    def initialize(self):
        """Initialize method which belongs to the LyfeCycle methods. We try to get a reference
        of some instances from several components needed for this component to work.
        If they are not available a Lifecycle exception is thrown and components already got
        are released. Errors are logged for debugging purposes.
        """

        self.__atm = None
        self.__atmName = "ATM"

        self.tolerance_azel = float(config.onsource_tolerance)
        self.tolerance_hxp_linear = float(config.tolerance_hxp_linear)
        self.tolerance_hxp_rotate = float(config.tolerance_hxp_rotate)

        self.logger.logDebug("tolerance = {} [arcsec]".format(self.tolerance_azel)
        )
        self.logger.logDebug(
            "tolerance_hxp_linear = {} [mm], tolerance_hxp_rotate = {} [arcsec]".format(
                self.tolerance_hxp_linear, self.tolerance_hxp_rotate
            )
        )

        longitude = float(config.longitude40mE)
        latitude = float(config.latitude40mN)
        height = float(config.height40m)
        self.site_location = EarthLocation(
            EarthLocation.from_geodetic(
                lon=longitude * units.deg,
                lat=latitude * units.deg,
                height=height * units.m,
                ellipsoid="WGS84",
            )
        )
        self.logger.debug(
            "Read from element latitude %f, longitude %f, height %f"
            % (latitude, longitude, height)
        )

        self.acu_status_host_mock = config.MockAcuIP
        self.acu_status_host = config.AcuIP
        self.acu_status_port = int(config.stsPort)
        self.acu_command_host_mock = config.MockAcuIP
        self.acu_command_host = config.AcuIP
        self.acu_command_port = int(config.cmdPort)

        self.logger.logDebug(
            "Read from CDB config: Acu IP = %s, Mock ACU IP = %s, command port = %d, status port = %d"
            % (
                self.acu_status_host,
                self.acu_status_host_mock,
                self.acu_command_port,
                self.acu_status_port,
            )
        )

        # Subreflector correction for Z as a function of elevation.
        # 3 order polynomial coefficients: a0 + a1*z + a2*z^2 + a3*z^3
        # Units: x, y and z are mm's. Hence a1 = 0.5 means that at 90 degs elevation
        # the correction is 45 mm.

        self.__zCorrEl = [
            float(config.zCorrEl0),
            float(config.zCorrEl1),
            float(config.zCorrEl2),
            float(config.zCorrEl3),
        ]

        # The other axis are correctable but seldom used
        self.__xCorrEl = [
            float(config.xCorrEl0),
            float(config.xCorrEl1),
            float(config.xCorrEl2),
            float(config.xCorrEl3),
        ]
        self.__yCorrEl = [
            float(config.yCorrEl0),
            float(config.yCorrEl1),
            float(config.yCorrEl2),
            float(config.yCorrEl3),
        ]
        self.__txCorrEl = [
            float(config.txCorrEl0),
            float(config.txCorrEl1),
            float(config.txCorrEl2),
            float(config.txCorrEl3),
        ]

        self.logger.logDebug(
            "txCorrEl: %f %f %f %f"
            % (
                self.__txCorrEl[0],
                self.__txCorrEl[1],
                self.__txCorrEl[2],
                self.__txCorrEl[3],
            )
        )

        self.__tyCorrEl = [
            float(config.tyCorrEl0),
            float(config.tyCorrEl1),
            float(config.tyCorrEl2),
            float(config.tyCorrEl3),
        ]

        # Create the supplier to publish structured data from ACU
        self.__nc_supplier = Supplier(tnrtAntennaMod.STATUS_CHANNEL_NAME)
        self.__nc_arrived_status = Supplier(tnrtAntennaMod.ANT_ARRIVED_CHANNEL_NAME)

        self.logger.logInfo("ACU connect state machine: [1 of 5] Not Connected")
        # Create a thread to listen and handle ACU status messages on TCP socket
        # and keep the local data in this class.  This thread exists as a simple function
        # in Antenna class rather than a separate subclass of Thread
        # beacuse we want to write the status message data to Antenna local
        # variable immediately without extra thread signal / event overhead except
        # a mutex lock when writing new data..
        self.thread_acu_status = threading.Thread(
            target=self.start_acu_status_client,
            args=(
                self.acu_status_host_mock,
                self.acu_status_host,
                self.acu_status_port,
            ),
        )
        self.thread_acu_status.start()

        # Blocking wait for socket connected before we continue Antenna initialize sequence
        # and check if we have connected Mock ACU or real ACU.
        # If neither Mock nor real ACU is available, this wait function will timeout and self.connected_acu_type is None
        self.acu_status_socket_connected.wait(
            timeout=self.acu_status_socket_connected_timeout
        )

        if not self.acu_status_socket_connected.is_set():
            # Cannot connect to any acu.  Raise an exception other than ComponentLifecycleException,
            # and the container will catch this exception and deactivate the component.
            newEx = tnrtAntennaErrorImpl.acuExceptionExImpl()
            newEx.addData(
                "errExpl", "Failed to MockACU and remote MTM ACU. Release component"
            )
            raise newEx  # <------ release the component
        else:
            self.logger.logInfo(
                "ACU connect state machine: [2 of 5] STATUS port connected. Waiting for first STATUS packet ..."
            )

            self.acu_status_message_received.wait(
                timeout=self.acu_status_message_received_timeout
            )
            # If we break the wait for timeout, raise an Exception and stop the component
            # But if we break the wait because data arrived, start the command client.
            if self.acu_status_message_received.is_set() == False:
                raise Exception
            else:
                self.logger.logInfo(
                    "ACU connect state machine: [3 of 5] Received first STATUS message packet"
                )

                if self.connected_acu_type == acu.ACU_TYPE_MOCK:
                    self.logger.logInfo("Connect command client to MockAcu")
                    self.start_acu_command_client(
                        self.acu_command_host_mock, self.acu_command_port
                    )
                elif self.connected_acu_type == acu.ACU_TYPE_REMOTE:
                    self.logger.logInfo("Connect command client to Remote MTM ACU")
                    self.start_acu_command_client(
                        self.acu_command_host, self.acu_command_port
                    )

            # Component to serve real-time updates of elevation angle correction due to atmosphere refraction.
            # TODO (SS 11/2020) - add this ATM component again when we have useful data.  For now,
            # do not enable it because testing is faster without extra connection to ATM ACS Component
            # try:
            #   self.__atm = self.getComponent(self.__atmName)
            # except Exception as e:
            #   self.logger.logError(e)
            #   strLine = 'Component %s unavailable' % self.self.__atmName
            #   ex = tnrtAntennaErrorImpl.unableToStartComponentExImpl()
            #   ex.addData("errExpl", 'Component %s unavailable' % self.__atmName)
            #   ex.log(self.logger)
            #   #if it is not absolutely necessary we should do not raise an exception. Comment if so ...
            #   raise ComponentLifecycleException(strLine)

            self.logger.logDebug("tnrtAntenna initialize() finished")

    def execute(self):
        pass

    def aboutToAbort(self):
        pass

    def cleanUp(self):
        self.logger.logInfo("acu_status_thread stop event")
        # set the threading Event to end the infinite loop in acu status socket client
        self.acu_status_stop_event.set()

        if self.acu_status_socket is not None:
            self.logger.logInfo("acu_status_socket close()")
            self.acu_status_socket.close()

        if self.acu_command_client is not None:
            self.logger.logInfo("acu_command client close")
            self.acu_command_client.client_close()
        
        if self.__nc_supplier != None:
            self.__nc_supplier.disconnect()
        
        if self.__nc_arrived_status != None:
            self.__nc_arrived_status.disconnect()

        # TODO (SS 11/2020) - add this ATM component again when we have useful data.  For now,
        # do not enable it because testing is faster without extra connecself.__atmtion to ATM ACS Component
        # if self.__atm != None:
        #   self.logger.logDebug("Releasing %s" % self.__atmName)
        #   self.releaseComponent(self.__atmName)
        #   self.__atm = None

    def start_acu_status_client(self, host_mock, host, port):
        self.logger.logDebug("clear socket connected event")
        self.acu_status_socket_connected.clear()

        self.logger.logDebug("clear message received event")
        self.acu_status_message_received.clear()

        self.logger.logDebug("clear stop event")
        self.acu_status_stop_event.clear()
        lock = threading.Lock()

        with socket.socket(
            socket.AF_INET, socket.SOCK_STREAM
        ) as self.acu_status_socket:
            try:
                self.logger.logInfo(
                    "Try connect socket to MockACU host {} port {} ...".format(
                        host_mock, port
                    )
                )
                self.acu_status_socket.connect((host_mock, port))
                self.connected_acu_type = acu.ACU_TYPE_MOCK
                # Signal to other threads that socket is connected
                self.acu_status_socket_connected.set()
            except ConnectionRefusedError as e:
                # Note the indent: only execute this code if the first attempt to connect MockACU failed
                try:
                    self.logger.logError(
                        "(MockAcu) ConnectionRefusedError: {}".format(e)
                    )
                    self.logger.logDebug(
                        "Try connect socket to MTM ACU host {} port {} ...".format(
                            host, port
                        )
                    )
                    self.acu_status_socket.connect((host, port))
                    self.connected_acu_type = acu.ACU_TYPE_REMOTE
                    # Signal to other threads that socket is connected
                    self.acu_status_socket_connected.set()
                except ConnectionRefusedError as e:
                    self.logger.logError(
                        "(Remote MTM ACU) ConnectionRefusedError: {}".format(e)
                    )
                    return
                except ConnectionAbortedError as e:
                    self.logger.logError(
                        "(Remote MTM ACU) ConnectionAbortedError: {}".format(e)
                    )
                    return

            # Set timeout for connection and blocking read
            self.logger.logDebug(
                "set socket timeout %f" % (self.acu_status_socket_recv_timeout)
            )
            self.acu_status_socket.settimeout(self.acu_status_socket_recv_timeout)

            # Start with the first request
            while not self.acu_status_stop_event.is_set():
                try:
                    # blocking receive with timeout.  Read all the bytes. They don't always arrive
                    # in one read, therefore need a loop to read all.
                    segments = []
                    bytes_received = 0

                    while bytes_received < ctypes.sizeof(acu.StatusMessageFrame):
                        remaining_bytes = (
                            ctypes.sizeof(acu.StatusMessageFrame) - bytes_received
                        )
                        segment = self.acu_status_socket.recv(
                            remaining_bytes, socket.MSG_WAITALL
                        )
                        if segment == b"":
                            raise RuntimeError("socket connection broken")
                        segments.append(segment)
                        bytes_received = bytes_received + len(segment)

                    raw_bytes = b"".join(segments)

                    # Get timestamp now on this local computer ASAP after we receive the message.
                    self.time_tcs_mjd = Time.now().mjd

                    # Acquire the mutex lock to protect self.acu_status in this thread during the write
                    # because other threads read this data structure asynchronously
                    # lock.acquire()

                    # Deserialize the packed byte array into a usable object
                    self.acu_status = acu.StatusMessageFrame.from_buffer_copy(raw_bytes)
                    # lock.release()

                    if len(raw_bytes) != ctypes.sizeof(self.acu_status):
                        raise TypeError(
                            "socket recv TIMEOUT. bytes read = %d of requested %lu"
                            % (len(raw_bytes), ctypes.sizeof(self.acu_status))
                        )
                    if self.acu_status.start_flag != acu.STARTFLAG.value:
                        raise ValueError(
                            "start_flag = %X.  expect %X"
                            % (self.acu_status.start_flag, acu.STARTFLAG.value)
                        )
                    if self.acu_status.status_message_type != 0:
                        raise ValueError(
                            "status_message_type = %d.  expect %d"
                            % (self.acu_status.status_message_type, 0)
                        )
                    if self.acu_status.message_length != ctypes.sizeof(self.acu_status):
                        raise ValueError(
                            "message_length = %d.  expect %ld"
                            % (
                                self.acu_status.message_length,
                                ctypes.sizeof(self.acu_status),
                            )
                        )
                    if self.acu_status.end_flag != acu.ENDFLAG.value:
                        raise ValueError(
                            "end_flag = %X.  expect %X"
                            % (self.acu_status.end_flag, acu.ENDFLAG.value)
                        )

                    self.__handler_acu_status_message()

                except Exception as e:
                    self.logger.logError(e)
                    self.logger.logError("stop receiving status messages")
                    self.acu_status_stop_event.set()

    def start_acu_command_client(self, host, port):
        self.logger.logDebug("clear remote connected event")
        self.acu_command_remote_connected.clear()

        # Settings for Mock ACU .  No need to wait long for the real ACU moving motors or command remote ready...
        if self.connected_acu_type == acu.ACU_TYPE_MOCK:
            self.acu_command_remote_connected.set()
            self.maximum_slewtime = 0.1
            self.maximum_slewtime_hxp = 0.1

        # Create a client to generate commands for ACU, send on TCP socket, and confirm ACK response
        # The behavior of this thread is controlled by Antenna.  Not by incoming events.
        # It has much more function than the simple status thread, therefore we make
        # a subclass of Thread to implement the details
        # The size of data moved by this client is very small (10s of bytes per command).

        self.logger.logDebug(
            "connect command socket to host {} port {}".format(host, port)
        )
        self.acu_command_client = AcuCommandClient(host, port, 0.5)
        self.acu_command_client.start()

        # After the socket is connected, the ACU requires 2 to 5 seconds to process and prepare
        # command message queue, ACK queue, etc.  When it is ready, 1 bit of the status message
        # shows that the port is ready to accept a command.
        self.logger.logInfo(
            "ACU connect state machine: [4 of 5] COMMAND port connected. Waiting for controlModeSts_Remote_Cmd_connect ..."
        )

        self.acu_command_remote_connected.wait(timeout=10.0)

        if self.acu_command_remote_connected.is_set() == True:
            self.logger.logInfo(
                "ACU connect state machine: [5 of 5] Ready to send commands"
            )
        else:
            # Timeout waiting for command port to connect.  Set event to stop listenting for status msg
            self.logger.logInfo(
                "ACU connect state: Timeout waiting for acu_command_remote_connected.is_set()"
            )
            self.acu_status_stop_event.set()

    def checkAcknowledgement(self, ack_status):
        """Checks the ACU acknowledgement and if it is correct it passes through
        and only converts the data type from the native ACU ctype sto the IDL enum type.

        If the acknowledgement was not correct it generates an exception.
        """
        if ack_status == tnrtAntennaMod.DONE:
            return ack_status

        elif ack_status == tnrtAntennaMod.WRONG_MODE:
            newEx = tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl()
            newEx.addData("errExpl", "Wrong ACU acknowledgement: wrong mode")
            raise newEx

        elif ack_status == tnrtAntennaMod.PARAM_ERROR:
            newEx = tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl()
            newEx.addData("errExpl", "Wrong ACU acknowledgement: Parameter error")
            raise newEx

        elif ack_status == tnrtAntennaMod.WRONG_LENGTH:
            newEx = tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl()
            newEx.addData("errExpl", "Wrong ACU acknowledgement: message length error")
            raise newEx

        elif ack_status == tnrtAntennaMod.UNDEF_COMMAND:
            newEx = tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl()
            newEx.addData("errExpl", "Wrong ACU acknowledgement: Undefined")
            raise newEx

        elif ack_status == tnrtAntennaMod.ACCEPTED:
            return ack_status

        elif ack_status == tnrtAntennaMod.TIMEOUT:
            newEx = tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl()
            newEx.addData("errExpl", "Wrong ACU acknowledgement: Undefined")
            raise newEx

        else:
            newEx = tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl()
            newEx.addData(
                "errExpl", "Wrong ACU acknowledgement: unknown acknowledgment!"
            )
            raise newEx

    # -------- IDL FUNCTIONS: CDB data  ---------------------------------------
    def get_ip(self):
        return self.ip

    def getSiteLocation(self):
        """Get site location from member astropy EarthLocation and return as a IDL struct"""
        return tnrtAntennaMod.llh(
            self.site_location.lon.deg,
            self.site_location.lat.deg,
            self.site_location.height.value,
        )

    def get_status_host_mock(self):
        return self.acu_status_host_mock

    def get_status_host(self):
        return self.acu_status_host

    def get_status_port(self):
        return self.acu_status_port

    def get_command_host_mock(self):
        return self.acu_command_host_mock

    def get_command_host(self):
        return self.acu_command_host

    def get_command_port(self):
        return self.acu_command_port

    def set_velocity(self, subsystem, velocity):
        """
        docstring
        """
        allowed_subsystems = [AZ, EL]

        if subsystem not in allowed_subsystems:
            self.logger.logWarning(
                "{} not in allowed list {}. Do nothing".format(
                    subsystem, allowed_subsystems
                )
            )
            return tnrtAntennaMod.PARAM_ERROR

        self.logger.logDebug("Checking if requested velocity {} deg/s is in range of {} axis".format(velocity, subsystem))
        if (velocity > tc.subsystem_properties[subsystem]["P108_v_MaxSys"]) or (velocity < tc.subsystem_properties[subsystem]["P109_v_MinSys"]):
            raise tnrtAntennaErrorImpl.paramOutOfLimitsExImpl()
        else:        
            self.logger.logDebug("Value {} is in range. Overwrite current value: {} of {} axis".format(velocity, self.velocity[subsystem], subsystem))
            self.velocity[subsystem] = velocity
            return tnrtAntennaMod.DONE
    
    def get_velocity(self, subsystem):
        allowed_subsystems = [AZ, EL]

        if subsystem not in allowed_subsystems:
            self.logger.logWarning(
                "{} not in allowed list {}. Do nothing".format(
                    subsystem, allowed_subsystems
                )
            )
            return tnrtAntennaMod.PARAM_ERROR
        
        return(self.velocity[subsystem])

    # -------- IDL FUNCTIONS: ACU STATUS  -------------------------------------
    # direct results from ACU without modification or additional logic
    # Note (SS. 05/2022) The axis status has 8 axes.  The integer array index is not the same
    # as the subsystem enum that we use throughout the code from tnrtAntenna.idl
    # enum subsystemId {AZ, EL, TR, HXP, M3, M3R, M4A, M4B, VX, THU, GRS};
    # because all subsystems don't have an AxisStatus.
    # For example, subsystem AZ enum value is 0.  Same array index 0 in axis status array. (convenient!)
    # THU subsystem enum value is 9, but THU axis status is at acu_status.axis_status[6]
    # The array index in axis_status are listed in self.flow_control[subsystem]['index']

    def getAzPosition(self):
        return self.acu_status.axis_status[0].actual_position

    def getElPosition(self):
        return self.acu_status.axis_status[1].actual_position

    def getM3Position(self):
        return self.acu_status.axis_status[2].actual_position

    def getM3RPosition(self):
        return self.acu_status.axis_status[3].actual_position

    def getM4APosition(self):
        return self.acu_status.axis_status[4].actual_position

    def getM4BPosition(self):
        return self.acu_status.axis_status[5].actual_position

    def getTHUPosition(self):
        return self.acu_status.axis_status[6].actual_position

    def getGRSPosition(self):
        return self.acu_status.axis_status[7].actual_position

    def getAxisPositionOffsetAZ(self):
        return self.acu_status.axis_status[0].current_position_offset
    
    def getAxisPositionOffsetEL(self):
        return self.acu_status.axis_status[1].current_position_offset
    
    def getAxisPositionOffsetM3(self):
        return self.acu_status.axis_status[2].current_position_offset
		
    def getTotalPointingCorrectionAZ(self):
        return self.acu_status.tracking_sts.total_pointing_correction_az

    def getTotalPointingCorrectionEL(self):
        return self.acu_status.tracking_sts.total_pointing_correction_el

    def getTotalPointingCorrectionGRS(self):
        return self.acu_status.tracking_sts.total_pointing_correction_grs

    def getCurrentPointingCorrectionAZ(self):
        return self.acu_status.axis_status[0].current_pointing_correction

    def getCurrentPointingCorrectionEL(self):
        return self.acu_status.axis_status[1].current_pointing_correction

    def getTrackStatus(self):
        return self.acu_status.tracking_sts.bit_status

    def getProgramTrackStatus(self):
        return self.acu_status.tracking_sts.prog_track_bs

    def getProgramOffsetStatus(self):
        return self.acu_status.tracking_sts.prog_offset_bs

    def stowPositionReachedAz(self):
        return (self.acu_status.axis_status[0].stow_status & 1) > 0

    def preLimitSwUpReachedAz(self):
        return (self.acu_status.axis_status[0].stow_status & 2) > 0

    def preLimitSwDownpReachedAz(self):
        return (self.acu_status.axis_status[0].stow_status & 4) > 0

    def stowPin1InAz(self):
        return (self.acu_status.axis_status[0].stow_status & 16) > 0

    def stowPin1OutAz(self):
        return (self.acu_status.axis_status[0].stow_status & 32) > 0

    def stowPin1ErrorAz(self):
        return (self.acu_status.axis_status[0].stow_status & 64) > 0

    def stowPin1SelectedAz(self):
        return (self.acu_status.axis_status[0].stow_status & 128) > 0

    def stowPin2InAz(self):
        return (self.acu_status.axis_status[0].stow_status & 256) > 0

    def stowPin2OutAz(self):
        return (self.acu_status.axis_status[0].stow_status & 512) > 0

    def stowPin2ErrorAz(self):
        return (self.acu_status.axis_status[0].stow_status & 1024) > 0

    def stowPin2SelectedAz(self):
        return (self.acu_status.axis_status[0].stow_status & 2048) > 0

    def cmdStowPin1InAz(self):
        return (self.acu_status.axis_status[0].stow_status & 4096) > 0

    def cmdStowPin1OutAz(self):
        return (self.acu_status.axis_status[0].stow_status & 8192) > 0

    def cmdStowPin2InAz(self):
        return (self.acu_status.axis_status[0].stow_status & 16384) > 0

    def cmdStowPin2OutAz(self):
        return (self.acu_status.axis_status[0].stow_status & 32768) > 0

    def stowPositionReachedEl(self):
        return (self.acu_status.axis_status[1].stow_status & 1) > 0

    def preLimitSwUpReachedEl(self):
        return (self.acu_status.axis_status[1].stow_status & 2) > 0

    def preLimitSwDownpReachedEl(self):
        return (self.acu_status.axis_status[1].stow_status & 4) > 0

    def stowPin1InEl(self):
        return (self.acu_status.axis_status[1].stow_status & 16) > 0

    def stowPin1OutEl(self):
        return (self.acu_status.axis_status[1].stow_status & 32) > 0

    def stowPin1ErrorEl(self):
        return (self.acu_status.axis_status[1].stow_status & 64) > 0

    def stowPin1SelectedEl(self):
        return (self.acu_status.axis_status[1].stow_status & 128) > 0

    def stowPin2InEl(self):
        return (self.acu_status.axis_status[1].stow_status & 256) > 0

    def stowPin2OutEl(self):
        return (self.acu_status.axis_status[1].stow_status & 512) > 0

    def stowPin2ErrorEl(self):
        return (self.acu_status.axis_status[1].stow_status & 1024) > 0

    def stowPin2SelectedEl(self):
        return (self.acu_status.axis_status[1].stow_status & 2048) > 0

    def cmdStowPin1InEl(self):
        return (self.acu_status.axis_status[1].stow_status & 4096) > 0

    def cmdStowPin1OutEl(self):
        return (self.acu_status.axis_status[1].stow_status & 8192) > 0

    def cmdStowPin2InEl(self):
        return (self.acu_status.axis_status[1].stow_status & 16384) > 0

    def cmdStowPin2OutEl(self):
        return (self.acu_status.axis_status[1].stow_status & 32768) > 0

    def stowPositionReachedTHU(self):
        return (
            self.acu_status.axis_status[self.flow_control[THU]["index"]].stow_status
            & acu.STOW_POSOK
        ) > 0

    def getRemoteCtl(self):
        return (self.acu_status.general_sts.facility_control_status & 128) > 0

    def getFacilityControlSts(self):
        return self.acu_status.general_sts.facility_control_status

    def getAzActive(self):
        return (self.acu_status.axis_status[0].bit_sts & 1024) > 0

    def getElActive(self):
        return (self.acu_status.axis_status[1].bit_sts & 1024) > 0

    def getM3Active(self):
        return (self.acu_status.axis_status[2].bit_sts & 1024) > 0

    def getM3RActive(self):
        return (self.acu_status.axis_status[3].bit_sts & 1024) > 0

    def getM4AActive(self):
        return (self.acu_status.axis_status[4].bit_sts & 1024) > 0

    def getM4BActive(self):
        return (self.acu_status.axis_status[5].bit_sts & 1024) > 0

    def getTHUActive(self):
        return (self.acu_status.axis_status[6].bit_sts & 1024) > 0

    def getGRSActive(self):
        return (self.acu_status.axis_status[7].bit_sts & 1024) > 0

    def getSimulatorEnabled(self):
        return (self.acu_status.general_sts.operation_status & acu.SO_SYSSIM) > 0

    def getVertex(self):
        return tc.from_ctype_VertexShutterStatus(self.acu_status.vertexshutter_sts)

    def getHexapodStatus(self):
        return tc.from_ctype_HexapodStatus(self.acu_status.hexapod_sts)

    def getTemperatureSensors(self):
        # Convert the ctypes array to a list using [:] operator.  No need to write a 1-line function in TypeConverterAntenna
        return self.acu_status.general_sts.temperature_M1[:]

    # ---------- IDL FUNCTIONS: ACU MODE COMMANDS   ---------------------------------------

    def deactivate(self, subsystem):
        """ """

        allowed_subsystems = [AZ, EL, HXP, M3, M3R, M4A, M4B, THU, GRS]

        if subsystem not in allowed_subsystems:
            self.logger.logWarning(
                "{} not in allowed list {}. Do nothing".format(
                    subsystem, allowed_subsystems
                )
            )
            return tnrtAntennaMod.PARAM_ERROR

        ss = tc.subsystem_properties[subsystem]["acu_code"]
        self.logger.logDebug("found subsystem {} {}".format(ss, subsystem))

        if subsystem == tnrtAntennaMod.HXP:
            ack = self.acu_command_client.send_hexapod_command(
                acu.CMD_MODE,
                ss,
                acu.CMD_INACTIVE,
                0.0,
                0.0,
                0.0,
                0.0,
                0.0,
                0.0,
                0.0,
                0.0,
                0.0,
                0.0,
                0.0,
                0.0,
            )
        else:
            ack = self.acu_command_client.send_standard_command(
                acu.CMD_MODE, ss, acu.CMD_INACTIVE, 0.0, 0.0
            )

        self.flow_control[subsystem]["event"]["deactivate"]["thread"].clear()
        result = {}
        thread = threading.Thread(
            name="{}-activate-blocking-thread".format(subsystem),
            target=self.blocking_flow_control,
            args=[subsystem, "deactivate", ack, result],
        )
        thread.start()
        self.flow_control_logging(
            subsystem, "deactivate", "{} deactivating ...".format(subsystem)
        )
        thread.join()
        return result["status"]

    def activate(self, subsystem):
        """ """

        allowed_subsystems = [AZ, EL, HXP, M3, M3R, M4A, M4B, THU, GRS]

        if subsystem not in allowed_subsystems:
            self.logger.logWarning(
                "{} not in allowed list {}. Do nothing".format(
                    subsystem, allowed_subsystems
                )
            )
            return tnrtAntennaMod.PARAM_ERROR

        self.logger.logInfo("activate {}".format(subsystem))
        ss = tc.subsystem_properties[subsystem]["acu_code"]
        self.logger.logDebug("found subsystem {}".format(ss))

        if subsystem == tnrtAntennaMod.HXP:
            ack = self.acu_command_client.send_hexapod_command(
                acu.CMD_MODE,
                ss,
                acu.CMD_ACTIVE,
                0.0,
                0.0,
                0.0,
                0.0,
                0.0,
                0.0,
                0.0,
                0.0,
                0.0,
                0.0,
                0.0,
                0.0,
            )
        else:
            ack = self.acu_command_client.send_standard_command(
                acu.CMD_MODE, ss, acu.CMD_ACTIVE, 0.0, 0.0
            )

        self.flow_control[subsystem]["event"]["activate"]["thread"].clear()
        result = {}
        thread = threading.Thread(
            name="{}-activate-blocking-thread".format(subsystem),
            target=self.blocking_flow_control,
            args=[subsystem, "activate", ack, result],
        )
        thread.start()
        self.flow_control_logging(
            subsystem, "activate", "{} activating ...".format(subsystem)
        )
        thread.join()
        return result["status"]

    def absolutePosition(self, subsystem, pos, rate, blocking=False):
        """
        docstring
        """
        allowed_subsystems = [AZ, EL, M3, THU, GRS]

        if subsystem not in allowed_subsystems:
            self.logger.logWarning(
                "{} not in allowed list {}. Do nothing".format(
                    subsystem, allowed_subsystems
                )
            )
            return tnrtAntennaMod.PARAM_ERROR

        self.logger.logInfo("absolute position {}".format(subsystem))
        ss = tc.subsystem_properties[subsystem]["acu_code"]
        self.logger.logDebug("found subsystem {}".format(ss))

        if np.abs(rate) > tc.subsystem_properties[subsystem]["P108_v_MaxSys"]:
            self.logger.logError("slew rate too fast")
            raise (
                tnrtAntennaErrorImpl.paramOutOfLimitsExImpl(
                    ACSErr.NameValue(
                        "errExpl",
                        "slew rate abs({}) over limit {}".format(
                            rate, tc.subsystem_properties[subsystem]["P108_v_MaxSys"]
                        ),
                    )
                )
            )

        if pos < tc.subsystem_properties[subsystem]["P104_p_FinEndDn"]:
            self.logger.logError("too position too low")
            raise (
                tnrtAntennaErrorImpl.paramOutOfLimitsExImpl(
                    ACSErr.NameValue(
                        "errExpl",
                        "pos({}) < min {}".format(
                            pos, tc.subsystem_properties[subsystem]["P104_p_FinEndDn"]
                        ),
                    )
                )
            )

        if pos > tc.subsystem_properties[subsystem]["P105_p_FinEndUp"]:
            self.logger.logError("too position too high")
            raise (
                tnrtAntennaErrorImpl.paramOutOfLimitsExImpl(
                    ACSErr.NameValue(
                        "errExpl",
                        "pos({}) > max {}".format(
                            pos, tc.subsystem_properties[subsystem]["P105_p_FinEndUp"]
                        ),
                    )
                )
            )

        ack = self.acu_command_client.send_standard_command(
            acu.CMD_MODE, ss, acu.CMD_POSITION_ABSOLUTE, pos, rate
        )

        if blocking is True:
            self.flow_control[subsystem]["event"]["axis_actual_mode_stop"]["thread"].clear()
            result = {}
            thread = threading.Thread(
                name="absolutePosition-blocking-thread",
                target=self.blocking_flow_control,
                args=[subsystem, "axis_actual_mode_stop", ack, result, 300],
            )
            thread.start()
            self.flow_control_logging(subsystem, "axis_actual_mode_stop", "waiting for axis actual mode to be MODE_STOP ...")
            thread.join()
            return result["status"]
        else:
            return tc.ack_properties[ack.status]["idl_enum"]

    def absolutePositionHxp(
        self,
        xPos,
        xRate,
        yPos,
        yRate,
        zPos,
        zRate,
        txPos,
        txRate,
        tyPos,
        tyRate,
        tzPos,
        tzRate,
    ):
        self.logger.logInfo("absolute position HXP")

        ss = tc.subsystem_properties[HXP]["acu_code"]
        self.logger.logDebug("found subsystem {}".format(ss))

        vmax = tc.subsystem_properties[HXP][hxp_x]["v_MaxSys"]
        tvmax = tc.subsystem_properties[HXP][hxp_tx]["v_MaxSys"]

        xmax = tc.subsystem_properties[HXP][hxp_x]["EndUp"]
        ymax = tc.subsystem_properties[HXP][hxp_y]["EndUp"]
        zmax = tc.subsystem_properties[HXP][hxp_z]["EndUp"]

        txmax = tc.subsystem_properties[HXP][hxp_tx]["EndUp"]
        tymax = tc.subsystem_properties[HXP][hxp_ty]["EndUp"]
        tzmax = tc.subsystem_properties[HXP][hxp_tz]["EndUp"]

        if (np.abs(xRate) > vmax) or (np.abs(yRate) > vmax) or (np.abs(zRate) > vmax):
            raise (
                tnrtAntennaErrorImpl.paramOutOfLimitsExImpl(
                    ACSErr.NameValue("errExpl", "linear rate over")
                )
            )
        if (
            (np.abs(txRate) > tvmax)
            or (np.abs(tyRate) > tvmax)
            or (np.abs(tzRate) > tvmax)
        ):
            raise (
                tnrtAntennaErrorImpl.paramOutOfLimitsExImpl(
                    ACSErr.NameValue("errExpl", "tilt rate over")
                )
            )

        if (np.abs(xPos) > xmax) or (np.abs(yPos) > ymax) or (np.abs(zPos) > zmax):
            raise (
                tnrtAntennaErrorImpl.paramOutOfLimitsExImpl(
                    ACSErr.NameValue("errExpl", "linear position over")
                )
            )
        if (
            (np.abs(txPos) > txmax)
            or (np.abs(tyPos) > tymax)
            or (np.abs(tzPos) > tzmax)
        ):
            raise (
                tnrtAntennaErrorImpl.paramOutOfLimitsExImpl(
                    ACSErr.NameValue("errExpl", "tilt position over")
                )
            )

        ack = self.acu_command_client.send_hexapod_command(
            acu.CMD_MODE,
            ss,
            acu.CMD_POSITION_ABSOLUTE,
            xPos,
            xRate,
            yPos,
            yRate,
            zPos,
            zRate,
            txPos,
            txRate,
            tyPos,
            tyRate,
            tzPos,
            tzRate,
        )
        return tc.ack_properties[ack.status]["idl_enum"]

    def relativePosition(self, subsystem, pos, rate):
        """
        docstring
        """
        allowed_subsystems = [AZ, EL, THU]

        if subsystem not in allowed_subsystems:
            self.logger.logWarning(
                "{} not in allowed list {}. Do nothing".format(
                    subsystem, allowed_subsystems
                )
            )
            return tnrtAntennaMod.PARAM_ERROR

        dpmax = (
            tc.subsystem_properties[subsystem]["P105_p_FinEndUp"]
            - tc.subsystem_properties[subsystem]["P104_p_FinEndDn"]
        )

        self.logger.logInfo("relative position {}".format(subsystem))
        ss = tc.subsystem_properties[subsystem]["acu_code"]
        self.logger.logDebug("found subsystem {}".format(ss))

        if np.abs(rate) > tc.subsystem_properties[subsystem]["P108_v_MaxSys"]:
            self.logger.logError("slew rate too fast")
            raise (
                tnrtAntennaErrorImpl.paramOutOfLimitsExImpl(
                    ACSErr.NameValue(
                        "errExpl",
                        "slew rate abs({}) over limit {}".format(
                            rate, tc.subsystem_properties[subsystem]["P108_v_MaxSys"]
                        ),
                    )
                )
            )

        if np.abs(pos) > dpmax:
            self.logger.logError("too position far")
            raise (
                tnrtAntennaErrorImpl.paramOutOfLimitsExImpl(
                    ACSErr.NameValue(
                        "errExpl",
                        "pos({}) < min {}".format(
                            pos, tc.subsystem_properties[subsystem]["P104_p_FinEndDn"]
                        ),
                    )
                )
            )

        ack = self.acu_command_client.send_standard_command(
            acu.CMD_MODE, ss, acu.CMD_POSITION_RELATIVE, pos, rate
        )
        return tc.ack_properties[ack.status]["idl_enum"]

    def relativePositionHxp(
        self,
        xPos,
        xRate,
        yPos,
        yRate,
        zPos,
        zRate,
        txPos,
        txRate,
        tyPos,
        tyRate,
        tzPos,
        tzRate,
    ):
        self.logger.logInfo("relative position HXP")

        ss = tc.subsystem_properties[HXP]["acu_code"]
        self.logger.logDebug("found subsystem {}".format(ss))

        vmax = tc.subsystem_properties[HXP][hxp_x]["v_MaxSys"]
        tvmax = tc.subsystem_properties[HXP][hxp_tx]["v_MaxSys"]

        dxmax = (
            tc.subsystem_properties[HXP][hxp_x]["EndUp"]
            - tc.subsystem_properties[HXP][hxp_x]["EndDn"]
        )
        dymax = (
            tc.subsystem_properties[HXP][hxp_y]["EndUp"]
            - tc.subsystem_properties[HXP][hxp_y]["EndDn"]
        )
        dzmax = (
            tc.subsystem_properties[HXP][hxp_z]["EndUp"]
            - tc.subsystem_properties[HXP][hxp_z]["EndDn"]
        )

        dtxmax = (
            tc.subsystem_properties[HXP][hxp_tx]["EndUp"]
            - tc.subsystem_properties[HXP][hxp_tx]["EndDn"]
        )
        dtymax = (
            tc.subsystem_properties[HXP][hxp_ty]["EndUp"]
            - tc.subsystem_properties[HXP][hxp_ty]["EndDn"]
        )
        dtzmax = (
            tc.subsystem_properties[HXP][hxp_tz]["EndUp"]
            - tc.subsystem_properties[HXP][hxp_tz]["EndDn"]
        )

        if (np.abs(xRate) > vmax) or (np.abs(yRate) > vmax) or (np.abs(zRate) > vmax):
            raise (
                tnrtAntennaErrorImpl.paramOutOfLimitsExImpl(
                    ACSErr.NameValue("errExpl", "linear rate over")
                )
            )
        if (
            (np.abs(txRate) > tvmax)
            or (np.abs(tyRate) > tvmax)
            or (np.abs(tzRate) > tvmax)
        ):
            raise (
                tnrtAntennaErrorImpl.paramOutOfLimitsExImpl(
                    ACSErr.NameValue("errExpl", "tilt rate over")
                )
            )

        if (np.abs(xPos) > dxmax) or (np.abs(yPos) > dymax) or (np.abs(zPos) > dzmax):
            raise (
                tnrtAntennaErrorImpl.paramOutOfLimitsExImpl(
                    ACSErr.NameValue("errExpl", "linear position over")
                )
            )
        if (
            (np.abs(txPos) > dtxmax)
            or (np.abs(tyPos) > dtymax)
            or (np.abs(tzPos) > dtzmax)
        ):
            raise (
                tnrtAntennaErrorImpl.paramOutOfLimitsExImpl(
                    ACSErr.NameValue("errExpl", "tilt position over")
                )
            )

        ack = self.acu_command_client.send_hexapod_command(
            acu.CMD_MODE,
            ss,
            acu.CMD_POSITION_RELATIVE,
            xPos,
            xRate,
            yPos,
            yRate,
            zPos,
            zRate,
            txPos,
            txRate,
            tyPos,
            tyRate,
            tzPos,
            tzRate,
        )
        return tc.ack_properties[ack.status]["idl_enum"]

    def slew(self, subsystem, rate):
        """'
        docstring
        """
        allowed_subsystems = [AZ, EL, M3R, M4A, M4B, THU, GRS]

        if subsystem not in allowed_subsystems:
            self.logger.logWarning(
                "{} not in allowed list {}. Do nothing".format(
                    subsystem, allowed_subsystems
                )
            )
            return tnrtAntennaMod.PARAM_ERROR

        self.logger.logInfo("slew {} rate {}".format(subsystem, rate))
        ss = tc.subsystem_properties[subsystem]["acu_code"]
        self.logger.logDebug("found subsystem {}".format(ss))

        if np.abs(rate) > tc.subsystem_properties[subsystem]["P108_v_MaxSys"]:
            self.logger.logError("slew rate too fast")
            raise (
                tnrtAntennaErrorImpl.paramOutOfLimitsExImpl(
                    ACSErr.NameValue(
                        "errExpl",
                        "slew rate abs({}) over limit {}".format(
                            rate, tc.subsystem_properties[subsystem]["P108_v_MaxSys"]
                        ),
                    )
                )
            )

        ack = self.acu_command_client.send_standard_command(
            acu.CMD_MODE, ss, acu.CMD_SLEW, 0.0, rate
        )
        return tc.ack_properties[ack.status]["idl_enum"]

    def stop(self, subsystem):
        """Sets the specified motor system to stop."""
        allowed_subsystems = [AZ, EL, HXP, M3, M3R, M4A, M4B, VX, THU, GRS]

        if subsystem not in allowed_subsystems:
            self.logger.logWarning(
                "{} not in allowed list {}. Do nothing".format(
                    subsystem, allowed_subsystems
                )
            )
            return tnrtAntennaMod.PARAM_ERROR

        self.logger.logInfo("stop {}".format(subsystem))
        ss = tc.subsystem_properties[subsystem]["acu_code"]
        self.logger.logDebug("found subsystem {}".format(ss))

        if subsystem == tnrtAntennaMod.HXP:
            ack = self.acu_command_client.send_hexapod_command(
                acu.CMD_MODE,
                ss,
                acu.CMD_STOP,
                0.0,
                0.0,
                0.0,
                0.0,
                0.0,
                0.0,
                0.0,
                0.0,
                0.0,
                0.0,
                0.0,
                0.0,
            )
        else:
            ack = self.acu_command_client.send_standard_command(
                acu.CMD_MODE, ss, acu.CMD_STOP, 0.0, 0.0
            )

        return tc.ack_properties[ack.status]["idl_enum"]

    def programTrack(self, subsystem, rate):
        allowed_subsystems = [AZ, EL]

        if subsystem not in allowed_subsystems:
            self.logger.logWarning(
                "{} not in allowed list {}. Do nothing".format(
                    subsystem, allowed_subsystems
                )
            )
            return tnrtAntennaMod.PARAM_ERROR

        ss = tc.subsystem_properties[subsystem]["acu_code"]
        self.logger.logDebug("found subsystem {}".format(ss))

        if np.abs(rate) > tc.subsystem_properties[subsystem]["P108_v_MaxSys"]:
            self.logger.logError("slew rate too fast")
            raise (
                tnrtAntennaErrorImpl.paramOutOfLimitsExImpl(
                    ACSErr.NameValue(
                        "errExpl",
                        "slew rate abs({}) over limit {}".format(
                            rate, tc.subsystem_properties[subsystem]["P108_v_MaxSys"]
                        ),
                    )
                )
            )

        ack = self.acu_command_client.send_standard_command(
            acu.CMD_MODE, ss, acu.CMD_PROGRAM_TRACK, 0.0, rate
        )
        return tc.ack_properties[ack.status]["idl_enum"]

    def programTrackHxp(self, linear_rate, rotate_rate):
        ss = tc.subsystem_properties[HXP]["acu_code"]
        self.logger.logDebug("found subsystem {}".format(ss))

        if np.abs(linear_rate) > tc.subsystem_properties[HXP][hxp_x]["v_MaxSys"]:
            self.logger.logError("linear slew rate too fast")
            raise (
                tnrtAntennaErrorImpl.paramOutOfLimitsExImpl(
                    ACSErr.NameValue(
                        "errExpl",
                        "linear slew rate abs({}) is over limit {}".format(
                            linear_rate, tc.subsystem_properties[HXP]["P108_v_MaxSys"]
                        ),
                    )
                )
            )

        if np.abs(rotate_rate) > tc.subsystem_properties[HXP][hxp_tx]["v_MaxSys"]:
            self.logger.logError("rotate slew rate too fast")
            raise (
                tnrtAntennaErrorImpl.paramOutOfLimitsExImpl(
                    ACSErr.NameValue(
                        "errExpl",
                        "rotate slew rate abs({}) is over limit {}".format(
                            rotate_rate, tc.subsystem_properties[HXP]["P108_v_MaxSys"]
                        ),
                    )
                )
            )

        ack = self.acu_command_client.send_hexapod_command(
            acu.CMD_MODE,
            ss,
            acu.CMD_PROGRAM_TRACK,
            0.0,
            linear_rate,
            0.0,
            linear_rate,
            0.0,
            linear_rate,
            0.0,
            rotate_rate,
            0.0,
            rotate_rate,
            0.0,
            rotate_rate,
        )
        return tc.ack_properties[ack.status]["idl_enum"]

    def reset(self, subsystem):
        """'
        Reset the corresponding drive system
        """
        allowed_subsystems = [AZ, EL, HXP, M3, M3R, M4A, M4B, VX, THU, GRS]

        if subsystem not in allowed_subsystems:
            self.logger.logWarning(
                "{} not in allowed list {}. Do nothing".format(
                    subsystem, allowed_subsystems
                )
            )
            return tnrtAntennaMod.PARAM_ERROR

        self.logger.logInfo("reset {}".format(subsystem))
        ss = tc.subsystem_properties[subsystem]["acu_code"]
        self.logger.logDebug("found subsystem {}".format(ss))

        if subsystem == tnrtAntennaMod.HXP:
            ack = self.acu_command_client.send_hexapod_command(
                acu.CMD_MODE,
                ss,
                acu.CMD_RESET,
                0.0,
                0.0,
                0.0,
                0.0,
                0.0,
                0.0,
                0.0,
                0.0,
                0.0,
                0.0,
                0.0,
                0.0,
            )
        else:
            ack = self.acu_command_client.send_standard_command(
                acu.CMD_MODE, ss, acu.CMD_RESET, 0.0, 0.0
            )

        return tc.ack_properties[ack.status]["idl_enum"]

    def predefinePosition(self, subsystem, PresetPos, rate):
        allowed_subsystems = [AZ, EL, GRS]

        if subsystem not in allowed_subsystems:
            self.logger.logWarning(
                "{} not in allowed list {}. Do nothing".format(
                    subsystem, allowed_subsystems
                )
            )
            return tnrtAntennaMod.PARAM_ERROR

        ss = tc.subsystem_properties[subsystem]["acu_code"]
        self.logger.logDebug("found subsystem {}".format(ss))

        param1 = tc.enum_properties[PresetPos]["acu_code"]

        if np.abs(rate) > tc.subsystem_properties[subsystem]["P108_v_MaxSys"]:
            self.logger.logError("slew rate too fast")
            raise (
                tnrtAntennaErrorImpl.paramOutOfLimitsExImpl(
                    ACSErr.NameValue(
                        "errExpl",
                        "slew rate abs({}) over limit {}".format(
                            rate, tc.subsystem_properties[subsystem]["P108_v_MaxSys"]
                        ),
                    )
                )
            )

        ack = self.acu_command_client.send_standard_command(
            acu.CMD_MODE, ss, acu.CMD_MOVE_PREPOSITION, param1, rate
        )
        return tc.ack_properties[ack.status]["idl_enum"]

    def openVX(self):
        ss = tc.subsystem_properties[VX]["acu_code"]
        self.logger.logDebug("found subsystem {}".format(ss))
        ack = self.acu_command_client.send_standard_command(
            acu.CMD_MODE, ss, acu.CMD_OPEN, 0.0, 0.0
        )
        self.flow_control[VX]["event"]["open"]["thread"].clear()
        result = {}
        thread = threading.Thread(
            name="openVX-blocking-thread",
            target=self.blocking_flow_control,
            args=[VX, "open", ack, result],
        )
        thread.start()
        self.flow_control_logging(VX, "open", "opening VX...")
        thread.join()
        return result["status"]

    def closeVX(self):
        ss = tc.subsystem_properties[VX]["acu_code"]
        self.logger.logDebug("found subsystem {}".format(ss))
        ack = self.acu_command_client.send_standard_command(
            acu.CMD_MODE, ss, acu.CMD_CLOSE, 0.0, 0.0
        )
        self.flow_control[VX]["event"]["close"]["thread"].clear()
        result = {}
        thread = threading.Thread(
            name="closeVX-blocking-thread",
            target=self.blocking_flow_control,
            args=[VX, "close", ack, result],
        )
        thread.start()
        self.flow_control_logging(VX, "close", "closing VX...")
        thread.join()
        return result["status"]

    def stow(self, subsystem, pinselect):
        allowed_subsystems = [AZ, EL, THU, GRS]

        if subsystem not in allowed_subsystems:
            self.logger.logWarning(
                "{} not in allowed list {}. Do nothing".format(
                    subsystem, allowed_subsystems
                )
            )
            return tnrtAntennaMod.PARAM_ERROR

        ss = tc.subsystem_properties[subsystem]["acu_code"]
        self.logger.logDebug("found subsystem {}".format(ss))

        param1 = tc.enum_properties[pinselect]["acu_code"]

        ack = self.acu_command_client.send_standard_command(
            acu.CMD_MODE, ss, acu.CMD_STOW, param1, 0.0
        )
        return tc.ack_properties[ack.status]["idl_enum"]

    def unstow(self, subsystem, pinselect):
        allowed_subsystems = [AZ, EL, THU, GRS]

        if subsystem not in allowed_subsystems:
            self.logger.logWarning(
                "{} not in allowed list {}. Do nothing".format(
                    subsystem, allowed_subsystems
                )
            )
            return tnrtAntennaMod.PARAM_ERROR

        ss = tc.subsystem_properties[subsystem]["acu_code"]
        # self.logger.logDebug('found subsystem {}'.format(ss))

        param1 = tc.enum_properties[pinselect]["acu_code"]

        ack = self.acu_command_client.send_standard_command(
            acu.CMD_MODE, ss, acu.CMD_UNSTOW, param1, 0.0
        )
        return tc.ack_properties[ack.status]["idl_enum"]

    def driveToStow(self, subsystem, stowPosition, rate):
        allowed_subsystems = [AZ, EL, THU, GRS]

        if subsystem not in allowed_subsystems:
            self.logger.logWarning(
                "{} not in allowed list {}. Do nothing".format(
                    subsystem, allowed_subsystems
                )
            )
            return tnrtAntennaMod.PARAM_ERROR

        if np.abs(rate) > tc.subsystem_properties[subsystem]["P108_v_MaxSys"]:
            self.logger.logError("slew rate too fast")
            raise (
                tnrtAntennaErrorImpl.paramOutOfLimitsExImpl(
                    ACSErr.NameValue(
                        "errExpl",
                        "slew rate abs({}) over limit {}".format(
                            rate, tc.subsystem_properties[subsystem]["P108_v_MaxSys"]
                        ),
                    )
                )
            )

        ss = tc.subsystem_properties[subsystem]["acu_code"]
        self.logger.logDebug("found subsystem {}".format(ss))

        param1 = tc.enum_properties[stowPosition]["acu_code"]

        ack = self.acu_command_client.send_standard_command(
            acu.CMD_MODE, ss, acu.CMD_DRIVE_TO_STOW, param1, rate
        )

        self.flow_control[subsystem]["event"]["drive_to_stow"]["thread"].clear()
        result = {}
        thread = threading.Thread(
            name="{}-drive-to-stow-blocking-thread".format(subsystem),
            target=self.blocking_flow_control,
            args=[subsystem, "drive_to_stow", ack, result, 300],
        )
        thread.start()
        self.flow_control_logging(
            subsystem, "drive_to_stow", "{} driving to stow ...".format(subsystem)
        )
        self.logger.logInfo("{} arrived at stow position".format(subsystem))
        thread.join()
        return result["status"]

    def stopstow(self, subsystem, pinselect):
        allowed_subsystems = [AZ, EL]

        if subsystem not in allowed_subsystems:
            self.logger.logWarning(
                "{} not in allowed list {}. Do nothing".format(
                    subsystem, allowed_subsystems
                )
            )
            return tnrtAntennaMod.PARAM_ERROR

        ss = tc.subsystem_properties[subsystem]["acu_code"]
        self.logger.logDebug("found subsystem {}".format(ss))
        param1 = tc.enum_properties[pinselect]["acu_code"]

        ack = self.acu_command_client.send_standard_command(
            acu.CMD_MODE, ss, acu.CMD_STOW_STOP, param1, 0.0
        )
        return tc.ack_properties[ack.status]["idl_enum"]

    def driveToPark(self, subsystem, rate):
        allowed_subsystems = [AZ, EL, THU, GRS]

        if subsystem not in allowed_subsystems:
            self.logger.logWarning(
                "{} not in allowed list {}. Do nothing".format(
                    subsystem, allowed_subsystems
                )
            )
            return tnrtAntennaMod.PARAM_ERROR

        if np.abs(rate) > tc.subsystem_properties[subsystem]["P108_v_MaxSys"]:
            self.logger.logError("slew rate too fast")
            raise (
                tnrtAntennaErrorImpl.paramOutOfLimitsExImpl(
                    ACSErr.NameValue(
                        "errExpl",
                        "slew rate abs({}) over limit {}".format(
                            rate, tc.subsystem_properties[subsystem]["P108_v_MaxSys"]
                        ),
                    )
                )
            )

        ss = tc.subsystem_properties[subsystem]["acu_code"]
        self.logger.logDebug("found subsystem {}".format(ss))

        ack = self.acu_command_client.send_standard_command(
            acu.CMD_MODE, ss, acu.CMD_DRIVE_TO_PARK, 0.0, rate
        )
        return tc.ack_properties[ack.status]["idl_enum"]

    # ------ IDL FUNCTIONS: ACU PARAMETER COMMANDS   -------------------------------------
    def setMaster(self, mastercontrol_enum):
        self.logger.logDebug("setMaster {}".format(mastercontrol_enum))
        # If the user selects HHP (hand-held panel),
        # ignore because the HHP mode can be activated by connecting
        # the HHP hardware device and activate with local panel switch.
        if mastercontrol_enum == tnrtAntennaMod.LOCAL_HHP:
            self.logger.logWarning(
                "HHP cannot be enabled from remote command.  Activate from HHP directly"
            )
            return tnrtAntennaMod.PARAM_ERROR

        # Look up the ACU command value from the enum (used by ACS Object Explorer GUI)
        param1 = tc.enum_properties[mastercontrol_enum]["acu_code"]

        ack = self.acu_command_client.send_standard_command(
            acu.CMD_PARAM, acu.SUBSYSTEM_SYSTEM, acu.CMD_SET_MASTER, param1, 0.0
        )
        return tc.ack_properties[ack.status]["idl_enum"]

    def setFan(self, fanmode_enum):
        self.logger.logDebug("setFan {}".format(fanmode_enum))
        # Look up the ACU command value from the enum (used by ACS Object Explorer GUI)
        param1 = tc.enum_properties[fanmode_enum]["acu_code"]

        ack = self.acu_command_client.send_standard_command(
            acu.CMD_PARAM, acu.SUBSYSTEM_SYSTEM, acu.CMD_SET_FAN, param1, 0.0
        )
        return tc.ack_properties[ack.status]["idl_enum"]

    def m3SyncEl(self, syncmode_enum):
        self.logger.logDebug("m3SyncEl {}".format(syncmode_enum))
        # Look up the ACU command value from the enum (used by ACS Object Explorer GUI)
        param1 = tc.enum_properties[syncmode_enum]["acu_code"]

        ack = self.acu_command_client.send_standard_command(
            acu.CMD_PARAM, acu.SUBSYSTEM_SYSTEM, acu.CMD_SET_M3SYNC_EL, param1, 0.0
        )
        return tc.ack_properties[ack.status]["idl_enum"]

    def positionOffset(self, subsystem, offset):
        allowed_subsystems = [AZ, EL, M3]

        if subsystem not in allowed_subsystems:
            self.logger.logWarning(
                "{} not in allowed list {}. Do nothing".format(
                    subsystem, allowed_subsystems
                )
            )
            return tnrtAntennaMod.PARAM_ERROR

        ss = tc.subsystem_properties[subsystem]["acu_code"]
        self.logger.logDebug("found subsystem {}".format(ss))

        ack = self.acu_command_client.send_standard_command(
            acu.CMD_PARAM, ss, acu.CMD_POSITION_OFFSET, offset, 0.0
        )
        return tc.ack_properties[ack.status]["idl_enum"]

    def setUserPointingCorrectionAz(self, corr_val):
        ss = tc.subsystem_properties[PO]["acu_code"]
        ack = self.acu_command_client.send_standard_command(
            acu.CMD_PARAM, ss, acu.CMD_SET_USER_POINTING_CORRECTION_AZ, 1.0, corr_val
        )
        return tc.ack_properties[ack.status]["idl_enum"]

    def setUserPointingCorrectionEl(self, corr_val):
        ss = tc.subsystem_properties[PO]["acu_code"]
        ack = self.acu_command_client.send_standard_command(
            acu.CMD_PARAM, ss, acu.CMD_SET_USER_POINTING_CORRECTION_EL, 1.0, corr_val
        )
        return tc.ack_properties[ack.status]["idl_enum"]

    def trackingOffsetTime(self, offset):
        ss = tc.subsystem_properties[TR]["acu_code"]
        ack = self.acu_command_client.send_standard_command(
            acu.CMD_PARAM, ss, acu.CMD_SET_TRACKING_OFFSET_TIME, 1.0, offset
        )
        return tc.ack_properties[ack.status]["idl_enum"]

    def timeSource(self, tSource, mjd):
        ss = tc.subsystem_properties[TR]["acu_code"]
        param1 = tc.enum_properties[tSource]["acu_code"]
        ack = self.acu_command_client.send_standard_command(
            acu.CMD_PARAM, ss, acu.CMD_TIME_SOURCE, param1, mjd
        )
        return tc.ack_properties[ack.status]["idl_enum"]

    def timeOffset(self, tOffsetMode, offset):
        ss = tc.subsystem_properties[TR]["acu_code"]
        param1 = tc.enum_properties[tOffsetMode]["acu_code"]

        ack = self.acu_command_client.send_standard_command(
            acu.CMD_PARAM, ss, acu.CMD_TIME_OFFSET, param1, offset
        )
        return tc.ack_properties[ack.status]["idl_enum"]

    def setHexapodElevationOffsetTableEnabled(self, elTableMode):
        ss = tc.subsystem_properties[HXP]["acu_code"]
        param1 = tc.enum_properties[elTableMode]["acu_code"]

        ack = self.acu_command_client.send_standard_command(
            acu.CMD_PARAM, ss, acu.CMD_HEXAPOD_ELEVATION_OFFSET_TABLE_ON, param1, 0.0
        )
        return tc.ack_properties[ack.status]["idl_enum"]

    def setCorrectionValuesEnabled(self, corType, corMode):
        ss = tc.subsystem_properties[PO]["acu_code"]
        param1 = tc.enum_properties[corType]["acu_code"]
        param2 = tc.enum_properties[corMode]["acu_code"]

        ack = self.acu_command_client.send_standard_command(
            acu.CMD_PARAM, ss, acu.CMD_CORRECTION_VALUES_ON, param1, param2
        )
        return tc.ack_properties[ack.status]["idl_enum"]

    def setDUT1AndGST0(self, dut1, gst0):
        self.logger.logDebug("DUT1 [ms]: {:d}, GST0hUT1 [ms]: {:d}".format(int(dut1), int(gst0)))
        ss = tc.subsystem_properties[TR]["acu_code"]
        if np.abs(dut1) > 500:
            self.logger.logError("dut1 > limit 500 ms")
            raise (
                tnrtAntennaErrorImpl.paramOutOfLimitsExImpl(
                    ACSErr.NameValue("errExpl", "dut1 {}  over limit {}".format(dut1))
                )
            )
        if (0 > gst0) or (gst0 > 86400000):
            self.logger.logError("dut1 > limit 500 ms")
            raise (
                tnrtAntennaErrorImpl.paramOutOfLimitsExImpl(
                    ACSErr.NameValue("errExpl", "dut1 {}  over limit {}".format(dut1))
                )
            )

        ack = self.acu_command_client.send_standard_command(
            acu.CMD_PARAM, ss, acu.CMD_SET_DUT1_GST0, dut1, gst0
        )
        return tc.ack_properties[ack.status]["idl_enum"]

    def hexapodPositionOffset(self, posOffMode, posOff):
        ss = tc.subsystem_properties[HXP]["acu_code"]
        param1 =  tc.enum_properties[posOffMode]["acu_code"]
        ack = self.acu_command_client.send_standard_command(
            acu.CMD_PARAM, ss, acu.CMD_HEXAPOD_POSITION_OFFSET, param1, posOff
        )
        return tc.ack_properties[ack.status]["idl_enum"]

    def enableTracking(self, enableState):
        ss = tc.subsystem_properties[TR]["acu_code"]
        param1 = param1 = tc.enum_properties[enableState]["acu_code"]
        ack = self.acu_command_client.send_standard_command(
            acu.CMD_PARAM, ss, acu.CMD_ENABLE_TRACKING, param1, 0.0
        )
        return tc.ack_properties[ack.status]["idl_enum"]

    def stopTracking(self):
        ss = tc.subsystem_properties[TR]["acu_code"]
        ack = self.acu_command_client.send_standard_command(
            acu.CMD_PARAM, ss, acu.CMD_STOP_TRACKING, 0.0, 0.0
        )
        return tc.ack_properties[ack.status]["idl_enum"]

    # ------ IDL FUNCTIONS: ACU SPECIAL COMMANDS   ---------------------------------------
    def loadProgramTrackTable(
        self, load_mode, interpolate_mode, track_mode, sequenceLength, timeAzRaElDecList):
        self.logger.logDebug("load_mode: {}, interpolate_mode: {}, track_mode: {}, length: {}, list: {}".format(
            load_mode, interpolate_mode, track_mode, sequenceLength, timeAzRaElDecList))
        if acu.M1_TRACK_TABLE_LEN_MIN > sequenceLength > acu.M1_TRACK_TABLE_LEN_MAX:
            self.logger.logError(
                "sequence length {} is out of range {} .. {}".format(
                    sequenceLength,
                    acu.M1_TRACK_TABLE_LEN_MIN,
                    acu.M1_TRACK_TABLE_LEN_MAX,
                )
            )
            raise (
                tnrtAntennaErrorImpl.paramOutOfLimitsExImpl(
                    ACSErr.NameValue("errExpl", "sequenceLength {}  out of range {} to {}".format(
                        sequenceLength, acu.M1_TRACK_TABLE_LEN_MIN, acu.M1_TRACK_TABLE_LEN_MAX))
                )
            )

        object_id = 1  # always 1
        lm = tc.enum_properties[load_mode]["acu_code"]
        im = tc.enum_properties[interpolate_mode]["acu_code"]
        tm = tc.enum_properties[track_mode]["acu_code"]

        # The track table send in the binary packet always has 50 items of (time, x, y).  sequenceLength tells the ACU
        # how many item to use for tracking.  Items in the array after sequenceLength are ignored
        table = tc.from_idl_timeAzRaElDecList(timeAzRaElDecList)
        self.logger.logDebug(table[:])
        ack = self.acu_command_client.send_track_table_command(
            acu.CMD_LOAD_PROGRAM_TRACK_TABLE,
            object_id,
            lm,
            im,
            tm,
            sequenceLength,
            table,
        )
        return tc.ack_properties[ack.status]["idl_enum"]

    def loadProgramOffsetTable(
        self, load_mode, interpolate_mode, track_mode, sequenceLength, timeAzRaElDecList
    ):
        if acu.M1_TRACK_TABLE_LEN_MIN > sequenceLength > acu.M1_TRACK_TABLE_LEN_MAX:
            self.logger.logError(
                "sequence length {} is out of range {} .. {}".format(
                    sequenceLength,
                    acu.M1_TRACK_TABLE_LEN_MIN,
                    acu.M1_TRACK_TABLE_LEN_MAX,
                )
            )
            raise (
                tnrtAntennaErrorImpl.paramOutOfLimitsExImpl(
                    ACSErr.NameValue(
                        "errExpl",
                        "sequence length {} is out of range {} .. {}".format(
                            sequenceLength,
                            acu.M1_TRACK_TABLE_LEN_MIN,
                            acu.M1_TRACK_TABLE_LEN_MAX,
                        ),
                    )
                )
            )

        object_id = 1  # always 1
        lm = tc.enum_properties[load_mode]["acu_code"]
        im = tc.enum_properties[interpolate_mode]["acu_code"]
        tm = tc.enum_properties[track_mode]["acu_code"]

        # The track table send in the binary packet always has 50 items of (time, x, y).  sequenceLength tells the ACU
        # how many item to use for tracking.  Items in the array after sequenceLength are ignored
        table = tc.from_idl_timeAzRaElDecList(timeAzRaElDecList)
        
        if track_mode == tnrtAntennaMod.azel:
            # Clear old program offset table to ensure that the tracking_sts.desired_el includes
            # only program track table (no effect from previous program offset)
            self.__clear_program_offset_table()

            for po_list_item in table:
                # pre-distort the Azimuth offset values in the coordinate system of the ACU
                # axis to create the correct scan pattern when it is projected onto the sky at EL > 0

                # Also, add the elevation program offset of each point in the calculation of pre-distortion.
                # The program offset values will end up in the axis desired position after we load the
                # program offset table.
                el = self.acu_status.axis_status[1].desired_position + po_list_item.el_dec
                el_projection_factor = np.cos(np.radians(el))
                self.logger.logDebug("EL = {}, Divide AZ offsets by cos(EL) = {}".format(el, el_projection_factor))
                po_list_item.az_ra = po_list_item.az_ra / el_projection_factor
        else:
            self.logger.logDebug("Program offset is *not* AZ, EL coords")
            self.logger.logDebug("Do *not* multiply AZ offsets by cos(EL)")
        
        self.logger.logDebug("po_table: {}".format(table[:]))
        ack = self.acu_command_client.send_track_table_command(
            acu.CMD_LOAD_PROGRAM_OFFSET_TABLE,
            object_id,
            lm,
            im,
            tm,
            sequenceLength,
            table,
        )
        return tc.ack_properties[ack.status]["idl_enum"]

    def loadProgramTrackTableHexapod(
        self, load_mode, interpolate_mode, sequenceLength, timeXYZTXTYTZList
    ):
        if acu.HXP_TRACK_TABLE_LEN_MIN > sequenceLength > acu.HXP_TRACK_TABLE_LEN_MAX:
            self.logger.logError(
                "sequence length {} is out of range {} .. {}".format(
                    sequenceLength,
                    acu.HXP_TRACK_TABLE_LEN_MIN,
                    acu.HXP_TRACK_TABLE_LEN_MAX,
                )
            )
            raise (
                tnrtAntennaErrorImpl.paramOutOfLimitsExImpl(
                    ACSErr.NameValue(
                        "errExpl",
                        "sequence length {} is out of range {} .. {}".format(
                            sequenceLength,
                            acu.HXP_TRACK_TABLE_LEN_MIN,
                            acu.HXP_TRACK_TABLE_LEN_MAX,
                        ),
                    )
                )
            )

        lm = tc.enum_properties[load_mode]["acu_code"]
        im = tc.enum_properties[interpolate_mode]["acu_code"]

        table = tc.from_idl_timeXYZTXTYTZList(timeXYZTXTYTZList)
        self.logger.logDebug(table[:])
        ack = self.acu_command_client.send_track_table_hexapod_command(
            lm, im, sequenceLength, table
        )
        return tc.ack_properties[ack.status]["idl_enum"]

    def loadHexapodElevationOffsetTable(self, sequenceLength, elXYZTXTYTZList):
        if acu.HXP_EL_TABLE_LEN_MIN > sequenceLength > acu.HXP_EL_TABLE_LEN_MAX:
            self.logger.logError(
                "sequence length {} is out of range {} .. {}".format(
                    sequenceLength, acu.HXP_EL_TABLE_LEN_MIN, acu.HXP_EL_TABLE_LEN_MAX
                )
            )
            raise (
                tnrtAntennaErrorImpl.paramOutOfLimitsExImpl(
                    ACSErr.NameValue(
                        "errExpl",
                        "sequence length {} is out of range {} .. {}".format(
                            sequenceLength,
                            acu.HXP_EL_TABLE_LEN_MIN,
                            acu.HXP_EL_TABLE_LEN_MAX,
                        ),
                    )
                )
            )

        table = tc.from_idl_elXYZTXTYTZList(elXYZTXTYTZList)
        self.logger.logDebug(table[:])
        ack = self.acu_command_client.send_elevation_table_hexapod_command(
            sequenceLength, table
        )
        return tc.ack_properties[ack.status]["idl_enum"]

    def setPointingModel(self, pointingModel):
        table = tc.from_idl_REAL64_array_9(pointingModel)
        self.logger.logDebug(table[:])
        ack = self.acu_command_client.send_pointing_model_command(table)
        return tc.ack_properties[ack.status]["idl_enum"]

    def setRefractionCorrection(self, r0, b1, b2):
        self.logger.logDebug("r0: {} b1: {} b2: {}".format(r0, b1, b2))
        if -1296000 > r0 > 1296000:
            self.logger.logError(
                "r0 {} is out of range {} .. {}".format(r0, -1296000, 1296000)
            )
            raise (
                tnrtAntennaErrorImpl.paramOutOfLimitsExImpl(
                    ACSErr.NameValue(
                        "errExpl",
                        "r0 {} is out of range {} .. {}".format(r0, -1296000, 1296000),
                    )
                )
            )

        if -360 > b1 > 360:
            self.logger.logError("b1 {} is out of range {} .. {}".format(b1, -360, 360))
            raise (
                tnrtAntennaErrorImpl.paramOutOfLimitsExImpl(
                    ACSErr.NameValue(
                        "errExpl",
                        "b1 {} is out of range {} .. {}".format(b1, -360, 360),
                    )
                )
            )

        ack = self.acu_command_client.send_refraction_command(r0, b1, b2)
        return tc.ack_properties[ack.status]["idl_enum"]

    def loadNORAD(self, noradName, noradLine1, noradLine2):
        # Convert the strings from IDL interface into byte arrays prepare to send to ACU
        tle_name_bytes = tc.from_idl_noradTLELine(noradName)
        tle_line1_bytes = tc.from_idl_noradTLELine(noradLine1)
        tle_line2_bytes = tc.from_idl_noradTLELine(noradLine2)
        ack = self.acu_command_client.send_norad_command(
            tle_name_bytes, tle_line1_bytes, tle_line2_bytes
        )
        return tc.ack_properties[ack.status]["idl_enum"]

    def setTrackMode(self, trackDataSource):
        ds = tc.enum_properties[trackDataSource]["acu_code"]
        ack = self.acu_command_client.send_set_track_mode_command(ds)
        return tc.ack_properties[ack.status]["idl_enum"]

    # ------ IDL FUNCTIONS: ANTENNA HIGH LEVEL FUNCTIONS ---------------------------------
    # These functions will use the lower-level ACU commands, ACU status, and additiona logic
    # to perform higher level functions.
    
    def azError(self):
        """
        Returns the azimuth error of the antenna in an ideal reference system.
        Axis status: desired position does not indicate the effects of pointing model
        corrections.  We trust that the effect of pointing model correctoin "pre-distortion" 
        compensates for physical structure outside the logic of axis encoder position and desired position. 
        """
        return 3600 * (
            self.acu_status.axis_status[0].actual_position - self.acu_status.axis_status[0].desired_position
            )

    def elError(self):
        """
        Returns the elevation error of the antenna in an ideal reference system.
        Axis status: desired position does not indicate the effects of pointing model
        corrections.  We trust that the effect of pointing model correctoin "pre-distortion" 
        compensates for physical structure outside the logic of axis encoder position and desired position.
        """
        return 3600 * (
            self.acu_status.axis_status[1].actual_position - self.acu_status.axis_status[1].desired_position
            )

    def M2status(self):
        """Returns the status of M2 position system, the position [mm] and [degs] relative
        to the M2 correction table with elevation, error in positioning and correction
        positions according to elevation table.
        To obtain the absolute positions of the subreflector it is necessary to add the
        predicted values according to elevation with the relative positions.
        """

        # Predicted corrections for M2 dependning on elevation.
        elx, ely, elz, eltx, elty = self.__getM2positionFromEl(self.getElPosition())

        hxpsts = self.getHexapodStatus()
        statusState = hxpsts.state
        statusMode = hxpsts.mode

        # M2 Offsets due to loaded elevation table
        xoff = hxpsts.offset[0]
        yoff = hxpsts.offset[1]
        zoff = hxpsts.offset[2]
        txoff = hxpsts.offset[3]
        tyoff = hxpsts.offset[4]

        # IMPORTANT. These are relative positions to the elevation table
        # TODO. We should check first if there is correction table loaded
        # This is not available in the status message
        x = hxpsts.pos_actual[0] - elx
        errorx = hxpsts.pos_desired[0] - hxpsts.pos_actual[0]
        y = hxpsts.pos_actual[1] - ely
        errory = hxpsts.pos_desired[1] - hxpsts.pos_actual[1]
        z = hxpsts.pos_actual[2] - elz
        errorz = hxpsts.pos_desired[2] - hxpsts.pos_actual[2]
        tx = hxpsts.pos_actual[3] - eltx
        errortx = hxpsts.pos_desired[3] - hxpsts.pos_actual[3]
        ty = hxpsts.pos_actual[4] - elty
        errorty = hxpsts.pos_desired[4] - hxpsts.pos_actual[4]

        vx = hxpsts.velocity_actual[0]
        vy = hxpsts.velocity_actual[1]
        vz = hxpsts.velocity_actual[2]
        vtx = hxpsts.velocity_actual[3]
        vty = hxpsts.velocity_actual[4]

        vstop = 1e-4

        if statusState == acu.AXIS_INACTIVE.value:
            motorStatus = tnrtAntennaMod.axis_inactive

        elif statusState == acu.AXIS_ACTIVE.value:

            if (
                statusMode == acu.MODE_IGNORE.value
                or statusMode == acu.MODE_INACTIVE.value
            ):
                motorStatus = tnrtAntennaMod.axis_inactive

            elif statusMode == acu.MODE_TRACKING_CONTROLLER.value:
                if (
                    abs(vx) < vstop
                    and abs(vy) < vstop
                    and abs(vz) < vstop
                    and abs(vtx) < vstop
                    and abs(vty) < vstop
                ):
                    motorStatus = tnrtAntennaMod.tracking
                else:
                    motorStatus = tnrtAntennaMod.slewing
            else:
                motorStatus = tnrtAntennaMod.stopped

        else:
            motorStatus = tnrtAntennaMod.hasErrors

        m2s = tnrtAntennaMod.m2StatusStructure(
            motorStatus,
            x,
            y,
            z,
            tx,
            ty,
            errorx,
            errory,
            errorz,
            errortx,
            errorty,
            elx,
            ely,
            elz,
            eltx,
            elty,
        )

        return m2s

    def antennaStatus(self):
        #   '''returns the status of the antenna. This is tricky since the ACU provides the status
        # of both az and el (main axis) drives and it is difficult to summarize both states in one.
        # We also have to take into account the posAzLoop and posElLoop status. All four properties
        # are combined to produce one easy to understand antenna status.
        # This method uses ifs but no elif sentences. The order is very important because the
        # ifs have got or instructions inside.
        # The antenna may be in the following states:
        # hasErrors, inactive, interlocked, halfstowed, stowed, stowing, unstowing, hasErrors, stopped, slewing, tracking, unknownState
        # '''
        #
        # #TODO exceptions.
        #
        #
        #
        #    #state
        #    #enum stsAxisState: AS_INACTIVE, AS_DEACTIVATING, AS_ACTIVATING, AS_ACTIVE };
        #    #trajectoryState
        #    #enum stsAxisTrajectoryState: AT_OFF, AT_HOLDING, AT_EMERGENCY_STOP, AT_STOP, AT_SLEWING_VELOCITY, AT_POSITION, AT_TRACKING };
        #    #actualMode
        #    #enum stsAxisMode: AM_IGNORE, AM_INACTIVE, AM_ACTIVE, AM_ABS_POS, AM_REL_POS, AM_SLEW, AM_STOP, AM_TR_CTRL_OUTPUT, AM_SUN_TRACK, AM_INTERLOCK, AM_RESET, AM_STOW, AM_UNSTOW, AM_DRIVE_TO_STOW, AM_STOP_STOW_PIN, AM_DRIVE_TO_PARK };

        axisAzStatus = self.acu_status.axis_status[0]
        axisElStatus = self.acu_status.axis_status[1]

        statusAzState = axisAzStatus.state
        statusElState = axisElStatus.state

        statusAzTraj = axisAzStatus.trajectory_state
        statusElTraj = axisElStatus.trajectory_state

        statusAzMode = axisAzStatus.actual_mode
        statusElMode = axisElStatus.actual_mode

        statusAzError = axisAzStatus.error_status
        statusElError = axisElStatus.error_status

        vaz = axisAzStatus.desired_velocity
        vel = axisElStatus.desired_velocity

        stowAz = self.stowPin1InAz()
        stowEl1 = self.stowPin1InEl()
        stowEl2 = self.stowPin2InEl()

        unstowAz = self.stowPin1OutAz()
        unstowEl1 = self.stowPin1OutEl()
        unstowEl2 = self.stowPin2OutEl()

        progTrackStatusWord = self.getProgramTrackStatus()

        # degs/s
        vStop = 1e-3
        vTrack = 5e-3  # This is a critical value and may need refinement.
        self.__aStatus = None

        # Beware the order is important (PdV)

        if statusAzError > 0 or statusElError > 0:
            self.__aStatus = tnrtAntennaMod.hasErrors
            self.__aStatusIndex = 1

            if (
                statusAzMode == acu.MODE_INTERLOCK.value
                or statusElMode == acu.MODE_INTERLOCK.value
            ):
                self.__aStatus = tnrtAntennaMod.interlocked
                self.__aStatusIndex = 3

            self.logger.logDebug("returning status: {}".format(self.__aStatus))
            return self.__aStatus

        else:
            self.logger.logDebug("HAS ERRORS: %s %s" % (statusAzError, statusElError))

        # We can arrive at stow position from the LCP

        if stowAz and stowEl1 and stowEl2:
            self.__aStatus = tnrtAntennaMod.stowed
            self.__aStatusIndex = 5
            self.logger.logDebug("returning status: {}".format(self.__aStatus))
            return self.__aStatus
        elif stowAz or stowEl1 or stowEl2:
            self.__aStatus = tnrtAntennaMod.halfstowed
            self.__aStatusIndex = 4
            self.logger.logDebug("returning status: {}".format(self.__aStatus))
            return self.__aStatus

        # If any of the axis is in inactive state the antenna is considered to be in inactive state

        if (
            statusAzMode == acu.MODE_INACTIVE.value
            or statusElMode == acu.MODE_INACTIVE.value
        ):
            self.__aStatus = tnrtAntennaMod.axis_inactive
            self.__aStatusIndex = 2
            self.logger.logDebug("returning status: {}".format(self.__aStatus))
            return self.__aStatus

        # If any of the axis has been reset the antenna is considered to be in inactive state

        if statusAzMode == acu.MODE_RESET.value or statusElMode == acu.MODE_RESET.value:
            self.__aStatus = tnrtAntennaMod.axis_inactive
            self.__aStatusIndex = 2
            self.logger.logDebug("returning status: {}".format(self.__aStatus))
            return self.__aStatus

            # If any of the axis has been set to interlock the antenna is  considered to be in interlock
        if (
            statusAzMode == acu.MODE_INTERLOCK.value
            or statusElMode == acu.MODE_INTERLOCK.value
        ):
            self.__aStatus = tnrtAntennaMod.interlocked
            self.__aStatusIndex = 3
            self.logger.logDebug("returning status: {}".format(self.__aStatus))
            return self.__aStatus

        # If any of the axis is stowed the antenna is considered to be stowed
        # tnrtAntennaMod.AM_STOW acu.MODE_STOW.value
        if statusAzMode == acu.MODE_STOW.value or statusElMode == acu.MODE_STOW.value:
            if stowAz and stowEl1 and stowEl2:
                self.__aStatus = tnrtAntennaMod.stowed
                self.__aStatusIndex = 5
            elif stowAz or stowEl1 or stowEl2:
                self.__aStatus = tnrtAntennaMod.halfstowed
                self.__aStatusIndex = 4
            elif unstowAz and unstowEl1 and unstowEl2:
                self.__aStatus = tnrtAntennaMod.stowing
                self.__aStatusIndex = 6
            else:
                self.__aStatus = tnrtAntennaMod.halfstowed
                self.__aStatusIndex = 4
            self.logger.logDebug("returning status: {}".format(self.__aStatus))
            return self.__aStatus

        # tnrtAntennaMod.AM_UNSTOW acu.MODE_UNSTOW.value
        if (
            statusAzMode == acu.MODE_UNSTOW.value
            or statusElMode == acu.MODE_UNSTOW.value
        ):
            if unstowAz and unstowEl1 and unstowEl2:
                # tnrtAntennaMod.AM_INACTIVE acu.MODE_INACTIVE.value
                if (
                    statusAzState == acu.MODE_INACTIVE.value
                    or statusElState == acu.MODE_INACTIVE.value
                ):
                    self.__aStatus = tnrtAntennaMod.axis_inactive
                    self.__aStatusIndex = 9
                else:
                    self.__aStatus = tnrtAntennaMod.stopped
                    self.__aStatusIndex = 2
            elif stowAz or stowEl1 or stowEl2:
                self.__aStatus = tnrtAntennaMod.halfstowed
                self.__aStatusIndex = 7
            else:
                self.__aStatus = tnrtAntennaMod.unstowing
                self.__aStatusIndex = 7
            self.logger.logDebug("returning status: {}".format(self.__aStatus))
            return self.__aStatus
            # tnrtAntennaMod.AM_DRIVE_TO_STOW acu.MODE_DRIVE_TO_STOW.value
        if (
            statusAzMode == acu.MODE_DRIVE_TO_STOW.value
            or statusElMode == acu.MODE_DRIVE_TO_STOW.value
        ):
            if stowAz and stowEl1 and stowEl2:
                self.__aStatus = tnrtAntennaMod.stowed
                self.__aStatusIndex = 5
            elif stowAz or stowEl1 or stowEl2:
                self.__aStatus = tnrtAntennaMod.halfstowed
                self.__aStatusIndex = 4
            else:
                self.__aStatus = tnrtAntennaMod.stowing
                self.__aStatusIndex = 6
            self.logger.logDebug("returning status: {}".format(self.__aStatus))
            return self.__aStatus
            # tnrtAntennaMod.AM_DRIVE_TO_PARK acu.MODE_DRIVE_TO_PARK.value
        if (
            statusAzMode == acu.MODE_DRIVE_TO_PARK.value
            or statusElMode == acu.MODE_DRIVE_TO_PARK.value
        ):
            self.__aStatus = tnrtAntennaMod.slewing
            self.__aStatusIndex = 10
            self.logger.logDebug("returning status: {}".format(self.__aStatus))
            return self.__aStatus
            # tnrtAntennaMod.AM_SLEW acu.MODE_SLEW.value
        if statusAzMode == acu.MODE_SLEW.value or statusElMode == acu.MODE_SLEW.value:
            self.__aStatus = tnrtAntennaMod.slewing
            self.__aStatusIndex = 10

            if abs(vaz) < vStop and abs(vel) < vStop:
                self.__aStatus = tnrtAntennaMod.stopped
                self.__aStatusIndex = 9
            self.logger.logDebug("returning status: {}".format(self.__aStatus))
            return self.__aStatus
            # tnrtAntennaMod.AM_ACTIVE acu.MODE_ACTIVE.value
        if (
            statusAzMode == acu.MODE_ACTIVE.value
            and statusElMode == acu.MODE_ACTIVE.value
        ):
            self.__aStatus = tnrtAntennaMod.stopped
            self.__aStatusIndex = 9
            self.logger.logDebug("returning status: {}".format(self.__aStatus))
            return self.__aStatus

            # Both axis have to be stopped.
            # tnrtAntennaMod.AM_ACTIVE acu.MODE_ACTIVE.value
        if (
            statusAzMode == acu.MODE_ACTIVE.value
            or statusElMode == acu.MODE_ACTIVE.value
        ):
            self.__aStatus = tnrtAntennaMod.slewing
            self.__aStatusIndex = 10

            if abs(vaz) < vStop and abs(vel) < vStop:
                self.__aStatus = tnrtAntennaMod.stopped
                self.__aStatusIndex = 9
            self.logger.logDebug("returning status: {}".format(self.__aStatus))
            return self.__aStatus
            # tnrtAntennaMod.AM_ABS_POS acu.MODE_ABS_POS.value
        if (
            statusAzMode == acu.MODE_ABS_POS.value
            and statusElMode == acu.MODE_ABS_POS.value
        ):
            self.__aStatus = tnrtAntennaMod.slewing
            self.__aStatusIndex = 10

            if abs(vaz) < vStop and abs(vel) < vStop:
                self.__aStatus = tnrtAntennaMod.stopped
                self.__aStatusIndex = 9
            self.logger.logDebug("returning status: {}".format(self.__aStatus))
            return self.__aStatus
            # tnrtAntennaMod.AM_REL_POS acu.MODE_REL_POS.value
        if (
            statusAzMode == acu.MODE_REL_POS.value
            and statusElMode == acu.MODE_REL_POS.value
        ):
            self.__aStatus = tnrtAntennaMod.slewing
            self.__aStatusIndex = 10

            if abs(vaz) < vStop and abs(vel) < vStop:
                self.__aStatus = tnrtAntennaMod.stopped
                self.__aStatusIndex = 9
            self.logger.logDebug("returning status: {}".format(self.__aStatus))
            return self.__aStatus
            # tnrtAntennaMod.AM_STOP acu.MODE_STOP.value
        if statusAzMode == acu.MODE_STOP.value or statusElMode == acu.MODE_STOP.value:
            self.__aStatus = tnrtAntennaMod.slewing
            self.__aStatusIndex = 10

            if abs(vaz) < vStop and abs(vel) < vStop:
                self.__aStatus = tnrtAntennaMod.stopped
                self.__aStatusIndex = 9
            self.logger.logDebug("returning status: {}".format(self.__aStatus))
            return self.__aStatus
            # tnrtAntennaMod.AM_STOP acu.MODE_STOP.value
        if statusAzMode == acu.MODE_STOP.value and statusElMode == acu.MODE_STOP.value:
            self.__aStatus = tnrtAntennaMod.stopped
            self.__aStatusIndex = 9
            self.logger.logDebug("returning status: {}".format(self.__aStatus))
            return self.__aStatus
            # tnrtAntennaMod.AM_TR_CTRL_OUTPUT acu.MODE_TRACKING_CONTROLLER.value
        if (
            statusAzMode == acu.MODE_TRACKING_CONTROLLER.value
            or statusElMode == acu.MODE_TRACKING_CONTROLLER.value
        ):
            self.__aStatus = tnrtAntennaMod.stopped

            if self.__checkBit(progTrackStatusWord, 4):
                self.__aStatus = tnrtAntennaMod.tracking
            else:
                if abs(vaz) > vTrack or abs(vel) > vTrack:
                    self.__aStatus = tnrtAntennaMod.slewing
                else:
                    self.__aStatus = tnrtAntennaMod.stopped
            self.logger.logDebug("returning status: {}".format(self.__aStatus))
            return self.__aStatus
            # tnrtAntennaMod.AM_SUN_TRACK acu.MODE_SUN_TRACK.value
        if (
            statusAzMode == acu.MODE_SUN_TRACK.value
            or statusElMode == acu.MODE_SUN_TRACK.value
        ):
            self.__aStatus = tnrtAntennaMod.stopped

            if self.__checkBit(progTrackStatusWord, 4):
                self.__aStatus = tnrtAntennaMod.tracking
            else:
                if abs(vaz) > vTrack or abs(vel) > vTrack:
                    self.__aStatus = tnrtAntennaMod.slewing
                else:
                    self.__aStatus = tnrtAntennaMod.stopped
            self.__aStatus = tnrtAntennaMod.stopped

            self.logger.logDebug("returning status: {}".format(self.__aStatus))
            return self.__aStatus
            # tnrtAntennaMod.AM_IGNORE acu.MODE_IGNORE.value
        if (
            statusAzMode == acu.MODE_IGNORE.value
            or statusElMode == acu.MODE_IGNORE.value
        ):
            self.__aStatus = tnrtAntennaMod.unknownState
            self.__aStatusIndex = 12
            self.logger.logDebug("returning status: {}".format(self.__aStatus))
            return self.__aStatus

        self.__aStatus = tnrtAntennaMod.unknownState
        self.logger.logDebug("returning status: {}".format(self.__aStatus))
        return self.__aStatus

    def setHexapodElevationOffsetModel(self, subMode):
        """Load M2 table. The values are taken either from the default polynomial table or reset to 0
        This function: stop M2 motors, deactivates them, loads the table, activates M2 versus El and activates M2 again

        Important. Changes in X, Y, XTilt and YTilt change the pointing model
        @param subMode: tnrtAntennaMod.astroM2ElMode, tnrtAntennaMod.geoM2ElMode, tnrtAntennaMod.zeroM2ElMode
                tnrtAntennaMod.geoM2ElMode loads a subreflector mode with all coefficients equal to those at elevation 45 degs.
        """

        m2OffsetElSeq = []
        zTilt = 0

        # The other axis are correctable but seldom used
        self.__xCorrEl = [
            float(config.xCorrEl0),
            float(config.xCorrEl1),
            float(config.xCorrEl2),
            float(config.xCorrEl3),
        ]
        self.__yCorrEl = [
            float(config.yCorrEl0),
            float(config.yCorrEl1),
            float(config.yCorrEl2),
            float(config.yCorrEl3),
        ]
        self.__zCorrEl = [
            float(config.zCorrEl0),
            float(config.zCorrEl1),
            float(config.zCorrEl2),
            float(config.zCorrEl3),
        ]
        self.__txCorrEl = [
            float(config.txCorrEl0),
            float(config.txCorrEl1),
            float(config.txCorrEl2),
            float(config.txCorrEl3),
        ]
        self.__tyCorrEl = [
            float(config.tyCorrEl0),
            float(config.tyCorrEl1),
            float(config.tyCorrEl2),
            float(config.tyCorrEl3),
        ]

        if subMode == tnrtAntennaMod.astroM2ElMode:
            for i in range(0, acu.HXP_EL_TABLE_LEN_MAX):
                el = i * 90.0 / (acu.HXP_EL_TABLE_LEN_MAX - 1)
                (xOff, yOff, zOff, xTilt, yTilt) = self.__getM2positionFromEl(el)
                m2OffsetElSeq.append(
                    tnrtAntennaMod.elXYZTXTYTZStruct(
                        el, xOff, yOff, zOff, xTilt, yTilt, zTilt
                    )
                )
                self.logger.logDebug(
                    "astroM2ElMode el: %f xOff: %f yOff: %f zOff: %f xTilt: %f yTilt: %f zTilt: %f"
                    % (el, xOff, yOff, zOff, xTilt, yTilt, zTilt)
                )

        elif subMode == tnrtAntennaMod.geoM2ElMode:
            # This mode keeps Z, tiltX and tiltY at their best position at 45 degrees, but moves X and Y
            elGeo = 45
            xOff45, yOff45, zOff45, xTilt45, yTilt45 = self.__getM2positionFromEl(elGeo)
            for i in range(0, acu.HXP_EL_TABLE_LEN_MAX):
                el = i * 90.0 / (acu.HXP_EL_TABLE_LEN_MAX - 1)
                (xOff, yOff, zOff, xTilt, yTilt) = self.__getM2positionFromEl(el)
                m2OffsetElSeq.append(
                    tnrtAntennaMod.elXYZTXTYTZStruct(
                        el, xOff, yOff, zOff45, xTilt45, yTilt45, zTilt
                    )
                )
                self.logger.logDebug(
                    "geoM2ElMode el: %f xOff: %f yOff: %f zOff: %f xTilt: %f yTilt: %f zTilt: %f"
                    % (el, xOff, yOff, zOff45, xTilt45, yTilt45, zTilt)
                )

        elif subMode == tnrtAntennaMod.zeroM2ElMode:
            for i in range(0, acu.HXP_EL_TABLE_LEN_MAX):
                el = i * 90.0 / (acu.HXP_EL_TABLE_LEN_MAX - 1)
                (xOff, yOff, zOff, xTilt, yTilt) = (0.0, 0.0, 0.0, 0.0, 0.0)
                m2OffsetElSeq.append(
                    tnrtAntennaMod.elXYZTXTYTZStruct(
                        el, xOff, yOff, zOff, xTilt, yTilt, zTilt
                    )
                )
                self.logger.logDebug(
                    "el: %f xOff: %f yOff: %f zOff: %f xTilt: %f yTilt: %f zTilt: %f"
                    % (el, xOff, yOff, zOff, xTilt, yTilt, zTilt)
                )
        else:
            self.logger.logDebug("Unknown subreflector Mode: %s" % (subMode))
            raise tnrtAntennaErrorImpl.unknownParameterExImpl()

        seqLength = len(m2OffsetElSeq)
        strLine = "subreflector Mode seqLength : %s" % (seqLength)
        self.logger.logDebug(strLine)

        self.logger.logDebug("Deactivate HXP")

        self.deactivate(HXP)

        # Then load the table
        try:
            listFormat = ""
            for v in m2OffsetElSeq:
                listFormat = listFormat + ", %f, %f, %f, %f, %f, %f" % (
                    v.el,
                    v.x,
                    v.y,
                    v.z,
                    v.tx,
                    v.ty,
                )
            self.logger.logInfo(
                "tnrtAntennaMod.loadHexapodElevationOffsetTable(%d %s)"
                % (seqLength, listFormat)
            )

            ackCmd = self.loadHexapodElevationOffsetTable(seqLength, m2OffsetElSeq)

            self.logger.logDebug(
                "self.loadHexapodElevationOffsetTable(seqLength, m2OffsetElSeq)"
            )
        except (
            tnrtAntennaErrorImpl.unknownModeEx,
            tnrtAntennaErrorImpl.tableSizeOutOfLimitsEx,
            tnrtAntennaErrorImpl.writeErrorEx,
            tnrtAntennaErrorImpl.readErrorEx,
            tnrtAntennaErrorImpl.wrongAcknowledgementEx,
            tnrtAntennaErrorImpl.wrongAcuStatusEx,
        ) as e:
            raise tnrtAntennaErrorImpl.acuExceptionExImpl(exception=e)
        except Exception as e:
            strLine = (
                "Unknown ACU exception calling loadHexapodElevationOffsetTable() %s"
                % str(e)
            )
            newEx = tnrtAntennaErrorImpl.unknownAcuExceptionExImpl()
            newEx.addData = ("errExpl", strLine)
            raise newEx

        try:
            self.checkAcknowledgement(ackCmd)
        except (tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl) as e:
            raise tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl(exception=e)

        # Then set table on
        try:
            self.logger.logDebug(
                "Enable hexapod elevation offset table"
            )
            self.setHexapodElevationOffsetTableEnabled(tnrtAntennaMod.EL_TABLE_ON)
        except tnrtAntennaErrorImpl.acuExceptionExImpl as e:
            raise tnrtAntennaErrorImpl.acuExceptionExImpl(exception=e)
        except tnrtAntennaErrorImpl.unknownAcuExceptionExImpl as e:
            raise tnrtAntennaErrorImpl.unknownAcuExceptionExImpl(exception=e)
        except tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl as e:
            raise tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl(exception=e)

        # Activate HXP again
        try:
            self.logger.logDebug("Activate HXP")
            self.activate(HXP)
        except tnrtAntennaErrorImpl.acuExceptionExImpl as e:
            raise tnrtAntennaErrorImpl.acuExceptionExImpl(exception=e)
        except tnrtAntennaErrorImpl.unknownAcuExceptionExImpl as e:
            raise tnrtAntennaErrorImpl.unknownAcuExceptionExImpl(exception=e)
        except tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl as e:
            raise tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl(exception=e)

    def stopAntenna(self, stopMode):
        """Stops any movement in the main axis drive and unloads any table present for the main drives."""

        if stopMode == tnrtAntennaMod.fullStop:

            try:
                ackCmd = self.stop(AZ)
                ackCmd = self.stop(EL)
                ackCmd = self.stop(M3)
                ackCmd = self.stop(HXP)
            except (
                tnrtAntennaErrorImpl.acuExceptionExImpl,
                tnrtAntennaErrorImpl.unknownParameterExImpl,
                tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl,
                tnrtAntennaErrorImpl.impossiblePositionExImpl,
            ) as e:
                raise
            except Exception as e:
                strLine = "Unknown Antenna exception calling stopMotor() %s" % str(e)
                self.logger.logError(strLine)
                newEx = tnrtAntennaErrorImpl.unknownAcuExceptionExImpl()
                newEx.addData = ("errExpl", strLine)
                raise newEx

        elif stopMode == tnrtAntennaMod.stopM1:

            try:
                ackCmd = self.stop(AZ)
                ackCmd = self.stop(EL)
                ackCmd = self.stop(M3)
            except (
                tnrtAntennaErrorImpl.acuExceptionExImpl,
                tnrtAntennaErrorImpl.unknownParameterExImpl,
                tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl,
                tnrtAntennaErrorImpl.impossiblePositionExImpl,
            ) as e:
                raise
            except Exception as e:
                strLine = "Unknown Antenna exception calling stopMotor() %s" % str(e)
                self.logger.logError(strLine)
                newEx = tnrtAntennaErrorImpl.unknownAcuExceptionExImpl()
                newEx.addData = ("errExpl", strLine)
                raise newEx

    def gotoAzEl(self, azPos, elPos, azRate, elRate, blocking=True, update_period=5.0):
        """Method to send the antenna to a given azimuth and elevation. No tables
        @param az Azimuth in degrees
        @param el Elevation in degress
        """
        self.logger.logInfo("absolutePosition(AZ, {}, {}, {}, {})".format(azPos, azRate, blocking, update_period))
        # Note (SS 12/2022). Do not use blocking flow control in absolutePosition() in this function.
        # use the old logic in blockUntilTracking()
        ackCmd = self.absolutePosition(AZ, azPos, azRate, False)

        try:
            self.checkAcknowledgement(ackCmd)
        except (tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl) as e:
            raise tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl(exception=e)

        self.logger.logInfo("absolutePosition(EL, {}, {}, {}, {})".format(elPos, elRate, blocking, update_period))
        # Note (SS 12/2022). Do not use blocking flow control in absolutePosition() in this function.
        # use the old logic in blockUntilTracking()
        ackCmd = self.absolutePosition(EL, elPos, elRate, False)

        try:
            self.checkAcknowledgement(ackCmd)
        except (tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl) as e:
            raise tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl(exception=e)

        # If blocking is False, this function returns control immediately.
        # If blocking is True, this function returns after antenna has arrived within `tolerance`
        # arcseconds of commanded position.
        if blocking is True:
            self.blockUntilTracking(update_period)

    def setUserPointingCorrections(self, offaz_arcsec, offel_arcsec):
        """
        User temporary "fine" pointing correction.  If the user wants to add additional dynamic pointing correction,
        but not edit 9-point model.
        Az correction will be multiplied by 1/cos(EL) in real-time by ACU "dynamic pointing correction"
        """
        offaz_deg = offaz_arcsec / 3600.0
        offel_deg = offel_arcsec / 3600.0

        self.logger.logDebug(
            "set user pointing correction AZ {} [deg]".format(offaz_deg)
        )
        ackCmd = self.setUserPointingCorrectionAz(offaz_deg)

        try:
            self.checkAcknowledgement(ackCmd)
        except (tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl) as e:
            raise tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl(exception=e)

        self.logger.logDebug(
            "set set user pointing correction EL {} [deg]".format(offel_deg)
        )
        ackCmd = self.setUserPointingCorrectionEl(offel_deg)

        try:
            self.checkAcknowledgement(ackCmd)
        except (tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl) as e:
            raise tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl(exception=e)

    def trackCoordinateHO(
        self,
        start_mjd,
        duration,
        az,
        el,
        user_pointing_correction_az,
        user_pointing_correction_el,
        blocking=True,
        update_period=2.0,
    ):
        """
        Command end the antenna to a given azimuth and elevation using ACU Program Track Table of time, AZ, EL.
        Parameters
        ----------
        az : float
                    Azimuth in degrees
        el : float
                Elevation in degress
        start_mjd : float
                MJD to load into ACU tracking table.
        duration : float
                Time the antenna remains at the commanded position. units: seconds
        user_pointing_correction_az : float
                Azimuth dynamic pointing correction on sky during this scan.  
                ACU should scale this value by 1/cos(EL) when it commands the AZ axis to achieve constant *on-sky* correction
        user_pointing_correction_el : float
                Elevation constant pointing correction during this scan
        blocking : boolean
                The antenna can take time up to 3  minutes to arrive at command
                position.  If blocking=False, this function returns immediately while the ACU moves the
                antenna to the tracking table coordinates.  If blocking=True, this function returns only
                after ACU signals that the antenna has arrived at the AZ, EL coordinate
                in the tracking table.
        update_period : float
                Time interval that the antenna component will log a message
                to show countown until arrive at command coordinate.  This is a rough approximation, because
                it uses the slewtime() function to estimate the total transit time in advance.  Some mechanical
                conditions could cause the slewtime to be longer.  In that case, the countdown would go negative
                until Antenna arrives at command position.
        """

        if az < tc.subsystem_properties[AZ]["P104_p_FinEndDn"] or az > tc.subsystem_properties[AZ]["P105_p_FinEndUp"]:
            strLine = "ValueError: {} must be in range [{},{}]".format(
                tc.subsystem_properties[AZ]["P104_p_FinEndDn"],
                tc.subsystem_properties[AZ]["P105_p_FinEndUp"],
            )
            newEx = tnrtAntennaErrorImpl.impossiblePositionExImpl()
            newEx.addData = ("errExpl", strLine)
            raise newEx
        if el < tc.subsystem_properties[EL]["P104_p_FinEndDn"] or el > tc.subsystem_properties[AZ]["P105_p_FinEndUp"]:
            strLine = "Commanded elevation is not between (%f,%f)" % (
                tc.subsystem_properties[EL]["P104_p_FinEndDn"],
                tc.subsystem_properties[AZ]["P105_p_FinEndUp"],
            )
            newEx = tnrtAntennaErrorImpl.impossiblePositionExImpl()
            newEx.addData = ("errExpl", strLine)
            raise newEx

        if duration < 0:
            strLine = "Integration time is negative %f" % (duration)
            newEx = tnrtAntennaErrorImpl.negativeIntegrationTimeExImpl()
            newEx.addData = ("errExpl", strLine)
            raise newEx

        # This is the minimum number of lines required by the ACU to run a tracking table
        # Interpolation algorithm requires minimum number of lines.
        nlines = acu.M1_TRACK_TABLE_LEN_MIN

        interpolationMode = tnrtAntennaMod.SPLINE

        # Tracking mode: AZ-EL
        trackMode = tnrtAntennaMod.azel

        # Load mode: We will abort the previous observation:
        loadMode = tnrtAntennaMod.newL

        # start_mjd is already in MJD, so nothing to convert.
        # Generate a list of time steps for the tracking table
        timelist = (
            Time(start_mjd, format="mjd", scale="utc")
            + np.linspace(0, duration, nlines) * units.second
        )

        # Create an empty list.  Prepare to add timeAzRaElDecStruct
        programTrackTable = []

        # Fill in the rest of the table with the last number
        # improve this by using Python slice notation to fill the list -- not loop.
        for t in timelist:
            programTrackTable.append(tnrtAntennaMod.timeAzRaElDecStruct(t.mjd, az, el))

        self.setUserPointingCorrections(user_pointing_correction_az, user_pointing_correction_el)

        # MTM GUI: Tracking -> Program Track
        try:
            self.logger.logDebug("sending to ACU")

            listFormat = ""
            for v in programTrackTable:
                listFormat = listFormat + ", %f, %f, %f" % (
                    v.absTimeMJD,
                    v.azRa,
                    v.elDec,
                )
            self.logger.logInfo(
                "self.loadProgramTrackTable(%s, %s, %s, %d, %s)"
                % (loadMode, interpolationMode, trackMode, nlines, listFormat)
            )

            ackCmd = self.loadProgramTrackTable(
                loadMode, interpolationMode, trackMode, nlines, programTrackTable
            )

        except (
            tnrtAntennaError.paramOutOfLimitsEx,
            tnrtAntennaError.unknownModeEx,
            tnrtAntennaError.tableSizeOutOfLimitsEx,
            tnrtAntennaError.writeErrorEx,
            tnrtAntennaError.readErrorEx,
            tnrtAntennaError.wrongAcknowledgementEx,
            tnrtAntennaError.wrongAcuStatusEx,
        ) as e:
            raise tnrtAntennaErrorImpl.acuExceptionExImpl(exception=e)
        except Exception as e:
            strLine = "Unknown ACU exception calling loadProgramTrackTable() %s" % str(
                e
            )
            self.logger.logError(strLine)
            newEx = tnrtAntennaErrorImpl.unknownAcuExceptionExImpl()
            newEx.addData = ("errExpl", strLine)
            raise newEx
        try:
            self.checkAcknowledgement(ackCmd)
        except (tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl) as e:
            raise tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl(exception=e)

        # Set tracking controller to read data from Program Track Table and calculate real-time AZ , EL
        # MTM GUI: Tracking -> Tracking Control -> Set Tracking Options -> Program Track Table -> Set Selecttion
        try:
            self.setTrackMode(tnrtAntennaMod.PROGRAM_TRACK)
        except tnrtAntennaErrorImpl.acuExceptionExImpl as e:
            raise tnrtAntennaErrorImpl.acuExceptionExImpl(exception=e)
        except tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl as e:
            raise tnrtAntennaErrorImpl.unknownAcuExceptionExImpl(exception=e)
        except tnrtAntennaErrorImpl.unknownAcuExceptionExImpl as e:
            raise tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl(exception=e)

        # Activate tracking
        self.logger.logDebug("Enable tracking and activate axis if not yet active")
        self.enableTracking(tnrtAntennaMod.ENABLE_AND_ACTIVATE)

        # Connect tracking controller result to axis motor
        # MTM GUI: Tracking -> Tracking Control -> Execute Tracking Object
        self.logger.logDebug("self.programTrack({} {})".format(AZ, self.velocity[AZ]))
        ackCmd = self.programTrack(AZ, self.velocity[AZ])

        try:
            self.checkAcknowledgement(ackCmd)
        except (tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl) as e:
            raise tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl(exception=e)

        self.logger.logDebug("self.programTrack({} {})".format(EL, self.velocity[EL]))
        ackCmd = self.programTrack(EL, self.velocity[EL])

        try:
            self.checkAcknowledgement(ackCmd)
        except (tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl) as e:
            raise tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl(exception=e)

        # If blocking is False, this function returns control immediately.
        # If blocking is True, this function returns after antenna has arrived within `tolerance`
        # arcseconds of commanded position.
        if blocking is True:
            self.blockUntilTracking(update_period)

    def trackCoordinateTLE(
        self,
        noradName,
        noradLine1,
        noradLine2,
        user_pointing_correction_az,
        user_pointing_correction_el,
        blocking=True,
        update_period=2.0,
    ):
        """Send the antenna to track the TLE (Two-Line Element) orbit parameters of a satellite.
        MTM ACU uses the data from the TLE strings and the SGP4 model to calculate the real-time
        Azimuth and Elevation from site loction to the satellite current location on orbit.

        If the satellite is below the horizon, don't abort.  Let ACU wait until satellite is visible and
        start tracking automatically. (TODO: test if this works).

        For now, we only need to track Intelsat 22 which is Geostationary, so it will always be visible
        from Thailand.

        Parameters
        ----------
        noradName : np.dtype('a25')
                line 0 of TLE file.  Full name and short name of satellite
                String length = 25
        noradLine1 : np.dtype('a70')
                line 1 of TLE file.  Header (ID, launch date, date of TLE data update)
                String length = 70
        noradLine2:  np.dtype('a70')
                line 2 of TLE file.  Data (orbit parameters).
                String length = 70
        user_pointing_correction_az : float
                Azimuth dynamic pointing correction on sky during this scan.  
                ACU should scale this value by 1/cos(EL) when it commands the AZ axis to achieve constant *on-sky* correction
        user_pointing_correction_el : float
                Elevation constant pointing correction during this scan
        blocking : boolean
                The antenna can take time up to 3  minutes to arrive at command
                position.  If blocking=False, this function returns immediately while the ACU moves the
                antenna to the tracking table coordinates.  If blocking=True, this function returns only
                after ACU signals that the antenna has arrived at the AZ, EL coordinate
                in the tracking table.
        update_period : float
                Time interval that the antenna component will log a message
                to show countown until arrive at command coordinate.  This is a rough approximation, because
                it uses the slewtime() function to estimate the total transit time in advance.  Some mechanical
                conditions could cause the slewtime to be longer.  In that case, the countdown would go negative
                until Antenna arrives at command position.
        """
        self.setUserPointingCorrections(user_pointing_correction_az, user_pointing_correction_el)

        # MTM GUI: Tracking -> Satellite TLE Elements -> Send TLE Elements
        try:
            self.logger.logInfo(
                "self.loadNORAD(name: %s, line1: %s, line2: %s)"
                % (noradName, noradLine1, noradLine2)
            )
            ackCmd = self.loadNORAD(noradName, noradLine1, noradLine2)

        except (
            tnrtAntennaError.paramOutOfLimitsEx,
            tnrtAntennaError.unknownModeEx,
            tnrtAntennaError.tableSizeOutOfLimitsEx,
            tnrtAntennaError.writeErrorEx,
            tnrtAntennaError.readErrorEx,
            tnrtAntennaError.wrongAcknowledgementEx,
            tnrtAntennaError.wrongAcuStatusEx,
        ) as e:
            raise tnrtAntennaErrorImpl.acuExceptionExImpl(exception=e)
        except Exception as e:
            strLine = "Unknown ACU exception calling loadNORAD() %s" % str(e)
            self.logger.logError(strLine)
            newEx = tnrtAntennaErrorImpl.unknownAcuExceptionExImpl()
            newEx.addData = ("errExpl", strLine)
            raise newEx
        try:
            self.checkAcknowledgement(ackCmd)
        except (tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl) as e:
            raise tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl(exception=e)

        # Set tracking controller to read TLE data and calculate real-time AZ , EL
        # MTM GUI: Tracking -> Tracking Control -> Set Tracking Options -> TLE Track -> Set Selecttion
        try:
            self.setTrackMode(tnrtAntennaMod.TLE)
        except tnrtAntennaErrorImpl.acuExceptionExImpl as e:
            raise tnrtAntennaErrorImpl.acuExceptionExImpl(exception=e)
        except tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl as e:
            raise tnrtAntennaErrorImpl.unknownAcuExceptionExImpl(exception=e)
        except tnrtAntennaErrorImpl.unknownAcuExceptionExImpl as e:
            raise tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl(exception=e)

        # Activate tracking
        self.logger.logDebug("Enable tracking and activate axis if not yet active")
        self.enableTracking(tnrtAntennaMod.ENABLE_AND_ACTIVATE)

        # Connect tracking controller result to axis motor
        # MTM GUI: Tracking -> Tracking Control -> Execute Tracking Object
        self.logger.logDebug("self.programTrack({} {})".format(AZ, self.velocity[AZ]))
        ackCmd = self.programTrack(AZ, self.velocity[AZ])

        try:
            self.checkAcknowledgement(ackCmd)
        except (tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl) as e:
            raise tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl(exception=e)

        self.logger.logDebug("self.programTrack({} {})".format(EL, self.velocity[EL]))
        ackCmd = self.programTrack(EL, self.velocity[EL])

        try:
            self.checkAcknowledgement(ackCmd)
        except (tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl) as e:
            raise tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl(exception=e)

        # If blocking is False, this function returns control immediately.
        # If blocking is True, this function returns after antenna has arrived within `tolerance`
        # arcseconds of commanded position.
        if blocking is True:
            self.blockUntilTracking(update_period)

    def trackCoordinateEQ(
        self,
        start_mjd,
        ra_icrs,
        dec_icrs,
        pm_ra,
        pm_dec,
        parallax,
        radial_velocity,
        send_icrs_to_acu,
        user_pointing_correction_az,
        user_pointing_correction_el,
        blocking=True,
        update_period=2.0,
        ):
        """
        Generate Program Track table for RA, DEC.  Send to ACU to start tracking.

        Parameters
        ----------
        start_mjd : float
                Start time in UTC MJD to create the Program Track data table first entry {time, ra, dec} [unit = day]
        ra_icrs : float
                Right Ascension from catalog using ICRS coordinate system [unit = deg]
        dec_icrs : float
                Declination from catalog using ICRS coordinate system [unit = deg]
        pm_ra : float
                proper motion of right ascension * cos declination. [unit = mas / year], default=0

                https://docs.astropy.org/en/stable/coordinates/velocities.html
        pm_dec : float
                proper motion of right declination. [unit = mas / year], default=0

                https://docs.astropy.org/en/stable/coordinates/velocities.html
        parallax :  float
                parallax. [unit = arcsec], default=0
        radial_velocity : float
                The component of the velocity along the line-of-sight (i.e., the radial direction).
                [unit = km / s], default=0, + is moving away from observer. (-) approaching observer
        user_pointing_correction_az : float
                Azimuth dynamic pointing correction on sky during this scan.  
				ACU should scale this value by 1/cos(EL) when it commands the AZ axis to achieve constant *on-sky* correction
        user_pointing_correction_el : float
                Elevation constant pointing correction during this scan
        blocking : {True, False}, default = True
                `True` ACU starts tracking. This function returns when antenna arrives at command coordinate

                `False` ACU starts tracking. This function returns immediately
        update_period : float
                Time period to show updates in the AntennaC log while waiting for antenna to arrive at tracking coordinate.
                [unit = second], default = 2.0
        """
        self.logger.logDebug(
            "Tracking EQ: (ra, dec) = ({}, {}) [deg ICRS], user_pointing_corrections  = ({}, {}) [arcsec], blocking = {}, update_period = {}".format(
            ra_icrs, dec_icrs,
            user_pointing_correction_az,user_pointing_correction_el, 
            blocking, update_period)
            )

        # Astropy Time object.
        tracking_starttime_apy = Time(start_mjd, format="mjd", scale="utc")

        if((tracking_starttime_apy.ut1.mjd % 1)  > (86395 / 86400)):
            self.logger.logInfo("Tracking start time is less than 5 seconds from end of day")
            self.logger.logInfo("Set new start time 10 seconds later to guarantee no day transition while this function is loading the parameters to ACU")
            tracking_starttime_apy = tracking_starttime_apy + 10 * units.s
        else:
            self.logger.logDebug("Tracking start time is not near UT1 day boundary. OK")

        # ---------------------------------------------------------------
        # Calculate parameters for sidereal time and earth rotation angle
        # ---------------------------------------------------------------
        # Greenwich apparent sidereal time at 0:00 UT1 on the day of start_mjd. 
        # Used by ACU as longitude reference to convert RA, DEC to AZ, EL. [unit = seconds of day]
        starttime_ut1 = tracking_starttime_apy.ut1
        self.logger.logDebug("start time UT1: {}".format(starttime_ut1))

        # Get integer part of the UT1 day.
        # MJD increments the day number +1 at midnight (0h)
        # JD increments the day number at noon (12h)
        starttime_ut1_integer_day = int(starttime_ut1.mjd)
        self.logger.logDebug("start time UT1 (integer day): {}".format(starttime_ut1_integer_day))

        # Get Greenwich Sidereal Time (GST) at start of the current day
        startday_0hUT1 = Time(starttime_ut1_integer_day, format="mjd", scale="ut1")
        GST0hUT1 = startday_0hUT1.sidereal_time("apparent", longitude="greenwich")

        # Astropy function sidereal_time returns a object of type astropy.coordinates.angles.Longitude
        # Convert the Longitude object to milliseconds of solar day for ACU interface
        HOURANGLES_PER_EARTH_ROTATION = 24.0
        MILLISECONDS_PER_SOLAR_DAY = 86400 * 1000
        GST0hUT1_ms = np.round(MILLISECONDS_PER_SOLAR_DAY * (GST0hUT1.hourangle / HOURANGLES_PER_EARTH_ROTATION))
        self.logger.logDebug("GST0hUT1: {:d} ms".format(int(GST0hUT1_ms)))

        # Get DUT1 from IERS and round to integer for ACU command
        dut1_ms = np.round(1000 * tracking_starttime_apy.delta_ut1_utc)
        self.logger.logDebug("DUT1: {:d} ms".format(int(dut1_ms)))

        # Send to ACU
        ackCmd = self.setDUT1AndGST0(dut1_ms, GST0hUT1_ms)
        try:
            self.checkAcknowledgement(ackCmd)
        except tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl as e:
            raise tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl(exception=e)

        # -------------------------------------------------------------
        # Calculate Geocentric Apparent RA, DEC at the current time now
        # -------------------------------------------------------------
        # Quote from IAU SOFA Astrometry Tools 2019 JUNE 17, Page 13:
        # http://www.iausofa.org/2019_0722_C/sofa/sofa_ast_c.pdf
        #
        # "As set out in Figure 1, the transformation from ICRS to GCRS covers space motion, parallax,
        # light deflection, and aberration; that from GCRS to CIRS comprises frame bias and precession-
        # nutation; that from CIRS to observed takes account of Earth rotation, polar motion, diurnal
        # aberration and parallax (unless subsumed into the ICRS ↔ GCRS transformation), and atmo-
        # spheric refraction.""

        # Calculate the geocentric apparent RA, DEC for the epoch (current time) of this scan.  
        # Adjusts the coordinate for precession, nutation, and parallax. The ACU requires input in geocentric apparent
        # {RA, DEC}.  Accoring to IAU modls for Earth Attitude http://www.iausofa.org/2019_0722_C/sofa/sofa_pn_c.pdf ,
        # this method of geocentric apparent ra, dec and Greenwich sidereal time has been replaced by the new
        # model that uses Earth rotation angle (ERA).
        # However, we can still use the updated SOFA / IAU models to calculate CIRS and then use EO
        # (equation of the origins) to interface legacy systems that require the GST method.
        
        # Interpolation algorithm requires minimum of 5 lines, maximum of 50. Use max
        n_steps = acu.M1_TRACK_TABLE_LEN_MAX
        
        # Generate a list of time steps for the tracking table
        duration_1day = 86400
        time_ndarray = tracking_starttime_apy + np.linspace(0, duration_1day, n_steps) * units.second

        if send_icrs_to_acu == True:
            self.logger.logWarning('send_icrs_to_acu is True.  No transform to Geocentric Apparent for ACU')
            # If we load ICRS to the table, we use a constant value for the entire program track table
            ra_program_track = np.full(n_steps, ra_icrs)
            dec_program_track = np.full(n_steps, dec_icrs)
        else:
            self.logger.logInfo('send_icrs_to_acu is False.  Transform to Geocentric Apparent for ACU')
            # Use Astropy Quantity objects to convert units.
            # Then get the scalar value to use for IAU model input
            rc = (ra_icrs * units.deg).to(units.radian).value  # ICRS [ α, δ ] at J2000.0 (radians, Note 1)
            dc = (dec_icrs * units.deg).to(units.radian).value  # RA proper motion (radians/year; Note 2)
            pr = (pm_ra * units.mas / units.year).to(units.radian / units.year).value  # Dec proper motion (radians/year)
            pd = (pm_dec * units.mas / units.year).to(units.radian / units.year).value  # parallax (arcsec)
            px = (parallax * units.arcsec).value  # parallax (arcsec)
            rv = (radial_velocity * units.km / units.s).value  # radial velocity (km/s, positive if receding)
            
            # NOTE that geocentric apparent coordinates are always changing with time (very a slowly. a few
            # arcsec per day).  So, don't use a constant RA, DEC for the program track table.
            # Calculate the data for the next 24 hours, and load that to the program track table.
            # ACU will interpolate at runtime to use the correct value in realtime. 
            date1 = time_ndarray.jd1  # UTC as a 2-part. . .
            date2 = time_ndarray.jd2  # . . . quasi Julian Date (Notes 3,4)

            (ri, di, eo) = erfa.atci13(rc, dc, pr, pd, px, rv, date1, date2)

            # Convert scalar result to Quantity object with units
            # Rotate the right ascension from ERA (Earth Rotation Angle) origin to
            # GST (Greenwich Sidereal Time) origin using EO (Equation of the Origins)
            # erfa.anp wraps angle to value within range [0 .. 2 * pi]
            ra_geocentric_apparent = (erfa.anp(ri - eo)) * units.radian
            dec_geocentric_apparent = di * units.radian

            # Convert to degrees and use as input to ACU program track
            ra_program_track = ra_geocentric_apparent.to(units.deg).value
            dec_program_track = dec_geocentric_apparent.to(units.deg).value
            
            self.logger.logDebug(
                "Time (MJD) to project geocentric RA, DEC: {} ".format(time_ndarray.mjd))
            self.logger.logDebug(
                "IAU SOFA Model: geocentric apparent RA [deg]: {} ".format(ra_program_track))
            self.logger.logDebug(
                "IAU SOFA Model: geocentric apparent REC [deg]: {} ".format(dec_program_track))

        # -------------------------------------------------------------
        # Generate data structure for ACU command ACU
        # -------------------------------------------------------------
        interpolationMode = tnrtAntennaMod.SPLINE

        # TODO - read this from somewhere else - set_trajectory_params function
        northCrossing = True

        # Tracking mode: We will rely on the (ra,dec)-> (az,el) done in the ACU.
        # Az,El are computed each 10 ms in the ACU.
        if northCrossing == True:
            trackMode = tnrtAntennaMod.radecshortcut
        else:
            trackMode = tnrtAntennaMod.radec

        # Load mode: We will overwrite the previous observation:
        loadMode = tnrtAntennaMod.newL

        # Create an empty list.  Prepare to add timeAzRaElDecStruct
        programTrackTable = []

        # Fill in the rest of the table with the last number
        # improve this by using Python slice notation to fill the list -- not loop.
        for (t, r, d) in zip(time_ndarray.mjd, ra_program_track, dec_program_track):
            programTrackTable.append(
                tnrtAntennaMod.timeAzRaElDecStruct(t, r, d))

        self.setUserPointingCorrections(user_pointing_correction_az, user_pointing_correction_el)
        
        # MTM GUI: Tracking -> Program Track
        try:
            ackCmd = self.loadProgramTrackTable(
                loadMode, interpolationMode, trackMode, n_steps, programTrackTable
            )

        except (
            tnrtAntennaError.paramOutOfLimitsEx,
            tnrtAntennaError.unknownModeEx,
            tnrtAntennaError.tableSizeOutOfLimitsEx,
            tnrtAntennaError.writeErrorEx,
            tnrtAntennaError.readErrorEx,
            tnrtAntennaError.wrongAcknowledgementEx,
            tnrtAntennaError.wrongAcuStatusEx,
        ) as e:
            raise tnrtAntennaErrorImpl.acuExceptionExImpl(exception=e)
        except Exception as e:
            strLine = "Unknown ACU exception calling loadProgramTrackTable() %s" % str(
                e
            )
            self.logger.logError(strLine)
            newEx = tnrtAntennaErrorImpl.unknownAcuExceptionExImpl()
            newEx.addData = ("errExpl", strLine)
            raise newEx
        try:
            self.checkAcknowledgement(ackCmd)
        except (tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl) as e:
            raise tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl(exception=e)

        # Set tracking controller to read data from Program Track Table and calculate real-time AZ , EL
        # MTM GUI: Tracking -> Tracking Control -> Set Tracking Options -> Program Track Table -> Set Selecttion
        try:
            self.setTrackMode(tnrtAntennaMod.PROGRAM_TRACK)
        except tnrtAntennaErrorImpl.acuExceptionExImpl as e:
            raise tnrtAntennaErrorImpl.acuExceptionExImpl(exception=e)
        except tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl as e:
            raise tnrtAntennaErrorImpl.unknownAcuExceptionExImpl(exception=e)
        except tnrtAntennaErrorImpl.unknownAcuExceptionExImpl as e:
            raise tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl(exception=e)

        # Activate tracking
        self.logger.logDebug("Enable tracking and activate axis if not yet active")
        self.enableTracking(tnrtAntennaMod.ENABLE_AND_ACTIVATE)

        # Connect tracking controller result to axis motor
        # MTM GUI: Tracking -> Tracking Control -> Execute Tracking Object
        self.logger.logDebug("self.programTrack({} {})".format(AZ, self.velocity[AZ]))
        ackCmd = self.programTrack(AZ, self.velocity[AZ])

        try:
            self.checkAcknowledgement(ackCmd)
        except (tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl) as e:
            raise tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl(exception=e)

        self.logger.logDebug("self.programTrack({} {})".format(EL, self.velocity[EL]))
        ackCmd = self.programTrack(EL, self.velocity[EL])

        try:
            self.checkAcknowledgement(ackCmd)
        except (tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl) as e:
            raise tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl(exception=e)

        # If blocking is False, this function returns control immediately.
        # If blocking is True, this function returns after antenna has arrived within `tolerance`
        # arcseconds of commanded position.
        if blocking is True:
            self.blockUntilTracking(update_period)

    def trackCoordinateSS(
        self,
        start_mjd,
        duration,
        planet,
        user_pointing_correction_az,
        user_pointing_correction_el,
        blocking=True,
        update_period=2.0,
    ):
        """track coordinate solar system"""

        if duration < 0:
            strLine = "Integration time is negative %f" % (duration)
            newEx = tnrtAntennaErrorImpl.negativeIntegrationTimeExImpl()
            newEx.addData = ("errExpl", strLine)
            raise newEx

        # This is the maximum number of lines required by the ACU to run a tracking table
        # Interpolation algorithm requires maximum number of lines.
        nlines = acu.M1_TRACK_TABLE_LEN_MAX

        interpolationMode = tnrtAntennaMod.SPLINE

        # Tracking mode: AZ-EL
        trackMode = tnrtAntennaMod.azel

        # Load mode: We will abort the previous observation:
        loadMode = tnrtAntennaMod.newL

        # start_mjd is already in MJD, so nothing to convert.
        # Generate a list of time steps for the tracking table
        timelist = (
            Time(start_mjd, format="mjd", scale="utc")
            + np.linspace(0, duration, nlines) * units.second
        )

        planet_GCRS = get_body(planet, timelist, self.site_location)
        observer_frame_AltAz = AltAz(location=self.site_location, obstime=timelist)
        observed_coord_AltAz = planet_GCRS.transform_to(observer_frame_AltAz)

        # Create an empty list.  Prepare to add timeAzRaElDecStruct
        programTrackTable = []

        # Fill in the rest of the table with the last number
        # improve this by using Python slice notation to fill the list -- not loop.
        for index, time in enumerate(observed_coord_AltAz.obstime):
            programTrackTable.append(
                tnrtAntennaMod.timeAzRaElDecStruct(
                    time.value,
                    observed_coord_AltAz.az.deg[index],
                    observed_coord_AltAz.alt.deg[index],
                )
            )

        self.setUserPointingCorrections(user_pointing_correction_az, user_pointing_correction_el)

        # MTM GUI: Tracking -> Program Track
        try:
            self.logger.logDebug("sending to ACU")

            listFormat = ""
            for v in programTrackTable:
                listFormat = listFormat + ", %f, %f, %f" % (
                    v.absTimeMJD,
                    v.azRa,
                    v.elDec,
                )
            self.logger.logInfo(
                "self.loadProgramTrackTable(%s, %s, %s, %d, %s)"
                % (loadMode, interpolationMode, trackMode, nlines, listFormat)
            )
            ackCmd = self.loadProgramTrackTable(
                loadMode, interpolationMode, trackMode, nlines, programTrackTable
            )
        except (
            tnrtAntennaError.paramOutOfLimitsEx,
            tnrtAntennaError.unknownModeEx,
            tnrtAntennaError.tableSizeOutOfLimitsEx,
            tnrtAntennaError.writeErrorEx,
            tnrtAntennaError.readErrorEx,
            tnrtAntennaError.wrongAcknowledgementEx,
            tnrtAntennaError.wrongAcuStatusEx,
        ) as e:
            raise tnrtAntennaErrorImpl.acuExceptionExImpl(exception=e)
        except Exception as e:
            strLine = "Unknown ACU exception calling loadProgramTrackTable() %s" % str(
                e
            )
            self.logger.logError(strLine)
            newEx = tnrtAntennaErrorImpl.unknownAcuExceptionExImpl()
            newEx.addData = ("errExpl", strLine)
            raise newEx
        try:
            self.checkAcknowledgement(ackCmd)
        except (tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl) as e:
            raise tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl(exception=e)

        # Set tracking controller to read data from Program Track Table and calculate real-time AZ , EL
        # MTM GUI: Tracking -> Tracking Control -> Set Tracking Options -> Program Track Table -> Set Selection
        try:
            self.setTrackMode(tnrtAntennaMod.PROGRAM_TRACK)
        except tnrtAntennaErrorImpl.acuExceptionExImpl as e:
            raise tnrtAntennaErrorImpl.acuExceptionExImpl(exception=e)
        except tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl as e:
            raise tnrtAntennaErrorImpl.unknownAcuExceptionExImpl(exception=e)
        except tnrtAntennaErrorImpl.unknownAcuExceptionExImpl as e:
            raise tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl(exception=e)

        # Activate tracking
        self.logger.logDebug("Enable tracking and activate axis if not yet active")
        self.enableTracking(tnrtAntennaMod.ENABLE_AND_ACTIVATE)

        # Connect tracking controller result to axis motor
        # MTM GUI: Tracking -> Tracking Control -> Execute Tracking Object
        self.logger.logDebug("self.programTrack({} {})".format(AZ, self.velocity[AZ]))
        ackCmd = self.programTrack(AZ, self.velocity[AZ])

        try:
            self.checkAcknowledgement(ackCmd)
        except (tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl) as e:
            raise tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl(exception=e)

        self.logger.logDebug("self.programTrack({} {})".format(EL, self.velocity[EL]))
        ackCmd = self.programTrack(EL, self.velocity[EL])

        try:
            self.checkAcknowledgement(ackCmd)
        except (tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl) as e:
            raise tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl(exception=e)

        # If blocking is False, this function returns control immediately.
        # If blocking is True, this function returns after antenna has arrived within `tolerance`
        # arcseconds of commanded position.
        if blocking is True:
            self.blockUntilTracking(update_period)

    def trackOffsetPattern(
        self,
        loadMode,
        interpolationMode,
        trackMode,
        table,
        blocking=True,
        update_period=2.0,
    ):
        """
        Load a list of coordinates [AZ,EL] or [RA, DEC] into the ACU Program Offset Table.
        These values will be added on top of the main Program Track Table to create a spaital pattern
        centered around the main program tracking coordinate.  For example, Program Track Table will track a
        star (EQ), satellite (TLE), or fixed Az/El coodinate.

        Program Offset Table controls the antenna to scan a single point, line, point map, line map, around the center point.
        The calculation of time, x, y, coordinates is done in the Scan and Subscan that knows how to construct itself.

        Parameters
        ----------
        loadMode : tnrtAntennaMod.cmdLoadMode
                newL, addL, resetL
        interpolationMode : tnrtAntennaMod.cmdInterpolationMode
                NEWTON, SPLINE
        trackMode : tnrtAntennaMod.cmdTrackMode
                azel, radec
        table : tnrtAntennaMod.timeAzRaElDecStruct[]
                List of (time, x, y) tuples which originate from the IDL struct tnrtAntennaMod.timeAzRaElDecStruct.
                        time : float
                                UTC time format=MJD.  units = days.
                        x : float
                                if `trackMode` == tnrtAntennaMod.azel, x = Azimuth
                                if `trackMode` == tnrtAntennaMod.radec, x = Right Ascension
                                if `trackMode` == tnrtAntennaMod.radecshortcut, x = Right Ascension
                        y : float
                                if `trackMode` == tnrtAntennaMod.azel, x = Elevation
                                if `trackMode` == tnrtAntennaMod.radec, x = Declination
                                if `trackMode` == tnrtAntennaMod.radecshortcut, x = Declination
        blocking : boolean
                The antenna can take time up to 3  minutes to arrive at command
                position.  If blocking=False, this function returns immediately while the ACU moves the
                antenna to the program offset table coordinates.  If blocking=True, this function returns only
                after ACU signals that the antenna has arrived at the AZ, EL coordinate
                in the tracking table.
        update_period : float
                Time interval that the antenna component will log a message
                to show countown until arrive at command coordinate.  This is a rough approximation, because
                it uses the slewtime() function to estimate the total transit time in advance.  Some mechanical
                conditions could cause the slewtime to be longer.  In that case, the countdown would go negative
                until Antenna arrives at command position.
        """
        nlines = len(table)
        if nlines > acu.M1_TRACK_TABLE_LEN_MAX:
            newEx = tnrtAntennaErrorImpl.offsetTableLength()
            newEx.addData = (
                "errExpl",
                "Tracking table length = {} > maximum lines {}".format(
                    nlines, acu.M1_TRACK_TABLE_LEN_MAX
                ),
            )
            raise newEx

        # Send data to ACU
        # MTM GUI: Tracking -> Program Offset Table
        try:
            self.logger.logDebug(
                "tnrtAntennaMod.loadProgramOffsetTable() sending to ACU"
            )

            listFormat = ""
            for v in table:
                listFormat = listFormat + ", %f, %f, %f" % (
                    v.absTimeMJD,
                    v.azRa,
                    v.elDec,
                )
            self.logger.logInfo(
                "self.loadProgramTrackOffsetTable(%s, %s, %s, %d, %s)"
                % (loadMode, interpolationMode, trackMode, nlines, listFormat)
            )

            ackCmd = self.loadProgramOffsetTable(
                loadMode, interpolationMode, trackMode, nlines, table
            )

        except (
            tnrtAntennaError.paramOutOfLimitsEx,
            tnrtAntennaError.unknownModeEx,
            tnrtAntennaError.tableSizeOutOfLimitsEx,
            tnrtAntennaError.writeErrorEx,
            tnrtAntennaError.readErrorEx,
            tnrtAntennaError.wrongAcknowledgementEx,
            tnrtAntennaError.wrongAcuStatusEx,
        ) as e:
            raise tnrtAntennaErrorImpl.acuExceptionExImpl(exception=e)
        except Exception as e:
            strLine = (
                "Unknown ACU exception calling loadProgramTrackOffsetTable() %s"
                % str(e)
            )
            self.logger.logError(strLine)
            newEx = tnrtAntennaErrorImpl.unknownAcuExceptionExImpl()
            newEx.addData = ("errExpl", strLine)
            raise newEx
        try:
            self.checkAcknowledgement(ackCmd)
        except (tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl) as e:
            raise tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl(exception=e)

        # If blocking is False, this function returns control immediately.
        # If blocking is True, this function returns after antenna has arrived within `tolerance`
        # arcseconds of commanded position.
        if blocking is True:
            self.blockUntilTracking(update_period)

    def trackPatternHxp(
        self, loadMode, interpolationMode, table, blocking=True, update_period=2.0
    ):
        """
        Load a list of times and  positions [X, Y, Z, TX, TY, TZ] or into the ACU Hexapod Program Track Table.

        This pattern is useful to can the M2 mirror and find the optimal focus position.

        Parameters
        ----------
        loadMode : tnrtAntennaMod.cmdLoadMode
                newL, addL, resetL

        interpolationMode : tnrtAntennaMod.cmdInterpolationMode
                NEWTON, SPLINE

        table : tnrtAntennaMod.timeXYZTXTYTZStruct[]
                List of (time, x, y, z, tx, ty, tz) tuples which originate from the IDL struct tnrtAntennaMod.timeXYZTXTYTZStruct.
                        time : float
                                UTC time format=MJD.  units = days.
                        x : float. unit = mm
                        y : float. unit = mm
                        z : float. unit = mm
                        tx : float. unit = deg
                        ty : float. unit = deg
                        tz : float. unit = deg
        blocking : boolean
                The hexapod can can take time up to 90 seconds to traverse the full length of Z axis.
                position.  If blocking=False, this function returns immediately while the ACU moves the
                hexapod to the program track table position.  If blocking=True, this function returns only
                after ACU signals that the hexapod has arrived at the desired position
        update_period : float
                Time interval that the antenna component will log a message
                to show countown until arrive at command coordinate.  This is a rough approximation, because
                it uses the slewtime() function to estimate the total transit time in advance.  Some mechanical
                conditions could cause the slewtime to be longer.  In that case, the countdown would go negative
                until Antenna arrives at command position.
        """
        nlines = len(table)
        if nlines > acu.HXP_TRACK_TABLE_LEN_MAX:
            newEx = tnrtAntennaErrorImpl.tableSizeOutOfLimits()
            newEx.addData = (
                "errExpl",
                "HXP Tracking table length = {} > maximum lines {}".format(
                    nlines, acu.HXP_TRACK_TABLE_LEN_MAX
                ),
            )
            raise newEx

        # Send data to ACU
        # MTM GUI: Hexapod Control -> Program Track Table -> Transmit
        try:
            self.logger.logDebug(
                "tnrtAntennaMod.loadProgramTrackTableHexapod() sending to ACU"
            )

            listFormat = ""
            for v in table:
                listFormat = listFormat + ", {}, {}, {}".format(
                    v.absTimeMJD, v.x, v.y, v.z, v.tx, v.ty, v.tz
                )
            self.logger.logInfo(
                "self.loadProgramTrackTableHexapod({}, {}, {}, {})".format(
                    loadMode, interpolationMode, nlines, listFormat
                )
            )

            ackCmd = self.loadProgramTrackTableHexapod(
                loadMode, interpolationMode, nlines, table
            )

        except (
            tnrtAntennaError.paramOutOfLimitsEx,
            tnrtAntennaError.unknownModeEx,
            tnrtAntennaError.tableSizeOutOfLimitsEx,
            tnrtAntennaError.writeErrorEx,
            tnrtAntennaError.readErrorEx,
            tnrtAntennaError.wrongAcknowledgementEx,
            tnrtAntennaError.wrongAcuStatusEx,
        ) as e:
            raise tnrtAntennaErrorImpl.acuExceptionExImpl(exception=e)
        
        try:
            self.checkAcknowledgement(ackCmd)
        except (tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl) as e:
            raise tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl(exception=e)

        # Activate tracking.  Connect tracking controller result to axis motors
        # MTM GUI: Hexapod Control -> Hexapod Program Track -> Start Prog Track
        linear_rate = 2.0
        rotate_rate = 0.04
        self.logger.logDebug(
            "self.programTrackHxp({} {})".format(linear_rate, rotate_rate)
        )
        ackCmd = self.programTrackHxp(linear_rate, rotate_rate)

        try:
            self.checkAcknowledgement(ackCmd)
        except (tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl) as e:
            raise tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl(exception=e)

        # If blocking is False, this function returns control immediately.
        # If blocking is True, this function returns after antenna has arrived within `tolerance`
        # arcseconds of commanded position.
        if blocking is True:
            self.blockUntilTrackingHxp(update_period)

    def slewTime(
        self,
        xcoord,
        ycoord,
        tMode,
        time_mjd,
        equinox=2000,
        offxarcsecs=0,
        offyarcsecs=0,
    ):
        """Method which computes the slew time between the current position of the telescope and a new one defined by
        deltaX and deltaY in the coordinate system defined by tMode.
        a distance of deltaX arcsecs in azimuth (or RA) and deltaY arcsecs in elevation (or declination).
        @param xcoord. X coordinate [degrees]
        @param ycoord Y coordinate  [degrees]
        @param tMode Coordinate system for the axis. May take two values:
        tnrtAntennaMod.azel (horizontal "HO") and tnrtAntennaMod.radec (equatorial "EQ")
        @param time_mjd. UTC time in units MJD
        @equinox Epoch to which the coordinates refer to.
        @return Slew time in seconds.
        """

        self.__stowElZoneLimit = 89.5
        self.__stowElZoneVelocity = 0.05

        # Distances in azimuth/elevation greater than offAzMax/offElMaz
        # require the antenna
        # to be accelerated in azimuth/elevation to the maximum allowed value
        # offAzMax is the distance the antenna moves after accelerating to the maximum
        # speed and immediatley decelerating to 0
        # This distance is in degrees.
        offAzMax = (self.velocity[AZ]**2) * np.pi / (2.0 * tc.subsystem_properties[AZ]["P111_a_MaxSys"])
        offElMax = (self.velocity[EL]**2) * np.pi / (2.0 * tc.subsystem_properties[EL]["P111_a_MaxSys"])

        self.logger.logTrace("offAzMax: %f" % (offAzMax))
        self.logger.logTrace("offElMax: %f" % (offElMax))
        self.logger.logTrace("tMode: %s" % (tMode))
        self.logger.logTrace("xcoord before EQ mode: %f" % (xcoord))
        self.logger.logTrace("ycoord before EQ mode: %f" % (ycoord))

        # Degrees...
        xcoordf = xcoord + offxarcsecs / 3600.0
        ycoordf = ycoord + offyarcsecs / 3600.0
        self.logger.logTrace("xcoordf before EQ mode: %f" % (xcoordf))
        self.logger.logTrace("ycoordf before EQ mode: %f" % (ycoordf))

        if tMode == tnrtAntennaMod.radec or tMode == tnrtAntennaMod.radecshortcut:
            # Convert RA / DEC to current AZ EL at this time
            time_test = Time(
                time_mjd, format="mjd", scale="utc", location=self.site_location
            )
            skpEQ = SkyCoord(
                ra=xcoordf,
                dec=ycoordf,
                frame="icrs",
                unit="deg",
                obstime=time_test,
                location=self.site_location,
            )
            skpHO = skpEQ.transform_to("altaz")
            xcoord = skpHO.az.deg
            ycoord = skpHO.alt.deg

        # Degrees...
        self.logger.logTrace(
            "xcoord Tmode self.site_location: %s" % (self.site_location)
        )
        self.logger.logTrace("xcoord after EQ mode: %f" % (xcoord))
        self.logger.logTrace("ycoord after EQ mode: %f" % (ycoord))

        xcoordf = xcoord + offxarcsecs / 3600.0
        ycoordf = ycoord + offyarcsecs / 3600.0

        try:
            currentAz = self.getAzPosition()
            currentEl = self.getElPosition()
        except AttributeError as e:
            self.logger.logError(e)
            self.logger.logError(
                "ACU not connected?. Return -1 so we can detect the failure in Scan and continue testing"
            )
            return -1.0

        self.logger.logTrace("xcoordf after EQ mode: %f" % (xcoordf))
        self.logger.logTrace("ycoordf after EQ mode: %f" % (ycoordf))
        self.logger.logTrace("currentAz: %f" % (currentAz))
        self.logger.logTrace("currentEl: %f" % (currentEl))

        xcoordf = self.__getClosestAz(
            currentAz, xcoordf, tc.subsystem_properties[AZ]["P105_p_FinEndUp"], tc.subsystem_properties[AZ]["P104_p_FinEndDn"]
        )
        self.logger.logTrace("xcoordf: %s" % (xcoordf))

        # If the antenna has to get out of the stow braking zone, take it into account.
        # Azimuth stow braking zone is not needed.
        if currentEl > self.__stowElZoneLimit and ycoordf < self.__stowElZoneLimit:
            self.__slewTimeStowZone = (
                currentEl - self.__stowElZoneLimit
            ) / self.__stowElZoneVelocity
        elif currentEl < self.__stowElZoneLimit and ycoordf > self.__stowElZoneLimit:
            self.__slewTimeStowZone = (
                ycoordf - self.__stowElZoneLimit
            ) / self.__stowElZoneVelocity
        elif currentEl > self.__stowElZoneLimit and ycoordf > self.__stowElZoneLimit:
            self.velocity[EL] = self.__stowElZoneVelocity
            self.__slewTimeStowZone = 0.0
        else:
            self.__slewTimeStowZone = 0.0

        difX = abs(xcoordf - currentAz)
        difY = abs(ycoordf - currentEl)
        kauX = difX / offAzMax
        kauY = difY / offElMax

        self.logger.logTrace("difX: %f" % (difX))
        self.logger.logTrace("difY: %f" % (difY))
        self.logger.logTrace("kauX: %f" % (kauX))
        self.logger.logTrace("kauY: %f" % (kauY))

        # TODO. Check if this is correct ....

        if difX > offAzMax:
            slewTimeX = (difX - offAzMax) / self.velocity[AZ] + np.pi * self.velocity[AZ] / tc.subsystem_properties[AZ]["P111_a_MaxSys"]
        else:
            slewTimeX = kauX ** (1 / 3.0) * np.pi * self.velocity[AZ] / tc.subsystem_properties[AZ]["P111_a_MaxSys"]

        if difY > offElMax:
            slewTimeY = (difY - offElMax) / self.velocity[EL] + np.pi * self.velocity[EL] / tc.subsystem_properties[EL]["P111_a_MaxSys"]
        else:
            slewTimeY = kauY ** (1 / 3.0) * np.pi * self.velocity[EL] / tc.subsystem_properties[EL]["P111_a_MaxSys"]

        slewTimeY = self.__slewTimeStowZone + slewTimeY

        self.logger.logTrace(
            "slewTimeX: %f, offAzMax: %f, tconst: %f"
            % (slewTimeX, offAzMax, (difX - offAzMax) / self.velocity[AZ])
        )
        self.logger.logTrace(
            "slewTimeY: %f, offElMax: %f, tconst: %f"
            % (slewTimeY, offElMax, (difY - offElMax) / self.velocity[EL])
        )

        if slewTimeX > slewTimeY:
            slewTime = slewTimeX
        else:
            slewTime = slewTimeY

        self.logger.logDebug("calculated slew time: %f" % slewTime)

        return slewTime

    # ------------- TODO
    def unstow_all(self):
        self.logger.info("unstow AZ")
        ackCmd = self.unstow(AZ, tnrtAntennaMod.stowpin1)
        try:
            self.checkAcknowledgement(ackCmd)
        except (tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl) as e:
            newEx = tnrtAntennaErrorImpl.unstowErrorExImpl(exception=e)
            newEx.addData = ("errExpl", "error unstowing AZ")
            raise newEx

        self.logger.info("unstow EL")
        ackCmd = self.unstow(EL, tnrtAntennaMod.stowpinBoth)
        try:
            self.checkAcknowledgement(ackCmd)
        except (tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl) as e:
            newEx = tnrtAntennaErrorImpl.unstowErrorExImpl(exception=e)
            newEx.addData = ("errExpl", "error unstowing EL")
            raise newEx

        self.logger.info("unstow THU")
        ackCmd = self.unstow(THU, tnrtAntennaMod.stowpinBoth)
        try:
            self.checkAcknowledgement(ackCmd)
        except (tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl) as e:
            newEx = tnrtAntennaErrorImpl.unstowErrorExImpl(exception=e)
            newEx.addData = ("errExpl", "error unstowing THU")
            raise newEx

    def stow_shutdown(self, stowPosition):
        """Sends the antenna to stow position. Previously remove the pointing model and pointing offsets, stop the antenna, close the vertex and switch off the fans.
        Warning: It takes 15 seconds to stow or unstow both pins. Elevation stow pin is moved first.
        It is possible to stow and unstow in active mode and in inactive mode.
        """
        self.logger.logDebug("stow_shutdown starting sequence ...")

        # Remove pointing model
        self.logger.logInfo("clear pointing model")
        pointingParams = [0, 0, 0, 0, 0, 0, 0, 0, 0]
        ackCmd = self.setPointingModel(pointingParams)
        try:
            self.checkAcknowledgement(ackCmd)
        except (tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl) as e:
            newEx = tnrtAntennaErrorImpl.antennaShutdownExImpl(exception=e)
            newEx.addData = ("errExpl", "error clearing pointing model")
            raise newEx

        # Refraction off
        self.logger.logInfo("disable refraction correction")
        ackCmd = self.setCorrectionValuesEnabled(
            tnrtAntennaMod.REFRACTION, tnrtAntennaMod.CORRECTION_OFF
        )
        try:
            self.checkAcknowledgement(ackCmd)
        except (tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl) as e:
            newEx = tnrtAntennaErrorImpl.antennaShutdownExImpl(exception=e)
            newEx.addData = ("errExpl", "error disable refrection correction")
            raise newEx

        # Stop antenna
        self.logger.logInfo("stop antenna motion")
        try:
            self.stopAntenna(tnrtAntennaMod.fullStop)
        except Exception as e:
            strLine = (
                "Unknown exception calling stopAntenna() from tnrtAntenna.stow() %s"
                % (str(e))
            )
            newEx = tnrtAntennaErrorImpl.antennaShutdownExImpl(exception=e)
            newEx.addData = ("errExpl", "error stopAntenna")
            raise newEx

        # Close vertex
        self.logger.logInfo("close vertex")
        ackCmd = self.closeVX()
        try:
            self.checkAcknowledgement(ackCmd)
        except (tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl) as e:
            newEx = tnrtAntennaErrorImpl.antennaShutdownExImpl(exception=e)
            newEx.addData = ("errExpl", "error closing Vertex shutter")
            raise newEx

        # And send the antenna to stow position
        self.logger.logInfo("drive AZ to stow position")
        ackCmd = self.driveToStow(AZ, stowPosition, self.velocity[AZ] / 2.0)
        try:
            self.checkAcknowledgement(ackCmd)
        except (tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl) as e:
            newEx = tnrtAntennaErrorImpl.antennaShutdownExImpl(exception=e)
            newEx.addData = ("errExpl", "error drive to stow position AZ")
            raise newEx

        self.logger.logInfo("drive EL to stow position")
        ackCmd = self.driveToStow(EL, stowPosition, self.velocity[EL] / 2.0)
        try:
            self.checkAcknowledgement(ackCmd)
        except (tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl) as e:
            newEx = tnrtAntennaErrorImpl.antennaShutdownExImpl(exception=e)
            newEx.addData = ("errExpl", "error drive to stow position EL")
            raise newEx

        self.logger.logInfo("drive THU to stow position")
        ackCmd = self.driveToStow(THU, stowPosition, self.velocity[EL] / 2.0)
        try:
            self.checkAcknowledgement(ackCmd)
        except (tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl) as e:
            newEx = tnrtAntennaErrorImpl.antennaShutdownExImpl(exception=e)
            newEx.addData = ("errExpl", "error drive to stow position THU")
            raise newEx

        # And switch off the fans
        self.logger.logInfo("stop fans")
        ackCmd = self.setFan(tnrtAntennaMod.offF)
        try:
            self.checkAcknowledgement(ackCmd)
        except (tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl) as e:
            newEx = tnrtAntennaErrorImpl.antennaShutdownExImpl(exception=e)
            newEx.addData = ("errExpl", "error switching off fans")
            raise newEx

        # Deactivate hexapod
        ackCmd = self.deactivate(HXP)
        try:
            self.checkAcknowledgement(ackCmd)
        except (tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl) as e:
            newEx = tnrtAntennaErrorImpl.antennaShutdownExImpl(exception=e)
            newEx.addData = ("errExpl", "error disable Hexapod")
            raise newEx

    # ------ TEST NOTIFICATION CHANNEL FUNCTION --------------------------------
    def test_notification(self):
        time_mjd = Time.now().mjd
        ctypes_obj = acu.StatusMessageFrame()
        metadata = tc.from_ctype_StatusMessageFrame(time_mjd, ctypes_obj)

        self.logger.logInfo("send data_struct on NC channel: {}".format(metadata))
        self.__nc_supplier.publish_event(metadata)
        time.sleep(0.2)

        self.logger.logInfo("send data struct on NC arrived channel")
        arrived_status = tnrtAntennaMod.AntennaArrivedStatus(False)
        self.__nc_arrived_status.publish_event(arrived_status)

    # ------ PRIVATE FUNCTIONS ------   ---------------------------------------

    def __clear_program_offset_table(self):
        object_id = 1
        sequence_length = 1
        timeAzRaElDecList = [tnrtAntennaMod.timeAzRaElDecStruct(0,0,0)]

        po_table_zeros = tc.from_idl_timeAzRaElDecList(timeAzRaElDecList)
        self.logger.logDebug("po_table_zeros: {}".format(po_table_zeros[:]))

        ack = self.acu_command_client.send_track_table_command(
            acu.CMD_LOAD_PROGRAM_OFFSET_TABLE,
            object_id,
            acu.CMD_LOAD_PROGRAM_TRACK_LOAD_MODE_RESET,
            acu.CMD_LOAD_PROGRAM_TRACK_INTERPOLATION_MODE_SPLINE,
            acu.CMD_LOAD_PROGRAM_TRACK_TRACK_MODE_AZEL,
            sequence_length,
            po_table_zeros,
        )
        return tc.ack_properties[ack.status]["idl_enum"]

    def __handler_acu_status_message(self):
        """
        Docstring
        """

        # Update flow control events for every status message that arrives.

        # Create a binary struct of data from ACU and publish on the ACS channel (struct defined in tnrtAntenna.idl)
        self.__nc_supplier.publish_event(
            tc.from_ctype_StatusMessageFrame(self.time_tcs_mjd, self.acu_status)
        )

        # Received the first message.  Therefore, we can start connection of the COMMAND client.
        self.acu_status_message_received.set()

        # After the COMMAND client connects, the ACU will prepare the socket for CMD and ACK
        # After it is ready, one bit of the ACU status message shows that ACU is ready
        # to receive and handle a command.
        if (self.acu_status.general_sts.control_mode_status & acu.CONTROL_MODE_STATUS_REMOTE_CMD_CONNECT) > 0:
            self.acu_command_remote_connected.set()

        for subsystem_name, subsystem_obj in self.flow_control.items():

            for event_name, event_obj in subsystem_obj["event"].items():
                data_element = None
                if "index" in subsystem_obj:
                    status_message_frame = getattr(
                        self.acu_status,
                        subsystem_obj["status_message_frame"],
                    )[subsystem_obj["index"]]
                    data_element = getattr(
                        status_message_frame, event_obj["data_element"]
                    )
                else:
                    status_message_frame = getattr(
                        self.acu_status,
                        subsystem_obj["status_message_frame"],
                    )
                    data_element = getattr(
                        status_message_frame, event_obj["data_element"]
                    )

                if "compare_value" in event_obj: 
                    if data_element == event_obj["compare_value"]:
                        self.flow_control[subsystem_name]["event"][event_name][
                            "thread"
                        ].set()
                elif "compare_bit" in event_obj:
                    if (data_element & event_obj["compare_bit"]) > 0:
                        self.flow_control[subsystem_name]["event"][event_name][
                            "thread"
                        ].set()

        # Check if antenna AZ, EL current position is already arrived at command position.
        # If so, set the threading.Event to signal that Antenna location is ready to work
        azErrorOnSky = self.azError() * np.cos(self.getElPosition() * np.pi / 180.0)
        errorAbs = np.sqrt(azErrorOnSky**2 + self.elError() ** 2)

        if errorAbs < self.tolerance_azel:
            self.on_track.set()
            self.__nc_arrived_status.publish_event(tnrtAntennaMod.AntennaArrivedStatus(True))

        # Check if Hexapod (M2) mirror has arrived to the command position.
        linear_error = (
            (
                self.acu_status.hexapod_sts.pos_actual[0]
                - self.acu_status.hexapod_sts.pos_desired[0]
            )
            + (
                self.acu_status.hexapod_sts.pos_actual[1]
                - self.acu_status.hexapod_sts.pos_desired[1]
            )
            + (
                self.acu_status.hexapod_sts.pos_actual[2]
                - self.acu_status.hexapod_sts.pos_desired[2]
            )
        )

        rotate_error = 3600 * (
            (
                self.acu_status.hexapod_sts.pos_actual[3]
                - self.acu_status.hexapod_sts.pos_desired[3]
            )
            + (
                self.acu_status.hexapod_sts.pos_actual[4]
                - self.acu_status.hexapod_sts.pos_desired[4]
            )
            + (
                self.acu_status.hexapod_sts.pos_actual[5]
                - self.acu_status.hexapod_sts.pos_desired[5]
            )
        )

        if (linear_error < self.tolerance_hxp_linear) and (
            rotate_error < self.tolerance_hxp_rotate
        ):
            self.on_track_hxp.set()

    def __handler_timeout_transit(self):
        """
        Handler function timeout and return some flow control
        that is blocking on threading.Event.wait()
        """
        self.logger.logDebug("timeout transit event to exit blocking on_track.wait()")
        self.on_track.set()

    def __handler_timeout_transit_hxp(self):
        """
        Handler function timeout and return some flow control
        that is blocking on threading.Event.wait()
        """
        self.logger.logDebug(
            "timeout transit event to exit blocking on_track_hxp.wait()"
        )
        self.on_track_hxp.set()

    def flow_control_logging(self, subsystem, event, msg):
        while not self.flow_control[subsystem]["event"][event]["thread"].is_set():
            self.logger.logInfo(msg)
            self.flow_control[subsystem]["event"][event]["thread"].wait(
                timeout=config.flow_control_logging_period
            )

    def blocking_flow_control(self, subsystem, event, ack, result, timeout=60):
        self.flow_control[subsystem]["event"][event]["thread"].wait(timeout=timeout)
        if self.flow_control[subsystem]["event"][event]["thread"].is_set():
            result["status"] = tc.ack_properties[ack.status]["idl_enum"]
        else:
            self.logger.logWarning("{} {} exceed timeout".format(event, subsystem))
            result["status"] = tc.ack_properties[acu.CMD_ACK_STS_TIMEOUT.value][
                "idl_enum"
            ]
            self.flow_control[subsystem]["event"][event]["thread"].set()

    def blockUntilTracking(self, update_period):
        """
        Blocking function that waits until the antenna has arrived at commmanded position.
        (based on encoder and axis command postion -- not based on  Tracking system active in ACU). 
        This function can be used by
        trackCoordinateHO, trackCoordinateTLE, trackCoordinateEQ, and trackCoordinateSS.
        This function will timeout after `maximum_slewtime` (typical 250 seconds) in case the
        antenna is unresponsive.

        Parameter
        ---------
        update_period: float
                Period to display updates in the log AntennaC

        """
        # clear old value of event so we must wait again until Antenna arrives.
        self.on_track.clear()

        # Start a watchdog timer that can timeout and unblock self.on_track.wait if antenna doesn't
        # arrive at tracking coordinate after maximum transit time
        wdt = threading.Timer(self.maximum_slewtime, self.__handler_timeout_transit)
        wdt.daemon = True
        self.logger.logDebug("Starting watchdog_timer %s [sec]" % self.maximum_slewtime)
        wdt.start()

        # Wait for ACU status message to show that the Antenna has arrived at command coordinate.
        while not self.on_track.is_set():
            self.logger.logDebug("Waiting for Antenna to arrive at command coordinate")
            self.on_track.wait(timeout=update_period)
        self.logger.logInfo('Antenna arrived at command coordinate')

        # Cancel the watchdog timer
        wdt.cancel()


    def blockUntilTrackingHxp(self, update_period):
        """
        Blocking function that waits until the hexapod has arrived at commmanded position.
        This function can be used by the trackPatternHxp.
        This function will timeout after `maximum_slewtime` 90 seconds in case the Hexapod
        or ACU is unresponsive.

        Parameter
        ---------
        update_period: float
                Period to display updates in the log AntennaC

        """
        # clear old value of event so we must wait again until Antenna arrives.
        self.on_track_hxp.clear()

        # Start a watchdog timer that can timeout and unblock self.on_track.wait if antenna doesn't
        # arrive at tracking coordinate after maximum transit time
        wdt = threading.Timer(
            self.maximum_slewtime_hxp, self.__handler_timeout_transit_hxp
        )
        wdt.daemon = True
        self.logger.logDebug(
            "Starting watchdog_timer %s [sec]" % self.maximum_slewtime_hxp
        )
        wdt.start()

        # Wait for ACU status message to show that the Antenna has arrived at command coordinate.
        while not self.on_track_hxp.wait(timeout=update_period):
            self.logger.logDebug("Waiting for Hexapod to arrive at command position")

        # Cancel the watchdog timer
        wdt.cancel()

    def blockingTrackingEvent(self, event, timeout):
        self.flow_control[TR]["event"][event]["thread"].wait(timeout=timeout)
        if self.flow_control[TR]["event"][event]["thread"].is_set():
            self.logger.logInfo("Tracking complete")
        else:
            self.logger.logError("Tracking timeout")
            self.flow_control[TR]["event"][event]["thread"].set()

    def blockUntilOffsetTrackingCompleted(self, timeout):

        # clear event
        self.flow_control[TR]["event"]["program_offset_complete"]["thread"].clear()

        # wait for complete bit from acu
        thread = threading.Thread(
            name="program-track-blocking-thread",
            target=self.blockingTrackingEvent,
            args=["program_offset_complete", timeout],
        )
        thread.start()
        # logging in acscommandcenter to show the users that something's running
        self.flow_control_logging(
            TR,
            "program_offset_complete",
            "{} waiting for acu complete bit...".format(TR),
        )

    def __getM2positionFromEl(self, el):
        #   '''Computes the subreflector position depending on elevation
        # This method should be private but primarytoSecondary thread needs it to be public.
        # @param el Elevation of the antenna in degs.
        # @return Tupple with positions of the subreflector: x, y, z, tiltx, tilty
        # '''
        el2 = el * el
        el3 = el * el * el
        xOff = (
            self.__xCorrEl[0]
            + self.__xCorrEl[1] * el
            + self.__xCorrEl[2] * el2
            + self.__xCorrEl[3] * el3
        )

        # Y position is fitted using a cosine plus a constant, because it has physical sense.
        yOff = (
            self.__yCorrEl[0]
            + self.__yCorrEl[1] * np.cos(np.pi * el / 180.0)
            + self.__yCorrEl[2] * np.sin(np.pi * el / 180.0)
        )
        # yOff = self.__yCorrEl[0] + self.__yCorrEl[1]*el + self.__yCorrEl[2]*el2 + self.__yCorrEl[3]*el3

        # Z position was fitted according to a polynominal.
        # zOff = self.__zCorrEl[0] + self.__zCorrEl[1]*el + self.__zCorrEl[2]*el2 + self.__zCorrEl[3]*el3

        # Z position is fitted using a sine function plus a constant, because it has physical sense.
        zOff = self.__zCorrEl[0] + self.__zCorrEl[1] * np.sin(np.pi * el / 180.0)

        # X tilt is proportional to Z because we have seen so in practice (see IT-OAN 2010-12)
        # xTilt = -9.8*zOff = -9.8 * zCorrEl[0] -9.8 * zCorrEl[1]
        xTilt = self.__txCorrEl[0] / 3600.0 + self.__txCorrEl[1] / 3600.0 * np.cos(
            np.pi * el / 180.0
        )

        # Y Tilt is unknown so we keep it as a polynomial.
        yTilt = self.__tyCorrEl[0] / 3600.0 + self.__tyCorrEl[1] / 3600.0 * np.cos(
            np.pi * el / 180.0
        )
        # yTilt = self.__tyCorrEl[0] + self.__tyCorrEl[1]*el + self.__tyCorrEl[2]*el2 + self.__tyCorrEl[3]*el3

        return xOff, yOff, zOff, xTilt, yTilt

    def __getClosestAz(self, initialAz, targetAz, upperLimit, lowerLimit):
        """Decides wether to add or substract 360 degrees to go from where the antenna is now to the final Azimuth
        using the shortest way.

        IMPORTANT. This method assumes that targetAz is between (0,360) and
        initialAz between (-60,420). No exceptions are thrown here in case of error.
        The caller should take care of that.

        Another issue: Taken into account the previous restriction the
        output azimuth is always between -60 and 420. It never surpasses this limit.
        However this method in a loop where targetAz increases or decreases continuously
        may cause a 360 jump of the output azimuth.

        @param initialAz Azimuth from which we want to go Allowed interval (-60,420)
        @param targetAz Final azimuth to where we want to go. It is always between (0,360)
        @param upperLimit  Upper azimuth limit
        @param lowerLimit Lower azimuth limit
        """

        diffUpper = upperLimit - 360.0
        diffLower = 0 - lowerLimit

        if diffUpper > diffLower:
            extraLength = diffLower
        else:
            extraLength = diffUpper

        initialUpperLimit = 360 - extraLength
        initialLowerLimit = 0 + extraLength

        # We are in this azimuth interval: (300, 420)
        if initialAz >= initialUpperLimit:

            # if we want to go to any place between (0, 60)
            if targetAz < extraLength:
                closestAz = targetAz + 360
            else:
                closestAz = targetAz

        # We are in this azimuth interval: (0, 60)
        elif initialAz < initialLowerLimit:

            # if we want to go to any place between (300, 360)
            if targetAz > (360 - extraLength):
                closestAz = targetAz - 360
            else:
                closestAz = targetAz

        # If we are between 60 and 120 (upperLimit - 180.)
        elif initialAz <= (initialUpperLimit - 180.0) and initialAz > initialLowerLimit:

            # and we go to any place between (300, 360)
            if targetAz > initialUpperLimit:
                closestAz = targetAz - 360
            else:
                closestAz = targetAz
        # If we are between 240 and 300 (lowerLimit + 180.)
        elif initialAz >= (initialLowerLimit + 180.0) and initialAz < initialUpperLimit:

            # and we go to any place between (0, 60)
            if targetAz < initialLowerLimit:
                closestAz = targetAz + 360
            else:
                closestAz = targetAz
        else:
            closestAz = targetAz
        return closestAz

    def __checkBit(word, bit):
        return (word & 2**bit) > 0


# Main defined only for testing with data in native format
# Not passing data throuth IDL type translations
if __name__ == "__main__":
    self = tnrtAntenna()
