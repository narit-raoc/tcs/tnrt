# Project: TNRT - Thai National Radio Telescope
# Organization: NARIT - National Astronomical Research Institute of Thailand
# Author: Spiro Sarris
# Date: 2022.05.04

import logging
import numpy as np
from astropy.time import Time
from unittest_common import AcuRemoteCommandTestCase

# import TCS modules
import tnrtAntennaMod
import DataFormatAcu

# Shortcut / alias
from tnrtAntennaMod import M4B as M4B
from tnrtAntennaMod import DONE as DONE
from tnrtAntennaMod import ACCEPTED as ACCEPTED
from tnrtAntennaMod import PARAM_ERROR as PARAM_ERROR


class State5550_M4B_Inactive(AcuRemoteCommandTestCase):
    def setUp(self):
        super().setUp()
        self.result = self.objref.deactivate(M4B)
        self.result = None

    def tearDown(self):
        self.logger.info(self.result)
        super().tearDown()

    def test5551_acu_axis_mode_deactivate(self):
        self.logger.debug("deactivate M4B")
        self.result = self.objref.deactivate(M4B)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test5552_acu_axis_mode_activate(self):
        self.logger.debug("activate M4B")
        self.result = self.objref.activate(M4B)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test55523_acu_axis_mode_reset(self):
        self.logger.debug("reset M4B")
        self.result = self.objref.reset(M4B)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])


class State5560_M4B_Active(AcuRemoteCommandTestCase):
    def setUp(self):
        # Use parent class setup to configure logger and connect ACU
        super().setUp()
        self.result = self.objref.activate(M4B)
        self.result = None

    def tearDown(self):
        self.logger.info(self.result)
        self.objref.stop(M4B)
        super().tearDown()

    def test5561_acu_axis_mode_deactivate(self):
        self.logger.debug("deactivate M4B")
        self.result = self.objref.deactivate(M4B)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test5562_acu_axis_mode_stop(self):
        self.logger.debug("stop M4B")
        self.result = self.objref.stop(M4B)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test5563_acu_axis_mode_slew(self):
        self.logger.debug("slew M4B")
        self.result = self.objref.slew(M4B, 0.5)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test5564_acu_axis_mode_slew(self):
        self.logger.debug("slew M4B")
        self.result = self.objref.slew(M4B, -0.5)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])


if __name__ == "__main__":
    unittest.main()
