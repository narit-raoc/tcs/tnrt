# test GST0hUT
import datetime
import numpy as np
from astropy import units as u
from astropy.time import Time
from astropy.coordinates import EarthLocation

print('Creating Astropy EarthLocation from lon, lat, alt, WGS84')
# Site coordinates
lon = 100.0
lat = 20.0
height = 0.0
loc = EarthLocation(EarthLocation.from_geodetic(lon=lon * u.deg,
										lat=lat * u.deg,
										height=height * u.m,
										ellipsoid='WGS84'))

datetime_now = datetime.datetime.utcnow()

datetime_start = datetime_now
#datetime_start = datetime.datetime(2019, 9, 22)

datetime_start_0hUTC = datetime.datetime(datetime_start.year, datetime_start.month, datetime_start.day)

# astropoy_time_0hUT = Time(datetime_today_0hUT, scale='ut1')
# Calculate GST0hUT1 using Astropy
astropy_time_start_0hUT1 = Time(datetime_start_0hUTC, scale='ut1', location=loc)
GST0hUT1 = astropy_time_start_0hUT1.sidereal_time('apparent', longitude='greenwich')

milliseconds_per_solar_day = 86400*1000.0

# Let Astropy tellme how long a sidereal day is so we are sure to use the same numbers
day1 = Time(Time.now())
day2 = day1 + 1*u.sday
milliseconds_per_sidereal_day = 1e3*(day2 - day1).sec
print('milliseconds_per_sidereal_day \t%f' % milliseconds_per_sidereal_day)
#milliseconds_per_sidereal_day = 86164090.53
GST0hUT1_ms_float = milliseconds_per_solar_day*(GST0hUT1.deg / 360.0)
GST0hUT1_ms_int = np.round(milliseconds_per_solar_day*(GST0hUT1.deg / 360.0))

print('datetime now: \t\t\t%s ' % datetime_start)
print('datetime today 0h: \t\t%s' % datetime_start_0hUTC)
print('astropy_time_today_0hUT: \t%s' % astropy_time_start_0hUT1)
print('GST0hUT_deg: \t\t\t%f' % GST0hUT1.deg)

print('GST0hUT_ms_float: \t\t%f' % GST0hUT1_ms_float)
print('GST0hUT_ms_int: \t\t%d' % GST0hUT1_ms_int)

dut1 = astropy_time_start_0hUT1.delta_ut1_utc.item()
print('DUT1: \t\t\t\t%f' % dut1)

# Calculate local sidereal time using Astropy
astropy_time_start = Time(datetime_start, scale='ut1', location=loc)
LST_apy = astropy_time_start.sidereal_time('apparent')
print('LST_apy [deg] \t\t\t%f' % LST_apy.deg)

# Calculate local sidreal time manually
MSEC = 1
SEC = 1e6
MIN = 60*SEC
HOUR = 60*MIN

microseconds_elapsed = datetime_start.hour*HOUR + datetime_start.minute*MIN + datetime_start.second*SEC + datetime_start.microsecond*MSEC
print('microseconds elapsed \t\t%d' % microseconds_elapsed)

fraction_of_solar_day = microseconds_elapsed/(1000.0*milliseconds_per_solar_day)
fraction_of_sidereal_day = microseconds_elapsed/(1000.0*milliseconds_per_sidereal_day)
degrees_in_day = fraction_of_sidereal_day * 360.0

# Calculate LST manually from GST0hUT1, longitude, and dut1
# First find GST now
GST = (360.0 * GST0hUT1_ms_float / milliseconds_per_solar_day) + degrees_in_day
print('GST [deg] \t\t\t%f' % GST)

# Add longitude
LST_manual = (GST + loc.lon.deg) % 360.0
print('LST_manual [deg] \t\t%f' % LST_manual)

# Calculate difference of LST in arcseconds
delta_LST = (LST_manual - LST_apy.deg)
print('LST_manual - LST_apy [deg] \t%f: ' % delta_LST )
print('LST_manual - LST_apy [arcsec] \t%f: ' % (delta_LST*3600))

