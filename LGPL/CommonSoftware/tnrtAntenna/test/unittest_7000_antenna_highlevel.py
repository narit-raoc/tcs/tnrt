# Project: TNRT - Thai National Radio Telescope
# Organization: NARIT - National Astronomical Research Institute of Thailand
# Author: Spiro Sarris
# Date: 2022.05.10

import logging
import numpy as np
import time
from astropy.time import Time
from astropy import units
from astropy.coordinates import SkyCoord
from astropy.coordinates import EarthLocation


from unittest_common import AcuTestCase
from unittest_common import AcuRemoteCommandTestCase

# import TCS modules
import tnrtAntennaMod
import DataFormatAcu

# Shortcut / alias
from tnrtAntennaMod import AZ as AZ
from tnrtAntennaMod import EL as EL
from tnrtAntennaMod import TR as TR
from tnrtAntennaMod import HXP as HXP
from tnrtAntennaMod import M3 as M3
from tnrtAntennaMod import M3R as M3R
from tnrtAntennaMod import M4A as M4A
from tnrtAntennaMod import M4B as M4B
from tnrtAntennaMod import VX as VX
from tnrtAntennaMod import THU as THU
from tnrtAntennaMod import GRS as GRS
from tnrtAntennaMod import DONE as DONE
from tnrtAntennaMod import ACCEPTED as ACCEPTED
from tnrtAntennaMod import PARAM_ERROR as PARAM_ERROR


class Test7000_Status(AcuTestCase):
    def setUp(self):
        super().setUp()
        self.result = None

    def tearDown(self):
        self.logger.info(self.result)
        super().tearDown()

    def test7003_status_highlevel(self):
        self.logger.debug("azError()")
        self.result = self.objref.azError()
        self.assertIsInstance(self.result, float)

    def test7004_status_highlevel(self):
        self.logger.debug("elError()")
        self.result = self.objref.elError()
        self.assertIsInstance(self.result, float)

    def test7007_status_highlevel(self):
        self.logger.debug("M2status()")
        self.result = self.objref.M2status()
        self.assertIsInstance(self.result, tnrtAntennaMod.m2StatusStructure)

    def test7008_status_highlevel(self):
        self.logger.debug("antennaStatus()")
        self.result = self.objref.antennaStatus()
        self.assertTrue(
            self.result
            in [
                tnrtAntennaMod.hasErrors,
                tnrtAntennaMod.axis_inactive,
                tnrtAntennaMod.interlocked,
                tnrtAntennaMod.halfstowed,
                tnrtAntennaMod.stowed,
                tnrtAntennaMod.stowing,
                tnrtAntennaMod.unstowing,
                tnrtAntennaMod.stopped,
                tnrtAntennaMod.slewing,
                tnrtAntennaMod.tracking,
                tnrtAntennaMod.unknownState,
            ]
        )

    def test7010_slewtime(self):
        self.logger.debug("slewTime(1, 2, tnrtAntennaMod.azel, 60123, 2000, 0, 0)")
        self.result = self.objref.slewTime(1, 2, tnrtAntennaMod.azel, 60123, 2000, 0, 0)
        self.assertIsInstance(self.result, float)


class Test7100_ACS_Notification(AcuTestCase):
    def setUp(self):
        super().setUp()
        # self.result = None

    def tearDown(self):
        # self.logger.info(self.result)
        super().tearDown()

    def test7101_notification_channel(self):
        try:
            self.logger.debug("test notification channel with AntennaStatusNotifyBlock")
            self.objref.test_notification()

        except Exception as e:
            self.fail(e)


class Test7200_LoadTables(AcuRemoteCommandTestCase):
    def setUp(self):
        super().setUp()
        self.objref.deactivate(AZ)
        self.objref.deactivate(EL)
        self.objref.deactivate(HXP)
        self.result = None

    def tearDown(self):
        super().tearDown()

    def test7202_setHexapodElevationOffsetModel1_astro(self):
        self.logger.debug("setHexapodElevationOffsetModel(astroM2ElMode)")
        self.objref.setHexapodElevationOffsetModel(tnrtAntennaMod.astroM2ElMode)

    def test7202_setHexapodElevationOffsetModel2_geo(self):
        self.logger.debug("setHexapodElevationOffsetModel(geoM2ElMode)")
        self.objref.setHexapodElevationOffsetModel(tnrtAntennaMod.geoM2ElMode)

    def test7202_setHexapodElevationOffsetModel3_zero(self):
        self.logger.debug("setHexapodElevationOffsetModel(zeroM2ElMode)")
        self.objref.setHexapodElevationOffsetModel(tnrtAntennaMod.zeroM2ElMode)


class Test7300_Move_Stop(AcuRemoteCommandTestCase):
    def setUp(self):
        super().setUp()

        # THU must be inactive before EL can move
        self.objref.deactivate(THU)

        self.objref.activate(AZ)
        self.objref.activate(EL)
        self.objref.activate(M3)
        self.objref.activate(HXP)
        self.result = None

    def tearDown(self):
        super().tearDown()

    def test7300_gotoAzEl(self):
        try:
            self.logger.debug("gotoAzEl(45.0, 20.0, 1.0, 0.5)")
            self.objref.gotoAzEl(45.0, 20.0, 1.0, 0.5, False, 1.0)
        except Exception as e:
            self.fail(e)

    def test7301_stopAntenna(self):
        try:
            self.logger.debug("stopAntenna(stopM1)")
            self.objref.stopAntenna(tnrtAntennaMod.stopM1)
        except Exception as e:
            self.fail(e)

    def test7302_stopAntenna(self):
        try:
            self.logger.debug("stopAntenna(fullStop)")
            self.objref.stopAntenna(tnrtAntennaMod.fullStop)
        except Exception as e:
            self.fail(e)


class Test7400_TrackingM1(AcuRemoteCommandTestCase):
    def setUp(self):
        super().setUp()
        # THU must be inactive before EL can move
        self.objref.deactivate(THU)

        self.result = self.objref.activate(AZ)
        self.result = self.objref.activate(EL)

    def tearDown(self):
        super().tearDown()

    def test7401_user_pointing_correction(self):
        try:
            self.logger.debug("setUserPointingCorrections 0, 0")
            self.objref.setUserPointingCorrections(0, 0)
        except Exception as e:
            self.fail(e)

    def test7402_track_coord_HO(self):
        try:
            tstart = Time.now().mjd + 2 / 86400.0
            self.logger.debug("trackCoordinateHO(tstart, 1000.0, 30, 15, 0, 0, False, 0.5)")
            self.objref.trackCoordinateHO(tstart, 1000.0, 30, 15, 0, 0, False, 0.5)
        except Exception as e:
            self.fail(e)

    def test7403_track_coord_EQ(self):
        try:
            self.logger.debug(
                "trackCoordinateEQ(tstart, coord_icrs.ra.deg, coord_icrs.dec.deg, 0.1, 1, 0, 0, False, 0.5)"
            )
            az = 30 * units.deg
            el = 40 * units.deg
            loc_apy = EarthLocation(
                EarthLocation.from_geodetic(
                    lon=98 * units.deg,
                    lat=19 * units.deg,
                    height=400 * units.m,
                    ellipsoid="WGS84",
                )
            )
            self.logger.debug(
                "Choose a coordinate above the horizon now. (AZ, EL) = ({}, {}) [deg]".format(
                    az.value, el.value
                )
            )
            coord_altaz = SkyCoord(
                frame="altaz", az=az, alt=el, obstime=Time.now(), location=loc_apy
            )
            coord_icrs = coord_altaz.transform_to("icrs")

            self.logger.debug(
                "Calculated ICRS (RA, DEC) = ({}, {}) [deg]".format(
                    coord_icrs.ra.deg, coord_icrs.dec.deg
                )
            )

            tstart = Time.now().mjd + 2 / 86400.0
            self.objref.trackCoordinateEQ(
                tstart, 
                coord_icrs.ra.deg,
                coord_icrs.dec.deg,
                0,
                0,
                0,
                0,
                False,
                1800,
                900,
                False,
                0.5
                )
        except Exception as e:
            self.fail(e)

    def test7404_track_coord_TLE(self):
        try:
            self.logger.debug("trackCoordinateTLE()")

            line0 = "LEMUR-2-SPIROVISION     "
            line1 = "1 42755U 17019E   22185.10451823  .00028765  00000+0  47696-3 0  9995"
            line2 = "2 42755  51.6329 134.5874 0005315 115.4668 333.2990 15.51888648283958"
            self.objref.trackCoordinateTLE(line0, line1, line2, 0, 0, False, 0.5)
        except Exception as e:
            self.fail(e)

    def test7405_track_offset_pattern_AZ_EL(self):
        try:
            delay = 5 / 86400.0  # start 5 seconds in the future
            tstart = Time.now().mjd + delay
            self.logger.debug("trackCoordinateHO(tstart, 1000.0, 30, 15, 0, 0, True, 0.5)")
            self.objref.trackCoordinateHO(tstart, 1000.0, 30, 15, 0, 0, True, 0.5)
            self.logger.debug("trackOffsetPattern()")
            blocking = False
            update_period = 0.1
            nlines = 5

            trackingTime = 30 / 86400.0  # 30 seconds

            tstart = Time.now().mjd

            tlist = np.linspace(tstart, tstart + trackingTime, nlines)
            xlist = np.linspace(-1, 1, nlines) / 10
            ylist = np.linspace(-1, 1, nlines) / 10

            po_table = []
            for (t, x, y) in zip(tlist, xlist, ylist):
                newitem = tnrtAntennaMod.timeAzRaElDecStruct(t, x, y)
                po_table.append(newitem)

            self.objref.trackOffsetPattern(
                tnrtAntennaMod.newL,
                tnrtAntennaMod.SPLINE,
                tnrtAntennaMod.azel,
                po_table,
                blocking,
                update_period,
            )

            self.logger.logDebug(
                "Wait for tracking to complete, see log in acscommandcenter in AntennaC tab "
            )
            self.objref.blockUntilOffsetTrackingCompleted(trackingTime + delay)
        except Exception as e:
            self.fail(e)

    def test7406_track_coord_SS(self):
        try:
            self.logger.debug("trackCoordinateSS(tstart, 1000, planet, 0, 0, False, 0.5)")
            planet = "sun"
            tstart = Time.now().mjd + 2 / 86400.0
            self.objref.trackCoordinateSS(tstart, 1000, planet, 0, 0, False, 0.5)
        except Exception as e:
            self.fail(e)


# TODO (SS. 05/2022). This causes error in TwinCAT (page fault).  Not required yet.  Figure it out later
# when we need to do focus scan for K-band performance evaluation..
# class Test7500_TrackingM2(AcuRemoteCommandTestCase):
#     def setUp(self):
#         super().setUp()
#         self.result = self.objref.activate(HXP)

#     def tearDown(self):
#         super().tearDown()

#     def test7501_track_pattern_hxp(self):
#         try:
#             self.logger.debug("trackPatternHxp()")
#             blocking = False
#             update_period = 0.1
#             nlines = 5

#             start_mjd = Time.now().mjd + 2 / 86400.0
#             tlim = (start_mjd, start_mjd + 1)
#             hxp_xlim = [0, 0]
#             hxp_ylim = [0, 0]
#             hxp_zlim = [-50, 50]
#             hxp_txlim = [0, 0]
#             hxp_tylim = [0, 0]
#             hxp_tzlim = [0, 0]

#             # Create lists of start and end values to prepare to generate 2D array from numpy linspace
#             start_values = [
#                 tlim[0],
#                 hxp_xlim[0],
#                 hxp_ylim[0],
#                 hxp_zlim[0],
#                 hxp_txlim[0],
#                 hxp_tylim[0],
#                 hxp_tzlim[0],
#             ]
#             end_values = [
#                 tlim[1],
#                 hxp_xlim[1],
#                 hxp_ylim[1],
#                 hxp_zlim[1],
#                 hxp_txlim[1],
#                 hxp_tylim[1],
#                 hxp_tzlim[1],
#             ]

#             # Generate a 2D numpy array for the program offset table.
#             # Conveniently, Numpy generates this array in "row-major" order
#             # so we don't require a transpose before sending to the ACU.
#             table_np = np.linspace(start_values, end_values, nlines)
#             table_idl = []

#             for row in table_np:
#                 table_idl.append(
#                     tnrtAntennaMod.timeXYZTXTYTZStruct(
#                         row[0], row[1], row[2], row[3], row[4], row[5], row[6]
#                     )
#                 )

#             self.objref.trackPatternHxp(
#                 tnrtAntennaMod.newL,
#                 tnrtAntennaMod.SPLINE,
#                 table_idl,
#                 blocking,
#                 update_period,
#             )
#         except Exception as e:
#             self.fail(e)


if __name__ == "__main__":
    unittest.main()
