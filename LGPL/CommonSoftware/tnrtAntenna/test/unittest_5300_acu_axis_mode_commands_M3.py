# Project: TNRT - Thai National Radio Telescope
# Organization: NARIT - National Astronomical Research Institute of Thailand
# Author: Spiro Sarris
# Date: 2022.05.04

import logging
import numpy as np
import time
from astropy.time import Time
from unittest_common import AcuRemoteCommandTestCase

# import TCS modules
import tnrtAntennaMod
import DataFormatAcu

# Shortcut / alias
from tnrtAntennaMod import EL as EL
from tnrtAntennaMod import M3 as M3
from tnrtAntennaMod import DONE as DONE
from tnrtAntennaMod import ACCEPTED as ACCEPTED
from tnrtAntennaMod import PARAM_ERROR as PARAM_ERROR


class State5300_M3_Inactive_NoSync(AcuRemoteCommandTestCase):
    def setUp(self):
        super().setUp()
        self.result = self.objref.m3SyncEl(tnrtAntennaMod.INACTIVE)
        self.result = self.objref.deactivate(M3)
        self.result = None

    def tearDown(self):
        self.logger.info(self.result)
        super().tearDown()

    def test5301_acu_axis_mode_reset(self):
        self.logger.debug("reset M3")
        self.result = self.objref.reset(M3)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test5302_acu_axis_mode_activate(self):
        self.logger.debug("activate M3")
        self.result = self.objref.activate(M3)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])


class State5310_M3_Inactive_SyncElevation(AcuRemoteCommandTestCase):
    def setUp(self):
        super().setUp()
        self.result = self.objref.m3SyncEl(tnrtAntennaMod.ACTIVE)
        # NOTE: we cannot explicitly deactivate M3 when it is sychronized to EL.
        # We must deactivate EL, and M3 will follow
        self.result = self.objref.deactivate(EL)
        self.result = None

    def tearDown(self):
        self.logger.info(self.result)
        super().tearDown()

    def test5312_acu_axis_mode_sync_elevation(self):
        self.logger.debug("m3SyncEl enabled")
        self.result = self.objref.m3SyncEl(tnrtAntennaMod.ACTIVE)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])


class State5320_M3_Active_NoSync(AcuRemoteCommandTestCase):
    def setUp(self):
        super().setUp()
        self.result = self.objref.m3SyncEl(tnrtAntennaMod.INACTIVE)
        self.result = self.objref.activate(M3)
        # hack flow control. after activate, we need some time before it can accept a command
        # even though Antenna.py is checking bits and doing flow control already.
        time.sleep(2)
        self.result = None

    def tearDown(self):
        self.logger.info(self.result)
        self.result = self.objref.stop(M3)

        super().tearDown()

    def test5321_acu_axis_mode_stop(self):
        self.logger.debug("stop M3")
        self.result = self.objref.stop(M3)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test5322_acu_parameter_positionOffset_M3(self):
        self.logger.debug("positionOffset M3 1.0")
        self.result = self.objref.positionOffset(M3, 1.0)
        self.assertTrue(self.result in [ACCEPTED, DONE])

    def test5323_acu_parameter_positionOffset_M3(self):
        self.logger.debug("positionOffset M3 0")
        self.result = self.objref.positionOffset(M3, 0)
        self.assertTrue(self.result in [ACCEPTED, DONE])

    def test5324_acu_axis_mode_absolute_position(self):
        self.logger.debug("absolutePosition M3 10 0.5")
        # Note: the old ICD (and what I see in the ACU local config) shows
        # velocity limit is 1.0.  However, any value > 0.3 returns PARAM_ERROR.
        # For now, set 0.1 to make the unit test pass  After OHB installs the
        # new M3, they should fix the limits of position and velocity.
        self.result = self.objref.absolutePosition(M3, 10, 0.1, False)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])


class State5330_M3_Active_SyncElevation(AcuRemoteCommandTestCase):
    def setUp(self):
        super().setUp()
        self.result = self.objref.m3SyncEl(tnrtAntennaMod.ACTIVE)
        self.result = self.objref.activate(EL)
        self.result = None

    def tearDown(self):
        self.logger.info(self.result)
        self.result = self.objref.stop(EL)

        super().tearDown()

    def test5331_acu_axis_mode_sync_elevation(self):
        self.logger.debug("m3SyncEl enabled")
        self.result = self.objref.m3SyncEl(tnrtAntennaMod.ACTIVE)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])


if __name__ == "__main__":
    unittest.main()
