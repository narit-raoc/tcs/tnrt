# Project: TNRT - Thai National Radio Telescope
# Organization: NARIT - National Astronomical Research Institute of Thailand
# Author: Spiro Sarris
# Date: 2022.05.04

import logging
from unittest_common import AcuRemoteCommandTestCase

# import TCS modules
import tnrtAntennaMod
import DataFormatAcu

# Shortcut / alias
from tnrtAntennaMod import VX as VX
from tnrtAntennaMod import DONE as DONE
from tnrtAntennaMod import ACCEPTED as ACCEPTED
from tnrtAntennaMod import PARAM_ERROR as PARAM_ERROR


class State5600_VX(AcuRemoteCommandTestCase):
    def setUp(self):
        super().setUp()
        self.result = None

    def tearDown(self):
        self.logger.info(self.result)
        super().tearDown()

    def test5601_acu_axis_mode_deactivate(self):
        self.logger.debug("deactivate VX")
        self.result = self.objref.deactivate(VX)
        self.assertTrue(self.result in [tnrtAntennaMod.PARAM_ERROR])
        # NOTE: the standard activate and deactivate functions don't work for VX
        # So, I assume that it is always active

    def test5602_acu_axis_mode_activate(self):
        self.logger.debug("activate VX")
        self.result = self.objref.activate(VX)
        self.assertTrue(self.result in [tnrtAntennaMod.PARAM_ERROR])

    def test5603_acu_axis_mode_reset(self):
        self.logger.debug("reset VX")
        self.result = self.objref.reset(VX)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test5604_acu_axis_mode_open(self):
        self.logger.debug("open VX")
        self.result = self.objref.openVX()
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test5605_acu_axis_mode_stop(self):
        self.logger.debug("stop VX")
        self.result = self.objref.stop(VX)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test5607_acu_axis_mode_close(self):
        self.logger.debug("close VX")
        self.result = self.objref.closeVX()
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])


if __name__ == "__main__":
    unittest.main()
