# Project: TNRT - Thai National Radio Telescope
# Organization: NARIT - National Astronomical Research Institute of Thailand
# Author: Spiro Sarris
# Date: 2022.05.04

import logging
import numpy as np
import time
from astropy.time import Time
from unittest_common import AcuRemoteCommandTestCase

# import TCS modules
import tnrtAntennaMod
import DataFormatAcu

# Shortcut / alias
from tnrtAntennaMod import EL as EL
from tnrtAntennaMod import THU as THU
from tnrtAntennaMod import DONE as DONE
from tnrtAntennaMod import ACCEPTED as ACCEPTED
from tnrtAntennaMod import PARAM_ERROR as PARAM_ERROR


class State5100_EL_Inactive(AcuRemoteCommandTestCase):
    def setUp(self):
        # Use parent class setup to configure logger and connect ACU
        super().setUp()

        # Deactivate axes and clear errors from previous test
        self.result = self.objref.deactivate(THU)
        self.result = self.objref.deactivate(EL)
        self.result = self.objref.reset(EL)
        self.result = self.objref.m3SyncEl(tnrtAntennaMod.INACTIVE)
        self.result = None

    def tearDown(self):
        self.logger.info(self.result)
        self.result = self.objref.unstow(EL, tnrtAntennaMod.stowpinBoth)
        super().tearDown()

    def test5101_acu_axis_mode_deactivate(self):
        self.logger.debug("deactivate EL")
        self.result = self.objref.deactivate(EL)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test5102_acu_axis_mode_activate(self):
        self.logger.debug("activate EL")
        self.result = self.objref.activate(EL)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test5103_acu_axis_mode_reset(self):
        self.logger.debug("reset EL")
        self.result = self.objref.reset(EL)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test5104_acu_axis_mode_drivetopark(self):
        self.logger.debug("driveToPark EL 0.5")
        self.result = self.objref.driveToPark(EL, 0.5)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])


class State5150_EL_Active(AcuRemoteCommandTestCase):
    def setUp(self):
        # Use parent class setup to configure logger and connect ACU
        super().setUp()

        # THU must not be active when we move EL (safety of THU cable chain)
        self.result = self.objref.deactivate(THU)

        self.result = self.objref.m3SyncEl(tnrtAntennaMod.INACTIVE)
        self.result = self.objref.activate(EL)
        # Note - if the axis was deactivated, then activate in unittest setUp(),
        # and then call next function immediately, it responds WRONG_MODE.
        # Looks like the
        # ACU status bits indicate too early that EL axis is active (tnrtAntenna.py flow control)
        # when it is actually not 100% active (enough for a stop command)
        time.sleep(2)
        self.result = None

    def tearDown(self):
        self.logger.info(self.result)
        self.result = self.objref.stop(EL)

        current_el = self.objref.getElPosition()
        if current_el > 88:
            self.objref.absolutePosition(EL, 88, 0.5, False)

        super().tearDown()

    def test5151_acu_axis_mode_deactivate(self):
        self.logger.debug("deactivate EL")
        self.result = self.objref.deactivate(EL)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test5152_acu_axis_mode_stop(self):
        self.logger.debug("stop EL")
        self.result = self.objref.stop(EL)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test5153_acu_axis_mode_absolute_position(self):
        self.logger.debug("absolutePosition EL 45 0.1")
        self.result = self.objref.absolutePosition(EL, 45, 0.1, False)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test5154_acu_axis_mode_relative_position(self):
        self.logger.debug("relativePosition EL 5 1")
        self.result = self.objref.relativePosition(EL, 5, 0.1)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test5155_acu_axis_mode_slew(self):
        self.logger.debug("slew EL 1.0")
        self.result = self.objref.slew(EL, 0.1)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test5156_acu_axis_mode_slew(self):
        self.logger.debug("slew EL -0.1")
        self.result = self.objref.slew(EL, -0.1)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test5157_acu_axis_mode_programTrack_EL(self):
        self.logger.debug("programTrack EL 0.5")
        self.result = self.objref.programTrack(EL, 0.5)
        self.assertTrue(self.result in [ACCEPTED, DONE])

    def test5161_acu_axis_mode_predefined_position(self):
        self.logger.debug("predefinePosition EL 2 0.5 (position 2 is 30 deg")
        self.result = self.objref.predefinePosition(EL, tnrtAntennaMod.PresetPos2, 0.5)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test5162_acu_parameter_positionOffset_EL(self):
        self.logger.debug("positionOffset EL 1.0")
        self.result = self.objref.positionOffset(EL, 1.0)
        self.assertTrue(self.result in [ACCEPTED, DONE])

    def test5163_acu_parameter_positionOffset_EL(self):
        self.logger.debug("positionOffset EL 0")
        self.result = self.objref.positionOffset(EL, 0)
        self.assertTrue(self.result in [ACCEPTED, DONE])


if __name__ == "__main__":
    unittest.main()
