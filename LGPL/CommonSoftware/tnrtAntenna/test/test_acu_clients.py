# standard python
import socket
import ctypes
import time
import threading
import logging
import coloredlogs
import socket

# modules from pip
from astropy.time import Time

# modules from TCS
import DataFormatAcu
from AcuCommandClient import AcuCommandClient


class AcuClientTest:
    def __init__(self):
        self.logger = logging.getLogger("AcuCommandClient")
        self.logger.setLevel(logging.DEBUG)

        fmt_scrn = "%(asctime)s [%(levelname)s]: %(message)s"

        level_styles_scrn = {
            "critical": {"color": "red", "bold": True},
            "debug": {"color": "white", "faint": True},
            "error": {"color": "red"},
            "info": {"color": "green", "bright": True},
            "notice": {"color": "magenta"},
            "spam": {"color": "green", "faint": True},
            "success": {"color": "green", "bold": True},
            "verbose": {"color": "blue"},
            "warning": {"color": "yellow", "bright": True, "bold": True},
        }
        field_styles_scrn = {
            "asctime": {},
            "hostname": {"color": "magenta"},
            "levelname": {"color": "cyan", "bright": True},
            "name": {"color": "blue", "bright": True},
            "programname": {"color": "cyan"},
        }
        formatter_screen = coloredlogs.ColoredFormatter(
            fmt=fmt_scrn, level_styles=level_styles_scrn, field_styles=field_styles_scrn
        )

        # creating a handler to log on the console
        handler_screen = logging.StreamHandler()
        handler_screen.setFormatter(formatter_screen)
        handler_screen.setLevel(logging.DEBUG)

        # remove all handlers if they exist
        # remove because PySimpleClient logger already has handlers which
        # show duplicate of my log messages, but without the pretty color format.
        if len(self.logger.handlers) > 0:
            self.logger.handlers = []

        # adding handlers
        self.logger.addHandler(handler_screen)
        self.logger.debug("Started logger name %s" % self.logger.name)

        try:
            acu_hostname = "acu"
            self.HOST = socket.gethostbyname(acu_hostname)
        except Exception as e:
            self.logger.error(
                "{}: '{}'.  Check file /etc/hosts".format(e, acu_hostname)
            )
            exit()
        self.PORT_CMD = 9000  # The port used by the server
        self.PORT_STS = 9001  # The port used by the server
        self.logger.info(
            "Connect status client to: {}:{}".format(self.HOST, self.PORT_STS)
        )

        # Flow control Events
        self.acu_status_message_received = threading.Event()
        self.acu_status_stop_event = threading.Event()
        self.acu_command_remote_connected = threading.Event()

        self.logger.info("ACU connection state: [0/4] Not Connected")
        # Start the status client
        self.thread_acu_status = threading.Thread(target=self.start_acu_status_client)
        self.thread_acu_status.start()

        self.acu_status_message_received.wait(timeout=10.0)
        # If we break the wait for timeout, do nothing.  wait for script to exit
        # But if we break the wait because data arrived, start the command client.
        if self.acu_status_message_received.is_set() == True:
            self.logger.info(
                "ACU connection state: [2/4] Received first STATUS message packet"
            )
        self.start_acu_command_client()

    def start_acu_status_client(self):
        self.acu_status_message_received.clear()
        self.acu_status_stop_event.clear()
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            # self.logger.info('start connect STATUS ...')
            s.connect((self.HOST, self.PORT_STS))
            s.settimeout(10)
            self.logger.info(
                "ACU connection state: [1/4] STATUS port connected. Waiting for first packet ..."
            )

            first_message_received = False
            mjd0 = 0

            while not self.acu_status_stop_event.is_set():
                segments = []
                bytes_received = 0

                while bytes_received < ctypes.sizeof(DataFormatAcu.StatusMessageFrame):
                    remaining_bytes = (
                        ctypes.sizeof(DataFormatAcu.StatusMessageFrame) - bytes_received
                    )
                    self.logger.debug(
                        "bytes received: {:d}, remaining_bytes: {:d}".format(
                            bytes_received, remaining_bytes
                        )
                    )
                    segment = s.recv(remaining_bytes, socket.MSG_WAITALL)
                    self.logger.debug(
                        "bytes received: {:d}, remaining_bytes: {:d}".format(
                            bytes_received, remaining_bytes
                        )
                    )
                    if segment == b"":
                        raise RuntimeError("socket connection broken")
                    segments.append(segment)
                    bytes_received = bytes_received + len(segment)
                raw_bytes = b"".join(segments)

                status_message = DataFormatAcu.StatusMessageFrame.from_buffer_copy(
                    raw_bytes
                )

                if first_message_received is False:
                    mjd0 = status_message.general_sts.actual_time

                first_message_received = True

                self.logger.info("start_flag: {}, status_message_type: {}, message_length: {}, status_message_counter: {}, version: {}".format(
                    status_message.start_flag,
                    status_message.status_message_type,
                    status_message.message_length,
                    status_message.status_message_counter,
                    status_message.general_sts.version,
                ))

                # self.logger.debug("TrackingStatus: {}".format(status_message.tracking_sts))
                # self.logger.debug("AxisStatus AZ(0): {}".format(status_message.axis_status[0]))
                # self.logger.debug("AxisStatus EL(1): {}".format(status_message.axis_status[1]))
                
                # self.logger.info(
                #     "site (lat, lon, height) = {},{},{}".format(
                #         status_message.tracking_sts.antenna_latitude,
                #         status_message.tracking_sts.antenna_longitude,
                #         status_message.tracking_sts.antenna_altitude,
                #     )
                # )
                self.acu_status_message_received.set()
                if (status_message.general_sts.control_mode_status & DataFormatAcu.CONTROL_MODE_STATUS_REMOTE_CMD_CONNECT) > 0:
                    self.acu_command_remote_connected.set()
            s.close()

    def start_acu_command_client(self):
        self.acu_command_remote_connected.clear()

        self.acu_command_client = AcuCommandClient(self.HOST, self.PORT_CMD, 10.0)
        self.acu_command_client.start()

        # blocking for command client to cconnect before this thread uses the client
        # self.acu_command_client.connect_success_event.wait(timeout=10.0)

        self.logger.info(
            "ACU connection state: [3/4] COMMAND port connected. Waiting for controlModeSts_Remote_Cmd_connect ..."
        )
        self.acu_command_remote_connected.wait(timeout=20.0)

        if self.acu_command_remote_connected.is_set() == True:
            self.logger.info("ACU connect state: [4/4] Ready to send commands")
            while True:
                try:
                    # Set control mode to RCP (Remote Control)
                    ack = self.acu_command_client.send_standard_command(
                        DataFormatAcu.CMD_PARAM,
                        DataFormatAcu.SUBSYSTEM_SYSTEM,
                        DataFormatAcu.CMD_SET_MASTER,
                        DataFormatAcu.CMD_SET_MASTER_REMOTE,
                        0.0,
                    )
                    time.sleep(2.0)

                    # Set control mode to LCP (Local Control)
                    ack = self.acu_command_client.send_standard_command(
                        DataFormatAcu.CMD_PARAM,
                        DataFormatAcu.SUBSYSTEM_SYSTEM,
                        DataFormatAcu.CMD_SET_MASTER,
                        DataFormatAcu.CMD_SET_MASTER_LCP,
                        0.0,
                    )
                    time.sleep(2.0)
                except KeyboardInterrupt:
                    break
        else:
            # Timeout waiting for command port to connect.  Set event to stop listenting for status msg
            self.logger.info(
                "ACU connect state: Timeout waiting for acu_command_remote_connected.is_set()"
            )
            self.acu_status_stop_event.set()

        self.acu_command_client.client_close()

        # End the infinite loop in acu status thread
        self.acu_status_stop_event.set()


if __name__ == "__main__":
    self = AcuClientTest()
