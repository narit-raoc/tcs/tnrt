# Project: TNRT - Thai National Radio Telescope
# Organization: NARIT - National Astronomical Research Institute of Thailand
# Author: Spiro Sarris
# Date: 2022.05.04

import logging
import numpy as np
from astropy.time import Time
from unittest_common import AcuRemoteCommandTestCase

# import TCS modules
import tnrtAntennaMod
import DataFormatAcu

# Shortcut / alias
from tnrtAntennaMod import M4A as M4A
from tnrtAntennaMod import M4B as M4B
from tnrtAntennaMod import DONE as DONE
from tnrtAntennaMod import ACCEPTED as ACCEPTED
from tnrtAntennaMod import PARAM_ERROR as PARAM_ERROR


class State5500_M4A_Inactive(AcuRemoteCommandTestCase):
    def setUp(self):
        super().setUp()
        self.result = self.objref.deactivate(M4A)
        self.result = None

    def tearDown(self):
        self.logger.info(self.result)
        super().tearDown()

    def test5501_acu_axis_mode_deactivate(self):
        self.logger.debug("deactivate M4A")
        self.result = self.objref.deactivate(M4A)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test5502_acu_axis_mode_activate(self):
        self.logger.debug("activate M4A")
        self.result = self.objref.activate(M4A)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test5503_acu_axis_mode_reset(self):
        self.logger.debug("reset M4A")
        self.result = self.objref.reset(M4A)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])


class State5510_M4A_Active(AcuRemoteCommandTestCase):
    def setUp(self):
        super().setUp()
        self.result = self.objref.activate(M4A)
        self.result = None

    def tearDown(self):
        self.logger.info(self.result)
        self.objref.stop(M4A)
        super().tearDown()

    def test5511_acu_axis_mode_deactivate(self):
        self.logger.debug("deactivate M4A")
        self.result = self.objref.deactivate(M4A)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test5512_acu_axis_mode_stop(self):
        self.logger.debug("stop M4A")
        self.result = self.objref.stop(M4A)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test5513_acu_axis_mode_slew(self):
        self.logger.debug("slew M4A")
        self.result = self.objref.slew(M4A, 0.5)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test5514_acu_axis_mode_slew(self):
        self.logger.debug("slew M4A")
        self.result = self.objref.slew(M4A, -0.5)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])


if __name__ == "__main__":
    unittest.main()
