# Project: TNRT - Thai National Radio Telescope
# Organization: NARIT - National Astronomical Research Institute of Thailand
# Author: Spiro Sarris
# Date: 2022.05.04

import logging
from unittest_common import AcuRemoteCommandTestCase
import time

# import TCS modules
import tnrtAntennaMod
import DataFormatAcu

# Shortcut / alias
from tnrtAntennaMod import GRS as GRS
from tnrtAntennaMod import DONE as DONE
from tnrtAntennaMod import ACCEPTED as ACCEPTED
from tnrtAntennaMod import PARAM_ERROR as PARAM_ERROR


class State5800_GRS_Inactive(AcuRemoteCommandTestCase):
    def setUp(self):
        super().setUp()
        self.result = self.objref.deactivate(GRS)
        self.result = None

    def tearDown(self):
        self.logger.info(self.result)
        super().tearDown()

    def test5801_acu_axis_mode_deactivate(self):
        self.logger.debug("deactivate GRS")
        self.result = self.objref.deactivate(GRS)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test5802_acu_axis_mode_activate(self):
        self.logger.debug("activate GRS")
        self.result = self.objref.activate(GRS)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test5802_acu_axis_mode_reset(self):
        self.logger.debug("reset GRS")
        self.result = self.objref.reset(GRS)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])


class State5850_GRS_Active(AcuRemoteCommandTestCase):
    def setUp(self):
        super().setUp()
        self.result = self.objref.activate(GRS)
        time.sleep(2)
        self.result = None

    def tearDown(self):
        self.logger.info(self.result)
        self.result = self.objref.stop(GRS)
        super().tearDown()

    def test5851_acu_axis_mode_deactivate(self):
        self.logger.debug("deactivate GRS")
        self.result = self.objref.deactivate(GRS)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test5852_acu_axis_mode_stop(self):
        self.logger.debug("stop GRS")
        self.result = self.objref.stop(GRS)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test5853_acu_axis_mode_absolute_position(self):
        self.logger.debug("absolutePosition GRS 10 2")
        self.result = self.objref.absolutePosition(GRS, 10, 2.0, False)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test5854_acu_axis_mode_slew(self):
        self.logger.debug("slew GRS 1.0")
        self.result = self.objref.slew(GRS, 1.0)
        time.sleep(3)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test5855_acu_axis_mode_slew(self):
        self.logger.debug("slew GRS 1.0")
        self.result = self.objref.slew(GRS, -1.0)
        time.sleep(3)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test5856_acu_axis_mode_predefined_position(self):
        self.logger.debug("predefinePosition GRS position 1 2")
        self.result = self.objref.predefinePosition(GRS, tnrtAntennaMod.PresetPos1, 2.0)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test5857_acu_axis_mode_predefined_position(self):
        self.logger.debug("predefinePosition GRS position 2 2")
        self.result = self.objref.predefinePosition(GRS, tnrtAntennaMod.PresetPos2, 2.0)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test5858_acu_axis_mode_predefined_position(self):
        self.logger.debug("predefinePosition GRS position 3 2")
        self.result = self.objref.predefinePosition(GRS, tnrtAntennaMod.PresetPos3, 2.0)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test5859_acu_axis_mode_drivetopark(self):
        self.logger.debug("driveToPark GRS 3")
        self.result = self.objref.driveToPark(GRS, 3)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])


if __name__ == "__main__":
    unittest.main()
