This directory contains example files that can the operator can copy to the ACU local file system and load from the MTM GUI.

Directory names must match exactly because ACU allows the user to select the file -- but not the complete file path.

File extensions must match the name pattern too.

Satellite Tracking TLE files:
path on ACU file system: C:\MT_Mechatronics\ACU\TrackingFiles\NORAD_TLE\
file extention: .tle
example file in this directory: ./ACU/TrackingFiles/NORAD_TLE/intelsat22.tle

Program Track Table (main tracking source):
path on ACU file system: C:\MT_Mechatronics\ACU\TrackingFiles\ProgTrack
file extension: .ptf
example file ./ACU/TrackingFiles/ProgTrack/track_azel.pto

Program Offset Table (subscan patterns):
path on ACU file system: C:\MT_Mechatronics\ACU\TrackingFiles\ProgOffset
file extension: .pto
example file ./ACU/TrackingFiles/ProgOffset/offset_azel.pto

Hexapod Program Track:
path on ACU file system: C:\MT_Mechatronics\HCU\TrackingFiles\HxpProgTrack\
file extention: .hpt
example file: ./HCU/TrackingFiles/HxpProgTrack/focusz.hpt

Hexapod Elevation Offset:
path on ACU file system: C:\MT_Mechatronics\HCU\CorrParam\HxpELOffsetFiles\
file extension: .elo
example file: ./HCU/CorrParam/HxpELOffsetFiles/elastro.elo