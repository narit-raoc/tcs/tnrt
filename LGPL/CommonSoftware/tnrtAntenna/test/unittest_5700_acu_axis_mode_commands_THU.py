# Project: TNRT - Thai National Radio Telescope
# Organization: NARIT - National Astronomical Research Institute of Thailand
# Author: Spiro Sarris
# Date: 2022.05.04

import logging
import numpy as np
import time
from unittest_common import AcuRemoteCommandTestCase

# import TCS modules
import tnrtAntennaMod
import DataFormatAcu

# Shortcut / alias
from tnrtAntennaMod import THU as THU
from tnrtAntennaMod import EL as EL
from tnrtAntennaMod import AZ as AZ
from tnrtAntennaMod import DONE as DONE
from tnrtAntennaMod import ACCEPTED as ACCEPTED
from tnrtAntennaMod import PARAM_ERROR as PARAM_ERROR


class State5700_THU_Inactive(AcuRemoteCommandTestCase):
    def setUp(self):
        super().setUp()
        self.result = self.objref.deactivate(THU)
        self.result = self.objref.reset(THU)
        self.result = None

    def tearDown(self):
        self.logger.info(self.result)
        self.result = self.objref.unstow(THU, tnrtAntennaMod.stowpinBoth)
        super().tearDown()

    def test5701_acu_axis_mode_deactivate(self):
        self.logger.debug("deactivate THU")
        self.result = self.objref.deactivate(THU)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test5702_acu_axis_mode_activate(self):
        self.logger.debug("activate THU")
        self.result = self.objref.activate(THU)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test5702_acu_axis_mode_reset(self):
        self.logger.debug("reset THU")
        self.result = self.objref.reset(THU)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])


class State5710_THU_Active(AcuRemoteCommandTestCase):
    def setUp(self):
        super().setUp()
        self.result = self.objref.activate(THU)
        # Activate is a blocking function, but the status bit arrives active before it is actually
        # ready to receive the command in active mode.  perhaps an artifact of simulator or an issue
        # in the real ACU too.  From experience on ACU simulator test, we need approximately 2 more
        # seconds to guarantee the next command works while fully active.
        time.sleep(2)
        self.result = None

    def tearDown(self):
        self.logger.info(self.result)
        self.result = self.objref.stop(THU)
        self.result = self.objref.unstow(THU, tnrtAntennaMod.stowpinBoth)
        super().tearDown()

    def test5711_acu_axis_mode_deactivate(self):
        self.logger.debug("deactivate THU")
        self.result = self.objref.deactivate(THU)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test5712_acu_axis_mode_stop(self):
        self.logger.debug("stop THU")
        self.result = self.objref.stop(THU)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])


class State5720_THU_Active_EL_zenith(AcuRemoteCommandTestCase):
    def setUp(self):
        super().setUp()
        # Must deactivate THU before we can move EL (safety of THU hardware)
        self.result = self.objref.deactivate(THU)
        self.result = self.objref.activate(EL)
        self.result = self.objref.activate(AZ)
        # Activate is a blocking function, but the status bit arrives active before it is actually
        # ready to receive the command in active mode.  perhaps an artifact of simulator or an issue
        # in the real ACU too.  From experience on ACU simulator test, we need approximately 2 more
        # seconds to guarantee the next command works while fully active.
        time.sleep(2)

        # Set EL up to zenith (approximately 90 degrees).  EL must be in this "up"
        # postion before we can rotate the THU.  If not, the cable tray will be
        # destroyed.
        self.result = self.objref.absolutePosition(EL, 88, 1, False)

        # TODO: THU doesn't care where the AZ is, but the flow control function
        # tnrtAntenna.blockUntilTracking doesn't work correctly if AZ has been reset.
        # (because desired position is not equal to encoder position)
        # Therefore set the AZ desired position to the same as the current encoder
        # here to make the flow control work until it is improved.
        self.result = self.objref.absolutePosition(AZ, self.objref.getAzPosition(), 2, False)
        self.objref.blockUntilTracking(2.0)

        # Now activate THU and prepare for the test
        self.result = self.objref.activate(THU)

        # Activate is a blocking function, but the status bit arrives active before it is actually
        # ready to receive the command in active mode.  perhaps an artifact of simulator or an issue
        # in the real ACU too.  From experience on ACU simulator test, we need approximately 2 more
        # seconds to guarantee the next command works while fully active.
        time.sleep(2)

        self.result = None

    def tearDown(self):
        self.logger.info(self.result)
        self.result = self.objref.stop(THU)
        self.result = self.objref.stop(EL)
        self.result = self.objref.unstow(THU, tnrtAntennaMod.stowpinBoth)
        super().tearDown()

    def test5720_acu_axis_mode_absolute_position(self):
        self.logger.debug("absolutePosition THU 0 0.2")
        self.result = self.objref.absolutePosition(THU, 90, 0.2, False)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test5722_acu_axis_mode_relative_position(self):
        self.logger.debug("relativePosition THU 0 0.2")
        self.result = self.objref.relativePosition(THU, 1, 0.2)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])

    def test5724_acu_axis_mode_slew(self):
        self.logger.debug("slew THU 2.0")
        self.result = self.objref.slew(THU, 0.2)
        self.assertTrue(self.result in [tnrtAntennaMod.ACCEPTED, tnrtAntennaMod.DONE])


if __name__ == "__main__":
    unittest.main()
