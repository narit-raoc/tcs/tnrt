# Project: TNRT - Thai National Radio Telescope
# Organization: NARIT - National Astronomical Research Institute of Thailand
# Author: Spiro Sarris

# import standard modules
import unittest
import logging
import coloredlogs
import time
import numpy as np

from astropy.time import Time
from astropy import units as units

# import ACS modules
from Acspy.Clients.SimpleClient import PySimpleClient

# import TCS modules
from Supplier import Supplier
import DataAggregatorMod
import tnrtAntennaMod
import DataSimulator
import AntennaDefaults



class Tests(unittest.TestCase):
    def setUp(self):
        """
        This function runs 1 time per test_xxx function in this class. before test start
        """
        # ================
        # Configure logger
        # ================
        self.logger = logging.getLogger("TestCase")
        self.logger.setLevel(logging.DEBUG)

        fmt_scrn = "%(asctime)s [%(levelname)s]: %(message)s"

        level_styles_scrn = {
            "critical": {"color": "red", "bold": True},
            "debug": {"color": "white", "faint": True},
            "error": {"color": "red"},
            "info": {"color": "green", "bright": True},
            "notice": {"color": "magenta"},
            "spam": {"color": "green", "faint": True},
            "success": {"color": "green", "bold": True},
            "verbose": {"color": "blue"},
            "warning": {"color": "yellow", "bright": True, "bold": True},
        }
        field_styles_scrn = {
            "asctime": {},
            "hostname": {"color": "magenta"},
            "levelname": {"color": "cyan", "bright": True},
            "name": {"color": "blue", "bright": True},
            "programname": {"color": "cyan"},
        }
        formatter_screen = coloredlogs.ColoredFormatter(
            fmt=fmt_scrn, level_styles=level_styles_scrn, field_styles=field_styles_scrn
        )

        # creating a handler to log on the console
        handler_screen = logging.StreamHandler()
        handler_screen.setFormatter(formatter_screen)
        handler_screen.setLevel(logging.DEBUG)

        # remove all handlers if they exist
        # remove because PySimpleClient logger already has handlers which
        # show duplicate of my log messages, but without the pretty color format.
        if len(self.logger.handlers) > 0:
            self.logger.handlers = []

        # adding handlers
        self.logger.addHandler(handler_screen)
        self.logger.debug("Started logger name %s" % self.logger.name)

        # ================
        # Notification Channels
        # ================
        # Initialize dictionary of important data for notification channel inputs to
        # DataAggregator.
        # We can use this data to iterate through inputs
        self.suppliers = {
            "antenna": {
                "channel": tnrtAntennaMod.STATUS_CHANNEL_NAME,
                "dtype": tnrtAntennaMod.AntennaStatusNotifyBlock,
                "supplier": None,
            },
            "spectrum_preview": {
                "channel": DataAggregatorMod.CHANNEL_NAME_SPECTRUM_PREVIEW,
                "supplier": None,
            },
        }

        for key in self.suppliers:
            self.logger.debug("Creating Notification Supplier %s: " % key)
            try:
                self.suppliers[key]["supplier"] = Supplier(self.suppliers[key]["channel"])
            except:
                self.suppliers[key]["supplier"] = None
                self.logger.logError("Failed to initialize NC Supplier: %s" % key)

        # ================
        # PySimpleClient and Component
        # ================
        self.logger.debug("Starting PySimpleClient")
        self.sc = None
        self.sc = PySimpleClient()

        self.objref = None
        self.component_name = "DataMonitor"
        # Get reference to ComponentA.  If it is already activated by
        # ACS Command Center, this will be a reference to the same object.
        # If it is not active, This command will activate and create a new instance.
        try:
            self.logger.debug("Connecting to ACS component %s" % self.component_name)
            self.objref = self.sc.getComponent(self.component_name)
        except Exception as e:
            self.logger.error(
                "Cannot get ACS component object reference for %s" % self.component_name
            )
            self.tearDown()

        # ================
        # Some extra variables for test
        # ================
        self.SECOND_PER_DAY = 86400.0
        self.DAYS_PER_SECOND = 1.0 / self.SECOND_PER_DAY

        self.sequence_counter_backend = 0
        self.sequence_counter_antenna = 0
        self.time_tcs_mjd = Time.now().mjd
        self.time_acu_mjd = self.time_tcs_mjd + 3 * (self.DAYS_PER_SECOND)

        self.az_acceleration = 0.03
        self.az_velocity0 = -1.0
        self.az_position0 = -60.0
        self.el_acceleration = 0.02
        self.el_veloctiy0 = -1.0
        self.el_position0 = 20.0

    def tearDown(self):
        """
        This function runs 1 time per test_xxx function in this class. after test complete
        """
        for key in self.suppliers:
            self.logger.debug("Destroying Notification Channel Supplier %s: " % key)
            if self.suppliers[key]["supplier"] is not None:
                try:
                    self.suppliers[key]["supplier"].disconnect()
                except:
                    self.suppliers[key] = None

        try:
            self.logger.debug("Releasing ACS component %s" % self.component_name)
            self.sc.releaseComponent(self.component_name)
            self.objref = None
        except Exception as e:
            self.logger.error("Cannot release component reference for %s" % self.component_name)

        if self.sc is not None:
            self.sc.disconnect()
            del self.sc

    def simulate_antenna(self):
        # Calculate peak value and location
        self.time_tcs_mjd += 0.2 * self.DAYS_PER_SECOND
        self.sequence_counter_antenna += 1
        self.time_acu_mjd += 0.2 * self.DAYS_PER_SECOND
        az_velocity = self.az_velocity0 + self.az_acceleration * self.sequence_counter_antenna
        az_position = self.az_position0 + az_velocity * self.sequence_counter_antenna
        el_velocity = self.el_veloctiy0 + self.el_acceleration * self.sequence_counter_antenna
        el_position = self.el_position0 + el_velocity * self.sequence_counter_antenna

        status = AntennaDefaults.metadata
        # Modify some values for the simulation
        status.time_tcs_mjd = self.time_tcs_mjd
        status.status_message_counter = self.sequence_counter_antenna
        status.general_sts.actual_time = self.time_acu_mjd

        status.axis_status[0].actual_position = az_position
        status.axis_status[0].current_velocity = az_velocity
        status.axis_status[0].trajectory_generator_acceleration = self.az_acceleration

        status.axis_status[1].actual_position = el_position
        status.axis_status[1].current_velocity = el_velocity
        status.axis_status[1].trajectory_generator_acceleration = self.el_acceleration

        return status

    def test10_data_simulator(self):
        try:
            offaxis_arcsec = -100
            psim = DataSimulator.PointingSimulator(offaxis=offaxis_arcsec)
            result = psim.data3D
            self.logger.debug("DataSimulator data cube:{}".format(repr(result)))
            self.assertEqual(result.dtype, np.dtype("float64"))
            # Uncomment the line about preview() to see the graphs of simulated data
            # psim.preview()
        except Exception as e:
            self.fail(e)

    def test30_graph_spectrum(self):
        DELAY = 0.2
        # Simulate datacube, iterate through it and publish data to be handled by DataAggregator and pipelines.
        offaxis_arcsec = -100
        sim = DataSimulator.PointingSimulator(offaxis=offaxis_arcsec, nchan=1024, nusefeed=4)

        # First dimension of the simulation data is angle off tracking center.  Iterate through
        # all angles.
        for index_angle in range(sim.data3D.shape[0]):
            # Generate a structured even from the row in simulated data (one integrated spectrum, 1024 channel)
            # This datacube can have more than one polarization.  eddfits will keep all polarizations ("sections")
            # in the packet .  DataMonitor use only 2 of the polarizations / sections to show total power
            # sim.data3D has dimensions (N-angles, N_polarizations, N_spectrum_channels)
            # Therefore we select the first dimension [index_angle] in the loop
            # Select the second dimension hard-code  [0], [2] to select 2 sections with noise cal off
            # Last dimension is spectrum channels array.
            spectrum_lowres_dB0 = 20 * np.log10(sim.data3D[index_angle][0])
            spectrum_lowres_dB1 = 20 * np.log10(sim.data3D[index_angle][2])

            spectrum_preview_packet = DataAggregatorMod.SpectrumPreview(
                float(Time.now().mjd),
                [
                    list(spectrum_lowres_dB0.astype(float)),
                    list(spectrum_lowres_dB1.astype(float)),
                ],
            )

            # Publish the event of new data in EddFits format
            self.logger.debug(
                "publish event in spectrum_preview format ({} / {})".format(
                    index_angle, sim.data3D.shape[0]
                )
            )
            self.suppliers["spectrum_preview"]["supplier"].publish_event(spectrum_preview_packet)
            time.sleep(DELAY)

    def test40_graph_antenna(self):
        DELAY = 0.2
        LOOP_COUNT = 100
        for k in range(LOOP_COUNT):
            eventAntenna = self.simulate_antenna()
            self.logger.debug("publish event from antenna ({} / {})".format(k, LOOP_COUNT))
            self.suppliers["antenna"]["supplier"].publish_event(eventAntenna)
            time.sleep(DELAY)

    def test50_graph_holography(self):
        pass


if __name__ == "__main__":
    unittest.main()
