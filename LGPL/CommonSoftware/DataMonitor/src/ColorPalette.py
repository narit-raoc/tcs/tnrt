from PyQt5 import QtGui

dark = QtGui.QPalette()
# base
gray1 = 0
gray2 = 100
gray3 = 20
dark.setColor(QtGui.QPalette.WindowText, QtGui.QColor(255, 255, 255))
dark.setColor(QtGui.QPalette.Background, QtGui.QColor(gray2, gray2, gray2))
dark.setColor(QtGui.QPalette.Button, QtGui.QColor(gray3, gray3, gray3))
dark.setColor(QtGui.QPalette.Light, QtGui.QColor(180, 180, 180))
dark.setColor(QtGui.QPalette.Midlight, QtGui.QColor(90, 90, 90))
dark.setColor(QtGui.QPalette.Dark, QtGui.QColor(35, 35, 35))
dark.setColor(QtGui.QPalette.Mid, QtGui.QColor(35, 35, 35))
dark.setColor(QtGui.QPalette.Text, QtGui.QColor(180, 180, 180))
dark.setColor(QtGui.QPalette.BrightText, QtGui.QColor(255, 255, 255))
dark.setColor(QtGui.QPalette.ButtonText, QtGui.QColor(255, 255, 255))
dark.setColor(QtGui.QPalette.Base, QtGui.QColor(0, 0, 0))
dark.setColor(QtGui.QPalette.Window, QtGui.QColor(gray1, gray1, gray1))
dark.setColor(QtGui.QPalette.Shadow, QtGui.QColor(50, 50, 50))
dark.setColor(QtGui.QPalette.Highlight, QtGui.QColor(42, 130, 218))
dark.setColor(QtGui.QPalette.HighlightedText, QtGui.QColor(180, 180, 180))
dark.setColor(QtGui.QPalette.Link, QtGui.QColor(56, 252, 196))
dark.setColor(QtGui.QPalette.LinkVisited, QtGui.QColor(56, 252, 196))
dark.setColor(QtGui.QPalette.AlternateBase, QtGui.QColor(66, 66, 66))
dark.setColor(QtGui.QPalette.NoRole, QtGui.QColor(66, 66, 66))
dark.setColor(QtGui.QPalette.ToolTipBase, QtGui.QColor(53, 53, 53))
dark.setColor(QtGui.QPalette.ToolTipText, QtGui.QColor(180, 180, 180))

# disabled
dark.setColor(QtGui.QPalette.Disabled, QtGui.QPalette.WindowText, QtGui.QColor(127, 127, 127))
dark.setColor(QtGui.QPalette.Disabled, QtGui.QPalette.Text, QtGui.QColor(127, 127, 127))
dark.setColor(QtGui.QPalette.Disabled, QtGui.QPalette.ButtonText, QtGui.QColor(127, 127, 127))
dark.setColor(QtGui.QPalette.Disabled, QtGui.QPalette.Highlight, QtGui.QColor(80, 80, 80))
dark.setColor(QtGui.QPalette.Disabled, QtGui.QPalette.HighlightedText, QtGui.QColor(127, 127, 127))