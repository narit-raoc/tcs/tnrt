# Import standard Python modules
import sys
import numpy as np
import logging
import argparse
import threading
from astropy.time import Time

# Prepare syntax for upgrade to PySide6 and new PyQtGraph.  
# In future versions of pyqtgraph, we can use syntax such as
# from pyqtgraph.Qt.< Qt module name> import <Qt class name> to abstract out the actual version of
# PyQt / PySide in use.  PyQtGraph will select whichever version is available.  This will allow us to
# upgrade version of PyQt / PySide dependencies without changing any code to reference new version.
import pyqtgraph as pg
from PyQt5.QtCore import pyqtSignal as Signal
from PyQt5.QtWidgets import QWidget, QApplication, QMainWindow, QTabWidget

# TNRT DataMonitor modules
import logconfig
import ColorPalette

from StateMachines import GraphSpectrum
from StateMachines import GraphAntenna
from StateMachines import GraphHolography

import tnrtAntennaMod
import DataAggregatorMod
import BackendMod
import DataFormatAcu as acu
from CentralDB import CentralDB
import cdbConstants
from BackendDefaults import generate_dtype_msg_holodata

class MainWindow(QMainWindow):
    """ """

    # Define QT signals in the Class, not the instance Object __init__().
    # But refer to them later as instance objects (example self.signal_data_ready_antenna.emit())
    # This style of signals is compatible with new
    # versions of Qt4 and Qt5.  Signals must be defined with data types before use.
    # Old-style SIGNAL could be created at runtime and emit by the QObject
    # https://www.riverbankcomputing.com/static/Docs/PyQt4/new_style_signals_slots.html
    signal_stop_antenna = Signal()
    signal_stop_spectrum = Signal()
    signal_stop_holography = Signal()
    
    signal_wait_for_data_antenna = Signal()
    signal_wait_for_data_spectrum = Signal()
    signal_wait_for_data_holography = Signal()
    
    signal_data_ready_antenna = Signal()
    signal_data_ready_spectrum = Signal()
    signal_data_ready_holography = Signal()

    def __init__(self, use_mock):
        """
        Create MainWindow QtGui

        Parameters
        ----------
                simulate : {True, False}
                        True : Do not connect to ACS Notification Channel messages.  Start
                        a data simulator thread that generates pyqt signals.  GUI will
                        handle the object data passed into each pyqtsignal

                        False : Subscribe to ACS Notification Channels.  Receive data messages
                        from systems connected to ACS.  GUI will handle the object data that
                        is passed to the ACS notification channel consumer.
        """

        self.use_mock = use_mock
        self.event_stop_loop_ssim_data = threading.Event()
        self.event_stop_loop_hsim_data = threading.Event()
        self.longoff = 0
        self.latoff = 0

        # Start logger
        self.logger = logging.getLogger(self.__class__.__name__)
        logconfig.configure_logger(self.logger)

        self.logger.info("Started log for %s" % self.__class__.__name__)

        super(MainWindow, self).__init__()

        # Create widgets, add to layouts and initialize all values that
        # remain constant and do not depend on selected subscan.
        self.create_widgets()

        # Configure the number of spectrum channels to show.  If we are running from ACS environment, read
        # the number of spectrum channels from .idl file. If run standalone outside ACS environment, hard-code
        # the number of channels here.
        try:
            import DataAggregatorMod

            self.nchannels_display_spectrum = DataAggregatorMod.SPECTRUM_PREVIEW_NCHAN
            self.logger.info(
                "Read number of spectrum channels from .idl file. nchan: {}".format(
                    self.nchannels_display_spectrum
                )
            )
        except ModuleNotFoundError:
            self.nchannels_display_spectrum = 1024
            self.logger.info(
                "Cannot import DataAggregatorMod from .idl definition.  Use default spectrum channels 1024"
            )

        self.canvas = None
        self.toolbar = None

        # Create a state machine for each graph tab.  Pass parameters for location in GUI and signals
        # that the state machine uses to transition states
        self.state_machine_graph_spectrum = GraphSpectrum(
            self.tabgroup_dataview,
            self.tab_spectrum,
            self.signal_stop_spectrum,
            self.signal_wait_for_data_spectrum,
            self.signal_data_ready_spectrum,
            self.nchannels_display_spectrum,
        )

        self.state_machine_graph_spectrum.setInitialState(
            self.state_machine_graph_spectrum.state_wait_for_data
        )
        self.state_machine_graph_spectrum.start()

        self.state_machine_graph_antenna = GraphAntenna(
            self.tabgroup_dataview,
            self.tab_antenna,
            self.signal_stop_antenna,
            self.signal_wait_for_data_antenna,
            self.signal_data_ready_antenna,
        )

        self.state_machine_graph_antenna.setInitialState(
            self.state_machine_graph_antenna.state_stop
        )
        self.state_machine_graph_antenna.start()

        self.state_machine_graph_holo = GraphHolography(
            self.tabgroup_dataview,
            self.tab_holography,
            self.signal_stop_holography,
            self.signal_wait_for_data_holography,
            self.signal_data_ready_holography,
        )

        self.state_machine_graph_holo.setInitialState(
            self.state_machine_graph_holo.state_stop
        )
        self.state_machine_graph_holo.start()

        # Tabs are prepared.  Now subscribe and wait for data
        if self.use_mock is True:
            self.logger.debug(
                "Found use_mock == True.  Do not subscribe Consumers. Start local simulator"
            )
            self.start_simulator()
        else:
            self.logger.debug("Found use_mock == False. Subscribe Consumers")
            self.subscribe_consumers()

    def create_widgets(self):
        pg.setConfigOptions(antialias=True)
        self.resize(1200, 800)
        self.setWindowTitle("Data Monitor")
        
        # Create the top-level tab group
        self.tabgroup_dataview = QTabWidget()

        # Create empty container widget for each tab
        self.tab_spectrum = QWidget()
        self.tab_antenna = QWidget()
        self.tab_holography = QWidget()

        # Add tab container widgets to the tab group
        self.tabgroup_dataview.addTab(self.tab_spectrum, "EDD Spectrum")
        self.tabgroup_dataview.addTab(self.tab_antenna, "Antenna Position")
        self.tabgroup_dataview.addTab(self.tab_holography, "Holography")

        # Layout
        self.setCentralWidget(self.tabgroup_dataview)


        # No need to connect signals for changing tabs.  This is done in the StateMachines
        # class.
        

    def shutdown(self):
        # Signal to all of the graph state machines to stop
        self.signal_stop_antenna.emit()
        self.signal_stop_spectrum.emit()
        self.signal_stop_holography.emit()

        if self.use_mock is True:
            self.logger.debug("Found use_mock = TRUE.  Stop simulator")
            self.stop_simulator()
        else:
            self.logger.debug("Found use_mock = FALSE. Unsubscribe ACS Notification Channels")
            self.unsubscribe_consumers()

    def subscribe_consumers(self):
        from Consumer import Consumer

        self.consumers = {
            "antenna": {
                "channel": tnrtAntennaMod.STATUS_CHANNEL_NAME,
                "handler": self.update_section_antenna,
                "consumer": None,
            },
            "spectrum_preview": {
                "channel": DataAggregatorMod.CHANNEL_NAME_SPECTRUM_PREVIEW,
                "handler": self.update_section_spectrum,
                "consumer": None,
            },
            "holodata" : {
                "channel": BackendMod.CHANNEL_NAME_HOLO_DATA,
                "handler": self.update_section_holography,
                "consumer": None,
            }
        }

        for key in self.consumers:
            self.logger.info("Creating Notification Channel Consumer %s: " % key)
            try:
                self.consumers[key]["consumer"] = Consumer(self.consumers[key]["channel"])
                self.consumers[key]["consumer"].add_subscription(self.consumers[key]["handler"])
            except:
                self.consumers[key]["consumer"] = None
                self.logger.error("Failed to initialize Consumer: %s " % key)
                raise

        self.cdb = CentralDB()

    def unsubscribe_consumers(self):
        for key in self.consumers:
            self.logger.debug("disconnect Consumer at key: %s" % key)
            try:
                self.consumers[key]["consumer"].disconnect()
            except Exception as e:
                self.consumers[key]["consumer"] = None
                self.logger.error(
                    "Catch exception while disconnecting Consumer {}: {}".format(key, e)
                )

    def update_section_antenna(self, dataStruct):
        # self.logger.debug("update section antenna with: {}".format(type(dataStruct)))
        # Translate IDL struct to numpy array
        data_ndarray = np.array(
            [
                [
                    dataStruct.axis_status[0].trajectory_generator_acceleration,
                    dataStruct.axis_status[0].current_velocity,
                    dataStruct.axis_status[0].actual_position,
                ],
                [
                    dataStruct.axis_status[1].trajectory_generator_acceleration,
                    dataStruct.axis_status[1].current_velocity,
                    dataStruct.axis_status[1].actual_position,
                ],
            ]
        )

        # Calculate LATOFF and LONGOFF from the current ACU status message
        actual_position_az = dataStruct.axis_status[0].actual_position # 0 is AZIMUTH axis data
        actual_position_el = dataStruct.axis_status[1].actual_position # 1 is ELEVATION axis data

        # Find tracking center coordinate from selected tracking algorithm in dataStruct.tracking_sts.bit_status
        # The bit status exists for every ACU status message in the array.  
        # Assume it does not change during this subscan. So, we can use the first value at index [0] 
        if (dataStruct.tracking_sts.bit_status & acu.TR_SUN_TRACK_ACTIVE) == acu.TR_SUN_TRACK_ACTIVE:
            #self.logger.logDebug("Found selected tracking algorithm: TR_SUN_TRACK_ACTIVE")
            selected_tracking_algorithm_position_az = dataStruct.tracking_sts.sun_track_az
            selected_tracking_algorithm_position_el = dataStruct.tracking_sts.sun_track_el

        elif (dataStruct.tracking_sts.bit_status & acu.TR_PROG_TRACK_ACTIVE) == acu.TR_PROG_TRACK_ACTIVE:
            #self.logger.logDebug("Found selected tracking algorithm: TR_PROG_TRACK_ACTIVE")
            selected_tracking_algorithm_position_az = dataStruct.tracking_sts.prog_track_az
            selected_tracking_algorithm_position_el = dataStruct.tracking_sts.prog_track_el
        
        elif (dataStruct.tracking_sts.bit_status & acu.TR_TLE_TRACK_ACTIVE) == acu.TR_TLE_TRACK_ACTIVE:
            #self.logger.logDebug("Found selected tracking algorithm: TR_TLE_TRACK_ACTIVE")
            selected_tracking_algorithm_position_az = dataStruct.tracking_sts.tle_track_az
            selected_tracking_algorithm_position_el = dataStruct.tracking_sts.tle_track_el

        elif (dataStruct.tracking_sts.bit_status & acu.TR_STAR_TRACK_ACTIVE) == acu.TR_STAR_TRACK_ACTIVE:
            #self.logger.logDebug("Found selected tracking algorithm: TR_STAR_TRACK_ACTIVE")
            selected_tracking_algorithm_position_az = dataStruct.tracking_sts.star_track_az
            selected_tracking_algorithm_position_el = dataStruct.tracking_sts.star_track_el
        
        else:
            #self.logger.warning("Cannot identify selected tracking algorithm.")
            #self.logger.warning("Set tracking algorithm position = (0,0). Set actual_position = program_offset")
            selected_tracking_algorithm_position_az = 0
            selected_tracking_algorithm_position_el = 0
            actual_position_az = dataStruct.tracking_sts.prog_offset_az
            actual_position_el = dataStruct.tracking_sts.prog_offset_el

        self.longoff = actual_position_az - selected_tracking_algorithm_position_az
        self.latoff = actual_position_el - selected_tracking_algorithm_position_el

        self.state_machine_graph_antenna.set_data(dataStruct.general_sts.actual_time, data_ndarray)

        # Signal to the state machine that data is available for graph
        self.signal_data_ready_antenna.emit()

    def update_section_spectrum(self, dataStruct):
        # self.logger.info("update section spectrum with data: {}".format(type(dataStruct)))
        # Translate the IDL struct to numpy array

        # The array that we receive in IDL struct has 2 sections ( polarizations)
        # So, the array size should be 2 * nchannels
        data_ndarray = np.asarray(dataStruct.spectrum_preview_data)
        self.state_machine_graph_spectrum.set_data(dataStruct.time_mjd, data_ndarray)

        # Signal to the state machine that data is available for graph
        self.signal_data_ready_spectrum.emit()

    def update_section_holography(self, msg_holodata):
        # use the data message from holo backend and add extra metadata from ACU position (LONGOFF, LATOFF)
        # and scan sequence state (SUBSTYPE) 
        self.state_machine_graph_holo.set_data(
                    msg_holodata,
                    self.longoff,
                    self.latoff,
                    self.cdb.get(cdbConstants.SUBSTYPE))
        
        # Signal to the state machine that data is available for graph
        self.signal_data_ready_holography.emit()

    # --------------------------------------
    # Simulator
    # --------------------------------------
    def start_simulator(self):
        # Start a separate thread that has an infiniute loop through the simulated data.  It will stop
        # loop when we close the GUI window and catch the threading.Event state to end the loop.
        # Set the flow control even to clear (not stopped)
        self.logger.debug("Generate ssim simulation data array")
        self.ssim = SpectrumSimulator(nchan=self.nchannels_display_spectrum, nusefeed=2)
    
        self.event_stop_loop_ssim_data.clear()   
        self.logger.debug("Start spectrum simulation loop forever")
        thread_ssim_dataloop = threading.Thread(target=self.loop_ssim_data)
        thread_ssim_dataloop.start()

        self.event_stop_loop_hsim_data.clear()
        self.logger.debug("Generate ssim simulation data array")
        npoints = 12
        angular_resolution = 80
        time_per_angular_step = 0.5
        subscans_per_cal = 1
        integrations_per_calN = 10
        integrations_per_cal0 = 10
        peak_magntidue_ref = 8.5e-6  # mean value of first calibration in Yebes file TEST5772.FITS
        peak_magntidue_tst = 5.8e-5  # mean value of first calibration in Yebes file TEST5772.FITS
        fwhm = 160

        self.hsim = HolographySimulator(
            npoints,
            angular_resolution,
            time_per_angular_step,
            subscans_per_cal,
            integrations_per_calN,
            integrations_per_cal0,
            peak_magntidue_ref,
            peak_magntidue_tst,
            fwhm,
        )
        
        self.logger.debug("Start holo simulation loop forever")
        thread_hsim_dataloop = threading.Thread(target=self.loop_hsim_data)
        thread_hsim_dataloop.start()


    def stop_simulator(self):
        self.logger.debug("stop simulation loop")
        self.event_stop_loop_ssim_data.set()
        self.event_stop_loop_hsim_data.set()

    def loop_ssim_data(self):
        """
        Infinite loop to traverse through the array of simulated data and make available for animation
        """
        data_packet_update_period_sec = 0.1
        data_packet_update_period_days = data_packet_update_period_sec / 86400.0

        integration_index = (
            0  # Start from integration index 0 (the first packet of data in the time series)
        )

        self.logger.debug(
            "Start thread to publish simulation data every {:.2f} seconds and repeat until stopped".format(
                data_packet_update_period_sec
            )
        )

        self.time_now_mjd = 0

        # Infinite loop with timeout  data_packet_update_period_sec until user triggers the signal 
        # event_stop_loop_ssim_data to exit the loop immediately
        while not self.event_stop_loop_ssim_data.wait(timeout=data_packet_update_period_sec):

            # data3D has 3 dimensions (N_integrations, N_polarizations, N_spectrum_channels).
            # GraphSpectrum.set_data expects an array that has 2 dimensions (N_polarizations, N_spectrum_channels)
            # So, we select the current integration index and pass the remaining 2D array to GraphSpectrum
            current_spectrum_dB = 20 * np.log10(self.ssim.data3D[integration_index])
            self.state_machine_graph_spectrum.set_data(self.time_now_mjd, current_spectrum_dB)

            # Signal to the state machine that data is available for graph
            self.signal_data_ready_spectrum.emit()

            current_antenna_data = self.ssim.data_ant[integration_index]
            self.state_machine_graph_antenna.set_data(self.time_now_mjd, current_antenna_data)
            self.signal_data_ready_antenna.emit()

            # Increase the integration index to read.  After we read the last integration, I want to repeat forever
            # Therefore use the '%" modulo operator to bring the index back into the available range'
            integration_index = (integration_index + 1) % self.ssim.data3D.shape[0]
            self.time_now_mjd = self.time_now_mjd + data_packet_update_period_days


    def loop_hsim_data(self):
        """
        Infinite loop to traverse through the array of simulated data and make available for animation
        """
        
        fft_length = 128
        # create msg_holodate numpy array.  Use np.squeeze to reduce arrays lengh=1 to be scalar values
        dtype_msg_holodata = generate_dtype_msg_holodata(fft_length)
        msg_holodata = np.squeeze(np.zeros(1, dtype=dtype_msg_holodata))

        sample_rate = 51.2e3 # default sample rate of Keysight FFT analyzer before decimation (DDC)        
        freq_ref = 0
        freq_tst = 0
        freq_ddc = 20e3
        # A series of time points to generate all signals for spectrum simulation
        tt = np.arange(0, fft_length) / sample_rate
        
        msg_holodata["sample_capture_duration"] = self.hsim.time_per_angular_step    
        msg_holodata["spectrum_freq_axis"] = np.linspace(-1 * sample_rate / 2, sample_rate/ 2 , fft_length, endpoint=False) + freq_ddc
        msg_holodata["timeseries_time_axis"] = tt
        win = np.hamming(fft_length)

        self.logger.debug(
            "Start thread to publish hsim data every {:.2f} seconds and repeat until stopped".format(
                msg_holodata["sample_capture_duration"]
            )
        )

        # Start with current time.  Overwrite with gen timestamps in the loop.
        msg_holodata["time_mjd"] = Time.now().mjd
        
        noise_magnitude_ref = 1e-7 # I don't know the real system values.  make something convenient for sim
        noise_magnitude_tst = 1e-9

        while not self.event_stop_loop_hsim_data.is_set():
            for k in range(self.hsim.integrations_per_cal0):
                msg_holodata["time_mjd"] = msg_holodata["time_mjd"] + (msg_holodata["sample_capture_duration"] / 86400)
                
                ref_magnitude = np.abs(self.hsim.center_cal0["ref_antenna_signal_watts"][0][k])
                tst_magnitude = np.abs(self.hsim.center_cal0["tst_antenna_signal_watts"][0][k])
                ref_phase = np.angle(self.hsim.center_cal0["ref_antenna_signal_watts"][0][k])
                tst_phase = np.angle(self.hsim.center_cal0["tst_antenna_signal_watts"][0][k])
                sig_ref = ref_magnitude * np.exp(1j * (2 * np.pi * freq_ref * tt + ref_phase))
                sig_tst = tst_magnitude * np.exp(1j * (2 * np.pi * freq_tst * tt + tst_phase))
                
                # Simulate noise floor and add to the pure signal
                noise_ref = noise_magnitude_ref * (np.random.normal(size=tt.shape[0]) + 1j * np.random.normal(size=tt.shape[0]))
                noise_tst = noise_magnitude_tst * (np.random.normal(size=tt.shape[0]) + 1j * np.random.normal(size=tt.shape[0]))
                sn_ref = sig_ref + noise_ref
                sn_tst = sig_tst + noise_tst

                # Save time-series phase data for visualization of frequency tracking
                msg_holodata["timeseries_phase_ch1_rad"] = np.angle(sn_ref)
                msg_holodata["timeseries_phase_ch2_rad"] = np.angle(sn_tst)

                # Calculate FFT spectrum
                spectrum_ref_complex = np.fft.fftshift(np.fft.fft(sn_ref * win)) / fft_length
                spectrum_tst_complex = np.fft.fftshift(np.fft.fft(sn_tst * win)) / fft_length
                spectrum_rel_complex = spectrum_tst_complex / spectrum_ref_complex
                msg_holodata["spectrum_absolute_magnitude_ch1_dbm"] = 30 + 10 * np.log10(np.abs(spectrum_ref_complex))
                msg_holodata["spectrum_absolute_magnitude_ch2_dbm"] = 30 + 10 * np.log10(np.abs(spectrum_tst_complex))
                msg_holodata["spectrum_relative_magnitude_db"] = 10*np.log10(np.abs(spectrum_rel_complex))
                msg_holodata["spectrum_relative_phase_deg"] = np.degrees(np.angle(spectrum_rel_complex))
                
                marker_index = np.argmax(msg_holodata["spectrum_absolute_magnitude_ch1_dbm"])
                msg_holodata["marker_index"] = marker_index
                msg_holodata["marker_val_freq_hz"] = msg_holodata["spectrum_freq_axis"][marker_index]
                msg_holodata["marker_val_absolute_magnitude_ch1_dbm"] = msg_holodata["spectrum_absolute_magnitude_ch1_dbm"][marker_index]
                msg_holodata["marker_val_absolute_magnitude_ch2_dbm"] = msg_holodata["spectrum_absolute_magnitude_ch2_dbm"][marker_index]
                msg_holodata["marker_val_relative_magnitude_db"] = msg_holodata["spectrum_relative_magnitude_db"][marker_index]
                msg_holodata["marker_val_phase_ch1_deg"] = np.degrees(np.angle(spectrum_ref_complex))[marker_index]
                msg_holodata["marker_val_phase_ch2_deg"] = np.degrees(np.angle(spectrum_tst_complex))[marker_index]
                msg_holodata["marker_val_relative_phase_deg"] = msg_holodata["spectrum_relative_phase_deg"][marker_index]

                self.state_machine_graph_holo.set_data(
                    msg_holodata,
                    0,
                    0,
                    "CORR")
                self.signal_data_ready_holography.emit()
                self.event_stop_loop_hsim_data.wait(timeout=float(msg_holodata["sample_capture_duration"]))

            for otf_map_row in self.hsim.otf_map:
                for k in range(self.hsim.npoints_per_otf_line):
                    msg_holodata["time_mjd"] = msg_holodata["time_mjd"] + (msg_holodata["sample_capture_duration"] / 86400)
                    
                    ref_magnitude = np.abs(otf_map_row["ref_antenna_signal_watts"][k])
                    tst_magnitude = np.abs(otf_map_row["tst_antenna_signal_watts"][k])
                    ref_phase = np.angle(otf_map_row["ref_antenna_signal_watts"][k])
                    tst_phase = np.angle(otf_map_row["tst_antenna_signal_watts"][k])
                    
                    sig_ref = ref_magnitude * np.exp(1j * (2 * np.pi * freq_ref * tt + ref_phase))
                    sig_tst = tst_magnitude * np.exp(1j * (2 * np.pi * freq_tst * tt + tst_phase))

                    # Simulate noise floor and add to the pure signal
                    noise_ref = noise_magnitude_ref * (np.random.normal(size=tt.shape[0]) + 1j * np.random.normal(size=tt.shape[0]))
                    noise_tst = noise_magnitude_tst * (np.random.normal(size=tt.shape[0]) + 1j * np.random.normal(size=tt.shape[0]))
                    sn_ref = sig_ref + noise_ref
                    sn_tst = sig_tst + noise_tst

                    # Save time-series phase data for visualization of frequency tracking
                    msg_holodata["timeseries_phase_ch1_rad"] = np.angle(sn_ref)
                    msg_holodata["timeseries_phase_ch2_rad"] = np.angle(sn_tst)
                    
                    # Calculate FFT spectrum
                    spectrum_ref_complex = np.fft.fftshift(np.fft.fft(sn_ref * win)) / fft_length
                    spectrum_tst_complex = np.fft.fftshift(np.fft.fft(sn_tst * win)) / fft_length
                    spectrum_rel_complex = spectrum_tst_complex / spectrum_ref_complex
                    
                    msg_holodata["spectrum_absolute_magnitude_ch1_dbm"] = 30 + 10 * np.log10(np.abs(spectrum_ref_complex))
                    msg_holodata["spectrum_absolute_magnitude_ch2_dbm"] = 30 + 10 * np.log10(np.abs(spectrum_tst_complex))
                    msg_holodata["spectrum_relative_magnitude_db"] = 10*np.log10(np.abs(spectrum_rel_complex))
                    msg_holodata["spectrum_relative_phase_deg"] = np.degrees(np.angle(spectrum_rel_complex))

                    marker_index = np.argmax(msg_holodata["spectrum_absolute_magnitude_ch1_dbm"])
                    msg_holodata["marker_index"] = marker_index
                    msg_holodata["marker_val_freq_hz"] = msg_holodata["spectrum_freq_axis"][marker_index]
                    msg_holodata["marker_val_absolute_magnitude_ch1_dbm"] = msg_holodata["spectrum_absolute_magnitude_ch1_dbm"][marker_index]
                    msg_holodata["marker_val_absolute_magnitude_ch2_dbm"] = msg_holodata["spectrum_absolute_magnitude_ch2_dbm"][marker_index]
                    msg_holodata["marker_val_relative_magnitude_db"] = msg_holodata["spectrum_relative_magnitude_db"][marker_index]
                    msg_holodata["marker_val_phase_ch1_deg"] = np.degrees(np.angle(spectrum_ref_complex))[marker_index]
                    msg_holodata["marker_val_phase_ch2_deg"] = np.degrees(np.angle(spectrum_tst_complex))[marker_index]
                    msg_holodata["marker_val_relative_phase_deg"] = msg_holodata["spectrum_relative_phase_deg"][marker_index]

                    self.state_machine_graph_holo.set_data(
                        msg_holodata,
                        otf_map_row["x"][k],
                        otf_map_row["y"][k],
                        "HOLO")
                    
                    self.signal_data_ready_holography.emit()
                    self.event_stop_loop_hsim_data.wait(timeout=float(msg_holodata["sample_capture_duration"]))

                for k in range(self.hsim.integrations_per_calN):
                    msg_holodata["time_mjd"] = msg_holodata["time_mjd"] + (msg_holodata["sample_capture_duration"] / 86400)
                    
                    ref_magnitude = np.abs(self.hsim.center_calN["ref_antenna_signal_watts"][0][k])
                    tst_magnitude = np.abs(self.hsim.center_calN["tst_antenna_signal_watts"][0][k])
                    ref_phase = np.angle(self.hsim.center_calN["ref_antenna_signal_watts"][0][k])
                    tst_phase = np.angle(self.hsim.center_calN["tst_antenna_signal_watts"][0][k])

                    sig_ref = ref_magnitude * np.exp(1j * (2 * np.pi * freq_ref * tt + ref_phase))
                    sig_tst = tst_magnitude * np.exp(1j * (2 * np.pi * freq_tst * tt + tst_phase))

                    # Simulate noise floor and add to the pure signal
                    noise_ref = noise_magnitude_ref * (np.random.normal(size=tt.shape[0]) + 1j * np.random.normal(size=tt.shape[0]))
                    noise_tst = noise_magnitude_tst * (np.random.normal(size=tt.shape[0]) + 1j * np.random.normal(size=tt.shape[0]))
                    sn_ref = sig_ref + noise_ref
                    sn_tst = sig_tst + noise_tst

                    # Save time-series phase data for visualization of frequency tracking
                    msg_holodata["timeseries_phase_ch1_rad"] = np.angle(sn_ref)
                    msg_holodata["timeseries_phase_ch2_rad"] = np.angle(sn_tst)

                    # Calculate FFT spectrum
                    spectrum_ref_complex = np.fft.fftshift(np.fft.fft(sn_ref * win)) / fft_length
                    spectrum_tst_complex = np.fft.fftshift(np.fft.fft(sn_tst * win)) / fft_length
                    spectrum_rel_complex = spectrum_tst_complex / spectrum_ref_complex
                    
                    msg_holodata["spectrum_absolute_magnitude_ch1_dbm"] = 30 + 10 * np.log10(np.abs(spectrum_ref_complex))
                    msg_holodata["spectrum_absolute_magnitude_ch2_dbm"] = 30 + 10 * np.log10(np.abs(spectrum_tst_complex))
                    msg_holodata["spectrum_relative_magnitude_db"] = 10*np.log10(np.abs(spectrum_rel_complex))
                    msg_holodata["spectrum_relative_phase_deg"] = np.degrees(np.angle(spectrum_rel_complex))
            
                    marker_index = np.argmax(msg_holodata["spectrum_absolute_magnitude_ch1_dbm"])
                    msg_holodata["marker_index"] = marker_index
                    msg_holodata["marker_val_freq_hz"] = msg_holodata["spectrum_freq_axis"][marker_index]
                    msg_holodata["marker_val_absolute_magnitude_ch1_dbm"] = msg_holodata["spectrum_absolute_magnitude_ch1_dbm"][marker_index]
                    msg_holodata["marker_val_absolute_magnitude_ch2_dbm"] = msg_holodata["spectrum_absolute_magnitude_ch2_dbm"][marker_index]
                    msg_holodata["marker_val_relative_magnitude_db"] = msg_holodata["spectrum_relative_magnitude_db"][marker_index]
                    msg_holodata["marker_val_phase_ch1_deg"] = np.degrees(np.angle(spectrum_ref_complex))[marker_index]
                    msg_holodata["marker_val_phase_ch2_deg"] = np.degrees(np.angle(spectrum_tst_complex))[marker_index]
                    msg_holodata["marker_val_relative_phase_deg"] = msg_holodata["spectrum_relative_phase_deg"][marker_index]

                    self.state_machine_graph_holo.set_data(
                        msg_holodata,
                        0,
                        0,
                        "CORR")
                    
                    self.signal_data_ready_holography.emit()
                    self.event_stop_loop_hsim_data.wait(timeout=float(msg_holodata["sample_capture_duration"]))

class SpectrumSimulator:
    def __init__(
        self,
        subscan_duration=60.0,
        integration_time=1.0,
        amplitude=1,
        fwhm=160,
        offaxis=0,
        fs=800e6,
        nchan=1024,
        nusefeed=1,
        az_a=0.03,
        az_v_start=0,
        az_p_start=-70,
        el_a=0.02,
        el_v_start=-1,
        el_p_start=10,
    ):
        # Scan pattern time
        self.subscan_duration = subscan_duration

        # After each integration_time, the receiver backend publishes a packet of data.
        self.integration_time = integration_time

        # Numbe of polarizations / sections / noise diode ON | OFF states
        self.nusefeed = nusefeed
        self.nsections = nusefeed

        # Gaussian beam shape of antenna at selected frequency and simulate offset [arcsec]
        self.signal_amplitude = amplitude
        self.fwhm = fwhm
        self.offaxis = offaxis  # [arcsec]

        # Number of position data points to generate per subscan.
        self.npoin = int(subscan_duration / integration_time)

        # Offset to scan 10 * FWHM
        self.lim = (-10 * fwhm / 2, 10 * fwhm / 2)

        # List of angles go generate simulation data
        self.angles = np.linspace(self.lim[0], self.lim[1], self.npoin)

        # Spectrum parameters to simulate rf signal data
        self.fs = fs
        self.nchan = nchan
        self.freq = 0 * fs / nchan

        # -----------------------------------------------------------------------------
        # Simulate a spectrum that has 1 signal
        # -----------------------------------------------------------------------------
        # Time axis
        tt = np.arange(0, self.nchan) / self.fs

        # Generate a complex-valued cosine signal (spectral line)
        sig_tt = self.signal_amplitude * np.exp(1j * (2 * np.pi * self.freq * tt))

        # Calculate FFT Spectcrum of signal and noise separately
        spectrum_signal_complex = np.fft.fft(sig_tt * np.hamming(self.nchan)) / self.nchan
        spectrum_signal_power = np.abs(np.fft.fftshift(spectrum_signal_complex))

        # Use equation 3 from Nikom's report on telescope performance evaluation
        # Axes of this cube are [peak_angle][index_spectral_channel]
        gaussian = self.signal_amplitude * np.exp(
            (-4 * np.log(2) * (self.angles - self.offaxis) ** 2) / (self.fwhm**2)
        )
        self.data2D_signal = np.outer(gaussian, spectrum_signal_power)

        # Generate noise.  We cannot simply use outerproduct.  Must generate a new set of
        # noise data for each angle / integration.
        # Amplitude of noise to add (set signal to noise ratio)
        noise_amplitude = self.signal_amplitude * 1e-3

        self.data2D_noise = np.zeros(self.data2D_signal.shape)
        for row in np.arange(self.data2D_noise.shape[0]):
            noise_tt = noise_amplitude * (
                np.random.normal(size=tt.shape[0]) + 1j * np.random.normal(size=tt.shape[0])
            )
            spectrum_noise_complex = np.fft.fft(noise_tt * np.hamming(self.nchan)) / self.nchan
            spectrum_noise_power = np.abs(np.fft.fftshift(spectrum_noise_complex))
            self.data2D_noise[row] = spectrum_noise_power

        self.data2D = self.data2D_signal + self.data2D_noise

        # Multiply for a scale factor to compensate for the losses introduced by the hamming window
        # in the FFT.
        amplitude_compensate = 1 / np.max(self.data2D)

        self.data2D = self.data2D * amplitude_compensate

        self.peak_angle = np.argmax(gaussian)
        self.peak_channel = np.argmax(spectrum_signal_power)

        # Create a new array that has 1 more dimensions for section (eddfits) / NUSEFEED (MBfits) / polarization
        # NOTE: the dimensions selected here are convenient to fill the array with data.
        # swap axes after data is full to be consistent with MBfits dimensions
        data3D_temp = np.zeros((self.nsections, self.npoin, self.nchan))

        # Fill the 3D array with copies of the 2D generated data
        for section_index in range(data3D_temp.shape[0]):
            # Divide the data by section index so the simulation data has different magnitude
            # in each section.  This will be helpful for debugging.
            # Section 0 will be divided by 1 -- so no change.
            data3D_temp[section_index] = self.data2D / (section_index + 1)

        # Swap the axes to make the dimensions consistent with MBfits
        self.data3D = data3D_temp.swapaxes(0, 1)

        # Generate some simulation data for Antenna AZ, EL
        tt = np.arange(0, subscan_duration, integration_time)

        az_velocity = az_v_start + az_a * tt
        az_position = az_p_start + az_velocity * tt
        el_velocity = el_v_start + el_a * tt
        el_position = el_p_start + el_velocity * tt

        self.data_az = np.array([np.full(len(tt), az_a), az_velocity, az_position]).T
        self.data_el = np.array([np.full(len(tt), el_a), el_velocity, el_position]).T

        self.data_temp = np.array([self.data_az, self.data_el])
        # Swap axes to make time step the first dimension.  easier to use in simulator loop
        self.data_ant = self.data_temp.swapaxes(0, 1)

class HolographySimulator:
    # Define a numpy data type to replace the old idl data type from Backend.idl that is currently
    # used by the PipelineALMATIfits.py.

    def __init__(
        self,
        npoints=128,
        angular_resolution=20,
        time_per_angular_step=0.5,
        subscans_per_cal=1,
        integrations_per_calN=10,
        integrations_per_cal0=120,
        peak_magnitude_ref=8.5e-6,
        peak_magnitude_tst=5.8e-5,
        fwhm=160,
        offaxis_x=0,
        offaxis_y=0,
    ):
        """
        Generate numpy array of data points to simulate results of holography scan pattern using gaussian beam.
        This data can be written to the HOLODATA and DATAPAR binary tables of ALMATI-FITS file.

        Parameters
        ----------
        npoints : float, default = 128
                number of points in both horizontal and vertical directions to simulate.
                result array will have dimension npoints x npoints

        angular_resolution : float. [unit = arcsec], default = 10
                Angular sample rate between points in the array.  In the real system, this must be syncrhonized
                with the integration time of the receiver such that a packet of data arrives for each angular step.

        time_per_angular_step : float [unit = sec], default = 0.25
                Antenna will sweep smoothly on scan lines.  This is the desired transit time between data points.
                It determines the veloctiy of the antenna.
                IMPORTANT: It should be the same as the receiver integration time to align the receiver signal data
                to the map grid mechanical movement of the ACU control.  Post processing / data reduction interpolates
                the data onto the grid from precise timestamps, but TCS should align as close as possible during
                data acquisition to create the best results.

        subscans_per_cal : int, default = 1
                Number of SubscanLines to run before return to center point and measure on-source for calibration

        integrations_per_calN : int, default = 1
                Number of integrations on-source at center point where each integration time is the same as
                time_per_angular_step.  This parameter is given as an integer rather than duration in seconds
                so the operator chooses a the calibration duration that is a ratio of a single sample of
                antenna pattern sweep data.  For example, integration time 0.5 seconds at each scan measurement point.
                integrations_per_calN = 10 means the on-source calibration will receive 10 integrations of calibration = 5 seconds.

        integrations_per_cal0 : int, default = 1
                Number of integrations on-source at center point BEFORE the the OTF map pattern starts.
                This calibration is typically longer than the other calibrations measurements between each OTF map line
                described by `integrations_per_cal`
                Each integration duration is the same as time_per_angular_step.
                This parameter is given as an integer rather than duration in seconds
                so the operator chooses a the calibration duration that is a ratio of a single sample of
                antenna pattern sweep data.  For example, integration time 0.5 seconds at each OTF scan measurement point.
                integrations_per_cal0 = 120 means the on-source calibration will receive 120 integrations of calibration = 60 seconds.

        peak_magnitude_ref : float, default = 1
                constant magnitude of reference antenna signal

        peak_magnitude_ref : float, default = 1
                Magnitude of the gaussian beam at the center using Ku band feed and 40m reflector

        fwhm : float, [unit = arcsec], default = 160
                Full-width at half max.  The 3dB beamwidth of magnitude_otf_map in arcseconds.

        offaxis_x : float [unit = arcsec]
                Offset x (longitude coordinate AZ / RA) from center of the data array to center the Gaussian beam

        offaxis_y : float [unit = arcsec]
                Offset y (latitude coordinate EL / DEC) from center of the data array to center the Gaussian beam
        """
        ARCSEC_PER_DEG = 3600
        
        # Gaussian beam shape and offset [arcsec]
        self.npoints_per_otf_line = int(npoints)
        self.angular_resolution = angular_resolution
        self.time_per_angular_step = time_per_angular_step
        self.subscans_per_cal = int(subscans_per_cal)
        self.integrations_per_calN = int(integrations_per_calN)
        self.integrations_per_cal0 = int(integrations_per_cal0)
        self.peak_magnitude_ref = peak_magnitude_ref
        self.peak_magnitude_tst = peak_magnitude_tst
        self.fwhm = fwhm
        self.offaxis_x = offaxis_x  # [arcsec]
        self.offaxis_y = offaxis_y  # [arcsec]

        # x and y lim
        # its are the same.  Use one variable for both axes
        self.lim_deg = (
            -0.5 * float(self.npoints_per_otf_line - 1) * self.angular_resolution / ARCSEC_PER_DEG,
            0.5 * float(self.npoints_per_otf_line - 1) * self.angular_resolution / ARCSEC_PER_DEG,
        )

        self.xx = np.linspace(self.lim_deg[0], self.lim_deg[1], self.npoints_per_otf_line)
        self.yy = np.linspace(self.lim_deg[0], self.lim_deg[1], self.npoints_per_otf_line)

        # Define a numpy data type to keep x,y,radius coordinates and complex-valued gaussian beam
        # simulated data together numpy structured array
        # This makes easy work to create the array with vectorized / broadcast numpy functions
        dtype_holodata_2D = np.dtype(
            [
                ("x", np.float64),
                ("y", np.float64),
                ("r", np.float64),
                ("ref_antenna_signal_watts", np.cdouble),  # complex-value
                ("tst_antenna_signal_watts", np.cdouble),
            ]
        )  # complex-value


        # Initialize arrays of zeros using coordinate data type and dimensions of xx and yy generated above
        # Assume the primary axis is x. (step through all "columns" x, then move to the next "row" y)
        # keep this as an instance variable to help debugging.  Real operation will actually use the list
        # of subscans that is generated from the final array.
        self.center_cal0 = np.zeros((1, self.integrations_per_cal0), dtype=dtype_holodata_2D)
        self.center_calN = np.zeros((1, self.integrations_per_calN), dtype=dtype_holodata_2D)
        self.otf_map = np.zeros((self.yy.size, self.xx.size), dtype=dtype_holodata_2D)

        # Generate a meshgrid of coordinates on which to calculate the antenna beam shape
        self.otf_map["x"], self.otf_map["y"] = np.meshgrid(self.xx, self.yy)
        # no need to generate coordinates for center_cal0 or center_calN because ideal case stays at (0,0)

        # Set the data for the reference antenna to be an ideal isotropic antenna. magnitude=1, phase=0
        ref_initial_phase = np.pi/4
        self.center_cal0["ref_antenna_signal_watts"] = self.peak_magnitude_ref * np.exp(1j * ref_initial_phase)
        self.center_calN["ref_antenna_signal_watts"] = self.peak_magnitude_ref * np.exp(1j * ref_initial_phase)
        self.otf_map["ref_antenna_signal_watts"] = self.peak_magnitude_ref * np.exp(1j * ref_initial_phase)

        # From Yebes example file, amplitude difference is 6, phase difference is -~-90 degrees.
        # Fill all of the calibration data with similar values.
        tst_initial_phase = ref_initial_phase - np.pi / 2
        self.center_cal0["tst_antenna_signal_watts"] = self.peak_magnitude_tst * np.exp(1j * tst_initial_phase)
        self.center_calN["tst_antenna_signal_watts"] = self.peak_magnitude_tst * np.exp(1j * tst_initial_phase)

        # Calculate radius to each point in the grid so we can use cylindrical coordinates for
        # Gaussian equation.  Don't need to create a theta angle coordinate because the Gaussian
        # pattern is symmetric around all angles theta
        self.otf_map["r"] = np.sqrt((self.otf_map["x"]) ** 2 + (self.otf_map["y"]) ** 2)

        # Generate a complex-valued gaussian beam cross-section and use this for the far_field_antenna_pattern
        # that we will measure with the real receiver.
        # Reference wikipedia
        beam_waist_arcsec = self.fwhm / (np.sqrt(2 * np.log(2)))
        radius_of_curvature = 1e-2

        beam_waist = beam_waist_arcsec / ARCSEC_PER_DEG

        # Generate magnitude profile
        # NOTE: add the smallest 32-bit fractional number to prevent truncation or divide by zero warnings
        # when we use this 64-bit simulated data with the 32-bit floats in ALMATI-FITS data format.
        magnitude_profile = np.exp(-1 * (self.otf_map["r"] ** 2) / (beam_waist**2))

        # This phase term is not correct, but it creates phase with a few wraps around 2*pi and a circular
        # pattern  Godd enough to confirm if the data file format has bugs.
        phase_profile = np.exp(-1j * (self.otf_map["r"] ** 2) / (2 * radius_of_curvature))

        # Combine the magnitude and phase profiles to make a complex-valued antenna pattern
        self.otf_map["tst_antenna_signal_watts"] = (
            self.peak_magnitude_tst * magnitude_profile * phase_profile
        )
        
if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Open DataMonitorGUI and receive asynchronous data messages from data source"
    )
    parser.add_argument(
        "-s",
        "--simulate",
        action="store_true",
        help="Receive simulated data using pyqtsignal locally.  Do not subscribe to ACS Notification Channel messages",
    )
    # Read command line arguments into argparse structure
    args = parser.parse_args()

    # Create instance of Qt GUI Application
    app = QApplication([])

    # Set colors
    app.setPalette(ColorPalette.dark)

    # Create instance of MainWindow.  The class MainWindow inherits QtGui objects
    # from Qt.QMainWindow and from DataMonitorGUI.  It extends the parent classes by
    # adding non-GUI member variables and functions to update GUI.
    window = MainWindow(args.simulate)

    # Enable the window to show after Qt event loop starts
    window.show()

    # Start Qt Event loop (including open GUI window)
    app.exec_()

    # After GUI window is closed, clean up.
    window.shutdown()

    # End Python process
    sys.exit(0)
