# Project: TNRT - Thai National Radio Telescope
# Organization: NARIT - National Astronomical Research Institute of Thailand
# Author: Spiro Sarris

import logging
import numpy as np
from astropy.time import Time

# Prepare syntax for upgrade to PySide6 and new PyQtGraph.  
# In future versions of pyqtgraph, we can use syntax such as
# from pyqtgraph.Qt.< Qt module name> import <Qt class name> to abstract out the actual version of
# PyQt / PySide in use.  PyQtGraph will select whichever version is available.  This will allow us to
# upgrade version of PyQt / PySide dependencies without changing any code to reference new version.
import pyqtgraph as pg
from PyQt5.QtCore import QStateMachine, QState, QMutex, QTimer, Qt
from PyQt5.QtWidgets import QHBoxLayout, QVBoxLayout, QGridLayout, QLabel, QGroupBox, QPushButton

import logconfig

class AbstractStateMachineGraph(QStateMachine):
    def __init__(
        self,
        qtTabWidget,
        qtContainerWidget,
        signal_stop,
        signal_wait_for_data,
        signal_data_available,
    ):
        super().__init__()

        self.logger = logging.getLogger(self.__class__.__name__)
        logconfig.configure_logger(self.logger)
        self.logger.info("Started log for %s" % self.__class__.__name__)

        # Keep references to the objects received in parameters
        self.tabgroup = qtTabWidget
        self.tab = qtContainerWidget

        # Colors
        self.color_pol0 = "c"
        self.color_pol1 = "g"
        self.color_position = "y"
        self.pen_position = pg.mkPen(color=self.color_position, width=2)
        self.pen_rel_phase = pg.mkPen(color="c", width=1)

        # Print log message to show list of widgets that we find in the tab container widget
        child_widget_list = self.tab.children()
        for child_widget in child_widget_list:
            self.logger.debug(
                "found widget: {}, objectName: {}: ".format(child_widget, child_widget.objectName())
            )

        self.signal_stop = signal_stop
        self.signal_wait_for_data = signal_wait_for_data
        self.signal_data_available = signal_data_available

        self.mutex = QMutex()

        self.configure_widgets()
        self.configure_state_machine()
        self.configure_axes()

    def configure_state_machine(self):
        # Define the finite state machine (FSM)
        self.state_stop = QState()
        self.state_wait_for_data = QState()
        self.state_animate = QState()

        self.state_stop.addTransition(self.signal_wait_for_data, self.state_wait_for_data)
        self.state_wait_for_data.addTransition(self.signal_stop, self.state_stop)
        self.state_wait_for_data.addTransition(self.signal_data_available, self.state_animate)
        self.state_animate.addTransition(self.signal_stop, self.state_stop)

        self.addState(self.state_stop)
        self.addState(self.state_wait_for_data)
        self.addState(self.state_animate)

        # Listen for the signals from state changes and call functions
        self.state_stop.entered.connect(self.stop)
        self.state_wait_for_data.entered.connect(self.wait_for_data)
        self.state_animate.entered.connect(self.animate)

        # Listen for signal from QtGUI tab select
        self.tabgroup.currentChanged.connect(self.handle_tab_select)

    def handle_tab_select(self, index):
        if index is self.tabgroup.indexOf(self.tab):
            self.logger.debug("tab selected.  signal state machine to wait for data")
            self.signal_wait_for_data.emit()
        else:
            self.logger.debug("tab not selected. signal state machine to stop")
            self.signal_stop.emit()


class AnimatedGraph(AbstractStateMachineGraph):
    """
    Implement the required abstractmethods using PyQtGraph library.  Can be used for any graph tab
    """

    def stop(self):
        self.logger.debug("Entered state_stop")
        try:
            self.timer.stop()
            del self.timer
        except AttributeError:
            self.logger.debug("self.timer has not yet been created.  Nothing to stop")

    def wait_for_data(self):
        self.logger.debug("Entered state_wait_for_data")

    def animate(self):
        self.logger.debug("Entered state_animate")

        # Record the time stamp of the first data
        self.time0 = self.time_mjd
        self.initialize_graph()

        frame_rate = 10
        update_interval_ms = int(1000 * (1 / frame_rate))

        self.timer = QTimer()
        self.timer.setInterval(update_interval_ms)
        self.timer.timeout.connect(self.update_graph)
        self.timer.start()

class GraphSpectrum(AnimatedGraph):
    """
    Implement graph functions of Spectrum using PyQtGraph
    """

    def __init__(
        self,
        qtTabWidget,
        qtContainerWidget,
        signal_stop,
        signal_wait_for_data,
        signal_data_available,
        nchan=1024,
    ):
        # Keep the number of spectrum channels for this type of graph.  Not relevant to other graphs.
        self.nchan = nchan
        # Call the parent class constructor to connect everything else.
        super().__init__(
            qtTabWidget,
            qtContainerWidget,
            signal_stop,
            signal_wait_for_data,
            signal_data_available,
        )

    def configure_widgets(self):
        self.plotwidget_freq = pg.PlotWidget()
        self.plotwidget_time = pg.PlotWidget()

        self.verticalLayout_tab_spectrum = QVBoxLayout()
        self.verticalLayout_tab_spectrum.addWidget(self.plotwidget_freq)
        self.verticalLayout_tab_spectrum.addWidget(self.plotwidget_time)
        self.tab.setLayout(self.verticalLayout_tab_spectrum)


    def configure_axes(self):
        self.plotwidget_freq.setYRange(-80, 0)
        self.plotwidget_freq.setTitle("Magnitude Spectrum", color="w", size="14pt")

        self.plotwidget_freq_axis_bottom = pg.AxisItem("bottom", text="Channel Index")
        self.plotwidget_freq_axis_bottom.showLabel(True)
        self.plotwidget_freq_axis_left = pg.AxisItem("left", text="Magnitude [dB no cal]")
        self.plotwidget_freq_axis_left.showLabel(True)
        self.plotwidget_freq.setAxisItems(
            axisItems={
                "bottom": self.plotwidget_freq_axis_bottom,
                "left": self.plotwidget_freq_axis_left,
            }
        )
        self.plotwidget_freq.addLegend()

        pen_spectrum0 = pg.mkPen(color=self.color_pol0, width=2)
        pen_spectrum1 = pg.mkPen(color=self.color_pol1, width=2)

        self.pdi_spectrum0 = self.plotwidget_freq.plot([], [], pen=pen_spectrum0, name="pol0")
        self.pdi_spectrum1 = self.plotwidget_freq.plot([], [], pen=pen_spectrum1, name="pol1")
    
        self.plotwidget_time.setYRange(-80, 0)
        plotwidget_time_axis_bottom = pg.AxisItem("bottom", text="Time")
        plotwidget_time_axis_bottom.showLabel(True)
        plotwidget_time_axis_left = pg.AxisItem("left", text="Magnitude [dB no cal]")
        plotwidget_time_axis_left.showLabel(True)
        self.plotwidget_time.setAxisItems(
            axisItems={
                "bottom": plotwidget_time_axis_bottom,
                "left": plotwidget_time_axis_left,
            }
        )
        self.plotwidget_time.addLegend()

        self.pen_time0 = pg.mkPen(color=self.color_pol0, width=2)
        self.pen_time1 = pg.mkPen(color=self.color_pol1, width=2)

        self.pdi_time0 = self.plotwidget_time.plot([], [], pen=self.pen_time0, name="pol0")
        self.pdi_time1 = self.plotwidget_time.plot([], [], pen=self.pen_time1, name="pol1")


    def initialize_graph(self):
        self.logger.debug("Entered initialize_graph")

        self.plotwidget_time.setYRange(-80, 0)

        # X axis data for spectrum and time.  we will re use this data
        self.xdata_spectrum = np.arange(self.nchan)
        self.xdata_time = list(np.arange(-30, 0, 0.1))

        # Y axis data for spectrum and time.  Use this only 1 time for initialize.
        # New data will arrive to update later
        self.ydata_spectrum_initial = np.zeros(self.nchan)
        self.ydata_time0 = list(np.zeros(300) - 1000)
        self.ydata_time1 = list(np.zeros(300) - 1000)

        self.pdi_spectrum0.setData(self.xdata_spectrum, self.ydata_spectrum_initial)
        self.pdi_spectrum1.setData(self.xdata_spectrum, self.ydata_spectrum_initial)

        self.pdi_time0.setData(self.xdata_time, self.ydata_time0)
        self.pdi_time1.setData(self.xdata_time, self.ydata_time0)

        grid_opacity = 0.4
        self.plotwidget_freq.getPlotItem().showGrid(True, True, alpha=grid_opacity)
        self.plotwidget_time.getPlotItem().showGrid(True, True, alpha=grid_opacity)

    def set_data(self, time_mjd, data_ndarray):
        self.mutex.lock()
        self.time_mjd = time_mjd
        self.data_ndarray = data_ndarray
        self.mutex.unlock()

    def update_graph(self):
        self.pdi_spectrum0.setData(self.xdata_spectrum, self.data_ndarray[0, :])
        self.pdi_spectrum1.setData(self.xdata_spectrum, self.data_ndarray[1, :])

        self.xdata_time = self.xdata_time[1:]  # Remove the first y element.
        time_elapsed_sec = (self.time_mjd - self.time0) * 86400
        self.plotwidget_time.setXRange(time_elapsed_sec - 30, time_elapsed_sec)
        self.xdata_time.append(time_elapsed_sec)

        # Peak of spectrum ignoring the last frequency channel (artifact is bigger than real signal)
        # TODO (SS. 08/2022): Add a slider ROI to spectrum graph and select the frequency channel instead
        # of using the peak.  Because the peak is usuall an interference signal.
        peak_spectrum0 = np.max(self.data_ndarray[0, :-1])
        peak_spectrum1 = np.max(self.data_ndarray[1, :-1])

        self.ydata_time0 = self.ydata_time0[1:]  # Remove the first
        self.ydata_time0.append(peak_spectrum0)
        self.pdi_time0.setData(
            self.xdata_time, self.ydata_time0, symbol="+", symbolPen=self.pen_time0
        )

        self.ydata_time1 = self.ydata_time1[1:]  # Remove the first
        self.ydata_time1.append(peak_spectrum1)
        self.pdi_time1.setData(
            self.xdata_time, self.ydata_time1, symbol="+", symbolPen=self.pen_time1
        )

class GraphAntenna(AnimatedGraph):
    """
    docstring
    """

    def configure_widgets(self):
        self.pw_antenna_az = pg.PlotWidget()
        self.pw_antenna_el = pg.PlotWidget()
        
        self.verticalLayout_tab_antenna = QVBoxLayout()
        self.verticalLayout_tab_antenna.addWidget(self.pw_antenna_az)
        self.verticalLayout_tab_antenna.addWidget(self.pw_antenna_el)
        self.tab.setLayout(self.verticalLayout_tab_antenna)

    def configure_axes(self):
        # Define a PlotItem to show data on position axis scale
        self.pi_antenna_az_pos = self.pw_antenna_az.getPlotItem()
        self.pdi_az_pos = self.pi_antenna_az_pos.plot(
            [], [], pen=self.pen_position, name="position"
        )

        self.pi_antenna_az_pos.setYRange(-70, 420)
        # Define axes and units
        el_axis_bottom = pg.AxisItem("bottom", text="Time", units="s")
        el_axis_bottom.showLabel(True)
        el_axis_left = pg.AxisItem("left", text="AZ Position", units="deg")
        el_axis_left.showLabel(True)

        self.pi_antenna_az_pos.setAxisItems(
            axisItems={
                "bottom": el_axis_bottom,
                "left": el_axis_left,
            }
        )

        # Define a PlotItem to show data on position axis scale
        self.pi_antenna_el_pos = self.pw_antenna_el.getPlotItem()
        self.pdi_el_pos = self.pi_antenna_el_pos.plot(
            [], [], pen=self.pen_position, name="position"
        )

        self.pi_antenna_el_pos.setYRange(0, 90)
        # Define axes and units
        el_axis_bottom = pg.AxisItem("bottom", text="Time", units="s")
        el_axis_bottom.showLabel(True)
        el_axis_left = pg.AxisItem("left", text="EL Position", units="deg")
        el_axis_left.showLabel(True)

        self.pi_antenna_el_pos.setAxisItems(
            axisItems={
                "bottom": el_axis_bottom,
                "left": el_axis_left,
            }
        )

    def initialize_graph(self):
        self.logger.debug("Entered initialize_graph")

        # Initialize data arrays
        self.xdata_time = list(np.arange(-30, 0, 0.1))

        self.ydata_az_pos = list(np.zeros(300) - 1000)
        self.ydata_az_vel = list(np.zeros(300) - 1000)
        self.ydata_az_acc = list(np.zeros(300) - 1000)

        self.ydata_el_pos = list(np.zeros(300) - 1000)
        self.ydata_el_vel = list(np.zeros(300) - 1000)
        self.ydata_el_acc = list(np.zeros(300) - 1000)

        self.pdi_az_pos.setData(self.xdata_time, self.ydata_az_pos)
        self.pdi_el_pos.setData(self.xdata_time, self.ydata_el_pos)

        grid_opacity = 0.4
        self.pw_antenna_az.getPlotItem().showGrid(True, True, alpha=grid_opacity)
        self.pw_antenna_el.getPlotItem().showGrid(True, True, alpha=grid_opacity)

    def set_data(self, time_mjd, data_ndarray):
        self.mutex.lock()
        self.time_mjd = time_mjd
        self.data_ndarray = data_ndarray
        self.mutex.unlock()

    def update_graph(self):
        # Remove the first item of each data
        self.xdata_time = self.xdata_time[1:]
        self.ydata_az_acc = self.ydata_az_acc[1:]
        self.ydata_az_pos = self.ydata_az_pos[1:]
        self.ydata_az_vel = self.ydata_az_vel[1:]

        self.ydata_el_acc = self.ydata_el_acc[1:]
        self.ydata_el_pos = self.ydata_el_pos[1:]
        self.ydata_el_vel = self.ydata_el_vel[1:]

        time_elapsed_sec = (self.time_mjd - self.time0) * 86400
        self.pw_antenna_az.setXRange(time_elapsed_sec - 30, time_elapsed_sec)
        self.pw_antenna_el.setXRange(time_elapsed_sec - 30, time_elapsed_sec)
        self.xdata_time.append(time_elapsed_sec)

        self.ydata_az_acc.append(self.data_ndarray[0][0])
        self.ydata_az_vel.append(self.data_ndarray[0][1])
        self.ydata_az_pos.append(self.data_ndarray[0][2])
        self.ydata_el_acc.append(self.data_ndarray[1][0])
        self.ydata_el_vel.append(self.data_ndarray[1][1])
        self.ydata_el_pos.append(self.data_ndarray[1][2])

        self.pdi_az_pos.setData(self.xdata_time, self.ydata_az_pos)
        self.pdi_el_pos.setData(self.xdata_time, self.ydata_el_pos)

class GraphHolography(AnimatedGraph):
    """
    Implement graph functions of Holography preview using PyQtGraph
    """

    def configure_widgets(self):
        
        self.group_holodata_message = QGroupBox("Holography Data Message")
        self.layout_group_holodata_message = QGridLayout()
        self.layout_group_holodata_message.setAlignment(Qt.AlignTop)
        self.group_holodata_message.setLayout(self.layout_group_holodata_message)

        # add labels to group
        self.label_timestamp_name = QLabel("Timestamp")
        self.label_integration_time_name = QLabel("Data Capture Duration")
        self.label_trace1_name = QLabel("ch1: REF") 
        self.label_trace2_name = QLabel("ch2: DUT)") 
        self.label_trace3_name = QLabel("tr3: (DUT / REF)") 
        self.label_trace4_name = QLabel("tr4: (DUT / REF)") 
        self.label_freq1_name = QLabel("Frequency")
        
        self.label_absolute_power1_name = QLabel("Absolute Magnitude")
        self.label_absolute_power2_name = QLabel("Absolute Magnitude")
        self.label_relative_magnitude_name = QLabel("Relative Magnitude")
        self.label_relative_phase_name = QLabel("Relative Phase")
        self.label_obsmode_name = QLabel("atfits OBSMODE")

        self.label_timestamp_val = QLabel()
        self.label_timestamp_val.setAutoFillBackground(True)

        self.label_integration_time_val = pg.ValueLabel(suffix="s", siPrefix=True)
        self.label_integration_time_val.setAutoFillBackground(True)
        
        self.label_freq1_val = pg.ValueLabel(formatStr="{value:.3f} kHz")
        self.label_freq1_val.setAutoFillBackground(True)
        
        self.label_absolute_power1_val = pg.ValueLabel(formatStr="{value:.1f} dBm")
        self.label_absolute_power1_val.setAutoFillBackground(True)
        
        self.label_absolute_power2_val = pg.ValueLabel(formatStr="{value:.1f} dBm")
        self.label_absolute_power2_val.setAutoFillBackground(True)
        
        self.label_relative_magnitude_val = pg.ValueLabel(formatStr="{value:.1f} dB")
        self.label_relative_magnitude_val.setAutoFillBackground(True)

        self.label_relative_phase_val = pg.ValueLabel(suffix="deg", siPrefix=True)
        self.label_relative_phase_val.setAutoFillBackground(True)

        self.label_obsmode_val = QLabel()
        self.label_obsmode_val.setAutoFillBackground(True)

        self.pb_clear_map = QPushButton("Clear Map")
    
        self.layout_group_holodata_message.addWidget(self.label_timestamp_name, 0, 0, 1, 1)
        self.layout_group_holodata_message.addWidget(self.label_timestamp_val, 0, 1, 1, 4)

        self.layout_group_holodata_message.addWidget(self.label_integration_time_name, 1, 0, 1, 1)
        self.layout_group_holodata_message.addWidget(self.label_integration_time_val, 1, 1, 1, 4)

        self.layout_group_holodata_message.addWidget(self.label_obsmode_name, 2, 0, 1, 1)
        self.layout_group_holodata_message.addWidget(self.label_obsmode_val, 2, 1, 1, 4)
        
        self.layout_group_holodata_message.addWidget(self.label_trace1_name, 3, 0)
        self.layout_group_holodata_message.addWidget(self.label_freq1_name, 3, 1)
        self.layout_group_holodata_message.addWidget(self.label_freq1_val, 3, 2)
        self.layout_group_holodata_message.addWidget(self.label_absolute_power1_name, 3, 3)
        self.layout_group_holodata_message.addWidget(self.label_absolute_power1_val, 3, 4)

        self.layout_group_holodata_message.addWidget(self.label_trace2_name, 4, 0)
        self.layout_group_holodata_message.addWidget(self.label_absolute_power2_name, 4, 3)
        self.layout_group_holodata_message.addWidget(self.label_absolute_power2_val, 4, 4)

        self.layout_group_holodata_message.addWidget(self.label_trace3_name, 5, 0)
        self.layout_group_holodata_message.addWidget(self.label_relative_magnitude_name, 5, 3)
        self.layout_group_holodata_message.addWidget(self.label_relative_magnitude_val, 5, 4)

        self.layout_group_holodata_message.addWidget(self.label_trace4_name, 6, 0)
        self.layout_group_holodata_message.addWidget(self.label_relative_phase_name, 6, 3)
        self.layout_group_holodata_message.addWidget(self.label_relative_phase_val, 6, 4)
     
        self.layout_left = QVBoxLayout()
        self.layout_right = QVBoxLayout()
        self.layout_main = QHBoxLayout()
        
        self.glw_geometry = pg.GraphicsLayoutWidget()
        self.pw_magnitude_ref = pg.PlotWidget()
        self.pw_magnitude_tst = pg.PlotWidget()
        self.pw_magnitude_rel = pg.PlotWidget()
        self.pw_phase = pg.PlotWidget()

        self.layout_left.addWidget(self.group_holodata_message)
        self.layout_left.addWidget(self.pb_clear_map)
        self.layout_left.addWidget(self.pw_magnitude_ref)
        self.layout_left.addWidget(self.pw_magnitude_tst)
        self.layout_left.addWidget(self.pw_magnitude_rel)

        
        self.layout_right.addWidget(self.glw_geometry)
        self.layout_right.addWidget(self.pw_phase)
        
        self.layout_main.addLayout(self.layout_left, 50)
        self.layout_main.addLayout(self.layout_right, 50)
        self.tab.setLayout(self.layout_main)

    def configure_axes(self):
        self.logger.debug("configure axes")
        
                # Create AxisItems for time slice.  Include {top, bottom} x {Time, Angles} = 4 AxisItems
        self.geometry_axis_list = [
            pg.AxisItem(
                orientation="bottom",
                text="Longitude offset from tracking",
                units="arcsec",
            ),
            pg.AxisItem(
                orientation="top",
                text="Longitude offset from tracking",
                units="deg",
            ),
            pg.AxisItem(
                orientation="left",
                text="Latitude offset from tracking",
                units="arcsec",
            ),
            pg.AxisItem(
                orientation="right",
                text="Latitude offset from tracking",
                units="deg",
            ),
        ]

        # Scale axis to convert degrees to arcseconds
        self.geometry_axis_list[0].setScale(3600)
        self.geometry_axis_list[2].setScale(3600)

        # Disable automatic SI prefix and always show unit = arcsec
        self.geometry_axis_list[0].enableAutoSIPrefix(False)
        self.geometry_axis_list[2].enableAutoSIPrefix(False)

        for axisitem in self.geometry_axis_list:
            axisitem.showLabel(True)

        # Create PlotItem from the PlotLayoutWidget.
        # Then Create the PlotDataItem from the PlotItem
        self.pi_geometry = self.glw_geometry.addPlot(
            0,
            0,
            title="Geometry",
            axisItems={
                "bottom": self.geometry_axis_list[0],
                "top": self.geometry_axis_list[1],
                "left": self.geometry_axis_list[2],
                "right": self.geometry_axis_list[3],
            },
        )
        self.pi_geometry.addLegend()
        self.pi_geometry.showGrid(True, True, alpha=0.2)
        self.pi_geometry.setAspectLocked(True)

        # Define a PlotItem to show data on position axis scale
        self.pi_magnitude_ref = self.pw_magnitude_ref.getPlotItem()
        self.pi_magnitude_tst = self.pw_magnitude_tst.getPlotItem()
        self.pi_magnitude_rel = self.pw_magnitude_rel.getPlotItem()
        self.pi_phase = self.pw_phase.getPlotItem()
        self.pi_phase.setTitle("I/Q Phase Constellation (Normalized Magnitude)")
        
        self.pi_magnitude_ref.addLegend()
        self.pi_magnitude_tst.addLegend()
        self.pi_magnitude_rel.addLegend()
        self.pi_phase.addLegend()

        # Line Plots
        self.pdi_magnitude_ref = self.pi_magnitude_ref.plot([], [], pen=pg.mkPen(color="y", width=1), name="ch1 REF [dBm]")
        self.pdi_magnitude_tst = self.pi_magnitude_tst.plot([], [], pen=pg.mkPen(color="c", width=1), name="ch2 DUT [dBm]")
        self.pdi_magnitude_rel = self.pi_magnitude_rel.plot([], [], pen=pg.mkPen(color="m", width=1), name="(DUT / REF) [dB]")
        
        # Markers
        self.pdi_magnitude_ref_marker = self.pi_magnitude_ref.plot([], [], pen=None, symbol="o", symbolPen=pg.mkPen(color="y", width=2))
        self.pdi_magnitude_tst_marker = self.pi_magnitude_tst.plot([], [], pen=None, symbol="o", symbolPen=pg.mkPen(color="c", width=2))
        self.pdi_magnitude_rel_marker = self.pi_magnitude_rel.plot([], [], pen=None, symbol="o", symbolPen=pg.mkPen(color="m", width=2))
        
        # I/Q Constellation
        angles = np.linspace(0, 2*np.pi, 360, endpoint=True)
        unit_circle_xx = np.cos(angles)
        unit_circle_yy = np.sin(angles)
        self.pdi_unit_circle = self.pi_phase.plot(unit_circle_xx, unit_circle_yy, pen="w")
        
        self.pdi_phase_ref = self.pi_phase.plot([], [], pen=None, symbol="o", symbolPen=pg.mkPen(color="y", width=2), symbolBrush=None, symbolSize=6)
        self.pdi_phase_tst = self.pi_phase.plot([], [], pen=None, symbol="o", symbolPen=pg.mkPen(color="c", width=2), symbolBrush=None, symbolSize=6)
        # self.pdi_phase_rel = self.pi_phase.plot([], [] , pen=None, symbol="o", symbolPen=pg.mkPen(color="m", width=2), symbolBrush=None, symbolSize=10)
        self.pdi_phase_marker_ref = self.pi_phase.plot([], [], name="ch1 REF", pen=None, symbol="o", symbolPen=pg.mkPen(color="y", width=4), symbolBrush=None, symbolSize=20)
        self.pdi_phase_marker_tst = self.pi_phase.plot([], [], name="ch2 DUT", pen=None, symbol="o", symbolPen=pg.mkPen(color="c", width=4), symbolBrush=None, symbolSize=24)
        self.pdi_phase_marker_rel = self.pi_phase.plot([], [], name="(DUT / REF)", pen=None, symbol="o", symbolPen=pg.mkPen(color="m", width=4), symbolBrush=None, symbolSize=28)
        
        self.pi_magnitude_ref.setAxisItems(
            axisItems={
                "bottom": pg.AxisItem("bottom", text="Frequency", units="Hz"),
                "left": pg.AxisItem("left", text="Magnitude", units="dBm", siPrefix=False),
            }
        )
        self.pi_magnitude_ref.getAxis("left").showLabel(True)
        self.pi_magnitude_ref.getAxis("bottom").showLabel(True)

        self.pi_magnitude_tst.setAxisItems(
            axisItems={
                "bottom": pg.AxisItem("bottom", text="Frequency", units="Hz"),
                "left": pg.AxisItem("left", text="Magnitude", units="dBm", siPrefix=False),
            }
        )
        self.pi_magnitude_tst.getAxis("left").showLabel(True)
        self.pi_magnitude_tst.getAxis("bottom").showLabel(True)

        self.pi_magnitude_rel.setAxisItems(
            axisItems={
                "bottom": pg.AxisItem("bottom", text="Frequency", units="Hz"),
                "left": pg.AxisItem("left", text="Magnitude", units="dB", siPrefix=False),
            }
        )
        self.pi_magnitude_rel.getAxis("left").showLabel(True)
        self.pi_magnitude_rel.getAxis("bottom").showLabel(True)


        #self.pi_phase.setYRange(0, 90)
        # Define axes and units
        el_axis_bottom = pg.AxisItem("bottom", text='Real "I"')
        el_axis_bottom.showLabel(True)
        el_axis_left = pg.AxisItem("left", text='Imaginary "Q"')
        el_axis_left.showLabel(True)

        self.pi_phase.setAxisItems(
            axisItems={
                "bottom": el_axis_bottom,
                "left": el_axis_left,
            }
        )

    def initialize_graph(self):
        self.logger.debug("Initialize graph empty list of data ...")

        self.longoff_holo_list = []
        self.latoff_holo_list = []

        self.longoff_corr_list = []
        self.latoff_corr_list = []

        # Geometry scatter plot
        self.spi_geometry_holo = pg.ScatterPlotItem(
            x=self.longoff_holo_list, y=self.latoff_holo_list, pen="y", brush=None, symbol="o", size=(160/3600), pxMode=False
        )
        self.spi_geometry_corr = pg.ScatterPlotItem(
            x=self.longoff_corr_list, y=self.latoff_corr_list, pen="c", brush=None, symbol="o", size=(160/3600/2), pxMode=False
        )
        
        self.pi_geometry.addItem(self.spi_geometry_holo)
        self.pi_geometry.addItem(self.spi_geometry_corr)

        self.pb_clear_map.clicked.connect(self.clear_map)
    
        # Set all plots to initial data
        self.pdi_magnitude_ref.setData([], [])
        self.pdi_magnitude_tst.setData([], [])
        self.pdi_magnitude_rel.setData([], [])
        
        # Set Y axis range
        self.pw_magnitude_ref.setYRange(-100, 0)
        self.pw_magnitude_tst.setYRange(-100, 0)
        self.pw_magnitude_rel.setYRange(-40, 20)

        grid_opacity = 0.4
        self.pi_magnitude_ref.showGrid(True, True, alpha=grid_opacity)
        self.pi_magnitude_tst.showGrid(True, True, alpha=grid_opacity)
        self.pi_magnitude_rel.showGrid(True, True, alpha=grid_opacity)
        self.pi_phase.showGrid(True, True, alpha=grid_opacity)
        self.pi_phase.setXRange(-1, 1)
        self.pi_phase.setYRange(-1, 1)
        self.pi_phase.setAspectLocked(True)
        

        self.logger.debug("Initialize graph DONE")

    def clear_map(self):
        self.mutex.lock()
        
        self.logger.debug("Clear geometry map")
        
        del(self.longoff_holo_list)
        del(self.latoff_holo_list)
        del(self.longoff_corr_list)
        del(self.latoff_corr_list)
        
        self.longoff_holo_list = []
        self.latoff_holo_list = []
        self.longoff_corr_list = []
        self.latoff_corr_list = []
        
        self.mutex.unlock()

    def set_data(self, 
                 msg_holodata,
                 longoff,
                 latoff,
                 obsmode):

        self.mutex.lock()
        
        self.msg_holodata = msg_holodata
        self.time_mjd = msg_holodata["time_mjd"]
        
        self.longoff = longoff
        self.latoff = latoff
        self.obsmode = obsmode
            
        self.mutex.unlock()

    def update_graph(self):
        time_str = Time(self.time_mjd, format="mjd").iso
        self.label_timestamp_val.setText(time_str)
        self.label_integration_time_val.setValue(self.msg_holodata["sample_capture_duration"])
        self.label_freq1_val.setValue(self.msg_holodata["marker_val_freq_hz"] / 1000.0)
        self.label_absolute_power1_val.setValue(self.msg_holodata["marker_val_absolute_magnitude_ch1_dbm"])
        self.label_absolute_power2_val.setValue(self.msg_holodata["marker_val_absolute_magnitude_ch2_dbm"])
        self.label_relative_magnitude_val.setValue(self.msg_holodata["marker_val_relative_magnitude_db"])
        self.label_relative_phase_val.setValue(self.msg_holodata["marker_val_relative_phase_deg"])

        self.label_obsmode_val.setText(self.obsmode)
        if self.obsmode == "CORR":
            self.label_obsmode_val.setStyleSheet("color: cyan; font-weight: bold")
            # update geometry scatter plot
            self.longoff_corr_list.append(self.longoff)
            self.latoff_corr_list.append(self.latoff)
            self.spi_geometry_corr.setData(x=self.longoff_corr_list, y=self.latoff_corr_list)
        elif self.obsmode == "HOLO":
            self.label_obsmode_val.setStyleSheet("color: yellow; font-weight: bold")
            # update geometry scatter plot
            self.longoff_holo_list.append(self.longoff)
            self.latoff_holo_list.append(self.latoff)
            self.spi_geometry_holo.setData(x=self.longoff_holo_list, y=self.latoff_holo_list)

        else:
            self.label_obsmode_val.setStyleSheet("color: red; font-weight: bold")

        self.pdi_magnitude_ref.setData(self.msg_holodata["spectrum_freq_axis"], self.msg_holodata["spectrum_absolute_magnitude_ch1_dbm"])
        self.pdi_magnitude_ref_marker.setData([self.msg_holodata["marker_val_freq_hz"]], [self.msg_holodata["marker_val_absolute_magnitude_ch1_dbm"]])

        self.pdi_magnitude_tst.setData(self.msg_holodata["spectrum_freq_axis"], self.msg_holodata["spectrum_absolute_magnitude_ch2_dbm"])
        self.pdi_magnitude_tst_marker.setData([self.msg_holodata["marker_val_freq_hz"]], [self.msg_holodata["marker_val_absolute_magnitude_ch2_dbm"]])

        self.pdi_magnitude_rel.setData(self.msg_holodata["spectrum_freq_axis"], self.msg_holodata["spectrum_relative_magnitude_db"])
        self.pdi_magnitude_rel_marker.setData([self.msg_holodata["marker_val_freq_hz"]], [self.msg_holodata["marker_val_relative_magnitude_db"]])

        self.pdi_phase_ref.setData(np.cos(self.msg_holodata["timeseries_phase_ch1_rad"]), np.sin(self.msg_holodata["timeseries_phase_ch1_rad"]))
        self.pdi_phase_tst.setData(np.cos(self.msg_holodata["timeseries_phase_ch2_rad"]), np.sin(self.msg_holodata["timeseries_phase_ch2_rad"]))
        self.pdi_phase_marker_ref.setData([np.cos(np.radians(self.msg_holodata["marker_val_phase_ch1_deg"]))],
                                          [np.sin(np.radians(self.msg_holodata["marker_val_phase_ch1_deg"]))])
        self.pdi_phase_marker_tst.setData([np.cos(np.radians(self.msg_holodata["marker_val_phase_ch2_deg"]))],
                                          [np.sin(np.radians(self.msg_holodata["marker_val_phase_ch2_deg"]))])
        self.pdi_phase_marker_rel.setData([np.cos(np.radians(self.msg_holodata["marker_val_relative_phase_deg"]))],
                                          [np.sin(np.radians(self.msg_holodata["marker_val_relative_phase_deg"]))])