import logging

def configure_logger(logger):
	logger.setLevel(logging.DEBUG)
	fmt_scrn = '%(asctime)s [%(levelname)s] %(module)s.%(name)s.%(funcName)s(): %(message)s'
	formatter_screen = logging.Formatter(fmt=fmt_scrn)
	handler_screen = logging.StreamHandler()
	handler_screen.setFormatter(formatter_screen)
	handler_screen.setLevel(logging.DEBUG)
	logger.addHandler(handler_screen)