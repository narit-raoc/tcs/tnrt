# Create a scan to check time synchronization between ACU and TCS computer.
# Automatically add it to the queue
delay_until_scheduled_start = 10
project_id = 'timesync'
obs_id = 'nash'
priority = 0
start_mjd = (Time.now() + delay_until_scheduled_start * units.second).mjd
source_name = 'no_source'
line_name = 'no_line'

# Record data for 10 seconds
duration = 10.0

schedule_params = ScheduleParams(project_id, obs_id, start_mjd=start_mjd)
scantype_params = ScantypeParamsTimeSync(duration)

# Create the scan and add it to the queue
add_scan(schedule_params, scantype_params)

# Run all scans in the queue.  Receive results after queue is finished.
results = run_queue()

# Show the results of all scans from this run to the terminal window.  
# All types of scans have some common results.  For example: a list of filenames 
# that were recorded. 
log('all results: %s' % results)
