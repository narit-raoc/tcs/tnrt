import argparse
from argparse import RawTextHelpFormatter
import os
import sys
import numpy as np
import matplotlib
matplotlib.use("QT5agg")
import matplotlib.pyplot as plt
plt.style.use('dark_background')
from astropy.io import fits

helpstring = "Expected MBFITS file: See DataFormatMbfits.py"

# Setup rules for command line arguments
parser = argparse.ArgumentParser(description='Reads FITS file written by PipelineMbfits and diplays some graphs',
									epilog=helpstring, formatter_class=RawTextHelpFormatter)
parser.add_argument('filename', help='FITS file with format specified by AntennaDataFormat.py')

# If no command line arguments are avaiable, show help message and exit
if len(sys.argv[1:]) == 0:
	parser.print_help()
	exit()

# Read command line arguments into argparse structure
args = parser.parse_args()

hdul = None

if(os.path.isfile(args.filename) is not True):
	print('ERROR: File not found')
	print('os.path.isfile(%s) = False' % args.filename)
	hdul = None
else:
	print('Processing file %s: ' % args.filename)
	fid = open(args.filename, 'rb')
	hdul = fits.open(fid)
	# leave the file open becuase fits.open will not store the entire file in RAM if it is
	# too big.  Close file at the end of this script.
	#print(hdul.info())
if(hdul is None):
	exit()

# -----------------------------------------------------------------------------
# Make Graphs
# -----------------------------------------------------------------------------
# Strings for labels
str_title = 'From File %s' % os.path.basename(args.filename)
grid_opacity = 0.2

X = 0
Y = 1
Z = 2
TX = 3
TY = 4
TZ = 5

# Make a time axis for all the plots.  Use seconds after start
tstart = hdul['MONITOR-ANT-PACKET'].data['time_tcs_mjd'][0]
tt = (hdul['MONITOR-ANT-PACKET'].data['time_tcs_mjd'] - tstart) * 86400.0

# Linear axes
fig2 = plt.figure('Linear Axes', figsize=(8,10))

fig2_ax1a = fig2.add_subplot(3, 1, 1)
fig2_ax1a.set_title(str_title)
fig2_ax1a.set_ylabel('X position [mm]')
fig2_ax1a.grid(True, which="both", alpha=grid_opacity)

fig2_ax1a.plot(tt, hdul['MONITOR-ANT-HXP'].data['pos_desired'][:,X], label='pos_desired')
fig2_ax1a.plot(tt, hdul['MONITOR-ANT-HXP'].data['pos_actual'][:,X], label='pos_actual')
fig2_ax1a.plot(tt, hdul['MONITOR-ANT-HXP'].data['offset'][:,X], label='offset')
fig2_ax1a.plot(tt, hdul['MONITOR-ANT-HXP-TRACK'].data['track_mode_actual_position'][:,0], label='track_mode_actual_position')
fig2_ax1a.legend()

fig2_ax2a = fig2.add_subplot(3, 1, 2)
fig2_ax2a.set_ylabel('Y position [mm]')
fig2_ax2a.grid(True, which="both", alpha=grid_opacity)
fig2_ax2a.plot(tt, hdul['MONITOR-ANT-HXP'].data['pos_desired'][:,Y], label='pos_desired')
fig2_ax2a.plot(tt, hdul['MONITOR-ANT-HXP'].data['pos_actual'][:,Y], label='pos_actual')
fig2_ax2a.plot(tt, hdul['MONITOR-ANT-HXP'].data['offset'][:,Y], label='offset')
fig2_ax2a.plot(tt, hdul['MONITOR-ANT-HXP-TRACK'].data['track_mode_actual_position'][:,0], label='track_mode_actual_position')
fig2_ax2a.legend()

fig2_ax3a = fig2.add_subplot(3, 1, 3)
fig2_ax3a.set_ylabel('Z position [mm]')
fig2_ax3a.grid(True, which="both", alpha=grid_opacity)
fig2_ax3a.plot(tt, hdul['MONITOR-ANT-HXP'].data['pos_desired'][:,Z], label='pos_desired')
fig2_ax3a.plot(tt, hdul['MONITOR-ANT-HXP'].data['pos_actual'][:,Z], label='pos_actual')
fig2_ax3a.plot(tt, hdul['MONITOR-ANT-HXP'].data['offset'][:,Z], label='offset')
fig2_ax3a.plot(tt, hdul['MONITOR-ANT-HXP-TRACK'].data['track_mode_actual_position'][:,0], label='track_mode_actual_position')
fig2_ax3a.set_xlabel('Time [sec] after start')
fig2_ax3a.legend()

# Rotation / Tilt axes
fig3 = plt.figure('Rotation Axes', figsize=(8,10))

fig3_ax1a = fig3.add_subplot(3, 1, 1)
fig3_ax1a.set_title(str_title)
fig3_ax1a.set_ylabel('TX position [deg]')
fig3_ax1a.grid(True, which="both", alpha=grid_opacity)

fig3_ax1a.plot(tt, hdul['MONITOR-ANT-HXP'].data['pos_desired'][:,TX], label='pos_desired')
fig3_ax1a.plot(tt, hdul['MONITOR-ANT-HXP'].data['pos_actual'][:,TX], label='pos_actual')
fig3_ax1a.plot(tt, hdul['MONITOR-ANT-HXP'].data['offset'][:,TX], label='offset')
fig3_ax1a.plot(tt, hdul['MONITOR-ANT-HXP-TRACK'].data['track_mode_actual_position'][:,0], label='track_mode_actual_position')
fig3_ax1a.legend()

fig3_ax2a = fig3.add_subplot(3, 1, 2)
fig3_ax2a.set_ylabel('TY position [deg]')
fig3_ax2a.grid(True, which="both", alpha=grid_opacity)
fig3_ax2a.plot(tt, hdul['MONITOR-ANT-HXP'].data['pos_desired'][:,TY], label='pos_desired')
fig3_ax2a.plot(tt, hdul['MONITOR-ANT-HXP'].data['pos_actual'][:,TY], label='pos_actual')
fig3_ax2a.plot(tt, hdul['MONITOR-ANT-HXP'].data['offset'][:,TY], label='offset')
fig3_ax2a.plot(tt, hdul['MONITOR-ANT-HXP-TRACK'].data['track_mode_actual_position'][:,0], label='track_mode_actual_position')
fig3_ax2a.legend()

fig3_ax3a = fig3.add_subplot(3, 1, 3)
fig3_ax3a.set_ylabel('TZ position [deg]')
fig3_ax3a.grid(True, which="both", alpha=grid_opacity)
fig3_ax3a.plot(tt, hdul['MONITOR-ANT-HXP'].data['pos_desired'][:,TZ], label='pos_desired')
fig3_ax3a.plot(tt, hdul['MONITOR-ANT-HXP'].data['pos_actual'][:,TZ], label='pos_actual')
fig3_ax3a.plot(tt, hdul['MONITOR-ANT-HXP'].data['offset'][:,TZ], label='offset')
fig3_ax3a.plot(tt, hdul['MONITOR-ANT-HXP-TRACK'].data['track_mode_actual_position'][:,0], label='track_mode_actual_position')
fig3_ax3a.set_xlabel('Time [sec] after start')
fig3_ax3a.legend()

# Close the file
fid.close()

plt.show()
