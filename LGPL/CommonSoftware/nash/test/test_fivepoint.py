# Project: TCS - Thai National Radio Telescope
# Company: NARIT - National Astronomical Research Institute of Thailand
# Author: Spiro Sarris
# Date: 2020.09.28

delay_until_scheduled_start = 5

project_id = "no_projid"
obs_id = "no_obsid"
scan_id = 8
priority = 0
start_mjd = (Time.now() + delay_until_scheduled_start * units.second).mjd
source_name = "no_source"
line_name = "no_line"

arm_length = 7200
time_per_point = 1.5
win_min = 1000
win_max = 1000
double_cross = True

schedule_params = ScheduleParams(
    project_id, obs_id, scan_id, priority, start_mjd, source_name, line_name
)
scantype_params = ScantypeParamsFivePoint(arm_length, time_per_point, win_min, win_max)


# Create a ScanFivePoint object with each type of TrackCoord

# Common Parameters
tracking_offset_az = 0.8
tracking_offset_el = 0.3
elevation_min = 15
elevation_max = 85
north_crossing = True
use_horizontal_tables = False
max_tracking_errors = 6
tracking_common = ScanMod.TrackingParamsCommon(
    tracking_offset_az,
    tracking_offset_el,
    elevation_min,
    elevation_max,
    north_crossing,
    use_horizontal_tables,
    max_tracking_errors,
)

az = -50
el = 10
tracking_params_HO_min = TrackingParamsHO(az, el)
tracking_params_HO_full = TrackingParamsHO(
    az,
    el,
    tracking_offset_az=0,
    tracking_offset_el=0,
    elevation_min=15,
    elevation_max=85,
    north_crossing=True,
    use_azel_tables=False,
    max_tracking_errors=6,
)

ra_catalog = 350
dec_catalog = 60
pm_ra = 0
pm_dec = 0
parallax = 0
radial_velocity = 10.0
tracking_params_EQ_min = TrackingParamsEQ(
    ra_catalog, dec_catalog, pm_ra, pm_dec, parallax, radial_velocity
)
tracking_params_EQ_full = TrackingParamsEQ(
    ra_catalog,
    dec_catalog,
    pm_ra,
    pm_dec,
    parallax,
    radial_velocity,
    tracking_offset_az=0,
    tracking_offset_el=0,
    elevation_min=15,
    elevation_max=85,
    north_crossing=True,
    use_azel_tables=False,
    max_tracking_errors=6,
)

line0 = "INTELSAT 22 (IS-22)     "
line1 = "1 38098U 12011A   20070.00674396 -.00000076  00000-0  00000+0 0  9990"
line2 = "2 38098   0.0020 184.2253 0002748 169.5430 248.8790  1.00272611 29083"
tracking_params_TLE_min = TrackingParamsTLE(line0, line1, line2)
tracking_params_TLE_full = TrackingParamsTLE(
    line0,
    line1,
    line2,
    tracking_offset_az=0,
    tracking_offset_el=0,
    elevation_min=15,
    elevation_max=85,
    north_crossing=True,
    use_azel_tables=False,
    max_tracking_errors=6,
)

# Create the scan and add it to the queue
add_scan(schedule_params, scantype_params, tracking_params_HO_min)

schedule_params = ScheduleParams(
    project_id, obs_id, scan_id + 1, priority, start_mjd, source_name, line_name
)
add_scan(schedule_params, scantype_params, tracking_params_HO_full)

schedule_params = ScheduleParams(
    project_id, obs_id, scan_id + 2, priority, start_mjd, source_name, line_name
)
add_scan(schedule_params, scantype_params, tracking_params_EQ_min)

schedule_params = ScheduleParams(
    project_id, obs_id, scan_id + 3, priority, start_mjd, source_name, line_name
)
add_scan(schedule_params, scantype_params, tracking_params_EQ_full)

schedule_params = ScheduleParams(
    project_id, obs_id, scan_id + 4, priority, start_mjd, source_name, line_name
)
add_scan(schedule_params, scantype_params, tracking_params_TLE_min)

schedule_params = ScheduleParams(
    project_id, obs_id, scan_id + 5, priority, start_mjd, source_name, line_name
)
add_scan(schedule_params, scantype_params, tracking_params_TLE_full)


# Run all scans in the queue.  Receive results after queue is finished.
results = run_queue()

# Show the results of all scans from this run to the terminal window.
# All types of scans have some common results.  For example: a list of filenames
# that were recorded.
log("all results: %s" % results)
