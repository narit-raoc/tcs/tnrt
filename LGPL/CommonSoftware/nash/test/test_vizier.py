from astroquery.vizier import Vizier
from astropy import units as u
from astropy.coordinates import SkyCoord

result = Vizier.query_region("iau J0116-1136", radius=0.1 * u.deg, catalog='GSC')
print(result)
print(result['I/254/out'])

# Compare Vizier and SkyCoord

c = SkyCoord('01 16 12.522 -11 36 15.434', unit=(u.hourangle, u.deg))
print(c) 

