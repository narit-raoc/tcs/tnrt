import argparse
from argparse import RawTextHelpFormatter
import os
import sys
import numpy as np
import matplotlib

matplotlib.use("QT5agg")
import matplotlib.pyplot as plt

plt.style.use("dark_background")
from astropy.io import fits

helpstring = (
    "Expected format of numpy temporary file and FITS file: See AntennaDataFormat.py"
)

# Setup rules for command line arguments
parser = argparse.ArgumentParser(
    description="Reads FITS file written by AntennaPipeline and diplays some graphs",
    epilog=helpstring,
    formatter_class=RawTextHelpFormatter,
)
parser.add_argument(
    "filename", help="FITS file with format specified by AntennaDataFormat.py"
)

# If no command line arguments are avaiable, show help message and exit
if len(sys.argv[1:]) == 0:
    parser.print_help()
    exit()

# Read command line arguments into argparse structure
args = parser.parse_args()

hdul = None

if os.path.isfile(args.filename) is not True:
    print("ERROR: File not found")
    print("os.path.isfile(%s) = False" % args.filename)
    hdul = None
else:
    print("Processing file %s: " % args.filename)
    fid = open(args.filename, "rb")
    hdul = fits.open(fid)
    # leave the file open becuase fits.open will not store the entire file in RAM if it is
    # too big.  Close file at the end of this script.
    # print(hdul.info())
if hdul is None:
    exit()

# -----------------------------------------------------------------------------
# Make Graphs
# -----------------------------------------------------------------------------
# Strings for labels
str_title = "From File %s" % os.path.basename(args.filename)
grid_opacity = 0.2

AZ = 0
EL = 1

# Make a time axis for all the plots.  Use seconds after start
tstart = hdul["MONITOR-ANT-PACKET"].data["time_tcs_mjd"][0]
tt = (hdul["MONITOR-ANT-PACKET"].data["time_tcs_mjd"] - tstart) * 86400.0

fig0 = plt.figure("Message Sequence", figsize=(8, 8))

# Plot the message sequence number
fig0_ax1a = fig0.add_subplot(2, 1, 1)
fig0_ax1a.set_title(str_title)
fig0_ax1a.set_xlabel("array index in data file")
fig0_ax1a.set_ylabel("ACU sequence number")
fig0_ax1a.grid(True, which="both")
fig0_ax1a.plot(hdul["MONITOR-ANT-PACKET"].data["status_message_counter"], "+")

# Plot the difference in message sequence number to show if data is lost
fig0_ax2a = fig0.add_subplot(2, 1, 2)
fig0_ax2a.set_xlabel("array index in data file")
fig0_ax2a.set_ylabel("numpy.diff(sequence number)")
fig0_ax2a.grid(True, which="both")
fig0_ax2a.plot(np.diff(hdul["MONITOR-ANT-PACKET"].data["status_message_counter"]), "-b")
fig0_ax2a.plot(np.diff(hdul["MONITOR-ANT-PACKET"].data["status_message_counter"]), "r+")

# Check message sequence (no data loss)
fig1 = plt.figure("Compare Time ACU / TCS", figsize=(8, 8))

# Plot the TCS timestamp
fig1_ax1a = fig1.add_subplot(3, 1, 1)
fig1_ax1a.set_title(str_title)
fig1_ax1a.set_ylabel("TCS Timestamp [MJD]")
fig1_ax1a.grid(True, which="both")
fig1_ax1a.plot(tt, hdul["MONITOR-ANT-PACKET"].data["time_tcs_mjd"], "b-")
fig1_ax1a.plot(tt, hdul["MONITOR-ANT-PACKET"].data["time_tcs_mjd"], "r+")

# Plot the ACU timestamp
fig1_ax2a = fig1.add_subplot(3, 1, 2)
fig1_ax2a.set_ylabel("ACU Timestamp [MJD]")
fig1_ax2a.grid(True, which="both")
fig1_ax2a.plot(tt, hdul["MONITOR-ANT-GENERAL"].data["actual_time"], "-b")
fig1_ax2a.plot(tt, hdul["MONITOR-ANT-GENERAL"].data["actual_time"], "r+")
# also show DUT1 value
fig1_ax2b = fig1_ax2a.twinx()
fig1_ax2b.set_ylabel("DUT1 [ms]")
fig1_ax2b.plot(tt, hdul["MONITOR-ANT-TRACKING"].data["dut1"], label="dut1")

# Calcualte difference of time between TCS and ACU.
MILLISECONDS_PER_DAY = 86400 * 1000
deltat_tcs_acu = MILLISECONDS_PER_DAY * (
    hdul["MONITOR-ANT-PACKET"].data["time_tcs_mjd"]
    - hdul["MONITOR-ANT-GENERAL"].data["actual_time"]
)

fig1_ax3a = fig1.add_subplot(3, 1, 3)
fig1_ax3a.set_xlabel("Time [sec] after start")
fig1_ax3a.set_ylabel("Difference (TCS - ACU) [msec]")
fig1_ax3a.grid(True, which="both")

fig1_ax3a.plot(tt, deltat_tcs_acu, "-b")
fig1_ax3a.plot(tt, deltat_tcs_acu, "+r")

# AZIMUTH
fig2 = plt.figure("Azimuth Angles", figsize=(8, 8))
fig2_ax1a = fig2.add_subplot(2, 1, 1)
fig2_ax1a.set_title(str_title)
fig2_ax1a.set_xlabel("Time [sec] after start")
fig2_ax1a.set_ylabel("AZ position [deg]")
fig2_ax1a.grid(True, which="both", alpha=grid_opacity)

fig2_ax1a.plot(
    tt,
    hdul["MONITOR-ANT-AXIS"].data["actual_position"][:, AZ],
    label="actual_position",
)
fig2_ax1a.plot(
    tt,
    hdul["MONITOR-ANT-AXIS"].data["desired_position"][:, AZ],
    label="desired_position",
)
fig2_ax1a.plot(
    tt,
    hdul["MONITOR-ANT-AXIS"].data["trajectory_generator_position"][:, AZ],
    label="trajectory_generator_position",
)

fig2_ax1a.plot(
    tt, hdul["MONITOR-ANT-TRACKING"].data["prog_track_az"], label="prog_track_az"
)
fig2_ax1a.plot(tt, hdul["MONITOR-ANT-TRACKING"].data["desired_az"], label="desired_az")
fig2_ax1a.legend()

fig2_ax2a = fig2.add_subplot(2, 1, 2)
fig2_ax2a.set_xlabel("Time [sec] after start")
fig2_ax2a.set_ylabel("AZ offset [deg]")
fig2_ax2a.grid(True, which="both", alpha=grid_opacity)

fig2_ax2a.plot(
    tt,
    hdul["MONITOR-ANT-AXIS"].data["current_position_offset"][:, AZ],
    label="current_position_offset",
)
fig2_ax2a.plot(
    tt, hdul["MONITOR-ANT-TRACKING"].data["prog_offset_az"], label="prog_offset_az"
)
fig2_ax2a.plot(
    tt, hdul["MONITOR-ANT-TRACKING"].data["pos_offset_az"], label="pos_offset_az"
)
fig2_ax2a.plot(
    tt,
    hdul["MONITOR-ANT-TRACKING"].data["pointing_model_correction_az"],
    label="pointing_model_correction_az",
)
fig2_ax2a.plot(
    tt,
    hdul["MONITOR-ANT-TRACKING"].data["pointing_correction_az"],
    label="pointing_correction_az",
)
fig2_ax2a.legend()

# ELEVATION
fig3 = plt.figure("Elevation Angles", figsize=(8, 8))
fig3_ax1a = fig3.add_subplot(2, 1, 1)
fig3_ax1a.set_title(str_title)
fig3_ax1a.set_xlabel("Time [sec] after start")
fig3_ax1a.set_ylabel("EL position [deg]")
fig3_ax1a.grid(True, which="both", alpha=grid_opacity)

fig3_ax1a.plot(
    tt,
    hdul["MONITOR-ANT-AXIS"].data["actual_position"][:, EL],
    label="actual_position",
)
fig3_ax1a.plot(
    tt,
    hdul["MONITOR-ANT-AXIS"].data["desired_position"][:, EL],
    label="desired_position",
)
fig3_ax1a.plot(
    tt,
    hdul["MONITOR-ANT-AXIS"].data["trajectory_generator_position"][:, EL],
    label="trajectory_generator_position",
)

fig3_ax1a.plot(
    tt, hdul["MONITOR-ANT-TRACKING"].data["prog_track_el"], label="prog_track_el"
)
fig3_ax1a.plot(tt, hdul["MONITOR-ANT-TRACKING"].data["desired_el"], label="desired_el")
fig3_ax1a.legend()

fig3_ax2a = fig3.add_subplot(2, 1, 2)
fig3_ax2a.set_xlabel("Time [sec] after start")
fig3_ax2a.set_ylabel("EL offset [deg]")
fig3_ax2a.grid(True, which="both", alpha=grid_opacity)

fig3_ax2a.plot(
    tt,
    hdul["MONITOR-ANT-AXIS"].data["current_position_offset"][:, AZ],
    label="current_position_offset",
)
fig3_ax2a.plot(
    tt,
    hdul["MONITOR-ANT-TRACKING"].data["prog_offset_el"],
    label="prog_offset_el",
)
fig3_ax2a.plot(
    tt, hdul["MONITOR-ANT-TRACKING"].data["pos_offset_el"], label="pos_offset_el"
)
fig3_ax2a.plot(
    tt,
    hdul["MONITOR-ANT-TRACKING"].data["pointing_model_correction_el"],
    label="pointing_model_correction_el",
)
fig3_ax2a.plot(
    tt,
    hdul["MONITOR-ANT-TRACKING"].data["pointing_correction_el"],
    label="pointing_correction_el",
)
fig3_ax2a.plot(
    tt,
    hdul["MONITOR-ANT-TRACKING"].data["refraction_model_correction_el"],
    label="refraction_model_correction_el",
)
fig3_ax2a.legend()

# Close the file
fid.close()

plt.show()
