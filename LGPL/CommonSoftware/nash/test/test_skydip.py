# Clear compensation for gravity deformation
clear_hxp_el_model()

# Clear compensation for atmophere reraction
clear_refraction()

delay_until_scheduled_start = 0

project_id = "test_skydip"
obs_id = "ss"
start_mjd = (Time.now() + delay_until_scheduled_start * units.second).mjd

az = 20
el_start = 5
el_end = 85
duration = 160

schedule_params = ScheduleParams(project_id, obs_id, start_mjd=start_mjd)
scantype_params = ScantypeParamsSkydip(az, el_start, el_end, duration)

# Create the scan and add it to the queue
# Note, ObjectFactoryScan will create the TrackingParamsHO object from Skydip parameters.
# Therefore we can pass None for he tracking_params and it will be replaced with a valid
# TrackingParamsHO.
add_scan(schedule_params, scantype_params)

# Another way to add Skydip is to use python syntax for optional parameters to set
# the frontend_params or backend_params when we don't have a tracking_params object for the third parameter.
# add_scan(schedule_params, scantype_params, backend_params=backend_params)


# Run all scans in the queue.  Receive results after queue is finished.
results = run_queue()

# Show the results of all scans from this run to the terminal window.
# All types of scans have some common results.  For example: a list of filenames
# that were recorded.
log("all results: %s" % results)
