# Schedule parameters
project_id = 'no_projid'
obs_id = 'no_obsid'

# To test stop_queue when scan is scheduled but not yet run.
start_mjd = (Time.now() + 30 * units.second).mjd

# Scan type parameters.
# Typically, a manual scan we must stop manually by command stop_queue().
# We have option to set the time for the scan to automatically stop.
# If we don't set the duration, it will automatically stop after 60 minutes 
# (file size grows approximately 1 MB per minute)
duration = 30 # seconds

schedule_params = ScheduleParams(project_id, obs_id, start_mjd=start_mjd)
scantype_params = ScantypeParamsManual(duration)

# Create the scan and add it to the queue
add_scan(schedule_params, scantype_params)

# Run all scans in the queue.  Results are emtpy if we run non-blocking
results = run_queue(blocking=False)

# Do something interesting
goto_azel(0,10, blocking=False)

# Wait a few seconds for ACU to move, ...
waitrel(10, True, 2.0)

results = stop_queue()

# Make another scan, and give it 20 seconds to run before we stop it
start_mjd = (Time.now() + 0 * units.second).mjd
schedule_params = ScheduleParams(project_id, obs_id, start_mjd=start_mjd)
scantype_params = ScantypeParamsManual(duration)

# Create the scan and add it to the queue
add_scan(schedule_params, scantype_params)

# Run all scans in the queue.  Results are emtpy if we run non-blocking
results = run_queue(blocking=False)

# Do something interesting
goto_azel(0,10, blocking=True)
goto_azel(10,15, blocking=True)

results = stop_queue()
