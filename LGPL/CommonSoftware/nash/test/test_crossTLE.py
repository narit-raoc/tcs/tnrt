# Create and add a ScanOnSourceHO to the queue

# Give the scan a unique name to name the file and read results after
# Note (22/03/2020 - Spiro) The data pipeline 'ant' does not yet ues this name to create
# the file name.  coming soon ....
name = "scan1"

# Schedule the scan to start after (10 seconds).  This is to confirm that the scheduler
# works afer antenna has already slewed to start AZ/EL position and wait for scheduled start time.

# In real operations, the operator will most likely create a timestamp in ISO format.
# So, the control software must accept timee string in the ISO format: 2020-03-22T10:06:57.067

delay_until_scheduled_start = 30

project_id = "no_projid"
obs_id = "no_obsid"
scan_id = 8
priority = 0
start_mjd = (Time.now() + delay_until_scheduled_start * units.second).mjd
source_name = "no_source"
line_name = "no_line"

line0 = "INTELSAT 22 (IS-22)     "
line1 = "1 38098U 12011A   20070.00674396 -.00000076  00000-0  00000+0 0  9990"
line2 = "2 38098   0.0020 184.2253 0002748 169.5430 248.8790  1.00272611 29083"
tracking_offset_az = 0
tracking_offset_el = 0

arm_length = 7200
time_per_arm = 30.0
win_min = -1000
win_max = 1000
double_cross = True

schedule_params = ScheduleParams(
    project_id, obs_id, scan_id, priority, start_mjd, source_name, line_name
)
scantype_params = ScantypeParamsCross(
    arm_length, time_per_arm, win_min, win_max, double_cross
)
tracking_params = TrackingParamsTLE(
    line0, line1, line2, tracking_offset_az, tracking_offset_el
)


# Create the scan and add it to the queue
add_scan(schedule_params, scantype_params, tracking_params)

# Run all scans in the queue.  Receive results after queue is finished.
results = run_queue()

# Show the results of all scans from this run to the terminal window.
# All types of scans have some common results.  For example: a list of filenames
# that were recorded.
log("all results: %s" % results)
