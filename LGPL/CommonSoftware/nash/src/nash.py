# Project: TNRT - Thai National Radio Telescope
# Organization: NARIT - National Astronomical Research Institute of Thailand
# Author: Spiro Sarris
# Date: 2021.09.20

# IMPORTANT - The comments docstring format that use ``` <text> ``` are processed
# automatically by sphinx to create API documentation website.
# The sections such as Parameters, Returns , etc.. are in numpydoc format.
# numpydoc is a preprocessor that helps human-readable comments translate
# to reST (reStructuredText), which can be read by Sphinx to generate the website.
# If you add / edit docstring comments to this file, please follow the style guides
# https://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html#images
# https://numpydoc.readthedocs.io/en/latest/format.html
# Numpy Docstring Template.
# https://numpydoc.readthedocs.io/en/latest/format.html#documenting-modules

# '''
# Summary

# Parameters
# ----------
# name : type
#   description

# Returns
# -------
# name : type
#   description

# See Also
# --------
# func

# Notes
# -----
# include math equations in LaTeX format
# .. math:: X(e^{j\omega } ) = x(n)e^{ - j\omega n}

# References
# ----------

# Examples
# --------
# >>> operator('XX')

# '''

## Enough COMMENTS ...  Begin DOCSTRING ! ###
"""

Updated |today|

"""

# from Python PyPi, etc.
import os
import sys
import logging
import threading
import coloredlogs
import datetime
import time
import datetime
import numpy as np
import json
import IPython
import atexit
import socket
import subprocess
from pathlib import Path
from IPython.terminal.prompts import Prompts, Token
from astropy import units as units
from astropy.coordinates import EarthLocation
from astropy.time import Time
from astropy.utils import iers
from astropy.coordinates import SkyCoord
from logging import DEBUG, INFO, WARNING, ERROR, CRITICAL

# import cdbConfig as cdb_config

# Modules from TNRT project
import ScanError
import ScanErrorImpl
import ScanMod
import tnrtAntennaMod
import tnrtAntennaError
import TypeConverterNash as tc
from ParameterBlocks import ScheduleParams
from ParameterBlocks import TrackingParamsEQ
from ParameterBlocks import TrackingParamsHO
from ParameterBlocks import TrackingParamsTLE
from ParameterBlocks import TrackingParamsSS
from ParameterBlocks import ScantypeParamsCalColdload
from ParameterBlocks import ScantypeParamsCalHotload
from ParameterBlocks import ScantypeParamsCalNoise
from ParameterBlocks import ScantypeParamsCross
from ParameterBlocks import ScantypeParamsFivePoint
from ParameterBlocks import ScantypeParamsFocus
from ParameterBlocks import ScantypeParamsManual
from ParameterBlocks import ScantypeParamsOnSource
from ParameterBlocks import ScantypeParamsRaster
from ParameterBlocks import ScantypeParamsSkydip
from ParameterBlocks import ScantypeParamsTimeSync
from ParameterBlocks import ScantypeParamsMap
from ParameterBlocks import FrontendParamsL
from ParameterBlocks import FrontendParamsK
from ParameterBlocks import FrontendParamsCX
from ParameterBlocks import FrontendParamsKu
from ParameterBlocks import FrontendParamsOptical
from ParameterBlocks import BackendParamsEDD
from ParameterBlocks import BackendParamsHoloFFT
from ParameterBlocks import BackendParamsHoloSDR
from ParameterBlocks import BackendParamsSKARAB
from ParameterBlocks import BackendParamsOptical
from ParameterBlocks import BackendParamsPowerMeter
from ParameterBlocks import DataParams
from tnrtAntennaMod import AZ as AZ
from tnrtAntennaMod import EL as EL
from tnrtAntennaMod import PO as PO
from tnrtAntennaMod import TR as TR
from tnrtAntennaMod import HXP as HXP
from tnrtAntennaMod import M3 as M3
from tnrtAntennaMod import M3R as M3R
from tnrtAntennaMod import M4A as M4A
from tnrtAntennaMod import M4B as M4B
from tnrtAntennaMod import VX as VX
from tnrtAntennaMod import THU as THU
from tnrtAntennaMod import GRS as GRS
from tnrtAntennaMod import hxp_x as hxp_x
from tnrtAntennaMod import hxp_y as hxp_y
from tnrtAntennaMod import hxp_z as hxp_z
from tnrtAntennaMod import hxp_tx as hxp_tx
from tnrtAntennaMod import hxp_ty as hxp_ty
from tnrtAntennaMod import hxp_tz as hxp_tz

# Modules for read catalog
import pandas

# from astroquery.simbad import Simbad
from astropy import units as u
from astropy.coordinates import SkyCoord

# Create a custom log level that is higher than CRITICAL (50) to silence all
# log output.
SILENT = CRITICAL + 10

session = None
"""docstring for session object"""


class Session:
    """
    Attributes
    ----------
    catalog : :py:class:`~Catalog`

    scan : :py:class:`~Scan`  Wrapper class to access ACS Component Scan

    antenna : :py:class:`~Antenna` Wrapper class to access ACS Component tnrtAntenna

    results : :py:class:`dict`  Dictionary of results returned by the last :py:func:`~Scan.run_queue()`
    """

    def __init__(self, name):
        """
        Docstring for __init__
        """
        self.name = name

        self.catalog = None
        self.scan = None
        self.antenna = None
        self.results = None

        # Create a new logger for this class that is a child of logger named 'nash'
        self.logger = logging.getLogger("nash." + self.__class__.__name__)
        self.logger.propagate = True
        self.logger.handlers = []
        self.logger.debug('Started log for object name "%s"' % self.name)

        # A list of components to disconnect when we exit. "remote garbage collection"
        self.connected_components = []

    def _initialize(self):
        # Initialize Catalog object
        self.catalog = Catalog()

        self._check_cdb_connection()

        # Update data from IERS
        self._check_IERS()

        # Classes get reference and activate components in the ACS Component Database.
        # Therefore, the name to attach the nash wrapper must be the same as the
        # name in ACS CDB

        try:
            self.scan = Scan("Scan")
            if self.scan.objref is not None:
                self.logger.debug("append connected component: {}".format(self.scan.objref))
                self.connected_components.append(self.scan)
        except:
            self.scan = None

        try:
            self.antenna = Antenna("AntennaTNRT")
            self.logger.debug(
                "Created nash.Antenna object: {}, objref: {}".format(
                    self.antenna, self.antenna.objref
                )
            )
            if self.antenna.objref is not None:
                self.logger.debug("append connected component: {}".format(self.antenna.objref))
                self.connected_components.append(self.antenna)
        except:
            self.antenna = None

        self.antenna.boot()

        # Note (SS 07/2022): the clock source must be selected in boot() function
        # before we can check time sync
        self._check_time_sync()

    def _check_cdb_connection(self):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.settimeout(2)
        try:
            HOST = os.getenv('DB_HOST')
            PORT = int(os.getenv('DB_PORT'))

            sock.connect((HOST, PORT))
            self.logger.info('CDB is up and running...')
            self.logger.info('Grafana Monitor is up and running...')
        except Exception as e:
            self.logger.info('Starting CDB...')
            self.logger.info('Starting Grafana Monitor...')

            # Define the directories where docker-compose.yaml files are located
            cdb_dir = os.path.join(os.environ["INTROOT"], 'config', 'CDB')
            grafana_dir = os.path.join(os.environ["INTROOT"], 'grafana')

            # Run docker-compose up in each directory
            subprocess.call(['docker', 'compose', 'up', '-d'], cwd=cdb_dir)
            subprocess.call(['docker', 'compose', 'up', '-d'], cwd=grafana_dir)

            self.logger.info('CDB is up and running')
            self.logger.info('Grafana Monitor is up and running')
        
        sock.close()

    def __del__(self):
        """
        Docstring for __del__
        """
        self.logger.debug("Deleting object %s" % (self.name))

    def _check_time_sync(self):
        # Check time synchronization between ACU and TCS clocks.
        if (self.scan.objref is not None) and (self.antenna.objref is not None):
            start_mjd = Time.now().mjd
            duration = 5.0

            # Generate the scan, add it to the queue
            schedule_params = ScheduleParams("startup", "nash", start_mjd=start_mjd)
            scantype_params = ScantypeParamsTimeSync(duration)
            self.scan.add_scan(schedule_params, scantype_params)

            self.logger.info("Check time sync between ACU and TCS for %.2f [sec]" % duration)
            self.results = self.scan.run_queue()

            # Choose a time sync tolerance.  how small do we reuire?
            time_sync_tolerance = 0.2
            # Get scan number of recent added scan
            try:
                self.logger.debug("results: {}".format(self.results))
                # Get dictionary of results for the last scan.
                # If remote Scan controller crashes and fails to return any result, this will
                # raise a KeyError searching for an integer key
                results_last_scan = self.results[self.scan.get_scan_id_lastrun()]
                
                # Get the result of interest from the last file.  
                # If result is not found, python raises KeyError searching for 
                # a string key "delta_acu_tcs_median" 
                syncerror = results_last_scan["delta_acu_tcs_median"]
                if np.abs(syncerror) < time_sync_tolerance:
                    self.logger.info(
                        "Time sync check SUCCESS. median timestamp ACU-TCS = %f [s] < tolerance %f [sec]"
                        % (syncerror, time_sync_tolerance)
                    )
                else:
                    self.logger.warning(
                        "WARNING!!! Median timestamp ACU-TCS = %f [s] > tolerance %f [sec]"
                        % (syncerror, time_sync_tolerance)
                    )
                    self.logger.warning(
                        "Scans can run, but ACU motor control clock is offset from TCS scan and data record schedule"
                    )
                    self.logger.log(
                        logging.NOTICE,
                        "Add [%f] seconds offset to ACU clock and force sync with TCS computer? [Y ?]",
                        -1 * syncerror,
                    )
                    if sys.version_info[0] < 3:
                        x = raw_input(":")
                    else:
                        x = input(":")

                    if x.lower() == "y":
                        self.logger.info(
                            "Adding %f seconds offset to ACU clock and repeat check_time_sync"
                            % (-1 * syncerror)
                        )
                        self.antenna.objref.timeOffset(tnrtAntennaMod.REL_OFFSET, (-1 * syncerror))
                        # run check_time_sync again to confirm that the offset worked and show the new syncerror.
                        self._check_time_sync()
                    else:
                        self.logger.debug("ACU clock not changed")
            except Exception as e:
                msg = "{}: {}".format(e.__class__.__name__, e)
                self.logger.error(msg)
                self.logger.error("Scan Controller and Antenna connected, but result not cacluated from .mbfits file")
                self.logger.error("Either .mbfits file was not written or an error occurred in ScanC while calculating result")
                self.logger.error("Please exit nash, restart containers DataC, ScanC, and try again.")
                
        else:
            self.logger.warning(
                "Cannot check time sync ACU-TCS.  Antenna, ACU, or Scan is not active"
            )

    def _check_IERS(self):
        """
        Read new data from IERS (International Earth Rotation Service)
        https://datacenter.iers.org/data/9/finals2000A.all
        or
        ftp://cddis.gsfc.nasa.gov/pub/products/iers/finals2000A.all
        """
        self.logger.info("Check Earth Rotation data (DUT1) in IERS cache")
        self.logger.info("If cache data is older than 10 days, auto download new data (~1 minute)")

        iers_table = iers.IERS_Auto.open()
        ut1_utc = iers_table.ut1_utc(Time.now().jd)  # sec
        (pm_x, pm_y) = iers_table.pm_xy(Time.now().jd)  # arcsec
        self.logger.info("DUT1 (UT1-UTC) = %f [sec]" % ut1_utc.value)
        self.logger.info("Polar motion (pm_x, pm_y) = (%f, %f) [arcsec]" % (pm_x.value, pm_y.value))
        self.logger.debug("Note: Polar motion not used by ACU tracking RADEC")

    def _handler_release_all(self):
        """Disconnect all components nicely"""
        # For some reason, all axes deactivate automatically when we disconnect the remote control client
        # .. but HXP does not deactivate automatically.  So, deactivat now before we disconnect
        deactivate("hxp")
        for c in self.connected_components:
            try:
                c._release()
            except:
                self.logger.error(
                    "error while disconnecting component %s" % c.__class__.__name__
                )
                raise

    @classmethod
    def _configure_logger(cls, logger):
        """
        Configure Python logger, colors, screen handler, file handler.
        This is a class method because it does not create or modify data within
        an instance of Session.  It only modifies the handlers of the logger.

        Parameters
        ----------
        logger : logging.logger
                reference to logger object

        Examples
        --------
        >>> _configure_logger(logging.getLogger('myname'), 'filename.log')
        """
        # Configure logger
        log_dir = os.getenv("HOME") + "/log"
        if not os.path.exists(log_dir):
            os.mkdir(log_dir)

        logfilename_base = datetime.datetime.utcnow().strftime("%Y%m%d_%H%M%S")
        logfilename = log_dir + "/nash_" + logfilename_base + ".txt"

        logger.setLevel(logging.DEBUG)
        fmt_file = "%(asctime)s [%(levelname)s] %(module)s.%(name)s.%(funcName)s() line %(lineno)s: %(message)s"
        fmt_scrn = "%(asctime)s [%(levelname)s] %(module)s.%(name)s.%(funcName)s(): %(message)s"
        # add %(msecs)03d to fmt_scrn to display milliseconds in ColoredFormatter

        level_styles_scrn = {
            "critical": {"color": "red", "bold": True},
            "debug": {"color": "white", "faint": True},
            "error": {"color": "red", "bright": True},
            "info": {"color": "green", "bright": True},
            "notice": {"color": "magenta", "bright": True, "bold": True},
            "spam": {"color": "green", "faint": True},
            "success": {"color": "green", "bold": True},
            "verbose": {"color": "blue"},
            "warning": {"color": "yellow", "bright": True, "bold": True},
        }
        field_styles_scrn = {
            "asctime": {},
            "hostname": {"color": "magenta"},
            "levelname": {"color": "cyan", "bright": True},
            "name": {"color": "blue", "bright": True},
            "programname": {"color": "cyan"},
        }
        # 'color': 'green'
        formatter_file = logging.Formatter(fmt_file)
        formatter_screen = coloredlogs.ColoredFormatter(
            fmt=fmt_scrn, level_styles=level_styles_scrn, field_styles=field_styles_scrn
        )

        # creating a handler to log on the filesystem
        handler_file = logging.FileHandler(filename=logfilename, mode="a")
        handler_file.setFormatter(formatter_file)
        handler_file.setLevel(logging.DEBUG)

        # creating a handler to log on the console
        handler_screen = logging.StreamHandler()
        handler_screen.setFormatter(formatter_screen)
        handler_screen.setLevel(logging.DEBUG)

        # Remove ACS log handlers so I can add new with color style for nash
        logger.handlers = []

        # adding handlers
        logger.addHandler(handler_screen)
        logger.addHandler(handler_file)

    @classmethod
    def _explain(cls, raisedException, logger):
        """Private method. Should only be called when the ACU sends an
        acknowledgement error"""

        # Cycle through all exception trace and show me the data at the first one
        # We discard the rest of the error trace since only the developer wants to see it.
        eripped = raisedException.errorTrace.errorTrace
        if eripped is not None:
            while True:
                try:
                    logger.error(
                        "Error Type %s, Code %s: %s"
                        % (
                            eripped.errorType,
                            eripped.errorCode,
                            eripped.shortDescription,
                        )
                    )
                    logger.error("Data: %s" % eripped.data)
                except Exception as e:
                    logger.error("Unable to parse the exception")
                if len(eripped.previousError) == 0:
                    break
                else:
                    eripped = eripped.previousError[0]
        # Complete information for the developper.
        logger.debug("raisedException: %s" % raisedException)

    def run(self, filename):
        """
        Run a script.  Script can contain any valid Python code and use the module
        Nash which gives access to control the TNRT telescope.

        Namespace for scripts is the same as interactive iPython namespace

        After script is finished, results from :py:func:`~Scan.run_queue` are available in the local python interactive
        session as a python dictionary named `results`.  You can use these results to find the file name or other
        results indexed by scan number.

        Parameters
        ----------
        filename : string
                path to script file

        Notes
        -----
        run(<script.py>) uses the IPython magic function %run.

        References
        ----------

        Examples
        --------
        >>> run('example.py')

        >>> run('~/tnrt/example.py')

        >>> run('home/user/tnrt/test_functions.py')
        In [5]: run('/home/ssarris/tnrt/LGPL/Tools/nash/test/test_functions.py')
        2020-05-15 14:14:36 [INFO] nash.Session.log(): Run script /home/ssarris/tnrt/LGPL/Tools/nash/test/test_functions.py:
        ...
        2020-05-15 14:26:28 [DEBUG] nash.Session.log(): Script finished. check results
        2020-05-15 14:26:28 [INFO] nash.Session.log(): results: {2: {u'filename': u'/home/ssarris/data/20200515_071841.mbfits'}, 3: {u'filename': u'/home/ssarris/data/20200515_071919.mbfits'}, 4: {u'filename': u'/home/ssarris/data/20200515_072323.mbfits'}, 5: {u'filename': u'/home/ssarris/data/20200515_072359.mbfits'}}

        >>> print(results)
        {2: {u'filename': u'/home/ssarris/data/20200515_071841.mbfits'}, 3: {u'filename': u'/home/ssarris/data/20200515_071919.mbfits'}, 4: {u'filename': u'/home/ssarris/data/20200515_072323.mbfits'}, 5: {u'filename': u'/home/ssarris/data/20200515_072359.mbfits'}}

        """
        self.log("Run script %s: " % filename)
        if os.path.isfile(filename):
            try:
                IPython.get_ipython().run_line_magic("run","-i {}".format(filename))
            except:
                self.logger.log("Exception. file not found?", ERROR)

            self.log("Script finished. check results", DEBUG)
            try:
                # Save the results to the session
                self.results = results
                # Use json to format the dictionary into a string with separate lines and tabs
                self.log("results: %s" % results, INFO)
            except:
                self.log("results not set", WARNING)
        else:
            self.log("File not found: {}".format(filename), ERROR)
            currentPath = os.getcwd()
            self.logger.error("Please make sure you enter the correct filename and path")
            self.logger.warning("You are now in {}".format(currentPath))

            pathList = list(Path.home().rglob(filename))
            if len(pathList) > 0:
                self.logger.warning(
                    "We found your selected file on these path, please use the full path to access the file"
                )
                for path in pathList:
                    self.logger.warning(path)

    def run_unittest(self):
        """
        Run all unit tests on an active session.  the '-i' parameter allows test_nash.py
        to access the current interactive namespace including all classes, objects, functions.
        """
        unittest_fullpath = os.getenv("INTROOT") + "/lib/python/site-packages/nash_unittest.py"
        self.logger.debug("Run unittest from path {}".format(unittest_fullpath))
        IPython.get_ipython().run_line_magic("run","-i {}".format(unittest_fullpath))

    def testlog(self):
        """
        Tests the display and logging for each log level

        Examples
        --------
        >>> testlog()
        2020-05-15 14:14:37 [DEBUG] nash.Session.testlog():     test log level = logging.DEBUG 10
        2020-05-15 14:14:37 [INFO] nash.Session.testlog():  test log level = logging.INFO: 20
        2020-05-15 14:14:37 [WARNING] nash.Session.testlog():   test log level = logging.WARNING: 30
        2020-05-15 14:14:37 [ERROR] nash.Session.testlog():     test log level = logging.ERROR: 40
        2020-05-15 14:14:37 [CRITICAL] nash.Session.testlog():  test log level = logging.CRITICAL: 50
        """
        if self.logger is not None:
            self.logger.debug("blank line")
            self.logger.debug("\ttest log level = logging.DEBUG %d" % logging.DEBUG)
            self.logger.info("\ttest log level = logging.INFO: %d" % logging.INFO)
            self.logger.warning("\ttest log level = logging.WARNING: %d" % logging.WARNING)
            self.logger.error("\ttest log level = logging.ERROR: %d" % logging.ERROR)
            self.logger.critical("\ttest log level = logging.CRITICAL: %d" % logging.CRITICAL)

    def log(self, message, level=INFO):
        """
        Log message from interactive user or script.
        Use this function log('message') instead of print('') to display
        data in consistent format and save in the log file.

        Parameters
        ----------
        message : string
                log message to display on screen and save to log file
        level : {DEBUG, INFO, WARNING, ERROR, CRITICAL, SILENT}, optional
                default=INFO

        Examples
        --------
        >>> log('my info message')
        2020-05-15 14:14:36 [INFO] nash.Session.log(): my info message

        >>> log('my debug message', DEBUG)
        2020-05-15 14:14:37 [DEBUG] nash.Session.log(): my debug message

        """
        self.logger.log(level, message)

    def set_loglevel(self, level):
        """
        Set the level of message to be displayed on the screen.  This does not affect the log file.
        Log file is configured to record all debug messages.
        Use level SILENT to supress all screen messages

        Parameters
        ----------
        level : {DEBUG, INFO, WARNING, ERROR, CRITICAL, SILENT}

        Examples
        --------
        >>> set_loglevel(WARNING)
        >>> testlog()
        2020-05-15 14:14:37 [ERROR] nash.Session.testlog():     test log level = logging.ERROR: 40
        2020-05-15 14:14:37 [CRITICAL] nash.Session.testlog():  test log level = logging.CRITICAL: 50

        """
        self.logger.parent.handlers[0].setLevel(level)

    def waitrel(self, wait_sec, verbose=False, verbose_period=10):
        """
        Wait for wait_sec seconds in the command flow. Meanwhile the shell
        is blocked until the time elapses. This command may be aborted with a CTRL-C.
        You may get status information with a periodicity setup by verbose_period.

        Parameters
        ----------
        wait_sec : float
                number of seconds to wait and block script execution
        verbose : {True, False}, optional
                Log status updates to the screen.
                default=False
        verbose_period : float, optional
                Period to use to print the status information. Units in seconds.
                default=10

        Examples
        --------
        >>> waitrel(3, True, 1)
        2020-05-15 14:14:37 [DEBUG] nash.Session.waitrel(): increments of verbsose_period: 3.00, remainder 0.00
        2020-05-15 14:14:37 [DEBUG] nash.Session.waitrel(): Wait ... countdown = 3.00 seconds
        2020-05-15 14:14:38 [DEBUG] nash.Session.waitrel(): Wait ... countdown = 2.00 seconds
        2020-05-15 14:14:39 [DEBUG] nash.Session.waitrel(): Wait ... countdown = 1.00 seconds
        2020-05-15 14:14:40 [INFO] nash.Session.waitrel(): Wait ... countdown = 0.00 seconds
        """
        if verbose:
            i = 0
            if wait_sec > verbose_period:
                nlines = int(wait_sec / verbose_period)
                remainder = wait_sec % verbose_period
                self.logger.debug(
                    "increments of verbsose_period: %.2f, remainder %.2f" % (nlines, remainder)
                )
                while i < nlines:
                    self.logger.debug(
                        "Wait ... countdown = %.2f seconds"
                        % ((nlines - i) * verbose_period + remainder)
                    )
                    time.sleep(verbose_period)
                    i = i + 1
                self.logger.info("Wait ... countdown = %.2f seconds" % (remainder))
                time.sleep(remainder)
            else:
                self.logger.info(
                    "Countdown time %.2f is less than verbose period %.2f"
                    % (wait_sec, verbose_period)
                )
                self.logger.info("Sleeping for %.2f seconds" % wait_sec)
                time.sleep(wait_sec)
        else:
            self.logger.info("Sleeping for %.2f seconds" % wait_sec)
            time.sleep(wait_sec)

    def waitabs(self, time_mjd, verbose=False, verbose_period=10):
        """
        Wait until the absolute time and block the command flow. This command can be aborted with CTRL-C.

        Parameters
        ----------
        time_mjd : float
                Absolute time scale=UTC.  format=MJD. unit = Days. Time MJD can be generated using
                Astropy.Time and Astropy.units modules.
        verbose : {True, False}, optional
                Log status updates to the screen.
                default=False
        verbose_period : float, optional
                Period to use to print the status information. Units in seconds.
                default=10

        Examples
        --------
        >>> start_mjd = (Time.now()+2*units.s).mjd
        >>> waitabs(start_mjd, True, 1)
        2020-05-15 14:14:40 [DEBUG] nash.Session.waitabs(): nowtime is 1.99 sec after endtime
        2020-05-15 14:14:40 [DEBUG] nash.Session.waitabs(): Wait 1.99 seconds
        2020-05-15 14:14:40 [DEBUG] nash.Session.waitrel(): increments of verbsose_period: 1.00, remainder 0.99
        2020-05-15 14:14:40 [DEBUG] nash.Session.waitrel(): Wait ... countdown = 1.99 seconds
        2020-05-15 14:14:41 [INFO] nash.Session.waitrel(): Wait ... countdown = 0.99 seconds

        >>> time_mjd = Time('2020-05-15T15:00:00.000').mjd
        >>> waitabs(time_mjd, True, 1.0)
        2020-05-15 14:50:56 [DEBUG] nash.Session.waitabs(): nowtime is 25743.90 sec after endtime
        2020-05-15 14:50:56 [DEBUG] nash.Session.waitabs(): Wait 25743.90 seconds
        2020-05-15 14:50:56 [DEBUG] nash.Session.waitrel(): increments of verbsose_period: 25743.00, remainder 0.90
        2020-05-15 14:50:57 [DEBUG] nash.Session.waitrel(): Wait ... countdown = 25742.90 seconds
        ...
        """
        nowtime = Time.now()
        endtime = Time(time_mjd, format="mjd")
        diff_s = (endtime - nowtime).sec

        self.logger.debug("nowtime is %.2f sec after endtime" % diff_s)

        if diff_s <= 0:
            self.logger.warning(
                "endtime has already passed %.2f seconds ago.  No wait" % (-1 * diff_s)
            )
        else:
            self.logger.debug("Wait %.2f seconds" % diff_s)
            self.waitrel(diff_s, verbose, verbose_period)

    def pause(self):
        """
        Pause script and wait for operator to press ENTER

        Examples
        --------
        >>> pause()
        2020-05-15 14:14:36 [INFO] nash.Session.pause(): Press ENTER to continue ...
        :
        """
        # Command is not the same for Python2 and Python3.
        self.logger.info("Press ENTER to continue ...")
        if sys.version_info[0] < 3:
            raw_input(":")
        else:
            input(":")


class ComponentWrapper:
    """
    A Wrapper component the creates a convenient command-line user interface to
    the actual CORBA component.  This parent class is inherited by subclasses
    to organize the user commands and documentation.

    Attributes
    ----------
    sc : :py:class:`Acspy.Clients.SimpleClient.PySimpleClient`
            client to get reference to ACS CORBA Components
    """

    def __init__(self, acs_component_name):
        self.name = acs_component_name
        self.sc = None
        self.objref = None

        # Create a new logger for this class that is a child of logger named 'nash'
        self.logger = logging.getLogger("nash." + self.__class__.__name__)
        # Set propagate = True to use the same screen and file handler as
        # the parent logger 'nash'.
        self.logger.propagate = True
        self.logger.handlers = []
        self.logger.debug("Started log for object %s" % self.name)

        # Instantiates a python client to access ACS components that are
        # already active, or activates component if not yet active.
        self.logger.debug("Starting PySimpleClient")
        self.sc = PySimpleClient()

        self.exception = None

        # Configure the PySimpleClient logger to use same format as session logger.
        Session._configure_logger(self.sc.logger)

        try:
            self.logger.info("Connecting ACS component {}".format(self.name))
            self.objref = self.sc.getComponent(self.name)
            self.logger.debug("created object reference: {}".format(self.objref))
        except Exception as e:
            self.logger.error("Cannot get component reference for %s" % self.name)
            self.objref = None
            if self.sc is not None:
                self.sc.disconnect()
                self.sc = None

    def __del__(self):
        self.logger.debug("Deleting object %s" % (self.name))

    def _release(self):
        """Release component reference before delete"""
        if self.objref is not None:
            self.logger.info("Releasing cdbObjRef %s" % self.name)
            self.sc.releaseComponent(self.name)
            self.objref = None
        else:
            self.logger.warning("%s.objref is already None" % self.__class__.__name__)

        if self.sc is not None:
            self.sc.disconnect()
            self.sc = None


class Antenna(ComponentWrapper):
    """
    A wrapper class to access the ACS Component "Antenna" from nash command line
    """

    def _release(self):
        self.logger.debug("Setting ACU control mode to LCP (Local Control Panel)")
        self.local()
        ComponentWrapper._release(self)

    def is_remote(self):
        """
        Check if the ACU is in remote control mode (RCP) or local mode (LCP)

        Returns
        -------
        is_remote : {True, False}
                `True` ACU is in Remote (RCP) control mode

                `False` ACU is in Local (LCP) control mode

        Examples
        --------
        >>> is_remote = is_remote()
        2020-05-15 14:14:42 [DEBUG] nash.Acu.is_remote(): checking facility control status if REMOTE
        2020-05-15 14:14:42 [INFO] nash.Acu.is_remote(): REMOTE control status = True
        """
        self.logger.debug("checking facility control status if REMOTE")
        try:
            bRemote = self.objref.getRemoteCtl()
        except (
            tnrtAntennaError.acuExceptionEx,
            tnrtAntennaError.unknownAcuExceptionEx,
            tnrtAntennaError.errorInAcuAcknowledgementEx,
        ) as e:
            Session._explain(ScanError.antennaExceptionEx(e), self.logger)
            self.logger.error("getRemoteCtl failed")

        self.logger.info("REMOTE control status = %s" % bRemote)
        return bRemote

    def remote(self):
        """
        Set the ACU to remote mode (RCP)

        See Also
        --------
        local

        Examples
        --------
        >>> remote()
        2020-05-15 14:10:30 [DEBUG] nash.Antenna.remote(): Setting ACU control mode to RCP (Remote Control Panel)

        """
        try:
            self.objref.setMaster(tnrtAntennaMod.REMOTE_M)
        except (
            tnrtAntennaError.acuExceptionEx,
            tnrtAntennaError.unknownAcuExceptionEx,
        ) as e:
            Session._explain(ScanError.antennaExceptionEx(e), self.logger)

    def local(self):
        """
        Set the ACU to local control mode (LCP)

        See Also
        --------
        remote

        Examples
        --------
        >>> local()
        2020-05-15 14:10:51 [DEBUG] nash.Antenna.local(): Setting ACU control mode to LCP (Local Control Panel)
        """
        try:
            self.objref.setMaster(tnrtAntennaMod.LOCAL_M)
        except (
            tnrtAntennaError.acuExceptionEx,
            tnrtAntennaError.unknownAcuExceptionEx,
        ) as e:
            Session._explain(ScanError.antennaExceptionEx(e), self.logger)

    def get_site_location(self):
        """
        Returns the site location object

        Returns
        -------
        loc : astropy.coordinates.EarthLocation

        Examples
        --------
        >>> l = get_site_location()
        >>> log(l)
        >>> log('site location geodetic spherical coordinates:')
        >>> log('longitude [deg]: %f, latitude [deg]: %f, height [m, WGS84]: %f' % (l.lon.deg, l.lat.deg, l.height.value))
        2020-05-15 14:14:42 [INFO] nash.Session.log(): (-967117.38507666, 5960086.26180619, 2049276.76718827)
        2020-05-15 14:14:42 [INFO] nash.Session.log(): site location geodetic spherical coordinates:
        2020-05-15 14:14:42 [INFO] nash.Session.log(): longitude [deg]: 99.216805, latitude [deg]: 18.864348, height [m, WGS84]: 403.625000
        """
        loc_idl = self.objref.getSiteLocation()
        loc_apy = EarthLocation(
            EarthLocation.from_geodetic(
                lon=loc_idl.lon * units.deg,
                lat=loc_idl.lat * units.deg,
                height=loc_idl.height * units.m,
                ellipsoid="WGS84",
            )
        )
        return loc_apy

    def activate(self, axis="az"):
        """
        Activate the specified motor axis

        Parameters
        ----------
        axis : {'az', 'el', 'hxp', 'm3', 'm3r', 'm4a', 'm4b', 'thu', 'grs'}
                'az' : Azimuth motors

                'el' : Elevation motors

                'hxp' : subreflector motors (and EL offset dependency)

                'm3': primary Nasmyth elevation tracking motors

                'm3r': primary Nasmyth redirection motors (point to M4A or M4B)

                'm4a': secondary Nasmyth redirection drive motors (M4A)

                'm4b': secondary Nasmyth redirection drive motors (M4B)

                'thu': THU primary / secondary redirection motors

                'grs': gravity slide motors

        See Also
        --------
        deactivate, stop, reset

        Examples
        --------
        >>> activate('az')
        2023-01-18 10:09:09 [INFO] nash.Antenna.activate(): activating AZIMUTH
        >>> activate('el')
        2023-01-18 10:09:12 [INFO] nash.Antenna.activate(): activating ELEVATION
        >>> activate('m3')
        2023-01-18 10:09:14 [INFO] nash.Antenna.activate(): Synchronize M3 with EL
        2023-01-18 10:09:14 [INFO] nash.Antenna.activate(): activating M3 Nasmyth Mirror
        >>> activate('m3r')
        2023-01-18 10:09:16 [INFO] nash.Antenna.activate(): activating M3R Nasmyth Redirect (steer to M4A or M4B)
        >>> activate('m4a')
        2023-01-18 10:09:19 [INFO] nash.Antenna.activate(): activating M4A
        >>> activate('m4b')
        2023-01-18 10:09:21 [INFO] nash.Antenna.activate(): activating M4B
        >>> activate('thu')
        2023-01-18 10:09:23 [INFO] nash.Antenna.activate(): activating THU Tetrapod Head Unit
        >>> activate('grs')
        2023-01-18 10:09:26 [INFO] nash.Antenna.activate(): activating GRS Gravity Slider
        >>> activate('hxp')
        2023-01-18 10:09:28 [INFO] nash.Antenna.activate(): Enable M2 HEXAPOD Elevation offset table
        2023-01-18 10:09:28 [INFO] nash.Antenna.activate(): activating M2 HEXAPOD
        """
        # Handle special case of HXP. enable HXP vs. EL lookup table before activating HXP
        if axis.upper() == "HXP":
            self.logger.info("Enable M2 HEXAPOD Elevation offset table")
            try:
                self.objref.setHexapodElevationOffsetTableEnabled(tnrtAntennaMod.EL_TABLE_ON)
            except (
                tnrtAntennaError.acuExceptionEx,
                tnrtAntennaError.unknownAcuExceptionEx,
                tnrtAntennaError.errorInAcuAcknowledgementEx,
            ) as e:
                Session._explain(ScanError.antennaExceptionEx(e))

        # Activate axis
        try:
            self.logger.info("activating " + tc.axis_properties[axis.upper()]["info"])
            self.objref.activate(tc.axis_properties[axis.upper()]["idl_enum"])
        except KeyError:
            self.logger.error(
                "unknown axis: %s\n choose from %s" % (axis, tc.axis_properties.keys())
            )
            return
        except (
            tnrtAntennaError.acuExceptionEx,
            tnrtAntennaError.unknownAcuExceptionEx,
            tnrtAntennaError.errorInAcuAcknowledgementEx,
        ) as e:
            Session._explain(ScanError.antennaExceptionEx(e), self.logger)

        # Handle special case of M3.  enable sync EL *after* active.
        # NOTE (SS. 01/2023): M3 does not immediately move to the same angle as EL.  EL must move first, then
        # M3 will move from current position until it is tracking EL.  How do we know if M3 is ready
        # to observe (actually following EL or not arrived yet?)
        if axis.upper() == "M3":
            # Set  M3 mirror synchronize with elevation
            # If M3 is synchronized with EL, M3 it will activate automatically after EL is active
            self.logger.info("Activate Sync M3 with EL")
            self.objref.m3SyncEl(tnrtAntennaMod.ACTIVE)

        # Handle special case of HXP after active
        # NOTE (SS 01/2023): The *first* time we send HXP position offsets to ACU, 
        # they are saved in the ACU, but the HXP does not move the HXP axes immediately
        # to the offset position.  After the HXP has moved at least once, it is in a
        # good state where the position offsets immediately affect the position.
        # So, command HXP to move to the same current position (no actual
        # movement, but I think this will set the HXP axis to the desired state)
        if axis.upper() == "HXP":
            log("Send absolute position command to get HXP in a state to respond to offsets", level=DEBUG)
            current_position = self.objref.getHexapodStatus().pos_actual
            current_offset = self.objref.getHexapodStatus().offset
            linear_velocity = 2.0
            rotation_velocity = 0.04
            self.objref.absolutePositionHxp(
                current_position[0] - current_offset[0], linear_velocity,
                current_position[1] - current_offset[1], linear_velocity,
                current_position[2] - current_offset[2], linear_velocity,
                current_position[3] - current_offset[3], rotation_velocity,
                current_position[4] - current_offset[4], rotation_velocity,
                current_position[5] - current_offset[5], rotation_velocity,
            )


    def deactivate(self, axis="az"):
        """
        Deactivate the specified motor axis

        Parameters
        ----------
        axis : {'az', 'el', 'hxp', 'm3', 'm3r', 'm4a', 'm4b', 'thu', 'grs'}
                'az' : Azimuth motors

                'el' : Elevation motors

                'hxp' : subreflector motors (and EL offset dependency)

                'm3': primary Nasmyth elevation tracking motors

                'm3r': primary Nasmyth redirection motors (point to M4A or M4B)

                'm4a': secondary Nasmyth redirection drive motors (M4A)

                'm4b': secondary Nasmyth redirection drive motors (M4B)

                'thu': THU primary / secondary redirection motors

                'grs': gravity slide motors

        See Also
        --------
        activate, stop, reset

        Examples
        --------
        >>> deactivate('az')
        2023-01-18 13:51:41 [INFO] nash.Antenna.deactivate(): deactivating AZIMUTH
        >>> deactivate('el')
        2023-01-18 13:51:44 [INFO] nash.Antenna.deactivate(): deactivating ELEVATION
        >>> deactivate('m3')
        2023-01-18 13:51:46 [INFO] nash.Antenna.deactivate(): deactivating M3 Nasmyth Mirror
        2023-01-18 13:51:46 [INFO] nash.Antenna.deactivate(): Deactivate Sync M3 with EL
        >>> deactivate('m3r')
        2023-01-18 13:51:53 [INFO] nash.Antenna.deactivate(): deactivating M3R Nasmyth Redirect (steer to M4A or M4B)
        >>> deactivate('m4a')
        2023-01-18 13:51:56 [INFO] nash.Antenna.deactivate(): deactivating M4A
        >>> deactivate('m4b')
        2023-01-18 13:51:58 [INFO] nash.Antenna.deactivate(): deactivating M4B
        >>> deactivate('thu')
        2023-01-18 13:52:00 [INFO] nash.Antenna.deactivate(): deactivating THU Tetrapod Head Unit
        >>> deactivate('grs')
        2023-01-18 13:52:02 [INFO] nash.Antenna.deactivate(): deactivating GRS Gravity Slider
        >>> deactivate('hxp')
        2023-01-18 13:52:05 [INFO] nash.Antenna.deactivate(): deactivating M2 HEXAPOD
        2023-01-18 13:52:05 [INFO] nash.Antenna.deactivate(): Disable M2 HEXAPOD Elevation offset table
        """
        # Special case for M3.  Disable sync with EL before we deactivate
        if axis.upper() == "M3":
            # Set  M3 mirror synchronize with elevation
            # If M3 is synchronized with EL, M3 it will activate automatically after EL is active
            self.logger.info("Deactivate Sync M3 with EL")
            self.objref.m3SyncEl(tnrtAntennaMod.INACTIVE)

        try:
            self.logger.info("deactivating " + tc.axis_properties[axis.upper()]["info"])
            self.objref.deactivate(tc.axis_properties[axis.upper()]["idl_enum"])
        except KeyError:
            self.logger.error(
                "unknown axis: %s\n choose from %s" % (axis, tc.axis_properties.keys())
            )
            return
        except (
            tnrtAntennaError.acuExceptionEx,
            tnrtAntennaError.unknownAcuExceptionEx,
            tnrtAntennaError.errorInAcuAcknowledgementEx,
        ) as e:
            Session._explain(ScanError.antennaExceptionEx(e), self.logger)

        # Handle special case of m2 subreflector lookup table before deactivate
        if axis.upper() == "HXP":
            self.logger.info("Disable M2 HEXAPOD Elevation offset table")
            try:
                self.objref.setHexapodElevationOffsetTableEnabled(tnrtAntennaMod.EL_TABLE_OFF)
            except (
                tnrtAntennaError.acuExceptionEx,
                tnrtAntennaError.unknownAcuExceptionEx,
                tnrtAntennaError.errorInAcuAcknowledgementEx,
            ) as e:
                Session._explain(ScanError.antennaExceptionEx(e))

    def stop(self, axis="az"):
        """
        Stop the motors for the specified axis

        Parameters
        ----------
        axis : {'az', 'el', 'hxp', 'm3', 'm3r', 'm4a', 'm4b', 'thu', 'grs'}
                'az' : Azimuth motors

                'el' : Elevation motors

                'hxp' : subreflector motors (and EL offset dependency)

                'm3': primary Nasmyth elevation tracking motors

                'm3r': primary Nasmyth redirection motors (point to M4A or M4B)

                'm4a': secondary Nasmyth redirection drive motors (M4A)

                'm4b': secondary Nasmyth redirection drive motors (M4B)

                'thu': THU primary / secondary redirection motors

                'grs': gravity slide motors

        See Also
        --------
        activate, deactivate, reset

        Examples
        --------
        >>> stop('m4a')
        2020-05-15 14:14:47 [INFO] nash.Antenna.stop(): stop secondary Nasmyth redirection drive motors (M4A)
        """
        try:
            self.logger.info("stop " + tc.axis_properties[axis.upper()]["info"])
            self.objref.stop(tc.axis_properties[axis.upper()]["idl_enum"])
        except KeyError:
            self.logger.error(
                "unknown axis: %s\n choose from %s" % (axis, tc.axis_properties.keys())
            )
        except (
            tnrtAntennaError.acuExceptionEx,
            tnrtAntennaError.unknownAcuExceptionEx,
            tnrtAntennaError.errorInAcuAcknowledgementEx,
        ) as e:
            Session._explain(ScanError.antennaExceptionEx(e), self.logger)

    def reset(self, axis="m1"):
        """
        Reset the motor from the specified axis

        Parameters
        ----------
        axis : {'az', 'el', 'hxp', 'm3', 'm3r', 'm4a', 'm4b', 'thu', 'grs'}
                'az' : Azimuth motors

                'el' : Elevation motors

                'hxp' : subreflector motors (and EL offset dependency)

                'm3': primary Nasmyth elevation tracking motors

                'm3r': primary Nasmyth redirection motors (point to M4A or M4B)

                'm4a': secondary Nasmyth redirection drive motors (M4A)

                'm4b': secondary Nasmyth redirection drive motors (M4B)

                'thu': THU primary / secondary redirection motors

                'grs': gravity slide motors

        See Also
        --------
        activate, deactivate, stop

        Examples
        --------
        >>> reset('m4a')
        2020-05-15 14:14:58 [INFO] nash.Antenna.reset(): reset secondary Nasmyth redirection drive motors (M4A)
        """
        try:
            self.logger.info("reset " + tc.axis_properties[axis.upper()]["info"])
            self.objref.reset(tc.axis_properties[axis.upper()]["idl_enum"])
        except KeyError:
            self.logger.error(
                "unknown axis: %s\n choose from %s" % (axis, tc.axis_properties.keys())
            )
        except (
            tnrtAntennaError.acuExceptionEx,
            tnrtAntennaError.unknownAcuExceptionEx,
            tnrtAntennaError.errorInAcuAcknowledgementEx,
        ) as e:
            Session._explain(ScanError.antennaExceptionEx(e), self.logger)

    def _log_acu_ack(self, ack):
        if (ack == tnrtAntennaMod.ACCEPTED) or (ack == tnrtAntennaMod.DONE):
            self.logger.log(DEBUG, ack)
        else:
            self.logger.log(
                WARNING,
                "ACU response: {}. Check LCP or ACU status message and confirm if required settings are OK to continue".format(
                    ack
                ),
            )

    def boot(self):
        """
        Initialize data tables, time settings, and motor axes in ACU.
        After boot finished.  All motor axes are active.
        Takes time apprximately 30 seconds.

        Examples
        --------
        >>> boot()
        2020-05-15 14:11:57 [INFO] nash.Antenna.boot(): Boot ACU / Antenna - initialize motor axes, time, tracking settings
        2020-05-15 14:11:57 [WARNING] nash.Antenna.boot(): This function will block until Antenna is ready (approximately 30 seconds)
        """
        self.logger.info("Initialize ACU axes, offsets, data tables ...")

        # Set remote control mode.  We don't know the state before, so set to LCP
        # first.  This will deactivate and clear some state in ACU.  Then we can
        # enable remote control and activate subsystems step-by-step
        ack = self.objref.setMaster(tnrtAntennaMod.LOCAL_M)
        time.sleep(2)
        self._log_acu_ack(ack)

        self.logger.debug("Set ACU control mode to RCP (Remote Control Panel)")
        ack = self.objref.setMaster(tnrtAntennaMod.REMOTE_M)
        time.sleep(2)
        self._log_acu_ack(ack)

        # Set time source.  If system is using Simulator mode, set INTERNAL time (Use Windows OS clock).
        # Else, use the NTP server clock source that should be available to the real ACU at the site.
        if self.objref.getSimulatorEnabled() is True:
            self.logger.warning("Simulator enabled = True.  Use time source internal OS clock")
            ack = self.objref.timeSource(tnrtAntennaMod.INTERNAL, 0)
            self._log_acu_ack(ack)
        else:
            self.logger.debug("Simulator enabled = False.  Use time source NTP clock")
            ack = self.objref.timeSource(tnrtAntennaMod.NTP, 0)
            self._log_acu_ack(ack)

        # Unstow ALL
        # self.logger.info("Unstow all axes that have stow pins [AZ, EL, THU, GRS]")
        # TODO fix unstow later when it actually works.
        # self.objref.unstow_all()

        # Activate main drive axes
        self.logger.debug("Reset errors AZ")
        ack = self.objref.reset(AZ)
        self._log_acu_ack(ack)

        self.logger.debug("Activate AZ")
        ack = self.objref.activate(AZ)
        self._log_acu_ack(ack)

        self.logger.debug("Reset errors EL")
        ack = self.objref.reset(EL)
        self._log_acu_ack(ack)

        self.logger.debug("Disable sync between M3 and EL")
        ack = self.objref.m3SyncEl(tnrtAntennaMod.INACTIVE)
        self._log_acu_ack(ack)
        self.logger.warning(
            "NOTE: To use K-band, the script must activate('m3') and activate('hxp')"
        )

        self.logger.debug("Activate EL")
        ack = self.objref.activate(EL)
        self._log_acu_ack(ack)

        # Set Pointing model from CDB

        # self.logger.debug("Loading pointing model from file")
        # ack = self.objref.setPointingModel(self.__pointingParams)

        # Reset M2 ----------------------------------------------------------------------------
        # self.logger.debug("Reset errors of HXP (M2)")
        # ack = self.objref.reset(HXP)

        # Activate M2 Load subreflector table and reset focus -------------------------------------------------
        # self.logger.debug("Loading subreflector table, and activating HXP (M2)")
        # try:
        #     self.setHexapodElevationOffsetModel(tnrtAntennaMod.geoM2ElMode)
        # except (
        #     tnrtAntennaErrorImpl.unknownAcuExceptionExImpl,
        #     tnrtAntennaErrorImpl.acuExceptionExImpl,
        #     tnrtAntennaErrorImpl.errorInAcuAcknowledgementExImpl,
        # ) as e:
        #     raise tnrtAntennaErrorImpl.antennaBootExImpl(exception=e)
        # except Exception as e:
        #     raise tnrtAntennaErrorImpl.antennaBootExImpl(
        #         ACSErr.NameValue("errExpl", "other exception during load Hexapod table")
        #     )

        # TODO (SS -05/2022). Activate these other axes when we are ready to use K-band.
        # Not used for L-band.

        # self.logger.debug("tnrtAntennaMod.boot() M3R M4A M4B")

        # self.logger.debug("logACU: tnrtAntenna40m. Activate M3R")
        # ack = self.objref.activate(M3R)

        # self.logger.debug("logACU: tnrtAntenna40m. Activate M4A")
        # ack = self.objref.activate(M4A)

        # self.logger.debug("logACU: tnrtAntenna40m. Activate M4B")
        # ack = self.objref.activate(M4B)

        # self.logger.debug("Reset errors and activate THU")
        # ack = self.objref.reset(THU)
        # ack = self.objref.activate(THU)

        # self.logger.debug("Reset errors and activate GRS")
        # ack = self.objref.reset(GRS)
        # ack = self.objref.activate(GRS)

        # Clear offsets
        self.logger.debug("clear tracking offset TIME")
        ack = self.objref.trackingOffsetTime(0.0)
        self._log_acu_ack(ack)

        self.logger.debug("clear user pointing correction AZ")
        ack = self.objref.setUserPointingCorrectionAz(0.0)
        self._log_acu_ack(ack)

        self.logger.debug("clear user pointing correction EL")
        ack = self.objref.setUserPointingCorrectionEl(0.0)
        self._log_acu_ack(ack)

        self.logger.debug("clear persistent position offset AZ")
        ack = self.objref.positionOffset(AZ, 0.0)
        self._log_acu_ack(ack)

        self.logger.debug("clear persistent position offset EL")
        ack = self.objref.positionOffset(EL, 0.0)
        self._log_acu_ack(ack)

        # self.logger.info("clear persistent position offset M3")
        # ack = self.objref.positionOffset(M3, 0.0)

        # self.logger.info("clear persistent position offset HXP-X")
        # ack = self.objref.hexapodPositionOffset(tnrtAntennaMod.POS_OFF_X, 0.0)

        # self.logger.debug("clear persistent position offset HXP-Y")
        # ack = self.objref.hexapodPositionOffset(tnrtAntennaMod.POS_OFF_Y, 0.0)

        # self.logger.debug("clear persistent position offset HXP-Z")
        # ack = self.objref.hexapodPositionOffset(tnrtAntennaMod.POS_OFF_Z, 0.0)

        # self.logger.debug("clear persistent position offset HXP-TX")
        # ack = self.objref.hexapodPositionOffset(tnrtAntennaMod.POS_OFF_TX, 0.0)

        # self.logger.debug("clear persistent position offset HXP-TY")
        # ack = self.objref.hexapodPositionOffset(tnrtAntennaMod.POS_OFF_TY, 0.0)

        # self.logger.debug("clear persistent position offset HXP-TZ")
        # ack = self.objref.hexapodPositionOffset(tnrtAntennaMod.POS_OFF_TZ, 0.0)

        self.logger.info("ACU startup sequence finished")

    def goto_azel(self, az, el, vel_az=2.0, vel_el=1.0, blocking=True, update_period=5.0):
        """
        Go to Azimuth and Elevation. If AZ and EL angular velocity is not
        input to the parameters `vel_az` and `vel_el`, Antenna moves at maximum
        rate.
        *NOTE* AZ and EL motor axes must be enabled.

        Parameters
        ----------
        az : float
                command AZ position. unit = deg
        el : float
                command EL position. unit = deg
        vel_az : float, optional
                command AZ angular velocity. unit = deg/s
        vel_el : float, optional
                command EL angular velocity. unit = deg/s

        Returns
        -------
        slewtime : float
                estimated travel time to arrive at command (AZ, EL). unit = sec

        See Also
        --------
        activate

        Examples
        --------
        >>> goto_azel(-30, 15)
        2020-05-15 14:15:04 [DEBUG] nash.Antenna.goto_azel(): now_mjd: 58984.302131
        2020-05-15 14:15:04 [DEBUG] nash.Antenna.goto_azel(): Calculated slew time [s]: 20.400665
        2020-05-15 14:15:04 [INFO] nash.Antenna.goto_azel(): gotoAzEl az=-30.00, el=15.00, vel_az=2.00, vel_el=1.00

        >>> goto_azel(-30, 15, 2.0, 1.0)
        2020-05-15 14:15:04 [DEBUG] nash.Antenna.goto_azel(): now_mjd: 58984.302131
        2020-05-15 14:15:04 [DEBUG] nash.Antenna.goto_azel(): Calculated slew time [s]: 20.400665
        2020-05-15 14:15:04 [INFO] nash.Antenna.goto_azel(): gotoAzEl az=-30.00, el=15.00, vel_az=2.00, vel_el=1.00
        """
        now_mjd = Time.now().mjd
        self.logger.debug("now_mjd: %f" % now_mjd)
        try:
            slTseconds = self.objref.slewTime(az, el, tnrtAntennaMod.azel, now_mjd, 2000, 0, 0)
            self.logger.debug("Calculated slew time [s]: %f" % slTseconds)
            self.logger.info(
                "gotoAzEl az=%.2f, el=%.2f, vel_az=%.2f, vel_el=%.2f" % (az, el, vel_az, vel_el)
            )
            self.objref.gotoAzEl(az, el, vel_az, vel_el, blocking, update_period)
            return slTseconds
        except (
            tnrtAntennaError.acuExceptionEx,
            tnrtAntennaError.unknownAcuExceptionEx,
            tnrtAntennaError.errorInAcuAcknowledgementEx,
        ) as e:
            self.logger.error(e)
            Session._explain(ScanError.antennaExceptionEx(e), self.logger)

    def goto_platform(self, blocking=True, update_period=5.0):
        """
        Park the telescope at (az=45,el=5) so that the scissor lift or cherry picker
        can reach the subreflector and THU.
        Put the antenna in deactivate when the final position is reached

        See Also
        --------
        goto_azel

        Examples
        --------
        >>> goto_platform()
        2020-05-15 14:15:29 [INFO] nash.Antenna.goto_platform(): Disable and reset pointing corrections
        2020-05-15 14:15:29 [INFO] nash.Antenna.set_pmodel(): Setting pointing model parameters to [0, 0, 0, 0, 0, 0, 0, 0, 0]
        2020-05-15 14:15:30 [INFO] nash.Antenna.stop(): stop main motors. AZ, EL, M3
        2020-05-15 14:15:36 [INFO] nash.Antenna.goto_platform(): Move antenna to position to access by lift
        2020-05-15 14:15:36 [DEBUG] nash.Antenna.goto_azel(): now_mjd: 58984.302507
        2020-05-15 14:15:36 [DEBUG] nash.Antenna.goto_azel(): Calculated slew time [s]: 41.987990
        2020-05-15 14:15:36 [INFO] nash.Antenna.goto_azel(): gotoAzEl az=45.00, el=5.00, vel_az=2.00, vel_el=1.00
        2020-05-15 14:15:41 [INFO] nash.Antenna.goto_platform(): Block until antenna arrives at command position
        2020-05-15 14:15:41 [DEBUG] nash.Antenna.block_until_arrived(): Block until Antenna arrives at command AZ, EL
        2020-05-15 14:16:21 [INFO] nash.Antenna.deactivate(): deactivating main motors. AZ, EL, M3
        2020-05-15 14:16:26 [INFO] nash.Antenna.deactivate(): deactivating subreflector motors (and EL offset dependency)
        2020-05-15 14:16:28 [INFO] nash.Antenna.deactivate(): disabling subreflector lookup table mode
        """
        self.logger.info("Disable and reset pointing corrections")
        self.set_pmodel(p1=0, p2=0, p3=0, p4=0, p5=0, p6=0, p7=0, p8=0, p9=0)
        self.stop("AZ")
        self.stop("EL")
        self.logger.info(
            "Move antenna to position to access by lift. Block until Antenna arrives at position"
        )
        self.goto_azel(45, 5, 2.0, 1.0, blocking, update_period)
        try:
            self.deactivate("AZ")
            self.deactivate("EL")
            self.deactivate("HXP")
        except:
            pass

    def block_until_arrived(self, update_period=5.0):
        """
        After a command is sent to antenna to move, this function can block
        script execution until antenna arrives at command coordinate.
        Used in Scan flow control.  Typically, this will not be used in scripts.

        Parameters
        ----------
        update_period : float, optional
                log updates every <update_period> seconds.  times out at 250 seconds.
                default = 5.0 seconds

        Examples
        --------
        >>> block_until_arrived(1.0)
        2020-05-15 14:15:41 [DEBUG] nash.Antenna.block_until_arrived(): Block until Antenna arrives at command AZ, EL
        """
        self.logger.debug("Block until Antenna arrives at command AZ, EL")
        self.objref.blockUntilTracking(update_period)

    def stow(self, azrate=2.0, elrate=1.0):
        """
        Stow the telescope in the survival position with stow pins inserted.
        Use for safety during maintenance or strong wind.

        See Also
        --------
        unstow

        Examples
        --------
        >>> stow()
        2020-12-17 09:39:12 [INFO] nash.Antenna.stow(): Drive to stow position and lock the stow pins

        """
        self.logger.info("Drive to stow position and lock the stow pins for AZ, EL")
        try:
            self.objref.driveToStow(tnrtAntennaMod.AZ, tnrtAntennaMod.stowPositionNearest, azrate)
            self.objref.driveToStow(tnrtAntennaMod.EL, tnrtAntennaMod.stowPositionNearest, elrate)
        except (
            tnrtAntennaError.acuExceptionEx,
            tnrtAntennaError.unknownAcuExceptionEx,
            tnrtAntennaError.errorInAcuAcknowledgementEx,
        ) as e:
            Session._explain(ScanError.antennaExceptionEx(e), self.logger)

    def unstow(self):
        """
        Retract the stow pins and switch to INACTIVE mode. This command is
        necessary after a previous :py:func:`stow` command to prepare the
        telescope for new scans.

        See Also
        --------
        stow

        Examples
        --------
        >>> unstow()
        2020-12-17 09:39:12 [INFO] nash.Antenna.unstow(): Unstow antenna. Wait 15 seconds

        """
        self.logger.info("Unstow AZ and EL stow pins")
        try:
            self.objref.unstow(tnrtAntennaMod.AZ, tnrtAntennaMod.stowpin1)
            self.objref.unstow(tnrtAntennaMod.EL, tnrtAntennaMod.stowpinBoth)
        except (
            tnrtAntennaError.acuExceptionEx,
            tnrtAntennaError.unknownAcuExceptionEx,
            tnrtAntennaError.errorInAcuAcknowledgementEx,
        ) as e:
            Session._explain(ScanError.antennaExceptionEx(e), self.logger)

    def get_vertex_status(self):
        """
        Get vertex status

        Returns
        -------
        status : str
                status string

        See Also
        --------
        open_vertex, close_vertex

        Examples
        --------
        >>> sts = get_vertex_status()
        2020-05-15 14:16:32 [DEBUG] nash.Antenna.get_vertex_status(): checking vertex shutter status
        2020-05-15 14:16:32 [INFO] nash.Antenna.get_vertex_status(): tnrtAntennaMod.VertexShutterStatus(vertexstatus=3L, vertexelementopen=0L, vertexelementclose=255L, vertexcommandelementopen=0L, vertexcommandelementclose=0L)

        """
        status = None
        self.logger.debug("checking vertex shutter status")
        try:
            vstatus = self.objref.getVertex()
        except (
            tnrtAntennaError.acuExceptionEx,
            tnrtAntennaError.unknownAcuExceptionEx,
            tnrtAntennaError.errorInAcuAcknowledgementEx,
        ) as e:
            Session._explain(ScanError.antennaExceptionEx(e), self.logger)
        except Exception as e:
            self.logger.error("Other exception")

        self.logger.debug(status)

        return tc.vertex_status_string[vstatus.status]

    def open_vertex(self):
        """
        Open the vertex door (allow signal to enter receiver room Nasmyth system)

        See Also
        --------
        get_vertex_status, close_vertex

        Examples
        --------
        >>> open_vertex()
        2022-06-29 11:39:55 [INFO] nash.Antenna.open_vertex(): Opening the vertex shutter doors ...
        2022-06-29 11:39:55 [DEBUG] nash.Antenna.open_vertex(): Block until open.  Estimate 30 seconds,  timeout after 60 seconds"""
        self.logger.info("Opening the vertex shutter doors ... ")
        self.logger.debug("Block until open.  Estimate 30 seconds,  timeout after 60 seconds")

        try:
            self.objref.openVX()
        except (
            tnrtAntennaError.acuExceptionEx,
            tnrtAntennaError.unknownAcuExceptionEx,
            tnrtAntennaError.errorInAcuAcknowledgementEx,
        ) as e:
            Session._explain(ScanError.antennaExceptionEx(e), self.logger)

    def close_vertex(self):
        """
        Close the vertex door (use for primary focus receiver - not Nasmyth)

        See Also
        --------
        get_vertex_status, open_vertex

        Examples
        --------
        >>> close_vertex()
        2022-06-29 11:38:56 [INFO] nash.Antenna.close_vertex(): Closing the vertex shutter doors ...
        2022-06-29 11:38:56 [DEBUG] nash.Antenna.close_vertex(): Block until closed.  Estimate 30 seconds,  timeout after 60 seconds.
        """
        self.logger.info("Closing the vertex shutter doors ... ")
        self.logger.debug("Block until closed.  Estimate 30 seconds,  timeout after 60 seconds")
        try:
            self.objref.closeVX()
        except (
            tnrtAntennaError.acuExceptionEx,
            tnrtAntennaError.unknownAcuExceptionEx,
            tnrtAntennaError.errorInAcuAcknowledgementEx,
        ) as e:
            Session._explain(ScanError.antennaExceptionEx(e), self.logger)

    def get_velocity(self, axis):
        """
        Get maximum velocity for position and tracking commands.  If not set by user function :py:func:`set_velocity`,
        the default value is the maximum velocity from the ACU config file (AZ 2.0 deg/s, EL 1.0 deg/s at the time of this writing)

        Parameters
        ----------
        axis : {'az', 'el'}
                'az' : Azimuth

                'el' : Elevation
        
        Returns
        -------
        velocity : float
                
                velocity [deg / s] limit of selected `axis`

        Examples
        --------
        >>> get_velocity("az")
        """
        if axis.upper() not in ["AZ", "EL"]:
            raise ValueError('"{}" not in allowed axis list ["AZ", "EL"]'.format(axis.upper()))
        
        return(self.objref.get_velocity(tc.axis_properties[axis.upper()]["idl_enum"]))

    def set_velocity(self, axis, velocity):
        """
        Set maximum velocity for position and tracking commands.

        Parameters
        ----------
        axis : {'az', 'el'}
                'az' : Azimuth

                'el' : Elevation
        
        velocity : float
                Maximum system limits in ACU configuration parameters P108_v_MaxSys
        
        Examples
        --------
        >>> set_velocity("az", 1.0)
        """
        if axis.upper() not in ["AZ", "EL"]:
            raise ValueError('"{}" not in allowed axis list ["AZ", "EL"]'.format(axis.upper()))
        
        self.objref.set_velocity(tc.axis_properties[axis.upper()]["idl_enum"], velocity)

    def set_pmodel(self, p1=0, p2=0, p3=0, p4=0, p5=0, p6=0, p7=0, p8=0, p9=0):
        """
        Set pointing model and enable pointing model corrections while tracking

        Parameters
        ----------
        p1 : float
                Encoder offset Azimuth. unit = arcsec
        p2 : float
                Collimation error Azimuth. unit = arcsec
        p3 : float
                Non Orthogonal and Nasmyth. unit = arcsec
        p4 : float
                E-W Azimuth tilt. unit = arcsec
        p5 : float
                N-S Azimuth tilt. unit = arcsec
        p6 : float
                Declination error. unit = arcsec
        p7 : float
                Encoder offset Elevation. unit = arcsec
        p8 : float
                Cosine term. Gravity, Nasmyth. unit = arcsec
        p9 : float
                Sine term. Gravity, Nasmyth. unit = arcsec

        Examples
        --------
        >>> set_pmodel(10, 20, 30, 40, 50, 60, 70, 80, 90)
        2021-02-18 11:50:03 [INFO] nash.Antenna.set_pmodel(): Setting pointing model parameters to [10, 20, 30, 40, 50, 60, 70, 80, 90]
        2021-02-18 11:50:03 [INFO] nash.Antenna.set_pmodel(): Enable pointing model correction while tracking
        """
        pointingParams = [p1, p2, p3, p4, p5, p6, p7, p8, p9]
        self.logger.info("Setting pointing model parameters to {}".format(pointingParams))
        try:
            self.objref.setPointingModel(pointingParams)
        except (
            tnrtAntennaError.acuExceptionEx,
            tnrtAntennaError.unknownAcuExceptionEx,
            tnrtAntennaError.errorInAcuAcknowledgementEx,
        ) as e:
            Session._explain(ScanError.antennaExceptionEx(e), self.logger)

        self.logger.info("Enable pointing model correction while tracking")
        try:
            self.objref.setCorrectionValuesEnabled(
                tnrtAntennaMod.POINTING_MODEL, tnrtAntennaMod.CORRECTION_ON
            )
        except (
            tnrtAntennaError.acuExceptionEx,
            tnrtAntennaError.unknownAcuExceptionEx,
            tnrtAntennaError.errorInAcuAcknowledgementEx,
        ) as e:
            Session._explain(ScanError.antennaExceptionEx(e), self.logger)

    def clear_pmodel(self):
        """
        Clear pointing model parameters and disable pointing model corrections while tracking

        Examples
        --------
        >>> clear_pmodel()
        2021-02-18 11:50:24 [INFO] nash.Antenna.clear_pmodel(): Setting pointing model parameters to [0, 0, 0, 0, 0, 0, 0, 0, 0]
        2021-02-18 11:50:24 [INFO] nash.Antenna.clear_pmodel(): Disable pointing model correction while tracking
        """
        pointingParams = [0, 0, 0, 0, 0, 0, 0, 0, 0]
        self.logger.info("Setting pointing model parameters to {}".format(pointingParams))
        try:
            self.objref.setPointingModel(pointingParams)
        except (
            tnrtAntennaError.acuExceptionEx,
            tnrtAntennaError.unknownAcuExceptionEx,
            tnrtAntennaError.errorInAcuAcknowledgementEx,
        ) as e:
            Session._explain(ScanError.antennaExceptionEx(e), self.logger)

        self.logger.info("Disable pointing model correction while tracking")
        try:
            self.objref.setCorrectionValuesEnabled(
                tnrtAntennaMod.POINTING_MODEL, tnrtAntennaMod.CORRECTION_OFF
            )
        except (
            tnrtAntennaError.acuExceptionEx,
            tnrtAntennaError.unknownAcuExceptionEx,
            tnrtAntennaError.errorInAcuAcknowledgementEx,
        ) as e:
            Session._explain(ScanError.antennaExceptionEx(e), self.logger)

    def set_refraction(self, r0, b1, b2):
        """
        Set parameters to control elevation angle correction for atmospheric refraction.
        Enable elevation compensation for this refraction effect.

        As elevation angle goes near the horizon, the effect of atmospheric refraction is
        stronger. Therefore, the antenna must lift up slightly to compensate for refraction
        that follows the Earth atmosphere.

        Parameters
        ----------
        r0 : float
                Atmospheric refraction parameter calculated from weather conditions (pressure, temperature, relative humidity) [unit = arcsec]

                Average weather data from TNRO year 2018 r0 = 76.6
        b1 : float
                Curve fitting parameter of Bennett's equation. [unit = deg]

                Average weather data from TNRO year 2018 b1 = 5.7346
        b2 : float
                Curve fitting parameter of Bennett's equation. [unit = deg]

                Average weather data from TNRO year 2018 b2 = 2.2874

        Examples
        --------
        >>> set_refraction(76.7, 5.7346, 2.2874)
        2021-02-18 11:46:19 [INFO] nash.Antenna.set_refraction(): Set refraction parameters r0=76.7, b1=5.7346, b2=2.2874
        2021-02-18 11:46:19 [INFO] nash.Antenna.set_refraction(): Enable refraction EL correction while tracking
        """
        self.logger.info("Set refraction parameters r0={}, b1={}, b2={}".format(r0, b1, b2))
        try:
            self.objref.setRefractionCorrection(r0, b1, b2)
        except (
            tnrtAntennaError.acuExceptionEx,
            tnrtAntennaError.unknownAcuExceptionEx,
            tnrtAntennaError.errorInAcuAcknowledgementEx,
        ) as e:
            Session._explain(ScanError.antennaExceptionEx(e), self.logger)

        self.logger.info("Enable refraction EL correction while tracking")
        try:
            self.objref.setCorrectionValuesEnabled(
                tnrtAntennaMod.REFRACTION, tnrtAntennaMod.CORRECTION_ON
            )
        except (
            tnrtAntennaError.acuExceptionEx,
            tnrtAntennaError.unknownAcuExceptionEx,
            tnrtAntennaError.errorInAcuAcknowledgementEx,
        ) as e:
            Session._explain(ScanError.antennaExceptionEx(e), self.logger)

    def clear_refraction(self):
        """
        Clear atmospheric refraction corrections and disable elevation angle compensation.

        Examples
        --------
        >>> clear_refraction()
        2021-02-18 11:48:05 [INFO] nash.Antenna.clear_refraction(): Set refraction parameters r0=0, b1=0, b2=0
        2021-02-18 11:48:05 [INFO] nash.Antenna.clear_refraction(): Disable refraction EL correction while tracking
        """
        self.logger.info("Set refraction parameters r0=0, b1=0, b2=0")
        try:
            self.objref.setRefractionCorrection(0, 0, 0)
        except (
            tnrtAntennaError.acuExceptionEx,
            tnrtAntennaError.unknownAcuExceptionEx,
            tnrtAntennaError.errorInAcuAcknowledgementEx,
        ) as e:
            Session._explain(ScanError.antennaExceptionEx(e), self.logger)

        self.logger.info("Disable refraction EL correction while tracking")
        try:
            self.objref.setCorrectionValuesEnabled(
                tnrtAntennaMod.REFRACTION, tnrtAntennaMod.CORRECTION_OFF
            )
        except (
            tnrtAntennaError.acuExceptionEx,
            tnrtAntennaError.unknownAcuExceptionEx,
            tnrtAntennaError.errorInAcuAcknowledgementEx,
        ) as e:
            Session._explain(ScanError.antennaExceptionEx(e), self.logger)

    def get_status(self):
        """
        Returns the status of the main drives axis, az and el

        Returns
        -------
        status : str
                string that includes "big picture" status information Antenna component

        Examples
        --------
        >>> sts = get_status()
        2020-12-17 09:39:12 [DEBUG] nash.Antenna.get_status(): Requesting status of major axes
        2020-12-17 09:39:12 [INFO] nash.Antenna.get_status(): M1: unknown state. Current position: 0.000000, 0.000000
        2020-12-17 09:39:12 [INFO] nash.Antenna.get_status(): M2:inactive. Current position: 0.000000, -83.000000, 0.000000, 0.000000, 0.000000, Errors:   0.00   0.00   0.00   0.00   0.00

        """
        self.logger.debug("Requesting status of major axes")
        try:
            st = self.objref.antennaStatus()
            az = self.objref.getAzPosition()
            el = self.objref.getElPosition()
            st1 = self.objref.M2status()
        except (
            tnrtAntennaError.acuExceptionEx,
            tnrtAntennaError.unknownAcuExceptionEx,
            tnrtAntennaError.errorInAcuAcknowledgementEx,
        ) as e:
            Session._explain(ScanError.antennaExceptionEx(e), self.logger)

        # Information about main drive axes
        self.logger.info("M1: %s. Current position: %f, %f" % (tc.axis_status_string[st], az, el))
        # Information about mirror Hexapod / subreflector:
        self.logger.info(
            "M2:%s. Current position: %f, %f, %f, %f, %f, Errors: %6.2f %6.2f %6.2f %6.2f %6.2f\n"
            % (
                tc.axis_status_string[st1.ast],
                st1.x,
                st1.y,
                st1.z,
                st1.tx,
                st1.ty,
                st1.errx,
                st1.erry,
                st1.errz,
                st1.errtx,
                st1.errty,
            )
        )
        self.logger.info(
            "El Table offset position: %6.2f %6.2f %6.2f %6.2f %6.2f\n"
            % (st1.elx, st1.ely, st1.elz, st1.eltx, st1.elty)
        )

        return tc.axis_status_string[st]

    def set_axis_position_offsets(self, offset_az, offset_el, offset_m3=0):
        """
        NOTE: (04/2023): This function does not work correctly on ACU yet (OHB responsibility).  
        Do not use this function until ACU is fixed.

        Set axis position offsets.  persistent during lifecycle of Antenna component
        through all scans.  Program Track Offsets are added on top of this constant axis offset
        based on the parameters of each scan.  The Azimuth offset is added directly to the
        AZ axis without dependency on current EL position

        Parameters
        ----------
        offset_az : float. unit = arcsec

        offset_el : float. unit = arcsec

        offset_m3 : float. unit = arcsec, optional. default = 0

        Examples
        --------
        >>> set_axis_position_offsets(100, 200, 0)
        2020-12-08 16:35:56 [INFO] nash.Antenna.set_axis_position_offsets(): Set axis position offsets [AZ, EL, M3] = [100, 200, 0]
        """
        self.logger.info(
            "Set axis position offsets [AZ, EL, M3] = [{}, {}, {}]".format(
                offset_az, offset_el, offset_m3
            )
        )
        if self.objref.getAzActive() is True:
            self.objref.positionOffset(tnrtAntennaMod.AZ, offset_az / 3600)
        else:
            log('AZ not active.  cannot set axis position offset', level=WARNING)
        
        if self.objref.getElActive() is True:
            self.objref.positionOffset(tnrtAntennaMod.EL, offset_el / 3600)
        else:    
            log('EL not active.  cannot set axis position offset', level=WARNING)
            
        if self.objref.getM3Active() is True:
            self.objref.positionOffset(tnrtAntennaMod.M3, offset_m3 / 3600)
        else:    
            log('M3 not active.  cannot set axis position offset', level=WARNING)

    def clear_axis_position_offsets(self):
        """
        Clear axis position offsets of axis AZ, EL, M3.

        Examples
        --------
        >>> clear_axis_position_offsets()
        """
        set_axis_position_offsets(0, 0, 0)

    def set_constant_focus_offsets(self, x, y, z, tx, ty, tz):
        """
        Set constant focus offsets.  persistent during lifecycle of Antenna component
        through all scans.  Hexapod program track and elevation table are added on top of this
        constant offset while tracking.

        Parameters
        ----------
        x : float. unit = mm

        y : float. unit = mm

        z : float. unit = mm

        tx : float. unit = deg

        ty : float. unit = deg

        tz : float. unit = deg

        Examples
        --------
        >>> set_constant_focus_offsets(0, 0, 80, 0, 0, 0)
        2020-12-08 16:35:57 [INFO] nash.Antenna.set_constant_focus_offsets(): Set constant focus offsets [x, y, z, tx, ty, tz] = [10, 20, 30, 0.1, 0.2, 0.3]
        """
        self.logger.info(
            "Set constant focus offsets [x, y, z, tx, ty, tz] = [{}, {}, {}, {}, {}, {}]".format(
                x, y, z, tx, ty, tz
            )
        )
        self.objref.hexapodPositionOffset(tnrtAntennaMod.POS_OFF_X, x)
        self.objref.hexapodPositionOffset(tnrtAntennaMod.POS_OFF_Y, y)
        self.objref.hexapodPositionOffset(tnrtAntennaMod.POS_OFF_Z, z)
        self.objref.hexapodPositionOffset(tnrtAntennaMod.POS_OFF_TX, tx)
        self.objref.hexapodPositionOffset(tnrtAntennaMod.POS_OFF_TY, ty)
        self.objref.hexapodPositionOffset(tnrtAntennaMod.POS_OFF_TZ, tz)

    def clear_constant_focus_offsets(self):
        """
        Clear constant focus offsets of  all axes of HXP.
        This does not affect Elevation HXP table and HXP program tracking table.

        Examples
        --------
        >>> clear_constant_focus_offsets()
        2020-12-08 16:35:57 [INFO] nash.Antenna.set_constant_focus_offsets(): Set constant focus offsets [x, y, z, tx, ty, tz] = [0, 0, 0, 0, 0, 0]
        """
        set_constant_focus_offsets(0, 0, 0, 0, 0, 0)

    def set_hxp_el_model(self, tablename):
        """
        Set the hexapod elevation offset table immediately.  This offset table compensates for
        effects of gravity on the M2 mirror.  At low elevation angles, the structure of M2 mirror will
        sag down slightly.  Therefore, the Hexapod must lift M2 up to remain in the correct focus.

        Parameters
        ----------
        tablename : {'astro', 'geo', 'zero'}
                'astro' : Use HXP-EL table optimized for astronomy observations (default).
                The values come from the default polynomial table in file
                `tnrt/LGPL/CommonSoftware/tnrtAntenna/src/AntennaDefaults.py`

                'geo' : Use HXP-EL table optimized for geodesy observations.
                All coefficients equal to those at elevation 45 degs.

                'zero' : Set HXP-EL to all 0 (zero) values.  M2 will not move to compensate for EL angle.

        Notes
        -----
        Changes in X, Y, XTilt and YTilt change the pointing model

        Examples
        --------
        >>> set_hxp_el_model('astro')
        2020-12-17 09:39:12 [INFO] nash.Antenna.set_hxp_el_model(): Set HXP EL offset table: Astronomy HXP-EL offset lookup table

        >>> set_hxp_el_model('geo')
        2020-12-17 09:39:17 [INFO] nash.Antenna.set_hxp_el_model(): Set HXP EL offset table: Geodesy HXP-EL offset lookup table

        See Also
        --------
        clear_hxp_el_model
        """
        try:
            self.logger.info(
                "Set HXP EL offset table: " + tc.hxp_el_properties[tablename.upper()]["info"]
            )
            self.objref.setHexapodElevationOffsetModel(
                tc.hxp_el_properties[tablename.upper()]["idl_enum"]
            )
        except KeyError:
            self.logger.error(
                "unknown HXP-EL offset table: %s\n choose from %s"
                % (tablename, tc.hxp_el_properties.keys())
            )
            return
        except (
            tnrtAntennaError.acuExceptionEx,
            tnrtAntennaError.unknownAcuExceptionEx,
            tnrtAntennaError.errorInAcuAcknowledgementEx,
        ) as e:
            Session._explain(ScanError.antennaExceptionEx(e), self.logger)

    def clear_hxp_el_model(self):
        """
        Clear the elevation offset table immediately

        Examples
        --------
        >>> clear_hxp_el_model()
        2020-12-17 09:39:22 [INFO] nash.Antenna.clear_hxp_el_model(): Set HXP EL offset table: Zeros HXP-EL offset lookup table

        """
        tablename = "zero"
        try:
            self.logger.info(
                "Set HXP EL offset table: " + tc.hxp_el_properties[tablename.upper()]["info"]
            )
            self.objref.setHexapodElevationOffsetModel(
                tc.hxp_el_properties[tablename.upper()]["idl_enum"]
            )
        except KeyError:
            self.logger.error(
                "unknown HXP-EL offset table: %s\n choose from %s"
                % (tablename, tc.hxp_el_properties.keys())
            )
            return
        except (
            tnrtAntennaError.acuExceptionEx,
            tnrtAntennaError.unknownAcuExceptionEx,
            tnrtAntennaError.errorInAcuAcknowledgementEx,
        ) as e:
            Session._explain(ScanError.antennaExceptionEx(e), self.logger)

    def set_grs(self, position, rate=6.0, blocking=True):
        """
        Set Gravity Slide (GRS) position

        *NOTE* GRS motor axes must be activated.

        Parameters
        ----------
        position : float
                command GRS position. unit = mm

                minimum position = -3.50 #P104_p_FinEndDn

                maximum position = 928.5 #P105_p_FinEndUp

        rate : float, optional
                command GRS rate. unit = mm / s

                maximum rate = 6.000  #P108_v_MaxSys

                minimum rate = 0.001  #P109

        blocking : {True, False}, optional

                block until GRS arrives at command position


        Returns
        -------
        no return value

        See Also
        --------
        activate

        Examples
        --------
        >>> set_grs(300, 5.0, True)
        2022-12-26 16:23:48 [INFO] nash.Antenna.activate(): activating GRS Gravity Slider
        2022-12-26 16:23:48 [INFO] nash.Antenna.set_grs(): set GRS position=300.00 mm, rate=5.00 mm/s, blocking=True
        >>> set_grs(200)
        2022-12-26 16:24:20 [INFO] nash.Antenna.activate(): activating GRS Gravity Slider
        2022-12-26 16:24:20 [INFO] nash.Antenna.set_grs(): set GRS position=200.00 mm, rate=6.00 mm/s, blocking=True
        """
        try:
            position = float(position)
            rate = float(rate)
            blocking = bool(blocking)
        except ValueError as e:
            self.logger.error(e)
            raise

        try:
            activate("grs")
            self.logger.info(
                "set GRS position={:.2f} mm, rate={:.2f} mm/s, blocking={}".format(
                    position, rate, blocking
                )
            )
            status = self.objref.absolutePosition(tnrtAntennaMod.GRS, position, rate, blocking)
            self.logger.debug("ACU ack status: {}".format(status))
        except Exception as e:
            self.logger.error(e)
            raise


class Scan(ComponentWrapper):
    """
    Docstring for class Scan (ComponentWrapper)
    """

    def add_scan(
        self,
        schedule_params,
        scantype_params,
        tracking_params=None,
        frontend_params=None,
        backend_params=None,
        data_params=None,
    ):
        """
        Construct a complete Scan object and add to the queue.

        All scan types require `schedule_params` and `scantype_params` in 
        order to create a Scan object, add to the queue, run, and save data to a file.

        Some scan types do not require`tracking_params`, `frontend_params`, or 'backend_params'. 
        For example, if scantype_params is :py:class:`~ParameterBlocks.ScantypeParamsTimeSync`, 
        the antenna does not track a coordinate or activate receivers using `frontend_params` and `backend_params`.
        :py:class:`~ParameterBlocks.ScantypeParamsTimeSync` requires only a schedule time
        and duration to record data from ACU status message.

        Parameters
        ----------
        schedule_params : :py:class:`~ParameterBlocks.ScheduleParams`
            Parameters for scheduler and ID of this scan

        scantype_params : {\
        :py:class:`~ParameterBlocks.ScantypeParamsCalColdload`,\
        :py:class:`~ParameterBlocks.ScantypeParamsCalHotload`,\
        :py:class:`~ParameterBlocks.ScantypeParamsCalNoise`,\
        :py:class:`~ParameterBlocks.ScantypeParamsCross`,\
        :py:class:`~ParameterBlocks.ScantypeParamsFivePoint`,\
        :py:class:`~ParameterBlocks.ScantypeParamsFocus`,\
        :py:class:`~ParameterBlocks.ScantypeParamsMap`,\
        :py:class:`~ParameterBlocks.ScantypeParamsManual`,\
        :py:class:`~ParameterBlocks.ScantypeParamsOnSource`,\
        :py:class:`~ParameterBlocks.ScantypeParamsRaster`,\
        :py:class:`~ParameterBlocks.ScantypeParamsSkydip`,\
        :py:class:`~ParameterBlocks.ScantypeParamsTimeSync`,\
        }
            Configure the scan pattern (ACU Program Offset Table) relative to the center tracking coordinate (ACU Program Track Table) specified in `tracking_params`

        tracking_params : {\
        :py:class:`~ParameterBlocks.TrackingParamsEQ`,\
        :py:class:`~ParameterBlocks.TrackingParamsHO`,\
        :py:class:`~ParameterBlocks.TrackingParamsSS`,\
        :py:class:`~ParameterBlocks.TrackingParamsTLE`},\
        optional. depends on scantype_params
            Configure the center tracking coordinate of the observation (ACU Program Track Table)

        frontend_params : {\
        :py:class:`~ParameterBlocks.FrontendParamsL`,\
        :py:class:`~ParameterBlocks.FrontendParamsCX`,\
        :py:class:`~ParameterBlocks.FrontendParamsKu`,\
        :py:class:`~ParameterBlocks.FrontendParamsK`,\
        :py:class:`~ParameterBlocks.FrontendParamsOptical`},\
        optional. depends on scantype_params
            Configure the receiver frontend.  Includes components between the sky and digitizer (digitizer config is in backend_params)

        backend_params : {\
        :py:class:`~ParameterBlocks.BackendParamsEDD`,\
        :py:class:`~ParameterBlocks.BackendParamsHoloFFT`,\
        :py:class:`~ParameterBlocks.BackendParamsHoloSDR`,\
        :py:class:`~ParameterBlocks.BackendParamsSKARAB`,\
        :py:class:`~ParameterBlocks.BackendParamsOptical`,\
        :py:class:`~ParameterBlocks.BackendParamsPowerMeter`},\
        optional. depends on scantype_params
            Configure the digital backend (data acquisition and processing).  Includes digitizer config

        data_params : {\
        :py:class:`~ParameterBlocks.DataParams`,\
        optional. depends on scantype_params
            Select the TCS data pipelines and data file formats.  Downstream from backend_params.

        Examples
        --------
        >>> delay_until_scheduled_start = 10
        >>> project_id = 'test script'
        >>> obs_id = 'SS'
        >>> start_mjd = (Time.now() + delay_until_scheduled_start * units.second).mjd
        >>> source_name = 'AAA'
        >>> line_name = 'BBB'
        >>> duration = 10.0
        >>> az = -40        # deg
        >>> el = 12.0       # deg
        >>> user_pointing_correction_az = 0.0 # arcsec
        >>> user_pointing_correction_el = 0.0 # arcsec
        >>> schedule_params = ScheduleParams(project_id, obs_id, start_mjd=start_mjd, source_name=source_name, line_name=line_name)
        >>> scantype_params = ScantypeParamsOnSource(duration)
        >>> tracking_params = TrackingParamsHO(az, el, user_pointing_correction_az, user_pointing_correction_el)
        >>> add_scan(schedule_params, scantype_params, tracking_params)
        2020-05-15 14:17:47 [DEBUG] nash.Scan.add_scan(): Creating scan and add to queue:
        ScanMod.ScheduleParams(project_id='test script', obs_id='SS', scan_id=0, priority=0, start_mjd=58984.304133787526, source_name='AAA', line_name='BBB')
        ScanMod.ScantypeParamsOnSource(duration=10.0)
        2020-05-15 14:17:47 [DEBUG] nash.Scan.add_scan(): schedule_params FOUND
        2020-05-15 14:17:47 [DEBUG] nash.Scan.add_scan(): scantype_params FOUND
        2020-05-15 14:17:47 [DEBUG] nash.Scan.add_scan(): tracking_params FOUND
        2020-05-15 14:17:47 [DEBUG] nash.Scan.add_scan(): signal_params NOT FOUND.  Does this scan require Receivers?
        2020-05-15 14:17:47 [DEBUG] nash.Scan.add_scan(): Adding scan ID: 2
        2020-05-15 14:17:47 [DEBUG] nash.Scan.add_scan(): Queue size: 1

        >>> delay_until_scheduled_start = 10
        >>> start_mjd = (Time.now() + delay_until_scheduled_start * units.second).mjd
        >>> source_name = 'CCC'
        >>> line_name = 'DDD'
        >>> az = -50 # deg
        >>> el = 20 # deg
        >>> user_pointing_correction_az = 0.0 # arcsec
        >>> user_pointing_correction_el = 0.0 # arcsec
        >>> arm_length = 7200 # arcsec
        >>> time_per_arm = 30.0 # arcsec
        >>> win_min = 1000 # arcsec
        >>> win_max = 1000 # arcsec 
        >>> schedule_params = ScheduleParams(project_id, obs_id, start_mjd=start_mjd, source_name=source_name, line_name=line_name)
        >>> scantype_params = ScantypeParamsCross(arm_length, time_per_arm, win_min, win_max)
        >>> tracking_params = TrackingParamsHO(az, el, user_pointing_correction_az, user_pointing_correction_el)
        >>> add_scan(schedule_params, scantype_params, tracking_params)
        2020-05-15 14:17:47 [DEBUG] nash.Scan.add_scan(): Creating scan and add to queue:
        ScanMod.ScheduleParams(project_id='test script', obs_id='SS', scan_id=0, priority=0, start_mjd=58984.30413409882, source_name='CCC', line_name='DDD')
        ScanMod.ScantypeParamsCross(arm_length=7200, time_per_arm=30.0, win_min=1000, win_max=1000)
        2020-05-15 14:17:47 [DEBUG] nash.Scan.add_scan(): schedule_params FOUND
        2020-05-15 14:17:47 [DEBUG] nash.Scan.add_scan(): scantype_params FOUND
        2020-05-15 14:17:47 [DEBUG] nash.Scan.add_scan(): tracking_params FOUND
        2020-05-15 14:17:47 [DEBUG] nash.Scan.add_scan(): signal_params NOT FOUND.  Does this scan require Receivers?
        2020-05-15 14:17:47 [DEBUG] nash.Scan.add_scan(): Adding scan ID: 3
        2020-05-15 14:17:47 [DEBUG] nash.Scan.add_scan(): Queue size: 2

        >>> delay_until_scheduled_start = 10
        >>> start_mjd = (Time.now() + delay_until_scheduled_start * units.second).mjd
        >>> source_name = 'EEE'
        >>> line_name = 'FFF'
        >>> duration = 10.0
        >>> tle_line0 = 'INTELSAT 22 (IS-22)     '
        >>> tle_line1 = '1 38098U 12011A   20070.00674396 -.00000076  00000-0  00000+0 0  9990'
        >>> tle_line2 = '2 38098   0.0020 184.2253 0002748 169.5430 248.8790  1.00272611 29083'
        >>> user_pointing_correction_az = 0.0
        >>> user_pointing_correction_el = 0.0
        >>> schedule_params = ScheduleParams(project_id, obs_id, start_mjd=start_mjd, source_name=source_name, line_name=line_name)
        >>> scantype_params = ScantypeParamsOnSource(duration)
        >>> tracking_params = TrackingParamsTLE(tle_line0, tle_line1, tle_line2, user_pointing_correction_az, user_pointing_correction_el)
        >>> add_scan(schedule_params, scantype_params, tracking_params)
        2020-05-15 14:17:47 [DEBUG] nash.Scan.add_scan(): Creating scan and add to queue:
        ScanMod.ScheduleParams(project_id='test script', obs_id='SS', scan_id=0, priority=0, start_mjd=58984.30413430339, source_name='EEE', line_name='FFF')
        ScanMod.ScantypeParamsOnSource(duration=10.0)
        2020-05-15 14:17:47 [DEBUG] nash.Scan.add_scan(): schedule_params FOUND
        2020-05-15 14:17:47 [DEBUG] nash.Scan.add_scan(): scantype_params FOUND
        2020-05-15 14:17:47 [DEBUG] nash.Scan.add_scan(): tracking_params FOUND
        2020-05-15 14:17:47 [DEBUG] nash.Scan.add_scan(): signal_params NOT FOUND.  Does this scan require Receivers?
        2020-05-15 14:17:47 [DEBUG] nash.Scan.add_scan(): Added scan ID: 4
        2020-05-15 14:17:47 [DEBUG] nash.Scan.add_scan(): Queue size: 3

        >>> delay_until_scheduled_start = 10
        >>> start_mjd = (Time.now() + delay_until_scheduled_start * units.second).mjd
        >>> source_name = 'GGG'
        >>> line_name = 'HHH'
        >>> user_pointing_correction_az = 0.0 # arcsec
        >>> user_pointing_correction_el = 0.0 # arcsec
        >>> arm_length=7200.0 # arcsec
        >>> time_per_arm = 60.0 # arcsec
        >>> win_min = -1000 # arcsec
        >>> win_max = 1000 # arcsec
        >>> schedule_params = ScheduleParams(project_id, obs_id, start_mjd=start_mjd, source_name=source_name, line_name=line_name)
        >>> scantype_params = ScantypeParamsCross(arm_length, time_per_arm, win_min, win_max)
        >>> tracking_params = TrackingParamsTLE(tle_line0, tle_line1, tle_line2, user_pointing_correction_az, user_pointing_correction_el)
        >>> add_scan(schedule_params, scantype_params, tracking_params)
        2020-05-15 14:17:47 [DEBUG] nash.Scan.add_scan(): Creating scan and add to queue:
        ScanMod.ScheduleParams(project_id='test script', obs_id='SS', scan_id=0, priority=0, start_mjd=58984.30414176438, source_name='GGG', line_name='HHH')
        ScanMod.ScantypeParamsCross(arm_length=7200.0, time_per_arm=60.0, win_min=-1000, win_max=1000)
        2020-05-15 14:17:47 [DEBUG] nash.Scan.add_scan(): schedule_params FOUND
        2020-05-15 14:17:47 [DEBUG] nash.Scan.add_scan(): scantype_params FOUND
        2020-05-15 14:17:47 [DEBUG] nash.Scan.add_scan(): tracking_params FOUND
        2020-05-15 14:17:47 [DEBUG] nash.Scan.add_scan(): signal_params NOT FOUND.  Does this scan require Receivers?
        2020-05-15 14:17:47 [DEBUG] nash.Scan.add_scan(): Added scan ID: 5
        2020-05-15 14:17:47 [DEBUG] nash.Scan.add_scan(): Queue size: 4

        >>> line_length = 7200
        >>> time_per_line = 10
        >>> nlines = 1
        >>> spacing = 720
        >>> axis = "x"
        >>> zigzag = False
        >>> coord_system = "HO"
        >>> subscans_per_cal = 0
        >>> time_cal0 = 30
        >>> time_per_cal = 5

        >>> scantype_params_map = ScantypeParamsMap(line_length, time_per_line, nlines, spacing, axis,
            zigzag, coord_system, subscans_per_cal, time_cal0, time_per_cal)
        
        >>> data_params_holo = DataParams(["atfits"])
        
        >>> add_scan(
            schedule_params,
            scantype_params_map,
            tracking_params_HO_min,
            # frontend_params_L,
            # backend_params_edd_default,
            data_params=data_params_holo
            )

        2024-01-23 11:32:46 [DEBUG] nash.Scan.add_scan(): tracking_params FOUND
        2024-01-23 11:32:46 [DEBUG] nash.Scan.add_scan(): ScanMod.TrackingParamsHO(az=10, el=40, common=ScanMod.TrackingParamsCommon(user_pointing_correction_az=0, user_pointing_correction_el=0, elevation_min=0, elevation_max=90, north_crossing=True, use_horizontal_tables=False, max_tracking_errors=6))
        2024-01-23 11:32:46 [DEBUG] nash.Scan.add_scan(): frontend_params NOT FOUND.  Does this scan require Frontend parameters?
        2024-01-23 11:32:46 [DEBUG] nash.Scan.add_scan(): backend_params NOT FOUND.  Does this scan require Backend parameters?
        2024-01-23 11:32:46 [DEBUG] nash.Scan.add_scan(): data params FOUND
        2024-01-23 11:32:46 [DEBUG] nash.Scan.add_scan(): ScanMod.DataParams(pipeline_name_list=['atfits'])
        2024-01-23 11:32:46 [DEBUG] nash.Scan.add_scan(): Next automatic scan ID: 9
        2024-01-23 11:32:46 [DEBUG] nash.Scan.add_scan(): Queue size: 7
        2024-01-23 11:32:46 [DEBUG] nash.Scan.add_scan(): Creating scan and add to queue:
        2024-01-23 11:32:46 [DEBUG] nash.Scan.add_scan(): schedule_params FOUND
        2024-01-23 11:32:46 [DEBUG] nash.Scan.add_scan(): ScanMod.ScheduleParams(project_id='UnitTest', obs_id='SS', scan_id=0, priority=0, start_mjd=60332.188542749966, source_name='undef_source', line_name='un
        """
        self.logger.debug("Creating scan and add to queue:")

        # Required blocks of parameters
        # If we cannot get the IDL valuetype object for this groupd of parameters,
        # log an exception and return.
        try:
            schedule_idl = schedule_params.objval
            self.logger.debug("schedule_params FOUND")
            self.logger.debug(repr(schedule_params))
        except AttributeError:
            self.logger.error("schedule_params NOT FOUND")
            return
        try:
            scantype_idl = scantype_params.objval
            self.logger.debug(
                "scantype_params FOUND: {} --> {}".format(type(scantype_params), type(scantype_idl))
            )
            self.logger.debug(repr(scantype_params))
        except AttributeError:
            self.logger.error(
                "scantype_params NOT FOUND. {} not yet implented. Cannot add this scan to the queue".format(
                    type(scantype_params)
                )
            )
            return

        # Optional blocks of parameters
        # If we cannot get the IDL valuetype object for this group of parameters,
        # log warning and continue ..
        # assume that the user does not want these optional paramter blocks.
        try:
            tracking_idl = tracking_params.objval
            self.logger.debug("tracking_params FOUND")
            self.logger.debug(repr(tracking_params))
        except AttributeError:
            self.logger.debug(
                "tracking_params NOT FOUND.  Does this scan require Tracking parameters?"
            )
            # create a dummy object to make IDL happy when we add the scan.  Cannot send None
            tracking_idl = ScanMod.AbstractTrackingParams()

        try:
            frontend_idl = frontend_params.objval
            self.logger.debug("frontend_params FOUND")
            self.logger.debug(repr(frontend_params))
        except AttributeError:
            self.logger.debug(
                "frontend_params NOT FOUND.  Does this scan require Frontend parameters?"
            )
            # create a dummy object to make IDL happy.  Cannot send None
            frontend_idl = ScanMod.AbstractFrontendParams()

        try:
            backend_idl = backend_params.objval
            self.logger.debug("backend_params FOUND")
            self.logger.debug(repr(backend_params))
        except AttributeError:
            self.logger.debug(
                "backend_params NOT FOUND.  Does this scan require Backend parameters?"
            )
            # create a dummy object to make IDL happy.  Cannot send None
            backend_idl = ScanMod.AbstractBackendParams()

        try:
            data_idl = data_params.objval
            self.logger.debug("data params FOUND")
            self.logger.debug(repr(data_params))
        except AttributeError:
            self.logger.warning("data_params NOT FOUND.  Selected default 'spectrum_preview' and 'mbfits' for backward compatibility.  New scripts should specify the data_params")
            # NOTE (SS. 01/2024). For backward compatibility with old user scripts,
            # set default data parameters to include spectrum_preview and mbfits
            # pipelines.  
            data_idl = ScanMod.DataParams(["mbfits", "spectrum_preview"])
            
            # Create a DataParams object pipeline name list of 0 items.  
            # Cannot send None through IDL interface
            # data_idl = ScanMod.DataParams([])

        try:
            self.logger.debug("Next automatic scan ID: %d" % self.objref.get_scan_id_counter())
            self.objref.addScan(
                schedule_idl,
                scantype_idl,
                tracking_idl,
                frontend_idl,
                backend_idl,
                data_idl,
            )
            self.logger.debug("Queue size: %d" % self.objref.qsize())
        except ScanError.sourceBelowHorizonEx:
            self.logger.error(
                "Failed to construct scan %d. Below horizon.   Choose a differnet RA, DEC, or Time"
                % schedule_params.scan_id
            )
        except:
            self.logger.error("Construct Scan object and add to queue failed")
            raise  # let python explain the error

    def run_queue_helper(self):
        """
        internal function to catch results from separate thread.
        """
        try:
            self.results_json = self.objref.run_queue()
        except Exception as e:
            self.exception = e

    def run_queue(self, blocking=True):
        """
        Process all scans that are loaded in the queue.

        Parameters
        ----------
                blocking : {True, False}, optional. default=`True`

                `True` Block execution of nash live session or script on :py:func:`run_queue` until queue is finished

                `False` Do not block execution of nash live session or script on :py:func:`run_queue`.
                :py:func:`run_queue` returns
                immediately and user can continue to send manual commands in script or ipython live session while
                queue is running -- including the command :py:func:`stop_queue` to stop the manual scan in the script.

                This is useful for manual scan (:py:class:`~ParameterBlocks.ScanTypeParamsManual`).  The user can
                start a Manual scan that does not use any receiver.  Only record ACU status messages while the script
                runs arbitrary motion commands to the ACU.  Good for testing Antenna / ACU without receivers.


        Returns
        -------
        results : dict
                First index is the scan number

        Examples
        --------
        >>> results = run_queue()
        2020-05-15 14:17:47 [INFO] nash.Scan.run_queue(): Run all scans in the queue. 4 scans.
        2020-05-15 14:17:47 [WARNING] nash.Scan.run_queue(): block until all scans are finished ...
        2020-05-15 14:17:47 [DEBUG] nash.Scan.run_queue(): see details of Scan queue execution in ACS CommandCenter log tab "ScanC"
        """
        self.logger.info("Run all scans in the queue. %d scans." % self.objref.qsize())

        results_dict = {}
        
        self.logger.debug("run queue [size: {}]".format(self.objref.qsize()))

        if blocking is True:
            # Run the queue and block waiting for thread to finish.  But provide the opportunity to catch
            # a CTRL+C keyboard interrupt and stop the scan immediately.
            try:
                self.logger.warning("Block until all scans are finished")
                self.logger.warning("CTRL+C to stop current scan, stop the queue, receive results")
                self.logger.debug(
                    "use function clear_queue() to delete remaining items after stopped"
                )
                self.logger.debug(
                    'see details of Scan queue execution in ACS CommandCenter log tab "ScanC" and DataMonitor GUI'
                )
                self.run_queue_helper()
                if(self.exception):
                    raise self.exception
            
                results_dict_str = json.loads(self.results_json)

                # First key [scan number] is string because it came from json format.
                # convert key to int for more convenient access.
                for k in results_dict_str.keys():
                    results_dict[int(k)] = results_dict_str[k]

            except KeyboardInterrupt as e:
                self.logger.debug("Catch KeyboardInterrupt. Stop the scan NOW")
                self.objref.stop_queue()
            except ScanError.backendsExceptionEx as e:
                self.logger.error('Backend Error: Cannot connect to EddFitsServer')
                self.logger.warning('Stop the queue')
                self.objref.stop_queue()
            except Exception as e:
                msg = "{}: {}".format(e.__class__.__name__, e)
                self.logger.error(msg)
                self.logger.warning('Stop the queue and exit')
                self.objref.stop_queue()
                                
        else:
            # Do not provide the CTRL+C KeyboardInterrupt.  User must use function stop_queue().
            # This mode of operation is useful for Manual scans where user is recording data from ACU
            # while controlling the antenna with arbitrary commands -- not a standard scan type.
            self.logger.warning(
                "Use function stop_queue() to stop current scan, stop the queue, receive results"
            )
            self.logger.debug("use function clear_queue() to delete remaining items after stopped")
            self.logger.debug(
                'see details of Scan queue execution in ACS CommandCenter log tab "ScanC" and DataMonitor GUI'
            )
            thread_queue = threading.Thread(target=self.run_queue_helper)
            thread_queue.start()

        # Note
        # if blocking == True, the results dictionary will be a full dictionary of scan IDs, file names, ...
        # if blocking == False, this function returns immediately before results are available.
        # Therefore, the user must receive results from the function stop_queue()
        return results_dict

    def clear_queue(self):
        """
        Delete all scan objects in the queue.

        Returns
        -------
        result : int
                number of items deleted from the queue

        Examples
        --------
        >>> result = clear_queue()

        """
        result = self.objref.clearQueue()
        self.logger.debug("Deleted {} items from the queue".format(result))

    def stop_queue(self):
        """
        Stop the current scan and stop the queue.  Remaining items stay in the queue and can run again.
        The current scan that was stopped is already removed from the queue.

        Returns
        -------
        result : int
                number of items remain in the queue

        Examples
        --------
        >>> result = stop_queue()

        """
        results_json = self.objref.stop_queue()

        results_dict_str = json.loads(results_json)

        self.logger.debug("Remain {} items in the queue".format(self.objref.qsize()))

        # First key [scan number] is string because it came from json format.
        # convert key to int for more convenient access.
        results_dict = {}
        for k in results_dict_str.keys():
            results_dict[int(k)] = results_dict_str[k]

        return results_dict

    def get_scan_id_lastrun(self):
        """
        Get the scan ID of the last scan that was run from the queue.  This function is useful to read
        results from scan such as filename or pointing offset caluclated results.

        Returns
        -------
        scan_id : int
                scan id.  either from manual scan ID override or automatically generated by scan controller.

        Examples
        --------
        >>> get_scan_id_lastrun()
        4
        """
        try:
            return self.objref.get_scan_id_lastrun()
        except:
            self.logger.error("Cannot read SCANNUM from current scan metadata")

    def get_tsys(self, scan_id=0):
        """
        Get the system noise temperature from a specific scan number or last calbration scan

        Parameters
        ----------
        scan_id : int, optional.  default = 0 (find last calibration scan)

        Returns
        -------
        tsys : float
                system temperature. unit = K

        Examples
        --------
        >>> t = get_tsys()
        2020-12-08 14:23:37 [WARNING] nash.Scan.get_tsys(): TODO
        Out[2]: -1.0

        >>> t = get_tsys(1234)
        2020-12-08 14:23:37 [WARNING] nash.Scan.get_tsys(): TODO
        Out[2]: -1.0
        """
        self.logger.warning("TODO")
        return -1.0

    def get_focusfit(self, scan_id=0):
        """
        Get the best focus hexapod position from a specific scan number or last focus scan

        Parameters
        ----------
        scan_id : int, optional.  default = 0 (find last focus scan)

        Returns
        -------
        [x, y, z, tx, ty, tz]: list of float
                Hexapod position with maximum SNR. unit = [mm, mm, mm, deg, deg, deg]

        Examples
        --------
        >>> f = get_focusfit()
        2020-12-08 16:56:05 [WARNING] nash.Scan.get_focusfit(): TODO
        Out[2]: [-9, -9, -9, -0.9, -0.9, -0.9]


        >>> f = get_focusfit(1234)
        2020-12-08 16:56:05 [WARNING] nash.Scan.get_focusfit(): TODO
        Out[2]: [-9, -9, -9, -0.9, -0.9, -0.9]

        """
        self.logger.warning("TODO")
        return [-9, -9, -9, -0.9, -0.9, -0.9]

    def get_az_el_vtopo(self, schedule_params, tracking_params):
        """
        Calculate the inizial azimuth, initial elevation and topocentric velocity
        of of the tracking coordinate.at the scheduled time.

        Parameters
        ----------
        schedule_params : :py:class:`~ParameterBlocks.ScheduleParams`
                Block of Schedule parameters that includes the start time

        tracking_params : {:py:class:`~ParameterBlocks.TrackingParamsHO`, :py:class:`~ParameterBlocks.TrackingParamsEQ`, :py:class:`~ParameterBlocks.TrackingParamsTLE`, :py:class:`~ParameterBlocks.TrackingParamsSS`}
                Block of tracking parameters for any coordinate system (HO, EQ, TLE, SS) that includes the data for tracking in that coordinate system.

        Returns
        -------
        az : float
                initial azimuth angle for this tracking coordinate at start of scan. unit = Deg

        el : float
                initial elevation angle for this tracking coordinate at start of scan. unit = Deg

        vtopo : float
                topocentric velocity (affects doppler shift of RF signal)

        Examples
        --------
        >>> schedule_params = ScheduleParams('ABC-XYZ', 'unittest_observer', start_mjd=59200)
        >>> tracking_params = TrackingParamsEQ(20, 45, 0, 0, 0, 1)
        >>> result = get_az_el_vtopo(schedule_params, tracking_params)
        >>> print(result)
        (0.0, 45.0, 500.0)

        >>> (az, el, vtopo) = get_az_el_vtopo(schedule_params, tracking_params)
        print(az)
        0.0
        """
        self.logger.warning("TODO. return fake value")
        return (0.0, 45.0, 500.0)


class Catalog:
    """
    A Python class to access star catalog data from .csv file or from online data
    source such as Simbad or Vizier through astroquery interface
    """

    def __init__(self):
        self.logger = logging.getLogger("nash." + self.__class__.__name__)
        self.logger.propagate = True
        self.logger.handlers = []
        self.logger.debug('Started log for "%s"' % self.__class__.__name__)
        self.catalog = None
        self.source_list = []
        self.catalog_list = None
        self.catalog_degree = None
        self.catalog_radian = None
        self.catalog_hms = None
        self.scan = Scan("Scan")

    def get_TLE(self, id=38098):  # default INTELSAT 22
        """
        Load realtime TLE data from api and return the Scanmod.TLEData object

        Parameters
        ----------
        id : number
                id of the interested satellite, default at 38098 which is the id of INTELSAT-22

        Returns
        -------
        TLE_data : Scanmod.TLEData(
                name : string
                        name of the satellite

                line1 : string
                        first line of the TLE data

                line2 : string
                        second line of the TLE data

                date : string
                        date when data was fetched from server

                message : string
                        warning message when can not get data from internet
        )

        Examples
        --------
        >>> TLE_data = get_TLE()
        >>> print(TLE_data)
        ScanMod.TLEData(
                name='INTELSAT 22 (IS-22)',
                line1='1 38098U 12011A   21228.50701771 -.00000067  00000-0  00000-0 0  9990',
                line2='2 38098   0.0177 222.8807 0002655 290.2598  66.5951  1.00271176 34113',
                date='2021-08-16T12:10:06+00:00',
                message=''
        )
        """
        try:
            response = self.scan.objref.get_TLE(id)
            if response.message:
                self.logger.warning(response.message)
            return response
        except (ScanError.httpResponseEx) as e:
            self.logger.error("Not found, please recheck TLE id")
        except ScanError.noInternetConnectionEx:
            self.logger.error("Cannot get TLE data, please check internet connection")
        except Exception as e:
            self.logger.error(e)

    def get_catalog(self, catalog_path="", mode="csv"):
        """
        Load the catalog data into a Python data structure and returns the
        :py:class:`pandas.core.frame.DataFrame` object.  Convenient for iterate, traverse, search.

        Parameters
        ----------
        catalog_path :  string
                relative path or full path to the CSV file that has columns in this format::

                        SourceName,System,Epoch,RA,Dec,LSR,!Comment
                        J0108+0135,EQ,2000,01 08 38.771,+01 35 00.317,540,!Quasar
                        J0116-1136,EQ,2000,01 16 12.522,-11 36 15.434,540,!Quasar
                        EM0137+3309,EQ,2000,01 37 41.299,+33 09 35.132,720,!Quasar
                        E0423-0120,EQ,2000,04 23 15.801,-01 20 33.065,540,!Quasar
                        J0410+7656,EQ,2000,04 10 45.606,+76 56 45.301,540,!AGN

                If filename parameter is not provided (or catalog_path is an empty string " "),
                this function loads an example catalog CSV file that contains fake data
                for software testing.
                Example file has 53 radio sources that span a wide range of coordinates.

                {RA = 0 .. 23 in steps of 1 hourangle, DEC = 0, VLSR = 0} (24 items)

                {RA = 0, DEC = -90 .. 90 in steps of 10 degrees, VLSR=0} (19 items)

                {RA = 0, DEC = 0, VLSR = 0 .. 900 in steps of 100 km/s} (10 items)

        mode : {'csv', 'simbad'}
                `csv` : file to read is comma separated values (.csv)

                `simbad` : TODO.  not implemented yet (use astroquery to search simbad catalog)

        Returns
        -------
        catalog : :py:class:`pandas.core.frame.DataFrame`

        Examples
        --------
        >>> c = get_catalog()

        >>> print(c)
            SourceName System  Epoch            RA            Dec  LSR !Comment
        0   J0000+0000     EQ   2000  00 00 00.000  +00 00 00.000    0    !Fake
        1   J0100+0000     EQ   2000  01 00 00.000  +00 00 00.000    0    !Fake
        2   J0200+0000     EQ   2000  02 00 00.000  +00 00 00.000    0    !Fake
        3   J0300+0000     EQ   2000  03 00 00.000  +00 00 00.000    0    !Fake
        4   J0400+0000     EQ   2000  04 00 00.000  +00 00 00.000    0    !Fake
        5   J0500+0000     EQ   2000  05 00 00.000  +00 00 00.000    0    !Fake
        6   J0600+0000     EQ   2000  06 00 00.000  +00 00 00.000    0    !Fake
        7   J0700+0000     EQ   2000  07 00 00.000  +00 00 00.000    0    !Fake
        8   J0800+0000     EQ   2000  08 00 00.000  +00 00 00.000    0    !Fake
        9   J0900+0000     EQ   2000  09 00 00.000  +00 00 00.000    0    !Fake
        10  J1000+0000     EQ   2000  10 00 00.000  +00 00 00.000    0    !Fake
        11  J1100+0000     EQ   2000  11 00 00.000  +00 00 00.000    0    !Fake
        12  J1200+0000     EQ   2000  12 00 00.000  +00 00 00.000    0    !Fake
        13  J1300+0000     EQ   2000  13 00 00.000  +00 00 00.000    0    !Fake
        14  J1400+0000     EQ   2000  14 00 00.000  +00 00 00.000    0    !Fake
        15  J1500+0000     EQ   2000  15 00 00.000  +00 00 00.000    0    !Fake
        16  J1600+0000     EQ   2000  16 00 00.000  +00 00 00.000    0    !Fake
        17  J1700+0000     EQ   2000  17 00 00.000  +00 00 00.000    0    !Fake
        18  J1800+0000     EQ   2000  16 00 00.000  +00 00 00.000    0    !Fake
        19  J1900+0000     EQ   2000  19 00 00.000  +00 00 00.000    0    !Fake
        20  J2000+0000     EQ   2000  20 00 00.000  +00 00 00.000    0    !Fake
        21  J2100+0000     EQ   2000  21 00 00.000  +00 00 00.000    0    !Fake
        22  J2200+0000     EQ   2000  22 00 00.000  +00 00 00.000    0    !Fake
        23  J2300+0000     EQ   2000  23 00 00.000  +00 00 00.000    0    !Fake
        24  J0000-9000     EQ   2000  00 00 00.000  -90 00 00.000    0    !Fake
        25  J0000-8000     EQ   2000  00 00 00.000  -80 00 00.000    0    !Fake
        26  J0000-7000     EQ   2000  00 00 00.000  -70 00 00.000    0    !Fake
        27  J0000-6000     EQ   2000  00 00 00.000  -60 00 00.000    0    !Fake
        28  J0000-5000     EQ   2000  00 00 00.000  -50 00 00.000    0    !Fake
        29  J0000-4000     EQ   2000  00 00 00.000  -40 00 00.000    0    !Fake
        30  J0000-3000     EQ   2000  00 00 00.000  -30 00 00.000    0    !Fake
        31  J0000-2000     EQ   2000  00 00 00.000  -20 00 00.000    0    !Fake
        32  J0000-1000     EQ   2000  00 00 00.000  -10 00 00.000    0    !Fake
        33  J0000+0000     EQ   2000  00 00 00.000  +00 00 00.000    0    !Fake
        34  J0000+1000     EQ   2000  00 00 00.000  +10 00 00.000    0    !Fake
        35  J0000+2000     EQ   2000  00 00 00.000  +20 00 00.000    0    !Fake
        36  J0000+3000     EQ   2000  00 00 00.000  +30 00 00.000    0    !Fake
        37  J0000+4000     EQ   2000  00 00 00.000  +40 00 00.000    0    !Fake
        38  J0000+5000     EQ   2000  00 00 00.000  +50 00 00.000    0    !Fake
        39  J0000+6000     EQ   2000  00 00 00.000  +60 00 00.000    0    !Fake
        40  J0000+7000     EQ   2000  00 00 00.000  +70 00 00.000    0    !Fake
        41  J0000+8000     EQ   2000  00 00 00.000  +80 00 00.000    0    !Fake
        42  J0000+9000     EQ   2000  00 00 00.000  +90 00 00.000    0    !Fake
        43  J0000+0000     EQ   2000  00 00 00.000  +00 00 00.000    0    !Fake
        44  J0000+0000     EQ   2000  00 00 00.000  +00 00 00.000  100    !Fake
        45  J0000+0000     EQ   2000  00 00 00.000  +00 00 00.000  200    !Fake
        46  J0000+0000     EQ   2000  00 00 00.000  +00 00 00.000  300    !Fake
        47  J0000+0000     EQ   2000  00 00 00.000  +00 00 00.000  400    !Fake
        48  J0000+0000     EQ   2000  00 00 00.000  +00 00 00.000  500    !Fake
        49  J0000+0000     EQ   2000  00 00 00.000  +00 00 00.000  600    !Fake
        50  J0000+0000     EQ   2000  00 00 00.000  +00 00 00.000  700    !Fake
        51  J0000+0000     EQ   2000  00 00 00.000  +00 00 00.000  800    !Fake
        52  J0000+0000     EQ   2000  00 00 00.000  +00 00 00.000  900    !Fake


        >>> for (ra,dec) in zip(c['RA'], c['Dec']): print(ra, dec)
        00 00 00.000 +00 00 00.000
        01 00 00.000 +00 00 00.000
        02 00 00.000 +00 00 00.000
        03 00 00.000 +00 00 00.000
        04 00 00.000 +00 00 00.000
        05 00 00.000 +00 00 00.000
        06 00 00.000 +00 00 00.000
        07 00 00.000 +00 00 00.000
        08 00 00.000 +00 00 00.000
        09 00 00.000 +00 00 00.000
        10 00 00.000 +00 00 00.000
        11 00 00.000 +00 00 00.000
        12 00 00.000 +00 00 00.000
        13 00 00.000 +00 00 00.000
        14 00 00.000 +00 00 00.000
        15 00 00.000 +00 00 00.000
        16 00 00.000 +00 00 00.000
        17 00 00.000 +00 00 00.000
        16 00 00.000 +00 00 00.000
        19 00 00.000 +00 00 00.000
        20 00 00.000 +00 00 00.000
        21 00 00.000 +00 00 00.000
        22 00 00.000 +00 00 00.000
        23 00 00.000 +00 00 00.000
        00 00 00.000 -90 00 00.000
        00 00 00.000 -80 00 00.000
        00 00 00.000 -70 00 00.000
        00 00 00.000 -60 00 00.000
        00 00 00.000 -50 00 00.000
        00 00 00.000 -40 00 00.000
        00 00 00.000 -30 00 00.000
        00 00 00.000 -20 00 00.000
        00 00 00.000 -10 00 00.000
        00 00 00.000 +00 00 00.000
        00 00 00.000 +10 00 00.000
        00 00 00.000 +20 00 00.000
        00 00 00.000 +30 00 00.000
        00 00 00.000 +40 00 00.000
        00 00 00.000 +50 00 00.000
        00 00 00.000 +60 00 00.000
        00 00 00.000 +70 00 00.000
        00 00 00.000 +80 00 00.000
        00 00 00.000 +90 00 00.000
        00 00 00.000 +00 00 00.000
        00 00 00.000 +00 00 00.000
        00 00 00.000 +00 00 00.000
        00 00 00.000 +00 00 00.000
        00 00 00.000 +00 00 00.000
        00 00 00.000 +00 00 00.000
        00 00 00.000 +00 00 00.000
        00 00 00.000 +00 00 00.000
        00 00 00.000 +00 00 00.000
        00 00 00.000 +00 00 00.000

        """
        try:
            if catalog_path == "":
                catalog_path = (
                    os.getenv("INTROOT") + "/lib/python/site-packages/catalog_example.csv"
                )
                self.logger.debug("Get catalog from {}".format(catalog_path))

            self.catalog = pandas.read_csv(catalog_path, sep=",")  # read csv
            self.catalog_list = self.catalog.values.tolist()

            # TODO - Add more when catalog no have RA-DEC
            # 1. check which item don't have ra dec
            # 2. query ra-dec from astroquery and append to the colum of catalog_list

            # create list of source
            for source in self.catalog_list:
                self.source_list.append(source[0])

            # convert string to degree
            row = 0
            self.catalog_degree = self.catalog.values.tolist()
            for colum in self.catalog_degree:
                # sourcename, system, Epoch, RA, Dec, LSR, !Commnet
                ra_decx = colum[3] + " " + colum[4]
                coordinates = SkyCoord(ra_decx, unit=(u.hourangle, u.deg))
                self.catalog_degree[row][3] = str(coordinates.ra.degree)
                self.catalog_degree[row][4] = str(coordinates.dec.degree)
                row += 1
            # convert string to redian
            row = 0
            self.catalog_radian = self.catalog.values.tolist()
            for colum in self.catalog_radian:
                # sourcename, system, Epoch, RA, Dec, LSR, !Commnet
                ra_decx = colum[3] + " " + colum[4]
                coordinates = SkyCoord(ra_decx, unit=(u.hourangle, u.deg))
                self.catalog_radian[row][3] = str(coordinates.ra.radian)
                self.catalog_radian[row][4] = str(coordinates.dec.radian)
                row += 1
            # convert string to hmr
            row = 0
            self.catalog_hms = self.catalog.values.tolist()
            for colum in self.catalog_hms:
                # sourcename, system, Epoch, RA, Dec, LSR, !Commnet
                ra_decx = colum[3] + " " + colum[4]
                coordinates = SkyCoord(ra_decx, unit=(u.hourangle, u.deg))
                self.catalog_hms[row][3] = str(coordinates.ra.hms)
                self.catalog_hms[row][4] = str(coordinates.dec.hms)
                row += 1

            del row

            return self.catalog

        except Exception as error:
            self.logger.log(logging.ERROR, "Failed to get catalog : %s. " % error)


if __name__ == "__main__":
    # Interface to ACS components
    # Import PySimpleClient here because this import will block and wait for ACS Manager to run.
    # If we import outside of __main__, sphinx autodoc will import PySimpleClient during the
    # make build process and block waiting for Manager -but we should not require Manager running
    # during make build.
    from Acspy.Clients.SimpleClient import PySimpleClient

    # Edit command prompt display----------------------------------------------
    # TODO: customize prompts
    # ip = get_ipython()
    # ip.prompts = Nash.Prompt(ip)

    # -------------------------------------------------------------------------
    logger = logging.getLogger("nash")
    # Set propagate = True to use the same screen and file handler as
    # the parent logger 'nash'.
    logger.propagate = True
    Session._configure_logger(logger)
    logger.debug("Started log for nash")

    logger.log(logging.NOTICE, "Narit Shell (nash) - TNRT")

    git_info_dir = os.getenv("INTROOT") + "/config/"
    logger.log(
        logging.NOTICE,
        "Checking version information from files in {}...".format(git_info_dir),
    )

    with open(git_info_dir + "git_url.txt", "rt") as git_file:
        logger.log(logging.INFO, "URL: {}".format(git_file.read().strip()))

    with open(git_info_dir + "git_tag.txt", "rt") as git_file:
        logger.log(logging.INFO, "RELEASE TAG: {}".format(git_file.read().strip()))

    with open(git_info_dir + "git_branch.txt", "rt") as git_file:
        logger.log(logging.INFO, "BRANCH: {}".format(git_file.read().strip()))

    with open(git_info_dir + "git_commit.txt", "rt") as git_file:
        logger.log(logging.INFO, "COMMIT: {}".format(git_file.read().strip()))

    with open(git_info_dir + "git_date.txt", "rt") as git_file:
        logger.log(logging.INFO, "DATE: {}".format(git_file.read().strip()))

    logger.log(
        logging.NOTICE,
        "This shell depends on ACS ORBSRVC, Manager, Containers activated.",
    )
    logger.log(
        logging.NOTICE,
        "If Component is already activated, Nash will get a reference to the same Component",
    )
    logger.log(
        logging.NOTICE,
        "If Component is not activated, Nash will activate the Component and keep a reference",
    )
    logger.log(logging.NOTICE, "Open acscommandcenter first and start containers.")
    logger.log(
        logging.NOTICE,
        "Set log level for display with loglevel({DEBUG, INFO, WARNING, ERROR, CRITICAL, SILENT})",
    )

    # Create an instance of class Session.  Constructor is minimal so it declares a few objects and always return successful
    session = Session("session")

    # Connect the ACS components and check time syncronization.
    session._initialize()

    # Register a function to release nash references to all ACS components if we
    # exit / quit ipython.
    # This must be after session._initialize().  For some reason, if we register this atexit function before
    # connecting components with session._initialize, the exit condition doesn't work the way we want.  It deletes
    # the PySimplClient objects before they can release the components gracefully.
    atexit.register(session._handler_release_all)

    # Create "shortcut" function aliases in the namespace of this interactive session.
    # Convenient for scripts and interactive use.

    # Session
    log = session.log
    set_loglevel = session.set_loglevel
    testlog = session.testlog
    waitrel = session.waitrel
    waitabs = session.waitabs
    pause = session.pause
    run = session.run
    run_unittest = session.run_unittest

    # Antenna
    is_remote = session.antenna.is_remote
    remote = session.antenna.remote
    local = session.antenna.local
    get_site_location = session.antenna.get_site_location
    activate = session.antenna.activate
    deactivate = session.antenna.deactivate
    stop = session.antenna.stop
    reset = session.antenna.reset
    boot = session.antenna.boot
    goto_azel = session.antenna.goto_azel
    goto_platform = session.antenna.goto_platform
    block_until_arrived = session.antenna.block_until_arrived
    stow = session.antenna.stow
    unstow = session.antenna.unstow
    get_vertex_status = session.antenna.get_vertex_status
    get_status = session.antenna.get_status
    open_vertex = session.antenna.open_vertex
    close_vertex = session.antenna.close_vertex
    get_velocity = session.antenna.get_velocity
    set_velocity = session.antenna.set_velocity
    set_pmodel = session.antenna.set_pmodel
    clear_pmodel = session.antenna.clear_pmodel
    set_refraction = session.antenna.set_refraction
    clear_refraction = session.antenna.clear_refraction
    set_axis_position_offsets = session.antenna.set_axis_position_offsets
    clear_axis_position_offsets = session.antenna.clear_axis_position_offsets
    set_constant_focus_offsets = session.antenna.set_constant_focus_offsets
    clear_constant_focus_offsets = session.antenna.clear_constant_focus_offsets
    set_hxp_el_model = session.antenna.set_hxp_el_model
    clear_hxp_el_model = session.antenna.clear_hxp_el_model
    set_grs = session.antenna.set_grs

    # Scan
    add_scan = session.scan.add_scan
    run_queue = session.scan.run_queue
    clear_queue = session.scan.clear_queue
    stop_queue = session.scan.stop_queue
    get_scan_id_lastrun = session.scan.get_scan_id_lastrun
    get_tsys = session.scan.get_tsys
    get_focusfit = session.scan.get_focusfit
    get_az_el_vtopo = session.scan.get_az_el_vtopo

    # Catalog
    get_catalog = session.catalog.get_catalog
    get_TLE = session.catalog.get_TLE

# End __main__
