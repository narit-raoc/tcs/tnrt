# Project: TNRT - Thai National Radio Telescope
# Organization: NARIT - National Astronomical Research Institute of Thailand
# Author: Spiro Sarris
# Date: 2020.11.05

# TypeConverterNash module includes classes and functions to convert data types between
# * ACS notification channel classes (CORBA / OMG IDL structs),  CORBA / IDL enum type
# and other native Python types for session

# Conversion from ctypes to python dictionaries is simple - use from_buffer command.
# however, IDL structs do not have such a convenient function.  CORBA ValueType could
# be used instead of struct and enable the use of a Factory function to construct, but
# This technique is easier becuase data always moves between ACU and Antenna component
# locally on this machine.

# NOTE!  convert ctypes array to list using [:] .  This allows us to copy into the IDL
# struct without manual iteration.

# CORBA IDL Types
import tnrtAntennaMod

axis_properties = {
    "AZ": {"idl_enum": tnrtAntennaMod.AZ, "info": "AZIMUTH"},
    "EL": {"idl_enum": tnrtAntennaMod.EL, "info": "ELEVATION"},
    "HXP": {"idl_enum": tnrtAntennaMod.HXP, "info": "M2 HEXAPOD"},
    "M3": {"idl_enum": tnrtAntennaMod.M3, "info": "M3 Nasmyth Mirror"},
    "M3R": {
        "idl_enum": tnrtAntennaMod.M3R,
        "info": "M3R Nasmyth Redirect (steer to M4A or M4B)",
    },
    "M4A": {"idl_enum": tnrtAntennaMod.M4A, "info": "M4A"},
    "M4B": {"idl_enum": tnrtAntennaMod.M4B, "info": "M4B"},
    "THU": {"idl_enum": tnrtAntennaMod.THU, "info": "THU Tetrapod Head Unit"},
    "GRS": {"idl_enum": tnrtAntennaMod.GRS, "info": "GRS Gravity Slider"},
}

hxp_el_properties = {
    "ASTRO": {
        "idl_enum": tnrtAntennaMod.astroM2ElMode,
        "info": "Astronomy HXP-EL offset lookup table",
    },
    "GEO": {
        "idl_enum": tnrtAntennaMod.geoM2ElMode,
        "info": "Geodesy HXP-EL offset lookup table",
    },
    "ZERO": {
        "idl_enum": tnrtAntennaMod.zeroM2ElMode,
        "info": "Zeros HXP-EL offset lookup table",
    },
}

axis_status_string = {
    tnrtAntennaMod.hasErrors: "has errors",
    tnrtAntennaMod.axis_inactive: "axis_inactive",
    tnrtAntennaMod.interlocked: "interlocked",
    tnrtAntennaMod.halfstowed: "half-stowed",
    tnrtAntennaMod.stowed: "stowed",
    tnrtAntennaMod.unknownState: "unknown state",
    tnrtAntennaMod.stopped: "stopped",
    tnrtAntennaMod.stowing: "going to stow position",
    tnrtAntennaMod.slewing: "slewing",
    tnrtAntennaMod.tracking: "tracking",
}

vertex_status_string = {
    0: "VertexInvalid",
    1: "VertexError",
    2: "VertexOpen",
    3: "VertexClosed",
    4: "VertexUndef",
    5: "VertexOpening",
}
