import json
import numpy as np
import copy
from astropy.time import Time
import ScanMod
import tnrtAntennaMod
import TypeConverterAntenna as tc
from astropy.coordinates import solar_system_ephemeris

import logging
from logging import DEBUG, INFO, WARNING, ERROR, CRITICAL

import redis # using for set metadata of TNRT_pulsar mode
import default_param_edd
import default_param_holofft
from CentralDB import CentralDB
import cdbConstants

# Define a unique object reference ID.  Set some optional parameters to use this
# default value so we can detect if the current value of the parameter came from the user
# or came from the default value.
OPTIONAL_PARAM_NOT_SPECIFIED = object()


def validate_cal_coord_params(
    offsource_x,
    offsource_y,
    offsource_duration,
    offsource_coord_system,
):
    """
    Validate user input and generate a valid CalCoordParams object if input is valid
    Else, raise an exception and explain errors.
    """
    # If all parameters are OPTIONAL_PARAM_NOT_SPECIFIED, then we know that user did not
    # provide any data for calibration reference coordinate. Use the placeholder AbstractCalCoord.
    # that will not generate any additional subscans
    if (
        (offsource_x is OPTIONAL_PARAM_NOT_SPECIFIED)
        and (offsource_y is OPTIONAL_PARAM_NOT_SPECIFIED)
        and (offsource_duration is OPTIONAL_PARAM_NOT_SPECIFIED)
        and (offsource_coord_system is OPTIONAL_PARAM_NOT_SPECIFIED)
    ):
        return ScanMod.AbstractCalCoordParams()

    # If one or more of the parameters is OPTIONAL_PARAM_NOT_SPECIFIED, raise an exception.
    # When we specify a calibration coord, all 4 parameters must have valid data to continue.
    for param in [offsource_x, offsource_y, offsource_duration, offsource_coord_system]:
        if param is OPTIONAL_PARAM_NOT_SPECIFIED:
            raise ValueError(
                "Parameter {} not specified.  If any of the 4 optional parameters are used, all 4 must be filled"
            )

    # Now, all parameters are filled.  Check if data is valid
    if offsource_duration < 0:
        raise ValueError("offsource_duration must be greater than zero")

    if offsource_coord_system.upper() == "HO":
        trackmode_enum = tnrtAntennaMod.azel
    elif offsource_coord_system.upper() == "EQ":
        trackmode_enum = tnrtAntennaMod.radec
    else:
        raise ValueError(
            "offsource_coord_system {} not found in [HO, ho, EQ, eq]".format(coord_system)
        )

    return ScanMod.CalCoordParams(offsource_x, offsource_y, offsource_duration, trackmode_enum)


def validate_axis(axis):
    # Process primary axis and translate to enum type from Scan.idl to use in ScanRaster or ScanMap
    if axis.upper() == "X":
        axis_enum = ScanMod.axis_x
    elif axis.upper() == "Y":
        axis_enum = ScanMod.axis_y
    else:
        raise ValueError("axis {} not found in [X, x, Y, y]".format(axis))

    return axis_enum


def validate_program_offset_coord_system(coord_system):
    # Process coord system and translate to enum type from Antenna.idl to use in SubscanSingle
    # Note : for program offset table, we can use azel or radec.  radecshortcut is not valid.
    # for program offset table.  radecshortcut has meaning only for program track table related
    # to AZ position to start tracking
    if coord_system.upper() == "HO":
        trackmode_enum = tnrtAntennaMod.azel
    elif coord_system.upper() == "EQ":
        trackmode_enum = tnrtAntennaMod.radec
    else:
        raise ValueError("coord_system {} not found in [HO, ho, EQ, eq]".format(coord_system))

    return trackmode_enum


class ScheduleParams:
    """
    A group of parameters to configure the schedule and ID of the scan.

    Parameters
    ----------
    project_id : string
            Project ID format TBD.  Used in output file names.
    obs_id : string
            Observer and Operator name.  Saved in MBFITS file headers as key OBSID
    scan_id : int, optional
            ID number (scan number) for this scan.  Unique must be unique within this observing session
            in order to identify the results after scan.
            default=auto increment
    priority : float, optional
            manual priority to insert the scan in the priority queue. (SS. not implemented yet)
            default=0
    start_mjd : float , optional
            UTC time in MJD to start the scan. [unit = day].  If start_mjd is not set in this paramter,
            the scan will go to the end of the queue (insertion order)
            default=0
    source_name : string, optional
            Name of the source / object that is observed in this scan
            default='undef_source'
    line_name : string, optional
            Name of the spectral line that is observed in this scan.
            default = 'undef line'

    Examples
    --------
    >>> delay_until_scheduled_start = 10
    >>> project_id = 'test script'
    >>> obs_id = 'SS'
    >>> start_mjd = (Time.now() + delay_until_scheduled_start * units.second).mjd
    >>> source_name = 'AAA'
    >>> line_name = 'BBB'
    >>> schedule_params = ScheduleParams(project_id, obs_id, start_mjd=start_mjd, source_name=source_name, line_name=line_name)

    Notes
    -----
    This is a wrapper class to handle optional parameters in Python and
    create the CORBA valuetype object to send to the Scan controller.
    """

    def __init__(
        self,
        project_id,
        obs_id,
        scan_id=0,
        priority=0,
        start_mjd=Time.now().mjd,
        source_name="undef_source",
        line_name="undef_line",
    ):
        self.objval = ScanMod.ScheduleParams(
            project_id, obs_id, scan_id, priority, start_mjd, source_name, line_name
        )

    def __repr__(self):
        return repr(self.objval)


class TrackingParamsHO:
    """
    Docstring

    Parameters
    ----------
    az : float
            Azimuth angle to track. [unit = deg]
    el : float
            Elevation angle to track. [unit = deg]
    user_pointing_correction_az : float, optional, default=0
            Temporary user pointing correction on top of the 9-value pointing model.  
            This value will be multiplied by 1/cos(EL) in real-time bye the ACU [unit = arcsec]
    user_pointing_correction_el : float, optional, default=0
            Temporary user pointing correction on top of 9-value pointing model. [unit = arcsec]
    elevation_min : float, optional, default=0
            User minimum elevation angle [unit = deg]

            ACU includes it's own soft and hard limits.  This parameter is for the user to set the minimum
            elevation for *this* scan to be higher than the ACU actual minimum.
    elevation_max : float, optional, default=0
            User maximum elevation angle [unit = deg]

            ACU includes it's own soft and hard limits.  This parameter is for the user to set the maximum
            elevation for *this* scan to be lower than than the ACU actual maximum.
    north_crossing : {True, False}, optional, default=True

            Has meaning only for EQ tracking.  Ignored for HO (azimuth, elevation) and TLE (satellite) tracking

            `True` : Antenna will cross the North (AZ = 0) if it is the shortest path to arrive at tracking coordinate.

            `False` : Antenna will not cross through AZ=0.  It must go the otehr direction to find the tracking coordinate.
    use_azel_tables : {True, False}, optional, default=False

            Has meaning only for EQ tracking.  Ignored for HO (azimuth, elevation) and TLE (satellite) tracking

            `True` :  Scan will generate AZ, EL program tables. not RADEC tables for EQ coords

            `False` : Scan will generate RADEC tables and load them into ACU.  Then ACU calculates realtime AZ, EL
    max_tracking_errors : int, optional, default=6
            Maximum number of allowed tracking errors before this scan aborts with failed status.
            Perhaps wind is too strong or the Antenna cannot stabilize on the tracking coodinate for some other reason..

    Examples
    --------
    >>> az = -40            # deg
    >>> el = 12.0           # deg
    >>> user_pointing_correction_az = 0.0 # arcsec
    >>> user_pointing_correction_el = 0.0 # arcsec
    >>> elevation_min = 20
    >>> elevation_max = 70
    >>> north_crossing = True
    >>> use_azel_tables = False
    >>> max_tracking_errors = 6
    >>> tracking_params = TrackingParamsHO(az, el)
    >>> tracking_params = TrackingParamsHO(az, el, user_pointing_correction_az, user_pointing_correction_el, elevation_min, elevation_max, north_crossing, use_azel_tables, max_tracking_errors)

    Notes
    -----
    This is a wrapper class to handle optional parameters in Python and
    create the CORBA valuetype object to send to the Scan controller.
    """

    def __init__(
        self,
        az,
        el,
        user_pointing_correction_az=0,
        user_pointing_correction_el=0,
        elevation_min=0,
        elevation_max=90,
        north_crossing=True,
        use_azel_tables=False,
        max_tracking_errors=6,
    ):
        tracking_common = ScanMod.TrackingParamsCommon(
            user_pointing_correction_az,
            user_pointing_correction_el,
            elevation_min,
            elevation_max,
            north_crossing,
            use_azel_tables,
            max_tracking_errors,
        )
        self.objval = ScanMod.TrackingParamsHO(az, el, tracking_common)

    def __repr__(self):
        return repr(self.objval)


class TrackingParamsTLE:
    """
    Docstring

    Parameters
    ----------
    tle_line0 : string
            TLE line0 - satellite name
    tle_line1 : string
            TLE line1 - satellite header info and timestamp of this TLE
    tle_line2 : string
            TLE line2 - satellite orbit data
    user_pointing_correction_az : float, optional, default=0
            Temporary user pointing correction on top of the 9-value pointing model.  
            This value will be multiplied by 1/cos(EL) in real-time bye the ACU [unit = arcsec]
    user_pointing_correction_el : float, optional, default=0
            Temporary user pointing correction on top of 9-value pointing model. [unit = arcsec]
    elevation_min : float, optional, default=0
            User minimum elevation angle [unit = deg]

            ACU includes it's own soft and hard limits.  This parameter is for the user to set the minimum
            elevation for *this* scan to be higher than the ACU actual minimum.
    elevation_max : float, optional, default=0
            User maximum elevation angle [unit = deg]

            ACU includes it's own soft and hard limits.  This parameter is for the user to set the maximum
            elevation for *this* scan to be lower than than the ACU actual maximum.
    north_crossing : {True, False}, optional, default=True

            Has meaning only for EQ tracking.  Ignored for HO (azimuth, elevation) and TLE (satellite) tracking

            `True` : Antenna will cross the North (AZ = 0) if it is the shortest path to arrive at tracking coordinate.

            `False` : Antenna will not cross through AZ=0.  It must go the otehr direction to find the tracking coordinate.
    use_azel_tables : {True, False}, optional, default=False

            Has meaning only for EQ tracking.  Ignored for HO (azimuth, elevation) and TLE (satellite) tracking

            `True` :  Scan will generate AZ, EL program tables. not RADEC tables for EQ coords

            `False` : Scan will generate RADEC tables and load them into ACU.  Then ACU calculates realtime AZ, EL
    max_tracking_errors : int, optional, default=6
            Maximum number of allowed tracking errors before this scan aborts with failed status.
            Perhaps wind is too strong or the Antenna cannot stabilize on the tracking coodinate for some other reason..

    Examples
    --------
    >>> tle_line0 = 'INTELSAT 22 (IS-22)     '
    >>> tle_line1 = '1 38098U 12011A   20070.00674396 -.00000076  00000-0  00000+0 0  9990'
    >>> tle_line2 = '2 38098   0.0020 184.2253 0002748 169.5430 248.8790  1.00272611 29083'
    >>> user_pointing_correction_az = 0.0    # arcsec
    >>> user_pointing_correction_el = 0.0    # arcsec
    >>> elevation_min = 20      # deg
    >>> elevation_max = 70      # deg
    >>> north_crossing = True
    >>> use_azel_tables = False
    >>> max_tracking_errors = 6
    >>> tracking_params = TrackingParamsTLE(tle_line0, tle_line1, tle_line2)
    >>> tracking_params = TrackingParamsTLE(tle_line0, tle_line1, tle_line2, user_pointing_correction_az, user_pointing_correction_el, elevation_min, elevation_max, north_crossing, use_azel_tables, max_tracking_errors)

    Notes
    -----
    This is a wrapper class to handle optional parameters in Python and
    create the CORBA valuetype object to send to the Scan controller.
    """

    def __init__(
        self,
        tle_line0,
        tle_line1,
        tle_line2,
        user_pointing_correction_az=0,
        user_pointing_correction_el=0,
        elevation_min=0,
        elevation_max=90,
        north_crossing=True,
        use_azel_tables=False,
        max_tracking_errors=6,
    ):
        tracking_common = ScanMod.TrackingParamsCommon(
            user_pointing_correction_az,
            user_pointing_correction_el,
            elevation_min,
            elevation_max,
            north_crossing,
            use_azel_tables,
            max_tracking_errors,
        )
        self.objval = ScanMod.TrackingParamsTLE(tle_line0, tle_line1, tle_line2, tracking_common)

    def __repr__(self):
        return repr(self.objval)


class TrackingParamsEQ:
    """
    Track a celestial object using RA, DEC coordinates in ICRS coordinate system.

    Parameters
    ----------
    ra : float
            Right ascension in ICRS (J2000) coordinate reference system. unit = deg.
    dec : float
            Declination in ICRS (J2000) coordinate reference system. unit = deg.
    pm_ra : float, optional
            proper motion of right ascension [unit = mas / year], default=0

            https://docs.astropy.org/en/stable/coordinates/velocities.html
    pm_dec : float, optional
            proper motion of right declination. [unit = mas / year], default=0

            https://docs.astropy.org/en/stable/coordinates/velocities.html
    parallax :  float, optional
            parallax. [unit = arcsec], default=0
    radial_velocity : float, optional
            The component of the velocity along the line-of-sight (i.e., the radial direction).
            [unit = km / s], default=0, + is moving away from observer. (-) approaching observer
    send_icrs_to_acu : {True, False}, optional, default = False
            The current version (July 2022) of ACU tracking algorithm for RA, DEC expects
            the RA, DEC input parameters for the program track table to be Geoentric Apparent
            coordinates (adjusted from ICRS catalog to actual date and time of observation).
            The default behavior is that NARIT TCS transforms the user input RA, DEC
            from the script (ICRS) into Geocentric Apparent RA, DEC before sending to ACU program
            track table.  send_icrs_to_acu=False bypasses the transform and sends ICRS RA, DEC
            to the ACU program track table without any transform.  This function can be useful for
            debugging future releases of the ACU.
    user_pointing_correction_az : float, optional, default=0
            Temporary user pointing correction on top of the 9-value pointing model.  
            This value will be multiplied by 1/cos(EL) in real-time bye the ACU [unit = arcsec]
    user_pointing_correction_el : float, optional, default=0
            Temporary user pointing correction on top of 9-value pointing model. [unit = arcsec]
    elevation_min : float, optional, default=0
            User minimum elevation angle [unit = deg]

            ACU includes it's own soft and hard limits.  This parameter is for the user to set the minimum
            elevation for *this* scan to be higher than the ACU actual minimum.
    elevation_max : float, optional, default=0
            User maximum elevation angle [unit = deg]

            ACU includes it's own soft and hard limits.  This parameter is for the user to set the maximum
            elevation for *this* scan to be lower than than the ACU actual maximum.
    north_crossing : {True, False}, optional, default=True

            Has meaning only for EQ tracking.  Ignored for HO (azimuth, elevation) and TLE (satellite) tracking

            `True` : Antenna will cross the North (AZ = 0) if it is the shortest path to arrive at tracking coordinate.

            `False` : Antenna will not cross through AZ=0.  It must go the otehr direction to find the tracking coordinate.
    use_azel_tables : {True, False}, optional, default=False

            Has meaning only for EQ tracking.  Ignored for HO (azimuth, elevation) and TLE (satellite) tracking

            `True` :  Scan will generate AZ, EL program tables. not RADEC tables for EQ coords

            `False` : Scan will generate RADEC tables and load them into ACU.  Then ACU calculates realtime AZ, EL
    max_tracking_errors : int, optional, default=6
            Maximum number of allowed tracking errors before this scan aborts with failed status.
            Perhaps wind is too strong or the Antenna cannot stabilize on the tracking coodinate for some other reason..

    Examples
    --------
    >>> ra = -150           # deg
    >>> dec = 20            # deg
    >>> pm_ra = 0           # mas / year
    >>> pm_dec = 0          # mas / year
    >>> parallax = 0        # arcsec
    >>> radial_velocity = 0     # km / s
    >>> user_pointing_correction_az = 0.0    # arcsec
    >>> user_pointing_correction_el = 0.0    # arcsec
    >>> elevation_min = 20      # deg
    >>> elevation_max = 70      # deg
    >>> north_crossing = True
    >>> use_azel_tables = False
    >>> max_tracking_errors = 6
    >>> tracking_params = TrackingParamsEQ(ra, dec)
    >>> tracking_params = TrackingParamsEQ(ra, dec, pm_ra, pm_dec, parallax, radial_velocity, user_pointing_correction_az, user_pointing_correction_el, elevation_min, elevation_max, north_crossing, use_azel_tables, max_tracking_errors)
    >>> tracking_params = TrackingParamsEQ(ra, dec, send_icrs_to_acu=True)


    Notes
    -----
    This is a wrapper class to handle optional parameters in Python and
    create the CORBA valuetype object to send to the Scan controller.
    """

    def __init__(
        self,
        ra,
        dec,
        pm_ra=0,
        pm_dec=0,
        parallax=0,
        radial_velocity=0,
        send_icrs_to_acu=False,
        user_pointing_correction_az=0,
        user_pointing_correction_el=0,
        elevation_min=0,
        elevation_max=90,
        north_crossing=True,
        use_azel_tables=False,
        max_tracking_errors=6,
    ):
        tracking_common = ScanMod.TrackingParamsCommon(
            user_pointing_correction_az,
            user_pointing_correction_el,
            elevation_min,
            elevation_max,
            north_crossing,
            use_azel_tables,
            max_tracking_errors,
        )
        self.objval = ScanMod.TrackingParamsEQ(
            ra, dec, pm_ra, pm_dec, parallax, radial_velocity, send_icrs_to_acu, tracking_common
        )

    def __repr__(self):
        return repr(self.objval)


class TrackingParamsSS:
    """
    Docstring

    Parameters
    ----------
    planet : string, required
            Choose from (sun, mercury, venus, earth-moon-barycenter, earth, moon, mars, jupiter, saturn, uranus, neptune, pluto)
    user_pointing_correction_az : float, optional, default=0
            Temporary user pointing correction on top of the 9-value pointing model.  
            This value will be multiplied by 1/cos(EL) in real-time bye the ACU [unit = arcsec]
    user_pointing_correction_el : float, optional, default=0
            Temporary user pointing correction on top of 9-value pointing model. [unit = arcsec]
    elevation_min : float, optional, default=0
            User minimum elevation angle [unit = deg]

            ACU includes it's own soft and hard limits.  This parameter is for the user to set the minimum
            elevation for *this* scan to be higher than the ACU actual minimum.
    elevation_max : float, optional, default=0
            User maximum elevation angle [unit = deg]

            ACU includes it's own soft and hard limits.  This parameter is for the user to set the maximum
            elevation for *this* scan to be lower than than the ACU actual maximum.
    north_crossing : {True, False}, optional, default=True

            Has meaning only for EQ tracking.  Ignored for HO (azimuth, elevation) and TLE (satellite) tracking

            `True` : Antenna will cross the North (AZ = 0) if it is the shortest path to arrive at tracking coordinate.

            `False` : Antenna will not cross through AZ=0.  It must go the otehr direction to find the tracking coordinate.
    use_azel_tables : {True, False}, optional, default=False

            Has meaning only for EQ tracking.  Ignored for HO (azimuth, elevation) and TLE (satellite) tracking

            `True` :  Scan will generate AZ, EL program tables. not RADEC tables for EQ coords

            `False` : Scan will generate RADEC tables and load them into ACU.  Then ACU calculates realtime AZ, EL
    max_tracking_errors : int, optional, default=6
            Maximum number of allowed tracking errors before this scan aborts with failed status.
            Perhaps wind is too strong or the Antenna cannot stabilize on the tracking coodinate for some other reason..

    Examples
    --------
    >>> planet = 'sun'
    >>> user_pointing_correction_az = 0.0    # arcsec
    >>> user_pointing_correction_el = 0.0    # arcsec
    >>> elevation_min = 20      # deg
    >>> elevation_max = 70      # deg
    >>> north_crossing = True
    >>> use_azel_tables = False
    >>> max_tracking_errors = 6
    >>> tracking_params = TrackingParamsSS(planet)
    >>> tracking_params = TrackingParamsSS(planet, user_pointing_correction_az, user_pointing_correction_el, elevation_min, elevation_max, north_crossing, use_azel_tables, max_tracking_errors)

    Notes
    -----
    This is a wrapper class to handle optional parameters in Python and
    create the CORBA valuetype object to send to the Scan controller.
    """

    def __init__(
        self,
        planet,
        user_pointing_correction_az=0,
        user_pointing_correction_el=0,
        elevation_min=0,
        elevation_max=90,
        north_crossing=True,
        use_azel_tables=False,
        max_tracking_errors=6,
    ):
        solar_system_ephemeris.set("jpl")
        if planet not in solar_system_ephemeris.bodies:
            raise ValueError("Value error: wrong planet")

        tracking_common = ScanMod.TrackingParamsCommon(
            user_pointing_correction_az,
            user_pointing_correction_el,
            elevation_min,
            elevation_max,
            north_crossing,
            use_azel_tables,
            max_tracking_errors,
        )
        self.objval = ScanMod.TrackingParamsSS(planet, tracking_common)

    def __repr__(self):
        return repr(self.objval)


class ScantypeParamsCalColdload:
    """
    Calibration scan that uses cold load reference for Y-factor calculation.

    Parameters
    ----------
    time_per_phase : float
            time to stay at each point of the pattern while recording data . [unit = sec]
    offsource_x : float
            Offset longitude (RA or AZ depends on `coord_type`) from tracking coordinate to use for reference calibration [unit = arcsec]
    offsource_y : float
            Offset latitude (DEC or EL depends on `coord_type`) from tracking coordinate to use for reference calibration [unit = arcsec]
    offsource_duration : float
            length of time to record data. [unit = seconds]
    offsource_coord_system : {'HO' , 'EQ'}
            `HO` : x -> Azimuth, y -> Elevation

            `EQ` : x -> Right Ascension, y -> Declination


    Examples
    --------
    >>> time_per_phase = 2.0 # sec
    >>> cal_coord_dx = 0
    >>> cal_coord_dy = -0
    >>> cal_coord_duration = 1
    >>> cal_coord_system = "HO"
    >>> scantype_params = ScantypeParamsCalColdload(
        time_per_phase,
        cal_coord_dx,
        cal_coord_dy,
        cal_coord_duration,
        cal_coord_system
        )

    Notes
    -----
    .. todo:: explain cold load
    """

    def __init__(
        self,
        time_per_phase,
        offsource_x,
        offsource_y,
        offsource_duration,
        offsource_coord_system,
    ):

        cal_coord_params = validate_cal_coord_params(
            offsource_x, offsource_y, offsource_duration, offsource_coord_system
        )

        self.objval = ScanMod.ScantypeParamsCalColdload(time_per_phase, cal_coord_params)

    def __repr__(self):
        return repr(self.objval)


class ScantypeParamsCalHotload:
    """
    Calibration scan that uses hot load reference for Y-factor calculation.

    Parameters
    ----------
    time_per_phase : float
            time to stay at each point of the pattern while recording data . [unit = sec]
    offsource_x : float
            Offset longitude (RA or AZ depends on `coord_type`) from tracking coordinate to use for reference calibration [unit = arcsec]
    offsource_y : float
            Offset latitude (DEC or EL depends on `coord_type`) from tracking coordinate to use for reference calibration [unit = arcsec]
    offsource_duration : float
            length of time to record data. [unit = seconds]
    offsource_coord_system : {'HO' , 'EQ'}
            `HO` : x -> Azimuth, y -> Elevation

            `EQ` : x -> Right Ascension, y -> Declination

    Examples
    --------
    >>> time_per_phase = 2.0 # sec
    >>> cal_coord_dx = 0
    >>> cal_coord_dy = -0
    >>> cal_coord_duration = 1
    >>> cal_coord_system = "HO"
    >>> scantype_params = ScantypeParamsCalHotload(
        time_per_phase,
        cal_coord_dx,
        cal_coord_dy,
        cal_coord_duration,
        cal_coord_system
        )

    Notes
    -----
    .. todo:: explain hot load
    """

    def __init__(
        self,
        time_per_phase,
        offsource_x,
        offsource_y,
        offsource_duration,
        offsource_coord_system,
    ):
        cal_coord_params = validate_cal_coord_params(
            offsource_x, offsource_y, offsource_duration, offsource_coord_system
        )

        self.objval = ScanMod.ScantypeParamsCalHotload(time_per_phase, cal_coord_params)

    def __repr__(self):
        return repr(self.objval)


class ScantypeParamsCalNoise:
    """
    Calibration scan that uses noise source mode as a reference for Y-factor calculation.

    Parameters
    ----------
    time_per_phase : float
            time to stay at each point of the pattern while recording data . [unit = sec]
    offsource_x : float
            Offset longitude (RA or AZ depends on `coord_type`) from tracking coordinate to use for reference calibration [unit = arcsec]
    offsource_y : float
            Offset latitude (DEC or EL depends on `coord_type`) from tracking coordinate to use for reference calibration [unit = arcsec]
    offsource_duration : float
            length of time to record data. [unit = seconds]
    offsource_coord_system : {'HO' , 'EQ'}
            `HO` : x -> Azimuth, y -> Elevation

            `EQ` : x -> Right Ascension, y -> Declination

    Examples
    --------
    >>> time_per_phase = 2.0 # sec
    >>> cal_coord_dx = 0
    >>> cal_coord_dy = -0
    >>> cal_coord_duration = 1
    >>> cal_coord_system = "HO"
    >>> scantype_params = ScantypeParamsCalNoise(
        time_per_phase,
        cal_coord_dx,
        cal_coord_dy,
        cal_coord_duration,
        cal_coord_system
        )

    Notes
    -----
    Point antenna to the OFF position, and then switch on and off noise diode.

    When the noise diode is on, we have power of sky plus power of noise source:

    .. math:: P_{sky+ns} = kGB(T_{sys} + T_{ns})

    When the noise diode is off, we have power of sky:

    .. math:: P_{sky} = kGB(T_{sys})

    Thus,

    .. math:: {{P_{sky+ns} - P_{sky} } \over {P_{sky}}} = {T_{ns} \over T_{sys}}

    Given,

    .. math:: {Y_{1}} = {{P_{sky + ns}} \over {P_{sky}}}

    Solve for:

    .. math:: {T_{sys}} = {{T_{ns}} \over {Y_{1} - 1}}

    Point antenna to OFF position (P sky ), and then ON position(P source ), we have

    .. math:: P_{sky} = kGB(T_{sys})

    and

    .. math:: P_{source} = kGB(T_{A} + T_{sys})

    Thus,

    .. math:: {{P_{source} - P_{sky} } \over {P_{sky}}} = {T_{A} \over T_{sys}}

    Given,

    .. math:: {Y_{2}} = {{P_{source}} \over {P_{sky}}}

    Solve for:

    .. math:: {T_{A}} = {{T_{sys}} \over { Y_{2}-1}}
    """

    def __init__(
        self,
        time_per_phase,
        offsource_x,
        offsource_y,
        offsource_duration,
        offsource_coord_system,
    ):

        cal_coord_params = validate_cal_coord_params(
            offsource_x, offsource_y, offsource_duration, offsource_coord_system
        )

        self.objval = ScanMod.ScantypeParamsCalNoise(time_per_phase, cal_coord_params)

    def __repr__(self):
        return repr(self.objval)


class ScantypeParamsCross:
    """
    Cross-pointing scan (OTF) while tracking object defined by tracking_params.
    Use this to calculate {AZ , EL} pointing offsets

    .. image:: ../images/ScantypeParamsCross.png

    Parameters
    ----------
    arm_length : float
            length of arms of the cross. [unit = arcsec]
    time_per_arm : float
            time to traverse one arm of the cross while recording data. [unit = sec]
    win_min : int, optional. default = -arm_length / 2
            for post-processing result.
            Minimum value of window in which to curve-fit the gaussian to data [unit = arcsec]
    win_max : int, , optional. default = arm_length / 2
            for post-processing result.
            Maximum value of window in which to curve-fit the gaussian to data. [unit = arcsec]
    double_cross : {True, False}, optional. default = False
            `False` Traverse the cross only in one direction positive `p` (- az to + az, then -el to +el)

            `True` Traverse the cross both directions (- az to + az, +az to -az, -el to +el, +el to -el)

    offsource_x : float
            Offset longitude (RA or AZ depends on `coord_type`) from tracking coordinate to use for reference calibration [unit = arcsec]
    offsource_y : float
            Offset latitude (DEC or EL depends on `coord_type`) from tracking coordinate to use for reference calibration [unit = arcsec]
    offsource_duration : float
            length of time to record data. [unit = seconds]
    offsource_coord_system : {'HO' , 'EQ'}
            `HO` : x -> Azimuth, y -> Elevation

            `EQ` : x -> Right Ascension, y -> Declination

    Notes
    -----
    If the user provides any one of the offsource calibration coordinate parameters, they must
    provide all 4.

    Examples
    --------
    >>> # Not including off-source calibration reference before and after
    >>> arm_length = 7200 # arcsec
    >>> time_per_arm = 30.0 # sec
    >>> win_min = 1000 # arcsec
    >>> win_max = 1000 # arcsec
    >>> scantype_params = ScantypeParamsCross(arm_length, time_per_arm, win_min, win_max, double_cross=True)
    >>> schedule_params = ...
    >>> tracking_params = ...
    >>> frontend_params = ...
    >>> backend_params = ...
    >>> add_scan(schedule_params, scantype_params, tracking_params, frontend_params, backend_params)
    >>> results = run_queue()
    >>> results
            {
            'filename' : string
            'xp' : {
                    'angle_error' : float [unit = arcsec],
                    'width' : float [unit = arcsec],
                    'ta_max' : float [unit = ?],
                    'snr' : float [unit = ?]
                    },
            'yp' : {
                    'angle_error' : float [unit = arcsec],
                    'width' : float [unit = arcsec],
                    'ta_max' : float [unit = ?],
                    'snr' : float [unit = ?]
                    },
            'xn' : {
                    'angle_error' : float [unit = arcsec],
                    'width' : float [unit = arcsec],
                    'ta_max' : float [unit = ?],
                    'snr' : float [unit = ?]
                    },
            'yn' : {
                    'angle_error' : float [unit = arcsec],
                    'width' : float [unit = arcsec],
                    'ta_max' : float [unit = ?],
                    'snr' : float [unit = ?]
                    }
            }

    >>> # Including off source calibration reference before and after
    >>> arm_length = 7200 # arcsec
    >>> time_per_arm = 30.0 # sec
    >>> win_min = 1000 # arcsec
    >>> win_max = 1000 # arcsec
    >>> cal_coord_dx = 3600
    >>> cal_coord_dy = 7200
    >>> cal_coord_duration = 1
    >>> cal_coord_system = "EQ"
    >>> scantype_params = ScantypeParamsCross(
        arm_length,
        time_per_arm,
        win_min,
        win_max,
        double_cross=True,
        cal_coord_dx,
        cal_coord_dy,
        cal_coord_duration,
        cal_coord_system
        )

    """

    def __init__(
        self,
        arm_length,
        time_per_arm,
        win_min=0,
        win_max=0,
        double_cross=False,
        offsource_x=OPTIONAL_PARAM_NOT_SPECIFIED,
        offsource_y=OPTIONAL_PARAM_NOT_SPECIFIED,
        offsource_duration=OPTIONAL_PARAM_NOT_SPECIFIED,
        offsource_coord_system=OPTIONAL_PARAM_NOT_SPECIFIED,
    ):
        # win_min and win_max are set to 0 to make them optional parameters.  If the values
        # are not set, choose default to include the entire subscan extent.
        if win_min == 0:
            win_min = int(-1 * arm_length / 2)

        if win_max == 0:
            win_max = int(arm_length / 2)

        cal_coord_params = validate_cal_coord_params(
            offsource_x, offsource_y, offsource_duration, offsource_coord_system
        )

        self.objval = ScanMod.ScantypeParamsCross(
            arm_length, time_per_arm, win_min, win_max, double_cross, cal_coord_params
        )

    def __repr__(self):
        return repr(self.objval)


class ScantypeParamsFivePoint:
    """
    Observe a 5 point pattern including OFF SOURCE reference position.
    Sequence is:
    OFF, -AZ, CENTER, +AZ, OFF, -EL, CENTER, +EL, OFF

    .. image:: ../images/ScantypeParamsFivePoint.png

    Parameters
    ----------
    arm_length : float
            Total width / height of the 5 point pattern. [unit = arcsec]
    time_per_point : float
            time to stay at each point of the pattern while recording data . [unit = sec]
    win_min : int, optional. default = -arm_length / 2
            for post-processing result.
            Minimum value of window in which to curve-fit the gaussian to data [unit = arcsec]
    win_max : int, , optional. default = arm_length / 2
            for post-processing result.
            Maximum value of window in which to curve-fit the gaussian to data. [unit = arcsec]
    offsource_x : float
            Offset longitude (RA or AZ depends on `coord_type`) from tracking coordinate to use for reference calibration [unit = arcsec]
    offsource_y : float
            Offset latitude (DEC or EL depends on `coord_type`) from tracking coordinate to use for reference calibration [unit = arcsec]
    offsource_duration : float
            length of time to record data. [unit = seconds]
    offsource_coord_system : {'HO' , 'EQ'}
            `HO` : x -> Azimuth, y -> Elevation

            `EQ` : x -> Right Ascension, y -> Declination

    Examples
    --------
    >>> # Not including off-source calibration reference before and after
    >>> arm_length = 7200 # arcsec
    >>> time_per_point = 30.0 # sec
    >>> win_min = 1000 # arcsec
    >>> win_max = 1000 # arcsec
    >>> scantype_params = ScantypeParamsFivePoint(
        arm_length,
        time_per_point,
        win_min,
        win_max)

    >>> # Including off source calibration reference before and after
    >>> arm_length = 7200 # arcsec
    >>> time_per_arm = 30.0 # sec
    >>> win_min = 1000 # arcsec
    >>> win_max = 1000 # arcsec
    >>> cal_coord_dx = 3600
    >>> cal_coord_dy = 7200
    >>> cal_coord_duration = 1
    >>> cal_coord_system = "EQ"
    >>> scantype_params = ScantypeParamsFivePoint(
        arm_length,
        time_per_point,
        win_min,
        win_max,
        cal_coord_dx,
        cal_coord_dy,
        cal_coord_duration,
        cal_coord_system
        )

    Notes
    -----
    This is a wrapper class to handle optional parameters in Python and
    create the CORBA valuetype object to send to the Scan controller.
    """

    def __init__(
        self,
        arm_length,
        time_per_point,
        win_min=0,
        win_max=0,
        offsource_x=OPTIONAL_PARAM_NOT_SPECIFIED,
        offsource_y=OPTIONAL_PARAM_NOT_SPECIFIED,
        offsource_duration=OPTIONAL_PARAM_NOT_SPECIFIED,
        offsource_coord_system=OPTIONAL_PARAM_NOT_SPECIFIED,
    ):
        # win_min and win_max are set to 0 to make them optional parameters.  If the values
        # are not set, choose default to include the entire subscan extent.
        if win_min == 0:
            win_min = int(-1 * arm_length / 2)

        if win_max == 0:
            win_max = int(arm_length / 2)

        cal_coord_params = validate_cal_coord_params(
            offsource_x, offsource_y, offsource_duration, offsource_coord_system
        )

        self.objval = ScanMod.ScantypeParamsFivePoint(
            arm_length, time_per_point, win_min, win_max, cal_coord_params
        )

    def __repr__(self):
        return repr(self.objval)


class ScantypeParamsFocus:
    """
    Focus scan starting at start_pos and ending at end_pos at the velocity along a specified
    axis while tracking a radio source {HO | EQ | TLE | SS}.

    If the focus axis is {x, y, z}, the hexapod moves linearly on the axis with linear velocity.

    If the focus axis is {tx, ty}, the hexapod rotates along the x or y axis with angular velocity.

    This focus pattern moves ony 1 axis even though ACU hexapod program track allows more complicated
    multi-axis motion.

    .. image:: ../images/ScantypeParamsFocus.png

    Parameters
    ----------
    axis : string {'x', 'y', 'z', 'tx', 'ty'}, optional. default = 'z'
    start_pos : float, optional. default = minimum value for axis
            start position for selected  axis.

            axis = 'x', default = -35 [unit = mm]

            axis = 'y', default = -45 [unit = mm]

            axis = 'z', default = -85 [unit = mm]

            axis = {'tx', ty'}, default = -0.25 [unit = arcsec]
    end_pos : float, optional. default = maximum value for axis
            end position for selected    axis.

            axis = 'x', default = +35 [unit = mm]

            axis = 'y', default = +45 [unit = mm]

            axis = 'z', default = +85 [unit = mm]

            axis = {'tx', ty'}, default = +0.25 [unit = arcsec]
    velocity : float, optional. default = maximum value for axis
            linear or angular velocity of the focus scan

            if axis is linear {'x', 'y', 'z'}, [unit = mm/s], maximum = 2 mm/s

            if axis is rotational {'tx', 'ty'}, [unit = deg/s], maximum  = 0.04 deg/s


    Examples
    --------


    Notes
    -----
    This is a wrapper class to handle optional parameters in Python and
    create the CORBA valuetype object to send to the Scan controller.
    """

    def __init__(self, axis="z", start_pos=999, end_pos=999, velocity=999):
        """
        Note (SS 02/2021): default values of start_pos, end_pos, velocity are initialized to
        an impossible value 999 that matches the data type for the IDL interface.
        The constructor of ScanFocus will replace the 999 with correct default values before
        it creates the HXP program track table
        """
        try:
            hxp_axis_idl = tc.hxp_axis_properties[axis.lower()]
        except KeyError:
            raise KeyError(
                "{} not in available axes {}".format(axis.lower(), tc.hxp_axis_properties.keys())
            )

        # Create the ScantypeParams IDL object and note that we use absolute value of velocity.
        # This will simplify the calculation of hxp program track table calculation.  The start and
        # and pos can be in either direction (- to + or + to -) and still work.
        self.objval = ScanMod.ScantypeParamsFocus(hxp_axis_idl, start_pos, end_pos, abs(velocity))

    def __repr__(self):
        return repr(self.objval)


class ScantypeParamsManual:
    """
    Scan to record data from auxiliary systems.  No radio receiver data is recorded in this scan.  Useful for logging
    data from ACU, weather station, other sensors.

    Parameters
    ----------
    duration : float
            time to keep this scan alive while recording monitor data from ACU, WeatherStation, other sensors. [unit = sec]

    Examples
    --------
    >>> duration = 600.0 # sec
    >>> scantype_params = ScantypeParamsMonitor(duration)

    Notes
    -----
    This is a wrapper class to handle optional parameters in Python and
    create the CORBA valuetype object to send to the Scan controller.
    """

    def __init__(self, duration=3600):
        self.objval = ScanMod.ScantypeParamsManual(duration)

    def __repr__(self):
        return repr(self.objval)


class ScantypeParamsOnSource:
    """
    Stay on source at center tracking coordinate defined in tracking_params

    .. image:: ../images/ScantypeParamsOnSource.png

    Parameters
    ----------
    duration : float
            length of time to record data. [unit = seconds]
    offsource_x : float
            Offset longitude (RA or AZ depends on `coord_type`) from tracking coordinate to use for reference calibration [unit = arcsec]
    offsource_y : float
            Offset latitude (DEC or EL depends on `coord_type`) from tracking coordinate to use for reference calibration [unit = arcsec]
    offsource_duration : float
            length of time to record data. [unit = seconds]
    offsource_coord_system : {'HO' , 'EQ'}
            `HO` : x -> Azimuth, y -> Elevation

            `EQ` : x -> Right Ascension, y -> Declination

    Examples
    --------
    >>> # Not including off-source calibration reference before and after
    >>> duration = 10.0
    >>> scantype_params = ScantypeParamsOnSource(duration)

    >>> # Including off source calibration reference before and after
    >>> duration = 10.0
    >>> cal_coord_dx = 3600
    >>> cal_coord_dy = 7200
    >>> cal_coord_duration = 1
    >>> cal_coord_system = "EQ"
    >> scantype_params = ScantypeParamsOnSource(
        duration,
        cal_coord_dx,
        cal_coord_dy,
        cal_coord_duration,
        cal_coord_system
        )

    Notes
    -----
    This is a wrapper class to handle optional parameters in Python and
    create the CORBA valuetype object to send to the Scan controller.
    """

    def __init__(
        self,
        duration,
        offsource_x=OPTIONAL_PARAM_NOT_SPECIFIED,
        offsource_y=OPTIONAL_PARAM_NOT_SPECIFIED,
        offsource_duration=OPTIONAL_PARAM_NOT_SPECIFIED,
        offsource_coord_system=OPTIONAL_PARAM_NOT_SPECIFIED,
    ):

        cal_coord_params = validate_cal_coord_params(
            offsource_x, offsource_y, offsource_duration, offsource_coord_system
        )

        self.objval = ScanMod.ScantypeParamsOnSource(duration, cal_coord_params)

    def __repr__(self):
        return repr(self.objval)


class ScantypeParamsMap:
    """
    Scans a set of parrallel lines of equal length and separated by consistent spacing 
    around a central tracking object.  Each line is a subscan of type `SubscanLine`.  
    The scan is OTF (On The Fly) which means the antenna
    moves though the line pattern while receiver and backend produces a steady stream of
    data packets.  The antenna does not stop at specific grid points like the Raster pattern.
    Angular positions of the antenna are interpolated onto timestamps of backend data packets at the end
    of each line (subscan) and saved in the data file.  
    Therefore, the time synchronization between ACU and backend data pipeline
    is required in order to assign correct angular position to received signal data.  This follows the same
    logic as Cross Scan.
    
    .. image:: ../images/ScantypeParamsMap.png

    Parameters
    ----------
    line_length : float
            length of arms of the cross. [unit = arcsec]
    time_per_line : float
            time to traverse one arm of the cross while recording data. [unit = sec]
    nlines : int (>=1)
            number of lines
    spacing : float
            distance between lines [arcsec]    
    axis: string, optional, default = "x"
        "x" : scan line along x axis, then step to next y coordinate

        "y" : scan line along y axis, then step to next x coordinate
    zigzag : {True, False}, optional, default = `False`
        `True` : line scan direction alternates between [-] to [+], then [+] to [-] for each line (faster)

        `False` : line always moves from [-] to [+]
    coord_system : {"HO", "EQ"}, optional, default = "EQ"
        "HO" {Azimuth, Elevation} is used to perform measurements that characterize the antenna itself. 
        For example: pointing model, beam pattern, Holography. line_length, spacing -> AZ, EL (EL, AZ if line_axis=='y')
        
        "EQ" {Right Ascension, Declination} is used to perform measurements in the natural coordinate system of 
        celestial objects.  For example: sky survey map. line_length, spacing -> RA, DEC (DEC, RA if line_axis=='y')
    subscans_per_cal : int, optional, default = 0
        < 1 means that this scan map does NOT perform on-source calibration subscans.
        Number of SubscanLines to run before return to center point and measure on-source for calibration.  
        `subscans_per_cal` must be a factor of `nlines`. If nlines % subscans_per_cal != 0, raise ValueError
    time_cal0: float, optional, default = 60 [unit = seconds]
        duration [sec] to point at the center once before  the OTF map pattern (use this subscan to calibrate the backend)
        If subscans_per_cal < 1, this parameter is not  used
    time_per_cal: float, optional, default = 10 [unit = seconds]
        duration [sec] to point at the center for each center calibration subscan that is interleaved between lines of the OTF map
        If subscans_per_cal < 1, this parameter is not  used
    """
    def __init__(
        self,
        line_length,
        time_per_line,
        nlines,
        spacing,
        axis="x",
        zigzag=False,
        coord_system="EQ",
        subscans_per_cal=0,
        time_cal0=60,
        time_per_cal=10,
        ):
        
        #Validate parameters
        if line_length <= 0:
            raise ValueError("line_length {} <= 0.  Value must be > 0".format(line_length))
        if time_per_line <= 0:
            raise ValueError("time_per_line {} <= 0.  Value must be > 0".format(time_per_line))
        if nlines < 1:
            raise ValueError("nlines {} < 1.  Value must be >= 1 ".format(nlines))
        if spacing <= 0:
            raise ValueError("spacing {} <= 0.  Value must be > 0".format(spacing))
        
        axis_enum = validate_axis(axis)

        if zigzag not in [True, False]:
            raise(ValueError("axis {} not in allowed values [True, False]".format(zigzag)))
        
        trackmode_enum = validate_program_offset_coord_system(coord_system)

        subscans_per_cal = int(subscans_per_cal)

        if subscans_per_cal >= 1 :
            if (nlines % subscans_per_cal) != 0:
                    raise ValueError("Invalid pattern. {}.nlines={} is not a multiple of {}.subscans_per_cal={}".format(
                        self.__class__.__name__, nlines, self.__class__.__name__, subscans_per_cal))

        if time_cal0 <= 0:
            raise ValueError("time_cal0 {} <= 0.  Value must be > 0".format(time_cal0))
        
        if time_per_cal <= 0:
            raise ValueError("time_per_cal {} <= 0.  Value must be > 0".format(time_per_cal))
    
        self.objval = ScanMod.ScantypeParamsMap(
            line_length,
            time_per_line,
            nlines,
            spacing,
            axis_enum,
            zigzag,
            trackmode_enum,
            subscans_per_cal,
            time_cal0,
            time_per_cal
            )

    def __repr__(self):
        return repr(self.objval)

class ScantypeParamsRaster:
    """
    Scans a 2D grid map around a central tracking coordinate.
    Stop at each point in the grid for `time_per_point` seconds to accumulate
    and record data from the receiver before it moves to the next grid point.
    Each point in the grid is a separate subscan of type `SubScanSingle`.  
    One point on the grid may receive many data packets from the receiver signal path.  
    The number of packets received per grid point = `time_per_point` / Backend actual_integration_time.

    .. image:: ../images/ScantypeParamsRaster.png

    Parameters
    ----------
    xlen : float
            length of pattern in y direction (AZ or RA depends on `coord_system`). [unit = arcsec]
    xstep : float
            distance between points in x direction.  [unit = arcsec]
    ylen : float
            length of pattern in y direction (EL or DEC depends on `coord_type`). [unit = arcsec]
    ystep : float
            distance between points in y direction.  [unit = arcsec]
    time_per_point : float
            time on each point of the grid. [unit = sec]
    zigzag : {True, False}, optional, default = `True`
            `True` : raster line alternates between [-] to [+], then [+] to [-] for each line (faster)

            `False` : raster line always moves from [-] to [+]
    primary_axis : {'x', 'y'}, optional, default = `x`
            'x' : step along x axis, then step to next y coordinate

            'y' : step along y axis, then step to next x coordinate
    coord_system : {`HO`, `EQ`}, optional, default = `EQ`
            `HO` : xlen, xstep-> Azimuth. ylen, ystep -> Elevation

            `EQ` : xlen, xstep -> Right Ascension. ylen, ystep -> Declination
    offsource_x : float
            Offset longitude (RA or AZ depends on `coord_type`) from tracking coordinate to use for reference calibration [unit = arcsec]
    offsource_y : float
            Offset latitude (DEC or EL depends on `coord_type`) from tracking coordinate to use for reference calibration [unit = arcsec]
    offsource_duration : float
            length of time to record data. [unit = seconds]
    offsource_coord_system : {'HO' , 'EQ'}
            `HO` : x -> Azimuth, y -> Elevation

            `EQ` : x -> Right Ascension, y -> Declination


    Examples
    --------
    >>> # Not including off-source calibration reference before and after
    >>> xlen = 3600
    >>> xstep = 720
    >>> ylen = 3600
    >>> ystep = 720
    >>> time_per_point = 1
    >>> zigzag = True
    >>> primary_axis = "X"
    >>> coord_system = "EQ"
    >>> scantype_params = ScantypeParamsRaster(
    >>>     xlen,
    >>>     xstep,
    >>>     ylen,
    >>>     ystep,
    >>>     time_per_point,
    >>>     zigzag,
    >>>     primary_axis,
    >>>     coord_system,
    >>>     )

    >>> # Including off source calibration reference before and after
    >>> xlen = 3600
    >>> xstep = 720
    >>> ylen = 3600
    >>> ystep = 720
    >>> time_per_point = 1
    >>> zigzag = True
    >>> primary_axis_enum = "X"
    >>> coord_system_enum = "EQ"
    >>> cal_coord_dx = 7200
    >>> cal_coord_dy = -7200
    >>> cal_coord_duration = 2
    >>> cal_coord_system = "EQ"
    >>> scantype_params = ScantypeParamsRaster(
    >>>     xlen,
    >>>     xstep,
    >>>     ylen,
    >>>     ystep,
    >>>     time_per_point,
    >>>     zigzag,
    >>>     primary_axis_enum,
    >>>     coord_system_enum,
    >>>     cal_coord_dx,
    >>>     cal_coord_dy,
    >>>     cal_coord_duration,
    >>>     cal_coord_system,
    >>>     )


    Notes
    -----
    This is a wrapper class to handle optional parameters in Python and
    create the CORBA valuetype object to send to the Scan controller.
    """

    def __init__(
        self,
        xlen,
        xstep,
        ylen,
        ystep,
        time_per_point,
        zigzag=True,
        primary_axis="x",
        coord_system="EQ",
        offsource_x=OPTIONAL_PARAM_NOT_SPECIFIED,
        offsource_y=OPTIONAL_PARAM_NOT_SPECIFIED,
        offsource_duration=OPTIONAL_PARAM_NOT_SPECIFIED,
        offsource_coord_system=OPTIONAL_PARAM_NOT_SPECIFIED,
    ):
        # Process x, y, time parameters
        if xlen <= 0:
            raise ValueError("xlen {} <= 0.  Value must be > 0")
        elif xstep <= 0:
            raise ValueError("xstep {} <= 0.  Value must be > 0")
        elif ylen <= 0:
            raise ValueError("ylen {} <= 0.  Value must be > 0")
        elif ystep <= 0:
            raise ValueError("ystep {} <= 0.  Value must be > 0")
        elif time_per_point <= 0:
            raise ValueError("time_per_point {} <= 0.  Value must be > 0")

        primary_axis_enum = validate_axis(primary_axis)

        trackmode_enum = validate_program_offset_coord_system(coord_system)

        cal_coord_params = validate_cal_coord_params(
            offsource_x, offsource_y, offsource_duration, offsource_coord_system
        )

        self.objval = ScanMod.ScantypeParamsRaster(
            xlen,
            xstep,
            ylen,
            ystep,
            time_per_point,
            zigzag,
            primary_axis_enum,
            trackmode_enum,
            cal_coord_params,
        )

    def __repr__(self):
        return repr(self.objval)


class ScantypeParamsSkydip:
    """
    Scan pattern at constant azimuth angle and scan from low elevation (direction of horizon)
    to high elevation (direction of zenith).  The result is one "line" type subscan that moves
    smoothly from low to high elevation while the receiver integrates and records data.

    Note (SS 02/2021): This implementation is different than the Yebes implementation that uses
    discrete steps in a tracking table.  I choose a continuous line sweep because I
    believe it makes sense for the curve fitting result processing.
    Each integration from the receiver will have an associated elevation angle in the data files
    (MBFITS DATAPAR.LATOFF or Gildas CLASS head.dri.apos )

    .. image:: ../images/ScantypeParamsSkydip.png

    Parameters
    ----------
    az : float
            fixed azimuth to perform the elevation skydip scan
    el_start : float
            end elevation angle. [unit = deg]
    el_end : float
            end elevation angle. [unit = deg]
    duration : float
            duration of the OTF subscan from low angle to high angle.
            Note. elevation velocity = (el_end - el_start) / duration.
            veloctiy must not exceed maximum value of 1.0 deg /s

    Examples
    --------
    >>> az = 25
    >>> el_start = 5
    >>> el_end = 80
    >>> duration = 160
    >>> scantype_params = ScantypeParamsSkydip(az, el_start, el_end, duration)

    Notes
    -----
    This is a wrapper class to handle optional parameters in Python and
    create the CORBA valuetype object to send to the Scan controller.
    """

    def __init__(self, az, el_start, el_end, duration):
        self.objval = ScanMod.ScantypeParamsSkydip(az, el_start, el_end, duration)

    def __repr__(self):
        return repr(self.objval)


class ScantypeParamsTimeSync:
    """
    TimeSync is a simple Scan that records status data from the
    ACU (Antenna Control Unit) to compare time between ACU and TCS.
    The ACU and TCS computers should be synchronized by NTP.
    The ACU status message includes a timestamp in MJD format that is the UTC system time of ACU
    When this message arrives (via LAN) to the TCS computer, another timestamp is added
    that indicates TCS OS system time in UTC.

    Parameters
    ----------
    duration : float
            length of time to record data. [unit = seconds]

    Examples
    --------
    >>> duration = 5.0
    >>> scantype_params = ScantypeParamsTimeSync(duration)

    Notes
    -----
    This is a wrapper class to handle optional parameters in Python and
    create the CORBA valuetype object to send to the Scan controller.
    """

    def __init__(self, duration):
        self.objval = ScanMod.ScantypeParamsTimeSync(duration)

    def __repr__(self):
        return repr(self.objval)


class FrontendParamsL:
    """
    .. todo:: add content
    """

    def __init__(self):
        self.objval = ScanMod.FrontendParamsL()

    def __repr__(self):
        return repr(self.objval)


class FrontendParamsCX:
    """
    .. todo:: add content
    """

    def __init__(self):
        self.objval = ScanMod.FrontendParamsCX()

    def __repr__(self):
        return repr(self.objval)


class FrontendParamsKu:
    """
    .. todo:: add content
    """

    def __init__(self):
        self.objval = ScanMod.FrontendParamsKu()

    def __repr__(self):
        return repr(self.objval)


class FrontendParamsK:
    """
    Parameter for FrontendK
    Frontend parameters related to Low Noise Amplifier (LNA) and digitizer functionality.

    Parameters :
    ------------
        auto_enable_lna (bool) : Determines if the LNA should be automatically enabled (true/false).
    
    Methods :
    ---------
        __init__(auto_enable_lna, digitizer_enable) : Initializes the parameters with the specified
        values and validates that the inputs are of boolean type. Raise a ValueError if the inputs are invalid.

        __repr__() : Returns a string representation of the FrontendParamsK object.
    
    Usage example :
    ---------------
        >>> auto_enable_lna = True
        >>> frontend_params = FrontendParamsK(auto_enable_lna)
    """

    def __init__(self, auto_enable_lna=False):
        # validate that user input is 'Boolean'       
        if not isinstance(auto_enable_lna, bool):
            raise ValueError("auto_enable_lna must be a boolean value")
        
        self.objval = ScanMod.FrontendParamsK(
            auto_enable_lna
        )

    def __repr__(self):
        return repr(self.objval)


class FrontendParamsOptical:
    """
    .. todo:: add content
    """

    def __init__(self):
        self.objval = ScanMod.FrontendParamsOptical()

    def __repr__(self):
        return repr(self.objval)

class RedisWriteError(Exception):
    pass

class BackendParamsEDD:
    """
    Parameters for EDD Backend (Effelsberg Direct Digitzation and Universal Software Backend)

    Parameters
    ----------
    preset_config_name : {'TNRT_dualpol_spectrometer_L' , 'TNRT_stokes_spectrometer_L' , 'TNRT_dualpol_spectrometer_K' , 'TNRT_stokes_spectrometer_K' ,  'TNRT_pulsar_L', 'TNRT_VLBI_L'}, optional. default='TNRT_dualpol_spectrometer_L'
            Name of the preset configuration.  Must match filename in edd_provisioning_tnrt repository, path /provision_descriptions/<preset_config_name>.json

    mock: {False | True}. optional, default=False
            `False` (default): Connect to the real EDD backend
            `True`: Do not connect to the real EDD Backend. It is a local process that simulates a spectrum and and procudes packets that
            look like data from the real EDD Backend when provisioned as `TNRT_dualpol_spectrometer_L`.

    Other Parameters
    ----------------
    **kwargs for 'TNRT_dualpol_spectrometer', 'TNRT_dualpol_spectrometer_L', 'TNRT_dualpol_spectrometer_K', `TNRT_stokes_spectrometer`, `TNRT_stokes_spectrometer_L`, `TNRT_stokes_spectrometer_K` : optional keyword parameters to overwrite settings in the preset configurations defined in `edd_privisioning_tnrt` project

            freq_res : `float`. unit = Hz, min = 1908.0, max = 976563.0
                    Analog bandwidth of receiver and sample rate of digitizer are constant.
                    Variable frequency resolution is achieved by variable FFT length in the spectrometer algorithm.
                    The EDD backend uses the parameter `fft_length` internally.

                    >>> sampling_rate = 4e9
                    >>> predecimation_factor = 2 # L-Band = 2, K Band = 1
                    >>> fft_length = (sampling_rate / predecimation_factor) / freq_res

                    .. Note:: fft_length must be a power of 2 (ex. 2048, 4096, 8192, ...).  The system will automatically calculate the next power of 2 above the "ideal" fft_length.  So, the actual frequency resolution will be finer than the requested frequency resolution in order to satisfy this power-of-2 requirement.

                    Actual fft_length used internally in EDD Spectrometer and resulting frequency resolution is calculated as follows:

                    >>> fft_length_power_of_2 = int(2 ** np.ceil(np.log2(fft_length)))
                    >>> actual_freq_res = (sampling_rate / predecimation_factor) / fft_length_power_of_2


            integration_time : `float`. unit = seconds
                    Integration time achieved by accumulation of digital data.  The EDD backend uses the parameter `naccumulate` internally.

                    >>> sampling_rate = 4e9
                    >>> predecimation_factor = 2 # L-Band = 2, K Band = 1
                    >>> naccumulate = requested_integration_time * (sampling_rate / predecimation_factor) / fft_length

                    .. Note:: naccumulate must be a power of 2 (ex. 2048, 4096, 8192, ...).  The system will automatically calculate the next power of 2 above the "ideal" naccumulate.  So, the actual integration time (time resolution) will be shorter (finer) than the requested integration time in order to satisfy this power-of-2 requirement.

                    .. Note:: Actual integration time depends on actual FFT length calculated above in frequency resolution section.

                    Actual accumulation length used internally in the EDD Spectrometer and resulting time resolution is calculated as follows:

                    >>> naccumulate_power_of_2 = int(2 ** np.floor(np.log2(naccumulate)))
                    >>> actual_integration_time = naccumulate_power_of_2 * (predecimation_factor / sampling_rate) * fft_length_power_of_2

    **kwargs for 'TNRT_pulsar', 'TNRT_pulsar_L' : optional keyword arguments to overwrite settings in the preset configuration 'TNRT_pulsar'.
            
            project : `string`
                    Output file path. The EDD pulsar will finish the process with itself, it's result will output path to the name of this parameter. 
                        
                    For example project = "tnrt_pulsar", the out put path should be 
                    
                    >>> "/hdd/mpifr_deploy/pipeline_data/timing1/test_pulsar" on the server that we deploy timing1.
                    >>> "/hdd/mpifr_deploy/pipeline_data/timing2/test_pulsar on the server that we deploy timing2.

            source_name : `string`
                    The EDD pulsar will select pulsar following this parameter from catalog ("/hdd/mpifr_deploy/pipeline_data/timing1/eff_psr_obs/tempo1/tzpar"). 
                    The catalog has many of "par" file which detail to pulsar parameter that EDD pulsar pipeline needs. 
                    
                    Note : source_name must start with capital "B" and must not have '/n' or any ' ' (space)

                    For example "source_name" = "B0332+5434"
                        ```
                        PSR	0332+5434 
                        RAJ	03:32:59.3469570182353 
                        DECJ	54:34:43.2453881363263 
                        PEPOCH	40621.0000000000000000 
                        F	1.3995435674553505D+00 
                        F1	-4.0145776451556378D-15
                        PMDEC	-1.3000000000000000D+01
                        PMRA	1.7000000000000000D+01 
                        POSEPOCH	40105.0000000000000000 
                        DM	2.6775999069213867D+01 
                        EPHEM	DE200
                        CLK	UTC(BIPM)   
                        TZRMJD	40621.000000
                        TZRFRQ	1408.000
                        TZRSITE	g
                        ```
            
            ra : `string`. format = "03h32m59.4096",
                    The EDD should read ra from par file like RAJ 03:32:59.3469570182353, but from now we let the operator team to config this parameter with themself.
                    This parameter does not affect ACU tracking, it just set metadata on redis "TELESCOPE_META_DATA"

            dec : `string`. format = "+54d34m43.329s",
                    The EDD should read ra from par file lie DECJ 54:34:43.2453881363263, but from now we let the operator team to config this parameter with themself.
                    This parameter does not affect ACU tracking, it just set metadata on redis "TELESCOPE_META_DATA"

    **kwargs for 'TNRT_VLBI_L' : optional keyword arguments to overwrite settings in the preset configuration.
            
            output_samplerate : float
                Output sample rate of the DDC block(s).  used to select the output bandwidth

            f_lo : float
                Digital "LO" local oscillator used to select the frequency of interest for DDC output.

            

    Examples
    --------
    >>> # Select backend EDD and use default configuration which is currenty set to "TNRT_dualpol_spectrometer_L"
    >>> backend_params = BackendParamsEDD()

    >>> # Select backend EDD and explicitly select L-band packetizer and H/V dualpol spectrometer
    >>> backend_params = BackendParamsEDD("TNRT_dualpol_spectrometer_L")

    >>> # Select backend EDD, choose dualpol (H/V) spectrometer, L-band packetizer, and override some settings
    >>> backend_params = BackendParamsEDD('TNRT_dualpol_spectrometer_L', freq_res=200000.0, integration_time=2.0)

    >>> # Select backend EDD, choose dualpol (H/V) spectrometer, K-band packetizer, and override some settings
    >>> backend_params = BackendParamsEDD('TNRT_dualpol_spectrometer_K', freq_res=200000.0, integration_time=2.0)

    >>> # Select backend EDD and choose stokes spectrometer using all default settings in edd_provisioning/provision_descriptions/TNRT_stokes_spectrometer_L.json
    >>> backend_params = BackendParamsEDD('TNRT_stokes_spectrometer_L')

    >>> # Select backend EDD, choose pulsar timing mode, and override some settings
    >>> backend_params = BackendParamsEDD('TNRT_pulsar_L', project="test_pulsar", source_name="B0332+5434", ra=03h32m59.4096 , dec=+54d34m43.329s)
    
    >>> # Select backend EDD, choose VLBI, L-band packetizer, and override some default settings
    >>> backend_params = BackendParamsEDD("TNRT_VLBI_L", output_samplerate=1000, f_lo=600e6)

    Notes
    -----
    This is a wrapper class to validate user input in Python and create the CORBA IDL ValueType object and send to Scan controller
    """

    def __init__(self, preset_config_name="TNRT_dualpol_spectrometer_L", mock=False, **kwargs):
        
        # Create a new logger for this class that is a child of logger named 'nash'
        self.logger = logging.getLogger("nash." + self.__class__.__name__)
        self.logger.propagate = True
        self.logger.handlers = []
        
        # Warning about deprecated names
        if preset_config_name in ["TNRT_dualpol_spectrometer", "TNRT_stokes_spectrometer", "TNRT_pulsar"]:
            self.logger.warning("Name '{}' is deprecated.  Please use the new name '{}_L'".format(preset_config_name, preset_config_name))
        
        # Validate preset config name
        try:
            allowed_user_parameters = default_param_edd.preset_config_name_setting[preset_config_name]['user_script_parameters']
        except KeyError:
            self.objval = None
            raise KeyError(
                'preset_config_name: "{}" not found in allowed preset configs.  Choose from {}'.format(
                    preset_config_name, list(default_param_edd.preset_config_name_setting.keys()))
                )
        
        # If no Exception yet, preset_config_name is OK
        # If kwargs is empty, use default config string and return.  
        if kwargs=={}:
            self.logger.info('kwargs not provided by user.  Set edd_configure_json="{}"')
            self.logger.info("Use default values from edd_provisioning_tnrt")
            edd_configure_json = "{}"
        else:
            self.logger.info("Validating optional parameters ...")
            # Check for valid kwargs
            for k in kwargs.keys():
                if k not in allowed_user_parameters:
                    self.objval = None
                    raise ValueError(
                        "kwarg: {} not found in allowed kwargs.  Choose from {}".format( k, list(allowed_user_parameters.keys()))
                    )
            self.logger.info("OK")
            # Generate the string for EDD configure
            edd_configure_json = self.gen_edd_config_json(preset_config_name, kwargs)
            self.logger.info("edd_configure_json: {}".format(edd_configure_json))

        # Package the preset_config_name and edd_configure_json string 
        # as a CORBA IDL ValueType so we can send to the BackendEDD 
        # component using ACS remote function call.
        self.objval = ScanMod.BackendParamsEDD(
            preset_config_name,
            edd_configure_json,
            mock
        )

    def gen_edd_config_json(self, preset_config_name, kwargs):
        """
        Now, we can generate the configure json string.  
        In th ecase of VLBI or Pulsar, the user script parameters are the same as
        the parameters used by EDD configure json string, so caluclation or
        translation is not required.

        In the case of spectrometer, the user script parameters are not the same as the 
        EDD configure parameters, so we have to calculate the values generate the actual 
        configure string for EDD.

        Example generate string for VLBI configure
        {
            "products": {
                "ddc_processor_1":{
                    "output_samplerate":16000000, 
                    "f_lo":[238000000]}
                }
        }


        Example generated string for pulsar configure
        "{}"

        Example generated string for spectrometer configure
        {
            "products": {
                "gated_spectrometer_0": {"fft_length": fft_length_power_of_2, "naccumulate": naccumulate_power_of_2},
                "gated_spectrometer_1": {"fft_length": fft_length_power_of_2, "naccumulate": naccumulate_power_of_2},
                }
        }


        """
        # Create a dictionary that has first key is product id.
        # Initialize the dictionary with default values of edd configure parameters
        config_dict = copy.deepcopy({"products" : dict.fromkeys(default_param_edd.preset_config_name_setting[preset_config_name]["product_id_list"], 
                                    default_param_edd.preset_config_name_setting[preset_config_name]["edd_config_parameters"])
                                    })

        self.logger.debug("Initialize default params dict: {}".format(config_dict))

        if preset_config_name in ["TNRT_VLBI_L"]:
            self.logger.info("preset_config_name is a type of VLBI.  Prepare configuration params for VLBI")
            # Get optional user script parameters
            try:
                output_samplerate = kwargs['output_samplerate']
            except KeyError:
                output_samplerate = default_param_edd.VLBI_DEFAULT_DDC_OUTPUT_SAMPLERATE
                self.logger.warning("output_samplerate not set. Use default value: {}".format(output_samplerate))
            
            try:
                f_lo = kwargs['f_lo']
                if type(f_lo) is not list:
                    self.logger.warning("f_lo is not list. conver to list [f_lo]")
                    f_lo = [f_lo]
            except KeyError:
                f_lo = default_param_edd.VLBI_DEFAULT_F_LO # type==list
                self.logger.warning("f_lo not set. Use default value: {}".format(f_lo))
            
            # Set the values of config dict prepare for edd configure
            for (product_name, params_dict) in config_dict["products"].items():
                params_dict["output_samplerate"] = output_samplerate
                params_dict["f_lo"] = f_lo

        elif preset_config_name in ["TNRT_pulsar", "TNRT_pulsar_L", "TNRT_pulsar_search_dryrun", "TEST_jp_pulsar_L", "TEST_jp_pulsar_L_R"]:
            self.logger.info("preset_config_name is a type of PULSAR.  Prepare configuration params for PULSAR")
            # Get optional user script parameters
            try:
                project = kwargs['project']
            except KeyError:
                project = default_param_edd.PULSAR_DEFAULT_PROJECT
                self.logger.warning("project name not set. Use default value: {}".format(project))
            
            try:
                source_name = kwargs['source_name']
            except KeyError:
                source_name = default_param_edd.PULSAR_DEFAULT_SOURCE_NAME
                self.logger.warning("source name not set. Use default value: {}".format(source_name))

            try:
                ra = kwargs['ra']
            except KeyError:
                ra = default_param_edd.PULSAR_DEFAULT_RA
                self.logger.warning("ra not set. Use default value: {}".format(ra))

            try:
                dec = kwargs['dec']
            except KeyError:
                dec = default_param_edd.PULSAR_DEFAULT_DEC
                self.logger.warning("dec not set. Use default value: {}".format(dec))

            # Set telescope_metadata in edd redis for TNRT_pulsar
            self.config_telescope_meta(project, source_name, ra, dec)

            # TODO (SS 05/2024): The configure dictionary is empty for pulsar
            # If we need to add something to the configure json string, add here
            # If not, the configure json string will be "{}"
            config_dict = {}

        else:
            # The selected config is a type of spectrometer
            self.logger.info("preset_config_name is a type of SPECTROMETER.  Prepare configuration params for SPECTROMETER")
            # Calcualte edd config parameters and generate a new config string 

            # Get default values from file that we need to calculate the config
            self.logger.debug("start with default values ...")
            
            sampling_rate = default_param_edd.SPECTROMETER_SAMPLING_RATE_DEFAULT
            self.logger.debug("default sampling_rate: {}".format(sampling_rate))
            
            ref_channel = default_param_edd.SPECTROMETER_REF_CHANNEL_DEFAULT
            self.logger.debug("default ref_channel: {}".format(ref_channel))

            predecimation_factor = default_param_edd.preset_config_name_setting[preset_config_name]['predecimation_factor']
            self.logger.debug("default predecimation_factor: {}".format(predecimation_factor))

            decimated_sampling_rate = sampling_rate / predecimation_factor
            self.logger.debug("default decimated_sampling_rate: {}".format(decimated_sampling_rate))

            f0_nyquist_zone_2 = decimated_sampling_rate / 2
            self.logger.debug("default f0_nyquist_zone_2: {}".format(f0_nyquist_zone_2))
        
            self.logger.debug("overwrite defaults with user values if provided in parameters")
            # Get optional user script parameters
            try:
                freq_res = kwargs['freq_res']
                if (freq_res < default_param_edd.SPECTROMETER_FREQ_RES_MIN) or (freq_res > default_param_edd.SPECTROMETER_FREQ_RES_MAX):
                    raise ValueError('freq_res out of range. valid range is {} to {}'.format(
                        default_param_edd.SPECTROMETER_FREQ_RES_MIN, 
                        default_param_edd.SPECTROMETER_FREQ_RES_MAX
                    ))
                self.logger.debug("user freq_res: {}".format(freq_res))
            except KeyError:
                freq_res = default_param_edd.SPECTROMETER_FREQ_RES_DEFAULT
                self.logger.warning('freq_res was not set, use the default value {} Hz/bins'.format(freq_res))
                    
            try:
                integration_time = kwargs['integration_time']
                self.logger.debug("user integration_time: {}".format(integration_time))
            except KeyError:
                integration_time = default_param_edd.SPECTROMETER_INTEGRATION_TIME_DEFAULT
                self.logger.warning('integration_time was not set, use the default value {} S'.format(integration_time))
                
            # Calculate edd configure parameters
            fft_length = decimated_sampling_rate / freq_res
            self.logger.debug("requested fft_length: {}".format(fft_length))

            # Round fft_length up to next power of 2 (finer frequency resolution than requested)
            fft_length_power_of_2 = int(2 ** np.ceil(np.log2(fft_length)))
            self.logger.debug("fft_length_power_of_2: {}".format(fft_length_power_of_2))
            
            actual_freq_res = (sampling_rate / predecimation_factor) / fft_length_power_of_2
            self.logger.debug("actual_freq_res: {}".format(actual_freq_res))

            naccumulate = integration_time * (sampling_rate / predecimation_factor) / fft_length_power_of_2
            self.logger.debug("requested naccumulate: {}".format(naccumulate))

            # Round naccumulate down to next power of 2 (finer time resolution than requested)
            naccumulate_power_of_2 = int(2 ** np.floor(np.log2(naccumulate)))
            self.logger.debug("naccumulate_power_of_2: {}".format(naccumulate_power_of_2))

            actual_integration_time = naccumulate_power_of_2 * (predecimation_factor / sampling_rate) * fft_length_power_of_2
            self.logger.debug('actual_integration_time: {}'.format(actual_integration_time))

            # Set the values of config dict prepare for edd configure
            for (product_name, params_dict) in config_dict["products"].items():
                params_dict["fft_length"] = fft_length_power_of_2
                params_dict["naccumulate"] = naccumulate_power_of_2
        
        # Translate dict to json string and return
        return json.dumps(config_dict, separators=(",", ":"))
    
    def __repr__(self):
        return repr(self.objval)

    def config_telescope_meta(self,project, source_name, ra, dec):
        try:
            # connecting to redis's TELESCOPE_META, for more detail looking at web interface: http://192.168.90.11:8081/
            #TODO move this function to update-metadata in BackendEDD contianer 
            edd_redis_host = default_param_edd.edd_redis_host
            edd_redis_port = default_param_edd.edd_redis_port

            telescope_meta_client = redis.Redis(host= edd_redis_host, port=edd_redis_port, db=4)

            # setting telescope_metadata for TNRT_pulsar with "hset"
            meta_project = {"value":project}
            telescope_meta_client.hmset("project", meta_project)
            meta_source_name = {"value":source_name}
            telescope_meta_client.hmset("source-name", meta_source_name)
            meta_ra = {"value":ra}
            telescope_meta_client.hmset("ra", meta_ra)
            meta_dec = {"value":dec}
            telescope_meta_client.hmset("dec", meta_dec)
            
            # confrim this set are correct.
            if(project != telescope_meta_client.hgetall("project")[b'value'].decode()):
                raise RedisWriteError("parameter project setting failed")
            if(source_name != telescope_meta_client.hgetall("source-name")[b'value'].decode()):
                raise RedisWriteError("parameter source-name setting failed")
            if(ra != telescope_meta_client.hgetall("ra")[b'value'].decode()):
                raise RedisWriteError("parameter ra setting failed")
            if(dec != telescope_meta_client.hgetall("dec")[b'value'].decode()):
                raise RedisWriteError("parameter dec setting failed")

            del telescope_meta_client            
        except ExceptionType as e:
            self.logger.info('metadata setting was problam, please check redis commander interface')  
            raise e


class BackendParamsHoloFFT:
    """
    Parameters for Keysight 35760A Dynamic Signal Analyzer

    Parameters
    ----------
    freq_res : `float`. unit = Hz, min = 0.0001220703125, max = 64.0, default = 1.0
        
                    .. Note:: For any configuration of sample_rate and fft_length, there are 20 available values for actual freq_res achieved by decimation of raw data by power-of-2 decimation factor.  If the user selects a value that is not on the list, the system selects the nearest allowed value that is finer resolution than requested.

                    For example:

                    >>> sample_rate = 51.2e3 # 51.2 kHz.  S et by 35670A device when 2 inputs are enabled.
                    >>> fft_length = 800
                    >>> decimation_exponents = np.arange(20)  # integers from 0 to 19
                    >>> decimated_sample_rates = sample_rate / ( 2 ** decimation_exponents)
                    >>> actual_freq_res = decimated_sample_rates / fft_length
                    >>> decimated_sample_periods = 1 / decimated_sample_rates
                    >>> measurement_duration = fft_length * decimated_sample_periods

                    allowed values of `actual_freq_res` for this configuration of `fft_length = 800`:

                    >>> actual_freq_res
                    >>> array([6.40000000e+01, 3.20000000e+01, 1.60000000e+01, 8.00000000e+00,
                    4.00000000e+00, 2.00000000e+00, 1.00000000e+00, 5.00000000e-01,
                    2.50000000e-01, 1.25000000e-01, 6.25000000e-02, 3.12500000e-02,
                    1.56250000e-02, 7.81250000e-03, 3.90625000e-03, 1.95312500e-03,
                    9.76562500e-04, 4.88281250e-04, 2.44140625e-04, 1.22070312e-04])

                    Resulting values of measurement_duration following the same parameters

                    >>> measurement_duration
                    array([1.5625e-02, 3.1250e-02, 6.2500e-02, 1.2500e-01, 2.5000e-01,
                    5.0000e-01, 1.0000e+00, 2.0000e+00, 4.0000e+00, 8.0000e+00,
                    1.6000e+01, 3.2000e+01, 6.4000e+01, 1.2800e+02, 2.5600e+02,
                    5.1200e+02, 1.0240e+03, 2.0480e+03, 4.0960e+03, 8.1920e+03])
                    
    fft_length: `int`. allowed values {100, 200, 400, 800}, default = 800

    mock : {True, False}, optional. default = False
        True : do not connect to the real BackendHoloFFT device.  Use simulation
        False : connect and receive data from the real BackendHoloFFT device.

    Notes
    ------
    Theory of operation:

    The Keysight 35760A Signal Analyzer digitizes time-domain buffers of data at a fixed
    sample rate depending on the number of analog inputs.

        - 1 input : sample rate is 102.4 kHz
        - 2 inputs : sample rate is 51.2 kHz (TNRT hardware Configuration)

    Following the time-domain capture, it performs DSP operations to process the raw time-domain
    data into useful results.
    
    DSP operations include:

        - DDC (digital down conversion): 
            Purpose is to re-center the digitized bandwdith at a frequency of interest.
            This function is used internally to auto-center on detected signal, but not available in
            user parameters for operation script.
        - Decimation: 
            Purpose is to reduce the bandwidth of interest and achieve
            "zoomed" fine frequency resolution.  The user does not set decimation directly.  It is
            calculated based on the user parameter `freq_res`.
        - FFT:
            Purpose is to transform time-domain to frequency domain data.  The user can specify the
            number of points directly using the paramater `freq_res`.  The instrument allows only 
            4 values {100, 200, 400, 800}.  By default, the software selects 800 to provide the finest frequency resolution.
            In typical operations the user does not need to set this because the freq_res parameter provides
            more detailed control over the result data than changing the FFT length.   

    Duration of data capture:

    This type of data recording does not accumulate or integrate data.  It digitizes the required number
    of samples using ADC (analog to digital converter) to achieve the algorithms listed above.  The
    time required to record sufficient samples for 1 measurement is:

    measurement_duration = 1 / freq_res

    A data acquisition that requires very fine frequency resolution requires a long time.  
    Conversely,  a data acquisition that requires coarse resolution can be acquired very quickly.

    Examples
    --------
    >>> # Select backend HoloFFT and use default configuration.
    >>> backend_params = BackendParamsHoloFFT()

    >>> # Select backend HoloFFT and request frequency resolution of 1 Hz and default fft_length
    >>> backend_params = BackendParamsHoloFFT(freq_res=1)

    >>> # Select backend HoloFFT and request frequency resolution of 1 Hz and fft_length of 400
    >>> backend_params = BackendParamsHoloFFT(freq_res=1, fft_length=400)
    """

    def __init__(self, freq_res=4.0, fft_length=800, mock=False, **kwargs):
        # Create a new logger for this class that is a child of logger named 'nash'
        self.logger = logging.getLogger("nash." + self.__class__.__name__)
        self.logger.propagate = True
        self.logger.handlers = []
        
        # Validate FFT length
        
        # Divide by 100, find the integer exponent that provides the next power of 2 up.
        # clip the exponent to allowed range of values (0..3) from 35670A analyzer.
        # Using the new FFT exponent, calcuate the actual fft_length that is allowed
        fft_exponent = np.log2(fft_length / 100)
        fft_exponent_actual = int(np.clip(np.ceil(fft_exponent), 0, 3))
        fft_length_actual = int(100 * (2 ** fft_exponent_actual))

        self.logger.info("requested fft_length: {}, actual fft_length: {}".format(fft_length, fft_length_actual))

        # Validate freq_res
        # Given an fft_length and default sample rate for 2 analog inputs,
        sample_rate = default_param_holofft.SAMPLE_RATE_FULL / default_param_holofft.NUMBER_OF_INPUTS
        freq_res_no_decimation = sample_rate / fft_length_actual
        decimation_factor = freq_res_no_decimation / freq_res
        decimation_exponent = np.log2(decimation_factor)
        self.logger.debug("requested freq_res: {}, decimation_factor: {}, decimation_exponent: {}".format(freq_res, decimation_factor, decimation_exponent))
        
        # Calculate logic on decimation exponent to round up to the nearest integer,
        # then clip to min / max of allowed range of decimation exponents 0..19 from 35670A device
        # Using the new decimation exponent, calculate the actual decimation factor and freq_res.
        # actual freq_res should be finer resolution than requested.
        decimation_exponent_actual = int(np.clip(np.ceil(decimation_exponent), 0, 19))
        decimation_factor_actual = 2 ** decimation_exponent_actual
        decimated_sample_rate = sample_rate / decimation_factor_actual
        freq_res_actual = decimated_sample_rate / fft_length_actual
        self.logger.debug("actual freq_res: {}, decimation_factor: {}, decimation_exponent: {}".format(freq_res_actual, decimation_factor_actual, decimation_exponent_actual))
        
        measurement_duration = fft_length_actual / decimated_sample_rate
        self.logger.info("expected measurement duration per capture: {}".format(measurement_duration))

        params = {
            "freq_res" : freq_res_actual,
            "fft_length": fft_length_actual,
            "mock" : mock,
        }
        self.objval = ScanMod.BackendParamsHoloFFT("", json.dumps(params))

    def __repr__(self):
        return repr(self.objval)


class BackendParamsHoloSDR:
    """
    .. todo:: not available yet
    """

    def __init__(self, preset_config_name="TODO", **kwargs):
        self.objval = ScanMod.BackendParamsHoloSDR(preset_config_name, json.dumps(kwargs))

    def __repr__(self):
        return repr(self.objval)


class BackendParamsSKARAB:
    """
    .. todo:: add content
    """

    def __init__(self, preset_config_name="TODO", **kwargs):
        self.objval = ScanMod.BackendParamsSKARAB(preset_config_name, json.dumps(kwargs))

    def __repr__(self):
        return repr(self.objval)


class BackendParamsOptical:
    """
    .. todo:: add content
    """

    def __init__(self, preset_config_name="TODO", **kwargs):
        self.objval = ScanMod.BackendParamsOptical(preset_config_name, json.dumps(kwargs))

    def __repr__(self):
        return repr(self.objval)


class BackendParamsPowerMeter:
    """
    Parameters for PowerMeter

    Parameters
    ----------
    integration_time : float, optional. default=0.5
        Maximum integration time.
        Accept value between 976.5625e to 68192.0

    Examples
    --------
    >>> # Select backend PowerMeter and use default configuration.
    >>> backend_params = BackendParamsPowerMeter()

    >>> # Select backend PowerMeter and choose settings
    >>> backend_params = BackendParamsPowerMeter(integration_time=1)

    """

    def __init__(self, preset_config_name="", integration_time=0.5, **kwargs):
        self.objval = ScanMod.BackendParamsPowerMeter(preset_config_name, json.dumps(kwargs))

        if integration_time < 976.5625e-6 or integration_time > 8192.0:
            raise ValueError("Integration time out of range (should be between 976.5625e-6 - 8192)")

        params = {
            "integrationTime": integration_time,
        }
        self.objval = ScanMod.BackendParamsPowerMeter("", json.dumps(params))

    def __repr__(self):
        return repr(self.objval)


class BackendParamsMockEDD(BackendParamsEDD):
    """
    Deprecated.  Keep for backward compatibility with old scripts.  
    New scripts should use `BackendParamsEDD(<preset_config_name>, mock=True, param1=<val>, param2=<val>)`.
    """
    def __init__(self, preset_config_name="TNRT_dualpol_spectrometer_L", **kwargs):
        super().__init__(preset_config_name, mock=True, kwargs=kwargs)


class BackendParamsMock(BackendParamsMockEDD):
    """
    Deprecated.  Keep for backward compatibility with old scripts.  
    New scripts should use `BackendParamsEDD(<preset_config_name>, mock=True)`.
    """

class DataParams():
    """
    Parameters for data management per scan.  Mostly output data file formats, but
    also includes data streaming for real-time preview GUI

    Parameters
    ----------
    pipelines : [list of strings], optional. default=[]
        List of pipelines to process data within Telescope Control Software.  
        Note 1: this is not the same as EDD pipelines
        Note 2: If one string is passed into the parameter (instead of list of strings), it will automatically
        be converted into a list of 1 string.
        
        allowed values: "mbfits", "spectrum_preview", "atfits", "gildas"

    Examples
    --------
    >>> # Select typical data pipelines for EDD Spectrometer observation.
    >>> data_params = DataParams(["mbfits", "spectrum_preview"])

    >>> # Select typical data pipelines for Holography observation.
    >>> data_params = DataParams(["atfits"])

    >>> # Select typical data pipelines for Holography observation using one string parameter.
    >>> data_params = DataParams("atfits")
    """

    def __init__(self, pipeline_names=[]):
        # validate parameters
        allowed_pipeline_names = ["mbfits", "spectrum_preview", "atfits", "gildas"]
        
        # Convert a single string into a list of 1 string
        if type(pipeline_names) == str:
            pipeline_names = [pipeline_names]

        for name in pipeline_names:
            if name not in allowed_pipeline_names:
                self.objval = None
                raise ValueError(
                    "{} not found in allowed pipeline_names.  Available names: {}".format(name, allowed_pipeline_names))
            
        self.objval = ScanMod.DataParams(pipeline_names)
    
    def __repr__(self):
        return repr(self.objval)

if __name__ == "__main__":
    # Some standalone examples to generate BackendParamsEDD objects
    print(BackendParamsEDD("TNRT_VLBI_L"))
    print(BackendParamsEDD("TNRT_VLBI_L", output_samplerate=4000000))
    print(BackendParamsEDD("TNRT_VLBI_L", f_lo=444000000))
    print(BackendParamsEDD("TNRT_VLBI_L", output_samplerate=4000000, f_lo=[123000000]))
    print(BackendParamsEDD("TNRT_pulsar_L"))
    print(BackendParamsEDD("TNRT_pulsar_L", project="hello", source_name="xyz", ra=10, dec=20)) 
    print(BackendParamsEDD("TNRT_dualpol_spectrometer_L"))
    print(BackendParamsEDD("TNRT_dualpol_spectrometer_K"))
    print(BackendParamsEDD("TNRT_dualpol_spectrometer_L", freq_res=122070.3125, integration_time=1.0))
    print(BackendParamsEDD("TNRT_stokes_spectrometer_L"))
    print(BackendParamsEDD("TNRT_stokes_spectrometer_L", freq_res=122070.3125, integration_time=1.0))
