'''
Summary

Parameters
----------
name : type
	description

Returns
-------
name : type
	description

See Also
--------
func

Notes
-----
include math equations in LaTeX format
.. math:: X(e^{j\omega } ) = x(n)e^{ - j\omega n}

References
----------

Examples
--------
>>> operator('XX')


Example  Equation in LaTeX format
---------------------------------
	.. math:: \LaTeX
	.. math:: e^{j\phi} = cos{\phi} + j \cdot sin{\phi}

Example Complete Scripts
------------------------
	:py:mod:`../example/example.py`	

'''