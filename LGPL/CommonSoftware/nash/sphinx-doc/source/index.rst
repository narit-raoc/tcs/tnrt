.. nash documentation master file, created by
   sphinx-quickstart on Wed Mar 25 09:42:44 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. automodule:: nash

.. currentmodule:: nash

===============================
TNRT TCS Command Line Interface
===============================

Updated: |today|

Introduction
============
The Thai National Radio Telscope (TNRT) includes a command line interface (CLI) for manual control and script operation.  The name "nash" means "narit shell".  The command line interface is an interactive iPython session which imports a module named `nash.py`.  This module provides many convenient user functions to construct automatic observations, command specific actions, and preview results.  Since `nash` is an iPython session, it can execute standard python operations, import external modules as needed, and run scripts in python3 syntax.

.. toctree::
   :hidden:

   Home <self>

.. toctree::
   :caption: User Interface
   :maxdepth: 3

   automatic_observations
   command_reference
   example_scripts

.. toctree::
   :caption: Source Code Repositories
   :hidden:
   :maxdepth: 2

   tnrt <https://gitlab.com/narit-raoc/tnrt>