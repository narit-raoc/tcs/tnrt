.. currentmodule:: nash

Example Scripts
===============

Check Time Sync
---------------
A script to compare the timestamps of messages from the ACU with current time of the TCS computer.

`test_timesync.py <https://gitlab.com/narit-raoc/tnrt/-/blob/master/scripts/test_timesync.py>`_ 

`process_timesync.py <https://gitlab.com/narit-raoc/tnrt/-/blob/master/scripts/process_timesync.py>`_


Basic Azimuth Elevation Movement
--------------------------------
Use :py:class:`~ParameterBlocks.ScantypeParamsManual` to perform a "Manual Scan" without receivers.  Record data from ACU while the user scripts commands the antenna to move AZ and EL axes.

`test_basic_az_el.py <https://gitlab.com/narit-raoc/tnrt/-/blob/master/scripts/test_basic_az_el.py>`_ 

`process_basic_az_el.py <https://gitlab.com/narit-raoc/tnrt/-/blob/master/scripts/process_basic_az_el.py>`_


Check Pointing Model Function
-----------------------------
Run 2 scans.  disable -> enable pointing model and compare results

`test_pmodel.py <https://gitlab.com/narit-raoc/tnrt/-/blob/master/scripts/test_pmodel.py>`_ 

`process_pmodel.py <https://gitlab.com/narit-raoc/tnrt/-/blob/master/scripts/process_pmodel.py>`_