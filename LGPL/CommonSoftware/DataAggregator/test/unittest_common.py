# Project: TNRT - Thai National Radio Telescope
# Organization: NARIT - National Astronomical Research Institute of Thailand

# import standard modules
import unittest
import logging
import coloredlogs

# import ACS modules
from Acspy.Clients.SimpleClient import PySimpleClient

import ScanDefaults
from astropy import units as units
from astropy.time import Time
from astropy.coordinates import EarthLocation

import tnrtAntennaMod
import atmosphere
import ScanMod
import BackendMod
import BackendDefaults
from Supplier import Supplier
from Consumer import Consumer
from Subscan import Metadata


class ColoredTestCase(unittest.TestCase):
    def setUp(self):
        # Configure logger
        self.logger = logging.getLogger("ColoredTestCase")
        self.logger.setLevel(logging.DEBUG)

        fmt_scrn = "%(asctime)s [%(levelname)s]: %(funcName)s(): %(message)s"

        level_styles_scrn = {
            "critical": {"color": "red", "bold": True},
            "debug": {"color": "white", "faint": True},
            "error": {"color": "red"},
            "info": {"color": "green", "bright": True},
            "notice": {"color": "magenta"},
            "spam": {"color": "green", "faint": True},
            "success": {"color": "green", "bold": True},
            "verbose": {"color": "blue"},
            "warning": {"color": "yellow", "bright": True, "bold": True},
        }
        field_styles_scrn = {
            "asctime": {},
            "hostname": {"color": "magenta"},
            "levelname": {"color": "cyan", "bright": True},
            "name": {"color": "blue", "bright": True},
            "programname": {"color": "cyan"},
        }
        formatter_screen = coloredlogs.ColoredFormatter(
            fmt=fmt_scrn, level_styles=level_styles_scrn, field_styles=field_styles_scrn
        )

        # creating a handler to log on the console
        handler_screen = logging.StreamHandler()
        handler_screen.setFormatter(formatter_screen)
        handler_screen.setLevel(logging.DEBUG)

        # remove all handlers if they exist
        # remove because PySimpleClient logger already has handlers which
        # show duplicate of my log messages, but without the pretty color format.
        if len(self.logger.handlers) > 0:
            self.logger.handlers = []

        # adding handlers
        self.logger.addHandler(handler_screen)


class DataAggregatorTestCase(ColoredTestCase):
    """
    Test case that has colored logs and connection to ACS Component DataAggregator
    """

    def setUp(self):
        # Use parent setup function to configure the logger
        super().setUp()

        # Connect ACS component
        # self.logger.debug("Starting PySimpleClient")
        self.sc = PySimpleClient()

        self.component_name = "DataAggregator"
        # Get reference to Component.  If it is already activated by
        # ACS Command Center, this will be a reference to the same object.
        # If it is not active, This command will activate and create a new instance.
        try:
            # self.logger.debug("Connecting to ACS component {}".format(self.component_name))
            self.objref = self.sc.getComponent(self.component_name)
        except Exception as e:
            self.logger.error(
                "Cannot get ACS component object reference for {}".format(self.component_name)
            )
            self.tearDown()

    def tearDown(self):
        """
        This function runs 1 time per test_xxx function in this class. after test complete
        """
        try:
            # self.logger.debug("Releasing ACS component %s" % self.component_name)
            self.sc.releaseComponent(self.component_name)
            self.objref = None
        except Exception as e:
            self.logger.error("Cannot release component reference for %s" % self.component_name)

        try:
            # Remove logging handlers from logger in PySimpleClient to reduce clutter
            # We are going to disconnect and delete the client, so don't care about last log messages
            self.sc.logger.handlers = []
            self.sc.disconnect()
            del self.sc
        except AttributeError as e:
            self.logger.error(e)

    def simulate_headers(self):
        # Create a default set of Scan metadata
        metadata = ScanDefaults.metadata

        longitude = 99.216805
        latitude = 18.864348
        height = 403.625

        site_location = EarthLocation(
            EarthLocation.from_geodetic(
                lon=longitude * units.deg,
                lat=latitude * units.deg,
                height=height * units.m,
                ellipsoid="WGS84",
            )
        )

        # Update Scan metadata structure to produce some meaningful data other than Default.
        time_apy = Time(Time.now(), location=site_location)

        # Set MBFITS-style metadata values enough to fill all of the parameters for an
        # example MBFITS file 1 arm of pointing scan
        metadata.SCAN_MBFITS_HEADER.TELESCOP = "TNRT40"
        metadata.SCAN_MBFITS_HEADER.SITELONG = longitude
        metadata.SCAN_MBFITS_HEADER.SITELAT = latitude
        metadata.SCAN_MBFITS_HEADER.SITEELEV = height
        metadata.SCAN_MBFITS_HEADER.OBSID = "SS"
        metadata.SCAN_MBFITS_HEADER.MJD = time_apy.mjd
        metadata.SCAN_MBFITS_HEADER.SCANNUM = 1
        metadata.SCAN_MBFITS_HEADER.TIMESYS = "UTC"
        metadata.SCAN_MBFITS_HEADER.RADESYS = "ICRS"
        metadata.SCAN_MBFITS_HEADER.X_OBJECT = "IS-22"
        metadata.SCAN_MBFITS_HEADER.BLNGTYPE = "ALON-GLS"
        metadata.SCAN_MBFITS_HEADER.BLATTYPE = "ALAT-GLS"
        metadata.SCAN_MBFITS_HEADER.EQUINOX = 2000.0
        metadata.SCAN_MBFITS_HEADER.BLONGOBJ = 237.0
        metadata.SCAN_MBFITS_HEADER.BLATOBJ = 52.0
        metadata.SCAN_MBFITS_HEADER.PATLONG = 0.0
        metadata.SCAN_MBFITS_HEADER.PATLAT = 0.0
        metadata.SCAN_MBFITS_HEADER.PDELTACA = 0
        metadata.SCAN_MBFITS_HEADER.CALCODE = "PHAS"

        metadata.FEBEPAR_MBFITS_HEADER.NUSEBAND = 1
        metadata.FEBEPAR_MBFITS_HEADER.FEBEFEED = 1

        subs_metadata = Metadata()
        subs_metadata.assign_arraydata_value()
        subs_metadata.assign_datapar_value()
        metadata.ARRAYDATA_MBFITS_HEADER = subs_metadata.arraydata_mbfits_header
        metadata.DATAPAR_MBFITS_HEADER = subs_metadata.datapar_mbfits_header

        metadata.ARRAYDATA_MBFITS_HEADER['DATE_OBS'] = time_apy.isot
        metadata.ARRAYDATA_MBFITS_HEADER['SCANNUM'] = metadata.SCAN_MBFITS_HEADER.SCANNUM
        metadata.ARRAYDATA_MBFITS_HEADER['CHANNELS'] = 2048
        metadata.ARRAYDATA_MBFITS_HEADER['NUSEFEED'] = 4
        metadata.ARRAYDATA_MBFITS_HEADER['SUBSNUM'] = 1
        metadata.ARRAYDATA_MBFITS_HEADER['MOLECULE'] = "UPKR"
        metadata.ARRAYDATA_MBFITS_HEADER['RESTFREQ'] = 11699e6

        metadata.ARRAYDATA_MBFITS_HEADER['X_1CDLT2S'] = (
            800e6 / metadata.ARRAYDATA_MBFITS_HEADER['CHANNELS']
        )

        metadata.DATAPAR_MBFITS_HEADER['DATE_OBS'] = time_apy.isot
        metadata.DATAPAR_MBFITS_HEADER['SCANNUM'] = metadata.SCAN_MBFITS_HEADER.SCANNUM
        metadata.DATAPAR_MBFITS_HEADER['LST'] = time_apy.sidereal_time("apparent").hour * 3600
        metadata.DATAPAR_MBFITS_HEADER['NLNGTYPE'] = "ALON-GLS"
        metadata.DATAPAR_MBFITS_HEADER['CTYPE1N'] = "ALON-GLS"
        metadata.DATAPAR_MBFITS_HEADER['NLATTYPE'] = "ALAT-GLS"
        metadata.DATAPAR_MBFITS_HEADER['CTYPE2N'] = "ALAT-GLS"
        metadata.DATAPAR_MBFITS_HEADER['SCANDIR'] = "ALON"

        return (metadata, time_apy)


class AsyncMessagingTestCase(DataAggregatorTestCase):
    def setUp(self):
        super().setUp()
        # Initialize dictionary of suppliers used by DataAggregator UnitTest
        self.suppliers = {
            # "antenna": {
            #     "channel": tnrtAntennaMod.STATUS_CHANNEL_NAME,
            #     "supplier": None,
            # },
            "atmosphere": {
                "channel": atmosphere.ATMCHANNEL,
                "supplier": None,
            },
            "scan": {
                "channel": ScanMod.CHANNEL_NAME_METADATA,
                "supplier": None,
            },
            "holodata": {
                "channel": BackendMod.CHANNEL_NAME_HOLO_DATA,
                "supplier": None,
            }
            # wait for NARIT's weather station
            # 'weather': {
            #   'channel': weatherComNet.CHANNELNAME,
            #   'supplier': None
            #   }
        }

        # Initialize dictionary data for notification channel consumers used by
        # DataAggregator UnitTest
        self.consumers = {}
        # TODO (SS 11/2022)- add a new Consumer and debug the disconnect issue
        # in the next branch when we have new feature Pipeline status
        # self.consumers = {
        #     "pipeline": {
        #         "channel": DataAggregatorMod.CHANNEL_NAME_PIPELINE,
        #         "dtype": DataAggregatorMod.PipelineStatus,
        #         "handler": self.handler_pipeline,
        #         "consumer": None,
        #     },
        # }

        # Create Notifiation Channel Suppliers to generate data and push into
        # DataAggregator instance for testing
        for key in self.suppliers:
            self.logger.debug("Creating Supplier {}: ".format(key))
            try:
                self.suppliers[key]["supplier"] = Supplier(self.suppliers[key]["channel"])
            except:
                self.suppliers[key]["supplier"] = None
                self.logger.logError("Failed to initialize NC Supplier: {}".format(key))
                raise

        # Create Notifiation Channel Consumer to receive the data that is output from
        # DataAggregator instance
        for key in self.consumers:
            self.logger.debug("Creating Consumer {}: ".format(key))
            try:
                self.consumers[key]["consumer"] = Consumer(self.consumers[key]["channel"])
                self.consumers[key]["consumer"].add_subscription(self.consumers[key]["handler"])
            except:
                self.consumers[key]["consumer"] = None
                self.logger.logError("Failed to initialize Consumer {}: ".format(key))

    def tearDown(self):
        for key in self.consumers:
            self.logger.debug("Disconnect Consumer{}: ".format(key))
            if self.consumers[key]["consumer"] is not None:
                try:
                    self.consumers[key]["consumer"].disconnect()
                except:
                    self.consumers[key]["consumer"] = None

        for key in self.suppliers:
            print(key)
            self.logger.debug("Disconnect Supplier {}: ".format(key))
            try:
                self.suppliers[key]["supplier"].disconnect()
            except:
                self.suppliers[key]["supplier"] = None

        super().tearDown()

    def handler_pipeline(self, dataStruct):
        """
        Handle the data that was output by the DataAggregator NC Supplier.
        """
        self.logger.debug("handler pipeline.  TODO")
