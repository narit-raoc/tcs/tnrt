# Project: TNRT - Thai National Radio Telescope
# Organization: NARIT - National Astronomical Research Institute of Thailand

from unittest_common import DataAggregatorTestCase

# import TCS modules
import AntennaDefaults
import DataAggregatorMod
import TypeConverterDA as tc


class DataTypeTestCase(DataAggregatorTestCase):
    def setUp(self):
        super().setUp()
        self.result = None

    def tearDown(self):
        self.logger.info(self.result)
        super().tearDown()

    def test1100_type_converter(self):
        self.logger.debug("from_idl_AntennaStatusNotifyBlock")
        try:
            idl_struct = AntennaDefaults.metadata

            (
                monitor_antenna_PacketHeader_row,
                monitor_antenna_GeneralStatus_row,
                monitor_antenna_SystemStatus_row,
                monitor_antenna_VertexShutterStatus_row,
                monitor_antenna_AxisStatus_row,
                monitor_antenna_MotorStatus_row,
                monitor_antenna_TrackingStatus_row,
                monitor_antenna_GeneralHexapodStatus_row,
                monitor_antenna_HexapodStatus_row,
                monitor_antenna_SpindleStatus_row,
                monitor_antenna_HexapodTrackStatus_row,
                monitor_antenna_TrackingObjectStatus_row,
            ) = tc.from_idl_AntennaStatusNotifyBlock(idl_struct)

            self.logger.debug(
                "monitor_antenna_PacketHeader_row: {}".format(
                    repr(monitor_antenna_PacketHeader_row)
                )
            )
            self.logger.debug(
                "monitor_antenna_GeneralStatus_row: {}".format(
                    repr(monitor_antenna_GeneralStatus_row)
                )
            )
            self.logger.debug(
                "monitor_antenna_SystemStatus_row: {}".format(
                    repr(monitor_antenna_SystemStatus_row)
                )
            )
            self.logger.debug(
                "monitor_antenna_VertexShutterStatus_row: {}".format(
                    repr(monitor_antenna_VertexShutterStatus_row)
                )
            )
            self.logger.debug(
                "monitor_antenna_AxisStatus_row: {}".format(repr(monitor_antenna_AxisStatus_row))
            )
            self.logger.debug(
                "monitor_antenna_MotorStatus_row: {}".format(repr(monitor_antenna_MotorStatus_row))
            )
            self.logger.debug(
                "monitor_antenna_TrackingStatus_row: {}".format(
                    repr(monitor_antenna_TrackingStatus_row)
                )
            )
            self.logger.debug(
                "monitor_antenna_GeneralHexapodStatus_row: {}".format(
                    repr(monitor_antenna_GeneralHexapodStatus_row)
                )
            )
            self.logger.debug(
                "monitor_antenna_HexapodStatus_row: {}".format(
                    repr(monitor_antenna_HexapodStatus_row)
                )
            )
            self.logger.debug(
                "monitor_antenna_SpindleStatus_row: {}".format(
                    repr(monitor_antenna_SpindleStatus_row)
                )
            )
            self.logger.debug(
                "monitor_antenna_HexapodTrackStatus_row: {}".format(
                    repr(monitor_antenna_HexapodTrackStatus_row)
                )
            )
            self.logger.debug(
                "monitor_antenna_TrackingObjectStatus_row: {}".format(
                    repr(monitor_antenna_TrackingObjectStatus_row)
                )
            )
        except Exception as e:
            self.fail(e)

    def test1101_get_metadata(self):
        self.logger.debug("get_metadata()")
        try:
            result = self.objref.get_metadata()
            self.logger.debug("result MetadataNotifyBlock:{}".format(repr(result)))
            self.assertIsInstance(result, DataAggregatorMod.MetadataNotifyBlock)
        except Exception as e:
            self.fail(e)
