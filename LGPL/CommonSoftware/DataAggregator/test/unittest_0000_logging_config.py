# Project: TNRT - Thai National Radio Telescope
# Organization: NARIT - National Astronomical Research Institute of Thailand
import logging
from unittest_common import ColoredTestCase


class Tests(ColoredTestCase):
    def setUp(self):
        """
        This function runs 1 time per test_xxx function in this class. before test start
        """
        super().setUp()

    def tearDown(self):
        """
        This function runs 1 time per test_xxx function in this class. after test complete
        """
        super().tearDown()

    def test0000_logging_levels(self):
        self.logger.debug("level = logging.DEBUG {}".format(logging.DEBUG))
        self.logger.info("level = logging.INFO: {}".format(logging.INFO))
        self.logger.warning("level = logging.WARNING: {}".format(logging.WARNING))
        self.logger.error("log level = logging.ERROR: {}".format(logging.ERROR))
        self.logger.critical("level = logging.CRITICAL: {}".format(logging.CRITICAL))
