# Project: TNRT - Thai National Radio Telescope
# Organization: NARIT - National Astronomical Research Institute of Thailand

import numpy as np
import time
from unittest_common import DataAggregatorTestCase
from DataSimulator import PointingSimulator
from DataSimulator import HolographySimulator


class DataSimTestCase(DataAggregatorTestCase):
    def setUp(self):
        super().setUp()
        self.result = None

    def tearDown(self):
        self.logger.info(self.result)
        super().tearDown()

    def test1200_pointing_simulator(self):
        try:
            offaxis_arcsec = -100
            psim = PointingSimulator(offaxis=offaxis_arcsec, nchan=512)
            result = psim.data3D
            self.logger.debug("PointingSimulator data cube:{}".format(repr(result)))
            self.assertEqual(result.dtype, np.dtype("float64"))
            psim.preview()
        except Exception as e:
            self.fail(e)

    def test1210_holography_simulator(self):
        # Simulate datacube, Fill MBFITS data arrays directly (in real system, DataAggregator
        # gets access to all of the data and prepares the data arrays in real-time).
        npoints = 128
        angular_resolution = 20
        time_per_angular_step = 0.5  # should be same as receiver integration time
        subscans_per_cal = 1
        integrations_per_calN = 10
        integrations_per_cal0 = 120
        peak_magntidue_ref = 8.5e-6  # mean value of first calibration in Yebes file TEST5772.FITS
        peak_magntidue_tst = 5.8e-5  # mean value of first calibration in Yebes file TEST5772.FITS
        fwhm = 160

        hsim = HolographySimulator(
            npoints,
            angular_resolution,
            time_per_angular_step,
            subscans_per_cal,
            integrations_per_calN,
            integrations_per_cal0,
            peak_magntidue_ref,
            peak_magntidue_tst,
            fwhm,
        )

        result = hsim.otf_map
        self.logger.debug("HolographySimulator otf_map:{}".format(repr(result)))

        # Uncomment the line about preview() to see the graphs of simulated data
        hsim.preview()

    # def test1220_spectrum_preview(self):
    #     # Data to design simulation
    #     subscan_duration = 10
    #     integration_time = 0.2
    #     nsections = 2

    #     # duration to observe incoming packets.  if stream_duration > subscan_duration, we will see subscan repeat (infinite loop)
    #     stream_duration = 10

    #     nchannels = 1024
    #     self.logger.info("test spectrum same channels as preview")
    #     self.logger.debug(
    #         "Simulate spectrum {} channels for {} seconds. Look at DataMonitor preview".format(
    #             nchannels, stream_duration
    #         )
    #     )
    #     self.objref.simulate_spectrum_preview_start(
    #         subscan_duration, integration_time, nsections, nchannels
    #     )
    #     time.sleep(stream_duration)
    #     self.objref.simulate_spectrum_preview_stop()

    #     nchannels = 256 * 1024
    #     self.logger.info("test spectrum more channels than preview")
    #     self.logger.warning(
    #         "Simulate spectrum {} channels for {} seconds. Look at DataMonitor preview".format(
    #             nchannels, stream_duration
    #         )
    #     )
    #     self.objref.simulate_spectrum_preview_start(
    #         subscan_duration, integration_time, nsections, nchannels
    #     )
    #     time.sleep(stream_duration)
    #     self.objref.simulate_spectrum_preview_stop()

    #     # nchannels = 256
    #     # self.logger.info('test spectrum less channels than preview')
    #     # self.logger.debug('Simulate spectrum {} channels for {} seconds. Look at DataMonitor preview'.format(nchannels, stream_duration))
    #     # self.objref.simulate_spectrum_preview_start(subscan_duration, integration_time, nsections, nchannels)
    #     # time.sleep(stream_duration)
    #     # self.objref.simulate_spectrum_preview_stop()

    # def test1230_simulate_spectrum_arraydata(self):
    #     subscan_duration = 10
    #     integration_time = 1.0
    #     nsections = 2
    #     nchannels = 32
    #     self.logger.warning(
    #         "Simulate spectrum arraydata. duration={}, nchannels={}, integration_time={}. Look at DataMonitor preview".format(
    #             subscan_duration, nchannels, integration_time
    #         )
    #     )
    #     self.objref.simulate_spectrum_arraydata_subscan(
    #         subscan_duration, integration_time, nsections, nchannels
    #     )
