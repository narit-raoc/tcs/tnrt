# Calculate the data rate of eddfits packets between EddFitsServer (backned system)
# and EddFitsClient (TCS system).
# Spiro Sarris 04/2022

import numpy as np
import pyqtgraph as pg
from pyqtgraph.Qt import QtGui, QtCore

pg.setConfigOption("background", "w")
pg.setConfigOption("foreground", "k")

fft_length_min = 1024 * 2
fft_length_max = 1024 * 1024 * 2

fft_length_exp_min = np.log2(fft_length_min)
fft_length_exp_max = np.log2(fft_length_max)

print(
    "fft_length_min: {} = (2 ** {}) .. fft_length_max: {} = (2 ** {})".format(
        fft_length_min, fft_length_exp_min, fft_length_max, fft_length_exp_max
    )
)

fft_length_exps = np.arange(fft_length_exp_min, fft_length_exp_max + 1, step=1)
fft_lengths = 2**fft_length_exps

# print("fft_length_exps: {}".format(fft_length_exps))
print("fft_lengths: {}".format(fft_lengths))


# I don't know the maximum, so choose this number for now.
# If naccumulate increases, the data rate across network decreases, so I am not
# worried about bigger numbers of naccumulate for the purpose of this script.
naccumulate_min = 1
naccumulate_max = 16777216

naccumulate_exp_min = np.log2(naccumulate_min)
naccumulate_exp_max = np.log2(naccumulate_max)

print(
    "naccumulate_min: {} = (2 ** {}) .. naccumulate_max: {} = (2 ** {})".format(
        naccumulate_min, naccumulate_exp_min, naccumulate_max, naccumulate_exp_max
    )
)

naccumulate_exps = np.arange(naccumulate_exp_min, naccumulate_exp_max + 1, step=1)
naccumulates = 2**naccumulate_exps

# print("naccumulate_exps: {}".format(naccumulate_exps))
print("naccumulates: {}".format(naccumulates))

dtype_data_parameters_2D = np.dtype(
    [
        ("user_spectrum_channels", np.uint32),
        ("fft_length", np.uint32),
        ("naccumulate", np.uint32),
        ("packet_period_s", np.float64),
        ("packet_size_bytes", np.uint32),
        ("data_rate_bytes_per_second", np.float64),
        ("data_rate_bps", np.float64),
    ]
)

# Create a 2D array of all combinations of fft_length and naccumulate
data_parameters = np.zeros(
    (naccumulates.size, fft_lengths.size), dtype=dtype_data_parameters_2D
)

# Fill the numpy structured array with all the data

# Generate a meshgrid of the "coordinates" in this 2D map of data
data_parameters["fft_length"], data_parameters["naccumulate"] = np.meshgrid(
    fft_lengths, naccumulates
)

# Output user spectrum has only half of the channels from complete FFT
data_parameters["user_spectrum_channels"] = data_parameters["fft_length"] / 2

# packetizer
sampling_rate = 4e9
predecimation_factor = 2

data_parameters["packet_period_s"] = (
    data_parameters["naccumulate"]
    * (predecimation_factor / sampling_rate)
    * data_parameters["fft_length"]
)

packet_header_size = 64  # ctypes.sizeof(DataFormatEDDfits.FWHeader)
section_header_size = 8  # ctypes.sizeof(DataFormatEDDfits.FWSectionHeader)
sample_data_size = 4  # ctypes.sizeof(DataFormatEDDfits.TYPE_MAP['F'][0])
nsections = 4  # dualpol_spectrometer
# nsections = 8 # stokes_spectrometer

data_parameters["packet_size_bytes"] = packet_header_size + nsections * (
    section_header_size + sample_data_size * data_parameters["fft_length"]
)

data_parameters["data_rate_bytes_per_second"] = (
    data_parameters["packet_size_bytes"] / data_parameters["packet_period_s"]
)

BITS_PER_BYTE = 8
data_parameters["data_rate_bps"] = (
    data_parameters["data_rate_bytes_per_second"] * BITS_PER_BYTE
)


# Make the graphs
app = QtGui.QApplication([])
win = pg.GraphicsLayoutWidget(show=True)
win.resize(1024, 768)
win.setWindowTitle("Data Rate of EddFits Packets")
pg.setConfigOptions(antialias=True)

# addPlot function returns a PlotItem
fft_index = 8
p1 = win.addPlot(
    title="Spectrum Channels: {}, freq resolution {} kHz: ".format(
        data_parameters["user_spectrum_channels"][0][fft_index],
         (sampling_rate / predecimation_factor / 2 / 1e3) / (data_parameters["user_spectrum_channels"][0][fft_index])
    )
)

p1.plot(
    data_parameters["packet_period_s"][:, fft_index],
    data_parameters["data_rate_bps"][:, fft_index],
    name="name_xxx",
    pen="b",
)

axis_bottom = pg.AxisItem(orientation="bottom", text="Integration Time", units="s")
axis_bottom.showLabel(True)

axis_left = pg.AxisItem(orientation="left", text="Data Rate", units="bps")
axis_left.showLabel(True)

p1.setAxisItems(
    axisItems={
        "bottom": axis_bottom,
        "left": axis_left,
    }
)

p1.setLogMode(x=True, y=True)
p1.showGrid(True, True, alpha=0.7)
p1.addLegend()
# Start the Qt event loop and block this script until user closes the GUI window
# QtGui.QApplication.instance().exec_()
app.exec_()
