# Project: TNRT - Thai National Radio Telescope
# Organization: NARIT - National Astronomical Research Institute of Thailand
# Author: Spiro Sarris
# Date: 2020.04.14

import logging
from abc import ABCMeta
from abc import abstractmethod
from abc import abstractproperty
import time
from astropy.time import Time
import os
import numpy as np
import threading

# QT for Threadpool
from PyQt5 import QtCore

from Acspy.Clients.SimpleClient import PySimpleClient

from Consumer import Consumer
from Supplier import Supplier
from CentralDB import CentralDB

import DataAggregatorMod
import DataAggregatorDefaults
from Subscan import Subscan


class AbstractBasePipeline(object):
    """
    AbstractBasePipeline is an abstract class that cannot be instantiated.
    It defines functions that must be implemented for subclasses of AbstractBasePipeline.
    Some common functions are implemented in AbstractBasePipeline such as enable and disable write
    during real-time operation
    """

    __metaclass__ = ABCMeta

    @abstractmethod
    def write_scan_header(self, mbfits_headers):
        """Writes scan header for current scan.  See subclass for implementation details"""
        pass

    @abstractmethod
    def write_before_subscan(self, mbfits_headers):
        """
        Writes subscan header for current scan.  In the case of Gildas, it doesn't actually write
        data to the file.  It prepares the pyclassfiller data struct with all of the data that is
        available before the scan runs.
        When the actual data from integrations / drifts is available, a few more header items
        are calculated at runtime (because the values are unknown before),
        and added to the header before the data buffer is written to file.

        In the case of MBFITS, this function is NO OP / pass.

        Parameters
        ----------
        mbfits_headers : ScanMod.MetadataNotifyBlock
                struct MetadataNotifyBlock{
                        SCAN_MBFITS_HEADER_TYPE SCAN_MBFITS_HEADER;
                        SCAN_MBFITS_COLUMNS_TYPE SCAN_MBFITS_COLUMNS;
                        FEBEPAR_MBFITS_HEADER_TYPE FEBEPAR_MBFITS_HEADER;
                        ARRAYDATA_MBFITS_HEADER_TYPE ARRAYDATA_MBFITS_HEADER;
                        DATAPAR_MBFITS_HEADER_TYPE DATAPAR_MBFITS_HEADER;
                        };
        """
        pass

    @abstractmethod
    def write_after_subscan(self, mbfits_headers, subscan_number):
        """
        End of subscan.  Write all of the data from this subcscan.
        """
        pass

    @abstractmethod
    def write_monitor(self, mbfits_headers):
        """
        End of scan.  Write all of the data from monitor buffers.
        """
        pass

    @abstractmethod
    def clear_subscan_buffers(self):
        """subscan finished, buffers written, clear buffers to prepare for next subscan"""
        pass

    @abstractmethod
    def clear_monitor_buffers(self):
        """scan finished, buffers written, clear buffers to prepare for next scan"""
        pass

    def __init__(self, metadata_ref):
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.debug(
            "test logger in class %s, module %s" % (self.__class__.__name__, (__name__))
        )

        # Keep a local reference to DataAggregator's metadata structure
        # (Note: SS 06/2021: In the future, use a metadata server?)
        self.metadata = metadata_ref
        self.file = None
        self.file_readback = None
        self.opened = False
        # Member variables that will be used in PipelineStatus and PipelineNotifyBlock
        self.filename = "no name"
        self.filename_base = "no name"
        self.fullpath_temp = "no path"

        self.write_enabled = True  # default is true.  Can be disabled for debugging.
        self.packets_received = 0
        self.packets_received_subscan = 0
        self.packets_written = 0

        self.subscan_dict = {}
        
        # Flow control events
        self.mutex = QtCore.QMutex()

        self.write_subscan_finished = threading.Event()
        self.write_subscan_finished.clear()

        self.write_monitor_finished = threading.Event()
        self.write_monitor_finished.clear()

        self.status_notify_stop_event = threading.Event()
        self.status_notify_stop_event.clear()

        # Get reference to the global QThreadPool.  This threadpool can push
        # function calls to separate thread (up to maximum).
        # If all threads are busy, QThreadPool will manage the queue and not block the
        # program main thread.  Perhaps a long-running thread would provide better performance,
        # but QThreadPool requires much less code to write.  If the system has performance limits
        # when writing many files, Come back here and improve the design with dedicated Threads
        # or some other file write.
        self.threadpool = QtCore.QThreadPool()
        self.logger.debug(
            "Access QThreadPool with maximum %d threads" % self.threadpool.maxThreadCount()
        )

        self.cdb = CentralDB()

    def open(self, mbfits_headers):
        """This open method only resets the counters.  Each subclss must open file"""
        self.opened = True
        self.packets_received = 0
        self.packets_written = 0
        self.packets_received_subscan = 0

        self.data_dir = os.getenv("DATA_DIR") or os.getenv("HOME") + "/data"
        if not os.path.exists(self.data_dir):
            self.logger.logInfo("data directory %s does not exist.  Create now" % self.data_dir)
            os.mkdir(self.data_dir)

        for key in self.outputs:
            self.logger.logInfo("Creating Notification Channel Supplier %s " % key)
            try:
                self.outputs[key]["supplier"] = Supplier(self.outputs[key]["channel"])
            except:
                self.outputs[key]["supplier"] = None
                self.logger.logError("Failed to initialize NC Supplier: %s " % key)

    def close(self):
        self.opened = False

        # If the file does not have any packets of data, delete the file.
        if (os.path.exists(self.fullpath_temp)) and (self.packets_written == 0):
            self.logger.logInfo("packets_written == 0, delete emtpy file %s " % self.fullpath_temp)
            os.remove(self.fullpath_temp)

        for key in self.outputs:
            self.logger.logDebug("disconnect Supplier at key: %s" % key)
            if self.outputs[key]["supplier"] is not None:
                try:
                    self.outputs[key]["supplier"].disconnect()
                except:
                    self.outputs[key]["supplier"] = None        

        # self.filename is set by the subclass when the file is opened.
        return self.filename

    def subscribe(self):
        """Subscribe to notification channels"""
        for key in self.inputs:
            self.logger.logInfo("Creating Notification Channel Consumer: {}".format(key))
            try:
                self.inputs[key]["consumer"] = Consumer(self.inputs[key]["channel"])
                self.inputs[key]["consumer"].add_subscription(self.inputs[key]["handler"])
            except:
                # Set the Consumer to a known state (check this state later on shutdown)
                self.inputs[key]["consumer"] = None
                self.logger.logError("Failed to initialize NC Consumer: {}".format(key))
                # Raise the same exception again and let DataAggregator die with traceback
                raise

    def unsubscribe(self):
        """Unsubscribe from notification channels"""
        for key in self.inputs:
            self.logger.logDebug("disconnect Consumer at key: %s" % key)
            if self.inputs[key]["consumer"] is not None:
                try:
                    self.inputs[key]["consumer"].disconnect()
                except Exception as e:
                    self.inputs[key] = None
                    self.logger.logDebug(
                        "Catch exception while disconnecting Consumer {}: {}".format(key, e)
                    )

    def start_status_notify(self):
        # Create notification channel suppliers to publish combined data
        self.thread_status_notify = threading.Thread(target=self.status_notify_loop)
        self.thread_status_notify.start()

    def stop_status_notify(self):
        self.status_notify_stop_event.set()

    def enable_write(self, subscan_number):
        if self.opened is True:
            self.logger.debug("write_enabled = TRUE")
            self.write_enabled = True
            self.subscan_dict[subscan_number] = Subscan(subscan_number)
        else:
            self.logger.info("opened = FALSE.  open before write_enable")

    def disable_write(self, subscan_number):
        self.logger.debug("write_enabled = FALSE")
        self.write_enabled = False
        self.subscan_dict[subscan_number].set_time_disable_write()
    
    def skip_subscan(self):
        # this function is used to skip write subscan
        # set write_subscan_finished event to unblock write monitor
        self.write_subscan_finished.set()

    def status_notify_loop(self):
        self.logger.logDebug("clear stop event")
        self.status_notify_stop_event.clear()

        UPDATE_PERIOD_S = 0.5

        # Use default pipeline status message until real data pipeline is created and has data.
        pipeline_status = DataAggregatorDefaults.pipeline_status

        self.logger.logDebug("enter the loop while status_notify_stop_event is not set")

        while not self.status_notify_stop_event.is_set():
            # Populate the PipelineNotifyBlock with some data
            time_str = Time.now().iso

            pipeline_status = DataAggregatorMod.PipelineStatus(
                time_str,
                self.__class__.__name__,
                self.filename_base,
                self.write_enabled,
                self.packets_received,
                self.packets_written,
            )

#            self.outputs["pipeline"]["supplier"].publish_event(pipeline_status)
            time.sleep(UPDATE_PERIOD_S)

        self.logger.logDebug("Exit status notify loop")


# Main defined only for testing with data in native format
# Not passing data throuth IDL type translations
if __name__ == "__main__":
    self = AbstractBasePipeline()
