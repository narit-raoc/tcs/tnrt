from astropy.time import Time
from CentralDB import CentralDB
import cdbConstants

class Subscan:
    def __init__(self, subs_num):
        self.arraydata_buffer = []
        self.datapar_buffer = []
        self.time_enable_write = Time.now()
        self.time_disable_write = None
        self.subs_num = subs_num
        self.status = 'running'

        self.metadata = Metadata()        

    def set_status(self, status):
        self.status = status

    def set_time_enable_write(self):
        self.time_enable_write = Time.now()

    def set_time_disable_write(self):
        self.time_disable_write = Time.now()

    def clear_buffer(self):
        list_to_delete = [
            self.arraydata_buffer,
            self.datapar_buffer,
        ]
        
        # Delete
        for item in list_to_delete:
            try:
                del item
            except NameError:
                self.logger.error("buffer | array not defined.  Nothing to delete")

        # Initialize buffers to empty list
        self.arraydata_buffer = []
        self.datapar_buffer = []

class Metadata:
    def __init__(self):
        self.cdb = CentralDB()
        self.arraydata_mbfits_header = {}
        self.datapar_mbfits_header = {}

        self.assign_arraydata_value()
        self.assign_datapar_value()
    
    def assign_arraydata_value(self):
        self.arraydata_mbfits_header['FEBE'] = '{}-{}-{}'.format(
            self.cdb.get(cdbConstants.FRONTEND_NAME),
            self.cdb.get(cdbConstants.BACKEND_NAME),
            self.cdb.get(cdbConstants.PRESET_CONFIG_NAME),
        )
        self.arraydata_mbfits_header['BASEBAND'] = self.cdb.get(cdbConstants.BASEBAND, float)
        self.arraydata_mbfits_header['SCANNUM'] = self.cdb.get(cdbConstants.SCANNUM, int)
        self.arraydata_mbfits_header['SUBSNUM'] = self.cdb.get(cdbConstants.SUBSCAN_NUMBER, int)
        self.arraydata_mbfits_header['DATE-OBS'] =  self.cdb.get(cdbConstants.SUBSCAN_DATEOBS)
        self.arraydata_mbfits_header['CHANNELS'] = self.cdb.get(cdbConstants.CHANNELS, int)
        self.arraydata_mbfits_header['NUSEFEED'] = self.cdb.get(cdbConstants.NUSEFEED, int)
        self.arraydata_mbfits_header['FREQRES'] = self.cdb.get(cdbConstants.FREQRES, float)
        self.arraydata_mbfits_header['BANDWID'] = self.cdb.get(cdbConstants.BANDWID, float)
        self.arraydata_mbfits_header['MOLECULE'] = self.cdb.get(cdbConstants.MOLECULE)
        self.arraydata_mbfits_header['TRANSITI'] = self.cdb.get(cdbConstants.TRANSITI)
        self.arraydata_mbfits_header['RESTFREQ'] = self.cdb.get(cdbConstants.RESTFREQ, float)
        self.arraydata_mbfits_header['SKYFREQ'] = self.cdb.get(cdbConstants.SKYFREQ, float)
        self.arraydata_mbfits_header['SIDEBAND'] = self.cdb.get(cdbConstants.SIDEBAND)
        self.arraydata_mbfits_header['SBSEP'] = self.cdb.get(cdbConstants.SBSEP, float)
        self.arraydata_mbfits_header['2CTYP2'] = self.cdb.get(cdbConstants.SUBS_2CTYP2)
        self.arraydata_mbfits_header['2CRPX2'] = self.cdb.get(cdbConstants.SUBS_2CRPX2, float)
        self.arraydata_mbfits_header['2CRVL2'] = self.cdb.get(cdbConstants.SUBS_2CRVL2, float)
        self.arraydata_mbfits_header['2CDLT2A'] = self.cdb.get(cdbConstants.SUBS_2CDLT2A, float)
        
        self.arraydata_mbfits_header['WCSNM2F'] = self.cdb.get(cdbConstants.WCSNM2F)
        self.arraydata_mbfits_header['1CTYP2F'] = self.cdb.get(cdbConstants.SUBS_1CTYP2F)
        self.arraydata_mbfits_header['1CRPX2F'] = self.cdb.get(cdbConstants.REF_CHANNEL, float)
        self.arraydata_mbfits_header['1CRVL2F'] = self.cdb.get(cdbConstants.FREQ_AT_REF_CH, float)
        self.arraydata_mbfits_header['1CDLT2F'] = self.cdb.get(cdbConstants.FREQRES, float)
        self.arraydata_mbfits_header['1SPEC2F'] = self.cdb.get(cdbConstants.SUBS_1SPEC2F)
        self.arraydata_mbfits_header['1SOBS2F'] = self.cdb.get(cdbConstants.SUBS_1SOBS2F)
        
        self.arraydata_mbfits_header['WCSNM2I'] = self.cdb.get(cdbConstants.WCSNM2I)
        self.arraydata_mbfits_header['1CTYP2I'] = self.cdb.get(cdbConstants.SUBS_1CTYP2I)
        self.arraydata_mbfits_header['1CRPX2I'] = self.cdb.get(cdbConstants.SUBS_1CRPX2I, float)
        self.arraydata_mbfits_header['1CRVL2I'] = self.cdb.get(cdbConstants.SUBS_1CRVL2I, float)
        self.arraydata_mbfits_header['1CDLT2I'] = self.cdb.get(cdbConstants.SUBS_1CDLT2I, float)
        self.arraydata_mbfits_header['1CUNI2I'] = self.cdb.get(cdbConstants.SUBS_1CUNI2I)
        self.arraydata_mbfits_header['1SPEC2I'] = self.cdb.get(cdbConstants.SUBS_1SPEC2I)
        self.arraydata_mbfits_header['1SOBS2I'] = self.cdb.get(cdbConstants.SUBS_1SOBS2I)
        
        self.arraydata_mbfits_header['WCSNM2S'] = self.cdb.get(cdbConstants.WCSNM2S)
        self.arraydata_mbfits_header['1CTYP2S'] = self.cdb.get(cdbConstants.SUBS_1CTYP2S)
        self.arraydata_mbfits_header['1CRPX2S'] = self.cdb.get(cdbConstants.SUBS_1CRPX2S, float)
        self.arraydata_mbfits_header['1CRVL2S'] = self.cdb.get(cdbConstants.SUBS_1CRVL2S, float)
        self.arraydata_mbfits_header['1CDLT2S'] = self.cdb.get(cdbConstants.SUBS_1CDLT2S, float)
        self.arraydata_mbfits_header['1CUNI2S'] = self.cdb.get(cdbConstants.SUBS_1CUNI2S)
        self.arraydata_mbfits_header['1SPEC2S'] = self.cdb.get(cdbConstants.SUBS_1SPEC2S)
        self.arraydata_mbfits_header['1SOBS2S'] = self.cdb.get(cdbConstants.SUBS_1SOBS2S)
        
        self.arraydata_mbfits_header['WCSNM2J'] = self.cdb.get(cdbConstants.WCSNM2J)
        self.arraydata_mbfits_header['1CTYP2J'] = self.cdb.get(cdbConstants.SUBS_1CTYP2J)
        self.arraydata_mbfits_header['1CRPX2J'] = self.cdb.get(cdbConstants.SUBS_1CRPX2J, float)
        self.arraydata_mbfits_header['1CRVL2J'] = self.cdb.get(cdbConstants.SUBS_1CRVL2J, float)
        self.arraydata_mbfits_header['1CDLT2J'] = self.cdb.get(cdbConstants.SUBS_1CDLT2J, float)
        self.arraydata_mbfits_header['1CUNI2J'] = self.cdb.get(cdbConstants.SUBS_1CUNI2J)
        self.arraydata_mbfits_header['1SPEC2J'] = self.cdb.get(cdbConstants.SUBS_1SPEC2J)
        self.arraydata_mbfits_header['1SOBS2J'] = self.cdb.get(cdbConstants.SUBS_1SOBS2J)
        
        self.arraydata_mbfits_header['WCSNM2R'] = self.cdb.get(cdbConstants.WCSNM2R)
        self.arraydata_mbfits_header['1CTYP2R'] = self.cdb.get(cdbConstants.SUBS_1CTYP2R)
        self.arraydata_mbfits_header['1CRPX2R'] = self.cdb.get(cdbConstants.SUBS_1CRPX2R, float)
        self.arraydata_mbfits_header['1CRVL2R'] = self.cdb.get(cdbConstants.SUBS_1CRVL2R, float)
        self.arraydata_mbfits_header['1CDLT2R'] = self.cdb.get(cdbConstants.SUBS_1CDLT2R, float)
        self.arraydata_mbfits_header['1CUNI2R'] = self.cdb.get(cdbConstants.SUBS_1CUNI2R)
        self.arraydata_mbfits_header['1SPEC2R'] = self.cdb.get(cdbConstants.SUBS_1SPEC2R)
        self.arraydata_mbfits_header['1SOBS2R'] = self.cdb.get(cdbConstants.SUBS_1SOBS2R)
        
        self.arraydata_mbfits_header['1VSOU2R'] = self.cdb.get(cdbConstants.SUBS_1VSOU2R, float)
        self.arraydata_mbfits_header['1VSYS2R'] = self.cdb.get(cdbConstants.SUBS_1VSYS2R, float)


    def assign_datapar_value(self):
        self.datapar_mbfits_header['SCANNUM'] = self.cdb.get(cdbConstants.SCANNUM, int)
        self.datapar_mbfits_header['OBSNUM'] = self.cdb.get(cdbConstants.SUBSCAN_NUMBER, int)
        self.datapar_mbfits_header['SUBSNUM'] = self.cdb.get(cdbConstants.SUBSCAN_NUMBER, int)
        self.datapar_mbfits_header['DATE-OBS'] = self.cdb.get(cdbConstants.SUBSCAN_DATEOBS)
        self.datapar_mbfits_header['FEBE'] = '{}-{}-{}'.format(
            self.cdb.get(cdbConstants.FRONTEND_NAME),
            self.cdb.get(cdbConstants.BACKEND_NAME),
            self.cdb.get(cdbConstants.PRESET_CONFIG_NAME),
        )

        self.datapar_mbfits_header['LST'] = self.cdb.get(cdbConstants.LST, float)
        self.datapar_mbfits_header['SUBSTYPE'] = self.cdb.get(cdbConstants.SUBSTYPE)
        self.datapar_mbfits_header['SCANTYPE'] = self.cdb.get(cdbConstants.SCANTYPE)
        self.datapar_mbfits_header['SCANMODE'] = self.cdb.get(cdbConstants.SCANMODE)
        self.datapar_mbfits_header['SCANDIR'] = self.cdb.get(cdbConstants.SCANDIR)
        self.datapar_mbfits_header['SCANLEN'] = self.cdb.get(cdbConstants.SCANLEN, float)
        self.datapar_mbfits_header['SCANXVEL'] = self.cdb.get(cdbConstants.SCANXVEL, float)
        self.datapar_mbfits_header['SCANTIME'] = self.cdb.get(cdbConstants.SCANTIME, float)
        self.datapar_mbfits_header['SCANXSPC'] = self.cdb.get(cdbConstants.SCANXSPC, float)
        self.datapar_mbfits_header['SCANYSPC'] = self.cdb.get(cdbConstants.SCANYSPC, float)
        self.datapar_mbfits_header['SCANSKEW'] = self.cdb.get(cdbConstants.SCANSKEW, float)
        self.datapar_mbfits_header['SCANPAR1'] = self.cdb.get(cdbConstants.SCANPAR1, float)
        self.datapar_mbfits_header['SCANPAR2'] = self.cdb.get(cdbConstants.SCANPAR2, float)
        
        self.datapar_mbfits_header['NLNGTYPE'] = self.cdb.get(cdbConstants.SUBS_NLNGTYPE)
        self.datapar_mbfits_header['NLATTYPE'] = self.cdb.get(cdbConstants.SUBS_NLATTYPE)

        self.datapar_mbfits_header['DPBLOCK'] = self.cdb.get(cdbConstants.SUBS_DPBLOCK, bool)
        self.datapar_mbfits_header['NINTS'] = self.cdb.get(cdbConstants.SUBS_NINTS, int)
        self.datapar_mbfits_header['OBSTATUS'] = self.cdb.get(cdbConstants.SUBS_OBSTATUS)
        self.datapar_mbfits_header['WOBCOORD'] = self.cdb.get(cdbConstants.SUBS_WOBCOORD, bool)