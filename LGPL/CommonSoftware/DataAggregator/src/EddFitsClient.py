"""
Copyright (c) 2018 Ewan Barr <ebarr@mpifr-bonn.mpg.de>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

import logging
import socket
import errno
import ctypes as C
import numpy as np
import coloredlogs
import os
import queue
import threading

from argparse import ArgumentParser

from DataFormatMBfits import DataFormatMBfits
from DataFormatEDDfits import TYPE_MAP
from DataFormatEDDfits import NOISEDIODETIMEDELTA
from DataFormatEDDfits import FWSectionHeader
from DataFormatEDDfits import FWHeader

# Define all state machine strings
state_new_instance = "NEW INSTANCE"
state_connect_failed = "CONNECT FAILED"
state_connected_idle = "CONNECTED IDLE"
state_wait_first_data = "WAIT FOR FIRST DATA"
state_receiving_data = "RECEIVING DATA"
state_receive_error = "RECEIVE ERROR"
state_disconnected = "DISCONNECTED"

# Import for simulation
import glob
import time
from astropy.time import Time
from astropy import units as units


class FitsClient(object):
    """
    Wrapper class for a KATCP client to a EddFitsWriterServer
    """

    def __init__(self, address, record_dest, standalone=False, standalone_log_level="DEBUG", hack_timestamps=True):
        """
        @brief      Construct new instance
                                If record_dest is not empty, create a folder named record_dest and record the received packages there.
        """
        if standalone is False:
            # getLogger will use ACS Python Logger
            self.logger = logging.getLogger(self.__class__.__name__)
            self.logger.setLevel(logging.DEBUG)
        else:
            # use standalone colored logger
            self.logger = get_colored_logger(self.__class__.__name__, standalone_log_level)

        if hack_timestamps == True:
            self.logger.warning("hack_timestamps = True.  All packets subtract 1 * integration time from timestamp")

        self._address = address
        self.__record_dest = record_dest
        if record_dest:
            if not os.path.isdir(record_dest):
                os.makedirs(record_dest)
        self._stop_event = threading.Event()
        self._socket = None
        self.__last_package = "0"
        self._header = None
        self._sections = None

        # simulation .dat file
        self._simulation = True
        self.dat = []
        self._currentdat = None
        self.count = 0

        if self._simulation is True:
            datfiles = []
            for file in glob.glob("../test/200401_EDD_FitsWriter_TCP_Output/*.dat"):
                datfiles.append(file)

            for i in datfiles:
                f = open(i, "rb")
                self.dat.append(f.read())  # read the entire file into data)

        # deinterleave packets
        self.diQueue = queue.Queue()
        self.searching_for_pairs = True
        self.search_pair_false_count = 0
        self.max_search_pair_packets = 3

        # process packets
        self.handlers = []

        # Set initial state
        self.set_current_state(state_new_instance)

    def __del__(self):
        pass

    def set_current_state(self, new_state):
        self.current_state = new_state
        # TODO (SS. 11/2022): Add a notification message for DataMonitor to display the state in GUI
        self.logger.info("FSM State: {}".format(self.current_state))
    
    def set_time_enable_write(self):
        self.time_enable_write = Time.now()

    def get_current_state(self):
        return self.current_state

    def register_handler(self, handler_function):
        if callable(handler_function):
            self.logger.debug("register handler: {}".format(handler_function))
            self.handlers.append(handler_function)
        else:
            self.logger.debug(
                "handler_function: {} is not callable.  ignore".format(handler_function)
            )

    def unregister_handler(self, handler_function):
        try:
            self.logger.debug("unregister handler: {}".format(handler_function))
            h = self.handlers.pop(self.handlers.index(handler_function))
        except Exception as e:
            self.logger.warning(e)
            self.lgger.debug(
                "handler {} not found in self.handlers.  cannot remove".format(handler_function)
            )

    def unregister_all_handlers(self):
        self.logger.debug("remove all handler functions")
        # We  could simply write self.handlers = [], but I want the log messages to show what was removed.
        while len(self.handlers) > 0:
            h = self.handlers.pop()
            self.logger.debug("unregsister handler function: {}".format(h))

    def process_queue(self):
        if self.diQueue.qsize() < 2:
            self.logger.debug(
                "qsize is {}. waiting for another packet before search for pairs".format(
                    self.diQueue.qsize()
                )
            )
            return

        if self.searching_for_pairs:
            self.logger.info("searching_for_pairs=True ...")
            packet_A = self.diQueue.get()
            packet_B = self.diQueue.get()
            time_A = Time(packet_A["header"].timestamp[:-4], format="isot", scale="utc").unix
            time_B = Time(packet_B["header"].timestamp[:-4], format="isot", scale="utc").unix

            self.logger.debug(
                "len(timestring_A): {}, timestring_A: {}, time_A: {}".format(
                    len(packet_A["header"].timestamp),
                    packet_A["header"].timestamp,
                    time_A,
                )
            )
            self.logger.debug(
                "len(timestring_B): {}, timestring_B: {}, time_B: {}".format(
                    len(packet_B["header"].timestamp),
                    packet_B["header"].timestamp,
                    time_B,
                )
            )
            dt_seconds = time_B - time_A
            self.logger.debug(
                "qsize after get: {}. dt_seconds: {} [s]".format(self.diQueue.qsize(), dt_seconds)
            )
            if np.abs(np.abs(dt_seconds) - NOISEDIODETIMEDELTA) < (NOISEDIODETIMEDELTA / 2):
                self.logger.info(
                    "found pair of packets.  deinterleave into separate {polarization, noise state} sections"
                )

                # Overwrite nsectoins in packet_A header and reuse
                packet_A["header"].nsections *= 2

                # Construct a new packet with data structured re-organized
                self.handler_eddfits_packet(
                    packet_A["header"],
                    packet_A["section_header_list"] + packet_B["section_header_list"],
                    packet_A["section_data_list"] + packet_B["section_data_list"],
                )
            else:
                self.logger.warning(
                    "current packets A and B are not a pair. Discard A. Send B -> A in the queue and wait for next B"
                )
                self.diQueue.put(packet_B)
                self.search_pair_false_count += 1

                # # Uncomment when we figure out how to handle missing packets from backend
                # # Always searching for pairs for now
                
                # if self.search_pair_false_count >= self.max_search_pair_packets:
                #     self.logger.info(
                #         "pair not found after {} packets. set searching_for_pairs=False".format(
                #             self.max_search_pair_packets
                #         )
                #     )
                #     self.searching_for_pairs = False
                return
        else:
            self.logger.info(
                "searching_for_pairs=False. pass data through without change (no switching noise ON / OFF"
            )
            # not searching for pairs by timestamp.
            # get() from the queue and pass through to handler function
            packet = self.diQueue.get()
            self.handler_eddfits_packet(
                packet["header"],
                packet["section_header_list"],
                packet["section_data_list"],
            )

    def handler_eddfits_packet(self, header, section_header_list, section_data_list):
        # Generate numpy dtype that has correct dimensions for the new packet of spectrum data
        # NOTE: Assume the number spectrum channels is the same for all sections of data.
        # Therefore, use the number of channels in the first section to define the array dimensions.
        dtype_arraydata_row = DataFormatMBfits.generate_dtype_arraydata_row(
            header.nsections, section_header_list[0].nchannels
        )

        self.logger.debug("generated dtype: {}".format(dtype_arraydata_row))

        # Note: EDDFits FWPacket has timestamp in format same as astropy "isot" but appends "UTC "
        # We must remove the last 4 characters "UTC " before we can create astropy.Time object."
        # Also, calculate MJD time here and add to the IDL packet.
        timestamp_apy = Time(header.timestamp[:-4], format="isot", scale="utc")
        timestamp_mjd = timestamp_apy.mjd

        # Create an array using dtype for MBFITS and fill with data from EDDFITS packet
        # flip the spectrum (frequency high->low  low->high()?
        # arraydata_row = np.array((timestamp_mjd, np.flip(np.asarray(section_data_list), axis=1)), dtype=dtype_arraydata_row)
        # ... or don't flip the spectrum
        arraydata_row = np.array(
            (timestamp_mjd, np.asarray(section_data_list)), dtype=dtype_arraydata_row
        )
        self.logger.debug("timestamp: {}, data: {}".format(timestamp_apy, arraydata_row["DATA"][:,0]))
        # self.logger.debug('call {} registered handler functions on packet time_mjd: {}'.format(len(self.handlers), arraydata_row['MJD']))
        for handler_function in self.handlers:
            # self.logger.debug('call handler function: {}'.format(handler_function))
            handler_function(header, arraydata_row)

    def connect(self):
        retry = 3
        self._socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._socket.setblocking(True)
        # mock server local host can use socket_timeout but it's doesn't work with real EDD fits_interface
        # socket_timeout = 5.0 # second
        # self._socket.settimeout(socket_timeout)       
        while retry > 0:
            retry = retry - 1
            try:
                self.logger.info(
                    "connecting to {}:{} ...".format(self._address[0], self._address[1])
                )
                self._socket.connect(self._address)
                self.set_current_state(state_connected_idle)
                t0 = Time.now()
                self.time_enable_write = t0
                self.logger.info("connected. start time: {}".format(t0.iso))
                break
            except socket.error as error:
                self.logger.error("EINPROGRESS Port in use, try again {}".format(retry))
                self.logger.error(error)
                if retry <= 0:
                    raise
            except Exception as error:
                self.set_current_state(state_connect_failed)
                raise

    def recv_nbytes(self, nbytes):
        received_bytes = 0
        data = b""
        while received_bytes < nbytes:
            try:
                # self.logger.debug("Requesting {} bytes".format(nbytes - received_bytes))
                if self._simulation is True:
                    current_data = self._currentdat[0:nbytes]
                    time.sleep(1)
                    self._currentdat = self._currentdat[nbytes:]
                else:
                    current_data = self._socket.recv(nbytes - received_bytes)

                received_bytes += len(current_data)
                data += current_data
                self.logger.debug(
                    "Received {} bytes ({} of {} bytes)".format(
                        len(current_data), received_bytes, nbytes
                    )
                )
            except socket.error as error:
                error_id = error.args[0]
                if error_id == errno.EAGAIN or error_id == errno.EWOULDBLOCK:
                    time.sleep(0.1)
                else:
                    self.logger.exception("Unexpected error on socket recv: {}".format(str(error)))
                    raise error
        return data

    def start_recv_loop(self):
        if self.current_state in [state_connected_idle]:
            self.logger.debug("start recv_loop and block this thread until recv_loop is canceled")
            self.recv_loop()

            self.logger.debug("shutdown and close socket")
            self._socket.shutdown(socket.SHUT_RD)
            self._socket.close()

            self.set_current_state(state_disconnected)
        else:
            self.logger.warning(
                "current_state is not in [state_connected_idle]. Do not start recv_loop"
            )

    def recv_loop(self):
        # Loop forever until loop is canceled.  I think it is possible
        # for this loop to exit while a partial (non-blocking) socket read is in progress.
        # For now, I don't care because the client socket will be closed immediately,
        # then the server socket will be closed.  New server and client for the next
        # connection should not be affected by any partial data remaining from an old
        # connection.
        self._stop_event.clear()
        self.set_current_state(state_wait_first_data)
        while not self._stop_event.is_set():
            try:
                if self._simulation is True:
                    n = len(self.dat) - 1
                    if self.count <= n:
                        self._currentdat = self.dat[self.count]
                    else:
                        self._simulation = False
                    self.count += 1

                packet_dict = self.recv_packet()

                self.set_current_state(state_receiving_data)

                # Check timestamp of packet.  If packet is old data from previous subscan
                # (time is before the creation of this client), *do not* add to the queue
                t = Time(packet_dict["header"].timestamp[:-4], format="isot", scale="utc")
                self.logger.debug(
                    "time: received packet={}, enable_write start={}".format(
                        t.iso, self.time_enable_write
                    )
                )

                if t < self.time_enable_write:
                    self.logger.warning("detect old data. discard.  do not add to the queue")
                else:
                    # Add to the queue to prepare for deinterleave of noise doide ON / OFF states
                    self.diQueue.put(packet_dict)
                    self.logger.debug("qsize after put: {}".format(self.diQueue.qsize()))

                    # Search for pairs of packets in the time-interleaved queue
                    self.process_queue()

            except Exception:
                self.logger.exception("Failure while receiving packet")
                # Signal to state machine to change state if we catch any exception
                self.set_current_state(state_receive_error)
                raise
        self.logger.debug("exit receive loop")
        # Signal state machine to indicate receive looped stopped by command -- not from error
        self.set_current_state(state_connected_idle)

    def stop_recv_loop(self):
        self.logger.debug("set _stop_event")
        self._stop_event.set()

    def recv_packet(self):
        self.logger.debug("Receive waiting for packet header ...")
        raw_header = self.recv_nbytes(C.sizeof(FWHeader))
        self.logger.debug("Converting packet header")
        header = FWHeader.from_buffer_copy(raw_header)
        self.logger.info("Received header: {}".format(header))
        
        # Start Hack --------------------------------------------------------- 
        # https://gitlab.com/narit-raoc/tnrt/-/issues/599
        # We believe that the timestamp that from Dual pol GatedSpectrometer / EddFitsServer
        # is assigned to the wrong packet.  The double-cross scan data from L-band is symmetric
        # after we subtract 1 * integration time from all packets. 
        # NOTE: integration time is milliseconds
        # TODO (SS 06/2023): remove this hack if we can find and fix the root cause in EDD or elsewhere
        self.logger.debug("Before hack:\t{}, integration_time: {} [us]".format(
            header.timestamp, header.integration_time))
                
        # Calculate the new timestamp
        t_hacked = Time(header.timestamp[:-4], format="isot", scale="utc") - (header.integration_time * units.us)
        
        # Overwrite the timestamp using original format from EddFitsServer
        header.timestamp = (t_hacked.isot+"UTC ").encode("utf8")

        self.logger.debug("After hack:\t{}".format(header.timestamp))
        ## End Hack -----------------------------------------------------
        
        if header.timestamp.decode() < self.__last_package:
            self.logger.error("Timestamps out of order!")
        else:
            self.__last_package = header.timestamp.decode()

        if self.__record_dest:
            filename = os.path.join(self.__record_dest, "FWP_{}.dat".format(header.timestamp))
            while os.path.isfile(filename):
                self.logger.warning("Filename {} already exists. Add suffix _".format(filename))
                filename += "_"
            self.logger.info("Recording to file {}".format(filename))
            ofile = open(filename, "wb")
            ofile.write(raw_header)

        fw_data_type = header.channel_data_type.decode().strip().upper()
        c_data_type, np_data_type = TYPE_MAP[fw_data_type]
        section_header_list = []
        section_data_list = []
        for section in range(header.nsections):
            self.logger.debug("Receiving section {} of {}".format(section + 1, header.nsections))
            raw_section_header = self.recv_nbytes(C.sizeof(FWSectionHeader))
            if self.__record_dest:
                ofile.write(raw_section_header)

            section_header = FWSectionHeader.from_buffer_copy(raw_section_header)
            self.logger.debug("Section {} header: {}".format(section, section_header))
            self.logger.debug("Receiving section data")
            raw_bytes = self.recv_nbytes(C.sizeof(c_data_type) * section_header.nchannels)
            if self.__record_dest:
                ofile.write(raw_bytes)        
            data = np.frombuffer(raw_bytes, dtype=np_data_type)
            self.logger.debug("Section {} section_data[0]: {}".format(section, data[0]))
            section_header_list.append(section_header)
            section_data_list.append(data)

        if self.__record_dest:
            ofile.close()

        # Return a dictionary
        return {
            "header": header,
            "section_header_list": section_header_list,
            "section_data_list": section_data_list,
        }


def get_colored_logger(name, log_level):
    fmt_scrn = "%(asctime)s.%(msecs)03d [%(levelname)s] %(name)s.%(funcName)s(): %(message)s"

    level_styles_scrn = {
        "critical": {"color": "red", "bold": True},
        "debug": {"color": "white", "faint": True},
        "error": {"color": "red"},
        "info": {"color": "green", "bright": True},
        "notice": {"color": "magenta"},
        "spam": {"color": "green", "faint": True},
        "success": {"color": "green", "bold": True},
        "verbose": {"color": "blue"},
        "warning": {"color": "yellow", "bright": True, "bold": True},
    }
    field_styles_scrn = {
        "asctime": {},
        "hostname": {"color": "magenta"},
        "levelname": {"color": "cyan", "bright": True},
        "name": {"color": "blue", "bright": True},
        "programname": {"color": "cyan"},
    }
    formatter_screen = coloredlogs.ColoredFormatter(
        fmt=fmt_scrn, level_styles=level_styles_scrn, field_styles=field_styles_scrn
    )

    # creating a handler to log on the console
    handler_screen = logging.StreamHandler()
    handler_screen.setFormatter(formatter_screen)

    logger = logging.getLogger(name)
    logger.addHandler(handler_screen)

    if log_level.upper() == "DEBUG":
        logger.setLevel(logging.DEBUG)
    elif log_level.upper() == "INFO":
        logger.setLevel(logging.INFO)
    elif log_level.upper() == "WARNING":
        logger.setLevel(logging.WARNING)
    elif log_level.upper() == "ERROR":
        logger.setLevel(logging.ERROR)
    else:
        logger.setLevel(logging.DEBUG)
        logger.error("invalid parameter log-level. Select level DEBUG")

    return logger


if __name__ == "__main__":
    parser = ArgumentParser(
        description="Receive (and optionally record) tcp packages send by EDD Fits writer interface."
    )
    parser.add_argument("-H", "--host", dest="host", type=str, help="Host interface to connect to")
    parser.add_argument("-p", "--port", dest="port", type=int, help="Port number to connect to")
    parser.add_argument(
        "--log-level", dest="log_level", type=str, help="Log level", default="DEBUG"
    )

    parser.add_argument(
        "--record-to",
        dest="record_to",
        type=str,
        help="Destination to record data to. No recording if empty",
        default="",
    )
    parser.add_argument(
        "--no-hack-timestamps",
        dest="no_hack_timestamps",
        default=False,
        action="store_true",
        help="DO NOT subtract 1 * integration_time from timestamps of spectrometer packets \
            to 'undo' what we believe is an incorrect timestamp in the EDD system. \
            Ref https://gitlab.com/narit-raoc/tnrt/-/issues/599 \
            IMPORTANT: Remove this hack if we can find the root cause in EDD Packetizer / Spectrometer / EddfitsServer",
    )
    args = parser.parse_args()

    logger = get_colored_logger("__main__", args.log_level)

    logger.debug("test logger in module {}".format(__name__))

    logger.info("Starting EddFitsClient")
    client = FitsClient(
        (args.host, args.port), 
        args.record_to, 
        standalone=True, 
        standalone_log_level=args.log_level, 
        hack_timestamps=not(args.no_hack_timestamps)
        )
    try:
        client.connect()
        client.start_recv_loop()
    except ConnectionRefusedError:
        logger.error(
            "ConnectionRefusedError.  Cannot connect eddfits spectrum server {}:{}".format(
                args.host, args.port
            )
        )
        pass
    except KeyboardInterrupt:
        logger.warning("Catch KeyboardInterrupt.  stop client")
        client.stop_recv_loop()
