# Project: TNRT - Thai National Radio Telescope
# Company: NARIT - National Astronomical Research Institute of Thailand
# Author: Spiro Sarris

# Standard python packages
import logging
import signal
import numpy as np

import socketserver
import ctypes
import time
import coloredlogs
from astropy.time import Time
import astropy.units as units
from argparse import ArgumentParser

# TNRT packages
from DataFormatEDDfits import FWHeader
from DataFormatEDDfits import FWSectionHeader
from DataSimulator import PointingSimulator


# Artificial time delta between noise diode on / off status (same as value in edd_fits_interface.py)
_NOISEDIODETIMEDELTA = 0.001
# Artificial latency in sending packets.  The real EddFitsServer delays all packets by at least 
# 3.0 seconds.  It inserts incoming SPEAD (?) / mkrecv packets into a queue.  Incoming packets are 
# not guaranteed to be in the correct time sequence, so EddFitsServer inserts the incoming packets 
# into the queue at the correct time sequence position.  Then, it pops the oldest packet form the
#  queue (guaranteed correct time sequence) and send out to the socket to EddFitsClient.
LATENCY = 0.0 # second

class FWPacketHandler(socketserver.BaseRequestHandler):
    """
    It is instantiated once per connection to the server, and must
    override the handle() method to implement communication to the
    client.
    """

    def handle(self):
        self.logger = get_colored_logger(self.__class__.__name__, "DEBUG")
        
        # Generate a fake FWPacket send it in a loop forever.
        self.logger.debug("Client connected. Start serving messages ...")
        self.logger.debug(
            "period: {}, nsections: {}, nchannels: {}".format(
                self.server.period, self.server.nsections, self.server.nchannels
            )
        )
        loop_counter = -1
        while True:
            # Simulate waiting for BackendEDD to accumulate data before send packet
            time.sleep(self.server.period)
            
            loop_counter = loop_counter + 1
            # Generate data
            section_header_list = []
            section_data_list = []
            noise_data_list = []
            for section_id in range(self.server.nsections):
                section_header = FWSectionHeader(section_id + 1, self.server.nchannels)
                
                self.logger.debug("loop counter: {}".format(loop_counter))
                if self.server.debug_pattern == True:
                    self.logger.debug("debug_pattern = True.  Generate dummy byte pattern instead of spectrum")
                    selected_spectrum_data = 2 * loop_counter * np.ones(self.server.sim.data3D[self.server.slice_index][section_id].shape)
                else:
                    self.logger.debug("debug_pattern == False. Use simulated spectrum data")
                    selected_spectrum_data = self.server.sim.data3D[self.server.slice_index][section_id]
                
                section_data = selected_spectrum_data.astype(np.float32)
                # Generate some fake "signal+noise" data.  Read actual data and add +1.  Just to make
                # data unique for comparing packets.
                noise_data = (1 + selected_spectrum_data).astype(np.float32)
                
                section_header_list.append(section_header)
                section_data_list.append(section_data)
                noise_data_list.append(noise_data)

            self.logger.debug(
                "Data from slice [{}, 0 ,:] of simulation datacube shape {}".format(
                    self.server.slice_index, self.server.sim.data3D.shape
                )
            )

            # Assign all of the header parameters that remain the same for
            # packets with noise calibration ON or OFF
            data_type = "EEEI".encode()  # C.c_char * 4
            channel_data_type = "F".encode()  # C.c_char * 4
            packet_size = ctypes.sizeof(FWHeader) + self.server.nsections * (
                ctypes.sizeof(FWSectionHeader) + 4 * self.server.nchannels
            )  # C.c_uint32
            backend_name = "EDDSPEAD".encode()  # C.c_char * 8
            integration_time = int(
                1000000 * self.server.period
            )  # C.c_uint32.  milliseconds
            nsections = int(self.server.nsections)  # C.c_uint32)
            blocking_factor = 1  # C.c_uint32)

            timestamp_apy = Time.now() - (integration_time * units.us / 2) - (LATENCY * units.s)

            # If noise calibration packets are enabled, the following parameters
            # will be overwritten and reused for the noise packet

            # NOTE the timestamp string must have 4 more characters at the end
            # 3 charachters "UTC" + one SPACE " " character to receive data from
            # edd_fits_interface that follows the  APEX Fits Writer spec.
            timestamp = (timestamp_apy.isot + "UTC ").encode()
            blank_phases = 2  # calibration noise OFF

            packet_header = FWHeader(
                data_type,
                channel_data_type,
                packet_size,
                backend_name,
                timestamp,
                integration_time,
                blank_phases,
                nsections,
                blocking_factor,
            )

            # In the first iteration of the loop, check the user parameter split_pair.  If the user has sets this option = True,
            # we will not send the first signal packet (without noise cal).  Continue to the next packet of noise cal data.
            # This send logic allows us to test the EddFitsClient receive queue logic of matching pairs.  If we
            # don't send the signal (no cal) packet, the client logic should discard the first noise cal packet and continue to
            # search for a valid pair sequence of (signal, signal + noise).
            
            # Loop is in first iteration and we config not to send the first signal packet
            if (loop_counter == 0) and (self.server.interleave_noise_packets == True) and (self.server.split_pair == True):
                self.logger.warning("loop_counter == 0 and split_pair == True.  Break the pair pattern and DO NOT send first signal packet")
                pass
            else:
                # After the first loop we will always send the signal packet
                try:
                    self.send_packet(packet_header, section_header_list, section_data_list)
                except ConnectionResetError:
                    break
                except BrokenPipeError:
                    break            

            # If we want to send another packet to simulate the noise diode source ON from GatedSpectrometer
            # Re-use the same packet data, but change the timestamp to use the aritifical offset used by
            # edd_fits_interface (to identify packets that include noise enabled).  This encoding of noise
            # in a fake time offset comes from history of compatibility with APEX Fits Writer.

            if self.server.interleave_noise_packets == True:
                self.logger.debug(
                    "timestamp of actual packet: {}".format(timestamp_apy.isot)
                )
                timestamp_apy_noise_packet = timestamp_apy + _NOISEDIODETIMEDELTA * units.s
                self.logger.debug(
                    "timestamp of offset packet: {}".format(timestamp_apy_noise_packet.isot)
                )

                packet_header.timestamp = (timestamp_apy_noise_packet.isot + "UTC ").encode()  # C.c_char * 28

                packet_header.blank_phases = 1  # calibration noise ON

                try:
                    self.send_packet(
                        packet_header, section_header_list, noise_data_list
                    )
                except ConnectionResetError:
                    break
                except BrokenPipeError:
                    break

            self.server.slice_index = (
                self.server.slice_index + 1
            ) % self.server.sim.data3D.shape[0]

    def send_packet(self, packet_header, section_header_list, section_data_list):
        self.logger.debug(
            "Send packet header {} bytes. packet_header: {} ".format(
                ctypes.sizeof(packet_header), packet_header
            )
        )
        self.request.sendall(packet_header)

        for section_id in range(self.server.nsections):
            self.logger.debug(
                "Send section {} header, {} bytes. section_header: {}".format(
                    section_id,
                    ctypes.sizeof(section_header_list[section_id]),
                    section_header_list[section_id],
                )
            )
            self.request.sendall(section_header_list[section_id])

            self.logger.debug(
                "Send section {} data, {} bytes. section_data[0]: {}".format(
                section_id, 
                len(section_data_list[section_id]),
                section_data_list[section_id][0]
                ))
            
            self.request.sendall(section_data_list[section_id].tobytes())

    def handle_timeout(self):
        self.logger.info("handle_timeout")


class TCPSpectrumServer(socketserver.TCPServer):
    """
    Inherits TCPServer.  Create this class only so we can set instance variables explicitly
    """

    def __init__(
        self,
        host_port_tuple,
        handler,
        subscan_duration,
        period,
        nsections,
        nchannels,
        interleave_noise_packets,
        debug_pattern,
        split_pair,
    ):
        # Save user parameters
        self.subscan_duration = subscan_duration
        self.period = period
        self.nsections = nsections
        self.nchannels = nchannels
        self.interleave_noise_packets = interleave_noise_packets
        self.debug_pattern = debug_pattern
        self.split_pair = split_pair

        self.sim = PointingSimulator(
            self.subscan_duration,
            self.period,
            nusefeed=nsections,
            nchan=nchannels,
            fs=2e9,
        )
        self.slice_index = 0

        # Allow reuse of IP / PORT immediately after close.  Don't wait for OS to cleanup (~ 30 seconds)
        self.allow_reuse_address = True
        super().__init__(host_port_tuple, handler)


class EddFitsMockServer:
    """
    docstring
    """

    # Constructor
    def __init__(
        self,
        host,
        port,
        subscan_duration=30,
        period=1.0,
        nsections=1,
        nchannels=1024,
        interleave_noise_packets=True,
        log_level="INFO",
        debug_pattern=False,
        split_pair=False,
    ):
        self.logger = get_colored_logger(self.__class__.__name__, log_level)
        
        # Get IP address and Port to create the socket servers
        self.host = host
        self.port = port
        self.subscan_duration = subscan_duration
        self.period = period
        self.nsections = nsections
        self.nchannels = nchannels
        self.interleave_noise_packets = interleave_noise_packets
        self.debug_pattern = debug_pattern
        self.split_pair = split_pair

        self.logger.info("Setting SIGTERM and SIGINT handler")
        signal.signal(signal.SIGTERM, self.handle_sigterm)
        signal.signal(signal.SIGINT, self.handle_sigint)

    def run(self):
        self.logger.info(
            "Starting socketserver IP address: {}, port: {}, subscan_duration: {}, period: {}, nsections: {}, nchannels: {}, interleave_noise_packets: {}, debug_pattern: {}, split_pairs: {}".format(
                self.host,
                self.port,
                self.subscan_duration,
                self.period,
                self.nsections,
                self.nchannels,
                self.interleave_noise_packets,
                self.debug_pattern,
                self.split_pair,
            )
        )
        self.logger.warning("Constant LATENCY set to: {} [s]".format(LATENCY))

        with TCPSpectrumServer(
            (self.host, self.port),
            FWPacketHandler,
            self.subscan_duration,
            self.period,
            self.nsections,
            self.nchannels,
            self.interleave_noise_packets,
            self.debug_pattern,
            self.split_pair,
        ) as self.server:
            # Add user parameters to server instance
            self.server.serve_forever()

    def handle_sigterm(self, param1, param2):
        self.logger.info(
            "Catch signal SIGTERM. param1: {} param2: {}. Raise KeyboardInterrupt and shutdown server".format(
                param1, param2
            )
        )
        raise KeyboardInterrupt

    def handle_sigint(self, param1, param2):
        self.logger.info(
            "Catch signal SIGINT. param1: {} param2: {}. Raise KeyboardInterrupt and shutdown server".format(
                param1, param2
            )
        )
        raise KeyboardInterrupt

    def cleanup(self):
        self.logger.warning(
            "socket servers shutdown(). wait serve_forever() to complete"
        )
        self.server.shutdown()

        self.logger.info("socket servers server_close()")
        self.server.server_close()

def get_colored_logger(name, log_level):
    fmt_scrn = "%(asctime)s.%(msecs)03d [%(levelname)s] %(name)s.%(funcName)s(): %(message)s"

    level_styles_scrn = {
        "critical": {"color": "red", "bold": True},
        "debug": {"color": "white", "faint": True},
        "error": {"color": "red"},
        "info": {"color": "green", "bright": True},
        "notice": {"color": "magenta"},
        "spam": {"color": "green", "faint": True},
        "success": {"color": "green", "bold": True},
        "verbose": {"color": "blue"},
        "warning": {"color": "yellow", "bright": True, "bold": True},
    }
    field_styles_scrn = {
        "asctime": {},
        "hostname": {"color": "magenta"},
        "levelname": {"color": "cyan", "bright": True},
        "name": {"color": "blue", "bright": True},
        "programname": {"color": "cyan"},
    }
    formatter_screen = coloredlogs.ColoredFormatter(
        fmt=fmt_scrn, level_styles=level_styles_scrn, field_styles=field_styles_scrn
    )

    # creating a handler to log on the console
    handler_screen = logging.StreamHandler()
    handler_screen.setFormatter(formatter_screen)

    logger = logging.getLogger(name)
    logger.addHandler(handler_screen)

    if log_level.upper() == "DEBUG":
        logger.setLevel(logging.DEBUG)
    elif log_level.upper() == "INFO":
        logger.setLevel(logging.INFO)
    elif log_level.upper() == "WARNING":
        logger.setLevel(logging.WARNING)
    elif log_level.upper() == "ERROR":
        logger.setLevel(logging.ERROR)
    else:
        logger.setLevel(logging.DEBUG)
        logger.error("invalid parameter log-level. Select level DEBUG")

    return logger

if __name__ == "__main__":
    parser = ArgumentParser(
        description="Generate simulation spectrum data and serve packets using EDDFits format"
    )
    parser.add_argument(
        "-H",
        "--host",
        dest="host",
        type=str,
        default="127.0.0.1",
        help="Hostname or ip address to serve packets",
    )
    parser.add_argument(
        "-p",
        "--port",
        dest="port",
        type=int,
        default=5002,
        help="Port number to serve packets",
    )
    parser.add_argument(
        "-d",
        "--subscan_duration",
        dest="subscan_duration",
        type=float,
        default=10.0,
        help="Subscan duration.  Duration of simulation data array before repeat",
    )
    parser.add_argument(
        "-T",
        "--period",
        dest="period",
        type=float,
        default=1.0,
        help="Integration period.  Time [seconds] between output packets",
    )
    parser.add_argument(
        "-s",
        "--nsections",
        dest="nsections",
        type=int,
        default=2,
        help="Number of sections (polarizations / stokes products not including gated noise on / off)",
    )
    parser.add_argument(
        "-n",
        "--nchannels",
        dest="nchannels",
        type=int,
        default=1024,
        help="Number of spectrum channels",
    )
    parser.add_argument(
        "-c",
        "--noisecal",
        dest="noisecal",
        default=False,
        action="store_true",
        help="Interleave calibration noise packets. \
            Results in total output sections = nsections * 2.  \
            Default=False (--noisecal True is the behavior of EDD GatedSpectrometerPipeline)",
    )
    parser.add_argument(
        "--log-level", dest="log_level", type=str, help="Log level", default="INFO"
    )
    parser.add_argument(
        "--debug-pattern",
        dest="debug_pattern",
        default=False,
        action="store_true",
        help="Generate sequential increasing value pattern instead of simulate spectrum.  \
            Conveinent for testing packet sequence, alignment.  \
            Default=False (--noisecal True is the behavior of EDD GatedSpectrometerPipeline)",
    )
    parser.add_argument(
        "--split-pair",
        dest="split_pair",
        default=False,
        action="store_true",
        help="Usually the connection starts with a pair of packets timestamps {t0, t0 + 0.001s}.  \
            split_pair=True starts the connection with a partial pair (one packet at t0 + 0.001s) \
            that should be discarded. \
            Allows test of EddFistClient logic in pair-matching queue.",
    )

    args = parser.parse_args()

    logger = get_colored_logger("__main__", args.log_level)

    logger.info(repr(args))

    server = EddFitsMockServer(
        args.host,
        args.port,
        args.subscan_duration,
        args.period,
        args.nsections,
        args.nchannels,
        args.noisecal,
        args.log_level,
        args.debug_pattern,
        args.split_pair,
    )
    try:
        logger.info("Run EDDFitsMockServer.  CTRL+C to stop")
        server.run()
    except KeyboardInterrupt:
        logger.warning("Catch keyboard interrupt.  Stop EddFitsMockServer")
        server.cleanup()
