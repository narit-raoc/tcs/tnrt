# Project: TNRT - Thai National Radio Telescope
# Organization: NARIT - National Astronomical Research Institute of Thailand
# Author: Spiro Sarris
# Date: 2020.04.24

# Python modules
import logging
import time
import threading
import numpy as np
from astropy.time import Time
from astropy.coordinates import EarthLocation

# ACS modules
from Acspy.Servants.ACSComponent import ACSComponent
from Acspy.Servants.ContainerServices import ContainerServices
from Acspy.Servants.ComponentLifecycle import ComponentLifecycle
from Acspy.Servants.ComponentLifecycle import ComponentLifecycleException
from AcsutilPy.ACSPorts import getIP
import ACSErrTypeCommonImpl

# TNRT project modules
import DataAggregatorMod
import DataAggregatorMod__POA
import DataAggregatorErrorImpl
import DataAggregatorError
import DataAggregatorDefaults
import DataSimulator
import tnrtAntennaMod
import atmosphere
import EddFitsClient
from EddFitsClient import state_new_instance
from EddFitsClient import state_connect_failed
from EddFitsClient import state_connected_idle
from EddFitsClient import state_wait_first_data
from EddFitsClient import state_receiving_data
from EddFitsClient import state_receive_error
from EddFitsClient import state_disconnected

# import weatherComNet # wait for NARIT's weather station
import ScanMod
import BackendMod
import ScanDefaults
import AntennaDefaults
import TypeConverterDA as tc
from DataFormatMONITORfits import DataFormatMONITORfits
from DataFormatMBfits import DataFormatMBfits
from DataFormatALMATIfits import DataFormatALMATIfits
from PipelineALMATIfits import PipelineALMATIfits
from PipelineGildas import PipelineGildas
from PipelineMBfits import PipelineMBfits
from PipelineSpectrumPreview import PipelineSpectrumPreview
from DataFormatEDDfits import FWHeader

from Supplier import Supplier
from Consumer import Consumer


class DataAggregator(
    DataAggregatorMod__POA.DataAggregator,
    ACSComponent,
    ContainerServices,
    ComponentLifecycle,
):
    """
    Create notification channel consumers for all of the inputs
    Create notification channel supplier for the combined data output.  Output event is
    triggered by backend input event.  After each integration / data dump from Backend,
    the DataAggregator pushes out a data structure with the new data from Backend
    and the most recent saved data from other inputs.

    Data will arrive at different times from different inputs, So they cannot be synchronized exactly.
    Possible to do some interpolation, but investigate that later.
    """

    # ------------------------------------------------------------------------------------
    # Constructor and destructor
    # ------------------------------------------------------------------------------------
    def __init__(self):
        """
        Create references and initialize to None
        """
        ACSComponent.__init__(self)
        ContainerServices.__init__(self)
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.logDebug(
            "test logger in class %s, module %s" % (self.__class__.__name__, (__name__))
        )
        self.logger.logDebug(self.logger.getLevels())

        self.ip = getIP()

        # Initialize references to access CDB to read configurations of components
        self.sc = None

        # Data file record path to record raw data from EddFitsClient
        # None means don't record data to file at directory <self.fits_client_record_path>
        self.fits_client_record_path = None
        self.fits_client = None

        # Flow control events
        self.event_stop_simulate_spectrum_preview_loop = threading.Event()

        # Initialize dictionary of important data for notification channel inputs.
        # We can use this data to iterate through inputs
        self.consumers = {
            "antenna": {
                "channel": tnrtAntennaMod.STATUS_CHANNEL_NAME,
                "dtype": tnrtAntennaMod.AntennaStatusNotifyBlock,
                "handler": self._handler_antenna,
                "consumer": None,
            },
            # "atmosphere": {
            #     "channel": atmosphere.ATMCHANNEL,
            #     "dtype": atmosphere.opacityNotifyBlock,
            #     "handler": self._handler_atmosphere,
            #     "consumer": None,
            # },
            "scan": {
                "channel": ScanMod.CHANNEL_NAME_METADATA,
                "dtype": ScanMod.MetadataNotifyBlock,
                "handler": self._handler_scan,
                "consumer": None,
            },
        }

        # Initialize dictionary data for notification channel output
        self.suppliers = {}

        # Map of pipeline names to actual Pipeline classes
        self.pipeline_map = {
            "gildas": PipelineGildas,
            "mbfits": PipelineMBfits,
            "atfits": PipelineALMATIfits,
            "spectrum_preview": PipelineSpectrumPreview,
        }

        # Empty dictionary of data pipelines.  Pipeline object will be created
        # and added to the dictionary key from self.pipelines_available
        self.pipelines = {}

        # ---------------------------------------------------------------------------
        # Initialize an internal data structure called "metadata" to combine data from
        # NotificationChannel
        # Also, add pipeline status (comes from DataAggregator, not from outside)

        # Populate the complete metadata structure.  Add fields in alphabetical order for readability
        # Python dictionary might mix the order internally, but we don't care.
        self.metadata = DataAggregatorDefaults.metadata

    def __del__(self):
        """
        Set all variables to None
        """
        self.sc = None
        self.consumers = None

    # ------------------------------------------------------------------------------------
    # LifeCycle functions called by ACS system when we activate and deactivate components.
    # Inherited from headers in Acspy.Servants.ComponentLifecycle.
    # ------------------------------------------------------------------------------------
    def initialize(self):
        self.logger.logDebug("initialize()")

        # Read hostnames and ports from file
        self.edd_fits_server_host = DataAggregatorDefaults.edd_fits_server_host
        self.edd_fits_server_port = DataAggregatorDefaults.edd_fits_server_port
        self.logger.logInfo(
            "Found edd_fits_server_host = {}, edd_fits_server_port = {}".format(
                self.edd_fits_server_host, self.edd_fits_server_port
            )
        )

        # Create notification channel consumers for all sources of data
        for key in self.consumers:
            self.logger.logInfo("Creating Notification Channel Consumer: {}".format(key))
            try:
                self.consumers[key]["consumer"] = Consumer(self.consumers[key]["channel"])
                self.consumers[key]["consumer"].add_subscription(self.consumers[key]["handler"])
            except:
                # Set the Consumer to a known state (check this state later on shutdown)
                self.consumers[key]["consumer"] = None
                # Raise the same exception again and let DataAggregator die with traceback
                self.logger.logError("Failed to initialize NC Consumer: {}".format(key))
                raise

        # Create notification channel suppliers to publish combined data
        for key in self.suppliers:
            self.logger.logInfo("Creating Notification Channel Supplier: {}".format(key))
            try:
                self.suppliers[key]["supplier"] = Supplier(self.suppliers[key]["channel"])
            except:
                # Set the Suppliers to a known state (check this state later on shutdown)
                self.suppliers[key]["supplier"] = None
                self.logger.logError("Failed to initialize NC Supplier: {}".format(key))
                # Raise the same exception again and let DataAggregator die with traceback
                raise

        self.pipeline_notify_enabled = False

    def execute(self):
        self.logger.logDebug("execute()")

    def aboutToAbort(self):
        self.logger.logDebug("aboutToAbort()")

    def cleanUp(self):
        self.logger.logDebug("cleanUp()")
        self.pipeline_notify_enabled = False
        time.sleep(0.2)

        self.fits_client_stop()

        for key in self.consumers:
            self.logger.logDebug("disconnect Consumer at key: %s" % key)
            if self.consumers[key]["consumer"] is not None:
                try:
                    self.consumers[key]["consumer"].disconnect()
                except Exception as e:
                    self.consumers[key] = None
                    self.logger.logDebug(
                        "Catch exception while disconnecting Consumer {}: {}".format(key, e)
                    )

        for key in self.suppliers:
            self.logger.logDebug("disconnect Supplier at key: %s" % key)
            if self.suppliers[key]["supplier"] is not None:
                try:
                    self.suppliers[key]["supplier"].disconnect()
                except Exception as e:
                    self.suppliers[key]["supplier"] = None
                    self.logger.logDebug("Catch exception while disconnecting Supplier {}: {}".format(key, e))

    # ------------------------------------------------------------------------------------
    # Functions defined in IDL to interface from outside this class
    # ------------------------------------------------------------------------------------
    def get_ip(self):
        return self.ip

    def open_pipeline(self, file_format):
        """
        Add a file writer object to the dictionary of file writers.
        """
        # Initialize references for Pipelines
        try:
            self.pipelines[file_format] = self.pipeline_map[file_format](self.metadata)
            self.pipelines[file_format].open(self.metadata["scan"])
            self.pipelines[file_format].subscribe()
            self.pipelines[file_format].start_status_notify()
        except KeyError as e:
            self.logger.logError('key "%s" not found in pipelines.keys()' % file_format)

    def close_pipeline(self, file_format):
        """
        Stop writing and close the file
        """
        filename = ""
        try:
            self.pipelines[file_format].unsubscribe()
            filename = self.pipelines[file_format].close()
            self.logger.logDebug(
                "pipeline {} closed. pipeline returned filename: {}".format(file_format, filename)
            )
            self.logger.logDebug(
                "pop file_format {} from dictionary of pipelines".format(file_format)
            )
            self.pipelines[file_format].stop_status_notify()
            # Remove the key from dictionary of active data pipelines
            self.pipelines.pop(file_format)
        except KeyError as e:
            self.logger.logError('key "%s" not found in pipelines.keys()' % file_format)
        except DataAggregatorError.timeoutOnCloseErrorEx as e:
            self.logger.logError(e)

        self.logger.logDebug("return filename: {}".format(filename))
        return filename

    def enable_write(self, file_format, subscan_number):
        """
        Start writing data.  Sets a function pointer such that call to
        write will call writePacket instread of writeNull
        """
        try:
            self.logger.logDebug("enable write for pipeline: {}".format(file_format))
            self.pipelines[file_format].enable_write(subscan_number)

        except AttributeError as e:
            self.logger.logError(e)
        except KeyError as e:
            self.logger.logError('key "%s" not found in pipelines.keys()' % file_format)

    def disable_write(self, file_format, subscan_number):
        """
        Stop writing data.  Sets a function pointer to a pass function.
        Equivalent to writing to null, bwrite_afut no interaction with OS /dev/null
        """
        try:
            self.logger.logDebug("disable write for pipeline: {}".format(file_format))
            self.pipelines[file_format].disable_write(subscan_number)
        except KeyError as e:
            self.logger.logError('key "%s" not found in pipelines.keys()' % file_format)

    def write_scan_header(self):
        """
        Write Scan headers to all open pipelines.  This function
        only sends the scan metadata block (as defined in Scan.idl) to the
        data Pipeline object.  Each data pipeline is responsible for translating / formatting
        data to the appropriate file type.
        """
        for index in self.pipelines:
            self.logger.logDebug("write scan header for pipeline: {}".format(index))
            self.pipelines[index].write_scan_header(self.metadata["scan"])

    def write_before_subscan(self):
        """
        Write Subscan headers to all open pipelines.  This function
        only sends the scan metadata block (as defined in Scan.idl) to the
        data Pipeline object.  Each data pipeline is responsible for translating / formatting
        data to the appropriate file type.
        """
        for index in self.pipelines:
            self.logger.logDebug("write before subscan for pipeline: {}".format(index))
            self.pipelines[index].write_before_subscan(self.metadata["scan"])

    def write_after_subscan(self, subscan_number):
        """
        astropy.io.fits does not have a convenient method to append rows to binary tables
        in real-time because it uses numpy arrays that are sequential in memory.  Therefore
        data are buffered in lists in DataAggregator.  At the end of a subscan, the lists
        are converted to numpy arrays of the correct format for MBFITS and written to file
        here.
        TODO : if this technique consumes too much memory for long subscans and large dimension
        arrays, find a better way to write blocks of data to the file throughout subscan
        before memory is overloaded.  Or, change to use a separate package fitsio instead
        of astropy.io.fits because the documentation suggests that it can append to files in
        realtime better than astropy.io.fits
        """
        for index in self.pipelines:
            self.logger.logDebug("write after subscan for pipeline: {}".format(index))
            self.pipelines[index].write_after_subscan(self.metadata["scan"], subscan_number)

    def skip_subscan(self):
        """
        set write_subscan_finished event to unblock loop in write_monitor
        """
        for index in self.pipelines:
            self.logger.logDebug("skip subscan for pipeline: {}".format(index))
            self.pipelines[index].skip_subscan()

    def write_monitor(self):
        """
        Buffer the common monitor data in one place here in DataAggregator Unlike the packets from HoloFFT or EDDFits
        because we want to use this data in more than one place, and I don't want to create copies of the same data.
        """
        try:
            for index in self.pipelines:
                self.logger.logDebug("write monitor for pipeline: {}".format(index))
                self.pipelines[index].write_monitor(self.metadata["scan"])
        except Exception as e:        
            self.logger.logError("Catch local Python exception: {}".format(e))
            acs_corba_exception = DataAggregatorErrorImpl.WriteErrorExImpl()
            acs_corba_exception.addData("errExpl", str(e))
            self.logger.logError("Raise CORBA exception: {}".format(acs_corba_exception))
            raise acs_corba_exception 

    def clear_subscan_buffers(self):
        for index in self.pipelines:
            self.logger.logDebug("clear subscan buffers for pipeline: {}".format(index))
            self.pipelines[index].clear_subscan_buffers()

    def clear_monitor_buffers(self):
        for index in self.pipelines:
            self.logger.logDebug("clear monitor buffers for pipeline: {}".format(index))
            self.pipelines[index].clear_monitor_buffers()

    def get_metadata(self):
        # IDL interface cannot return a python dictionary directly, so pack the
        # Python dictionary into a struct that is defined in the DataAggregator.idl
        metadata_struct = DataAggregatorMod.MetadataNotifyBlock(
            self.metadata["antenna"],
            self.metadata["atmosphere"],
            self.metadata["backend"],
            self.metadata["scan"],
            # self.metadata['weather'] # wait for NARIT's weather station
        )

        self.logger.logDebug("type of return object: {}".format(type(metadata_struct)))
        return metadata_struct

    def fits_client_start(self):
        """
        @brief Connect EddFitsClient to receive spectrum data
        """
        try:
            current_state = self.fits_client.get_current_state()
            self.logger.logInfo("EddFitsClient object current_state: {}".format(current_state))
        except AttributeError:
            self.logger.logInfo("Instance of EddFitsClient not created yet. Create new")
            self.fits_client = EddFitsClient.FitsClient(
                (self.edd_fits_server_host, self.edd_fits_server_port),
                self.fits_client_record_path,
            )
            current_state = self.fits_client.get_current_state()

        running_states = [state_connected_idle, state_wait_first_data, state_receiving_data]
        if current_state in running_states:
            self.logger.logWarning("current_state: {} in {}.".format(current_state, running_states))
            self.logger.logWarning("Do not create another client. return")
            return

        error_states = [state_connect_failed, state_receive_error]
        if current_state in error_states:
            self.logger.logWarning("current_state {} in {}".format(current_state, error_states))
            self.logger.logWarning("stop, delete and create new")
            self.fits_client.stop_recv_loop()
            del self.fits_client
            self.fits_client = EddFitsClient.FitsClient(
                (self.edd_fits_server_host, self.edd_fits_server_port),
                self.fits_client_record_path,
            )
            current_state = self.fits_client.get_current_state()

        new_object_states = [state_new_instance]
        if current_state in new_object_states:
            # Register spectrum handler functions in FitsClient to further process (preview, write to file, etc.)
            self.logger.logInfo("current_state {} in {}".format(current_state, new_object_states))
            self.logger.logInfo(
                "Register handler functions for {} pipelines".format(len(self.pipelines))
            )
            for index in self.pipelines:
                try:
                    self.logger.logDebug("register handler for pipeline: {}".format(index))
                    self.fits_client.register_handler(
                        self.pipelines[index].handler_spectrum_arraydata
                    )
                except AttributeError as e:
                    self.logger.logError(
                        "Pipeline {} does not have function handler_spectrum_arraydata. Do nothing. {}".format(
                            index, e
                        )
                    )

        disconnected_states = [state_new_instance, state_disconnected]
        if current_state in disconnected_states:
            self.logger.logInfo("current_state {} in {}".format(current_state, disconnected_states))
            self.logger.logInfo("Connect EddFitsclient")
            try:
                self.fits_client.connect()
            except ConnectionRefusedError as e:
                self.logger.logError("Catch Python exception and raise ACS exception")
                acs_exception = DataAggregatorErrorImpl.ConnectionRefusedErrorExImpl(exception=e)
                acs_exception.addData(
                    "errExpl",
                    "Failed to connect to eddfits spectrum server {}:{}".format(
                        self.edd_fits_server_host, self.edd_fits_server_port
                    ),
                )
                raise acs_exception

            self.logger.logInfo("Starting EddFitsClient run loop in a separate thread")
            # Note - EddFitsClient receive loop will block the thread until loop is finished.
            # therefore, we must start this client in a separate thread and not block
            # DataAggregator IDL fits_client_start.
            self.recv_loop_thread = threading.Thread(
                target=self.fits_client.start_recv_loop, name="recv_loop_thread"
            )
            self.recv_loop_thread.daemon = True
            self.recv_loop_thread.start()

    def fits_client_stop(self):
        """
        @brief Stop a fits interface client.
        """
        try:
            self.logger.logInfo("Stop fits_client")
            self.fits_client.stop_recv_loop()
            self.fits_client.unregister_all_handlers()
            self.recv_loop_thread.join()
            del self.fits_client
        except AttributeError:
            self.logger.logWarning("fits_client instance not found. nothing to stop")

    def simulate_spectrum_preview_start(
        self, subscan_duration, integration_time, nsections, nchannels
    ):
        # Start the Spectrum Preview pipeline to handle the data
        self.open_pipeline("spectrum_preview")
        self.thread_simulate_spectrum_preview = threading.Thread(
            target=self._simulate_spectrum_preview_loop,
            args=(subscan_duration, integration_time, nsections, nchannels),
        )
        self.logger.logInfo("Start simulate_spectrum_preview_loop")
        self.event_stop_simulate_spectrum_preview_loop.clear()
        self.thread_simulate_spectrum_preview.start()

    def simulate_spectrum_preview_stop(self):
        self.logger.logInfo("Stop simulate_spectrum_preview_loop")
        self.event_stop_simulate_spectrum_preview_loop.set()
        self.close_pipeline("spectrum_preview")

    def simulate_spectrum_arraydata_subscan(
        self, subscan_duration, integration_time, nsections, nchannels
    ):
        """
        Run in this thread and block until finished. Makes the flow control a lot easier.
        """
        self.logger.logDebug(
            "Simulate spectrum arraydata from subscan_duration: {}, integration_time: {}, nsections: {}, nchannels: {}".format(
                subscan_duration, integration_time, nsections, nchannels
            )
        )
        # Simulate datacube, iterate through it and publish data to be handled by DataAggregator and pipelines.
        offaxis_arcsec = -100
        sim = DataSimulator.PointingSimulator(
            subscan_duration,
            integration_time,
            nusefeed=nsections,
            nchan=nchannels,
            offaxis=offaxis_arcsec,
        )

        # Creata notification supplier to use locally in this test
        temporary_antenna_supplier = Supplier(tnrtAntennaMod.STATUS_CHANNEL_NAME)

        # Simulate some Antenna data to insert the offset angles from our simulation in the loop.
        eventAntenna = AntennaDefaults.metadata

        # Generate the numpy dtype that has correct array dimensions
        dtype_arraydata_row = DataFormatMBfits.generate_dtype_arraydata_row(nsections, nchannels)
        self.logger.logDebug("Generated dtype: {}".format(dtype_arraydata_row))

        # Simulate subscan 1 arm of pointing shape from (-) Azimuth to (+) Azimuth.  Publish data
        for index_angle in range(sim.data3D.shape[0]):
            # Publish the new offset angles
            self.logger.logDebug(
                "set fake offset angles. index: {}, az: {} [arcsec], ({} [deg])".format(
                    index_angle,
                    sim.angles[index_angle],
                    sim.angles[index_angle] / 3600.0,
                )
            )
            eventAntenna.tracking_sts.prog_offset_az = sim.angles[index_angle] / 3600.0
            temporary_antenna_supplier.publish_event(eventAntenna)

            header = FWHeader()
            header.integration_time = int(integration_time * 1e6)
            arraydata_row = np.array(
                (Time.now().mjd, sim.data3D[index_angle]), dtype=dtype_arraydata_row
            )
            # self.logger.logDebug("Shape of sim.data3D[index_angle]: {}".format(sim.data3D[index_angle].shape))
            # self.logger.logDebug("Shape of arraydata_row['DATA']: {}".format(arraydata_row['DATA'].shape))

            # Call all handler_spectrum_arraydata functions that are available in self.pipelines.
            # In real operation, these pipelines will be registered to edd fits client if they have the function
            # handler_spectrum_arraydata. Then EddFitsClient will call the handlers in the receive loop.
            # In this test, we call handler_spectrum_arraydata on all open pipelines, even if the pipeline doesn't have
            # the function.  Catch the exception, no problem.
            for index in self.pipelines:
                try:
                    self.logger.logTrace(
                        "call handler_spectrum_arraydata for pipeline: {}".format(index)
                    )
                    self.pipelines[index].handler_spectrum_arraydata(header, arraydata_row)
                except AttributeError as e:
                    self.logger.logError(
                        "Pipeline {} does not have function handler_spectrum_arraydata. Do nothing. {}".format(
                            index, e
                        )
                    )

            # sleep for integration time so data flows at the actual speed in the real system
            time.sleep(integration_time)

        # Loop is finished.  disconnect the supplier
        temporary_antenna_supplier.disconnect()

    # ------------------------------------------------------------------------------------
    # Internal functions not shared in IDL
    # ------------------------------------------------------------------------------------
    def _simulate_spectrum_preview_loop(
        self, subscan_duration, integration_time, nsections, nchannels
    ):
        # Generate simulation data
        sim = DataSimulator.PointingSimulator(
            subscan_duration, integration_time, nusefeed=nsections, nchan=nchannels
        )
        slice_index = 0

        dtype_arraydata_row = DataFormatMBfits.generate_dtype_arraydata_row(nsections, nchannels)
        self.logger.logDebug("Generated dtype: {}".format(dtype_arraydata_row))

        # Loop through data array, create arrays using the MBFits ARRAYDATA format and pass to handlers.
        while not self.event_stop_simulate_spectrum_preview_loop.is_set():
            self.logger.logInfo("simulate integration number {}".format(slice_index))

            header = FWHeader()
            header.integration_time = int(integration_time * 1000)
            arraydata_row = np.array(
                (Time.now().mjd, sim.data3D[slice_index]), dtype=dtype_arraydata_row
            )
            self.pipelines["spectrum_preview"].handler_spectrum_arraydata(header, arraydata_row)

            # Increase the slice index to read.  After we read the last slice, I want to repeat forever
            # Use the '%" modulo operator to bring the index back into the available range'
            slice_index = (slice_index + 1) % sim.data3D.shape[0]

            # use wait instead of sleep because it can cancel immediately if anothe thread calls function stop_simulate
            self.event_stop_simulate_spectrum_preview_loop.wait(timeout=integration_time)

    def _handler_antenna(self, data_idl):
        # self.logger.logInfo("antenna")
        # self.logger.logDebug('handle event from antenna')
        lock = threading.Lock()
        lock.acquire()
        self.metadata["antenna"] = data_idl
        lock.release()

    def _handler_atmosphere(self, dataStruct):
        # self.logger.logDebug('handle event from atmosphere')
        lock = threading.Lock()
        lock.acquire()
        self.metadata["atmosphere"] = dataStruct
        lock.release()

    def _handler_scan(self, dataStruct):
        self.logger.logInfo("handle event from scan")
        lock = threading.Lock()
        lock.acquire()
        self.metadata["scan"] = dataStruct
        # generate data format of array data with dimensions from current scan configuration
        lock.release()

    def _handler_weather(self, dataStruct):
        # self.logger.logDebug('handle event from weather')
        lock = threading.Lock()
        lock.acquire()
        self.metadata["weather"] = dataStruct
        lock.release()

        monitor_weather_row = np.array(0, dtype=self.dtype_monitor_weather_row)
        monitor_weather_row["MJD"] = Time.now().mjd
        self.monitor_weather_buffer.append(monitor_weather_row)


# Main defined only for testing with data in native format
# Not passing data throuth IDL type translations
if __name__ == "__main__":
    self = DataAggregator()
