# -*- coding: ascii -*-
# Project: TNRT - Thai National Radio Telescope
# Organization: NARIT - National Astronomical Research Institute of Thailand
# Author: Spiro Sarris
# Date: 2021.05.06

import numpy as np
from astropy.io.fits import Header
from astropy.io.fits import Card
from astropy.io.fits import ColDefs

from DataFormatMONITORfits import DataFormatMONITORfits
import DataFormatFitsPrimitives as fp
# -------------------------------------------------------------
# Generate ALMATI-FITS headers
# -------------------------------------------------------------
# Fits header can generate cards automatically from tuple (keyword', value, comment),
# but create the list of Card objects explicitly to make more obvious to 
# someone who reads this code in the future (SS).

# Note: The ALMATI-FITS specification document tnrt/Documents/AlmaTIFor_v3.pdf
# revision 3, 2001-11-27 shows the complete file structure of ALMATI-FITS includes
# the following list of headers and tables by keyword:

# PRIMARY
# DATAPAR-ALMATI
# CALIBR-ALMATI
# CORRDATA-ALMATI
# AUTODATA-ALMATI
# HOLODATA-ALMATI
# MONITOR-***

# In the TNRT system, we will only use this ALMATI-FITS data format for
# Holography data collections.  
# Therefore, CALBR-ALMATI, CORRDATA-ALMATI, and AUTODATA-ALMATI are not immplemented.
# The File created by TNRT control software should have the following headers and tables:

# PRIMARY
# DATAPAR-ALMATI (1 per subscan)
# HOLODATA-ALMATI (1 per subscan)
# MONITOR-***

class DataFormatALMATIfits(DataFormatMONITORfits):
	'''
	Note: Inherit monitor point data formats (ACU status message, weather station, ...} from common format.  
	This common monitor data format is also inherited by DataFormatMBfits
	'''
	def generate_header_primary(hdr_idl_primary, hdr_idl_scan):
		'''
		Generate PRIMARY header from IDL struct
		
		Parameters
		----------
		hdr_idl : ScanMod.PRIMARY_MBFITS_HEADER_TYPE
		
		Returns
		-------
		hdr_apy : astropy.io.fits.Header
		
		NOTE (SS 04/2021) check if ASCII strings are limited to 13 characters or some other limit.
		Only TELESCOP is specified to have type 13A. Others are just A
		
		TODO: (SS. 04/2021): Almati spec v3 (Lucas, 2001) does not have the header cards:
			 'OBSERVER'
			 'SCAN-NUM'
			 But, Yebes Aries21 ALMATIDataFormat.java includes these keys.  Add them later if needed

		NOTE: (ss. 05/2021) Astropy.io.fits sets BITPIX=8 automatically (why 8?) and doesn't use
			any number we pass in the Card.value parameter.
		'''
		cards = [
			Card(keyword='NAXIS',		value=0,	comment='type="I" value=0'),
			Card(keyword='SIMPLE',		value=True,	comment='type="L" value=True'),
			Card(keyword='BITPIX',		value=32,	comment='type="I" value=32'), 
			Card(keyword='EXTEND',		value=True,	comment='type="L" value=True'),
			Card(keyword='TELESCOP',	value=hdr_idl_primary.TELESCOP,	comment='Telescope Name [13A]'),
			Card(keyword='ORIGIN',		value=hdr_idl_primary.ORIGIN,	comment='Organisation or Institution [A]'), 
			Card(keyword='CREATOR',		value=hdr_idl_primary.CREATOR,	comment='Software (including version) [A]'),
			Card(keyword='OBSERVER',	value=hdr_idl_scan.OBSID, 		comment='Observer and operator initials [24A]'), # NOTE.  this card is not in the ALMA spec, but follow OAN Yebes format for convenience
			Card(keyword='SCAN-NUM',	value=hdr_idl_scan.SCANNUM, 	comment='Scan number [1J]'), # NOTE.  this card is not in the ALMA spec, but follow OAN Yebes format for convenience
			Card(keyword='COMMENT',		value=hdr_idl_primary.COMMENT,	comment='[A]')
			]
		header_apy = Header(cards)
		return(header_apy)

	def generate_header_datapar(hdr_idl_primary, hdr_idl_scan, hdr_dict_datapar):
		'''
		Generate DATAPAR-ALMATI header from IDL struct (uses data from DATAPAR-MBFITS format)
		
		Parameters
		----------
		hdr_idl_primary : IDL struct of header values from this file
		hdr_idl_scan : IDL struct of header values from this SCAN
		hdr_dict_datapar : dictionary of header values from Subscan Metadata
						where original data comes from in MBFITS DATPAR header
		
		Returns
		-------
		hdr_apy : astropy.io.fits.Header

		NOTE: header key 'NO_SWITCH' is 9 characters long from spec.  only 8 characters allowd. truncate to 8
		(Same as Yebes)
		'''
		cards = [
			Card(keyword='EXTNAME',		value='DATAPAR-ALMATI',			comment='name of this table "DATAPAR-ALMATI"'),
			Card(keyword='TABLEREV',	value=3,						comment='Table format revision number type=J'),
			Card(keyword='TELESCOP',	value=hdr_idl_primary.TELESCOP,	comment='Telescope Name [13A]'),
			Card(keyword='SCAN-NUM',	value=hdr_dict_datapar['SCANNUM'],	comment='Scan number'),
			Card(keyword='OBS-NUM',		value=hdr_dict_datapar['SUBSNUM'],	comment='Subscan number'),
			Card(keyword='DATE-OBS',	value=hdr_dict_datapar['DATE-OBS'],	comment='Subscan start in ISO-T format [23A]'),
			Card(keyword='TIMESYS',		value=hdr_idl_scan.TIMESYS,		comment='Time system {TAI, UTC, UT1} [04A]'),
			Card(keyword='LST',			value=hdr_dict_datapar['LST'],		comment='Local apparent sidereal time subs. start [s]'),
			Card(keyword='OBSMODE',		value=hdr_dict_datapar['SUBSTYPE'],	comment='Subscan type [4A]'),
			Card(keyword='PROJID',		value=hdr_idl_scan.PROJID,		comment='Project ID [4A]'),
			Card(keyword='AZIMUTH',		value=0,						comment='Azimuth at start of subscan type=D'),
			Card(keyword='ELEVATIO',	value=0,						comment='Elevation at start of subscan type=D'),
			Card(keyword='LATSYS',		value=hdr_dict_datapar['NLATTYPE'],	comment='Native system type (latitude) XLAT-SFL [8A]'),
			Card(keyword='LONGSYS',		value=hdr_dict_datapar['NLNGTYPE'],	comment='Native system type (longitude) XLON-SFL [8A]'),
			Card(keyword='EXPOSURE',	value=0,						comment='Total integration time. type=E [s]'),
			Card(keyword='NO_ANT',		value=1,						comment='number of base-bands type=J'),
			Card(keyword='NO_BAND',		value=1,						comment='number of antennas type=J'),
			Card(keyword='NO_CHAN',		value=0,						comment='Total number of spectral channels type=J'),
			Card(keyword='NO_FEED',		value=1,						comment='number of polar. feeds (1/2) type=J'),
			Card(keyword='NO_POL',		value=1,						comment='number of polar. products (1/2/4) type=J'),
			Card(keyword='NO_SIDE',		value=1,						comment='number of sidebands type=J'),
			Card(keyword='NO_PHCOR',	value=0,						comment='number of different averaging results type=J'),
			Card(keyword='NO_CORR',		value=0,						comment='number of correlation tables per bb type=J'),
			Card(keyword='NO_AUTO',		value=0,						comment='number of auto correlation tables per bb type=J'),
			Card(keyword='NO_HOLO',		value=1,						comment='number of SingleDish Holo tables (0/1) type=J'),
			Card(keyword='VFRAME',		value=0,						comment='radial vel. correction type=D [m/s]'),
			Card(keyword='OBS-LONG',	value=hdr_idl_scan.SITELONG,	comment='Observatory longitude. type=D [deg]'),
			Card(keyword='OBS-LAT',		value=hdr_idl_scan.SITELAT,		comment='Observatory latitude type=D [deg]'),
			Card(keyword='OBS-ELEV',	value=hdr_idl_scan.SITEELEV,	comment='Observatory elevation type=D [m]'),
			Card(keyword='SOURCE',		value=hdr_idl_scan.X_OBJECT,	comment='Source name [12A]'),
			Card(keyword='CALCODE',		value=hdr_idl_scan.CALCODE,		comment='Calibrator Code [04A]'),
			Card(keyword='RADESYS',		value='ICRS',					comment='Equatorial Coordinate System [04A]'),
			Card(keyword='RA',			value=0,						comment='Right Ascension (ICRF) type=D [deg]'),
			Card(keyword='DEC',			value=0,						comment='Declination (ICRF) type=D [deg]'),
			Card(keyword='PMRA',		value=0,						comment='Proper motion RA*cosDec type=D [deg/yr]'),
			Card(keyword='PMDEC',		value=0,						comment='Proper motion DEC type="D" [deg/yr]'),
			Card(keyword='EQUINOX',		value=2000,						comment='Equinox [Julian years]'),
			Card(keyword='VELTYP',		value='',						comment='Velocty Type [A]'),
			Card(keyword='GLON',		value=0,						comment='Galactic Longitude type=D [deg]'),
			Card(keyword='GLAT',		value=0,						comment='Galactic Latitude type=D [deg]'),
			Card(keyword='ELON',		value=0,						comment='Ecliptic Longitude type=D [deg]'),
			Card(keyword='ELAT',		value=0,						comment='Ecliptic Latitude type=D [deg]'),
			Card(keyword='AZIM-FIX',	value=0,						comment='Fixed Azimuth type=D [deg]'),
			Card(keyword='ELEV-FIX',	value=0,						comment='Fixed Elevation type=D [deg]'),
			Card(keyword='DISTANCE',	value=hdr_idl_scan.DISTANCE,	comment='Geocentric Distance [AU]'),
			Card(keyword='CALMODE',		value='',						comment='Calibration mode [12A]'),
			Card(keyword='UT1UTC',		value=0,						comment='UT1-UTC time translation type=D [s]'),
			Card(keyword='TAIUTC',		value=0,						comment='TAI-UTC time translation type=D [s]'),
			Card(keyword='POLARX',		value=0,						comment='x coordinate of North Pole type=D [deg]'),
			Card(keyword='POLARY',		value=0,						comment='y coordinate of North Pole type="D" [deg]'),
			Card(keyword='NO_SWITC',	value=0,						comment='num. of switch phases in a switch cycle type=J'),
			Card(keyword='FRONTEND',	value=-1,						comment='Front end number type=J'),
			]
		header_apy = Header(cards)
		return(header_apy)

	def generate_header_holodata(hdr_idl_scan, hdr_dict_datapar):
		'''
		Generate HOLODATA-ALMATI header from IDL struct (uses data from ARRAYDATA-MBFITS format)

		Parameters
		----------
		hdr_idl_scan : IDL struct of header values from this SCAN
		hdr_dict_datapar : dictionary of header values from Subscan Metadata
						where original data comes from in MBFITS DATPAR header
		
		Returns
		-------
		hdr_apy : astropy.io.fits.Header

		TODO: (SS. 04/2021): Almati spec v3 (Lucas, 2001) does not have the header cards:
			 'CHANNELS'
			 'TABLEID'
			 'NO_POL'
			 'NO_FEED'
			 But, Yebes Aries21 ALMATIDataFormat.java includes these keys.  Add them later if needed
		'''
		cards = [
			Card(keyword='EXTNAME', 	value='HOLODATA-ALMATI',		comment='name of this table "HOLODATA-ALMATI"'),
			Card(keyword='TABLEREV',	value=3,						comment='Table format revision number type=J'),
			Card(keyword='SCAN-NUM',	value=hdr_dict_datapar['SCANNUM'],	comment='Scan number'),
			Card(keyword='OBS-NUM',		value=hdr_dict_datapar['SUBSNUM'],	comment='Observation number'), 
			Card(keyword='DATE-OBS',	value=hdr_dict_datapar['DATE-OBS'],	comment='Subscan start in ISO-T format [23A]'),
			Card(keyword='BASEBAND',	value=1,						comment='Baseband number type=J value=1'),
			Card(keyword='CHANNELS',	value=1,						comment='Number of channels in baseband type=J value=1'),
			Card(keyword='TABLEID',		value=1,						comment='Table ID type=J value=1'),
			Card(keyword='TRANDIST',	value=hdr_idl_scan.TRANDIST,	comment='Holography TX distance [m]'), 
			Card(keyword='TRANFREQ',	value=hdr_idl_scan.TRANFREQ,	comment='Holography TX frequency [Hz]'),
			Card(keyword='TRANFOCU',	value=hdr_idl_scan.TRANFOCU,	comment='Holography TX offset from prime focus [m]'),
			Card(keyword='NO_POL',		value=1,						comment='Number of pols type=J value=1'),
			]

		header_apy = Header(cards)
		return(header_apy)

	# -------------------------------------------------------------
	# Generate ALMATI Binary Data table arrays
	# -------------------------------------------------------------
	# Fits BinTableHDU can generate a binary data table from a Numpy structured
	# array with named fields and numpy datatypes, but I can't find a way to add 
	# units (MHz, km/s, Jy, etc) in the numpy arrays, so add the units later to the coldefs.

	# The datapar and holodate array dimensions are constant and do not depend on any other scan
	# configuration parameters. Therefore, we don' tneed a generate_xxx() function.  Create
	# the types once here and reuse in other modules.

	# TODO - check key 'ISWITCH'.  at Yebes, it is type 1J.  In document it is 4A
	
	# NOTE: (SS. 05/2021):
	# Python uses row-major order in the language syntax to describe array dimensions
	# FITS uses column-major order because it was born from  from Fortran language.
	# Therefore switch the numbers in the data structure definitions here to allow
	# FITS and numpy arrays to work together using astropy.io.fits read / write operations.
	# https://docs.astropy.org/en/stable/io/fits/appendix/faq.html#what-convention-does-astropy-use-for-indexing-such-as-of-image-coordinates
	dtype_datapar_row = np.dtype([
		('INTEGNUM',	fp.J),			# Integration point number
		('INTEGTIM',	fp.E),			# Integration time
		('MJD',			fp.D),			# Observing date/time (Modified Julian Date)
		('UUVVWW',		fp.D, (1,3)),	# u,v,w antenna coord. projected on source vector.
		('AZELERR',		fp.E, (1,2)), 	# Az,El pointing errors
		('SOURDIR',		fp.D, (1,3)), 	# Source direction cosines
		('DELAYGEO',	fp.D),			# Geometrical Delay
		('DELAYOFF',	fp.D, (1,1)),	# Delay offset
		('PHASEGEO',	fp.D),			# Geometrical Phase
		('PHASEOFF',	fp.D, (1,1)), 	# Phase Offset
		('RATEGEO',		fp.D),			# Geometrical Phase Rate
		('RATEOFF',		fp.D, (1,1)),	# Phase Rate Offset
		('FOCUSOFF',	fp.E),			# Focus offset
		('LATOFF',		fp.E),			# alt.-like offset (dLat)
		('LONGOFF',		fp.E),			# long.-like offset (dLong)*cos(lat)
		('TOTPOWER',	fp.E, (1,1)),	# Total Power in each baseband
		('WINDSPEE',	fp.E),			# Wind speed
		('WINDDIRE',	fp.E),			# Wind direction (E from N)
		('FLAG',		fp.J),			# Flag word NOTE SS. 07/2021: ALMA spec uses 3 dimensional array (N_PO x N_BD x N_A).  OAN Yebes has only a scalar value.  Follow Yebes format
		('ISWITCH',		fp.J),			# ID of phase in switch cycle # NOTE ss 07/2021. alma SPEC uses type 4A (4 ascii characters).  OAN Yebes uses 1J and hardcode to 1.  Follow OAN Yebes format
		('WSWITCH',		fp.E),			# weight of phase in switch cycle
		#('AUTO',		fp.L, 0),		# Integration present in AUTODATA-ALMATI Tables.
		#('CORR',		fp.L, 0),		# Integration present in CORRDATA-ALMATI Tables
		('HOLO',		fp.L)			# Integration present in HOLODATA-ALMATI Table
		])
		# Strictly speaking, 'AUTO' and 'CORR' should have dimension (0,1), but zero dimension
		# is illegal in FITS format.  Therefore, don't add these columns.

	# TODO check extra fields in Yebes format 'AMPREF', 'AMPTST', 'AMPREL', 'PHAREL', 'FREQMX'
	dtype_holodata_row = np.dtype([
		('INTEGNUM',	fp.J),	# Integration point number
		('HOLOSS', 		fp.E),	# Data S*S 	Yebes: holoData.testPowWatt / 8.0
		('HOLORR', 		fp.E),	# Data R*R 	Yebes: holoData.refPowWatt / 8.0
		('HOLOQQ', 		fp.E),	# Data Q*Q 	Yebes: holoData.testPowWatt / 8.0
		('HOLOSR', 		fp.E),	# Data S*R 	Yebes: (Math.sqrt(holoData.testPowWatt * holoData.refPowWatt) * Math.cos(Math.toRadians(holoData.relPhase)) / 8.0)
		('HOLOSQ', 		fp.E),	# Data S*Q 	Yebes: 0
		('HOLOQR', 		fp.E),	# Data Q*R 	Yebes: (-Math.sqrt(holoData.testPowWatt * holoData.refPowWatt) * Math.sin(Math.toRadians(holoData.relPhase)) / 8.0)
		('AMPREF', 		fp.E),	# Amplitude of the REF signal from wide beam horn antenna -Yebes: Math.sqrt(50.0 * holoData.refPowWatt)
		('AMPTST', 		fp.E),	# Amplitude of the TST signal from Ku feed and 40m reflector -Yebes: Math.sqrt(50.0 * holoData.testPowWatt)
		('AMPREL', 		fp.E),	# Relative amplitude -Yebes: holoData.relAmp
		('PHAREL', 		fp.E),	# Relative phase -Yebes: holoData.relPhase
		('FREQMX', 		fp.E)	# Frequency where we find maximum signal -Yebes: holoData.freq
		])

	# -------------------------------------------------------------
	# Generate ColDefs.  Add units if exist
	# -------------------------------------------------------------
	# Fits BinTableHDU can generate data table directly from numpy structured array,
	# but I don't see a way to add units.  Therefore, create Coldefs objects
	# from numpy structured arrays, and add units if present.  
	# Then BinTableHDU can read this ColdDefs object including the original
	# numpy array and column units.

	def generate_coldefs_datapar(data_ndarray):
		coldefs = ColDefs(data_ndarray)
		coldefs['INTEGTIM'].unit = 's'
		coldefs['MJD'].unit = 'day'
		coldefs['UUVVWW'].unit = 's'
		coldefs['AZELERR'].unit = 'deg'
		coldefs['DELAYGEO'].unit = 's'
		coldefs['DELAYOFF'].unit = 's'
		coldefs['PHASEGEO'].unit = 'rad'
		coldefs['PHASEOFF'].unit = 'rad'
		coldefs['RATEGEO'].unit = 'rad/s'
		coldefs['RATEOFF'].unit = 'rad/s'
		coldefs['FOCUSOFF'].unit = 'm'
		coldefs['LATOFF'].unit = 'deg'
		coldefs['LONGOFF'].unit = 'deg'
		coldefs['TOTPOWER'].unit = 'adu'
		coldefs['WINDSPEE'].unit = 'm/s'
		coldefs['WINDDIRE'].unit = 'deg'
		return(coldefs)

	def generate_coldefs_holodata(data_ndarray):
		coldefs = ColDefs(data_ndarray)
		coldefs['HOLOSS'].unit = 'W'
		coldefs['HOLORR'].unit = 'W'
		coldefs['HOLOQQ'].unit = 'W'
		coldefs['HOLOSR'].unit = 'W'
		coldefs['HOLOSQ'].unit = 'W'
		coldefs['HOLOQR'].unit = 'W'
		coldefs['AMPREF'].unit = 'V'
		coldefs['AMPTST'].unit = 'V'
		coldefs['AMPREL'].unit = ''
		coldefs['PHAREL'].unit = 'deg'
		coldefs['FREQMX'].unit = 'Hz'
		return(coldefs)
