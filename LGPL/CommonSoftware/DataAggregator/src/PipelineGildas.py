# Project: TNRT - Thai National Radio Telescope
# Organization: NARIT - National Astronomical Research Institute of Thailand
# Author: Spiro Sarris
# Date: 2020.04.03

import logging
import time
import datetime
import os
import numpy as np

import astropy.units as units
import astropy.constants as constants
from astropy.time import Time
from astropy.coordinates import EarthLocation

try:
    # Import for Gildas python interface
    import pyclassfiller
    from pyclassfiller import code  # The various codes needed by the CLASS Data Format

    import DataFormatGildas
except Exception:
    print("Can not import pyclassfiller")

# QT for QRunnable task in Threadpool
from PyQt5 import QtCore

from AbstractBasePipeline import AbstractBasePipeline
from DataFormatMBfits import DataFormatMBfits
import DataAggregatorMod
import DataAggregatorErrorImpl
import DataAggregatorError


class PipelineGildas(AbstractBasePipeline):
    """
    Docstring
    """

    def __init__(self, metadata_ref):
        AbstractBasePipeline.__init__(self, metadata_ref)

        self.inputs = {}

        self.outputs = {
            "pipeline": {
                "channel": DataAggregatorMod.CHANNEL_NAME_PIPELINE_GILDAS,
                "supplier": None,
            }
        }

        # Create an empty lists to accumulate data as it is recorded
        # Use Python list instead of numpy array becuase list can append faster.
        # Each item in the list is a numpy array with specified dtype.
        # At end of subscan, convert Python list of numpy 1-D arrays into a 2-D numpy structured array
        # for faster / convenient processing after all append operations are finished.
        self.arraydata_buffer = []
        self.datapar_buffer = []

    def open(self, mbfits_headers):
        AbstractBasePipeline.open(self, mbfits_headers)
        # Generate filename and open a new file
        self.filename_base = datetime.datetime.utcnow().strftime("%Y%m%d_%H%M%S")
        self.filename = self.data_dir + "/" + self.filename_base + ".40m"
        self.logger.info("Generate filename= " + self.filename)

        # Allow appending file if file exists
        allow_file_exists = True

        # If overwrite_mode == True, overwrite file.  same as file open mode 'w'.
        # If overwrite_mode == False, append to file.  same as file open mode 'a'
        overwrite_mode = False

        # Maximum number of scans in the file (obsolete)
        max_scans = 999999

        # enforce each scan to be unique in the file
        enforce_unique_scans = True

        self.fileout = pyclassfiller.ClassFileOut()
        self.fileout.open(
            file=self.filename,
            new=allow_file_exists,
            over=overwrite_mode,
            size=max_scans,
            single=enforce_unique_scans,
        )

        # Create a python CLASS observation data structure to populate.
        # pyclassfiller knows how to write this structure to the actual file.
        self.obs = pyclassfiller.ClassObservation()

        # Observation number [int32][no unit]
        self.obs.head.gen.num = DataFormatGildas.defaults.head.gen.num

        # Version number [int32][no unit]
        self.obs.head.gen.ver = DataFormatGildas.defaults.head.gen.ver
        # Enable sections of file
        self.obs.head.presec[:] = False  # Disable all sections except...
        self.obs.head.presec[code.sec.gen] = True  # General
        self.obs.head.presec[code.sec.pos] = True  # Position
        self.obs.head.presec[code.sec.spe] = True  # Spectroscopy
        self.obs.head.presec[code.sec.dri] = True  # Continuum drifts
        self.obs.head.presec[code.sec.cal] = True  # Calibration

    def close(self):

        if self.fileout is not None:
            # Blocking wait for event write_subscan_finished to be set true by another thread before close the file.
            self.logger.info(
                "Wait for write_subscan_finished event before closing file ..."
            )
            self.write_subscan_finished.wait(timeout=60.0)
            if self.write_subscan_finished.is_set() == False:
                ex = DataAggregatorErrorImpl.timeoutOnCloseErrorExImpl()
                ex.addData(
                    "errExpl",
                    "Timeout while waiting for file to close.  Astropy IERS download?",
                )
                raise ex
            else:
                self.fileout.close()
                del self.obs, self.fileout
                return AbstractBasePipeline.close(self)

    def handler_spectrum_arraydata(self, header, arraydata_row):
        # self.logger.logDebug('handle spectrum')

        # Add this row of arraydata to the buffer for this subscan
        self.arraydata_buffer.append(arraydata_row)

        datapar_row = np.array(0, dtype=DataFormatMBfits.dtype_datapar_row)

        # Set same timestamp in datapar.
        datapar_row["MJD"] = arraydata_row["MJD"]
        # Set integration time for this integration. (SS. Why change during subscan?)
        # TODO read / calculate INTEGTIM from fw_client packet.  Temporary hard code
        # to test dataformat

        # TODO interpolate all values that are stored in datapar rows.  For now, test
        # interpolation for LONGOFF to confirm that the technique works.
        datapar_row["INTEGTIM"] = 1.0
        # TODO interpolate LONGOFF after we confirm that set last value makes sense.
        datapar_row["LONGOFF"] = self.metadata["antenna"].tracking_sts.prog_offset_az

        # Add this row of datapar to the buffer for this subscan
        self.datapar_buffer.append(datapar_row)

        # Evertying else in DATAPAR can be processed at the end of the subscan.
        # For example, use timestamps to interpolate MONITOR data
        # for AZ, EL, etc. into the DATAPAR array which will be used to write files
        # and post-process.

        # Write one integrated spectrum (1 packet from backend) to the data file
        if self.opened is True:
            # Increment counter of packets received
            self.packets_received += 1
            if self.write_enabled is True:
                # self.logger.debug('QThreadpool.start WorkerWriteSpectrum, MJD = %f' % arraydata_row['MJD'])
                # Create a runnable and delegate the work to the Threadpool.
                worker = WorkerWriteSpectrum(
                    self.fileout, self.obs, arraydata_row, datapar_row
                )
                self.threadpool.start(worker)
                self.packets_written += 1

    def write_scan_header(self, mbfits_headers):
        """
        Writes scan header for current scan.  In the case of Gildas, it doesn't actually write
        data to the file.  It prepares the pyclassfiller data struct with information.
        When we write the actual data from integrations / drifts this data will go to to the file
        with current values.

        Parameters
        ----------
        mbfits_headers : ScanMod.MetadataNotifyBlock
                struct MetadataNotifyBlock{
                        SCAN_MBFITS_HEADER_TYPE SCAN_MBFITS_HEADER;
                        FEBEPAR_MBFITS_HEADER_TYPE FEBEPAR_MBFITS_HEADER;
                        ARRAYDATA_MBFITS_HEADER_TYPE ARRAYDATA_MBFITS_HEADER;
                        DATAPAR_MBFITS_HEADER_TYPE DATAPAR_MBFITS_HEADER;
                        };
        """
        # Define the required numpy datatypes for this SCAN whose array dimensions depend on
        # on current values of header at runtime.  Other dtypes for MBfits have constant dimensions, and don't need
        # to generate at runtime.
        self.logger.logDebug(
            "Generate dtype_febepar_row and dtype_arraydata_row dims from SCAN metadata"
        )
        self.dtype_arraydata_row = DataFormatMBfits.generate_dtype_arraydata_row(
            mbfits_headers.ARRAYDATA_MBFITS_HEADER['NUSEFEED'],
            mbfits_headers.ARRAYDATA_MBFITS_HEADER['CHANNELS'],
        )

        self.logger.debug(
            "QThreadpool.start WorkerWriteScanHeader, Scan = %d"
            % (mbfits_headers.SCAN_MBFITS_HEADER.SCANNUM)
        )
        # Create a runnable and delegate the work to the Threadpool.
        worker = WorkerWriteScanHeader(self.obs, mbfits_headers)
        self.threadpool.start(worker)

    def write_before_subscan(self, mbfits_headers):
        """
        Writes subscan header for current scan.  In the case of Gildas, it doesn't actually write
        data to the file.  It prepares the pyclassfiller data struct with all of the data that is
        available before the scan runs.
        When the actual data from integrations / drifts is available, a few more header items
        are calculated at runtime (because the values are unknown before),
        and added to the header before the data buffer is written to file.

        Parameters
        ----------
        mbfits_headers : ScanMod.MetadataNotifyBlock
                struct MetadataNotifyBlock{
                        SCAN_MBFITS_HEADER_TYPE SCAN_MBFITS_HEADER;
                        SCAN_MBFITS_COLUMNS_TYPE SCAN_MBFITS_COLUMNS;
                        FEBEPAR_MBFITS_HEADER_TYPE FEBEPAR_MBFITS_HEADER;
                        ARRAYDATA_MBFITS_HEADER_TYPE ARRAYDATA_MBFITS_HEADER;
                        DATAPAR_MBFITS_HEADER_TYPE DATAPAR_MBFITS_HEADER;
                        };
        """

        # ------------------
        # General Parameters
        # ------------------
        # Type of data [code]
        # If head.gen.kind = spec (continuum), header section obs.head.spe must be filled
        # If head.gen.kind = cont (continuum), header section obs.head.dri must be filled
        self.logger.debug(
            "QThreadpool.start WorkerWriteSubscanHeader, Scan = %d, Subscan = %d"
            % (
                mbfits_headers.ARRAYDATA_MBFITS_HEADER['SCANNUM'],
                mbfits_headers.ARRAYDATA_MBFITS_HEADER['SUBSNUM'],
            )
        )
        # Create a runnable and delegate the work to the Threadpool.
        worker = WorkerWriteBeforeSubscan(self.obs, mbfits_headers)
        self.threadpool.start(worker)

    def write_after_subscan(self, mbfits_headers, subscan_number):
        """
        End of subscan.  Gildas class already writes each integration in real-time during subscan, so no need
        to write that now.  Find the peak of ARRAYDATA and write a drift at that channel for the first FEBE.
        Gildas CLASS doesn't use all of the other detailed arrays that MBFITS writes at the end of subscan.

        Calculates a few Gildas header parameters at runtime that depend on temporary buffered data.
        (unknown before runtime)
        Then writes a drift to the file.  A drift is a sequence of power measurements from 1 spectral
        channel.  The timestamps of each power measurement are used to lookup interpolated AZ/EL
        position of where the antenna was pointing at the timestamp of data recording.  This interpolated
        angle data is used to write the drift data. Each power measurement point in the drift is
        associated with an angle offset from the tracking source. This type of drift is used for
        pointing scans.  In the case of a continuum detector or power meter, the device has only 1 channel.
        1 drift is stored as 1 "observation" in the Gildas CLASS file index.

        Parameters
        ---------
        mbfits_headers : ScanMod.MetadataNotifyBlock
                Complete set of MBFITS-style header information.  Including the DATAPAR header
                that was created at runtime during the subscan for each integration.
        """

        # Check if any of the buffers are empty.  If empty, create 1 fake row of zeros
        # so that length of array == 1. If length == 0, error in creating FITS columns because 0
        # is not a valid dimension.  In a real operation system, all buffers should have real data
        # For test and debug, sometimes we do not have data in some buffer.  Also, if we
        # only want to record data about Antenna position -- but not use receivers, we need
        # to create a placeholder for arrays that otherwise have a 0 dimension.
        if len(self.arraydata_buffer) == 0:
            self.logger.logDebug(
                "arraydata_buffer len==0. append 1 dummy array of zeros to avoid array dimension errors in FITS file"
            )
            self.arraydata_buffer.append(np.array(0, dtype=self.dtype_arraydata_row))

        if len(self.datapar_buffer) == 0:
            self.logger.logDebug(
                "datapar_buffer len==0. append 1 dummy array of zeros to avoid array dimension errors in FITS file"
            )
            self.datapar_buffer.append(
                np.array(0, dtype=DataFormatMBfits.dtype_datapar_row)
            )

        # self.<xxx>_buffer is a Python list of self.dtype_<xxx>_row.  Each dtype_<xxx>_row
        # is a numpy structured array with timestamp MJD and an array of data that has dimensions
        # depend on spectrum channels, sections, etc (defined at runtime).  List of rows was used
        # because it is faster to append list than concatenate numpy array.   Now append is finished
        # and convert the entire list into 1 numpy array for convenient / efficient processing.
        arraydata = np.asarray(self.arraydata_buffer)

        # self.datapar_buffer is a Python list of self.dtype_datapar_row.  Each dtype_datapar_row
        # is a fixed size and includes metadata associated with each row of ARRAYDATA (Az, El angles, etc)
        datapar = np.asarray(self.datapar_buffer)

        if self.opened is True:
            if self.write_enabled is True:
                # Clear the flow control flag
                self.write_subscan_finished.clear()
                self.logger.debug("QThreadpool.start WorkerWriteAfterSubscan")
                # Create a runnable and delegate the work to the Threadpool.
                worker = WorkerWriteAfterSubscan(
                    self.write_subscan_finished,
                    self.fileout,
                    self.obs,
                    mbfits_headers,
                    arraydata,
                    datapar,
                )
                self.threadpool.start(worker)
                self.packets_written += 1

    def write_monitor(self, mbfits_headers):
        pass

    def clear_subscan_buffers(self):
        """subscan finished, buffers written, clear buffers to prepare for next subscan"""
        list_to_delete = [
            self.arraydata_buffer,
            self.datapar_buffer,
        ]
        # Delete
        for item in list_to_delete:
            try:
                del item
            except NameError:
                self.logger.error("buffer | array not defined.  Nothing to delete")

        # Initialize buffers to empty list
        self.arraydata_buffer = []
        self.datapar_buffer = []

    def clear_monitor_buffers(self):
        pass


class WorkerWriteScanHeader(QtCore.QRunnable):
    """Worker thread to write scan header using resources from Threadpool"""

    def __init__(self, obs, mbfits_headers):
        super().__init__()
        self.logger = logging.getLogger(
            "%s.%s" % (self.__module__, self.__class__.__name__)
        )
        self.mutex = QtCore.QMutex()
        self.obs = obs
        self.mbfits_headers = mbfits_headers

    def run(self):
        self.logger.logTrace("mutex lock")
        self.mutex.lock()

        self.site_location = EarthLocation(
            EarthLocation.from_geodetic(
                lon=self.mbfits_headers.SCAN_MBFITS_HEADER.SITELONG * units.deg,
                lat=self.mbfits_headers.SCAN_MBFITS_HEADER.SITELAT * units.deg,
                height=self.mbfits_headers.SCAN_MBFITS_HEADER.SITEELEV * units.m,
                ellipsoid="WGS84",
            )
        )

        # ------------------
        # General Parameters
        # ------------------
        # Telescope name. char[12]
        # Telescope name + Frontend-Backend configuration of telescope.
        # compare to MBFITS FEBE
        # For Gildas file, assume only one FEBE in the list. Use FEBE[0]
        try:
            self.obs.head.gen.teles = "%s-?" % (
                self.mbfits_headers.SCAN_MBFITS_HEADER.TELESCOP
            )
        except:
            self.logger.logError(
                "WARNING: Using default value of head.gen.teles = %s"
                % DataFormatGildas.defaults.head.gen.teles
            )
            self.obs.head.gen.teles = DataFormatGildas.defaults.head.gen.teles

        # Integer date of observation [int32], [MJD - 60549].
        try:
            self.obs.head.gen.dobs = (
                int(self.mbfits_headers.SCAN_MBFITS_HEADER.MJD)
                - DataFormatGildas.GAG_MJD_OFFSET
            )
        except:
            self.logger.logError(
                "WARNING: Using default value of head.gen.dobs = %d"
                % DataFormatGildas.defaults.head.gen.dobs
            )
            self.obs.head.gen.dobs = DataFormatGildas.defaults.head.gen.dobs

        # Integer date of reduction [int32] [MJD - 60549].
        try:
            self.obs.head.gen.dred = (
                int(self.mbfits_headers.SCAN_MBFITS_HEADER.MJD)
                - DataFormatGildas.GAG_MJD_OFFSET
            )
        except:
            self.logger.logError(
                "WARNING: Using default value of head.gen.dred = %d"
                % DataFormatGildas.defaults.head.gen.dred
            )
            self.obs.head.gen.dred = DataFormatGildas.defaults.head.gen.dred

        # Quality of data [code]  Ignore
        self.obs.head.gen.qual = code.qual.unknown

        # Scan number [int32][no unit]
        try:
            self.obs.head.gen.scan = int(self.mbfits_headers.SCAN_MBFITS_HEADER.SCANNUM)
        except:
            self.logger.logError(
                "WARNING: Using default value of head.gen.scan = %d"
                % DataFormatGildas.defaults.head.gen.scan
            )
            self.obs.head.gen.scan = DataFormatGildas.defaults.head.gen.scan

        # --------------------
        # Position Information
        # --------------------
        # Source name char[12]
        try:
            self.obs.head.pos.sourc = self.mbfits_headers.SCAN_MBFITS_HEADER.X_OBJECT
        except:
            self.logger.logError(
                "WARNING: Using default value of head.pos.sourc = %s"
                % DataFormatGildas.defaults.head.pos.sourc
            )
            self.obs.head.pos.sourc = DataFormatGildas.defaults.head.pos.sourc

        # Coordinate System [code] {.unk, .equ, .gal, .hor}
        try:
            self.obs.head.pos.system = DataFormatGildas.map_code_coord[
                self.mbfits_headers.SCAN_MBFITS_HEADER.BLNGTYPE.upper()
            ]
        except:
            self.logger.logError(
                "WARNING: Using default value of head.pos.system = %d"
                % DataFormatGildas.defaults.head.pos.system
            )
            self.obs.head.pos.system = DataFormatGildas.defaults.head.pos.system

        # Equinox of coordinates [float32][unit = year]
        try:
            self.obs.head.pos.equinox = self.mbfits_headers.SCAN_MBFITS_HEADER.EQUINOX
        except:
            self.logger.logError(
                "WARNING: Using default value of head.pos.equinox = %f"
                % DataFormatGildas.defaults.head.pos.equinox
            )
            self.obs.head.pos.equinox = DataFormatGildas.defaults.head.pos.equinox

        # Projection system [code]
        self.obs.head.pos.proj = code.proj.none

        # Lambda : Longitude of source [float64][unit = radian]
        try:
            self.obs.head.pos.lam = (
                self.mbfits_headers.SCAN_MBFITS_HEADER.BLONGOBJ
                * DataFormatGildas.RADIANS_PER_DEGREE
            )
        except:
            self.logger.logError(
                "WARNING: Using default value of head.pos.lam = %f"
                % DataFormatGildas.defaults.head.pos.lam
            )
            self.obs.head.pos.lam = DataFormatGildas.defaults.head.pos.lam

        # Beta : Latitude of source [float64][unit = radian]
        try:
            self.obs.head.pos.bet = (
                self.mbfits_headers.SCAN_MBFITS_HEADER.BLATOBJ
                * DataFormatGildas.RADIANS_PER_DEGREE
            )
        except:
            self.logger.logError(
                "WARNING: Using default value of head.pos.bet = %f"
                % DataFormatGildas.defaults.head.pos.bet
            )
            self.obs.head.pos.bet = DataFormatGildas.defaults.head.pos.bet

        # Projection angle [float64][unit = radian]
        self.obs.head.pos.projang = DataFormatGildas.defaults.head.pos.projang

        # Offset in longitude [float32][unit = radian]
        try:
            self.obs.head.pos.lamof = (
                self.mbfits_headers.SCAN_MBFITS_HEADER.PATLONG
                * DataFormatGildas.RADIANS_PER_DEGREE
            )
        except:
            self.logger.logError(
                "WARNING: Using default value of head.pos.lamof = %f"
                % DataFormatGildas.defaults.head.pos.lamof
            )
            self.obs.head.pos.lamof = DataFormatGildas.defaults.head.pos.lamof

        # Offset in latitude [float32][unit = radian]
        try:
            self.obs.head.pos.betof = (
                self.mbfits_headers.SCAN_MBFITS_HEADER.PATLAT
                * DataFormatGildas.RADIANS_PER_DEGREE
            )
        except:
            self.logger.logError(
                "WARNING: Using default value of head.pos.betof = %f"
                % DataFormatGildas.defaults.head.pos.betof
            )
            self.obs.head.pos.betof = DataFormatGildas.defaults.head.pos.betof

        self.logger.logTrace("mutex unlock")
        self.mutex.unlock()


class WorkerWriteBeforeSubscan(QtCore.QRunnable):
    """Worker thread to write subscan header using resources from Threadpool"""

    def __init__(self, obs, mbfits_headers):
        super().__init__()
        self.logger = logging.getLogger(
            "%s.%s" % (self.__module__, self.__class__.__name__)
        )
        self.mutex = QtCore.QMutex()
        self.obs = obs
        self.mbfits_headers = mbfits_headers

    def run(self):
        self.logger.logTrace("mutex lock")
        self.mutex.lock()

        # Subscan number [int32][no unit]
        try:
            self.obs.head.gen.subscan = int(
                self.mbfits_headers.ARRAYDATA_MBFITS_HEADER['SUBSNUM']
            )
        except:
            self.logger.logError(
                "WARNING: Using default value of head.gen.subscan = %d"
                % DataFormatGildas.defaults.head.gen.subscan
            )
            self.obs.head.gen.subscan = DataFormatGildas.defaults.head.gen.subscan

        # Opacity [float32][unit = neper]
        # TODO - get this from MBFITS monitor point TAU_FEBE
        try:
            self.obs.head.gen.tau = DataFormatGildas.defaults.head.gen.tau
        except:
            self.logger.logError(
                "WARNING: Using default value of head.gen.tau = %f"
                % DataFormatGildas.defaults.head.gen.tau
            )
            self.obs.head.gen.tau = DataFormatGildas.defaults.head.gen.tau

        # System temperature [float32][unit = Kelvin]
        # TODO - get this from MBFITS monitor point TSYS_FEBE
        try:
            self.obs.head.gen.tsys = DataFormatGildas.defaults.head.gen.tsys
        except:
            self.logger.logError(
                "WARNING: Using default value of head.gen.tsys = %f"
                % DataFormatGildas.defaults.head.gen.tsys
            )
            self.obs.head.gen.tsys = DataFormatGildas.defaults.head.gen.tsys

        # X Unit. [code]
        # (If X coodinates section is present) {.velo, .freq, .wave}
        self.obs.head.gen.xunit = code.xunit.freq

        # -------------------------
        # Spectroscopic Information
        # Required if If head.gen.kind = spec.
        # If head.gen.kind = cont (continuum), some parameters are ignored. But some data is used
        # for labels on plots.  Fill the sections anyway.
        # -------------------------
        # Spectral line name char[12]
        try:
            self.obs.head.spe.line = (
                self.mbfits_headers.ARRAYDATA_MBFITS_HEADER['MOLECULE']
            )
        except:
            self.logger.logError(
                "WARNING: Using default value of head.spe.line = %s"
                % DataFormatGildas.defaults.head.spe.line
            )
            self.obs.head.spe.linee = DataFormatGildas.defaults.head.spe.line

        # Rest frequency [float64][unit = MHz].  MBFITS has unit of Hz.
        try:
            self.obs.head.spe.restf = (
                self.mbfits_headers.ARRAYDATA_MBFITS_HEADER['RESTFREQ'] / 1e6
            )
        except:
            self.logger.logError(
                "WARNING: Using default value of head.spe.restf = %f"
                % DataFormatGildas.defaults.head.spe.restf
            )
            self.obs.head.spe.restf = DataFormatGildas.defaults.head.spe.restf

        # Number of channels [int32][no unit]
        # In this example, hardcode 1 because it is a continuum detector.  In the real
        # data pipeline, this will come from an array dimension
        try:
            self.obs.head.spe.nchan = int(
                self.mbfits_headers.ARRAYDATA_MBFITS_HEADER['CHANNELS']
            )
        except:
            self.logger.logError(
                "WARNING: Using default value of head.spe.nchan = %d"
                % DataFormatGildas.defaults.head.spe.nchan
            )
            self.obs.head.spe.nchan = DataFormatGildas.defaults.head.spe.nchan

        # Reference channels [float32][no unit]
        try:
            if self.obs.head.spe.nchan % 2 == 0:
                self.obs.head.spe.rchan = self.obs.head.spe.nchan / 2 + 0.5
            else:
                self.obs.head.spe.rchan = self.obs.head.spe.nchan / 2
                self.logger.info(
                    "Selected reference channel rchan = %f" % self.obs.head.spe.rchan
                )
        except:
            self.logger.logError(
                "WARNING: Using default value of head.dri.rchan = %f"
                % DataFormatGildas.defaults.head.spe.rchan
            )
            self.obs.head.spe.rchan = DataFormatGildas.defaults.head.spe.rchan

        # Frequency resolution [float32][unit = MHz]
        # TODO
        try:
            self.obs.head.spe.fres = (
                self.mbfits_headers.ARRAYDATA_MBFITS_HEADER['X_1CDLT2S'] / 1e6
            )
        except:
            self.logger.logError(
                "WARNING: Using default value of head.spe.fres = %f"
                % DataFormatGildas.defaults.head.spe.fres
            )
            self.obs.head.spe.fres = DataFormatGildas.defaults.head.spe.fres

        # Velocity resolution [float32][unit = km/s]
        try:
            self.obs.head.spe.vres = (
                -1
                * constants.c.value
                * self.obs.head.spe.fres
                / self.obs.head.spe.restf
                / 1e3
            )
        except:
            self.logger.logError(
                "WARNING: Using default value of head.spe.vres = %f"
                % DataFormatGildas.defaults.head.spe.vres
            )
            self.obs.head.spe.vres = DataFormatGildas.defaults.head.spe.vres

        # Velocity at reference channel [float32][unit = km/s]
        # TODO
        try:
            self.obs.head.spe.voff = DataFormatGildas.defaults.head.spe.voff
        except:
            self.logger.logError(
                "WARNING: Using default value of head.spe.voff = %f"
                % DataFormatGildas.defaults.head.spe.voff
            )
            self.obs.head.spe.voff = DataFormatGildas.defaults.head.spe.voff

        # Blanking value [float32][unit?]
        try:
            self.obs.head.spe.bad = DataFormatGildas.defaults.head.spe.bad
        except:
            self.logger.logError(
                "WARNING: Using default value of head.spe.bad = %f"
                % DataFormatGildas.defaults.head.spe.bad
            )
            self.obs.head.spe.bad = DataFormatGildas.defaults.head.spe.bad

        # Image frequency [float64][unit = MHz]
        # TODO
        try:
            self.obs.head.spe.image = DataFormatGildas.defaults.head.spe.image
        except:
            self.logger.logError(
                "WARNING: Using default value of head.spe.image = %f"
                % DataFormatGildas.defaults.head.spe.image
            )
            self.obs.head.spe.image = DataFormatGildas.defaults.head.spe.image

        # Type of velocity [code] {unk, lsr, helio, obs, earth, auto}
        # TODO
        try:
            self.obs.head.spe.vtype = DataFormatGildas.defaults.head.spe.vtype
        except:
            self.logger.logError(
                "WARNING: Using default value of head.spe.vtype = %d"
                % DataFormatGildas.defaults.head.spe.vtype
            )
            self.obs.head.spe.vtype = DataFormatGildas.defaults.head.spe.vtype

        # Velocity convention [code] {unk, rad, opt, thirtym}
        # Use rad = radio (not optical or 30m IRAM convention .. whatever that means).
        self.obs.head.spe.vconv = DataFormatGildas.defaults.head.spe.vconv

        # Doppler correction -V/c (CLASS convention) [float64][unit = km/s]
        # TODO
        try:
            self.obs.head.spe.doppler = DataFormatGildas.defaults.head.spe.doppler
        except:
            self.logger.logError(
                "WARNING: Using default value of head.spe.doppler = %f"
                % DataFormatGildas.defaults.head.spe.doppler
            )
            self.obs.head.spe.doppler = DataFormatGildas.defaults.head.spe.doppler

        # -------------------------
        # Continuum drift description
        # (only used if If head.gen.kind = cont)
        # -------------------------
        # Rest frequency [float64][unit = MHz]
        try:
            self.obs.head.dri.freq = (
                self.mbfits_headers.ARRAYDATA_MBFITS_HEADER['RESTFREQ'] / 1e6
            )
        except:
            self.logger.logError(
                "WARNING: Using default value of head.dri.freq = %f"
                % DataFormatGildas.defaults.head.dri.freq
            )
            self.obs.head.dri.freq = DataFormatGildas.defaults.head.dri.freq

        # Bandwidth [float32][unit = MHz]
        # TODO
        try:
            self.obs.head.dri.width = DataFormatGildas.defaults.head.dri.width
        except:
            self.logger.logError(
                "WARNING: Using default value of head.dri.width = %f"
                % DataFormatGildas.defaults.head.dri.width
            )
            self.obs.head.dri.width = DataFormatGildas.defaults.head.dri.width

        # Time at reference [float32][?]
        self.obs.head.dri.tref = DataFormatGildas.defaults.head.dri.tref

        # Angular offset at reference [float32][unit = radian]
        # We are using this continuum drift to calculate offsets, so don't add more here
        self.obs.head.dri.aref = DataFormatGildas.defaults.head.dri.aref

        # Position angle of drift [float32][unit = radian]
        try:
            self.obs.head.dri.apos = DataFormatGildas.map_apos[
                self.mbfits_headers.DATAPAR_MBFITS_HEADER['SCANDIR'].upper()
            ]
        except:
            self.logger.logError(
                "WARNING: Using default value of head.dri.apos = %f"
                % DataFormatGildas.defaults.head.dri.apos
            )
            self.obs.head.dri.apos = DataFormatGildas.defaults.head.dri.apos

        # Time resolution [float32][unit = seconds]
        try:
            self.obs.head.dri.tres = self.obs.head.gen.time
        except:
            self.logger.logError(
                "WARNING: Using default value of head.dri.tres = %f"
                % DataFormatGildas.defaults.head.dri.tres
            )
            self.obs.head.dri.tres = DataFormatGildas.defaults.head.dri.tres

        # Blanking value [float32]
        self.obs.head.dri.bad = DataFormatGildas.defaults.head.dri.bad

        # Type of offsets [code]
        try:
            self.obs.head.dri.ctype = DataFormatGildas.map_code_coord[
                self.mbfits_headers.DATAPAR_MBFITS_HEADER['NLNGTYPE'].upper()
            ]
        except:
            self.logger.logError(
                "WARNING: Using default value of head.dri.ctype = %f"
                % DataFormatGildas.defaults.head.dri.ctype
            )
            self.obs.head.dri.ctype = DataFormatGildas.defaults.head.dri.ctype

        # Image frequency [float64][unit = MHz]
        self.obs.head.dri.cimag = DataFormatGildas.defaults.head.dri.cimag

        # Collimation error Az [float32]
        # TODO
        try:
            self.obs.head.dri.colla = self.mbfits_headers.SCAN_MBFITS_HEADER.PDELTACA
        except:
            self.logger.logError(
                "WARNING: Using default value of head.dri.colla = %f"
                % DataFormatGildas.defaults.head.dri.colla
            )
            self.obs.head.dri.colla = DataFormatGildas.defaults.head.dri.colla

        # Collimation error El [float32]
        # TODO
        try:
            self.obs.head.dri.colle = self.mbfits_headers.SCAN_MBFITS_HEADER.PDELTAIE
        except:
            self.logger.logError(
                "WARNING: Using default value of head.dri.colle = %f"
                % DataFormatGildas.defaults.head.dri.colle
            )
            self.obs.head.dri.colle = DataFormatGildas.defaults.head.dri.colle

        self.logger.logTrace("mutex unlock")
        self.mutex.unlock()


class WorkerWriteSpectrum(QtCore.QRunnable):
    """Worker thread to write drift using resources from Threadpool"""

    def __init__(self, fileout, obs, arraydata_row, datapar_row):
        super().__init__()
        self.logger = logging.getLogger(
            "%s.%s" % (self.__module__, self.__class__.__name__)
        )
        self.mutex = QtCore.QMutex()
        self.fileout = fileout
        self.obs = obs
        self.arraydata_row = arraydata_row
        self.datapar_row = datapar_row

    def run(self):
        self.logger.logTrace("mutex lock")
        self.mutex.lock()

        # Set type spectrum
        self.obs.head.gen.kind = code.kind.spec

        # Update longitude and latitude offset for this integration.

        # Offset in longitude [float32][unit = radian]
        try:
            self.obs.head.pos.lamof = (
                self.datapar_row["LONGOFF"] * DataFormatGildas.RADIANS_PER_DEGREE
            )
        except:
            self.logger.logError(
                "WARNING: Using default value of head.pos.lamof = %f"
                % DataFormatGildas.defaults.head.pos.lamof
            )
            self.obs.head.pos.lamof = DataFormatGildas.defaults.head.pos.lamof

        # Offset in latitude [float32][unit = radian]
        try:
            self.obs.head.pos.betof = (
                self.datapar_row["LATOFF"] * DataFormatGildas.RADIANS_PER_DEGREE
            )
        except:
            self.logger.logError(
                "WARNING: Using default value of head.pos.betof = %f"
                % DataFormatGildas.defaults.head.pos.betof
            )
            self.obs.head.pos.betof = DataFormatGildas.defaults.head.pos.betof

        # Write the data
        self.obs.datay = np.array(self.arraydata_row["DATA"][0], dtype=np.float32)
        self.obs.write()

        self.logger.logTrace("mutex unlock")
        self.mutex.unlock()


class WorkerWriteAfterSubscan(QtCore.QRunnable):
    """Worker thread to write drift using resources from Threadpool"""

    def __init__(
        self, write_subscan_finished, fileout, obs, mbfits_headers, arraydata, datapar
    ):

        super().__init__()
        self.logger = logging.getLogger(
            "%s.%s" % (self.__module__, self.__class__.__name__)
        )
        self.mutex = QtCore.QMutex()
        self.write_subscan_finished = write_subscan_finished
        self.fileout = fileout
        self.obs = obs
        self.mbfits_headers = mbfits_headers
        self.arraydata = arraydata
        self.datapar = datapar
        # No need to keep the monitor data.  Gildas should only need interpolated
        # metadata from DATAPAR

    def run(self):
        self.logger.logTrace("mutex lock")
        self.mutex.lock()

        self.obs.head.gen.kind = code.kind.cont

        # Get header data that is now available at the end of a subscan.
        # Add to the Gildas ClassObservation object

        self.site_location = EarthLocation(
            EarthLocation.from_geodetic(
                lon=self.mbfits_headers.SCAN_MBFITS_HEADER.SITELONG * units.deg,
                lat=self.mbfits_headers.SCAN_MBFITS_HEADER.SITELAT * units.deg,
                height=self.mbfits_headers.SCAN_MBFITS_HEADER.SITEELEV * units.m,
                ellipsoid="WGS84",
            )
        )

        self.logger.logDebug(
            "DATAPAR_MBFITS_HEADER['DATE_OBS']: %s"
            % self.mbfits_headers.DATAPAR_MBFITS_HEADER['DATE_OBS']
        )
        time_apy = Time(
            self.mbfits_headers.DATAPAR_MBFITS_HEADER['DATE_OBS'],
            location=self.site_location,
            format="isot",
        )

        # UT of observation [float64][unit = radian]
        # Timestamp comes from the first integration returned from the receiver during this subscan
        try:
            self.obs.head.gen.ut = (
                2 * np.pi * ((time_apy.ut1.mjd) % 1)
            )  # fractional days  * 2pi
            self.logger.info(
                "Calculate subscan start time UT [radians] = %f" % self.obs.head.gen.ut
            )
        except:
            self.logger.logError(
                "WARNING: Using default value of head.gen.ut = %f"
                % DataFormatGildas.defaults.head.gen.ut
            )
            self.obs.head.gen.ut = DataFormatGildas.defaults.head.gen.ut

        # LST of observation [float64][unit = radian]
        # Timestamp comes from the first integration returned from the receiver during this subscan
        try:
            self.obs.head.gen.st = time_apy.sidereal_time("apparent").radian
            self.logger.info(
                "Calculate subscan start time LST [radians] = %f" % self.obs.head.gen.st
            )
        except:
            self.logger.logError(
                "WARNING: Using default value of head.gen.st = %f"
                % DataFormatGildas.defaults.head.gen.st
            )
            self.obs.head.gen.st = DataFormatGildas.defaults.head.gen.st

        # Integration Time [float32][unit = second]
        # Use the integration time from the first row of the DATAPAR table.  Perhaps it could
        # change during the subscan, but we have to assume that the integration time does not
        # change so we can choose 1 number to prepare the Gildas header time resolution.
        try:
            self.obs.head.gen.time = self.datapar["INTEGTIM"][0]
            self.logger.info(
                "Found integration time time = %f" % self.obs.head.gen.time
            )
        except:
            self.logger.logError(
                "WARNING: Using default value of head.gen.time = %f"
                % DataFormatGildas.defaults.head.gen.time
            )
            self.obs.head.gen.time = DataFormatGildas.defaults.head.gen.time

        # Number of data points (angles) [int32][no unit]
        try:
            self.obs.head.dri.npoin = self.arraydata.shape[0]
            self.logger.info(
                "Found number of points npoin = %d" % self.obs.head.dri.npoin
            )
        except:
            self.logger.logError(
                "WARNING: Using default value of head.dri.npoin = %d"
                % DataFormatGildas.defaults.head.dri.npoin
            )
            self.obs.head.dri.npoin = DataFormatGildas.defaults.head.dri.npoin

        # Reference point [float32][no unit]
        # If number of points in drift is an odd number, use the center point.
        # else, create a reference point between the 2 center points.
        # This method assumes that the scan is designed to see the radio source in the center
        # after accounting for pre-programmed longitude (AZ, RA) and latitude (EL, DEC) offsets.

        # TODO improve this function to look at the offset angles in DATAPAR instead of than assume
        # that reference angle is exactly in the center of the subscan (typical)
        try:
            if self.obs.head.dri.npoin % 2 == 0:
                self.obs.head.dri.rpoin = self.obs.head.dri.npoin / 2 + 0.5
            else:
                self.obs.head.dri.rpoin = self.obs.head.dri.npoin / 2
            self.logger.info(
                "Selected reference point rpoin = %f" % self.obs.head.dri.rpoin
            )
        except:
            self.logger.logError(
                "WARNING: Using default value of head.dri.rpoin = %f"
                % DataFormatGildas.defaults.head.dri.rpoin
            )
            self.obs.head.dri.rpoin = DataFormatGildas.defaults.head.dri.rpoin

        # Angular resolution [float32][unit = radian]
        # convert arcseconds to radians

        # Choose list of angles to use to calculate the angular resolution. (2 choices)
        # Use the MBFITS keywords from DATAPAR_HEADER.SCANDIR to determine if it is a longitude drift (x)
        # or latitude drift (y).  lookup from the dictionary is the MBFITS keyword to get the list of angles
        # from DATAPAR.<keyword>
        try:
            # Select the list of angles from DATAPAR (2 choices)
            scandir = self.mbfits_headers.DATAPAR_MBFITS_HEADER['SCANDIR'].upper()
            self.logger.debug(
                "Select angle list. scan direction from DATAPAR_MBFITS_HEADER: %s"
                % scandir
            )

            angle_key = DataFormatGildas.map_angle_key[scandir]
            self.logger.debug("Selected angle key: %s" % angle_key)

            angle_list = self.datapar[angle_key]

            # Calculate angular resolution.
            # calculate from -10% to +10% near the center of the subscan because we believe the antenna
            # moves at constant velocity.
            angle_index_min = int(
                self.obs.head.dri.rpoin - 0.1 * self.obs.head.dri.npoin
            )
            angle_index_max = int(
                self.obs.head.dri.rpoin + 0.1 * self.obs.head.dri.npoin
            )
            self.logger.debug(
                "Calculate angular resolution from index %d to %d of total %d"
                % (angle_index_min, angle_index_max, self.obs.head.dri.npoin)
            )

            angle_list_centered = angle_list[angle_index_min:angle_index_max]
            self.obs.head.dri.ares = (np.pi / 180) * (
                np.mean(np.diff(angle_list_centered))
            )
            self.logger.debug(
                "Calculated angular resolution = %f [deg], %f [arcsec]"
                % (self.obs.head.dri.ares, self.obs.head.dri.ares * 3600 * 180 / np.pi)
            )
        except:
            self.logger.logError(
                "WARNING: Using default value of head.dri.ares = %f"
                % DataFormatGildas.defaults.head.dri.ares
            )
            self.obs.head.dri.ares = DataFormatGildas.defaults.head.dri.ares

        # Get the sensor / receiver data from ARRAYDATA.
        # Slice the 2D array to find all integrations (time series) of a selected channel and selected feed.
        # TODO sort the data for monotonically increasing angles.  Gildas doesn't get a list of angles
        # to handle this.  It only knows the number of points, angular resolution, and reference index.
        # It assumes monotonically increasing angles, therefore we must sort the arraydata in this order.
        # In the case of a scan that moves from -X to +X or -Y to +Y, no need to sort. But if the scan
        # goes +X to -X or +Y to -Y, we have to sort the data to avoid a +/- sign change in pointing offset.

        # Find the index in ARRAYDATA that contains the maximum power
        peak3d = np.unravel_index(
            np.argmax(self.arraydata["DATA"], axis=None), self.arraydata["DATA"].shape
        )
        self.logger.info("found peak at index at [%d, %d, %d]" % peak3d)
        self.logger.info("integration angle: %d, usefeed: %d, channel: %d" % peak3d)

        # Separate the peak3D into index for each axis
        peak_angle = peak3d[0]
        peak_usefeed = peak3d[1]
        peak_channel = peak3d[2]

        # Slice data.  All angles, 1 usefeed, 1 spectrum channel.
        self.obs.datay = np.array(
            self.arraydata["DATA"][:, peak_usefeed, peak_channel], dtype=np.float32
        )

        # Temporary: print the entire Gildas header
        self.obs.write()

        self.write_subscan_finished.set()
        self.logger.logTrace("mutex unlock")
        self.mutex.unlock()


# Main defined only for testing with data in native format
# Not passing data throuth IDL type translations
if __name__ == "__main__":
    p = PipelineGildas()
