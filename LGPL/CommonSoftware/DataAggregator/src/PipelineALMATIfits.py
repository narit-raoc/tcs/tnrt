# Project: TNRT - Thai National Radio Telescope
# Organization: NARIT - National Astronomical Research Institute of Thailand
# Author: Spiro Sarris

# Python Packages
import logging
import time
import datetime
import os
import numpy as np
from scipy.interpolate import interp1d
from astropy.time import Time
from astropy.io import fits
from PyQt5 import QtCore

# TNRT
import DataAggregatorMod
import DataAggregatorErrorImpl
import TypeConverterDA as tc
import BackendMod
import tnrtAntennaMod
import DataFormatAcu as acu
from AbstractBasePipeline import AbstractBasePipeline
from DataFormatALMATIfits import DataFormatALMATIfits

class PipelineALMATIfits(AbstractBasePipeline):
    """
    Docstring
    """

    def __init__(self, metadata_ref):
        AbstractBasePipeline.__init__(self, metadata_ref)

        self.inputs = {
            "antenna": {
                "channel": tnrtAntennaMod.STATUS_CHANNEL_NAME,
                "handler": self.handler_antenna,
                "consumer": None,
            },
            "holofft": {
                "channel": BackendMod.CHANNEL_NAME_HOLO_DATA,
                "handler": self.handler_holofft,
                "consumer": None,
            },
        }

        self.outputs = {
            "pipeline": {
                "channel": DataAggregatorMod.CHANNEL_NAME_PIPELINE_ALMATIFITS,
                "supplier": None,
            }
        }
        
        self.monitor_antenna_PacketHeader_buffer = []
        self.monitor_antenna_GeneralStatus_buffer = []
        self.monitor_antenna_SystemStatus_buffer = []
        self.monitor_antenna_VertexShutterStatus_buffer = []
        self.monitor_antenna_AxisStatus_buffer = []
        self.monitor_antenna_MotorStatus_buffer = []
        self.monitor_antenna_TrackingStatus_buffer = []
        self.monitor_antenna_GeneralHexapodStatus_buffer = []
        self.monitor_antenna_HexapodStatus_buffer = []
        self.monitor_antenna_SpindleStatus_buffer = []
        self.monitor_antenna_HexapodTrackStatus_buffer = []
        self.monitor_antenna_TrackingObjectStatus_buffer = []

        self.latest_data_timestamp = None

    def open(self, mbfits_headers):
        AbstractBasePipeline.open(self, mbfits_headers)
        # Generate filename and open a new file
        
        # adding a Y-m-d folder format
        file_date_utc = datetime.datetime.utcnow()
        file_date_year = file_date_utc.strftime("%Y")
        file_date_month = file_date_utc.strftime("%m")
        file_date_day = file_date_utc.strftime("%d")

        # Year / month / day folder
        self.data_dir = self.data_dir + "/" + file_date_year + "/" + file_date_month + "/" + file_date_day + "/"

        if not os.path.exists(self.data_dir):
            self.logger.logInfo("data directory %s does not exist.  Create now" % self.data_dir)
            os.makedirs(name=self.data_dir, exist_ok=True)

        self.filename_base = file_date_year + file_date_month + file_date_day + "_" + file_date_utc.strftime("%H%M%S")
        self.filename = self.data_dir + self.filename_base + ".atfits"
        
        self.logger.logInfo("Generate filename= " + self.filename)

        self.logger.logDebug("Write Primary HDU")
        # Create a new file with only primary header.
        # Convert the Python object with attributes into dictionary
        # and automatically fill the primary header.
        hdr = DataFormatALMATIfits.generate_header_primary(
            mbfits_headers.PRIMARY_MBFITS_HEADER, mbfits_headers.SCAN_MBFITS_HEADER
        )
        hdu_primary = fits.PrimaryHDU(header=hdr)
        # Temporary hdul to create file
        hdul = fits.HDUList([hdu_primary])
        # Write file
        hdul.writeto(self.filename)
        hdul.close()

        # Open the same file in append mode (fits mode 'update').
        # 'update' mode allows use of hdul.flush() to write to file after
        # data in hdul list is updated / appended
        self.hdul = fits.open(self.filename, mode="append")

    def close(self):

        if self.hdul is not None:
            # Blocking wait for event write_subscan_finished to be set true by another thread before close the file.
            self.logger.logInfo(
                "Wait for write_monitor_finished event before closing file ..."
            )
            self.write_monitor_finished.wait(timeout=60.0)
            if self.write_monitor_finished.is_set() == False:
                ex = DataAggregatorErrorImpl.timeoutOnCloseErrorExImpl()
                ex.addData(
                    "errExpl",
                    "Timeout while waiting for file to close.  Astropy IERS download?",
                )
                raise ex
            else:
                self.hdul.close()
                del self.hdul
                return AbstractBasePipeline.close(self)

    def handler_antenna(self, data_idl):
        # Convert the IDL structs (OmniORB python objects) to a numpy arrays
        (
            monitor_antenna_PacketHeader_row,
            monitor_antenna_GeneralStatus_row,
            monitor_antenna_SystemStatus_row,
            monitor_antenna_VertexShutterStatus_row,
            monitor_antenna_AxisStatus_row,
            monitor_antenna_MotorStatus_row,
            monitor_antenna_TrackingStatus_row,
            monitor_antenna_GeneralHexapodStatus_row,
            monitor_antenna_HexapodStatus_row,
            monitor_antenna_SpindleStatus_row,
            monitor_antenna_HexapodTrackStatus_row,
            monitor_antenna_TrackingObjectStatus_row,
        ) = tc.from_idl_AntennaStatusNotifyBlock(data_idl)

        self.logger.logTrace("mutex lock")
        self.mutex.lock()

        # append to the buffers
        self.monitor_antenna_PacketHeader_buffer.append(
            monitor_antenna_PacketHeader_row
        )
        self.monitor_antenna_GeneralStatus_buffer.append(
            monitor_antenna_GeneralStatus_row
        )
        self.monitor_antenna_SystemStatus_buffer.append(
            monitor_antenna_SystemStatus_row
        )
        self.monitor_antenna_VertexShutterStatus_buffer.append(
            monitor_antenna_VertexShutterStatus_row
        )
        self.monitor_antenna_AxisStatus_buffer.append(monitor_antenna_AxisStatus_row)
        self.monitor_antenna_MotorStatus_buffer.append(monitor_antenna_MotorStatus_row)
        self.monitor_antenna_TrackingStatus_buffer.append(
            monitor_antenna_TrackingStatus_row
        )
        self.monitor_antenna_GeneralHexapodStatus_buffer.append(
            monitor_antenna_GeneralHexapodStatus_row
        )
        self.monitor_antenna_HexapodStatus_buffer.append(
            monitor_antenna_HexapodStatus_row
        )
        self.monitor_antenna_SpindleStatus_buffer.append(
            monitor_antenna_SpindleStatus_row
        )
        self.monitor_antenna_HexapodTrackStatus_buffer.append(
            monitor_antenna_HexapodTrackStatus_row
        )
        self.monitor_antenna_TrackingObjectStatus_buffer.append(
            monitor_antenna_TrackingObjectStatus_row
        )

        self.logger.logTrace("mutex unlock")
        self.mutex.unlock()

    def handler_holofft(self, msg_holodata):
        #self.logger.logDebug("handle event from holofft: {}".format(msg_holodata))
        self.packets_received = self.packets_received + 1
        self.packets_received_subscan = self.packets_received_subscan + 1
        self.logger.logDebug("self.packets_received_subscan: {}".format(self.packets_received_subscan))
        # Initialize an array for HOLODATA for a new row to append
        holodata_row = np.array(0, dtype=DataFormatALMATIfits.dtype_holodata_row)
        # Fill data in HOLODATA
        holodata_row["INTEGNUM"] = self.packets_received_subscan
        
        # Convert decibel units from holodata message to linear scale units for .atfits
        power_magnitude_watt_ch1 = 10 ** ((msg_holodata["marker_val_absolute_magnitude_ch1_dbm"] - 30) / 10)
        power_magnitude_watt_ch2 = 10 ** ((msg_holodata["marker_val_absolute_magnitude_ch2_dbm"] - 30) / 10)
        relative_magnitude_linear = 10 ** (msg_holodata["marker_val_relative_magnitude_db"] / 10)
              
        holodata_row["HOLOSS"] = power_magnitude_watt_ch2
        holodata_row["HOLORR"] = power_magnitude_watt_ch1
        holodata_row["HOLOQQ"] = power_magnitude_watt_ch2
        holodata_row["HOLOSR"] = (
            np.sqrt(np.abs(power_magnitude_watt_ch2 * power_magnitude_watt_ch1))
            * np.cos(np.radians(msg_holodata["marker_val_relative_phase_deg"]))
        )
        holodata_row["HOLOSQ"] = 0  # broadcast
        holodata_row["HOLOQR"] = (
            -1
            * np.sqrt(power_magnitude_watt_ch2 * power_magnitude_watt_ch1)
            * np.sin(np.radians(msg_holodata["marker_val_relative_phase_deg"]))
        )
        holodata_row["AMPREF"] = np.sqrt(50 * power_magnitude_watt_ch1)
        holodata_row["AMPTST"] = np.sqrt(50 * power_magnitude_watt_ch2)
        holodata_row["AMPREL"] = relative_magnitude_linear
        holodata_row["PHAREL"] = msg_holodata["marker_val_relative_phase_deg"]
        holodata_row["FREQMX"] = msg_holodata["marker_val_freq_hz"]

        # Initialize a new DATAPAR row to associate with the current HOLODATA
        # In this "fast" loop, set only the values that can change for each
        # data packet during the subscan.

        # Remaining DATAPAR can be processed at the end of the subscan.
        # For example, use timestamps MONITOR-ANT data onto LONGOFF / LATOFF.
        # or fill arrays of constant data that doesn't change during subscan

        datapar_row = np.array(0, dtype=DataFormatALMATIfits.dtype_datapar_row)
        datapar_row["INTEGNUM"] = self.packets_received_subscan
        datapar_row["INTEGTIM"] = msg_holodata["sample_capture_duration"]
        datapar_row["MJD"] = msg_holodata["time_mjd"]

        # Manage packet timestamps to prepare for sorting high-latency data after subscan
        packet_timestamp = Time(datapar_row["MJD"],format='mjd')
        self.latest_data_timestamp = packet_timestamp
        now_timestamp = Time.now()
        
        # Create a copy (by reference, NOT values) to dictionary that we can traverse
        # and sort the incoming packets (with latency) to the appropriate subscans
        # that have already been created.  Note: that this is only a reference to subscans
        # that have been created from the past until now -- not a copy
        # of the original data.  Meanwhile, another thread can add new subscans
        # to the original subscan dict without changing the size or interfering with
        # the iteration / traversal of the subscans we want to process now.
        subscan_dict_shallow_copy = dict(self.subscan_dict)

        for subs_num, subscan in subscan_dict_shallow_copy.items():
            if packet_timestamp >= subscan.time_enable_write:
                # If the subscan we are processing is still running (real-time) and has not yet 
                # been stopped, use the current now_timestamp to avoid writing incorrectly 
                # timestamped "future" data into the subscan.
                if subscan.time_disable_write is None:
                    end_condition = now_timestamp
                else:
                    # the subscan we are processing has already finished, and we want to
                    # sort the data packets that arrive now (with latency) to the
                    # correct subscan that has already finished.
                    end_condition = subscan.time_disable_write

                if packet_timestamp <= end_condition:
                    self.logger.logDebug("packet_timestamp {} <= {} end_condition. APPEND!".format(
                        packet_timestamp.iso, end_condition
                    ))
                    self.subscan_dict[subs_num].arraydata_buffer.append(holodata_row)   
                    self.subscan_dict[subs_num].datapar_buffer.append(datapar_row)
            else:
                self.logger.logDebug("packet_timestamp {} < {} subscan_time_enable_write. Do not append this subscan".format(
                    packet_timestamp.iso, subscan.time_enable_write)
                )

    def write_scan_header(self, mbfits_headers):
        """
        NO OP.  ALMATI format does not have a scan header
        This function exists to implement required abstractmethod in AbstractBaseScan
        """
        pass

    def write_before_subscan(self, mbfits_headers):
        self.packets_received_subscan = 0

    def write_after_subscan(self, mbfits_headers, subscan_number):
        """
        astropy.io.fits does not have a convenient method to append rows to binary tables
        in real-time because it uses numpy arrays that are sequential in memory.  Therefore
        data are buffered in lists in DataAggregator.  At the end of a subscan, the lists
        are converted to numpy arrays of the correct format for ALMATIfits and written to file
        here.
        TODO : if this technique consumes too much memory for long subscans and large dimension
        arrays, find a better way to write blocks of data to the file throughout subscan
        before memory is overloaded.  Or, change to use a separate package fitsio instead
        of astropy.io.fits because the documentation suggests that it can append to files in
        realtime better than astropy.io.fits

        Parameters
        ---------
        mbfits_headers : ScanMod.MetadataNotifyBlock
                Complete set of MBFITS-style header information.  Including the DATAPAR header
                that was created at runtime during the subscan for each integration.
                ALMATIfits uses only a subset of data from MBfits, so this pipeline selects
                the required data from the bigger MBfits headers and writes in the correct
                ALMATIfits format.
        """

        subscan_obj = self.subscan_dict[subscan_number]
        
        # check for data buffer if it has some data and wait for the late data due to backend latency
        # only apply to AbstractSubscanAZEL (SubscanLine, SubscanSingle) where we intend to get data from backend
        # Assume maximum latency is 5 seconds.  In the case that scan / subscan data structures have are created,
        # and subscan_obj.time_disable_write != None , it is possible that the backend stops sending data and
        # we wait here forever if the subscan has not yet received 1 packet.
        if subscan_obj.time_disable_write != None:
            latency = 0
            MAX_LATENCY = 5.0
            while (len(subscan_obj.arraydata_buffer) <= 0) and (latency < MAX_LATENCY) :
                latency = Time.now() - subscan_obj.time_disable_write
                self.logger.logInfo('subscan {} - wait for first data packet. latency: {}'.format(subscan_number, latency))

                # if we are expecting data from backend, the latest_data_timestamp should not be None because 
                # it should records timestamp of received packet since measurement start. If we expect to get data 
                # from backend but expecting_backend_data is None, there is 2 possible reason
                # 1. we don't receive data from backend
                # 2. the integration time is too long and does not suit the observation time. 
                expecting_backend_data = self.latest_data_timestamp != None 

                # check if the lates data timestamp has passed the time enable write.
                # If we really receiving spectrum data, the buffer should already not be empty.
                # If enter this condition could possibly from 3 reason
                # 1. we do not use backend for the observation (nothing to worry about)
                # 2. some thing wrong with backend streaming 
                # 3. some thing wrong with the data recieving client
                # break the loop and skip to adding dummy data
                if expecting_backend_data and (self.latest_data_timestamp > subscan_obj.time_enable_write):
                    self.logger.logInfo('Subscan {} - No data appended to data buffer')
                    self.logger.logInfo('time_enable_write = {}, time_disable_write = {}'.format(subscan_obj.time_enable_write, subscan_obj.time_disable_write))
                    break
                elif (not expecting_backend_data) and (Time.now() > subscan_obj.time_disable_write):
                    self.logger.logInfo('Subscan does not require backend, break the loop.')
                    break
                time.sleep(0.5)

            if len(subscan_obj.datapar_buffer) > 0:
                while (self.latest_data_timestamp < subscan_obj.time_disable_write):
                    self.logger.logInfo('subscan {} - wait for late data... '.format(subscan_number))
                    self.logger.logDebug('buffer latest timestamp {}, disable write timestamp {}'.format(
                        Time(subscan_obj.datapar_buffer[-1]['MJD'],format='mjd').iso,
                        subscan_obj.time_disable_write.iso
                        ))
                    time.sleep(0.5)
                    
        # Check if any of the buffers are empty.  If empty, create 1 fake row of zeros
        # so that length of array == 1. If length == 0, error in creating FITS columns because 0
        # is not a valid dimension.  In a real operation system, all buffers should have real data
        # For test and debug, sometimes we do not have data in some buffer.  Also, if we
        # only want to record data about Antenna position -- but not use receivers, we need
        # to create a placeholder for arrays that otherwise have a 0 dimension.
        if len(subscan_obj.arraydata_buffer) == 0:
            self.logger.logDebug(
                "holodata_buffer len==0. append 1 dummy array of zeros to avoid array dimension errors in FITS file"
            )
            subscan_obj.arraydata_buffer.append(
                np.array(0, dtype=DataFormatALMATIfits.dtype_holodata_row)
            )

        if len(subscan_obj.datapar_buffer) == 0:
            self.logger.logDebug(
                "datapar_buffer len==0. append 1 dummy array of zeros to avoid array dimension errors in FITS file"
            )
            subscan_obj.datapar_buffer.append(
                np.array(0, dtype=DataFormatALMATIfits.dtype_datapar_row)
            )


        # self.<xxx>_buffer is a Python list of self.dtype_<xxx>_row.  Each dtype_<xxx>_row
        # is a numpy structured array with timestamp MJD and an array of data that has dimensions
        # depend on spectrum channels, sections, etc (defined at runtime).  List of rows was used
        # because it is faster to append list than concatenate numpy array.   Now append is finished
        # and convert the entire list into 1 numpy array for convenient / efficient processing.
        holodata = np.asarray(subscan_obj.arraydata_buffer)

        # self.datapar_buffer is a Python list of self.dtype_datapar_row.  Each dtype_datapar_row
        # is a fixed size and includes metadata associated with each row of ARRAYDATA (Az, El angles, etc)
        datapar = np.asarray(subscan_obj.datapar_buffer)

        # Broadcast constant data to all integration packets of this subscan
        datapar["FLAG"] = 1
        datapar["ISWITCH"] = 1
        datapar["HOLO"] = True

        # TODO
        # Reference PipelineBMfits.py already works for TNRT spectrometer pointing model
        # if datapar is not a dummy data (>1), interpolate position LONGOFF and LATOFF
        # from monitor antenna to datapar timestamps
        if len(datapar) > 0:
            try:
                AZ = 0
                EL = 1
                monitor_ant_axis = np.asarray(self.monitor_antenna_AxisStatus_buffer)
                # sometimes the acu data is appended to buffer in another thread in the middle of reading these data
                # we need to make sure that the length of 3 arrays (monitor_ant_axis, monitor_ant_tracking, monitor_ant_general)
                # are the same, so use length of monitor_ant_axis as a reference and make it the length of the other 
                # by cutting the last one of
                monitor_ant_tracking = np.asarray(self.monitor_antenna_TrackingStatus_buffer)[:len(monitor_ant_axis)]
                monitor_ant_general = np.asarray(self.monitor_antenna_GeneralStatus_buffer)[:len(monitor_ant_axis)]

                timestamps_spe = datapar["MJD"]
                timestamps_acu = monitor_ant_general["actual_time"]

                actual_position_az = monitor_ant_axis["actual_position"][:, AZ]
                actual_position_el = monitor_ant_axis["actual_position"][:, EL]

                # Find tracking center coordinate from selected tracking algorithm in monitor_ant_tracking["bit_status"]
                # The bit status exists for every ACU status message in the array.  
                # Assume it does not change during this subscan. So, we can use the first value at index [0] 
                if (monitor_ant_tracking["bit_status"][0] & acu.TR_SUN_TRACK_ACTIVE) == acu.TR_SUN_TRACK_ACTIVE:
                    self.logger.logDebug("Found selected tracking algorithm: TR_SUN_TRACK_ACTIVE")
                    selected_tracking_algorithm_position_az = monitor_ant_tracking["sun_track_az"]
                    selected_tracking_algorithm_position_el = monitor_ant_tracking["sun_track_el"]

                elif (monitor_ant_tracking["bit_status"][0] & acu.TR_PROG_TRACK_ACTIVE) == acu.TR_PROG_TRACK_ACTIVE:
                    self.logger.logDebug("Found selected tracking algorithm: TR_PROG_TRACK_ACTIVE")
                    selected_tracking_algorithm_position_az = monitor_ant_tracking["prog_track_az"]
                    selected_tracking_algorithm_position_el = monitor_ant_tracking["prog_track_el"]
                
                elif (monitor_ant_tracking["bit_status"][0] & acu.TR_TLE_TRACK_ACTIVE) == acu.TR_TLE_TRACK_ACTIVE:
                    self.logger.logDebug("Found selected tracking algorithm: TR_TLE_TRACK_ACTIVE")
                    selected_tracking_algorithm_position_az = monitor_ant_tracking["tle_track_az"]
                    selected_tracking_algorithm_position_el = monitor_ant_tracking["tle_track_el"]
                
                elif (monitor_ant_tracking["bit_status"][0] & acu.TR_STAR_TRACK_ACTIVE) == acu.TR_STAR_TRACK_ACTIVE:
                    self.logger.logDebug("Found selected tracking algorithm: TR_STAR_TRACK_ACTIVE")
                    selected_tracking_algorithm_position_az = monitor_ant_tracking["star_track_az"]
                    selected_tracking_algorithm_position_el = monitor_ant_tracking["star_track_el"]
                
                else:
                    self.logger.logWarning("Cannot identify selected tracking algorithm.")
                    self.logger.logWarning("Set tracking algorithm position = (0,0). Set actual_position = program_offset")
                    selected_tracking_algorithm_position_az = 0
                    selected_tracking_algorithm_position_el = 0
                    actual_position_az = monitor_ant_tracking["prog_offset_az"]
                    actual_position_el = monitor_ant_tracking["prog_offset_el"]

                long_off = actual_position_az - selected_tracking_algorithm_position_az
                lat_off = actual_position_el - selected_tracking_algorithm_position_el

                self.logger.logDebug('timestamps_acu len = {}'.format(np.asarray(timestamps_acu).shape))
                self.logger.logDebug('actual_position_el len = {}'.format(np.asarray(actual_position_el).shape))
                self.logger.logDebug('long_off len = {}'.format(np.asarray(long_off).shape))
                self.logger.logDebug('lat_off len = {}'.format(np.asarray(lat_off).shape))

                interp_function_acu_el = interp1d(
                    timestamps_acu, actual_position_el, kind="cubic", fill_value="extrapolate"
                )
                interp_function_acu_long_off = interp1d(
                    timestamps_acu, long_off, kind="cubic", fill_value="extrapolate"
                )
                interp_function_acu_lat_off = interp1d(
                    timestamps_acu, lat_off, kind="cubic", fill_value="extrapolate"
                )

                el_at_timestamps_spectrometer = interp_function_acu_el(timestamps_spe)
                long_off_at_timestamps_spectrometer = interp_function_acu_long_off(timestamps_spe)
                lat_off_at_timestamps_spectrometer = interp_function_acu_lat_off(timestamps_spe)

                datapar["LATOFF"] = lat_off_at_timestamps_spectrometer
                
                # If the program offset table scan pattern is an AZ,EL pattern, scale the LONGOFF
                # values based on the GLS projection.
                # Reference

                """ Sinusoidal (GLS) projection for the LONGOFF and LATOFF columns in the DATAPAR-MBFITS table. This
                was done to be more compatible with the Gildas software that is used for subsequent spectral line data
                reductions. Many bolometer data reduction packages also inherently assume this kind of projection. The
                GLS projection is very similar though not identical to SFL. Originally, it was described in AIPS memo 46
                by E. Greisen. The difference is that the cosine is applied to the latitude rather than the delta in latitude:
                GLS projection: 
                x = (φ - φ0 ) cos θ
                y = (θ - θ0 ) 
                """
                self.logger.logDebug("NLNGTYPE: {}".format(subscan_obj.metadata.datapar_mbfits_header['NLNGTYPE']))

                if subscan_obj.metadata.datapar_mbfits_header['NLNGTYPE']=="ALON-GLS":
                    self.logger.logDebug("program offset longitude is ALON-GLS (AZ,EL), scale LONGOFF for GLS projection.")
                    datapar["LONGOFF"] = long_off_at_timestamps_spectrometer * np.cos(np.radians(el_at_timestamps_spectrometer))
                    
                    self.logger.logDebug("Rename ALON-GLS -> AZIM-SIN, ALAT-GLS ->  ELEV-SIN for ALAMATI-FITS")
                    subscan_obj.metadata.datapar_mbfits_header['NLNGTYPE'] = "AZIM-SIN"
                    subscan_obj.metadata.datapar_mbfits_header['NLATTYPE'] = "ELEV-SIN"
                else:
                    self.logger.logDebug("program offset longitude is not ALON-GLS (AZ,EL). Do not scale LONGOFF.")
                    datapar["LONGOFF"] = long_off_at_timestamps_spectrometer
                
            except ValueError as e:
                self.logger.logError(e)
                self.logger.logError('Can not interpolate data when using MockACU')
            except Exception as e:
                self.logger.logError(e)
                self.logger.logError('Something went wrong while interpolating data')

        if self.opened is True:
            self.logger.logDebug("pipeline opened is True")
            # Clear the flow control flag
            self.write_subscan_finished.clear()
            self.logger.logDebug("QThreadpool.start WorkerWriteAfterSubscan")
            # Create a runnable and delegate the work to the Threadpool.
            worker = WorkerWriteAfterSubscan(
                self.write_subscan_finished,
                self.hdul,
                mbfits_headers,
                subscan_obj.metadata,
                holodata,
                datapar,
            )

            self.threadpool.start(worker)
            self.packets_written += holodata.shape[0]
    
        # clear finished subscan to save the ram
        subscan_obj.clear_buffer()

    def write_monitor(self, mbfits_headers):
        # I don't actually write monitor data, but set the event to allow file to close
        self.logger.logDebug("set write_monitor_finished Event")
        self.write_monitor_finished.set()

    def clear_subscan_buffers(self):
        pass

    def clear_monitor_buffers(self):
        pass


class WorkerWriteAfterSubscan(QtCore.QRunnable):
    """Worker thread to write drift using resources from Threadpool"""

    def __init__(
        self, write_subscan_finished, hdul, mbfits_headers_idl, subscan_metadata, arraydata, datapar
    ):

        super().__init__()
        self.logger = logging.getLogger(
            "%s.%s" % (self.__module__, self.__class__.__name__)
        )
        self.mutex = QtCore.QMutex()
        self.write_subscan_finished = write_subscan_finished
        self.hdul = hdul
        self.mbfits_headers_idl = mbfits_headers_idl
        self.subscan_metedata = subscan_metadata
        self.arraydata = arraydata
        self.datapar = datapar

    def run(self):
        # Get the mutex to the hdul and file.  If another subscan finished too fast before this subscan,
        # It will have to wait before modifying the MBFITS HDU List object.
        self.logger.logTrace("mutex lock")
        self.mutex.lock()

        self.logger.info(
            "Add DATAPAR-MBFITS header and data. Scan: {}, Subscan: {}".format(
                self.subscan_metedata.datapar_mbfits_header['SCANNUM'],
                self.subscan_metedata.datapar_mbfits_header['SUBSNUM'],
            )
        )
        # Generate the DATAPAR header for ALMATI fits format. IT looks confusing because all
        # of the header information we manage for TCS scan run workflow is organized following
        # the structure of MBFITS.  ALMATI fits can use the same data, but has to be re-organized
        # The data that remains constant for the entire scan is still using the IDL struct
        # The data that changes during the subscans is now managed by redis cdb and subscan metadata dict
        # We need both to constuct the correct datapar header for ALMATI fits format.
        hdr = DataFormatALMATIfits.generate_header_datapar(
            self.mbfits_headers_idl.PRIMARY_MBFITS_HEADER,
            self.mbfits_headers_idl.SCAN_MBFITS_HEADER,
            self.subscan_metedata.datapar_mbfits_header)
        
        # Add of the sum of all integration times in this subscan to calculate the total exposure time.
        hdr["EXPOSURE"] = np.sum(self.datapar["INTEGTIM"])
        coldefs = DataFormatALMATIfits.generate_coldefs_datapar(self.datapar)
        hdu = fits.BinTableHDU.from_columns(coldefs, header=hdr)
        self.hdul.append(hdu)

        self.logger.info(
            "Add HOLODATA-MBFITS header and data. Scan: {}, Subscan: {}".format(
                self.subscan_metedata.datapar_mbfits_header['SCANNUM'],
                self.subscan_metedata.datapar_mbfits_header['SUBSNUM'],
            )
        )

        hdr = DataFormatALMATIfits.generate_header_holodata(
            self.mbfits_headers_idl.SCAN_MBFITS_HEADER,
            self.subscan_metedata.datapar_mbfits_header)

        coldefs = DataFormatALMATIfits.generate_coldefs_holodata(self.arraydata)
        hdu = fits.BinTableHDU.from_columns(coldefs, header=hdr)
        self.hdul.append(hdu)

        # Write to file on disk.
        # Sometimes it shows IOError: close() called during concurrent operation on the same file object
        # So catch the error, wait a little bit, and try again.
        try:
            self.logger.logDebug("try: hdul flush Subscan data")
            self.hdul.flush()
        except IOError:
            self.logger.logError("Caught IOError.  Wait 1 second and try again")
            time.sleep(1.0)
            self.logger.logDebug("repeat: hdul flush Subscan data")
            self.hdul.flush()

        # Set the event to let other threads know that this write operation is finished.
        self.logger.logDebug("set write_subscan_finished Event")
        self.write_subscan_finished.set()

        # Unlock access to the objects in this worker thread and allow other waiting worker threads
        # to continue
        self.logger.logTrace("mutex unlock")
        self.mutex.unlock()


class WorkerWriteMonitor(QtCore.QRunnable):
    """Worker thread to write drift using resources from Threadpool"""

    def __init__(
        self,
        write_monitor_finished,
        hdul,
        mbfits_headers,
        monitor_antenna_PacketHeader,
        monitor_antenna_GeneralStatus,
        monitor_antenna_SystemStatus,
        monitor_antenna_VertexShutterStatus,
        monitor_antenna_AxisStatus,
        monitor_antenna_MotorStatus,
        monitor_antenna_TrackingStatus,
        monitor_antenna_GeneralHexapodStatus,
        monitor_antenna_HexapodStatus,
        monitor_antenna_SpindleStatus,
        monitor_antenna_HexapodTrackStatus,
        monitor_antenna_TrackingObjectStatus,
        monitor_cal,
        monitor_weather,
    ):

        super().__init__()
        self.logger = logging.getLogger(
            "%s.%s" % (self.__module__, self.__class__.__name__)
        )
        self.mutex = QtCore.QMutex()
        self.write_monitor_finished = write_monitor_finished
        self.hdul = hdul
        self.mbfits_headers = mbfits_headers
        self.monitor_antenna_PacketHeader = monitor_antenna_PacketHeader
        self.monitor_antenna_GeneralStatus = monitor_antenna_GeneralStatus
        self.monitor_antenna_SystemStatus = monitor_antenna_SystemStatus
        self.monitor_antenna_VertexShutterStatus = monitor_antenna_VertexShutterStatus
        self.monitor_antenna_AxisStatus = monitor_antenna_AxisStatus
        self.monitor_antenna_MotorStatus = monitor_antenna_MotorStatus
        self.monitor_antenna_TrackingStatus = monitor_antenna_TrackingStatus
        self.monitor_antenna_GeneralHexapodStatus = monitor_antenna_GeneralHexapodStatus
        self.monitor_antenna_HexapodStatus = monitor_antenna_HexapodStatus
        self.monitor_antenna_SpindleStatus = monitor_antenna_SpindleStatus
        self.monitor_antenna_HexapodTrackStatus = monitor_antenna_HexapodTrackStatus
        self.monitor_antenna_TrackingObjectStatus = monitor_antenna_TrackingObjectStatus
        self.monitor_cal = monitor_cal
        self.monitor_weather = monitor_weather

    def run(self):
        # Get the mutex to the hdul and file.  If another subscan finished too fast before this subscan,
        # It will have to wait before modifying the MBFITS HDU List object.
        self.logger.logTrace("mutex lock")
        self.mutex.lock()

        self.logger.logInfo(
            "Add MONITOR-ANT-PACKET header and data. Scan: {}".format(
                self.mbfits_headers.DATAPAR_MBFITS_HEADER.SCANNUM
            )
        )
        hdr = DataFormatMONITORfits.generate_header_monitor_antenna_PacketHeader(
            self.mbfits_headers.DATAPAR_MBFITS_HEADER
        )
        coldefs = DataFormatMONITORfits.generate_coldefs_monitor_antenna_PacketHeader(
            self.monitor_antenna_PacketHeader
        )
        hdu = fits.BinTableHDU.from_columns(coldefs, header=hdr)
        self.hdul.append(hdu)

        self.logger.logInfo(
            "Add MONITOR-ANT-GENERAL header and data. Scan: {}".format(
                self.mbfits_headers.DATAPAR_MBFITS_HEADER.SCANNUM
            )
        )
        hdr = DataFormatMONITORfits.generate_header_monitor_antenna_GeneralStatus(
            self.mbfits_headers.DATAPAR_MBFITS_HEADER
        )
        coldefs = DataFormatMONITORfits.generate_coldefs_monitor_antenna_GeneralStatus(
            self.monitor_antenna_GeneralStatus
        )
        hdu = fits.BinTableHDU.from_columns(coldefs, header=hdr)
        self.hdul.append(hdu)

        self.logger.logInfo(
            "Add MONITOR-ANT-SYSTEM header and data. Scan: {}".format(
                self.mbfits_headers.DATAPAR_MBFITS_HEADER.SCANNUM
            )
        )
        hdr = DataFormatMONITORfits.generate_header_monitor_antenna_SystemStatus(
            self.mbfits_headers.DATAPAR_MBFITS_HEADER
        )
        coldefs = DataFormatMONITORfits.generate_coldefs_monitor_antenna_SystemStatus(
            self.monitor_antenna_SystemStatus
        )
        hdu = fits.BinTableHDU.from_columns(coldefs, header=hdr)
        self.hdul.append(hdu)

        self.logger.logInfo(
            "Add MONITOR-ANT-VERTEX header and data. Scan: {}".format(
                self.mbfits_headers.DATAPAR_MBFITS_HEADER.SCANNUM
            )
        )
        hdr = DataFormatMONITORfits.generate_header_monitor_antenna_VertexShutterStatus(
            self.mbfits_headers.DATAPAR_MBFITS_HEADER
        )
        coldefs = (
            DataFormatMONITORfits.generate_coldefs_monitor_antenna_VertexShutterStatus(
                self.monitor_antenna_VertexShutterStatus
            )
        )
        hdu = fits.BinTableHDU.from_columns(coldefs, header=hdr)
        self.hdul.append(hdu)

        self.logger.logInfo(
            "Add MONITOR-ANT-AXIS header and data. Scan: {}".format(
                self.mbfits_headers.DATAPAR_MBFITS_HEADER.SCANNUM
            )
        )
        hdr = DataFormatMONITORfits.generate_header_monitor_antenna_AxisStatus(
            self.mbfits_headers.DATAPAR_MBFITS_HEADER
        )
        coldefs = DataFormatMONITORfits.generate_coldefs_monitor_antenna_AxisStatus(
            self.monitor_antenna_AxisStatus
        )
        hdu = fits.BinTableHDU.from_columns(coldefs, header=hdr)
        self.hdul.append(hdu)

        self.logger.logInfo(
            "Add MONITOR-ANT-MOTOR header and data. Scan: {}".format(
                self.mbfits_headers.DATAPAR_MBFITS_HEADER.SCANNUM
            )
        )
        hdr = DataFormatMONITORfits.generate_header_monitor_antenna_MotorStatus(
            self.mbfits_headers.DATAPAR_MBFITS_HEADER
        )
        coldefs = DataFormatMONITORfits.generate_coldefs_monitor_antenna_MotorStatus(
            self.monitor_antenna_MotorStatus
        )
        hdu = fits.BinTableHDU.from_columns(coldefs, header=hdr)
        self.hdul.append(hdu)

        self.logger.logInfo(
            "Add MONITOR-ANT-TRACKING header and data. Scan: {}".format(
                self.mbfits_headers.DATAPAR_MBFITS_HEADER.SCANNUM
            )
        )
        hdr = DataFormatMONITORfits.generate_header_monitor_antenna_TrackingStatus(
            self.mbfits_headers.DATAPAR_MBFITS_HEADER
        )
        coldefs = DataFormatMONITORfits.generate_coldefs_monitor_antenna_TrackingStatus(
            self.monitor_antenna_TrackingStatus
        )
        hdu = fits.BinTableHDU.from_columns(coldefs, header=hdr)
        self.hdul.append(hdu)

        self.logger.logInfo(
            "Add MONITOR-ANT-HXP-GEN header and data. Scan: {}".format(
                self.mbfits_headers.DATAPAR_MBFITS_HEADER.SCANNUM
            )
        )
        hdr = (
            DataFormatMONITORfits.generate_header_monitor_antenna_GeneralHexapodStatus(
                self.mbfits_headers.DATAPAR_MBFITS_HEADER
            )
        )
        coldefs = (
            DataFormatMONITORfits.generate_coldefs_monitor_antenna_GeneralHexapodStatus(
                self.monitor_antenna_GeneralHexapodStatus
            )
        )
        hdu = fits.BinTableHDU.from_columns(coldefs, header=hdr)
        self.hdul.append(hdu)

        self.logger.logInfo(
            "Add MONITOR-ANT-HXP header and data. Scan: {}".format(
                self.mbfits_headers.DATAPAR_MBFITS_HEADER.SCANNUM
            )
        )
        hdr = DataFormatMONITORfits.generate_header_monitor_antenna_HexapodStatus(
            self.mbfits_headers.DATAPAR_MBFITS_HEADER
        )
        coldefs = DataFormatMONITORfits.generate_coldefs_monitor_antenna_HexapodStatus(
            self.monitor_antenna_HexapodStatus
        )
        hdu = fits.BinTableHDU.from_columns(coldefs, header=hdr)
        self.hdul.append(hdu)

        self.logger.logInfo(
            "Add MONITOR-ANT-SPINDLE header and data. Scan: {}".format(
                self.mbfits_headers.DATAPAR_MBFITS_HEADER.SCANNUM
            )
        )
        hdr = DataFormatMONITORfits.generate_header_monitor_antenna_SpindleStatus(
            self.mbfits_headers.DATAPAR_MBFITS_HEADER
        )
        coldefs = DataFormatMONITORfits.generate_coldefs_monitor_antenna_SpindleStatus(
            self.monitor_antenna_SpindleStatus
        )
        hdu = fits.BinTableHDU.from_columns(coldefs, header=hdr)
        self.hdul.append(hdu)

        self.logger.logInfo(
            "Add MONITOR-ANT-HXP-TRACK header and data. Scan: {}".format(
                self.mbfits_headers.DATAPAR_MBFITS_HEADER.SCANNUM
            )
        )
        hdr = DataFormatMONITORfits.generate_header_monitor_antenna_HexapodTrackStatus(
            self.mbfits_headers.DATAPAR_MBFITS_HEADER
        )
        coldefs = (
            DataFormatMONITORfits.generate_coldefs_monitor_antenna_HexapodTrackStatus(
                self.monitor_antenna_HexapodTrackStatus
            )
        )
        hdu = fits.BinTableHDU.from_columns(coldefs, header=hdr)
        self.hdul.append(hdu)

        self.logger.logInfo(
            "Add MONITOR-ANT-TRACKING-OBJECT header and data. Scan: {}".format(
                self.mbfits_headers.DATAPAR_MBFITS_HEADER.SCANNUM
            )
        )
        hdr = (
            DataFormatMONITORfits.generate_header_monitor_antenna_TrackingObjectStatus(
                self.mbfits_headers.DATAPAR_MBFITS_HEADER
            )
        )
        coldefs = (
            DataFormatMONITORfits.generate_coldefs_monitor_antenna_TrackingObjectStatus(
                self.monitor_antenna_TrackingObjectStatus
            )
        )
        hdu = fits.BinTableHDU.from_columns(coldefs, header=hdr)
        self.hdul.append(hdu)

        self.logger.logInfo(
            "Add MONITOR-CAL header and data. Scan: {}".format(
                self.mbfits_headers.DATAPAR_MBFITS_HEADER.SCANNUM
            )
        )
        hdr = DataFormatMONITORfits.generate_header_monitor_cal(
            self.mbfits_headers.DATAPAR_MBFITS_HEADER
        )
        coldefs = DataFormatMONITORfits.generate_coldefs_monitor_cal(self.monitor_cal)
        hdu = fits.BinTableHDU.from_columns(coldefs, header=hdr)
        self.hdul.append(hdu)

        self.logger.logInfo(
            "Add MONITOR-WEATHER header and data. Scan: {}".format(
                self.mbfits_headers.DATAPAR_MBFITS_HEADER.SCANNUM
            )
        )
        hdr = DataFormatMONITORfits.generate_header_monitor_weather(
            self.mbfits_headers.DATAPAR_MBFITS_HEADER
        )
        coldefs = DataFormatMONITORfits.generate_coldefs_monitor_weather(
            self.monitor_weather
        )
        hdu = fits.BinTableHDU.from_columns(coldefs, header=hdr)
        self.hdul.append(hdu)

        # Write to file on disk.
        # Sometimes it shows IOError: close() called during concurrent operation on the same file object
        # So catch the error, wait a little bit, and try again.
        try:
            self.logger.logDebug("try: mbfits hdul flush Monitor data")
            self.hdul.flush()
        except IOError:
            self.logger.logError("Caught IOError.  Wait 1 second and try again")
            time.sleep(1.0)
            self.logger.logDebug("repeat: mbfits hdul flush Monitor data")
            self.hdul.flush()

        self.logger.logDebug("set write_monitor_finished Event")
        self.write_monitor_finished.set()

        # Unlock access to the objects in this worker thread and allow other waiting worker threads
        # to continue
        self.logger.logTrace("mutex unlock")
        self.mutex.unlock()


# Main defined only for testing with data in native format
# Not passing data throuth IDL type translations
if __name__ == "__main__":
    p = PipelineALMATIfits()
