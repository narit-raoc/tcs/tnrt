# -*- coding: ascii -*-
# Project: TNRT - Thai National Radio Telescope
# Organization: NARIT - National Astronomical Research Institute of Thailand
# Author: Spiro Sarris
# Date: 2021.05.06

import numpy as np
from astropy.io.fits import Header
from astropy.io.fits import Card
from astropy.io.fits import ColDefs

# -------------------------------------------------------------
# Generate MONITOR headers
# -------------------------------------------------------------
# Fits header can generate cards automatically from tuple (keyword', value, comment),
# but create the list of Card objects explicitly to make more obvious to
# someone who reads this code in the future (SS).


class DataFormatMONITORfits:
    def generate_header_monitor_antenna_PacketHeader(hdr_dict):
        """
        Generate MONITOR-ANTENNA header from IDL struct.
        This is not an official EXTNAME of MBFITS, but organize monitor data in separate BindTableHDUs
        makes work in DataAggregator easier.  Each BinTableHDU of monitor data is a group of monitor points that
        have value available at the same time.

        NOTE: This function reads input from the ARRAYDATA header beacuse the basic info is the same.  The data file
        will have 1 MONITOR-ANTENNA BinTableHDU for each ARRAYDATA BinTableHDU (per subscan). The monitor table header
        could also be read from DATAPAR table and have the same data, but read from ARRAYDATA header follows the
        chapters of MBFITS specification document.

        Parameters
        ----------
        hdr_dict : ScanMod.SCAN_ARRAYDATA_HEADER_TYPE

        Returns
        -------
        hdr_apy : astropy.io.fits.Header

        """
        cards = [
            Card(
                keyword="EXTNAME",
                value="MONITOR-ANT-PACKET",
                comment='name "MONITOR-ANT-PACKET"',
            ),
            Card(keyword="SCANNUM", value=hdr_dict['SCANNUM'], comment="Scan number"),
            Card(keyword="SUBSNUM", value=hdr_dict['SUBSNUM'], comment="Subscan number"),
            Card(
                keyword="DATE-OBS",
                value=hdr_dict['DATE_OBS'],
                comment="Subscan start in TIMESYS system [23A]",
            ),
        ]
        header_apy = Header(cards)
        return header_apy

    def generate_header_monitor_antenna_GeneralStatus(hdr_dict):
        cards = [
            Card(
                keyword="EXTNAME",
                value="MONITOR-ANT-GENERAL",
                comment='name "MONITOR-ANT-GENERAL"',
            ),
            Card(keyword="SCANNUM", value=hdr_dict['SCANNUM'], comment="Scan number"),
            Card(keyword="SUBSNUM", value=hdr_dict['SUBSNUM'], comment="Subscan number"),
            Card(
                keyword="DATE-OBS",
                value=hdr_dict['DATE_OBS'],
                comment="Subscan start in TIMESYS system [23A]",
            ),
        ]
        header_apy = Header(cards)
        return header_apy

    def generate_header_monitor_antenna_SystemStatus(hdr_dict):
        cards = [
            Card(
                keyword="EXTNAME",
                value="MONITOR-ANT-SYSTEM",
                comment='name "MONITOR-ANT-SYSTEM"',
            ),
            Card(keyword="SCANNUM", value=hdr_dict['SCANNUM'], comment="Scan number"),
            Card(keyword="SUBSNUM", value=hdr_dict['SUBSNUM'], comment="Subscan number"),
            Card(
                keyword="DATE-OBS",
                value=hdr_dict['DATE_OBS'],
                comment="Subscan start in TIMESYS system [23A]",
            ),
        ]
        header_apy = Header(cards)
        return header_apy

    def generate_header_monitor_antenna_VertexShutterStatus(hdr_dict):
        cards = [
            Card(
                keyword="EXTNAME",
                value="MONITOR-ANT-VERTEX",
                comment='name "MONITOR-ANT-VERTEX"',
            ),
            Card(keyword="SCANNUM", value=hdr_dict['SCANNUM'], comment="Scan number"),
            Card(keyword="SUBSNUM", value=hdr_dict['SUBSNUM'], comment="Subscan number"),
            Card(
                keyword="DATE-OBS",
                value=hdr_dict['DATE_OBS'],
                comment="Subscan start in TIMESYS system [23A]",
            ),
        ]
        header_apy = Header(cards)
        return header_apy

    def generate_header_monitor_antenna_AxisStatus(hdr_dict):
        cards = [
            Card(
                keyword="EXTNAME",
                value="MONITOR-ANT-AXIS",
                comment='name "MONITOR-ANT-AXIS"',
            ),
            Card(keyword="SCANNUM", value=hdr_dict['SCANNUM'], comment="Scan number"),
            Card(keyword="SUBSNUM", value=hdr_dict['SUBSNUM'], comment="Subscan number"),
            Card(
                keyword="DATE-OBS",
                value=hdr_dict['DATE_OBS'],
                comment="Subscan start in TIMESYS system [23A]",
            ),
        ]
        header_apy = Header(cards)
        return header_apy

    def generate_header_monitor_antenna_MotorStatus(hdr_dict):
        cards = [
            Card(
                keyword="EXTNAME",
                value="MONITOR-ANT-MOTOR",
                comment='name "MONITOR-ANT-MOTOR"',
            ),
            Card(keyword="SCANNUM", value=hdr_dict['SCANNUM'], comment="Scan number"),
            Card(keyword="SUBSNUM", value=hdr_dict['SUBSNUM'], comment="Subscan number"),
            Card(
                keyword="DATE-OBS",
                value=hdr_dict['DATE_OBS'],
                comment="Subscan start in TIMESYS system [23A]",
            ),
        ]
        header_apy = Header(cards)
        return header_apy

    def generate_header_monitor_antenna_TrackingStatus(hdr_dict):
        cards = [
            Card(
                keyword="EXTNAME",
                value="MONITOR-ANT-TRACKING",
                comment='name "MONITOR-ANT-TRACKING"',
            ),
            Card(keyword="SCANNUM", value=hdr_dict['SCANNUM'], comment="Scan number"),
            Card(keyword="SUBSNUM", value=hdr_dict['SUBSNUM'], comment="Subscan number"),
            Card(
                keyword="DATE-OBS",
                value=hdr_dict['DATE_OBS'],
                comment="Subscan start in TIMESYS system [23A]",
            ),
        ]
        header_apy = Header(cards)
        return header_apy

    def generate_header_monitor_antenna_GeneralHexapodStatus(hdr_dict):
        cards = [
            Card(
                keyword="EXTNAME",
                value="MONITOR-ANT-HXP-GEN",
                comment='name "MONITOR-ANT-HXP-GEN"',
            ),
            Card(keyword="SCANNUM", value=hdr_dict['SCANNUM'], comment="Scan number"),
            Card(keyword="SUBSNUM", value=hdr_dict['SUBSNUM'], comment="Subscan number"),
            Card(
                keyword="DATE-OBS",
                value=hdr_dict['DATE_OBS'],
                comment="Subscan start in TIMESYS system [23A]",
            ),
        ]
        header_apy = Header(cards)
        return header_apy

    def generate_header_monitor_antenna_HexapodStatus(hdr_dict):
        cards = [
            Card(
                keyword="EXTNAME",
                value="MONITOR-ANT-HXP",
                comment='name "MONITOR-ANT-HXP"',
            ),
            Card(keyword="SCANNUM", value=hdr_dict['SCANNUM'], comment="Scan number"),
            Card(keyword="SUBSNUM", value=hdr_dict['SUBSNUM'], comment="Subscan number"),
            Card(
                keyword="DATE-OBS",
                value=hdr_dict['DATE_OBS'],
                comment="Subscan start in TIMESYS system [23A]",
            ),
        ]
        header_apy = Header(cards)
        return header_apy

    def generate_header_monitor_antenna_SpindleStatus(hdr_dict):
        cards = [
            Card(
                keyword="EXTNAME",
                value="MONITOR-ANT-SPINDLE",
                comment='name "MONITOR-ANT-SPINDLE"',
            ),
            Card(keyword="SCANNUM", value=hdr_dict['SCANNUM'], comment="Scan number"),
            Card(keyword="SUBSNUM", value=hdr_dict['SUBSNUM'], comment="Subscan number"),
            Card(
                keyword="DATE-OBS",
                value=hdr_dict['DATE_OBS'],
                comment="Subscan start in TIMESYS system [23A]",
            ),
        ]
        header_apy = Header(cards)
        return header_apy

    def generate_header_monitor_antenna_HexapodTrackStatus(hdr_dict):
        cards = [
            Card(
                keyword="EXTNAME",
                value="MONITOR-ANT-HXP-TRACK",
                comment='name "MONITOR-ANT-HXP-TRACK"',
            ),
            Card(keyword="SCANNUM", value=hdr_dict['SCANNUM'], comment="Scan number"),
            Card(keyword="SUBSNUM", value=hdr_dict['SUBSNUM'], comment="Subscan number"),
            Card(
                keyword="DATE-OBS",
                value=hdr_dict['DATE_OBS'],
                comment="Subscan start in TIMESYS system [23A]",
            ),
        ]
        header_apy = Header(cards)
        return header_apy

    def generate_header_monitor_antenna_TrackingObjectStatus(hdr_dict):
        cards = [
            Card(
                keyword="EXTNAME",
                value="MONITOR-ANT-TRACKING-OBJECT",
                comment='name "MONITOR-ANT-TRACKING-OBJECT"',
            ),
            Card(keyword="SCANNUM", value=hdr_dict['SCANNUM'], comment="Scan number"),
            Card(keyword="SUBSNUM", value=hdr_dict['SUBSNUM'], comment="Subscan number"),
            Card(
                keyword="DATE-OBS",
                value=hdr_dict['DATE_OBS'],
                comment="Subscan start in TIMESYS system [23A]",
            ),
        ]
        header_apy = Header(cards)
        return header_apy

    def generate_header_monitor_weather(hdr_dict):
        """
        Generate MONITOR-WEATHER header from IDL struct.
        This is not an official EXTNAME of MBFITS, but organize monitor data in separate BindTableHDUs
        makes work in DataAggregator easier.  Each BinTableHDU of monitor data is a group of monitor points that
        have value available at the same time.

        NOTE: This function reads input from the ARRAYDATA header beacuse the basic info is the same.  The data file
        will have 1 MONITOR-ANTENNA BinTableHDU for each ARRAYDATA BinTableHDU (per subscan). The monitor table header
        could also be read from DATAPAR table and have the same data, but read from ARRAYDATA header follows the
        chapters of MBFITS specification document.

        Parameters
        ----------
        hdr_dict : ScanMod.SCAN_ARRAYDATA_HEADER_TYPE

        Returns
        -------
        hdr_apy : astropy.io.fits.Header

        """
        cards = [
            Card(
                keyword="EXTNAME",
                value="MONITOR-WEATHER",
                comment='name "MONITOR-WEATHER"',
            ),
            Card(keyword="SCANNUM", value=hdr_dict['SCANNUM'], comment="Scan number"),
            Card(keyword="SUBSNUM", value=hdr_dict['SUBSNUM'], comment="Subscan number"),
            Card(
                keyword="DATE-OBS",
                value=hdr_dict['DATE_OBS'],
                comment="Subscan start in TIMESYS system [23A]",
            ),
        ]
        header_apy = Header(cards)
        return header_apy

    def generate_header_monitor_frontend(hdr_dict):
        cards = [
            Card(
                keyword="EXTNAME",
                value="MONITOR-FRONTEND",
                comment='name "MONITOR-FRONTEND"',
            ),
            Card(keyword="SCANNUM", value=hdr_dict['SCANNUM'], comment="Scan number"),
            Card(keyword="SUBSNUM", value=hdr_dict['SUBSNUM'], comment="Subscan number"),
            Card(
                keyword="DATE-OBS",
                value=hdr_dict['DATE_OBS'],
                comment="Subscan start in TIMESYS system [23A]",
            ),
        ]
        header_apy = Header(cards)
        return header_apy
    # -------------------------------------------------------------
    # Generate binary data table arrays
    # -------------------------------------------------------------
    # Fits BinTableHDU can generate a binary data table from a Numpy structured
    # array with named fields and numpy datatypes, but I can't find a way to add
    # units (MHz, km/s, Jy, etc) in the numpy arrays, so add the units later to the coldefs.

    # Create a dtype for each section of Antenna status message.  Ideally, I would like to keep all
    # of the Antenna / ACU status message in one BinTableHDU for FITS, but astropy.io.fits does not
    # appear to support multi-level nested data formats in the same
    # way that numpy can create infinitely complicated multi-level nested formats.
    # We can't simply flatten the structure into a long list of dictionary keys because of namespace
    # collision.  Some sections use the same keyword as other sections.  For example:
    # "bit_status" is used in several different namespaces.  If we want to iterate through
    # the AntennaStatus IDL structure and use the same keyword names, we must create a separate BinTableHDU
    # for each section of the Antenna monitor data.

    # The antenna monitor array dimensions are constant and do not depend on any other scan
    # configuration parameters. Therefore, we don' tneed a generate_xxx() function.  Create
    # the types once here and reuse in other modules.

    dtype_monitor_antenna_PacketHeader_row = np.dtype(
        [("time_tcs_mjd", np.float64), ("status_message_counter", np.uint32)]
    )

    COUNT_TEMPERATURE_SENSORS = 16
    dtype_monitor_antenna_GeneralStatus_row = np.dtype(
        [
            ("version", np.uint16),
            ("diagnosis_signal", np.float64),
            ("facility_control_status", np.uint32),
            ("power_status", np.uint32),
            ("operation_status", np.uint32),
            ("system_warnings", np.uint32),
            ("software_interlock", np.uint32),
            ("emergency_stops", np.uint32),
            ("safety_device_errors", np.uint32),
            ("safety_device_errors_aux", np.uint32),
            ("communication_status", np.uint32),
            ("control_mode_status", np.uint32),
            ("actual_time", np.float64),
            ("actual_time_offset", np.float64),
            ("time_source", np.uint16),
            ("no_pps_signal", np.uint8),
            ("clock_online", np.uint8),
            ("clock_ok", np.uint8),
            ("wind_speed", np.float32),
            ("temperature_M1", np.float32, COUNT_TEMPERATURE_SENSORS),
        ]
    )

    dtype_monitor_antenna_SystemStatus_row = np.dtype([("bit_status", np.uint32)])

    dtype_monitor_antenna_VertexShutterStatus_row = np.dtype(
        [
            ("status", np.uint16),
            ("elements_open", np.uint16),
            ("elements_close", np.uint16),
            ("command_elements_open", np.uint16),
            ("command_elements_close", np.uint16),
        ]
    )

    COUNT_AXIS = 8
    dtype_monitor_antenna_AxisStatus_row = np.dtype(
        [
            ("bit_sts", np.uint32, COUNT_AXIS),
            ("stow_status", np.uint32, COUNT_AXIS),
            ("error_status", np.uint32, COUNT_AXIS),
            ("warning_status", np.uint32, COUNT_AXIS),
            ("encoder_disc_status", np.uint32, COUNT_AXIS),
            ("state", np.uint16, COUNT_AXIS),
            ("trajectory_state", np.uint16, COUNT_AXIS),
            ("actual_mode", np.uint16, COUNT_AXIS),
            ("desired_position", np.float64, COUNT_AXIS),
            ("trajectory_generator_position", np.float64, COUNT_AXIS),
            ("encoder_position_incl_pcorr", np.float64, COUNT_AXIS),
            ("actual_position", np.float64, COUNT_AXIS),
            ("current_position_offset", np.float64, COUNT_AXIS),
            ("position_deviation", np.float64, COUNT_AXIS),
            ("filtered_position_deviation", np.float64, COUNT_AXIS),
            ("current_pointing_correction", np.float64, COUNT_AXIS),
            ("external_position_offset", np.float64, COUNT_AXIS),
            ("trajectory_generator_velocity", np.float64, COUNT_AXIS),
            ("desired_velocity", np.float64, COUNT_AXIS),
            ("current_velocity", np.float64, COUNT_AXIS),
            ("trajectory_generator_acceleration", np.float64, COUNT_AXIS),
            ("motor_selection", np.uint16, COUNT_AXIS),
            ("brakes_selection", np.uint16, COUNT_AXIS),
            ("brakes_open", np.uint16, COUNT_AXIS),
            ("brakes_command", np.uint16, COUNT_AXIS),
        ]
    )

    COUNT_MOTORS = 16
    dtype_monitor_antenna_MotorStatus_row = np.dtype(
        [
            ("bit_sts", np.uint32, COUNT_MOTORS),
            ("actual_position", np.float64, COUNT_MOTORS),
            ("actual_velocity", np.float64, COUNT_MOTORS),
            ("actual_torque", np.float64, COUNT_MOTORS),
            ("utilization_rate", np.float64, COUNT_MOTORS),
            ("motor_temperature", np.float32, COUNT_MOTORS),
            ("motor_overload_inverter", np.float32, COUNT_MOTORS),
            ("motor_overload_motor", np.float32, COUNT_MOTORS),
            ("motor_error_instance", np.uint16, COUNT_MOTORS),
            ("motor_error_reaction", np.uint16, COUNT_MOTORS),
            ("motor_error_module", np.uint16, COUNT_MOTORS),
            ("motor_error_code", np.uint16, COUNT_MOTORS),
            ("limit_down", np.uint8, COUNT_MOTORS),
            ("limit_up", np.uint8, COUNT_MOTORS),
        ]
    )

    dtype_monitor_antenna_TrackingStatus_row = np.dtype(
        [
            ("bit_status", np.uint32),
            ("prog_track_bs", np.uint32),
            ("prog_track_act_index", np.uint32),
            ("prog_track_end_index", np.uint32),
            ("prog_offset_bs", np.uint32),
            ("prog_offset_act_index", np.uint32),
            ("prog_offset_end_index", np.uint32),
            ("sun_track_az", np.float32),
            ("sun_track_el", np.float32),
            ("prog_track_az", np.float32),
            ("prog_track_el", np.float32),
            ("star_track_az", np.float32),
            ("star_track_el", np.float32),
            ("tle_track_az", np.float32),
            ("tle_track_el", np.float32),
            ("prog_offset_az", np.float64),
            ("prog_offset_el", np.float64),
            ("position_offset_az", np.float64),
            ("position_offset_el", np.float64),
            ("time_offset", np.float32),
            ("desired_az", np.float64),
            ("desired_el", np.float64),
            ("antenna_latitude", np.float64),
            ("antenna_longitude", np.float64),
            ("antenna_altitude", np.float64),
            ("inclinometer1_x", np.float64),
            ("inclinometer1_y", np.float64),
            ("inclinometer1_temp", np.float64),
            ("inclinometer2_x", np.float64),
            ("inclinometer2_y", np.float64),
            ("inclinometer2_temp", np.float64),
            ("inclinometer_model_correction_el", np.float64),
            ("p1", np.float64),
            ("p2", np.float64),
            ("p3", np.float64),
            ("p4", np.float64),
            ("p5", np.float64),
            ("p6", np.float64),
            ("p7", np.float64),
            ("p8", np.float64),
            ("p9", np.float64),
            ("pointing_model_correction_az", np.float64),
            ("pointing_model_correction_el", np.float64),
            ("r0", np.float64),
            ("b1", np.float64),
            ("b2", np.float64),
            ("refraction_model_correction_el", np.float64),
            ("user_fine_pointing_correction_az", np.float64),
            ("user_fine_pointing_correction_el", np.float64),
            ("total_pointing_correction_az", np.float64),
            ("total_pointing_correction_el", np.float64),
            ("total_pointing_correction_grs", np.float64),
            ("dut1", np.int16),
            ("gst0hut", np.uint32),
        ]
    )

    dtype_monitor_antenna_GeneralHexapodStatus_row = np.dtype(
        [("general_hexapod_status", np.uint32)]
    )

    COUNT_HEXAPOD_AXIS = 6
    dtype_monitor_antenna_HexapodStatus_row = np.dtype(
        [
            ("bit_sts", np.uint32),
            ("bit_error", np.uint32),
            ("bit_warning", np.uint32),
            ("state", np.uint16),
            ("spindle_select", np.uint16),
            ("mode", np.uint16),
            ("pos_desired", np.float32, COUNT_HEXAPOD_AXIS),
            ("velocity_desired", np.float32, COUNT_HEXAPOD_AXIS),
            ("pos_trajectory", np.float32, COUNT_HEXAPOD_AXIS),
            ("velocity_trajectory", np.float32, COUNT_HEXAPOD_AXIS),
            ("offset", np.float32, COUNT_HEXAPOD_AXIS),
            ("pos_actual", np.float32, COUNT_HEXAPOD_AXIS),
            ("velocity_actual", np.float32, COUNT_HEXAPOD_AXIS),
        ]
    )

    COUNT_SPINDLES = 6
    dtype_monitor_antenna_SpindleStatus_row = np.dtype(
        [
            ("motor_sts", np.uint32, COUNT_SPINDLES),
            ("motor_temperature", np.float32, COUNT_SPINDLES),
            ("motor_temperature_inverter", np.float32, COUNT_SPINDLES),
            ("motor_overload_motor", np.float32, COUNT_SPINDLES),
            ("motor_error_instance", np.uint16, COUNT_SPINDLES),
            ("motor_error_reaction", np.uint16, COUNT_SPINDLES),
            ("motor_error_module", np.uint16, COUNT_SPINDLES),
            ("motor_error_code", np.uint16, COUNT_SPINDLES),
            ("upper_limit_switch", np.uint8, COUNT_SPINDLES),
            ("lower_limit_switch", np.uint8, COUNT_SPINDLES),
            ("actual_torque", np.float32, COUNT_SPINDLES),
            ("rate_loop", np.uint16, COUNT_SPINDLES),
            ("actual_velocity", np.float32, COUNT_SPINDLES),
            ("spindle_sts", np.uint32, COUNT_SPINDLES),
            ("spindle_errors", np.uint32, COUNT_SPINDLES),
            ("spindle_warnings", np.uint32, COUNT_SPINDLES),
            ("actual_position", np.float32, COUNT_SPINDLES),
        ]
    )

    COUNT_HEXAPOD_AXIS = 6
    dtype_monitor_antenna_HexapodTrackStatus_row = np.dtype(
        [
            ("bit_sts", np.uint32),
            ("start_time", np.float64),
            ("actual_index", np.uint32),
            ("end_index", np.uint32),
            ("track_mode_actual_position", np.float64, COUNT_HEXAPOD_AXIS),
        ]
    )

    LEN_TLE_NAME = 25
    LEN_TLE_LINE = 70
    dtype_monitor_antenna_TrackingObjectStatus_row = np.dtype(
        [
            ("prog_track_mode", np.uint16),
            ("time_offset", np.float32),
            ("tracking_object_position_offset_az", np.float32),
            ("tracking_object_position_offset_el", np.float32),
            ("tle_name", np.unicode_, LEN_TLE_NAME),
            ("tle_line1", np.unicode_, LEN_TLE_LINE),
            ("tle_line2", np.unicode_, LEN_TLE_LINE),
            ("pt_interpolation_mode", np.uint16),
            ("pt_track_mode", np.uint16),
            ("pt_end_index", np.uint16),
            ("pt_start_time", np.float64),
            ("pt_table11", np.float64),
            ("pt_table12", np.float64),
            ("pt_table21", np.float64),
            ("pt_table22", np.float64),
            ("po_interpolation_mode", np.uint16),
            ("po_track_mode", np.uint16),
            ("po_end_index", np.uint16),
            ("po_start_time", np.float64),
            ("po_table11", np.float64),
            ("po_table12", np.float64),
            ("po_table21", np.float64),
            ("po_table22", np.float64),
            ("pt_star_track_epoch", np.uint16),
            ("pt_star_track_ra", np.float64),
            ("pt_star_track_dec", np.float64),
        ]
    )

    dtype_monitor_weather_row = np.dtype(
        [("MJD", np.float64)]  # MJD at integration midpoint in TIMESYS system
    )

    CHAIN_COUNT = 2
    dtype_monitor_frontend_row = np.dtype(
        [   
            ("timestamp", np.float64),
            # tfr
            ("level1pps", np.float64),
            ("level100mhz", np.float64),
            ("valve_sts", np.uint32),
            # box weather
            ("temp_cold", np.float64),
            ("temp_warm", np.float64),
            # auto temp control
            ("temp_object", np.float64),
            ("temp_sink", np.float64),
            # LNA
            ("lna_status", np.uint32, CHAIN_COUNT),
            ("attenuation", np.float64, CHAIN_COUNT),
            ("total_power", np.float64, CHAIN_COUNT),
            # Dewar
            ("heater20k", np.float64),
            ("heater70k", np.float64),
            ("t15", np.float64),
            ("t70", np.float64),
            # down converter
            ("down_converter_freq", np.float64),
        ]
    )
    # -------------------------------------------------------------
    # Generate ColDefs.  Add units if present
    # -------------------------------------------------------------
    # Fits BinTableHDU can generate data table directly from numpy structured array,
    # but I don't see a way to add units.  Therefore, create Coldefs objects
    # from numpy structured arrays, and add units if present.
    # Then BinTableHDU can read this ColdDefs object including the original
    # numpy array and column units.

    def generate_coldefs_monitor_antenna_PacketHeader(data_ndarray):
        coldefs = ColDefs(data_ndarray)
        coldefs["time_tcs_mjd"].unit = "day[MJD]"
        return coldefs

    def generate_coldefs_monitor_antenna_GeneralStatus(data_ndarray):
        coldefs = ColDefs(data_ndarray)
        coldefs["actual_time"].unit = "day[MJD]"
        coldefs["actual_time_offset"].unit = "s"
        coldefs["wind_speed"].unit = "km/h"
        coldefs["temperature_M1"].unit = "C"
        return coldefs

    def generate_coldefs_monitor_antenna_SystemStatus(data_ndarray):
        coldefs = ColDefs(data_ndarray)
        return coldefs

    def generate_coldefs_monitor_antenna_VertexShutterStatus(data_ndarray):
        coldefs = ColDefs(data_ndarray)
        return coldefs

    def generate_coldefs_monitor_antenna_AxisStatus(data_ndarray):
        coldefs = ColDefs(data_ndarray)
        coldefs["desired_position"].unit = "deg"
        coldefs["trajectory_generator_position"].unit = "deg"
        coldefs["encoder_position_incl_pcorr"].unit = "deg"
        coldefs["actual_position"].unit = "deg"
        coldefs["current_position_offset"].unit = "deg"
        coldefs["position_deviation"].unit = "deg"
        coldefs["filtered_position_deviation"].unit = "deg"
        coldefs["current_pointing_correction"].unit = "deg"
        coldefs["trajectory_generator_velocity"].unit = "deg/s"
        coldefs["desired_velocity"].unit = "deg/s"
        coldefs["current_velocity"].unit = "deg/s"
        coldefs["trajectory_generator_acceleration"].unit = "deg/s/s"
        return coldefs

    def generate_coldefs_monitor_antenna_MotorStatus(data_ndarray):
        coldefs = ColDefs(data_ndarray)
        coldefs["actual_position"].unit = "rot"
        coldefs["actual_velocity"].unit = "rot/min"
        coldefs["actual_torque"].unit = "Nm"
        coldefs["utilization_rate"].unit = "percent"
        coldefs["motor_temperature"].unit = "C"
        coldefs["motor_overload_inverter"].unit = "%"
        coldefs["motor_overload_motor"].unit = "%"
        return coldefs

    def generate_coldefs_monitor_antenna_TrackingStatus(data_ndarray):
        coldefs = ColDefs(data_ndarray)
        coldefs["sun_track_az"].unit = "deg"
        coldefs["sun_track_el"].unit = "deg"
        coldefs["prog_track_az"].unit = "deg"
        coldefs["prog_track_el"].unit = "deg"
        coldefs["star_track_az"].unit = "deg"
        coldefs["star_track_el"].unit = "deg"
        coldefs["tle_track_az"].unit = "deg"
        coldefs["tle_track_el"].unit = "deg"
        coldefs["prog_offset_az"].unit = "deg"
        coldefs["prog_offset_el"].unit = "deg"
        coldefs["position_offset_az"].unit = "deg"
        coldefs["position_offset_el"].unit = "deg"
        coldefs["time_offset"].unit = "s"
        coldefs["desired_az"].unit = "deg"
        coldefs["desired_el"].unit = "deg"
        coldefs["antenna_latitude"].unit = "deg"
        coldefs["antenna_longitude"].unit = "deg"
        coldefs["antenna_altitude"].unit = "m"
        coldefs["inclinometer1_x"].unit = "deg"
        coldefs["inclinometer1_y"].unit = "deg"
        coldefs["inclinometer1_temp"].unit = "C"
        coldefs["inclinometer2_x"].unit = "deg"
        coldefs["inclinometer2_y"].unit = "deg"
        coldefs["inclinometer2_temp"].unit = "C"
        coldefs["inclinometer_model_correction_el"].unit = "deg"
        coldefs["p1"].unit = "arcsec"
        coldefs["p2"].unit = "arcsec"
        coldefs["p3"].unit = "arcsec"
        coldefs["p4"].unit = "arcsec"
        coldefs["p5"].unit = "arcsec"
        coldefs["p6"].unit = "arcsec"
        coldefs["p7"].unit = "arcsec"
        coldefs["p8"].unit = "arcsec"
        coldefs["p9"].unit = "arcsec"
        coldefs["pointing_model_correction_az"].unit = "deg"
        coldefs["pointing_model_correction_el"].unit = "deg"
        coldefs["r0"].unit = ""
        coldefs["b1"].unit = ""
        coldefs["b2"].unit = ""
        coldefs["refraction_model_correction_el"].unit = "deg"
        coldefs["user_fine_pointing_correction_az"].unit = "mdeg"
        coldefs["user_fine_pointing_correction_el"].unit = "mdeg"
        coldefs["total_pointing_correction_az"].unit = "deg"
        coldefs["total_pointing_correction_el"].unit = "deg"
        coldefs["total_pointing_correction_grs"].unit = "deg"
        coldefs["dut1"].unit = "ms"
        coldefs["gst0hut"].unit = "ms"
        return coldefs

    def generate_coldefs_monitor_antenna_GeneralHexapodStatus(data_ndarray):
        coldefs = ColDefs(data_ndarray)
        return coldefs

    def generate_coldefs_monitor_antenna_HexapodStatus(data_ndarray):
        coldefs = ColDefs(data_ndarray)
        coldefs["pos_desired"].unit = "[mm]|[deg]"
        coldefs["velocity_desired"].unit = "[mm/s]|[deg/s]"
        coldefs["pos_trajectory"].unit = "[mm]|[deg]"
        coldefs["velocity_trajectory"].unit = "[mm/s]|[deg/s]"
        coldefs["offset"].unit = "[mm]|[deg]"
        coldefs["pos_actual"].unit = "[mm]|[deg]"
        coldefs["velocity_actual"].unit = "[mm/s]|[deg/s]"
        return coldefs

    def generate_coldefs_monitor_antenna_SpindleStatus(data_ndarray):
        coldefs = ColDefs(data_ndarray)
        coldefs["actual_torque"].unit = "Nm"
        coldefs["actual_velocity"].unit = "rot/min"
        coldefs["actual_position"].unit = "rot"
        return coldefs

    def generate_coldefs_monitor_antenna_HexapodTrackStatus(data_ndarray):
        coldefs = ColDefs(data_ndarray)
        return coldefs

    def generate_coldefs_monitor_antenna_TrackingObjectStatus(data_ndarray):
        coldefs = ColDefs(data_ndarray)
        coldefs["time_offset"].unit = "s"
        coldefs["tracking_object_position_offset_az"].unit = "deg"
        coldefs["tracking_object_position_offset_el"].unit = "deg"
        coldefs["pt_start_time"].unit = "day[MJD]"
        coldefs["pt_table11"].unit = "deg"
        coldefs["pt_table12"].unit = "deg"
        coldefs["pt_table21"].unit = "deg"
        coldefs["pt_table22"].unit = "deg"
        coldefs["po_start_time"].unit = "day[MJD]"
        coldefs["po_table11"].unit = "deg"
        coldefs["po_table12"].unit = "deg"
        coldefs["po_table21"].unit = "deg"
        coldefs["po_table22"].unit = "deg"
        return coldefs

    def generate_coldefs_monitor_weather(data_ndarray):
        coldefs = ColDefs(data_ndarray)
        # TODO add units
        return coldefs

    def generate_coldefs_monitor_frontend(data_ndarray):
        coldefs = ColDefs(data_ndarray)

        coldefs["timestamp"].unit='day[MJD]'
        #tfr
        coldefs["level1pps"].unit='V'
        coldefs["level100mhz"].unit='V'

        coldefs['valve_sts'].unit=' '

        # box weather
        coldefs["temp_cold"].unit='C'
        coldefs["temp_warm"].unit='C'
        
        # auto temp control
        coldefs["temp_object"].unit='C'
        coldefs["temp_sink"].unit='C'
        
        # LNA
        coldefs["lna_status"].unit=' ' 
        coldefs["attenuation"].unit='dB' 
        coldefs["total_power"].unit='dBm' 
        
        # Dewar
        coldefs["heater20k"].unit='%'
        coldefs["heater70k"].unit='%'
        coldefs["t15"].unit='K'
        coldefs["t70"].unit='K'

        coldefs['down_converter_freq'].unit='MHz'
        
        return coldefs