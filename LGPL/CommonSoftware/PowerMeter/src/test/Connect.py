import pyvisa

from time import sleep

from datetime import datetime

## Connect VISA
rm=pyvisa.ResourceManager()

inst = rm.open_resource('TCPIP0::192.168.90.171::inst0::INSTR')

#Set Timeout - 10 seconds
inst.timeout = 10000


r = inst.query("*IDN?")
r = str(r)

print(r)        
    

r = inst.write("*CLS")      # Clear all status registers

print(r)


# print type(inst)
# print inst

# r = inst.query("*IDN?")        

# print type(r)

# r = str(r)

# print type(r)

# print r

'''
import visa
import time
## Start of RF power meter example
## This example connects to an N1913A ro N1914A and takes a power 
## reading at the specified frequency
##This program requires PyVISA to run
## This example is provided "as is" by Keysight Technologies
## Please feel free to contact us for technical support at 1800/829/4444

#Change test frequency to frequency desired
testFreq=1000000000

rm = visa.ResourceManager()
#Change resource to VISA address of the power meter
SCPI_N191x_EPM = rm.open_resource('TCPIP0::10.80.7.15::inst0::INSTR')
SCPI_N191x_EPM.write(':INITiate1:CONTinuous %d' % (1))
SCPI_N191x_EPM.write('SENS:FREQ %d' % (testFreq))
temp_values = SCPI_N191x_EPM.query_ascii_values(':FETCh:SCALar:POWer:AC?')
result = temp_values[0]
print (result)
SCPI_N191x_EPM.close()
rm.close()

# end of Example
'''