#ifndef helloworldcppImpl_h
#define helloworldcppImpl_h

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif

#include <baciCharacteristicComponentImpl.h>
#include <baciSmartPropertyPointer.h>
#include <tcpsocketclient.h>
#include <acsThread.h>
#include <helloworldcppS.h>

using namespace baci;


// forward declaration
class helloworldcppImpl;

class ClientThread : public ACS::Thread
{
        public:
                ClientThread(const ACE_CString& name,
                                helloworldcppImpl * hwp,
                                const ACS::TimeInterval& responseTime=ThreadBase::defaultResponseTime,
                                const ACS::TimeInterval& sleepTime=ThreadBase::defaultSleepTime) :
                        ACS::Thread(name)
                {
                        ACS_TRACE("ServerThread::ServerThread");
                        loopCounter_m = 0;
                        hwp_p = hwp;
                }

                ~ClientThread() { ACS_TRACE("ServerThread::~ServerThread"); }

                virtual void runLoop();

        protected:
                int loopCounter_m;
                helloworldcppImpl * hwp_p;
};

class helloworldcppImpl : public virtual CharacteristicComponentImpl, public virtual POA_helloworldcppMod::helloworldcppIface

{

	public:
                /**
                * Constructor
                * @param containerServices ContainerServices which are needed for
                * various component related methods.
                * @param name component name
                */
                helloworldcppImpl(const ACE_CString& name, maci::ContainerServices* containerServices);

                /**
                 * Destructor
                 */
                virtual ~helloworldcppImpl();


/* ------------------- [ Lifecycle START interface ] --------------------- */

                /**
                 * Lifecycle method called at component initialization state.
                 * Overwrite it in case you need it.
                 */
                virtual void initialize(void) throw (acsErrTypeLifeCycle::acsErrTypeLifeCycleExImpl);

                /**
                 * Lifecycle method called at component execute state.
                 * Overwrite it in case you need it.
                 */
                virtual void execute(void) throw (acsErrTypeLifeCycle::acsErrTypeLifeCycleExImpl);

                /**
                 * Lifecycle method called at component clean up state.
                 * Overwrite it in case you need it.
		**/

		        virtual void cleanUp(void);

                /**
                 * Lifecycle method called at component abort state.
                 * Overwrite it in case you need it.
                 */
                virtual void aboutToAbort(void);

/* ------------------- [ Lifecycle END interface ] --------------- */

/* --------------------- [ CORBA START interface ] ----------------*/

		virtual void foo(void);
		virtual void readCDB(void);
		virtual void readCDB2(void);
		virtual void writeSocket(const char* message);
		virtual char* readSocket(void);
		virtual void checkServer(void);

/* --------------------- [ CORBA END interface ] ----------------*/

	private:

		Tcpsocketclient* m_socketClient_p;
		ClientThread* m_clientThread_p;
		//Tcpsocketclient newsock;
		bool m_socketAlive;
};


#endif /* helloworldcppImpl_h */
