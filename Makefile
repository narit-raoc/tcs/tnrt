#*******************************************************************************
# E.S.O. - ACS project
#
# "@(#) $Id: Makefile,v 1.195 2012/11/16 13:19:32 acaproni Exp $"
#
#

osrev  = $(shell uname -r)
os     = $(shell uname)

###############################################
# Modules in the various ACS sub-packages.    #
###############################################

MODULE_PREFIX = LGPL
#!#MODULES_KIT = vlt doc acs acstempl
MODULES_KIT =
#
# I skip doxygen, that should be after compat and before tat,
# because it is already built in the prepare phase.
#
#GMP = gmp
GMP =

ifeq ($(os),Linux)
  OS_DIST:=  "$(shell lsb_release -d)"
endif

MODULES_TOOLS =
MODULES_ACS = TemplatePy Atm MockAcu tnrtAntenna Scan Backend DataAggregator DataMonitor nash LBandPump Frontend
# MODULES_UTILS = NotificationChannel CentralDB
# MODULES_ACS = nash
######## end Modules ###########################

###############################################
# Macro definitions.                          #
###############################################
define makeIt
   ( ((/usr/bin/time -f "$1 COMPILATION TIME %E" make $(MAKE_FLAGS) -C $1 $2 2>&1) && ( echo "### ==> SUCCEDED" | tee -a  $4 >/dev/null )) || ( echo "### ==> FAILED $2 ! " | tee -a $3 $4 1>&2 )) | tee -a $3 $4 >/dev/null;
endef

define makeItAux
   (( make $(MAKE_FLAGS) -C $1 $2 2>&1 ) || ( echo "### ==> FAILED $2 ! " | tee -a $3 $4 1>&2 )) | tee -a $3 $4 >/dev/null;
endef

# SCM tag definition

SCM_URL = "$(shell git config --get remote.origin.url)"
SCM_BRANCH = "$(shell git symbolic-ref --short HEAD)"
SCM_COMMIT = "$(shell git rev-parse HEAD)"
SCM_TAG = "$(shell git describe --tags)"
SCM_DATE = "$(shell git show -s --format=%ci HEAD)"

###############################################

            # $(foreach utils, $(MODULES_UTILS), utils/$(utils))
MODULES = 	$(foreach tools, $(MODULES_TOOLS), $(MODULE_PREFIX)/Tools/$(tools))\
			$(foreach acs, $(MODULES_ACS), $(MODULE_PREFIX)/CommonSoftware/$(acs)) \
			
			

SHELL=/bin/ksh
ECHO=echo

ifdef MAKE_VERBOSE
	AT = 
	OUTPUT =
else
	AT = @
	OUTPUT = > /dev/null
endif
#

startupDir = $(shell pwd)

#
# This target just forward any make target to all modules
#
define canned
	@$(ECHO) "############ Executing '$@' on all ACS modules #################"
	@for member in  $(foreach name, $(MODULES), $(name) ) ; do \
			$(ECHO) "############ $${member}" ;\
			if [ ! -d $${member} ]; then \
						 echo "######## ==> $${member} MODULE NOT FOUND! FAILED! " | tee -a build.log;\
					fi;\
			if [ -f $${member}/src/Makefile ]; then \
			$(MAKE) $(MAKE_FLAGS) -C $${member}/src/ $@ || break ;\
			elif [ -f $${member}/ws/src/Makefile ]; then \
			$(MAKE) $(MAKE_FLAGS) -C $${member}/ws/src/ $@ || break ;\
			elif [ -f $${member}/Makefile ]; then \
			$(MAKE) $(MAKE_FLAGS) -C $${member}/ $@ | tee -a build.log;\
			fi;\
			if [ "$(VXWORKS_RTOS)" == "YES" ]; then \
			if [ -f $${member}/lcu/src/Makefile ]; then \
			$(MAKE) $(MAKE_FLAGS) -C $${member}/lcu/src/ $@ || break ;\
			fi;\
			fi;\
		done
endef

#
# This target builds and installs the complete TNRT
# on a clean directory structure from GIT repository
# Per each module it executes:
#    make clean all install
#
build: prepare scm-tag clean_log checkModuleTree install_cdb modify_startup copy_config_file init_cache update
	@$(ECHO) "... done"

clean_log:
	@$(ECHO) "############ Clean Build Log File: build.log #################"
	@rm -f build.log
	@touch build.log

clean_test_log:
	@$(ECHO) "############ Clean Test Log File: test.log #################"
	@rm -f test.log
	@touch test.log

#
# Check module tree
#
checkModuleTree:	
	@$(ECHO) "############ Check directory tree for modules  #################"| tee -a build.log
	@for member in  $(foreach name, $(MODULES), $(name) ) ; do \
			if [ ! -d $${member} ]; then \
						 echo "######## ==> $${member} MODULE NOT FOUND! FAILED! " | tee -a build.log;\
					fi;\
					if [ -f $${member}/Makefile ]; then \
						 $(SHELL) $(MODULE_PREFIX)/acsBUILD/src/acsBUILDCheckModuleTree.sh $${member} >> build.log 2>& 1;\
			fi;\
			if [ -f $${member}/src/Makefile ]; then \
						 $(SHELL) $(MODULE_PREFIX)/acsBUILD/src/acsBUILDCheckModuleTree.sh $${member} >> build.log 2>& 1;\
			fi;\
			if [ -f $${member}/ws/src/Makefile ]; then \
						 $(SHELL) $(MODULE_PREFIX)/acsBUILD/src/acsBUILDCheckModuleTree.sh $${member}/ws >> build.log 2>& 1;\
			fi;\
			if [ -f $${member}/lcu/src/Makefile ]; then \
						 $(SHELL) $(MODULE_PREFIX)/acsBUILD/src/acsBUILDCheckModuleTree.sh $${member}/lcu >> build.log 2>& 1;\
			fi;\
		done

#
# Empty target.  Remains from ACS Makefile.  Maybe TNRT will use it in the future
prepare:
ifeq ($(INTROOT),$(HOME)/introot)
	@$(ECHO) "Found INTROOT == ~/introot.  Delete directory and create blank template"
	@rm -rf $(INTROOT)
	@getTemplateForDirectory INTROOT $(INTROOT)
else
	@$(ECHO) "Found INTROOT not ~/introot.  Don't delete directory.  Fix INTROOT?"
endif

#
# Update of all core components
# According to SE standards does not make man and does not cleanup at the end.
#
# GCH 2005-02-02
#   Added a 'true' at the end of the look to ensure
#   that if the LAST module fails the whole Make does not fail#

update:	scm-tag checkModuleTree
	@$(ECHO) "############ (Re-)build TNRT Software         #################"| tee -a build.log
	@for member in  $(foreach name, $(MODULES), $(name) ) ; do \
			if [ ! -d $${member} ]; then \
					echo "######## ==> $${member} MODULE NOT FOUND! FAILED! " | tee -a build.log;\
					fi;\
			if [ -f $${member}/src/Makefile ]; then \
				 $(ECHO) "############ $${member} SRC" | tee -a build.log;\
						 $(call makeItAux,$${member}/src,clean,build.log,$${member}/src/NORM-BUILD-OUTPUT) \
						 $(call makeIt,$${member}/src,all,build.log,$${member}/src/NORM-BUILD-OUTPUT) \
			 $(call makeItAux,$${member}/src,install,build.log,$${member}/src/NORM-BUILD-OUTPUT) \
					elif [ -f $${member}/ws/src/Makefile ]; then \
				 $(ECHO) "############ $${member} WS" | tee -a build.log;\
			 $(call makeItAux,$${member}/ws/src,clean,build.log,$${member}/ws/src/NORM-BUILD-OUTPUT) \
						 $(call makeIt,$${member}/ws/src,all,build.log,$${member}/ws/src/NORM-BUILD-OUTPUT) \
			 $(call makeItAux,$${member}/ws/src,install,build.log,$${member}/ws/src/NORM-BUILD-OUTPUT) \
			elif [ -f $${member}/Makefile ]; then \
				 $(ECHO) "############ $${member} MAIN" | tee -a build.log;\
			 $(call makeItAux,$${member},-s $@,build.log,$${member}/NORM-BUILD-OUTPUT) \
			fi;\
			if [ "$(VXWORKS_RTOS)" == "YES" ]; then \
			if [ -f $${member}/lcu/src/Makefile ]; then \
			 $(ECHO) "############ $${member} LCU" | tee -a build.log;\
						 $(call makeItAux,$${member}/lcu/src,clean,build.log,$${member}/lcu/src/NORM-BUILD-OUTPUT) \
			 $(call makeIt,$${member}/lcu/src,all,build.log,$${member}/lcu/src/NORM-BUILD-OUTPUT) \
			 $(call makeItAux,$${member}/lcu/src,install,build.log,$${member}/lcu/src/NORM-BUILD-OUTPUT) \
			fi;\
			fi;\
		done;\
		 true;
	@$(ECHO) "############ DONE (Re-)build TNRT Software    #################"| tee -a build.log


# show_modules target
#
# Simply lists all MODULES that would be build
# with the current setup
#
show_modules:
	@$(ECHO) "Modules in build list are:" 
	@$(ECHO) ${MODULES}

################################################################
# SCM targets.
# 
# The following targets and expressions are helpers for GIT
# repository information in build process.
# Developers should be interested revision hash, branch, tag
################################################################

#
# This expression extracts the SCM tag for the tnrt git project
# (if exists).
#
# This target puts the SCM commit ID for the tnrt project
# (if exists) into a file, so that it can be used
# to mark an installation.
#
scm-tag:
	@ $(ECHO) $(OS_DIST);
	@ $(ECHO) "Checking Source Control Management (SCM) data of GIT project"; \
	if [ X$(SCM_COMMIT) != X ]; then \
		$(ECHO) "SCM_URL: $(SCM_URL)"; \
		$(ECHO) "SCM_COMMIT: $(SCM_COMMIT)"; \
		$(ECHO) "SCM_BRANCH: $(SCM_BRANCH)"; \
		$(ECHO) "SCM_TAG <last TAG>-[commits since last TAG]-[current commit short hash]: $(SCM_TAG)"; \
		$(ECHO) "SCM_DATE: $(SCM_DATE)"; \
		$(ECHO) $(SCM_URL) > $(INTROOT)/config/git_url.txt ; \
		$(ECHO) $(SCM_COMMIT) > $(INTROOT)/config/git_commit.txt ; \
		$(ECHO) $(SCM_BRANCH) > $(INTROOT)/config/git_branch.txt ; \
		$(ECHO) $(SCM_TAG) > $(INTROOT)/config/git_tag.txt ; \
		$(ECHO) $(SCM_DATE) > $(INTROOT)/config/git_date.txt ; \
	else \
		if [ -f commit_id.txt ]; then \
			$(ECHO) "file TNRT_REV already exists: "; \
			cat commit_id.txt; $(ECHO) ""; \
		else \
			$(ECHO) "No SCM revision data available"; \
		fi; \
	fi

nash_doc:
	@echo "clean sphinx doc"
	$(MAKE) -C LGPL/CommonSoftware/nash/sphinx-doc clean
	@echo "make target: sphinx"
	$(MAKE) -C LGPL/CommonSoftware/nash/sphinx-doc html

install_cdb:
ifneq (,$(wildcard LGPL/acsBUILD/config/defaultCDB/CDB))
	@ $(ECHO) "############ Copy Component Database to $(INTROOT)/config/CDB";
	$(shell cp -ar LGPL/acsBUILD/config/defaultCDB/CDB/* $(INTROOT)/config/CDB)
else
	@ $(ECHO) "!!!!!!!!!!!! ERROR file not found LGPL/acsBUILD/config/defaultCDB/CDB"
endif

modify_startup:
ifneq (,$(wildcard provision))
	@ $(ECHO) "############ Modify and Copy Container Config File to $(INTROOT)/config";
	$(shell cp -ar provision/TNRT_dev.xml $(INTROOT)/config)
	$(shell sed -i "s/REMOTE_HOST/$(shell hostname)/" $(INTROOT)/config/TNRT_dev.xml)
	$(shell sed -i "s/REMOTE_ACCOUNT/$$USER/" $(INTROOT)/config/TNRT_dev.xml)
	$(shell sed -i "s/CDB_ROOT/\/home\/$$USER\/introot\/config/" $(INTROOT)/config/TNRT_dev.xml)

	$(shell cp -ar provision/TNRT_production.xml $(INTROOT)/config)
	$(shell sed -i "s/REMOTE_HOST/$(shell hostname)/" $(INTROOT)/config/TNRT_production.xml)
	$(shell sed -i "s/REMOTE_ACCOUNT/$$USER/" $(INTROOT)/config/TNRT_production.xml)
	$(shell sed -i "s/CDB_ROOT/\/home\/$$USER\/introot\/config/" $(INTROOT)/config/TNRT_production.xml)

	$(shell cp -ar provision/TNRT_test_all_containers.xml $(INTROOT)/config)
	$(shell sed -i "s/REMOTE_HOST/$(shell hostname)/" $(INTROOT)/config/TNRT_test_all_containers.xml)
	$(shell sed -i "s/REMOTE_ACCOUNT/$$USER/" $(INTROOT)/config/TNRT_test_all_containers.xml)
	$(shell sed -i "s/CDB_ROOT/\/home\/$$USER\/introot\/config/" $(INTROOT)/config/TNRT_test_all_containers.xml)
else
	@ $(ECHO) "!!!!!!!!!!!! ERROR file not found startupTNRT.xml"
endif

copy_config_file:
ifneq (,$(wildcard provision))
	@ $(ECHO) "############ Copy Config File to $(INTROOT)/config";
	$(shell cp -ar provision/config.json $(INTROOT)/config)
else
	@ $(ECHO) "!!!!!!!!!!!! ERROR file not found config.json"
endif


init_cache: # to backup no internet connection
ifneq (,$(wildcard LGPL/CommonSoftware/Scan/src/ScanImpl))
	@ $(ECHO) "############ Copy cache directory to $(HOME)";
	$(shell cp -ar LGPL/CommonSoftware/Scan/src/ScanImpl/cache $(HOME))
else
	@ $(ECHO) "!!!!!!!!!!!! ERROR directory not found LGPL/CommonSoftware/Scan/src/ScanImpl/cache"
endif

#
# Standard targets
#
clean:	
	$(canned)
all:	
	$(canned)
install:	
	$(canned)

man:
	$(canned)

#
# ___oOo___
